#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;


#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;

#pragma link C++ class insane::physics::PhaseSpaceSampler+;
#pragma link C++ class insane::physics::_EG_Event+;
#pragma link C++ class insane::physics::EventGenerator+;
#pragma link C++ class insane::physics::TargetEventGenerator+;
#pragma link C++ class insane::physics::BETAG4SavedEventGenerator+;

#endif

