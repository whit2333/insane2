#ifndef _EG_Event_HH
#define _EG_Event_HH 1

#include "Particle.h"
#include "TClonesArray.h"
#include <vector>

namespace insane {
namespace physics {

class DiffXSec;

/** Transitional class for varous EG output formats.
 */ 
class _EG_Event {

   public:
      //Int_t                  fEventNumber;
      //Int_t                  fRunNumber;
      Int_t                  fXS_id;      // unique number
      Double_t               fBeamPol; 
      Double_t               fTargetPol; 
      DiffXSec       * fXSec;        //!
      TClonesArray         * fParticles;  //->
      std::vector<Particle*> fArray;
      std::vector<double>    fPSVariables;

   public:
      _EG_Event();
      virtual ~_EG_Event();

      void Clear();

      Particle * GetParticle(int i = 0) ;
      void SetParticles(TList * l) ;
      void SetPSVariables(double * , int);

      Particle * GetParticle(int i) const ;
      int              GetNParticles() const ;

      ClassDef(_EG_Event,6)
};

}
}

#endif

