#ifndef PhaseSpaceSampler_H
#define PhaseSpaceSampler_H 1
#include <TROOT.h>
#include <TObject.h>
#include "TFoam.h"
#include "TFoamIntegrand.h"
#include "TMath.h"
#include "DiffXSec.h"
#include "TRandom3.h"
#include "TList.h"
#include <iostream>
#include <iomanip>

namespace insane {
  namespace physics {

    /** Base class for a random sampling of a phase space.
     *  This class uses ROOT's TFoam classes for sampling the phase space.
     *  
     *  The phase-space variables are scaled to be between 0 and 1 in order to work with TFoam. 
     *  This is why the result of foam is pass through DiffXSec::GetUnnormalizedVariables();
     *  The final result of PhaseSpaceSampler::GenerateEvent() comes from passing the random 
     *  (unnormalized variables) through the virutal method DiffXSec::GetDependentVariables(),
     *  which appends any possible variables which are kinematically constrained (and thus not random).
     *  For example, in exclusive elastic scattering where the electron variables are random, the
     *  nuclei's variables are calculated and appended to the result.
     *
     *  Note: The sampler owns the cross section.
     *
     * \ingroup EventGen
     */
    class PhaseSpaceSampler : public TObject {
    protected:
      Double_t         fWeight;        /// Weight used by event generator
      Bool_t           fIsModified;
      Double_t         fTotalXSection;
      Double_t         fMCvect[30];     //
      DiffXSec*        fDiffXSec;      //!
      TFoam*           fFoam;          //->
      TRandom*         fRandomNumberGenerator; //!
      TList            fXSectionList;
      Int_t            fFoamCells;     // number of cells
      Int_t            fFoamSample;    // number of MC events per cell in build-up
      Int_t            fFoamBins;      // number of bins in build-up 
      Int_t            fFoamOptRej  ;  // Wted events for OptRej=0; wt=1 for OptRej=1 (default)
      Int_t            fFoamOptDrive;  // (D=2) Option, type of Drive =0,1,2 for TrueVol,Sigma,WtMax
      Int_t            fFoamEvPerBin;  // Maximum events (equiv.) per bin in buid-up
      Int_t            fFoamChat    ;  // Chat level  };
      Double_t         fFoamMaxWtRej;  // 1.0

      TList            fCrossSection;  // This exists to store the cross section which doesn't stream by itself
      // but does when it is in a container. When the sampler is read back from
      // a file it should initialized with this cross section by calling InitFromDisk

  public:
      PhaseSpaceSampler(DiffXSec * xsec = nullptr);

      PhaseSpaceSampler(const PhaseSpaceSampler& rhs);
      PhaseSpaceSampler& operator=(const PhaseSpaceSampler& rhs);

      //PhaseSpaceSampler(const PhaseSpaceSampler&) = default;               // Copy constructor
      PhaseSpaceSampler(PhaseSpaceSampler&&) = default;                    // Move constructor
      //PhaseSpaceSampler& operator=(const PhaseSpaceSampler&) & = default;  // Copy assignment operator
      PhaseSpaceSampler& operator=(PhaseSpaceSampler&&) & = default;       // Move assignment operato
      //virtual ~PhaseSpaceSampler() { }

      /** The sampler owns the XSection so it is deleted. */
      virtual ~PhaseSpaceSampler();

      void InitFromDisk() ;

      TFoam* GetFoam(){ return(fFoam); }

      void SetFoamCells(Int_t v)       { fFoamCells = v;  if(fFoam) fFoam->SetnCells(fFoamCells); }
      void SetFoamSample(Int_t v)      { fFoamSample = v; if(fFoam) fFoam->SetnSampl(fFoamSample); }
      void SetFoamBins(Int_t v)        { fFoamBins = v;   if(fFoam) fFoam->SetnBin(fFoamBins); }
      void SetFoamOptRej(Int_t v)      { fFoamOptRej = v;   if(fFoam) fFoam->SetOptRej(fFoamOptRej); }
      void SetFoamOptDrive(Int_t v)    { fFoamOptDrive = v;   if(fFoam) fFoam->SetOptDrive(fFoamOptDrive); }
      void SetFoamEvPerBin(Int_t v)    { fFoamEvPerBin = v; if(fFoam) fFoam->SetEvPerBin(fFoamEvPerBin); }
      void SetFoamChat(Int_t v)        { fFoamChat = v;   if(fFoam) fFoam->SetChat(fFoamChat); }
      void SetFoamMaxWtRej(Double_t v) { fFoamMaxWtRej = v; if(fFoam) fFoam->SetMaxWtRej(fFoamMaxWtRej); }

      void PrintFoamConfig(std::ostream& c = std::cout) const;

      /** Refresh creates a new TFoam object after changing the parameters
       *  of the phase space or cross section.
       *  If a cross section argument is provided the old one is deleted and the new one is used
       *  (creating a new phase space space too).
       */
      void Refresh(DiffXSec * xsec = nullptr) ;

      /** Generates a new event and returns an array of random variables.
       *  Returns array fiilled with variables.
       */
      Double_t * GenerateEvent();

      /** Calculates the cross section normalization (ie total cross section).
       *
       *  The cross section normalization is just the total cross section
       *  which is just the integral of the differential cross section over the
       *  entire phase space.
       */
      virtual Double_t NormalizePDF();

      /** Set Cross Section whos weighted phase-space is used to generate random variables.
      */
      void SetXSec(DiffXSec * xsec, Bool_t mod=true) ;

      DiffXSec* GetXSec() const { return(fDiffXSec); }

      void Print(const Option_t* option = "") const ;
      void Print(std::ostream& stream) const ;

      Double_t GetTotalXSection() const { return(fTotalXSection); }
      Double_t CalculateTotalXSection();

      bool IsModified() const { return fIsModified; }
      void SetModified(bool val = false) { fIsModified = val; }

      /** Set the weight used by the event generator. Must be between 0 and 1.0 */
      void SetWeight(Double_t w) {
        if( w == 0.0 ) Warning("SetWeight","Weight set to zero!");
        else if(w < 0.0 ) Error("SetWeight","Weight less than zero!");
        else if(w > 1.0 ) Error("SetWeight","Weight bigger than 1");
        else fWeight = w;
      }
      Double_t GetWeight(){return fWeight;}


      /** Work around to load the first cross section saved in list when the
       *  cross section is saved to a file. F
       *  INCOMPLETE
       */
      void Load() {
        if (fXSectionList.GetEntries() == 0) {
          std::cout << " NO SAVED CROSS SECTIONS!!!!\n";
        } else {
          SetXSec((DiffXSec*)fXSectionList.At(0));
          /*         GetXSec()->InitializePhaseSpaceVariables();         */
          //         fMomentum_pi = varEnergy2->GetCurrentValueAddress();

        }
      }

      ClassDef(PhaseSpaceSampler, 3)
  };

}
}

#endif

