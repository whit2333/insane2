#include "insane/fixedtarget/SANETargets.h"


namespace insane {


//_______________________________________________________________________________
UVAPolarizedAmmoniaTarget::UVAPolarizedAmmoniaTarget(const char * name, const char * title,Double_t pf): Target(name, title)
{
/*   TSQLServer * db = TSQLServer::Connect("mysql://quarks.temple.edu","sane","secret");
   TString  sql = Form(" SELECT beam_energy,target_angle FROM SANE.run_info WHERE run_number=%d ", run_number);
   SELECT packing_fraction FROM `packing_fractions` WHERE first_run < 72600  AND last_run > 72600*/

   fPackingFraction = pf;
   DefineMaterials();
}
//_______________________________________________________________________________
UVAPolarizedAmmoniaTarget::~UVAPolarizedAmmoniaTarget() {
}
//_______________________________________________________________________________

void UVAPolarizedAmmoniaTarget::DefineMaterials() {
   // From https://hallcweb.jlab.org/experiments/sane/wiki/index.php/UVa_Polarized_Target#Materials_in_Target
   auto * matH3  = new TargetMaterial("H3", "H3 in Ammonia", 1, 1);
   matH3->fLength                = 3.0;           //cm
   matH3->fZposition             = 0.15;          //cm
   matH3->fDensity               = 2.0 * 0.0770;  // g/cm3, mutiplied by 2 because the denisty assumes a 50% packing fraction
   matH3->fIsPolarized           = true;
   matH3->fPackingFraction       = fPackingFraction;

   auto * matN14 = new TargetMaterial("14N", "14N in Ammonia", 7, 14);
   matN14->fLength               = 3.0;           //cm
   matN14->fZposition            = 0.15;          //cm
   matN14->fDensity              = 2.0 * 0.3576;  //g/cm3 mutiplied by 2 because the denisty assumes a 50% packing fraction
   matN14->fPackingFraction      = fPackingFraction;

   auto * matHe  = new TargetMaterial("He", "LHe around Ammonia beads", 2, 4);
   matHe->fLength                = 3.0;      //cm
   matHe->fZposition             = 0.15;     //cm
   matHe->fDensity               = 0.1450;   // g/cm3
   matHe->fPackingFraction       = 1.0 - fPackingFraction;

   //TargetMaterial * matLHe = new TargetMaterial("LHe", "Nose Liquid Helium ", 2, 4);
   //matLHe->fLength               = 0.50000 * 2.0;  //cm
   //matLHe->fDensity              = 0.1450;         // g/cm3

   auto * matLHe1 = new TargetMaterial("LHe1", "Nose Liquid Helium upstream", 2, 4);
   matLHe1->fLength               =  0.50000;  //cm
   matLHe1->fDensity              =  0.1450;   //g/cm3
   matLHe1->fZposition            = -1.60;     //cm

   auto * matLHe2 = new TargetMaterial("LHe2", "Nose Liquid Helium downstream", 2, 4);
   matLHe2->fLength               =  0.50000;  //cm
   matLHe2->fDensity              =  0.1450;   // g/cm3
   matLHe2->fZposition            =  1.90;     //cm

   //TargetMaterial * matAlEndcap  = new TargetMaterial("Al-endcap", "Aluminum target endcap", 13, 27);
   //matAlEndcap->fLength                = 2.0 * 0.00381 ;  //cm
   //matAlEndcap->fDensity               = 2.7;             // g/cm3

   auto * matAlEndcap1 = new TargetMaterial("Al-endcap1", "Aluminum target endcap upstream", 13, 27);
   matAlEndcap1->fLength               =  0.00381 ;   //cm
   matAlEndcap1->fDensity              =  2.7;        // g/cm3
   matAlEndcap1->fZposition            = -1.35 ;     //cm

   auto * matAlEndcap2 = new TargetMaterial("Al-endcap2", "Aluminum target endcap downstream", 13, 27);
   matAlEndcap2->fLength               =  0.00381 ;  //cm
   matAlEndcap2->fDensity              =  2.7;       // g/cm3
   matAlEndcap2->fZposition            =  1.65;     //cm

   auto * matAlTail1 = new TargetMaterial("Al-tail1", "Aluminum target tail upstream", 13, 27);
   matAlTail1->fLength               = 0.01016  ;  //cm
   matAlTail1->fDensity              = 2.7;              // g/cm3
   matAlTail1->fZposition            = -2.05 ;     //cm

   auto * matAlTail2 = new TargetMaterial("Al-tail2", "Aluminum target tail downstream", 13, 27);
   matAlTail2->fLength               = 0.01016  ;  //cm
   matAlTail2->fDensity              = 2.7;              // g/cm3
   matAlTail2->fZposition            = 2.35 ;     //cm

   auto * matAl4kShield1 = new TargetMaterial("Al-4kshield1", "Aluminum target 4K shield upstream", 13, 27);
   matAl4kShield1->fLength               = 0.00254  ;  //cm
   matAl4kShield1->fDensity              = 2.7; // g/cm3
   matAl4kShield1->fZposition            = -3.80 ;     //cm

   auto * matAl4kShield2 = new TargetMaterial("Al-4kshield2", "Aluminum target 4K shield upstream", 13, 27);
   matAl4kShield2->fLength               = 0.00254  ;  //cm
   matAl4kShield2->fDensity              = 2.7; // g/cm3
   matAl4kShield2->fZposition            = 3.80 ;     //cm

   auto * matCu = new TargetMaterial("Cu", "NMR-Cu ", 29, 64);
   matCu->fLength          = 0.00050   ;    //cm
   matCu->fDensity         = 8.9600 ; // g/cm3
   matCu->fZposition       = 0.15 ;     //cm

   auto * matNi = new TargetMaterial("Ni", "NMR-Ni  ", 28, 59);
   matNi->fLength          = 0.00022   ;    //cm
   matNi->fDensity         = 8.9020  ; // g/cm3
   matNi->fZposition       = 0.15 ;     //cm

   this->AddMaterial(matH3);
   this->AddMaterial(matN14);
   this->AddMaterial(matHe);
   //this->AddMaterial(matLHe);
   this->AddMaterial(matLHe1);
   this->AddMaterial(matLHe2);
   this->AddMaterial(matAlEndcap1);
   this->AddMaterial(matAlEndcap2);
   this->AddMaterial(matAlTail1);
   this->AddMaterial(matAlTail2);
   this->AddMaterial(matAl4kShield1);
   this->AddMaterial(matAl4kShield2);
   this->AddMaterial(matCu);
   this->AddMaterial(matNi);
   
   matH3->fTGeoVolume->SetTransparency(50);
   matH3->fTGeoVolume->SetFillColor(4);
   matH3->fTGeoVolume->SetLineColor(4);

   matN14->fTGeoVolume->SetTransparency(100);
   matHe->fTGeoVolume->SetTransparency(100);

   matLHe1->fTGeoVolume->SetTransparency(50);
   matLHe1->fTGeoVolume->SetFillColor(6);
   matLHe1->fTGeoVolume->SetLineColor(6);

   matLHe2->fTGeoVolume->SetTransparency(50);
   matLHe2->fTGeoVolume->SetFillColor(6);
   matLHe2->fTGeoVolume->SetLineColor(6);

   matAlEndcap1->fTGeoVolume->SetTransparency(30);
   matAlEndcap1->fTGeoVolume->SetFillColor(2);
   matAlEndcap1->fTGeoVolume->SetLineColor(2);

   matAlEndcap2->fTGeoVolume->SetTransparency(30);
   matAlEndcap2->fTGeoVolume->SetFillColor(2);
   matAlEndcap2->fTGeoVolume->SetLineColor(2);

   matAlTail1->fTGeoVolume->SetTransparency(30);
   matAlTail1->fTGeoVolume->SetFillColor(3);
   matAlTail1->fTGeoVolume->SetLineColor(3);

   matAlTail2->fTGeoVolume->SetTransparency(30);
   matAlTail2->fTGeoVolume->SetFillColor(3);
   matAlTail2->fTGeoVolume->SetLineColor(3);

}
//_______________________________________________________________________________


UVACarbonTarget::UVACarbonTarget(const char * name, const char * title): Target(name, title)
{
   fPackingFraction = 0.6;
   DefineMaterials();
}
//_______________________________________________________________________________

UVACarbonTarget::~UVACarbonTarget()
{

}
//_______________________________________________________________________________

void UVACarbonTarget::DefineMaterials()
{
   auto * matH3 = new TargetMaterial("H3", "H3 in Ammonia", 1, 1);
   matH3->fLength          = 3.0;    //cm
   matH3->fDensity         = 2.0 * 0.0770/*/3.0*/; // g/cm3
   matH3->fIsPolarized     = true;
   matH3->fPackingFraction = fPackingFraction;

   auto * matN14 = new TargetMaterial("14N", "14N in Ammonia", 7, 14);
   matN14->fLength          = 3.0;    //cm
   matN14->fDensity         = 2.0 * 0.3576; // g/cm3
   matN14->fPackingFraction = fPackingFraction;

   auto * matHe = new TargetMaterial("He", "LHe around Ammonia beads", 2, 4);
   matHe->fLength          = 3.0;    //cm
   matHe->fDensity         = 0.1450; // g/cm3
   matHe->fPackingFraction = 1.0 - fPackingFraction;

   auto * matLHe = new TargetMaterial("LHe", "Liquid Helium", 2, 4);
   matLHe->fLength          = 0.50000 * 2.0;  //cm
   matLHe->fDensity         = 0.1450; // g/cm3

   auto * matAlEndcap = new TargetMaterial("Al-endcap", "Aluminum target endcp", 13, 27);
   matAlEndcap->fLength          = 2.0 * 0.00381 ;  //cm
   matAlEndcap->fDensity         = 2.7; // g/cm3

   auto * matAlTail = new TargetMaterial("Al-tail", "Aluminum target tail", 13, 27);
   matAlTail->fLength          = 2.0 * 0.01016  ;  //cm
   matAlTail->fDensity         = 2.7; // g/cm3

   auto * matAl4kShield = new TargetMaterial("Al-4kshield", "Aluminum target 4K shield", 13, 27);
   matAl4kShield->fLength          = 2.0 * 0.00254  ;  //cm
   matAl4kShield->fDensity         = 2.7; // g/cm3

   auto * matCu = new TargetMaterial("Cu", "NMR-Cu ", 29, 64);
   matCu->fLength          = 0.00050   ;    //cm
   matCu->fDensity         = 8.9600 ; // g/cm3

   auto * matNi = new TargetMaterial("Ni", "NMR-Ni  ", 28, 59);
   matCu->fLength          = 0.00022   ;    //cm
   matCu->fDensity         = 8.9020  ; // g/cm3

   this->AddMaterial(matH3);
   this->AddMaterial(matN14);
   this->AddMaterial(matLHe);
   this->AddMaterial(matHe);
   this->AddMaterial(matAlEndcap);
   this->AddMaterial(matAlTail);
   this->AddMaterial(matAl4kShield);
   this->AddMaterial(matCu);
   this->AddMaterial(matNi);

}
//_______________________________________________________________________________


UVACrossHairTarget::UVACrossHairTarget(const char * name, const char * title): Target(name, title)
{

   fPackingFraction = 0.6;
   DefineMaterials();
}
//_______________________________________________________________________________

UVACrossHairTarget::~UVACrossHairTarget()
{

}
//_______________________________________________________________________________

void UVACrossHairTarget::DefineMaterials()
{
   auto * matH3 = new TargetMaterial("H3", "H3 in Ammonia", 1, 1);
   matH3->fLength          = 3.0;    //cm
   matH3->fDensity         = 2.0 * 0.0770/3.0; // g/cm3
   matH3->fIsPolarized     = true;
   matH3->fPackingFraction = fPackingFraction;

   auto * matN14 = new TargetMaterial("14N", "14N in Ammonia", 7, 14);
   matN14->fLength          = 3.0;    //cm
   matN14->fDensity         = 2.0 * 0.3576; // g/cm3
   matN14->fPackingFraction = fPackingFraction;

   auto * matHe = new TargetMaterial("He", "LHe around Ammonia beads", 2, 4);
   matHe->fLength          = 3.0;    //cm
   matHe->fDensity         = 0.1450; // g/cm3
   matHe->fPackingFraction = 1.0 - fPackingFraction;

   auto * matLHe = new TargetMaterial("LHe", "Liquid Helium", 2, 4);
   matLHe->fLength          = 0.50000 * 2.0;  //cm
   matLHe->fDensity         = 0.1450; // g/cm3

   auto * matAlEndcap = new TargetMaterial("Al-endcap", "Aluminum target endcp", 13, 27);
   matAlEndcap->fLength          = 2.0 * 0.00381 ;  //cm
   matAlEndcap->fDensity         = 2.7; // g/cm3

   auto * matAlTail = new TargetMaterial("Al-tail", "Aluminum target tail", 13, 27);
   matAlTail->fLength          = 2.0 * 0.01016  ;  //cm
   matAlTail->fDensity         = 2.7; // g/cm3

   auto * matAl4kShield = new TargetMaterial("Al-4kshield", "Aluminum target 4K shield", 13, 27);
   matAl4kShield->fLength          = 2.0 * 0.00254  ;  //cm
   matAl4kShield->fDensity         = 2.7; // g/cm3

   auto * matCu = new TargetMaterial("Cu", "NMR-Cu ", 29, 64);
   matCu->fLength          = 0.00050   ;    //cm
   matCu->fDensity         = 8.9600 ; // g/cm3

   auto * matNi = new TargetMaterial("Ni", "NMR-Ni  ", 28, 59);
   matCu->fLength          = 0.00022   ;    //cm
   matCu->fDensity         = 8.9020  ; // g/cm3

   this->AddMaterial(matH3);
   this->AddMaterial(matN14);
   this->AddMaterial(matLHe);
   this->AddMaterial(matHe);
   this->AddMaterial(matAlEndcap);
   this->AddMaterial(matAlTail);
   this->AddMaterial(matAl4kShield);
   this->AddMaterial(matCu);
   this->AddMaterial(matNi);

}
//_______________________________________________________________________________



//_______________________________________________________________________________


UVAPureHeliumTarget::UVAPureHeliumTarget(const char * name, const char * title): Target(name, title)
{
   fPackingFraction = 0.0;
   DefineMaterials();
}
//_______________________________________________________________________________

UVAPureHeliumTarget::~UVAPureHeliumTarget()
{

}
//_______________________________________________________________________________

void UVAPureHeliumTarget::DefineMaterials()
{
   //TargetMaterial * matH3 = new TargetMaterial("H3", "H3 in Ammonia", 1, 1);
   //matH3->fLength          = 3.0;    //cm
   //matH3->fDensity         = 2.0 * 0.0770/*/3.0*/; // g/cm3
   //matH3->fIsPolarized     = true;
   //matH3->fPackingFraction = fPackingFraction;

   //TargetMaterial * matN14 = new TargetMaterial("14N", "14N in Ammonia", 7, 14);
   //matN14->fLength          = 3.0;    //cm
   //matN14->fDensity         = 2.0 * 0.3576; // g/cm3
   //matN14->fPackingFraction = fPackingFraction;

   auto * matHe  = new TargetMaterial("He", "LHe around Ammonia beads", 2, 4);
   matHe->fLength                = 3.0;      //cm
   matHe->fZposition             = 0.15;     //cm
   matHe->fDensity               = 0.1450;   // g/cm3
   matHe->fPackingFraction       = 1.0 - fPackingFraction;

   //TargetMaterial * matLHe = new TargetMaterial("LHe", "Liquid Helium", 2, 4);
   //matLHe->fLength          = 0.50000 * 2.0;  //cm
   //matLHe->fDensity         = 0.1450; // g/cm3

   //TargetMaterial * matAlEndcap = new TargetMaterial("Al-endcap", "Aluminum target endcp", 13, 27);
   //matAlEndcap->fLength          = 2.0 * 0.00381 ;  //cm
   //matAlEndcap->fDensity         = 2.7; // g/cm3

   //TargetMaterial * matAlTail = new TargetMaterial("Al-tail", "Aluminum target tail", 13, 27);
   //matAlTail->fLength          = 2.0 * 0.01016  ;  //cm
   //matAlTail->fDensity         = 2.7; // g/cm3

   //TargetMaterial * matAl4kShield = new TargetMaterial("Al-4kshield", "Aluminum target 4K shield", 13, 27);
   //matAl4kShield->fLength          = 2.0 * 0.00254  ;  //cm
   //matAl4kShield->fDensity         = 2.7; // g/cm3

   //TargetMaterial * matCu = new TargetMaterial("Cu", "NMR-Cu ", 29, 64);
   //matCu->fLength          = 0.00050   ;    //cm
   //matCu->fDensity         = 8.9600 ; // g/cm3

   //TargetMaterial * matNi = new TargetMaterial("Ni", "NMR-Ni  ", 28, 59);
   //matCu->fLength          = 0.00022   ;    //cm
   //matCu->fDensity         = 8.9020  ; // g/cm3

   //this->AddMaterial(matH3);
   //this->AddMaterial(matN14);
   this->AddMaterial(matHe);
   //this->AddMaterial(matLHe);
   //this->AddMaterial(matAlEndcap);
   //this->AddMaterial(matAlTail);
   //this->AddMaterial(matAl4kShield);
   //this->AddMaterial(matCu);
   //this->AddMaterial(matNi);

}
//_______________________________________________________________________________

}

