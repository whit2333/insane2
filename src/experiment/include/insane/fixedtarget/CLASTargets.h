#ifndef InSANE_CLASTargets_HH
#define InSANE_CLASTargets_HH

#include "insane/base/Target.h"

namespace insane {
   namespace physics {

      class Helium4GasTarget : public Target {
         public:
            Helium4GasTarget(const char * name  = "Helium4GasTarget",
                             const char * title = "Helium4 Gas Target" );
            virtual ~Helium4GasTarget();

            virtual void DefineMaterials();

            ClassDef(Helium4GasTarget,1)
      };

      class DeuteriumGasTarget : public Target {
         public:
            DeuteriumGasTarget(const char * name  = "DeuteriumGasTarget",
                             const char * title = "Deuterium Gas Target" );
            virtual ~DeuteriumGasTarget();

            virtual void DefineMaterials();

            ClassDef(DeuteriumGasTarget,1)
      };

      class HydrogenGasTarget : public Target {
         public:
            HydrogenGasTarget(const char * name  = "HydrogenGasTarget",
                             const char * title = "Hydrogen Gas Target" );
            virtual ~HydrogenGasTarget();

            virtual void DefineMaterials();

            ClassDef(HydrogenGasTarget,1)
      };

      class LH2Target : public Target {
         public:
            LH2Target(const char * name  = "LH2Target",
                             const char * title = "Liquid H2 Target" );
            virtual ~LH2Target();

            virtual void DefineMaterials();

            ClassDef(LH2Target,1)
      };


      // TDIS targets (not really CLAS)

      class TDISDeuteriumGasTarget : public Target {
         public:
            TDISDeuteriumGasTarget(const char * name  = "DeuteriumGasTarget",
                             const char * title = "Deuterium Gas Target" );
            virtual ~TDISDeuteriumGasTarget();

            virtual void DefineMaterials();

            ClassDef(TDISDeuteriumGasTarget,1)
      };

      class TDISHydrogenGasTarget : public Target {
         public:
            TDISHydrogenGasTarget(const char * name  = "TDISHydrogenGasTarget",
                             const char * title = "TDISHydrogen Gas Target" );
            virtual ~TDISHydrogenGasTarget();

            virtual void DefineMaterials();

            ClassDef(TDISHydrogenGasTarget,1)
      };


   }
}

#endif

