#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;


#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;

//#pragma link C++ class insane::SimpleTarget+;
//#pragma link C++ class insane::SimpleTargetWithWindows+;
#pragma link C++ class insane::UVAPolarizedAmmoniaTarget+;
#pragma link C++ class insane::UVACarbonTarget+;
#pragma link C++ class insane::UVACrossHairTarget+;
#pragma link C++ class insane::UVAPureHeliumTarget+;

#pragma link C++ class insane::physics::Helium4GasTarget+;
#pragma link C++ class insane::physics::TDISDeuteriumGasTarget+;
#pragma link C++ class insane::physics::DeuteriumGasTarget+;
#pragma link C++ class insane::physics::HydrogenGasTarget+;
#pragma link C++ class insane::physics::TDISHydrogenGasTarget+;
#pragma link C++ class insane::physics::LH2Target+;


#endif

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;


#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;

//#pragma link C++ class insane::physics::LH2Target+;


#endif

