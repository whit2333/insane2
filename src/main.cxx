/*! 
 
 \page InSANE2 Old main page

  \tableofcontents

  \section introduction Introduction

  `InSANE` is a collection C++ libraries for nuclear physics. Starting as
  a tool for analysis of the SANE experiment, `InSANE` has evolved since its
  first version and is currently being refactored for modern C++, hence \b InSANE2.
  (\e 2SANE or \e 2InSANE were considered for new names but starting with 
  a number is probably not a good idea.)

  \section quickstart Quick Start

  - `InSANE2` requires a \b C++17 compiler! But you might be OK with C++14 ;) 
  - [ROOT](https://root.cern.ch/) (compiled with `-Dcxx17:BOOL=ON -Droot7:BOOL=ON` among others)
  - [EIGEN](http://eigen.tuxfamily.org)
  - [CLHEP](https://gitlab.cern.ch/CLHEP/CLHEP)

```
git clone https://eicweb.phy.anl.gov/whit/insane2.git
mkdir build && cd build
cmake ../insane2/. -DCMAKE_INSTALL_PREFIX=$HOME
make -j4 install

# then make sure that paths are configured
export PATH=$HOME/bin:$PATH
export LD_LIBRARY_PATH=$HOME/lib:$LD_LIBRARY_PATH
```

  \section about About

  \subsection contributors Contributors

  InSANE was created and is maintained by Whitney Armstrong (whit@temple.edu).

  InSANE has recieved contributions from the following people:
  - Whitney Armstrong
  - David Flay 
  - Matthew Posik

  \subsection license License
  LGPL 

  <b>In it's current state </b> InSANE does not have complete references. 
  Hopefully that will be my in thesis. <b>Also</b> many exitisting codes, 
  mostly older standlone fortran codes, are used. <b> If you find your code </b> (which you hold copy(ing) rights), 
  and <b>you do not want it used here</b>
  please kindly let me know and it we be removed from the project. 
  But its mostly old fotran codes that have no copyright information. 
  </p>

*/

/*! \page linkingwithpython Using Python and PyROOT with InSANE

    \section pyROOTlink Linking InSANE Libraries with pyROOTlink

    First you need <a href="http://www.python.org/">python</a> 
    and <a href="http://root.cern.ch/drupal/content/pyroot">PyROOT</a>. 
    Also recommended is the python module 
    <a href="http://packages.python.org/rootplot/index.html"> rootplot </a>.
    
    The following example shows one way to use ROOT in python:
    \code
    from ROOT import gROOT,TCanvas
    c=TCanvas() 
    \endcode
    See <a href="http://wlav.web.cern.ch/wlav/pyroot/using.html"> PyROOT Manual</a> for 
    other ways and more details on using PyROOT. 

    \section pyinsanelink Using InSANE in python
    To use InSANE in python you import classes from the module ROOT (like the example above)
    but first you need to load the InSANE libraries and their dependencies into ROOT. 
    This similarly done in your \ref rootlogonC . 
    
    Create the file .rootlogon.py which is executed when you import ROOT. 
    The following example shows how to load and use InSANE from your .rootlogon.py script.
    
    \subsection rootlogonpy Example .rootlogon.py loading InSANE:
    \include simple_rootlogon.py
    
    \subsection example2py Example python script using InSANE:
    \include example2.py
    \image html bigcal_calibration.png "Plot of bigcal calibration coefficients produced by example2.py"
 */


/*!
   \page UnitsAndConstants Units and Constants

   \subpage SystemOfUnits     "System of Units"
   \subpage PhysicalConstants "Physical Constants"

 */

//______________________________________________________________________________



