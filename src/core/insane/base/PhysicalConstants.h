#ifndef InSANEPhysicalConstants_HH
#define InSANEPhysicalConstants_HH 1

#include "TObject.h"

//#include "CLHEP/Units/SystemOfUnits.h"
//#include "CLHEP/Units/PhysicalConstants.h"
#include "insane/base/SystemOfUnits.h"

namespace CLHEP {

static constexpr double Avogadro = 6.02214179e+23/mole;

// c   = 299.792458 mm/ns
// c^2 = 898.7404 (mm/ns)^2 
static constexpr double c_light   = 2.99792458e+8 * m/s;
static constexpr double c_squared = c_light * c_light;

// h     = 4.13566e-12 MeV*ns
// hbar  = 6.58212e-13 MeV*ns
// hbarc = 197.32705e-12 MeV*mm
static constexpr double h_Planck      = 6.62606896e-34 * joule*s;
static constexpr double hbar_Planck   = h_Planck/twopi;
static constexpr double hbarc         = hbar_Planck * c_light;
static constexpr double hbarc_squared = hbarc * hbarc;

static constexpr double electron_charge = - eplus; // see SystemOfUnits.h
static constexpr double e_squared = eplus * eplus;
// amu_c2 - atomic equivalent mass unit
//        - AKA, unified atomic mass unit (u)
// amu    - atomic mass unit
static constexpr double electron_mass_c2 = 0.510998910 * MeV;
static constexpr double   proton_mass_c2 = 938.272013 * MeV;
static constexpr double  neutron_mass_c2 = 939.56536 * MeV;
static constexpr double           amu_c2 = 931.494028 * MeV;
static constexpr double              amu = amu_c2/c_squared;

// permeability of free space mu0    = 2.01334e-16 Mev*(ns*eplus)^2/mm
// permittivity of free space epsil0 = 5.52636e+10 eplus^2/(MeV*mm)
static constexpr double mu0      = 4*pi*1.e-7 * henry/m;
static constexpr double epsilon0 = 1./(c_squared*mu0);

// electromagnetic coupling = 1.43996e-12 MeV*mm/(eplus^2)
static constexpr double elm_coupling           = e_squared/(4*pi*epsilon0);
static constexpr double fine_structure_const   = elm_coupling/hbarc;
static constexpr double classic_electr_radius  = elm_coupling/electron_mass_c2;
static constexpr double electron_Compton_length = hbarc/electron_mass_c2;
static constexpr double Bohr_radius = electron_Compton_length/fine_structure_const;

static constexpr double alpha_rcl2 = fine_structure_const
                                   *classic_electr_radius
                                   *classic_electr_radius;

static constexpr double twopi_mc2_rcl2 = twopi*electron_mass_c2
                                             *classic_electr_radius
                                             *classic_electr_radius;
static constexpr double k_Boltzmann = 8.617343e-11 * MeV/kelvin;

static constexpr double STP_Temperature = 273.15*kelvin;
static constexpr double STP_Pressure    = 1.*atmosphere;
static constexpr double kGasThreshold   = 10.*mg/cm3;

static constexpr double universe_mean_density = 1.e-25*g/cm3;

}  // namespace CLHEP

//using CLHEP::pi;
//using CLHEP::twopi;
//using CLHEP::halfpi;
//using CLHEP::pi2;
//using CLHEP::Avogadro;
//using CLHEP::c_light;
//using CLHEP::c_squared;
//using CLHEP::h_Planck;
//using CLHEP::hbar_Planck;
//using CLHEP::hbarc;
//using CLHEP::hbarc_squared;
//using CLHEP::electron_charge;
//using CLHEP::e_squared;
//using CLHEP::electron_mass_c2;
//using CLHEP::proton_mass_c2;
//using CLHEP::neutron_mass_c2;
//using CLHEP::amu_c2;
//using CLHEP::amu;

//
// permeability of free space mu0    = 2.01334e-16 Mev*(ns*eplus)^2/mm
// permittivity of free space epsil0 = 5.52636e+10 eplus^2/(MeV*mm)
//
//static const double mu0      = 4*pi*1.e-7 * henry/m;
//static const double epsilon0 = 1./(c_squared*mu0);
//using CLHEP::mu0;
//using CLHEP::epsilon0;
//
////
//// electromagnetic coupling = 1.43996e-12 MeV*mm/(eplus^2)
////
////
//
//
//
//using CLHEP::elm_coupling;
//using CLHEP::fine_structure_const;
//using CLHEP::classic_electr_radius;
//using CLHEP::electron_Compton_length;
//using CLHEP::Bohr_radius;
//using CLHEP::alpha_rcl2;
//using CLHEP::twopi_mc2_rcl2;
//using CLHEP::k_Boltzmann;
//using CLHEP::STP_Temperature;
//using CLHEP::STP_Pressure;
//using CLHEP::kGasThreshold;
//using CLHEP::universe_mean_density;

namespace insane {

  /**  Units and constants.
   *
   */
  namespace units {

    using CLHEP::GeV;
    using CLHEP::amu;
    using CLHEP::amu_c2;

    using CLHEP::pi;
    using CLHEP::pi2;
    using CLHEP::twopi;

    static constexpr double M_test = 0.9382720013 * GeV;

  } // namespace units

  namespace constants {

    using CLHEP::Avogadro;
    using CLHEP::c_light;
    using CLHEP::c_squared;
    using CLHEP::e_squared;
    using CLHEP::electron_charge;
    using CLHEP::electron_mass_c2;
    using CLHEP::h_Planck;
    using CLHEP::halfpi;
    using CLHEP::hbar_Planck;
    using CLHEP::hbarc;
    using CLHEP::hbarc_squared;
    using CLHEP::neutron_mass_c2;
    using CLHEP::proton_mass_c2;
    using CLHEP::pi;
    using CLHEP::pi2;
    using CLHEP::twopi;
    // static const double mu0      = 4*pi*1.e-7 * henry/m;
    // static const double epsilon0 = 1./(c_squared*mu0);
    using CLHEP::epsilon0;
    using CLHEP::mu0;
    // electromagnetic coupling = 1.43996e-12 MeV*mm/(eplus^2)
    using CLHEP::alpha_rcl2;
    using CLHEP::Bohr_radius;
    using CLHEP::classic_electr_radius;
    using CLHEP::electron_Compton_length;
    using CLHEP::elm_coupling;
    using CLHEP::fine_structure_const;
    using CLHEP::k_Boltzmann;
    using CLHEP::kGasThreshold;
    using CLHEP::STP_Pressure;
    using CLHEP::STP_Temperature;
    using CLHEP::twopi_mc2_rcl2;
    using CLHEP::universe_mean_density;


    static constexpr double alpha  = CLHEP::fine_structure_const;
    static constexpr double alpha2 = CLHEP::fine_structure_const * CLHEP::fine_structure_const;

  } // namespace constants

  /** Particle masses.
   *  See \ref PhysicalConstants for more details.
   */
  namespace masses {

    using insane::units::GeV;

    /** @name Hadron masses. 
    */
    //@{
    /// PDG: 139.57018 ± 3.5E-4 MeV
    static constexpr double M_pion  = 0.13957018 * GeV;
    static constexpr double M_pi0   = 0.1349766 * GeV;
    static constexpr double M_p     = 0.9382720013 * GeV;
    static constexpr double M_n     = 0.939565378 * GeV;
    static constexpr double M_p_GeV = 0.9382720013;
    static constexpr double M_n_GeV = 0.939565378;
    static constexpr double M_d     = 1.875612859 * GeV;
    static constexpr double M_2H    = M_d;
    static constexpr double M_3H    = 2.80943 * GeV;
    static constexpr double M_3He   = 2.80941 * GeV;
    static constexpr double M_4He   = 3.7284 * GeV;
    //@}

    /** @name Nucleon resonances.
     */
    //@{
    static constexpr double M_Delta = 1.232 * GeV;
    static constexpr double M_P33   = 1.232 * GeV;
    static constexpr double M_S11   = 1.535 * GeV;
    static constexpr double M_D13   = 1.520 * GeV;
    static constexpr double M_F15   = 1.680 * GeV;
    static constexpr double M_S15   = 1.650 * GeV;
    static constexpr double M_P11   = 1.440 * GeV;
    //@}

    /** @name lepton masses
     */
    //@{
    static constexpr double M_e    = 0.000511 * GeV;

    /// PDG: 0.1134289256 ± 0.0000000029
    static constexpr double M_muon = 0.1134289256 * GeV;

    /// PDG: 1776.82 ± 0.16
    static constexpr double M_tau  = 1.77682 * GeV;
    //@}

    /** @name quark masses:
     * \todo Add the quark masses
     */
    //@{
    static constexpr double M_up      = 0 * GeV;
    static constexpr double M_down    = 0 * GeV;
    static constexpr double M_strange = 0 * GeV;
    static constexpr double M_top     = 0 * GeV;
    static constexpr double M_bottom  = 0 * GeV;
    static constexpr double M_beauty  = 0 * GeV;
    static constexpr double M_charm   = 0 * GeV;
    //@}
  } // namespace masses

  /** Electric and Magnetic moments and other properties.
   */
  namespace properties {

    using insane::units::GeV;
    using insane::units::eV;
    using insane::units::tesla;
    // static constexpr double bohr_magneton = eplus*hbar
    
    static constexpr double bohr_magneton    = 5.7883818066e-5 * eV / tesla; 
    static constexpr double nuclear_magneton = 3.1524512605e-8 * eV / tesla;


    /** @name magnetic moments.
     *
     */
    //@{
    static constexpr double Mu_B = bohr_magneton;
    static constexpr double Mu_N = nuclear_magneton;

    /// PDG: 2.792847356 ± 0.000000023 Mu_N
    static constexpr double Mu_p   = 2.792847356;    
    /// PDG: -1.91304272 ± 0.00000045 Mu_N
    static constexpr double Mu_n   = -1.91304272;    
    /// CODATA: 0.857 438 2308(72)   Mu_N
    static constexpr double Mu_d   = 0.8574382308;   
    /// source??
    static constexpr double Mu_He3 = -2.12762523466; 
    /// triton CODATA: 2.978 962 448(38) Mu_N
    static constexpr double Mu_t   = 2.978962448;    
    //@}
    
    /** @name Anomalous magnetic moments.
     *
     */
    //@{
    static constexpr double Kappa_p   = Mu_p - 1.0; /// Anomalous magnetic moment of the proton
    static constexpr double Kappa_n   = Mu_n;       /// Anomalous magnetic moment of the neutron
    static constexpr double Kappa_He3 = -4.12762;     /// Anomalous magnetic moment of 3He
    //@}

    static constexpr double g_A = 1.2673;       /// nucleon axial charge / Axial-vector coupling constexprant
    static constexpr double M_A = 1.015 * GeV;  /// GeV/c^2 Axial nucleon mass (a dipole FF fit mass)
    static constexpr double M_V = 0.8426 * GeV; /// =sqrt(0.71) GeV/c^2 Vector nucleon mass (a dipole FF fit mass)

    /// quark charges
    static constexpr double Q_up      = 2. / 3.;
    static constexpr double Q_down    = 1. / 3.;
    static constexpr double Q_strange = 1. / 3.;

  }

  namespace units {
    using namespace masses;
    using namespace properties;


  } // namespace units

} // namespace insane

/** Extention of CLHEP constants.
 * \deprecated use insane::units.
 */
// namespace CLHEP {
//
//  static const double M_test      =  0.9382720013 *GeV;
//
//  /// lepton masses
//  static const double M_e       =  0.000511*GeV;
//  static const double M_muon    =  0.1134289256*GeV;                /// PDG: 0.1134289256 ±
//  0.0000000029 static const double M_tau     =  1.77682*GeV;                     /// PDG: 1776.82
//  ± 0.16
//  /// quark masses:
//  /// \todo Add the quark masses
//  static const double M_up      =  0*GeV;
//  static const double M_down    =  0*GeV;
//   static const double M_strange =  0*GeV;
//   static const double M_top     =  0*GeV;
//   static const double M_bottom  =  0*GeV;
//   static const double M_beauty  =  0*GeV;
//   static const double M_charm   =  0*GeV;
//   /// quark charges
//   static const double Q_up      = 2./3.;
//   static const double Q_down    = 1./3.;
//   static const double Q_strange = 1./3.;
//   /// meson masses
//   static const double M_pion    =  0.13957018   *GeV;               /// PDG: 139.57018 ± 3.5E-4
//   MeV static const double M_pi0     =  0.1349766    *GeV;
//   /// nucleon masses
//   static const double M_p       =  0.9382720013 *GeV;
//   static const double M_n       =  0.939565378  *GeV;
//   static const double M_p_GeV   =  0.9382720013     ;
//   static const double M_n_GeV   =  0.939565378      ;
//   /// nucleus masses
//   static const double M_d       =  1.875612859  *GeV;
//   static const double M_3He     =  3.0160293    *amu_c2;            /// wikipedia
//   /// nucleon resonances
//   static const double M_Delta   =  1.232*GeV;
//   static const double M_P33     =  1.232*GeV;
//   static const double M_S11     =  1.535*GeV;
//   static const double M_D13     =  1.520*GeV;
//   static const double M_F15     =  1.680*GeV;
//   static const double M_S15     =  1.650*GeV;
//   static const double M_P11     =  1.440*GeV;
//
//   //static const double bohr_magneton = eplus*hbar
//   static const double bohr_magneton    =  5.7883818066e-5*eV/tesla; ///  5.7883818066(38)×10−5
//   eV·T−1 static const double nuclear_magneton =  3.1524512605e-8*eV/tesla; ///  3.152 451
//   2605(22) x 10-8 eV T-1
//
//   static const double Mu_B = bohr_magneton ;
//   static const double Mu_N = nuclear_magneton;
//
//   /// magnetic moments
//   static const double Mu_p   =  2.792847356;                        /// PDG: 2.792847356 ±
//   0.000000023 Mu_N static const double Mu_n   = -1.91304272;                         /// PDG:
//   -1.91304272 ± 0.00000045 Mu_N static const double Mu_d   =  0.8574382308; /// CODATA: 0.857 438
//   2308(72)   Mu_N static const double Mu_He3 = -2.12762523466;                      /// source??
//   static const double Mu_t   =  2.978962448 ;                       /// triton magnetic moment
//   CODATA: 2.978 962 448(38)    Mu_N
//
//   static const double Kappa_p   = Mu_p - 1.0;                       /// Anomalous magnetic moment
//   of the proton static const double Kappa_n   = Mu_n;                             /// Anomalous
//   magnetic moment of the neutron static const double Kappa_He3 = -4.200; /// Anomalous magnetic
//   moment of 3He
//
//   static const double hbarc2_gev_b  = 3.89379*1e-04;                /// (h*c)^2 in GeV^2*b
//   static const double hbarc2_gev_mb = 3.89379*1e-01;                /// (h*c)^2 in GeV^2*mb
//   static const double hbarc2_gev_ub = 3.89379*1e+02;                /// (h*c)^2 in GeV^2*ub
//   static const double hbarc2_gev_nb = 3.89379*1e+05;                /// (h*c)^2 in GeV^2*nb
//   static const double hbarc2_mev_pb = 3.89379*1e+05;                /// (h*c)^2 in MeV^2*pb
//   static const double hbarc_gev_fm  = 0.197326968;                  /// h*c in GeV fm
//
//   static const double g_A = 1.2673;    // nucleon axial charge / Axial-vector coupling constant
//   static const double M_A = 1.015;     // GeV/c^2 Axial nucleon mass (a dipole FF fit mass)
//   static const double M_V = 0.8426;     // =sqrt(0.71)  GeV/c^2 Vector nucleon mass (a dipole FF
//   fit mass)
//
//}


#endif

