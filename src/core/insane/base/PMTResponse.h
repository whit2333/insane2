#ifndef PMTResponse_HH
#define PMTResponse_HH
#include "TMath.h"

#include "TF1.h"
#include "TObject.h"


namespace insane {
/**
 * NIMA 451 (2000) 623}637
 */
class  PMTResponse {
   protected:
      Int_t  fNmax;
      Int_t  fNIntPoints;
      double fXIntMin;
      double fXIntMax;

      TF1 * fFit_low_light;
      TF1 * fFit;
      Double_t fA,fN0,fP0,fP1,fpe,fxp,fsigp,fx0,fsig0,fx1,fsig1,fmu;
      Double_t fA2;
      Double_t fgN;

   public:
      PMTResponse();
      virtual ~PMTResponse();
      Int_t GetMaxNPE() const {return(fNmax);}
      void  SetMaxNPE(Int_t n){ fNmax = n;}
      Int_t GetNIntPoints() const {return(fNIntPoints);}
      void  SetNIntPoints(Int_t n){ fNIntPoints = n;}

      /** Sets all the internal parameters.*/
      virtual void SetParameters(double *p);
      void PrintParameters();

      /** PMT response fit function with 12 parameters.  */
      virtual double PMT_fit(double *x, double *p) const ;

      double SER0(double *x, double *p) const ;
      double SER0_exp(double *x, double *p) const ;
      double SER0_peak(double *x, double *p) const ;
      double SER(double *x, double *p) const ;
      double SER_exp(double *x, double *p) const ;
      double SER_peak(double *x, double *p) const ;
      double M_pmt(double *x, double *p) const ;
      double M_pmt1(double *x, double *p) const ;

      double f_N_0(double *x, double *p,int n) const ;
      double f_N(double *x, double *p,int n) const ;
      double f_N_wrap(double *x, double *p) const { return(f_N(x,p,int(p[6])) ); }
      double conv_fit(double *x, double *p) const {return(0.0);}

      /** Light light version of PMT_fit with 10 parameters.
       */
      double PMT_fit_low_light(double *x, double *p) const ;

      TF1 * GetPeakFunction(Int_t n = 1) const ;
      TF1 * GetDoublePeakFunction(Int_t n = 1) const ;
      TF1 * GetExpFunction() const ;

ClassDef(PMTResponse,1)
};

/** Fit iwth 9 parameters for small mu only. Approximates sig1 and x1 for small mu only. */ 
class  PMTResponse2 : public PMTResponse  {
public :
      /** PMT response fit function with 9 parameters. For small mu only.  */
      double PMT_fit(double *x, double *p) const ;
      void   SetParameters(double *p);

ClassDef(PMTResponse2,1)
};

/** Fit with 9 parameters for all mu.  Approximates sig1 and x1 as sig0 and x1. */ 
class  PMTResponse3 : public PMTResponse  {
public :
      /** PMT response fit function with 9 parameters  for n > 1  */
      double PMT_fit(double *x, double *p) const ;
      void   SetParameters(double *p);

ClassDef(PMTResponse3,1)
};

/** Fit with 11 parameters for all mu.  sig1 and x1  are now free parameters. */ 
class  PMTResponse32 : public PMTResponse  {
public :
      /** PMT response fit function with 9 parameters  for n > 1  */
      double PMT_fit(double *x, double *p) const ;
      void   SetParameters(double *p);

ClassDef(PMTResponse32,1)
};

/** Fit with 10 parameters for all mu.  
 *  sig1 and x1  are now free parameters. 
 *  P0 and P0 are both calculated from mu using Poisson dist.
 */ 
class  PMTResponse33 : public PMTResponse  {
public :
      double PMT_fit(double *x, double *p) const ;
      void   SetParameters(double *p);

ClassDef(PMTResponse33,1)
};

/** Fit includes a second peak for the "double track"
 */
class  PMTResponse4 : public PMTResponse  {
public :
      double PMT_fit(double *x, double *p) const ;
      void   SetParameters(double *p);

ClassDef(PMTResponse4,1)
};

/** Fit includes a second peak for the "double track"
 */
class  PMTResponse42 : public PMTResponse  {
   //public:
   //   double fSmear;
   public:
      double PMT_fit(double *x, double *p) const ;
      void   SetParameters(double *p);

      ClassDef(PMTResponse42,1)
};


/** Fit includes a second peak for the "double track" and an exponential background.
 * 
 */
class  PMTResponse5 : public PMTResponse  {
public :
      double PMT_fit(double *x, double *p) const ;
      void   SetParameters(double *p);
      TF1 * GetBackgroundExpFunction() const;
      Double_t fA0;   /// Amplitude multiplying exponential background 
ClassDef(PMTResponse5,1)
};


}
#endif
