#ifndef InSANEMaterial_H
#define InSANEMaterial_H 1

#include <cstdlib> 
#include <iostream>
#include <iomanip> 
#include <cmath> 
#include <string> 
#include "insane/base/SystemOfUnits.h"
#include "insane/base/PhysicalConstants.h"

/** @defgroup materials Materials
 *  Material and its properties.
 */

namespace insane {

  /** Material object.
   * \ingroup materials
   */
  class Material {

  private:
    std::string fName;     /// Name of material
    double      fZ;        /// Z   = Atomic number
    double      fA;        /// A   = Atomic mass number
    double      fL;        /// L   = length (cm)
    double      fT;        /// T   = thickness (g/cm^2),
    double      fX0;       /// X0  = Radiation length (g/cm^2),
    double      fTR;       /// TR  = Number of radiation lengths (= T/X0)
    double      fRho;      /// Rho = density (g/cm^3)

  public: 
    Material(std::string Name="",double Z=0,double A=0,double L=0,
             double T=0,double Rho=0,double TR=0,double X0=0);

    virtual ~Material();

    void Clear();
    void Print();
    void Process();                     /// Compute T, TR, etc.  
    void CalculateRadiationLength();    /// for mixtures  

    void SetZ(double z){fZ = z;}
    void SetA(double a){fA = a;}
    void SetL(double l){fL = l;}
    void SetX0(double x0){fX0 = x0;}
    void SetT(double t){fT = t;} 
    void SetTR(double tr){fTR = tr;} 
    void SetRho(double rho){fRho = rho;} 
    void SetName(const std::string& name){fName = name;} 

    std::string GetName(){return fName;}

    Double_t GetZ(){return fZ;}
    Double_t GetA(){return fA;}
    Double_t GetL(){return fL;}
    Double_t GetT(){return fT;}
    Double_t GetTR(){return fTR;}
    Double_t GetX0(){return fX0;}
    Double_t GetRho(){return fRho;}

    ClassDef(Material,2) 

  }; 

}
#endif 
