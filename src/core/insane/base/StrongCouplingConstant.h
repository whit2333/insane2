#ifndef StrongCouplingConstant_HH
#define StrongCouplingConstant_HH 1

#include "TMath.h"
#include "TObject.h"

namespace insane::physics {

  /** Strong coupling constant in the MSbar scheme.
   */
  class StrongCouplingConstant {

  protected:
    double n_f;
    double fLambda;

  public:
    StrongCouplingConstant();
    virtual ~StrongCouplingConstant();
    double operator()(double mu2);

    double b0(double mu2) const;
    double b1(double mu2) const;
    double b2(double mu2) const;
    double b3(double mu2) const;

    ClassDef(StrongCouplingConstant, 1)
  };
} // namespace insane::physics

#endif
