#ifndef insane_physics_HH
#define insane_physics_HH

#include "insane/base/Kinematics.h"

#include <functional>

namespace insane {

  /** \addtogroup physics  Physics
   *
   * Physics topics:
   *
   * - @ref elasticScattering "Elastic Scattering"
   * - @ref inelasticScattering "Inelastic Scattering"
   * - @ref targetMassCorrections "Target Mass Corrections"
   * - @ref enumsAndHelpers "Parton enumerations and other helper functions"
   *
   */
  namespace physics {


    /** 
     * @anchor elasticScattering 
     * \name Elastic Scattering
     *  Elastic scattering
     *  \ingroup physics
     *  @{
     */

    /** Dirac Form factor from Sachs form factors.
     * 
     * \f[  F_1 = \frac{G_E + \tau G_M}{1+\tau} \f]
     * where
     * \f$ \tau = \frac{Q^2}{4M^2} \f$.
     *  \ingroup physics
     */
    double F1(double Q2, double GE, double GM);

    /** Pauli Form factor from Sachs form factors.
     * 
     * \f[  F_2 = \frac{G_M - G_E}{1+\tau} \f]
     * where
     * \f$ \tau = \frac{Q^2}{4M^2} \f$.
     *  \ingroup physics
     */
    double F2(double Q2, double GE, double GM);

    /** The total squared form factor.
     * 
     * \f[ \Epsilon^{\prime}(Q^2, \theta_e) = \Big(1+2(1+\tau)\tan^2(\theta_e/2)\Big)^{-1} \f]
     * \f[ \frac{1}{(1+\tau)\Epsilon^{\prime}}\Big(\Epsilon^{\prime}(G_{E})^{2} + \tau (G_M)^2 \Big) \f]
     * where
     * \f$ \tau = \frac{Q^2}{4M^2} \f$.
     * \param sachs_FFs Sachs form factors \f$G_E\f$ and \f$G_M\f$.
     * \param Q2 \f$-q^2\f$
     * \param tan2th  \f$\tan^2(\theta_e/2)\f$ where \f$\theta_e\f$ is the electron scattering angle in collinear lab frame.
     *
     *  \ingroup physics
     */
    double F_total_squared(std::array<double,2> sachs_FFs,  const kinematics::ElasticKinematics& kine);

    /** Elastic cross section for a generic collinear frame.
     *
     * From http://inspirehep.net/record/895827 .
     *
     * \param FFs form factors.
     * \param Q2 \f$Q^2 \geq 0\f$
     * \param E_e electron beam energy.
     * \param E_p proton beam energy.
     * \param theta_e electron scattering angle in lab (not necessarily nucleon rest frame)
     *
     *  \ingroup physics
     */
    double Elastic_XS(std::array<double, 2> FFs, const kinematics::ElasticKinematics& kine);

    //@}

    /** 
     * @anchor inelasticScattering 
     * \name Inelastic Scattering
     *  Inelastic scattering
     *  \ingroup physics
     *  @{
     */

    /** R \f$ = \sigma_L/\sigma_T\f$ (ratio of L/T cross sections).
     *
     * Double_t res = F2p(x, Q2) / (2.0 * x * F1p(x, Q2)) ;
     * res = res * (1 + 4.0 * M_p/GeV * M_p/GeV * x * x / Q2) - 1.0;
     *
     * \ingroup DIS
     * \ingroup physics
     */
    double R(double F1, double F2, double x, double Q2);

    /** Soffer-Teryaev  Positivity bound.
     *
     *  \f$ |A_2| < \sqrt{R(1+A_1)/2} \f$
     *
     *  \ingroup spinStructure
     *  \ingroup positivity
     *  \ingroup DIS
     * @{
     */
    double A2_PositivityBound(double F_1, double F_2, double A_1, double x, double Q2);
    double A2_PositivityBound(double R, double A_1, double x, double Q2);
    //@}

    /** Wandzura-Wilczek function form.
     *
     *  \f$ \displaystyle g_2 = -g_1(x) + \int g_1(y)\frac{dy}{y} \f$
     *
     *  \ingroup physics
     *  \ingroup DIS
     */
    double WW(std::function<double(double)> func, double x, double max=1.0);

    /** @}*/


    /** \addtogroup TMCs Target Mass Corrections
     *
     *  \ingroup physics
     *
     */
    namespace TMCs {

      /**
       * @anchor targetMassCorrections
       * \name Target Mass Corrections
       *
       *  References.
       *
       *   - A Review of Target Mass Corrections, Schienbein et.al., J. Phys. G 35, 053101 (2008) https://inspirehep.net/literature/760572
       *   - Target mass corrections and twist-3 in the nucleon spin structure functions,  https://inspirehep.net/literature/754773
       *
       *   \todo Add WW breaking
       *   \todo Add Blumlein refetrence
       *
       * \ingroup physics
       * \ingroup TMCs
       *
       *  @{
       */

      /** TMC correction function.
       * Target Mass correction function
       * J. Phys. G 35, 053101 (2008)
       *  \ingroup TMCs
       */
      double h2(std::function<double(double)> F1, double xi, double Q2);

      /** TMC correction function.
       * Target Mass correction function
       * J. Phys. G 35, 053101 (2008)
       *  \ingroup TMCs
       */
      double g2(std::function<double(double)> F1, double xi, double Q2);
      //@}

    } // namespace TMCs

    /** Cornwall-Norton Moments.
     *
     * \f$ M_n = \int x^n f(x) dx \f$
     *
     * Note this is the same as a n+1 Mellin moment.
     *
     * \ingroup physics
     */
    double CN_moment(std::function<double(double)> f, int n, double x_low=0.0, double x_high=1.0);


    /**
     * @anchor enumsAndHelpers
     * \name Parton enums and other helper functions
     *
     * \ingroup physics
     * @{
     */

    const size_t NPartons     = 13; ///< Number of parts (size of enum)
    const size_t NLightQuarks = 6;  ///< Number of light quarks (u,d,s,ubar,dbar,sbar)

    //// deprecated
    //enum PartonFlavor {
    //  kUP          = 0,
    //  kDOWN        = 1,
    //  kSTRANGE     = 2,
    //  kCHARM       = 3,
    //  kBOTTOM      = 4,
    //  kTOP         = 5,
    //  kGLUON       = 6,
    //  kANTIUP      = 7,
    //  kANTIDOWN    = 8,
    //  kANTISTRANGE = 9,
    //  kANTICHARM   = 10,
    //  kANTIBOTTOM  = 11,
    //  kANTITOP     = 12,
    //  kUPBAR       = kANTIUP,
    //  kDOWNBAR     = kANTIDOWN,
    //  kSTRANGEBAR  = kANTISTRANGE,
    //  kCHARMBAR    = kANTICHARM,
    //  kBOTTOMBAR   = kANTIBOTTOM,
    //  kTOPBAR      = kANTITOP
    //};

    //template <typename E>
    //constexpr typename std::underlying_type<E>::type to_underlying(E e) noexcept {
    //  return static_cast<typename std::underlying_type<E>::type>(e);
    //}

    /** Parton flavor enum.
     *
     * Follows other parton numbering conventions.
     *
     * \ingroup physics
     */
    enum class Parton : unsigned int {
      u    = 0, ///< up quark
      d    = 1, ///< down quark
      s    = 2, ///< strange quark
      c    = 3, ///< charm quark
      b    = 4, ///< bottom quark
      t    = 5, ///< top quark
      g    = 6, ///< gluon
      ubar = 7, ///< anti-up quark
      dbar = 8, ///< anti-down quark
      sbar = 9, ///< anti-s quark
      cbar = 10, ///< anti-c quark
      bbar = 11, ///< anti-b quark
      tbar = 12, ///< anti-t quark
      up   = u,
      down = d,
      strange = s,
    };

    /** Return the parton enum id (number). 
     */
    template <typename E = Parton>
    constexpr typename std::underlying_type<E>::type q_id(E e) noexcept {
      return static_cast<typename std::underlying_type<E>::type>(e);
    }

    constexpr std::array<Parton,NLightQuarks> LightQuarks = {
      Parton::u    , Parton::d    , Parton::s     ,
      Parton::ubar, Parton::dbar, Parton::sbar
    };
    /** Parton squared charges.
     */
    constexpr std::array<double,NPartons> PartonCharge2 = {
      4.0/9.0, 1.0/9.0, 1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0,
      0.0,
      4.0/9.0, 1.0/9.0, 1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0
    };

    /** Parton charges.
     */
    constexpr std::array<double,NPartons> PartonCharge = {
      2.0/3.0, -1.0/3.0, -1.0/3.0, 2.0/3.0, -1.0/3.0, 2.0/3.0,
      0.0,
      -2.0/3.0, 1.0/3.0, 1.0/3.0, -2.0/3.0, 1.0/3.0, -2.0/3.0
    };

    constexpr std::array<double,NPartons> IsoSpinConjugatePartonCharge2 = { 
      1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0,
      0.0,
      1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0
    };
    constexpr std::array<double,NPartons> IsoSpinConjugatePartonCharge = { 
      -1.0/3.0, 2.0/3.0, -1.0/3.0, 2.0/3.0, -1.0/3.0, 2.0/3.0,
      0.0,
      1.0/3.0, -2.0/3.0, 1.0/3.0, -2.0/3.0, 1.0/3.0, -2.0/3.0
    };
    //@}

    enum class SFType {
      Unpolarized, Polarzed, Spin, FormFactor, Compton,
    };

    const size_t NStructureFunctions = 15;
    enum class SF {
      F1, F2, FL, R, W1, W2, g1, g2, gT, g2_WW, g1_BT,
    };

    const size_t NSpinStructureFunctions = 15;
    enum class SSF {
      g1,
      g2,
      gT,
      g2_WW,
      g1_BT,
    };

    const size_t NComptonAsymmetries = 16;
    enum class ComptonAsymmetry {
      A1, A2, AT, AL,
    };

    const size_t NFormFactors = 10;
    enum class FormFactor {
      GE, GM, F1, F2, GC, GQ, A, B,
    };

    enum class FragmentationType {
      D1, H1,
    };

    enum class Chirality {
      Even, Odd,
    };

    const size_t NNuclei = 7;
    enum class Nuclei {
      p,
      n,
      d,
      t,
      _3He,
      _4He,
      H     = p,
      D     = d,
      _2H   = d,
      _3H   = t,
      alpha = _4He,
      Other,
    };

    enum class TargetSpin {
      Zero,
      OneHalf,
      One,
      ThreeHalves,
      Two,
      FiveHalves,
      ThreeHalf = ThreeHalves,
      FiveHalf  = ThreeHalf,
    };

    enum class OPELimit {
      Bjorken,
      MasslessTarget,
      MasslessQuark,
      MassiveTarget,
      MassiveQuark,
      MassiveTargetAndQuark,
      TMC                    = MassiveTarget,
      Massless               = Bjorken,
      Massive                = MassiveTargetAndQuark,
    };
    enum class Twist {
      Two,
      Three,
      Four,
      TwoAndThree,
      TwoThreeAndFour,
      Leading             = Two,
      NextToLeading       = TwoAndThree,
      NextToNextToLeading = TwoThreeAndFour,
      All                 = TwoThreeAndFour,
    };

    enum class GPDType {
      H,
      Htilde,
      E,
      Etilde,
      H_T,
      Htilde_T,
      E_T,
      Etilde_T,
    };

    enum class Hadron {
      pi_plus,
      pi_minus,
      K_plus,
      K_minus,
      Other,
    };

  } // namespace physics

} // namespace insane

#endif

