#ifndef insane_materialproperties_HH
#define insane_materialproperties_HH

namespace insane {


  /** Material related properties.
   * \ingroup materials
   *
   */
  namespace materials {

   /** \addtogroup matDensities Material Densities
    *  \ingroup materials
    *
    * Some common material densities.
    * @{
    */

   static const double LH2_density  = 0.07085;  ///< LH2 density in g/cm3
   static const double LD2_density  = 0.1711;   ///< g/cm3
   static const double L4He_density = 0.1450;   ///< g/cm3
   static const double LN2_density  = 0.8116;   ///< LN2
   static const double Si_density   = 2.329002; ///<
   static const double Al_density   = 2.7;      ///<
   static const double Cu_density   = 8.96;     ///<
   static const double Ni_density   = 8.908;    ///<
   static const double SNH3_density = 0.817;    ///< Solid NH3 density (wikipedia)
   // static const double  SNH3_density = 0.696; //(Wolfram Alpha)
   static const double STP_3He_density = 0.000125; ///< 3He gas density g/cm3
   static const double Si3N4_density   = 3.17;     ///< Si3N4 density  g/cm3 (wikipdeia)
   static const double NbN_density     = 8.417;    ///< NbN density g/cm3 (wikipdeia)

   //@}
  }

  /** Returns the radition length in g/cm^2. 
   *  \ingroup materials
   *
   */
  double radiation_length(int Z, int A);

  /** Approximate formulat for Moliere radius.
   *  \ingroup materials
   */
  inline double R_Moliere(int Z, int A) {
    return 0.0265*radiation_length(Z,A)*(Z + 1.2);
  }


  //** Returns the radition length in g/cm^2. */
  //double X0(int Z, int A) { return radiation_length(Z, A); }

}

#endif
