#ifndef insane_nuclei_HH
#define insane_nuclei_HH

namespace insane::material {

  /** Compute the PDG code for atomic nuclei.
   *
   * \ingroup materials
   */
  int NuclearPDGCode(int A, int Z, int L = 0) {
    int code = 0;
    if ((Z == 1) && (A == 1) && (L == 0)) {
      code = 2212;
    } else if ((Z == 0) && (A == 1) && (L == 0)) {
      code = 2112;
    } else {
      //     10LZZZAAAI
      code = 1000000000 + 10000000 * L + 10000 * Z + 10 * A;
    }
    return code;
  }

} // namespace insane::material
#endif
