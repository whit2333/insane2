#ifndef insane_physics_Sigma_hh
#define insane_physics_Sigma_hh

#include "insane/kinematics/Core.h"
#include "insane/base/NFoldDifferential.h"

namespace insane {
  namespace physics {

    namespace helper = insane::helpers;

    namespace tags {

      /// Default cross section tag.
      using sigma_xs = kine::Var<struct sigma_xs_tag>; 

    }

    namespace xsec {

      using namespace fluent;

      namespace impl {

        template <typename Diff, typename T = tags::sigma_xs,
                  typename = typename std::enable_if<is_Differential<Diff>::value>::type>
        struct SigmaBaseImpl {};

      } // namespace impl


      /** Sigma cross section.
       * 
       */
      template <typename Diff, typename Vars, typename T = tags::sigma_xs,
                typename = typename std::enable_if<is_Differential<Diff>::value>::type>
      struct Sigma {
        using DiffType = Diff;
        using VarTypes = typename Diff::VarTypes;
        using AllVars  = Vars;
        using InputVars_t = typename Vars::IndependentVariables_t;

        AllVars xsfunc;

        Sigma(const Vars& v) : xsfunc(v) {}

        constexpr auto operator()(const InputVars_t& v) const { return std::get<T>(xsfunc(v)); }

      };

      template <typename Diff, typename Vars, typename T = tags::sigma_xs>
      auto make_Sigma(Vars vars){
        return Sigma<Diff,Vars,T>(vars);
      }

    }
  }
}
#endif

