#ifndef InSANE_MATH_HH
#define InSANE_MATH_HH

#include "Math/GaussIntegrator.h"
#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/Functor.h"
#include "Math/GaussIntegrator.h"
#include "Math/RichardsonDerivator.h"
#include "Math/Derivator.h"
#include "Math/Vector2D.h"
#include "Math/Vector3D.h"
#include <functional>

namespace insane {

  /** \brief Integration helpers.
   * \ingroup integration Integration methods. 
   *
   */
  namespace integrate   {

    /** Guassian integrator. 
     */
    double gaus(std::function<double(double)> func, double min, double max);

    /** Legendre integrator
     */
    double legendre(std::function<double(double)> func, double min, double max);

    /** Adaptive integrator
     */
    double adaptive(std::function<double(double)> func, double min, double max);
    
    /** Simple integrator.
     */
    double simple(std::function<double(double)> func, double min, double max, int N = 100);

    /** Simple multithreaded integrator.
     */
    double simple_threads(std::function<double(double)> func, double min, double max, int N = 100, int nthreads = 4);

  }

  /** \brief Math helpers.
   * \ingroup math Math helpers. 
   *
   */
  namespace math   {

    /** Quick derivative value.
     */
    double simple_quad_sum(std::function<double(double)> func, double min, double max, int N = 100);

    /** Quick derivative value.
     */
    double derivative(std::function<double(double)> func, double x);

    /** Returns angle value in the range [-pi,pi].
     */
    double StandardPhi(double phi);

  }
}
#endif

