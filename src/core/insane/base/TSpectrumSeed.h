#ifndef TSpectrumSeed_HH
#define TSpectrumSeed_HH 1

#include "ClusteringSeed.h"
namespace insane {
namespace physics {

/** Concrete implementation of ClusteringSeed
 *
 *  Use TSpectrum to find seeds
 *
 *  \ingroup Clustering
 */
class TSpectrumSeed : public ClusteringSeed {
public:
   TSpectrumSeed() {  };

   ~TSpectrumSeed() { };

///Concrete classes should imp clear
   void Clear() {
      for (int k = 0; k < fNSeeds; k++) {
         fiSeed[k] = 0;
         fjSeed[k] = 0;
         fxSeed[k] = -99999.0;
         fySeed[k] = -99999.0;
      }
      fNSeeds = 0;
   }

   ClassDef(TSpectrumSeed, 1)
};
}}


#endif

