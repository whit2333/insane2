#ifndef insane_base_Helpers_H
#define insane_base_Helpers_H

#include "Math/Vector4D.h"
#include "Math/Vector4Dfwd.h"
#include "Math/VectorUtil.h"
#include "Math/Polar3D.h"

#include <type_traits>
#include <experimental/type_traits>
#include <utility>
#include <tuple>
#include <cstddef>
#include <functional>
#include <stdexcept>
#include <array>
#include <iostream>


namespace insane {

  namespace physics {
    class InitialState;
  }

  /** Template helpers.
   *
   */
  namespace helpers {

    using InitialState = insane::physics::InitialState;

    // ---------------------------------------------------------
    // template debugging
    // to print a warning do something like:
    // debug_type<second_arg_type<decltype(f2)>>()
    //
    template <typename T>
    inline void debug_type(const T&) __attribute__((deprecated));

    template <typename T>
    inline void debug_type(const T&) {}

    template <typename T>
    inline void debug_type() __attribute__((deprecated));

    template <typename T>
    inline void debug_type() {}
    //__________________________________________________________

    template <bool... Bs>
    constexpr bool require = std::conjunction<std::bool_constant<Bs>...>::value;

    template <bool... Bs>
    constexpr bool either = std::disjunction<std::bool_constant<Bs>...>::value;

    template <bool... Bs>
    constexpr bool disallow = not require<Bs...>;

    using std::experimental::is_detected;
    using std::experimental::is_detected_convertible;
    using std::experimental::is_detected_exact;

    template <template <class...> class Op, class... Args>
    constexpr bool exists = is_detected<Op, Args...>::value;

    template <class To, template <class...> class Op, class... Args>
    constexpr bool converts_to = is_detected_convertible<To, Op, Args...>::value;

    template <class Exact, template <class...> class Op, class... Args>
    constexpr bool identical_to = is_detected_exact<Exact, Op, Args...>::value;


    /** Filter variadic template pack by predicate.
     *
     * @{
     * https://stackoverflow.com/questions/33543287/how-to-filter-a-variadic-template-pack-by-type-derivation
     *
     */
    template <template <typename> class Pred, typename TUPLE, typename Res = std::tuple<> >
    struct Filter;

    template <template <typename> class Pred, typename Res> 
    struct Filter<Pred, std::tuple<>, Res>
    {
      using type = Res;
    };

    template <template <typename> class Pred, typename T, typename ... Ts, typename ... TRes> 
    struct Filter<Pred, std::tuple<T, Ts...>, std::tuple<TRes...>> : 
    Filter<Pred, 
    std::tuple<Ts...>, 
    std::conditional_t<Pred<T>::value, std::tuple<TRes..., T>, std::tuple<TRes...>>>
    { };

    //@}


    /** @name is_tuple
     *  Determine if type is a std::tuple.
     *
     * @{
     */
    template<typename T>
    struct is_tuple_impl : std::false_type {};

    template<typename... Ts>
    struct is_tuple_impl<std::tuple<Ts...>> : std::true_type {};

    template<typename T>
    struct is_tuple : is_tuple_impl<std::decay_t<T>> {};
    //@}

    /** @name has_type
     *  Determine if tuple has type.
     * https://stackoverflow.com/questions/25958259/how-do-i-find-out-if-a-tuple-contains-a-type
     */
    //@{
    template <typename T, typename Tuple>
    struct has_type : std::false_type {};

    template <typename T>
    struct has_type<T, std::tuple<>> : std::false_type {};

    template <typename T, typename... Us>
    struct has_type<T, std::tuple<Us...>> : std::disjunction<std::is_same<T, Us>...> {};

    template<class... B>
    inline constexpr bool has_type_v = has_type<B...>::value;

    /** Test if tuple has all types in second tuple.
     *
     * \tparam T1 tuple check for presence of types
     * \tparam T2 types to check for
     * @{
     */
    template<typename T1, typename T2>
    struct contains_tuple_types : std::false_type {};

    template<typename T1>
    struct contains_tuple_types<T1, std::tuple<>> : std::true_type {};

    template <typename T1, typename... T>
    struct contains_tuple_types<T1, std::tuple<T...>> : std::conjunction<has_type<T1, T>...> {};

    template <class... B>
    inline constexpr bool contains_tuple_types_v = contains_tuple_types<B...>::value;
    //template <typename T1, typename T2, std::size_t... Is>
    //constexpr bool contains_tuple_types(const T1& , const T2&, std::index_sequence<Is...>)  {
    //  return (has_type<std::tuple_element_t<Is, T2>, T1>::value && ...);
    //}
    //template <typename T1, typename T2>
    //constexpr bool contains_tuple_types(const T1& t1, const T2& t2)  {
    //  return contains_tuple_types(t1,t2, std::make_index_sequence<std::tuple_size_v<T2>>());
    //}
    //@}

    /** \name print tuple
     * @{
     */
    template <typename T, std::size_t... I, typename F>
    void tuple_foreach_impl(T&& tuple, std::index_sequence<I...>, F&& func) {
      // In C++17 we would use a fold expression here, but in C++14 we have to resort to this.
      using dummy_array = int[];
      dummy_array{(void(func(std::get<I>(tuple))), 0)..., 0};
    }

    template <typename T, typename F>
    void tuple_foreach(T&& tuple, F&& func) {
      constexpr int size = std::tuple_size<std::remove_reference_t<T>>::value;
      tuple_foreach_impl(std::forward<T>(tuple), std::make_index_sequence<size>{},
                         std::forward<F>(func));
    }
    template <typename T>
    void print_tuple(const T& tuple) {
      std::cout << '{';
      tuple_foreach(tuple, [first = true](auto& value) mutable {
        if (!first)
          std::cout << "; ";
        else
          first = 0;
        std::cout << value;
      });
      std::cout << '}';
    }

    //@}


    /** Intesection
     * https://stackoverflow.com/questions/41200299/common-types-in-two-typesets-tuples
     *
     */
    template <typename S1, typename S2>
    struct intersect {
      template <std::size_t... Indices>
      static constexpr auto make_intersection(std::index_sequence<Indices...>) {
        return std::tuple_cat(
            std::conditional_t<has_type<std::tuple_element_t<Indices, S1>, S2>::value,
                               std::tuple<std::tuple_element_t<Indices, S1>>, std::tuple<>>{}...);
      }
      template <std::size_t... Indices>
      static constexpr auto make_intersection2(std::index_sequence<Indices...>) {
        return std::tuple_cat(
            std::conditional_t<has_type<std::tuple_element_t<Indices, S2>, S1>::value,
                               std::tuple<std::tuple_element_t<Indices, S2>>, std::tuple<>>{}...);
      }
      using type =
          decltype(make_intersection(std::make_index_sequence<std::tuple_size<S1>::value>{}));
      using type2 =
          decltype(make_intersection2(std::make_index_sequence<std::tuple_size<S2>::value>{}));
    };


    //template <typename T1, typename T2>
    //constexpr bool assign_tuple_type(T1& t1, const T2& t2)  {
    //  if constexpr ( has_type_v<T1,T2> ){
    //    std::get<T2>(t1) = t2;
    //    return true;
    //  } else{
    //    return false;
    //  }
    //}
    //
    template <class Tuple, class F>
    constexpr decltype(auto) for_each(Tuple&& tuple, F&& f) {
      return []<std::size_t... I>(Tuple && tuple, F && f, std::index_sequence<I...>) {
        (f(std::get<I>(tuple)), ...);
        return f;
      }
      (std::forward<Tuple>(tuple), std::forward<F>(f),
       std::make_index_sequence<std::tuple_size<std::remove_reference_t<Tuple>>::value>{});
    }
    // assign t1 = t2 when they have a common type
    template <typename T1, typename T2, std::size_t... Is>
    constexpr void assign_tuple_by_types(T1& t1 , const T2& t2, std::index_sequence<Is...>)  {
      //return ( ... && assign_tuple_type(t1,std::get<Is>(t2)));
      ((std::get<std::tuple_element_t<Is,T2>>(t1) = std::get<Is>(t2)),...);
    }
    template <typename T1, typename T2>
    constexpr void assign_tuple_by_types(T1& t1, const T2& t2)  {
      assign_tuple_by_types(t1,t2, std::make_index_sequence<std::tuple_size_v<T2>>());
    }


    /** \name equivalent_types
     * https://stackoverflow.com/questions/40117987/how-do-i-compare-tuples-for-equivalent-types-disregarding-type-order
     * @{
     */
    template <typename T, typename Tuple>
    struct type_counter;

    template <typename T, typename... Ts>
    struct type_counter<T, std::tuple<Ts...>>
        : std::integral_constant<std::size_t, (... + std::is_same<T, Ts>::value)> {};

    template <typename Tuple1, typename Tuple2, std::size_t... Is>
    constexpr bool equivalent_types(const Tuple1&, const Tuple2&, std::index_sequence<Is...>) {
      return (... && (type_counter<std::tuple_element_t<Is, Tuple1>, Tuple1>::value ==
                      type_counter<std::tuple_element_t<Is, Tuple1>, Tuple2>::value));
    }

    template <typename Tuple1, typename Tuple2>
    constexpr bool equivalent_types(const Tuple1& t1, const Tuple2& t2) {
      constexpr auto s1 = std::tuple_size<Tuple1>::value;
      constexpr auto s2 = std::tuple_size<Tuple2>::value;
      return s1 == s2 && equivalent_types(t1, t2, std::make_index_sequence<std::min(s1, s2)>());
    }

    //@}

    //template <typename LHS, typename RHS, std::size_t... Is>
    //constexpr bool equivalent_types(LHS&, const RHS&, std::index_sequence<Is...>) {
    //  return (... && (type_counter<std::tuple_element_t<Is, LHS>, LHS>::value ==
    //                  type_counter<std::tuple_element_t<Is, LHS>, RHS>::value));
    //}

    //template <typename LHS, typename RHS>
    //constexpr bool equivalent_types(LHS& t1, const RHS& t2) {
    //  constexpr auto s1 = std::tuple_size<LHS>::value;
    //  constexpr auto s2 = std::tuple_size<RHS>::value;
    //  return s1 == s2 && equivalent_types(t1, t2, std::make_index_sequence<std::min(s1, s2)>());


    // std::is_array< std::array > == false !
    // WTF

    /** @name is_std_array
     *  Determine if type is a std::tuple.
     */
    //@{
    template<class T>
    struct is_array : std::is_array<T> {};

    template<class T, std::size_t N>
    struct is_array< std::array<T, N>> : std::true_type {
      using array_t = T; 
      static constexpr std::size_t size = N;
    };

    template<class T>
    struct is_std_array_impl : std::false_type {};

    template<class T, std::size_t N>
    struct is_std_array_impl<std::array<T, N>> : std::true_type {
      using array_t = T;
      static constexpr std::size_t size = N;
    };

    template<class T>
    struct is_std_array : is_std_array_impl<T> {
      using  type                  = typename is_std_array_impl<T>::array_t;
      static constexpr size_t size = is_std_array_impl<T>::N;
    };

    template <class T>
    using is_std_array_t = typename is_std_array<std::decay_t<T>>::type;
    //@}

    // The following code provides std::apply until we have c++17 support
    namespace notstd {

      template<std::size_t...Is>
      auto index_over(std::index_sequence<Is...>){
        return [](auto&&f)->decltype(auto){
          return decltype(f)(f)( std::integral_constant<std::size_t, Is>{}... );
        };
      }
      template<std::size_t N>
      auto index_upto(std::integral_constant<std::size_t, N> ={}){
        return index_over( std::make_index_sequence<N>{} );
      }
      template<class F, class Tuple>
      decltype(auto) apply( F&& f, Tuple&& tup ) {
        auto indexer = index_upto< std::tuple_size<std::remove_reference_t<Tuple>>::value >();
        return indexer(
          [&](auto...Is)->decltype(auto) {
            return std::forward<F>(f)( std::get<Is>(std::forward<Tuple>(tup))... );
          });
      }
    } // namespace notstd

    namespace detail {

      template <class Tuple, size_t... Is>
      constexpr auto take_front_impl(Tuple t,
                                     std::index_sequence<Is...>) {
        return std::make_tuple(std::get<Is>(t)...);
      }
      template <size_t N, class Tuple>
      constexpr auto take_front(Tuple t) {
        return take_front_impl(t, std::make_index_sequence<N>{});
      }
      template <class F, std::size_t... Is>
      constexpr auto index_apply_impl(F f,
                                      std::index_sequence<Is...>) {
        return f(std::integral_constant<std::size_t, Is> {}...);
      }

      template <size_t N, class F>
      constexpr auto index_apply(F f) {
        return index_apply_impl(f, std::make_index_sequence<N>{});
      }
      template <class Tuple>
      constexpr auto reverse(Tuple t) {
        return index_apply<std::tuple_size<Tuple>{}>(
          [&](auto... Is) {
            return std::make_tuple(
              std::get<std::tuple_size<Tuple>{} - 1 - Is>(t)...);
          });
      }

      template<typename F, typename Tuple, size_t ...S >
      decltype(auto) apply_tuple_impl(F&& fn, Tuple&& t, std::index_sequence<S...>) {
        return std::forward<F>(fn)(std::get<S>(std::forward<Tuple>(t))...);
      }

      template<typename F, typename Tuple>
      decltype(auto) apply_from_tuple(F&& fn, Tuple&& t) {
        std::size_t constexpr tSize = std::tuple_size<typename std::remove_reference<Tuple>::type>::value;
        return apply_tuple_impl(std::forward<F>(fn),
                                std::forward<Tuple>(t),
                                std::make_index_sequence<tSize>());
      }

      template<typename E>
      using is_class_enum = std::integral_constant<bool, std::is_enum<E>::value && !std::is_convertible<E, int>::value>;

      template<bool...> struct bool_pack;
      template<bool... bs> 
      using all_true = std::is_same<bool_pack<bs..., true>, bool_pack<true, bs...>>;

      template<class R, class... Ts>
      using are_all_convertible = all_true<std::is_convertible<Ts, R>::value...>;

      template<class R, class... Ts>
      using are_all_enum = all_true<std::is_enum<R>::value && !std::is_convertible<Ts, R>::value...>;

    } // namespace detail

    template<typename T> struct is_std_function : public std::false_type {
      using Func_t = T;
    };

    template<typename T>
    struct is_std_function<std::function<T>> : public std::true_type {
      using Func_t = T;
    };

    template <typename T>
    using is_std_function_t = typename is_std_function<T>::Func_t;


    /** @name FunctionTraits
     *  Helpers for function_traits.
     */
    //@{

    /** Function Traits.
     * 
     * https://stackoverflow.com/questions/7943525/is-it-possible-to-figure-out-the-parameter-type-and-return-type-of-a-lambda
     */
    template <typename T>
    struct function_traits : public function_traits<decltype(&T::operator())>
    {};
    // For generic types, directly use the result of the signature of its 'operator()'
    template <typename ClassType, typename ReturnType, typename... Args>
    struct function_traits<ReturnType(ClassType::*)(Args...) const>
    // we specialize for pointers to member function
    {
      enum { arity = sizeof...(Args) }; // arity is the number of arguments.
      using result_type = ReturnType;
      template <size_t i>
      struct arg {
        using type = typename std::decay<typename std::tuple_element<i, std::tuple<Args...>>::type>::type;
        // the i-th argument is equivalent to the i-th tuple element of a tuple
        // composed of those arguments.
      };
    };

    template<class Func, size_t i>
    using arg_type = typename function_traits<Func>::template arg<i>::type;

    template<class Func, size_t i,
            typename = typename std::enable_if<is_array<arg_type<Func,i>>::value>::type>
    struct arg_array_size_type : std::tuple_size<arg_type<Func,i>>::type {};

    template<class Func, size_t i>
    constexpr size_t arg_array_size = arg_array_size_type<Func,i>::value;

    //@}

    // -------------------------------------------------

    //@{
    /** Return type helpers.
     * Determine the return types.
     */
    template<class Func> using 
    returns_a_four_vector = std::is_same< ROOT::Math::XYZTVector, typename function_traits<Func>::result_type >;

    template<class Func> using 
    returns_a_double = std::is_same< double, typename function_traits<Func>::result_type >;

    template<class Func> using 
    returns_a_std_array = is_std_array< typename function_traits<Func>::result_type >;

    template<class Func> 
    using result_t = typename function_traits<Func>::result_type;

    template<class Func, typename = typename std::enable_if<returns_a_std_array<Func>::value>::type>
    struct result_array_size_impl  {
      static constexpr size_t N = std::tuple_size<result_t<Func>>::value;
    };
    template<class Func>
    constexpr size_t result_array_size = result_array_size_impl<Func>::N;
    template<class Func>
    constexpr size_t result_array_size_v = result_array_size_impl<Func>::N;


    //@}

    template<class Func> using 
    is_first_arg_inital_state = std::is_same<InitialState, typename function_traits<Func>::template arg<0>::type >;

    template<class Func> using 
    second_arg_type = typename std::decay_t< typename function_traits<Func>::template arg<1>::type>;

    template<class Func> using
    second_arg_array_size_type = typename std::conditional< is_array<second_arg_type<Func>>::value,
              typename std::tuple_size< second_arg_type<Func> >::type,
              typename std::integral_constant<size_t, 0>::type >::type;

    template<class Func>
    constexpr size_t second_arg_array_size = second_arg_array_size_type<Func>::value;
    
    //template<class Func>
    //constexpr size_t second_arg_array_size = std::tuple_size< second_arg_type<Func>>::value;


    template<class Func > using 
    array_from_lambda = typename std::array<double, arg_array_size<Func,1> >;

    template<class Func > using 
    is_second_arg_array = std::is_same< second_arg_type<Func>, array_from_lambda<Func> >;

    //template<class Func> using
    //has_array_second_arg  =  std::is_same< second_arg_type<Func>, array_from_lambda<Func> >;
    //std::cout << __PRETTY_FUNCTION__ << "\n";
    
    //template<class Func>
    //constexpr bool has_array_second_arg2(){ return std::is_same< second_arg_type<Func>, array_from_lambda<Func> >::value;}

    template<class Func>
    constexpr bool will_return_a_four_vector(){ return returns_a_four_vector<Func>::value;}


    /** Index dispatcher for looping over tuple.
     *  See https://blog.tartanllama.xyz/exploding-tuples-fold-expressions/
     */
    template <std::size_t... Idx>
    constexpr auto make_index_dispatcher(std::index_sequence<Idx...>) {
      return [] (auto&& f) {
        return std::array<double,sizeof...(Idx)>{f(std::integral_constant<std::size_t,Idx>{})...}; };
    }
    template <std::size_t N>
    constexpr auto make_index_dispatcher() {
      return make_index_dispatcher(std::make_index_sequence<N>{}); 
    }

    template <typename T, std::size_t... Idx>
    constexpr auto make_index_dispatcher(std::index_sequence<Idx...>) {
      return [] (auto&& f) {
        return std::array<T,sizeof...(Idx)>{f(std::integral_constant<std::size_t,Idx>{})...}; };
    }
    template <typename T, std::size_t N>
    constexpr auto make_index_dispatcher() {
      return make_index_dispatcher<T>(std::make_index_sequence<N>{}); 
    }

    //template <typename... Args, typename Func>
    //void for_each(const std::tuple<Args...>& t, const Func& f) {
    //  auto dispatcher = make_index_dispatcher<sizeof...(Args)>();
    //  dispatcher([&](auto idx) { f(std::get<idx>(t)); });
    //}



    /** 
     * \name Enum helpers
     *
     * https://stackoverflow.com/questions/207976/how-to-easily-map-c-enums-to-strings
     *
     * \code
     * std::map<MyEnum, const char*> MyMap;
     * map_init(MyMap)
     *    (eValue1, "A")
     *    (eValue2, "B")
     *    (eValue3, "C");
     * \endcode
     *
     * @{
     */

    template<typename T> struct map_init_helper
    {
      T& data;
      map_init_helper(T& d) : data(d) {}
      map_init_helper& operator() (typename T::key_type const& key, typename T::mapped_type const& value)
      {
        data[key] = value;
        return *this;
      }
    };

    template<typename T> map_init_helper<T> map_init(T& item)
    {
      return map_init_helper<T>(item);
    }
    /** @} */


    template<typename T,typename = typename std::enable_if<is_array<std::decay_t<T>>::value>::type>
    auto to_vector_impl(T&& arr, std::true_type){
      //debug_type<T>();
      std::cout << " rvalue \n";
      return std::vector<is_std_array_t<T>>(std::make_move_iterator(arr.begin()),
                                 std::make_move_iterator(arr.end())); // move
    }
    template<typename T,typename = typename std::enable_if<is_array<std::decay_t<T>>::value>::type>
    auto to_vector_impl(T&& arr, std::false_type){
      return std::vector<is_std_array_t<T>>(arr.begin(), arr.end());
    }

    template<typename T>
    auto to_vector(T&& arr){
      return to_vector_impl(arr, std::is_rvalue_reference<T>());
    }

    /** Convert array from types as a tuple of types.
     */
    template <class... Formats, size_t N, size_t... Is>
    std::tuple<Formats...> as_tuple(std::array<double, N> const& arr, std::index_sequence<Is...>) {
      return std::make_tuple(Formats{arr[Is]}...);
    }

    template <class... Formats, size_t N, class = std::enable_if_t<(N == sizeof...(Formats))>>
    std::tuple<Formats...> as_tuple(std::array<double, N> const& arr) {
      return as_tuple<Formats...>(arr, std::make_index_sequence<N>{});
    }

    /** Convert array to tuple.
     */
    template <typename T, size_t N, size_t... Is>
    constexpr T to_tuple(std::array<double, N> const& arr, std::index_sequence<Is...>) {
      return T(std::get<Is>(arr)...);
    }
    template <typename T, size_t N, class = std::enable_if_t<(N == std::tuple_size<T>::value)>>
    constexpr T to_tuple(std::array<double, N> const& arr) {
      return to_tuple<T>(arr, std::make_index_sequence<N>{});
    }

    template <typename T>
    constexpr T to_tuple(std::vector<double> const& arr) {
      return to_tuple<T>(*(reinterpret_cast<const std::array<double,std::tuple_size<T>::value>*>(arr.data())));
    }
    //template <template <class...> class Cont, typename...Ts>
    //Cont<Ts...> as_tuple(std::array<double, sizeof...(Ts)> const& arr) {
    //  return as_tuple<Ts...>(arr);
    //}

  } // namespace helpers
} // namespace insane

#endif
