Core library   {#core1}
============

\tableofcontents

## Core overview

The Core module contains the sub header paths:
- [base](#base)
- [kinematics](#kinematics)
- [xsec](#xsec)


\defgroup kinematics Kinematics
@{
@}

\defgroup physics Physics
@{
@}

\defgroup xsections Cross-Sections
@{
\brief Scattering cross-sections for various processes.
@}


