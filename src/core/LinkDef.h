#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;

#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;
#pragma link C++ namespace insane::units;
#pragma link C++ namespace insane::materials;
#pragma link C++ namespace insane::kinematics;
#pragma link C++ namespace insane::Kine;
#pragma link C++ namespace insane::Phys;
#pragma link C++ namespace insane::Math;
#pragma link C++ namespace insane::helpers;
#pragma link C++ namespace insane::integrate;
#pragma link C++ namespace insane::math;

#pragma link C++ class std::vector<double>+;
#pragma link C++ class std::function<double(double)>+;

#pragma link C++ function insane::math::StandardPhi(double); 
//#pragma link C++ function insane::integrate::gaus(std::function<double(double)>, double, double);
//#pragma link C++ function insane::integrate::WW(std::function<double(double)>, double, double);

//-----------------------------------------------------------------------

#pragma link C++ class insane::Nucleus+; 


//#pragma link C++ global fgProton+;
//#pragma link C++ global fgNeutron+;
//#pragma link C++ global fgDeuteron+;
//#pragma link C++ global fg3He+;
//#pragma link C++ global fgTriton+;
// #pragma link C++ global fgIron+;
// #pragma link C++ global fgOther+;

//#pragma link C++ class std::map<Int_t,TFile*>+;
//#pragma link C++ class std::pair<Int_t,TFile*>+;

#pragma link C++ class std::vector<TString*>+;
#pragma link C++ class std::map<int, double >+;
#pragma link C++ class std::vector<TGraph*>+;

#pragma link C++ class std::map<Int_t,TFile*>+;
#pragma link C++ class std::pair<Int_t,TFile*>+;

#pragma link C++ class insane::Particle+;
#pragma link C++ class std::vector<insane::Particle*>+;
#pragma link C++ class insane::Material+; 

#pragma link C++ class insane::physics::StrongCouplingConstant+;

#pragma link C++ function insane::Kine::SetMomFromEThetaPhi(TParticle*, double *x);

#pragma link C++ function insane::Math::TestFunction22(double);
#pragma link C++ function insane::Math::CGLN_F1(double,double,double);
#pragma link C++ function insane::Math::CGLN_F2(double,double,double);
#pragma link C++ function insane::Math::CGLN_F3(double,double,double);
#pragma link C++ function insane::Math::CGLN_F4(double,double,double);
#pragma link C++ function insane::Math::CGLN_F5(double,double,double);
#pragma link C++ function insane::Math::CGLN_F6(double,double,double);
#pragma link C++ function insane::Math::CGLN_F7(double,double,double);
#pragma link C++ function insane::Math::CGLN_F8(double,double,double);

#pragma link C++ function insane::Math::multipole_E(double,double);
#pragma link C++ function insane::Math::multipole_M(double,double);


// insane/base/KineFuncs.h
#pragma link C++ function insane::Kine::v0(double,double,double);
#pragma link C++ function insane::Kine::vL(double,double);
#pragma link C++ function insane::Kine::vT(double,double,double);
#pragma link C++ function insane::Kine::vTT(double,double);
#pragma link C++ function insane::Kine::vTL(double,double,double);
#pragma link C++ function insane::Kine::vTprime(double,double,double);
#pragma link C++ function insane::Kine::vTLprime(double,double,double);

#pragma link C++ function insane::Kine::tau(double,double);
#pragma link C++ function insane::Kine::q_abs(double,double);

#pragma link C++ function insane::Kine::Sig_Mott(double,double);
#pragma link C++ function insane::Kine::fRecoil(double,double,double);

#pragma link C++ function insane::Kine::Q2(double,double,double);
#pragma link C++ function insane::Kine::Q2_xW(double,double,double);
#pragma link C++ function insane::Kine::Q2_EEprimeTheta(double,double,double);

#pragma link C++ function insane::Kine::xBjorken(double,double,double);
#pragma link C++ function insane::Kine::xBjorken_EEprimeTheta(double,double,double);
#pragma link C++ function insane::Kine::xi_Nachtmann(double,double,double);

#pragma link C++ function insane::Kine::nu(double,double);
#pragma link C++ function insane::Kine::nu_WsqQsq(double,double,double);

#pragma link C++ function insane::Kine::W2(double,double,double);

#pragma link C++ function insane::Kine::epsilon(double,double,double);
#pragma link C++ function insane::Kine::epsilon_xQ2(double,double,double);

#pragma link C++ function insane::Kine::W_xQsq(double,double,double);
#pragma link C++ function insane::Kine::W_xQ23(double,double,double);
#pragma link C++ function insane::Kine::W_EEprimeTheta(double,double,double);

#pragma link C++ function insane::Kine::xBjorken_WQsq(double,double,double);
#pragma link C++ function insane::Kine::xBjorken_WQ2(double,double,double);

#pragma link C++ function insane::Kine::BeamEnergy_xQ2y(double,double,double,double);
#pragma link C++ function insane::Kine::BeamEnergy_epsilonQ2W2(double,double,double,double);

#pragma link C++ function insane::Kine::Eprime_xQ2y(double,double,double,double);
#pragma link C++ function insane::Kine::Eprime_W2theta(double,double,double,double);
#pragma link C++ function insane::Kine::Eprime_epsilonQ2W2(double,double,double,double);

#pragma link C++ function insane::Kine::Theta_xQ2y(double,double,double,double);
#pragma link C++ function insane::Kine::Theta_epsilonQ2W2(double,double,double,double);

#pragma link C++ function insane::Kine::K_Hand(double,double);
#pragma link C++ function insane::Kine::Gamma_v(double,double,double);

#pragma link C++ function insane::Kine::D(double,double,double,double);
#pragma link C++ function insane::Kine::d(double,double,double,double);
#pragma link C++ function insane::Kine::Eta(double,double,double);
#pragma link C++ function insane::Kine::Xi(double,double,double);
#pragma link C++ function insane::Kine::Chi(double,double,double,double);

    
#pragma link C++ class insane::physics::PhaseSpace+;
#pragma link C++ class insane::physics::PhaseSpaceVariable+;
#pragma link C++ class insane::physics::DiscretePhaseSpaceVariable+;
#pragma link C++ class std::vector<insane::physics::PhaseSpaceVariable*>+;

#pragma link C++ class insane::physics::DiffXSec+;
//#pragma link C++ class insane::physics::InitalState+;
//#pragma link C++ class insane::physics::FinalState+;
//#pragma link C++ class insane::physics::DiffXSecKinematicKey+;
#pragma link C++ class insane::physics::InclusiveDiffXSec+;
#pragma link C++ class insane::physics::ExclusiveDiffXSec+;
//#pragma link C++ class insane::physics::InclusiveMottXSec+;
#pragma link C++ class insane::physics::ExclusiveMottXSec+;
#pragma link C++ class insane::physics::FlatInclusiveDiffXSec+;
#pragma link C++ class insane::physics::FlatExclusiveDiffXSec+;

#pragma link C++ class insane::kinematics::ElasticKinematics+;
#pragma link C++ class insane::kinematics::SIDISKinematics+;



#pragma link C++ class insane::physics::InitalState+;

#pragma link C++ class insane::physics::PSVBase+;
//#pragma link C++ class insane::physics::PhaseSpace+;
//#pragma link C++ class insane::physics::PhaseSpaceVariable+;
//#pragma link C++ class insane::physics::DiscretePhaseSpaceVariable+;
//#pragma link C++ class std::vector<insane::physics::PhaseSpaceVariable*>+;
//#pragma link C++ class insane::physics::PhaseSpaceSampler+;

#pragma link C++ class insane::physics::FinalState+;
//#pragma link C++ class insane::physics::DiffXSecKinematicKey+;
//#pragma link C++ class insane::physics::InclusiveDiffXSec+;
//#pragma link C++ class insane::physics::ExclusiveDiffXSec+;
//#pragma link C++ class insane::physics::InclusiveMottXSec+;
//#pragma link C++ class insane::physics::ExclusiveMottXSec+;
//#pragma link C++ class insane::physics::FlatInclusiveDiffXSec+;
//#pragma link C++ class insane::physics::FlatExclusiveDiffXSec+;





#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;

//#pragma link C++ class insane::xsec::FinalStateParticle+;
//#pragma link C++ class std::vector<insane::xsec::FinalStateParticle*>+;
//#pragma link C++ class insane::xsec::GeneratorBase+;
//#pragma link C++ class insane::xsec::SamplerBase+;

//#pragma link C++ class insane::physics::_EG_Event+;
//#pragma link C++ class insane::physics::EventGenerator+;

#endif

