#include "RadiativeCorrections.h"
#include "Radiator.h"

namespace insane {
namespace physics {

//______________________________________________________________________________
RadiativeCorrections1D::RadiativeCorrections1D(const char * n, const char * t):TNamed(n,t){

   fBeamEnergy  = 0;
   fThetaTarget = 0;

   fSigmaBorn       = nullptr;
   fSigmaBorn_Plus  = nullptr;
   fSigmaBorn_Minus = nullptr;
   fDeltaBorn       = nullptr;
   fAsymBorn       = nullptr;

   fSigmaERT        = nullptr;
   fSigmaERT_Plus   = nullptr;
   fSigmaERT_Minus  = nullptr;
   fDeltaERT        = nullptr;
   fAsymERT         = nullptr;

   fSigmaIRT        = nullptr;
   fSigmaIRT_Plus   = nullptr;
   fSigmaIRT_Minus  = nullptr;
   fDeltaIRT        = nullptr;
   fAsymIRT         = nullptr;

   fAvgKine         = nullptr;

   fPolarizedPhaseSpace = nullptr;
   fPhaseSpace          = nullptr;

   fBorn0 = nullptr;
   fBorn1 = nullptr;
   fBorn2 = nullptr;
   fERT0_rt = nullptr;
   fERT1_rt = nullptr;
   fERT2_rt = nullptr;
   fInelasticXSec0_rt = nullptr;
   fInelasticXSec1_rt = nullptr;
   fInelasticXSec2_rt = nullptr;
   fDiffXSec03 = nullptr;
   fDiffXSec13 = nullptr;
   fDiffXSec23 = nullptr;
}
//______________________________________________________________________________
RadiativeCorrections1D::~RadiativeCorrections1D(){

}
//______________________________________________________________________________
void RadiativeCorrections1D::Browse(TBrowser* b) {
   if(fSigmaBorn       )b->Add(fSigmaBorn      );
   if(fSigmaBorn_Plus  )b->Add(fSigmaBorn_Plus );
   if(fSigmaBorn_Minus )b->Add(fSigmaBorn_Minus);
   if(fDeltaBorn       )b->Add(fDeltaBorn      );
   if(fAsymBorn        )b->Add(fAsymBorn      );
   if(fSigmaERT        )b->Add(fSigmaERT       );
   if(fSigmaERT_Plus   )b->Add(fSigmaERT_Plus  );
   if(fSigmaERT_Minus  )b->Add(fSigmaERT_Minus );
   if(fDeltaERT        )b->Add(fDeltaERT       );
   if(fAsymERT         )b->Add(fAsymERT       );
   if(fSigmaIRT        )b->Add(fSigmaIRT       );
   if(fSigmaIRT_Plus   )b->Add(fSigmaIRT_Plus  );
   if(fSigmaIRT_Minus  )b->Add(fSigmaIRT_Minus );
   if(fDeltaIRT        )b->Add(fDeltaIRT       );
   if(fAsymIRT         )b->Add(fAsymIRT       );
   if(fAvgKine         )b->Add(fAvgKine        );
}
//______________________________________________________________________________
Int_t RadiativeCorrections1D::InitCrossSections(){

   TVector3 P_target(0,0,0);
   P_target.SetMagThetaPhi(1.0,fThetaTarget,0.0);

   fPolarizedPhaseSpace = new PhaseSpace();
   fPhaseSpace          = new PhaseSpace();

   auto * varEnergy = new PhaseSpaceVariable("Energy","E#prime"); 
   varEnergy->SetMinimum(0.001);         //GeV
   varEnergy->SetMaximum(6.0); //GeV
   varEnergy->SetVariableUnits("GeV");        //GeV
   fPolarizedPhaseSpace->AddVariable(varEnergy);
   fPhaseSpace->AddVariable(varEnergy);

   auto *   varTheta = new PhaseSpaceVariable("theta","#theta");
   varTheta->SetMinimum(2.0*TMath::Pi()/180.0); //
   varTheta->SetMaximum(180.0*TMath::Pi()/180.0); //
   varTheta->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varTheta);
   fPhaseSpace->AddVariable(varTheta);

   auto *   varPhi = new PhaseSpaceVariable("phi","#phi");
   varPhi->SetMinimum(-180.0*TMath::Pi()/180.0); //
   varPhi->SetMaximum(180.0*TMath::Pi()/180.0); //
   varPhi->SetVariableUnits("rads"); //
   fPolarizedPhaseSpace->AddVariable(varPhi);
   fPhaseSpace->AddVariable(varPhi);

   auto *   varHelicity = new DiscretePhaseSpaceVariable("helicity","#lambda");
   varHelicity->SetNumberOfValues(3); // ROOT string latex
   fPolarizedPhaseSpace->AddVariable(varHelicity);

   fPolarizedPhaseSpace->Refresh();
   //varEnergy->Print();
   //varTheta->Print();
   //varPhi->Print();
   //varHelicity->Print();


   //-----------------------
   // Born Cross sections
   // born unpolarized 
   auto * fDiffXSec00 = new  POLRADBornDiffXSec();
   fDiffXSec00->SetBeamEnergy(fBeamEnergy);
   //fDiffXSec00->SetTargetNucleus(Nucleus::Proton());
   fDiffXSec00->SetPhaseSpace(fPhaseSpace);
   fDiffXSec00->GetPOLRAD()->SetTargetPolarization(0.0);
   fDiffXSec00->GetPOLRAD()->SetPolarizationVectors(P_target,0.0);
   fDiffXSec00->Refresh();
   fBorn0 = fDiffXSec00;
   // born polarized + 
   auto * fDiffXSec10 = new  POLRADBornDiffXSec();
   fDiffXSec10->SetBeamEnergy(fBeamEnergy);
   fDiffXSec10->SetPhaseSpace(fPhaseSpace);
   //fDiffXSec10->SetTargetNucleus(Nucleus::Proton());
   fDiffXSec10->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec10->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec10->Refresh();
   fBorn1 = fDiffXSec10;
   // born polarized - 
   auto * fDiffXSec20 = new  POLRADBornDiffXSec();
   fDiffXSec20->SetBeamEnergy(fBeamEnergy);
   fDiffXSec20->SetPhaseSpace(fPhaseSpace);
   //fDiffXSec20->SetTargetNucleus(Nucleus::Proton());
   fDiffXSec20->GetPOLRAD()->SetTargetPolarization(1.0);
   fDiffXSec20->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec20->Refresh();
   fBorn2 = fDiffXSec20;

   //------------------------
   // Elastic Radiatve Tail 
   // internal and external radiated elastic unpolarized 
   auto * fDiffXSec02 = new  ElasticRadiativeTail();
   fDiffXSec02->SetBeamEnergy(fBeamEnergy);
   fDiffXSec02->SetPolarizations(0.0,0.0);
   fDiffXSec02->SetTargetNucleus(Nucleus::Proton());
   fDiffXSec02->SetPhaseSpace(fPhaseSpace);
   fDiffXSec02->Refresh();
   fERT0_rt = fDiffXSec02;
   // internal and external radiated elastic polarized + 
   auto * fDiffXSec12 = new  ElasticRadiativeTail();
   fDiffXSec12->SetBeamEnergy(fBeamEnergy);
   fDiffXSec12->SetPolarizations(1.0,1.0);
   fDiffXSec12->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec12->GetBornXSec()->GetPOLRAD()->SetPolarizationVectors(P_target,1.0);
   fDiffXSec12->SetTargetNucleus(Nucleus::Proton());
   fDiffXSec12->SetPhaseSpace(fPhaseSpace);
   fDiffXSec12->Refresh();
   fERT1_rt = fDiffXSec12;
   // internal and external radiated polarized -
   auto * fDiffXSec22 = new ElasticRadiativeTail();
   fDiffXSec22->SetBeamEnergy(fBeamEnergy);
   fDiffXSec22->SetPolarizations(1.0,1.0);
   fDiffXSec22->GetBornXSec()->GetPOLRAD()->SetPolarizationVectors(P_target,-1.0);
   fDiffXSec22->SetTargetNucleus(Nucleus::Proton());
   fDiffXSec22->SetPhaseSpace(fPhaseSpace);
   fDiffXSec22->Refresh();
   fERT2_rt = fDiffXSec22;


   //-------------------------
   //// IRT unpolarized 
   auto * fDiffXSec01 = new  Radiator<POLRADBornDiffXSec>();
   fDiffXSec01->SetBeamEnergy(fBeamEnergy);
   fDiffXSec01->SetTargetNucleus(Nucleus::Proton());
   fDiffXSec01->POLRADBornDiffXSec::SetTargetNucleus(Nucleus::Proton());
   fDiffXSec01->POLRADBornDiffXSec::SetPolarizations(0.0,0.0);
   fDiffXSec01->SetPolarizations(0.0,0.0);
   fDiffXSec01->SetPhaseSpace(fPhaseSpace);
   fDiffXSec01->Refresh();
   fInelasticXSec0_rt = fDiffXSec01;

   // internal radiated polarized + 
   auto * fDiffXSec11 = new Radiator<POLRADBornDiffXSec>();
   fDiffXSec11->SetBeamEnergy(fBeamEnergy);
   fDiffXSec11->SetTargetNucleus(Nucleus::Proton());
   fDiffXSec11->POLRADBornDiffXSec::SetTargetNucleus(Nucleus::Proton());
   fDiffXSec11->SetPolarizations(1.0,1.0);
   fDiffXSec11->POLRADBornDiffXSec::SetPolarizations(1.0,1.0);
   fDiffXSec11->SetTargetPolarization(P_target);
   fDiffXSec11->POLRADBornDiffXSec::SetTargetPolarization(P_target);
   fDiffXSec11->SetPhaseSpace(fPhaseSpace);
   fDiffXSec11->Refresh();
   fInelasticXSec1_rt = fDiffXSec11;

   // internal radiated polarized -
   auto * fDiffXSec21 = new  Radiator<POLRADBornDiffXSec>();
   fDiffXSec21->SetBeamEnergy(fBeamEnergy);
   fDiffXSec21->SetTargetNucleus(Nucleus::Proton());
   fDiffXSec21->POLRADBornDiffXSec::SetTargetNucleus(Nucleus::Proton());
   fDiffXSec21->SetPolarizations(1.0,-1.0);
   fDiffXSec21->POLRADBornDiffXSec::SetPolarizations(1.0,-1.0);
   fDiffXSec21->SetTargetPolarization(P_target);
   fDiffXSec21->POLRADBornDiffXSec::SetTargetPolarization(P_target);
   fDiffXSec21->SetPhaseSpace(fPhaseSpace);
   fDiffXSec21->Refresh();
   fInelasticXSec2_rt = fDiffXSec21;

   fDiffXSec01->SetRadiationLength(0.0275,0.0236);
   fDiffXSec11->SetRadiationLength(0.0275,0.0236);
   fDiffXSec21->SetRadiationLength(0.0275,0.0236);

   //PrintConfiguration();

   return 0;
}
//_____________________________________________________________________________
Int_t RadiativeCorrections1D::InitHist(const TH1F * h0, TH1F ** h1, const char *n){
   if(!h0) return -1;
   (*h1) = (TH1F*)h0->Clone(n);
   (*h1)->Reset();
   (*h1)->SetNameTitle(n,n);
   return 0;
}
//______________________________________________________________________________
Int_t RadiativeCorrections1D::CreateHistograms(TH1F * h0){

   InitHist(h0, &fSigmaBorn, "SigmaBorn");
   InitHist(h0, &fSigmaBorn_Plus, "SigmaBorn_Plus");
   InitHist(h0, &fSigmaBorn_Minus, "SigmaBorn_Minus");
   InitHist(h0, &fDeltaBorn, "DeltaBorn");
   InitHist(h0, &fAsymBorn, "AsymBorn");

   InitHist(h0, &fSigmaIRT, "SigmaIRT");
   InitHist(h0, &fSigmaIRT_Plus, "SigmaIRT_Plus");
   InitHist(h0, &fSigmaIRT_Minus, "SigmaIRT_Minus");
   InitHist(h0, &fDeltaIRT, "DeltaIRT");
   InitHist(h0, &fAsymIRT, "AsymIRT");

   InitHist(h0, &fSigmaERT, "SigmaERT");
   InitHist(h0, &fSigmaERT_Plus, "SigmaERT_Plus");
   InitHist(h0, &fSigmaERT_Minus, "SigmaERT_Minus");
   InitHist(h0, &fDeltaERT, "DeltaERT");
   InitHist(h0, &fAsymERT, "AsymERT");

   //InitHist(h0, &fSigmaBorn, "SigmaBorn");
   //InitHist(h0, &fSigmaBorn_Plus, "SigmaBorn_Plus");
   //InitHist(h0, &fSigmaBorn_Minus, "SigmaBorn_Minus");
   //InitHist(h0, &fDeltaBorn, "DeltaBorn");

   return 0;
}
//______________________________________________________________________________
Int_t RadiativeCorrections1D::Calculate(){
   if( fBeamEnergy == 0 ) {
      Error("Calculate","Beam energy not set.");
      return -1;
   }
   if( !fAvgKine ) {
      Error("Calculate","Averaged kinematics not set.");
      return -1;
   }
   if( !fSigmaBorn ) CreateHistograms(&fAvgKine->fN);

   Double_t x_arg[3] = {0.0,0.0,0.0};
   Double_t p_arg[3] = {0.0,0.0,0.0};
   TH1F * h0 = &fAvgKine->fN;
   int nx = h0->GetNbinsX();
   int ny = h0->GetNbinsY();
   int nz = h0->GetNbinsZ();
   for(int i = 1; i<=nx; i++) {
      for(int j = 1; j<=ny; j++) {
         for(int k = 1; k<=nz; k++) {

            if( fAvgKine->fN.GetBinContent(i,j,k) == 0.0 ) continue;
            Double_t E0    = fBeamEnergy;
            Double_t x     = fAvgKine->fx.GetBinContent(i,j,k);
            Double_t W     = fAvgKine->fW.GetBinContent(i,j,k);
            Double_t phi   = fAvgKine->fPhi.GetBinContent(i,j,k);
            Double_t theta = fAvgKine->fTheta.GetBinContent(i,j,k);
            Double_t Q2    = fAvgKine->fQ2.GetBinContent(i,j,k);
            Double_t y     = (Q2/(2.*(M_p/GeV)*x))/E0;
            Double_t Ep    = fAvgKine->fE.GetBinContent(i,j,k);

            //x_arg[0]                = x;
            //p_arg[0]                = Q2;
            //p_arg[1]                = phi;
            x_arg[0]                = Ep; 
            x_arg[1]                = theta;
            x_arg[2]                = phi;

            //std::cout << "x=" << x << std::flush;
            Double_t sigma_b       = fBorn0->EvaluateXSec(x_arg);//xDependentXSec(x_arg,p_arg);
            Double_t sigma_b_plus  = fBorn1->EvaluateXSec(x_arg);//xDependentXSec(x_arg,p_arg);
            Double_t sigma_b_minus = fBorn2->EvaluateXSec(x_arg);//xDependentXSec(x_arg,p_arg);
            Double_t A_b           = (sigma_b_plus - sigma_b_minus)/(sigma_b_plus + sigma_b_minus);
            if(TMath::IsNaN(sigma_b)) sigma_b             = 0.0;
            if(TMath::IsNaN(sigma_b_plus)) sigma_b_plus   = 0.0;
            if(TMath::IsNaN(sigma_b_minus)) sigma_b_minus = 0.0;
            if(TMath::IsNaN(A_b)) A_b = 0.0;
            if(std::isinf(A_b)) A_b = 0.0;

            fSigmaBorn->SetBinContent(i,j,k, sigma_b);
            fSigmaBorn_Plus->SetBinContent(i,j,k,  sigma_b_plus);
            fSigmaBorn_Minus->SetBinContent(i,j,k, sigma_b_minus);
            fDeltaBorn->SetBinContent(i,j,k, sigma_b_plus - sigma_b_minus);
            fAsymBorn->SetBinContent(i,j,k,A_b);
            //std::cout << ", A_born=" << A_b << std::flush;

            Double_t sigma_in       = fInelasticXSec0_rt->EvaluateXSec(x_arg);//xDependentXSec(x_arg,p_arg);
            Double_t sigma_in_plus  = fInelasticXSec1_rt->EvaluateXSec(x_arg);//xDependentXSec(x_arg,p_arg);
            Double_t sigma_in_minus = fInelasticXSec2_rt->EvaluateXSec(x_arg);//xDependentXSec(x_arg,p_arg);
            Double_t A_in           = (sigma_in_plus - sigma_in_minus)/(sigma_in_plus + sigma_in_minus);
            if(TMath::IsNaN(sigma_in)) sigma_in             = 0.0;
            if(TMath::IsNaN(sigma_in_plus)) sigma_in_plus   = 0.0;
            if(TMath::IsNaN(sigma_in_minus)) sigma_in_minus = 0.0;
            if(TMath::IsNaN(A_in)) A_in = 0.0;
            if(std::isinf(A_in)) A_in = 0.0;

            fSigmaIRT->SetBinContent(i,j,k, sigma_in);
            fSigmaIRT_Plus->SetBinContent(i,j,k, sigma_in_plus);
            fSigmaIRT_Minus->SetBinContent(i,j,k, sigma_in_minus);
            fDeltaIRT->SetBinContent(i,j,k, sigma_in_plus - sigma_in_minus);
            fAsymIRT->SetBinContent(i,j,k,A_in);
            //std::cout << ", A_inel=" << A_in << std::flush;

            Double_t sigma_el_plus  = fERT1_rt->EvaluateXSec(x_arg);//DependentXSec(x_arg,p_arg);
            Double_t sigma_el_minus = fERT2_rt->EvaluateXSec(x_arg);//DependentXSec(x_arg,p_arg);
            Double_t sigma_el       = (sigma_el_plus + sigma_el_minus)/2.0;//fERT0_rt->xDependentXSec(x_arg,p_arg);
            Double_t A_el           = (sigma_el_plus - sigma_el_minus)/(sigma_el_plus + sigma_el_minus);
            if(TMath::IsNaN(sigma_el)) sigma_el             = 0.0;
            if(TMath::IsNaN(sigma_el_plus)) sigma_el_plus   = 0.0;
            if(TMath::IsNaN(sigma_el_minus)) sigma_el_minus = 0.0;
            if(TMath::IsNaN(A_el)) A_el = 0.0;
            if(std::isinf(A_el)) A_el = 0.0;
            fSigmaERT->SetBinContent(i,j,k, sigma_el);
            fSigmaERT_Plus->SetBinContent(i,j,k, sigma_el_plus);
            fSigmaERT_Minus->SetBinContent(i,j,k, sigma_el_minus);
            fDeltaERT->SetBinContent(i,j,k, sigma_el_plus - sigma_el_minus);
            fAsymERT->SetBinContent(i,j,k,A_el);
            //std::cout << ", A_el=" << A_el << std::endl;

            //Double_t A_calc         = (sigma_in_plus - sigma_in_minus)/(sigma_in_plus + sigma_in_minus);
            //Double_t C_el           = (sigma_el_plus - sigma_el_minus)/(sigma_in);
            //Double_t f_el           =  sigma_in/(sigma_in + sigma_el);
            //Double_t f_bg           =  4.122*TMath::Exp(-2.442*Ep);
            //Double_t C_back         = (1.0 - f_bg*0.05/A)/(1.0 - f_bg);
            //Double_t A_corr         = C_back*(A/f_el - C_el);
            //Double_t delta_A        = A - A_calc;

         }
      }
   }
   return 0;
}
//______________________________________________________________________________

void RadiativeCorrections1D::PrintConfiguration(std::ostream& s)
{
   s << " Born 0 : " << fBorn0->GetName() << "  " << fBorn0->GetTitle() << std::endl;
   s << " Born 1 : " << fBorn1->GetName() << "  " << fBorn1->GetTitle() << std::endl;
   s << " Born 2 : " << fBorn2->GetName() << "  " << fBorn2->GetTitle() << std::endl;
   s << " ERT  0 : " << fERT0_rt->GetName() << "  " << fERT0_rt->GetTitle() << std::endl;
   s << " ERT  1 : " << fERT1_rt->GetName() << "  " << fERT1_rt->GetTitle() << std::endl;
   s << " ERT  2 : " << fERT2_rt->GetName() << "  " << fERT2_rt->GetTitle() << std::endl;
   s << " IRT  0 : " << fInelasticXSec0_rt->GetName() << "  " << fInelasticXSec0_rt->GetTitle() << std::endl;
   s << " IRT  1 : " << fInelasticXSec1_rt->GetName() << "  " << fInelasticXSec1_rt->GetTitle() << std::endl;
   s << " IRT  2 : " << fInelasticXSec2_rt->GetName() << "  " << fInelasticXSec2_rt->GetTitle() << std::endl;
   s << " Theta Target : " << fThetaTarget << std::endl;

   std::vector<InclusiveDiffXSec*> xsecs;
   xsecs.push_back(fBorn0);
   xsecs.push_back(fBorn1);
   xsecs.push_back(fBorn2);
   xsecs.push_back(fERT0_rt);
   xsecs.push_back(fERT1_rt);
   xsecs.push_back(fERT2_rt);
   xsecs.push_back(fInelasticXSec0_rt);
   xsecs.push_back(fInelasticXSec1_rt);
   xsecs.push_back(fInelasticXSec2_rt);

   s << " Beam Energy check : " << fBeamEnergy ;
   for( auto xs : xsecs ) {
      s <<  " " << xs->GetBeamEnergy();
      auto * polrad = dynamic_cast<POLRADInternalPolarizedDiffXSec*>(xs);
      if(polrad){
         s <<  " " << polrad->GetPOLRAD()->GetKinematics()->GetEs();
      }

      auto * rad = dynamic_cast<Radiator<POLRADBornDiffXSec>*>(xs);
      if(rad){
         s <<  " " << rad->GetPOLRAD()->GetKinematics()->GetEs();
         s <<  " " << rad->GetRADCOR()->GetKinematics()->GetEs();
      }

   }
   s << std::endl;

   s << " Target Check: " ;
   for( auto xs : xsecs ) {
      s <<  " " << xs->GetTargetNucleus().GetName();
      auto * polrad = dynamic_cast<POLRADInternalPolarizedDiffXSec*>(xs);
      if(polrad){
         s <<  " " << polrad->GetPOLRAD()->fTargetNucleus.GetName();
      }

      auto * rad = dynamic_cast<Radiator<POLRADBornDiffXSec>*>(xs);
      if(rad){
         s <<  " " << rad->GetPOLRAD()->fTargetNucleus.GetName();
      }

   }
   s << std::endl;

   s << " Structure Functions: " ;
   for( auto xs : xsecs ) {
      s <<  " " << xs->GetUnpolarizedStructureFunctions()->GetName();
      //POLRADInternalPolarizedDiffXSec * polrad = dynamic_cast<POLRADInternalPolarizedDiffXSec*>(xs);
      //if(polrad){
      //   s <<  " " << polrad->GetPOLRAD()->fTargetNucleus.GetName();
      //}

      //Radiator<POLRADBornDiffXSec> * rad = dynamic_cast<Radiator<POLRADBornDiffXSec>*>(xs);
      //if(rad){
      //   s <<  " " << rad->GetPOLRAD()->fTargetNucleus.GetName();
      //}

   }
   s << std::endl;

   s << "Polarized Structure Functions: " ;
   for( auto xs : xsecs ) {
      s <<  " " << xs->GetPolarizedStructureFunctions()->GetName();
      //POLRADInternalPolarizedDiffXSec * polrad = dynamic_cast<POLRADInternalPolarizedDiffXSec*>(xs);
      //if(polrad){
      //   s <<  " " << polrad->GetPOLRAD()->fTargetNucleus.GetName();
      //}

      //Radiator<POLRADBornDiffXSec> * rad = dynamic_cast<Radiator<POLRADBornDiffXSec>*>(xs);
      //if(rad){
      //   s <<  " " << rad->GetPOLRAD()->fTargetNucleus.GetName();
      //}

   }
   s << std::endl;

   s << "Polarized Structure Functions: " ;
   for( auto xs : xsecs ) {
      s <<  " " << xs->GetFormFactors()->GetName();
      //POLRADInternalPolarizedDiffXSec * polrad = dynamic_cast<POLRADInternalPolarizedDiffXSec*>(xs);
      //if(polrad){
      //   s <<  " " << polrad->GetPOLRAD()->fTargetNucleus.GetName();
      //}

      //Radiator<POLRADBornDiffXSec> * rad = dynamic_cast<Radiator<POLRADBornDiffXSec>*>(xs);
      //if(rad){
      //   s <<  " " << rad->GetPOLRAD()->fTargetNucleus.GetName();
      //}

   }
   s << std::endl;

   //fERT0_rt           ;
   //fERT1_rt           ;
   //fERT2_rt           ;
   //fInelasticXSec0_rt ;
   //fInelasticXSec1_rt ;
   //fInelasticXSec2_rt ;
   //fDiffXSec03        ;
   //fDiffXSec13        ;
   //fDiffXSec23        ;
}
//______________________________________________________________________________
}}
