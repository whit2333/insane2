#include "insane/base/KineFuncs.h"

using namespace insane::units;

void insane::kine::SetMomFromEThetaPhi(TParticle* p, double* x) {
  double momentum = sqrt(x[0] * x[0] - pow(p->GetMass(), 2));
  p->SetMomentum(cos(x[2]) * sin(x[1]) * momentum, // px
                 sin(x[2]) * sin(x[1]) * momentum, // py
                 cos(x[1]) * momentum,             // pz
                 x[0]);                            // E
}
void insane::kine::SetEFromMomThetaPhi(TParticle* p, double* x) {
  double momentum = x[0];
  double mass     = p->GetMass();
  double energy   = TMath::Sqrt(momentum * momentum + mass * mass);
  p->SetMomentum(cos(x[2]) * sin(x[1]) * momentum, // px
                 sin(x[2]) * sin(x[1]) * momentum, // py
                 cos(x[1]) * momentum,             // pz
                 energy);                          // E
}

std::complex<double> insane::Math::CGLN_F1(double Q2, double s, double t) {
  std::complex<double> res(1.0);
  return (res);
}

std::complex<double> insane::Math::CGLN_F2(double Q2, double s, double t) {
  std::complex<double> res(1.0);
  return (res);
}

std::complex<double> insane::Math::CGLN_F3(double Q2, double s, double t) {
  std::complex<double> res(1.0);
  return (res);
}

std::complex<double> insane::Math::CGLN_F4(double Q2, double s, double t) {
  std::complex<double> res(1.0);
  return (res);
}

std::complex<double> insane::Math::CGLN_F5(double Q2, double s, double t) {
  std::complex<double> res(1.0);
  return (res);
}

std::complex<double> insane::Math::CGLN_F6(double Q2, double s, double t) {
  std::complex<double> res(1.0);
  return (res);
}

std::complex<double> insane::Math::CGLN_F7(double Q2, double s, double t) {
  std::complex<double> res(1.0);
  return (res);
}

std::complex<double> insane::Math::CGLN_F8(double Q2, double s, double t) {
  std::complex<double> res(1.0);
  return (res);
}

std::complex<double> insane::Math::multipole_E(double Q2, double W) {
  std::complex<double> res(1.0);
  return (res);
}
std::complex<double> insane::Math::multipole_M(double Q2, double W) {
  std::complex<double> res(1.0);
  return (res);
}

//double insane::f_rad_length(double a) {
//  double res = 0;
//  res        = a * a *
//        (1.0 / (1.0 + a * a) + 0.20206 - 0.0369 * a * a + 0.0083 * a * a * a * a -
//         0.002 * a * a * a * a * a * a);
//  return (res);
//}
///** Returns the radition length */
//double insane::rad_length(int Z, int A) {
//  double res   = 1.0 / (716.408 * A);
//  double alpha = 1.0 / 137.0;
//  double Lrad, Lradprime;
//  if (Z == 1) {
//    Lrad      = 5.31;
//    Lradprime = 6.144;
//  } else if (Z == 2) {
//    Lrad      = 4.79;
//    Lradprime = 5.621;
//  } else if (Z == 3) {
//    Lrad      = 4.74;
//    Lradprime = 5.805;
//  } else if (Z == 4) {
//    Lrad      = 4.71;
//    Lradprime = 5.924;
//  } else {
//    Lrad      = TMath::Log(184.15 * TMath::Power(float(Z), -1.0 / 3.0));
//    Lradprime = TMath::Log(1194.0 * TMath::Power(float(Z), -2.0 / 3.0));
//  }
//  res = res * (float(Z * Z) * (Lrad - f_rad_length(alpha * float(Z))) + float(Z) * Lradprime);
//  return (1.0 / res);
//}

double insane::TestFunction22(double x) { return (x * x); }

namespace insane {
  namespace kine {

    /** \f$ \nu_0 = -Q^2 \f$  */
    double v0(double en, double enprime, double q) {
      return ((en - enprime) * (en - enprime) - q * q);
    }
    double vL(double Qsq, double q) { return ((Qsq / (q * q)) * (Qsq / (q * q))); }
    double vT(double Qsq, double q, double theta) {
      return (0.5 * Qsq / (q * q) + TMath::Power(TMath::Tan(theta / 2.0), 2));
    }
    double vTT(double Qsq, double q) { return ((-Qsq / (q * q)) * 0.5); }
    double vTL(double Qsq, double q, double theta) {
      return (-Qsq / (q * q) *
              TMath::Sqrt((Qsq / (q * q) + TMath::Power(TMath::Tan(theta / 2.0), 2)) / 2.0));
    }
    double vTprime(double Qsq, double q, double theta) {
      return (TMath::Sqrt(Qsq / (q * q) + TMath::Power(TMath::Sin(theta / 2.0), 2)) *
              TMath::Tan(theta / 2.0));
    }
    double vTLprime(double Qsq, double q, double theta) {
      return (-Qsq / (q * q) * TMath::Tan(theta / 2.0) / TMath::Sqrt(2.0));
    }

    double Sig_Mott(double en, double theta) {
      double res = (1.0 / 137.0) * TMath::Cos(theta / 2.0) /
                   (2.0 * en * TMath::Power(TMath::Sin(theta / 2.0), 2.0));
      return res * res;
    }
    double fRecoil(double en, double theta, double Mtarg) {
      return (1.0 + 2.0 * en * TMath::Power(TMath::Sin(theta / 2.0), 2) / (Mtarg));
    }

    double tau(double Qsq, double Mtarg) { return (Qsq / (4.0 * Mtarg * Mtarg)); }

    double q_abs(double Qsq, double Mtarg) {
      return (TMath::Sqrt(4.0 * Mtarg * Mtarg * tau(Qsq, Mtarg) * (1.0 + tau(Qsq, Mtarg))));
    }

    // ---------------------------------------------
    double Q2(double en, double enprime, double theta) {
      return (4.0 * en * enprime * TMath::Power(TMath::Sin(theta / 2.0), 2));
    }
    double Q2_EEprimeTheta(double en, double enprime, double theta) {
      return (4.0 * en * enprime * TMath::Power(TMath::Sin(theta / 2.0), 2));
    }
    double Q2_xW(double x, double W, double Mtarg) {
      return ((W * W - Mtarg * Mtarg) / ((1 / x) - 1));
    }


    double xBjorken(double Qsq, double nu, double Mtarg) {
      // Bjorken scaling variable, x
      return (Qsq / (2.0 * Mtarg * nu));
    }

    double xBjorken_EEprimeTheta(double e, double eprime, double theta) {
      // Bjorken scaling variable, x
      return (xBjorken(Q2(e, eprime, theta), (e - eprime)));
    }

    double xi_Nachtmann(double x, double Q2) {
      // Nachtmann scaling variable, xi
      return (2.0 * x / (1.0 + TMath::Sqrt(1.0 + 4.0 * x * x * ((M_p / GeV) * (M_p / GeV)) / Q2)));
    }

    double nu(double en, double enprime) { return (en - enprime); }

    double nu_WsqQsq(double Wsq, double Qsq, double Mtarg) {
      double num = Wsq - Mtarg * Mtarg + Qsq;
      double den = 2. * Mtarg;
      double res = num / den;
      return res;
    }

    double epsilon(double en, double enprime, double theta) {
      if (theta == 0.0)
        return (0.0);
      double nu2  = nu(en, enprime) * nu(en, enprime);
      double Q2   = Q2_EEprimeTheta(en, enprime, theta);
      double TAN  = TMath::Tan(theta / 2.0);
      double TAN2 = TAN * TAN;
      double num  = 1.;
      double den  = 1. + 2. * (1. + nu2 / Q2) * TAN2;
      double eps  = num / den;
      return eps;
    }

    double epsilon_xQ2(double xB, double Q2, double M){
      return 2.0*xB*M/TMath::Sqrt(Q2);
    }
    //______________________________________________________________________________

    double W2(double Qsq, double nu, double Mtarg) {
      return (2.0 * nu * Mtarg - Qsq + Mtarg * Mtarg);
    }

    double W_xQsq(double x, double Qsq, double Mtarg) {
      double result = 0.0;
      double t      = 0.0;
      double W      = TMath::Sqrt(Mtarg * Mtarg + Qsq / x - Qsq);
      if (W > 0) {
        result = W;
      } else {
        result = 0;
      }
      return result;
    }
    double W_xQ2(double x, double Qsq, double Mtarg) { return W_xQsq(x, Qsq, Mtarg); }

    double W_EEprimeTheta(double e, double eprime, double theta) {
      double result;
      double W = W_xQsq(xBjorken_EEprimeTheta(e, eprime, theta), Q2(e, eprime, theta));
      if (W > 0) {
        result = W;
      } else {
        result = 0;
      }
      return result;
    }

    double xBjorken_WQsq(double W, double Qsq, double Mtarg) {
      return (Qsq / (W * W + Qsq - Mtarg * Mtarg));
    }
    double xBjorken_WQ2(double W, double Q2, double Mtarg) {
      return (Q2 / (W * W + Q2 - Mtarg * Mtarg));
    }

    double BeamEnergy_xQ2y(double x, double Q2, double y, double Mtarg) {
      return (Q2 / (2. * Mtarg * x * y));
    }
    double Eprime_xQ2y(double x, double Q2, double y, double Mtarg) {
      return (-(Q2 * (-1.0 + y)) / (2. * Mtarg * x * y));
    }
    double Eprime_W2theta(double W2, double theta, double Ebeam, double Mtarg) {
      return ((Mtarg * Mtarg + 2.0 * Mtarg * Ebeam - W2) /
              (2.0 * Mtarg + 4.0 * Ebeam * TMath::Power(TMath::Sin(theta / 2.0), 2.0)));
    }
    double Theta_xQ2y(double x, double Q2, double y, double Mtarg) {
      if (Q2 == 0.0)
        return (0.0);
      double res = 2.0 * TMath::ASin((Mtarg * x * y) / TMath::Sqrt(Q2 - Q2 * y));
      if (res > TMath::Pi())
        return TMath::Pi();
      return res;
    }
    double Theta_epsilonQ2W2(double eps, double Q2, double W2, double Mtarg) {
      using namespace TMath;
      double nu = (W2 - Mtarg * Mtarg + Q2) / (2.0 * Mtarg);
      if (Q2 == 0.0)
        return 0.0;
      double arg = ((1.0 / eps) - 1.0) / (2.0 * (1.0 + (nu * nu) / Q2));
      return (2.0 * ATan(Sqrt(arg)));
    }
    double Eprime_epsilonQ2W2(double eps, double Q2, double W2, double Mtarg) {
      using namespace TMath;
      double nu    = (W2 - Mtarg * Mtarg + Q2) / (2.0 * Mtarg);
      double theta = 0.0;
      if (Q2 != 0.0)
        theta = Theta_epsilonQ2W2(eps, Q2, W2, Mtarg);
      double s2 = Power(Sin(theta / 2.0), 2.0);
      double t1 = (nu / 2.0);
      double t2 = -1.0;
      if (Q2 != 0.0)
        t2 = (-1.0 + Sqrt(1.0 + Q2 / (nu * nu * s2)));
      return t1 * t2;
    }
    double BeamEnergy_epsilonQ2W2(double eps, double Q2, double W2, double Mtarg) {
      using namespace TMath;
      double nu     = (W2 - Mtarg * Mtarg + Q2) / (2.0 * Mtarg);
      double Eprime = Eprime_epsilonQ2W2(eps, Q2, W2, Mtarg);
      return (nu + Eprime);
    }

    double K_Hand(double x, double Q2) {
      // Hand convention for the virtual photon flux factor.
      // K is the energy requred for a real photon to create the final state
      // L.N. Hand Phys.Rev. 129 (1963) 1834-1846
      double nu = Q2 / (2.0 * (M_p / GeV) * x);
      return (nu - Q2 / (2.0 * (M_p / GeV)));
    }

    //double I_gamma_1_approx(double t, double E0, double k) {
    //  // Bremsstrahlung bremsstrahlung spectrum.
    //  // Approximation formula
    //  // Tsai and Whitis, SLAC-PUB-184 1966 eqn.25
    //  using namespace TMath;
    //  double num = Power(1.0 - k / E0, 4.0 * t / 3.0) - Exp(-7.0 * t / 9.0);
    //  double den = 7.0 / 9.0 + (4.0 / 3.0) * Log(1.0 - k / E0);
    //  return (num / (k * den));
    //}
    //double I_gamma_1_sumterm(int n, double tprime, double u) {
    //  // sum over n term from below
    //  using namespace TMath;
    //  double num =
    //      ((4.0 / 3.0) * Power(-1.0, double(n)) - u * u) * Power(Log(1.0 / u), double(n + 1));
    //  double den = double(Factorial(n)) * (double(n) + 4.0 * tprime / 3.0 + 1.0);
    //  return (num / den);
    //}
    //double I_gamma_1_integrand(double tprime, double u) {
    //  // integrand used below
    //  using namespace TMath;
    //  double num    = Exp(7.0 * tprime / 9.0) * Power(Log(1.0 / u), 4.0 * tprime / 3.0);
    //  double den    = Gamma(4.0 * tprime / 3.0 + 1.0);
    //  double sum    = u;
    //  int    Nterms = 5;
    //  for (int n = 0; n < Nterms; n++) {
    //    double nterm = I_gamma_1_sumterm(n, tprime, u);
    //    sum += nterm;
    //    // std::cout << "n = " << n << "  " << nterm << std::endl;
    //  }
    //  return sum * num / den;
    //}
    //double I_gamma_1(double t, double E0, double k) {
    //  // Bremsstrahlung bremsstrahlung spectrum.
    //  // Tsai and Whitis, SLAC-PUB-184 1966 eqn.24
    //  // Exact formula (but with a finite sum. See eqn 24.)
    //  // simple integration
    //  // This result is still wrong. Cannot find bug.
    //  // using approximate instead.
    //  // return I_gamma_1_approx(t,E0,k);
    //  if (k >= E0)
    //    return 0.0;
    //  using namespace TMath;
    //  double u = k / E0;
    //  if (u < 0.0005)
    //    u = 0.0005; // for small u just fix the coefficient and let the spectrum go like 1/k
    //  int    Nint    = 20;
    //  double delta_t = t / double(Nint);
    //  double tprime  = 0.0;
    //  double tot     = 0.0;
    //  for (int i = 0; i < Nint; i++) {
    //    tprime = (double(i) + 0.5) * delta_t;
    //    tot += I_gamma_1_integrand(tprime, u) * delta_t;
    //  }
    //  return (Exp(-7.0 * t / 9.0) * tot / k);
    //}

    //double I_gamma_1_test(double t, double E0, double k) {
    //  // Bremsstrahlung bremsstrahlung spectrum.
    //  // Tsai and Whitis, SLAC-PUB-184 1966 eqn.24
    //  // Exact formula (but with a finite sum. See eqn 24.)
    //  // simple integration
    //  // This result is still wrong. Cannot find bug.
    //  if (k >= E0)
    //    return 0.0;
    //  using namespace TMath;
    //  double u = k / E0;
    //  if (u < 0.0005)
    //    u = 0.0005; // for small u just fix the coefficient and let the spectrum go like 1/k
    //  int    Nint    = 100;
    //  double delta_t = t / double(Nint);
    //  double tprime  = 0.0;
    //  double tot     = 0.0;
    //  for (int i = 0; i < Nint; i++) {
    //    tprime       = (double(i) + 0.5) * delta_t;
    //    double iterm = I_gamma_1_integrand(tprime, u) * delta_t;
    //    tot += iterm;
    //    // std::cout << "tprime = " << tprime << "  " << iterm << std::endl;
    //  }
    //  return (Exp(-7.0 * t / 9.0) * tot / k);
    //}

    //double N_gamma(double dOverX0, double kmin, double kmax, double E0) {
    //  // Number of photons from bremstrahlung beam.
    //  // Formula from Particle Data Group. Eq(27.29)-2012
    //  // If E0 is optional. By default it is set to kmax
    //  // dOverX0 is the target length (in units of radiation length over the radiation length)
    //  // E0 is the electron beam energy
    //  // kmax is the the maximum photon energy and kmin is the min.
    //  if (E0 < 0.0)
    //    E0 = kmax;
    //  if (kmin > kmax)
    //    return 0.0;
    //  if (kmin > E0)
    //    return 0.0;
    //  return (dOverX0 * ((4.0 / 3.0) * TMath::Log(kmax / kmin) - 4.0 * (kmax - kmin) / (3.0 * E0) +
    //                     (kmax * kmax - kmin * kmin) / (2 * E0 * E0)));
    //}

    //double I_gamma_1_k_avg(double t, double kmin, double E0) {
    //  // Average photon energy for a bremsstrahlung spectrum
    //  // E0 is the electron beam energy
    //  // kmin defines the range of photon energies to average over.
    //  if (E0 < 0.0)
    //    return 0.0;
    //  int    npoints = 50;
    //  double deltak  = (E0 - kmin) / ((double)npoints);
    //  double k       = 0.0;
    //  double num     = 0.0;
    //  for (int i = 0; i < npoints; i++) {
    //    k = kmin + ((double)i + 0.5) * deltak;
    //    num += k * I_gamma_1_approx(t, E0, k) * deltak;
    //    // std::cout << "deltak = " << deltak << std::endl;
    //    // std::cout << "k = " << k << std::endl;
    //    // std::cout << "num = " << num << std::endl;
    //  }
    //  return (num);
    //}

    //double Ebrem_avg(double t, double kmin, double E0) {
    //  // Average photon energy for a bremsstrahlung spectrum
    //  // E0 is the electron beam energy
    //  // kmin defines the range of photon energies to average over.
    //  if (E0 < 0.0)
    //    return 0.0;
    //  int    npoints = 5;
    //  double deltak  = (E0 - kmin) / ((double)npoints);
    //  double k       = 0.0;
    //  double num     = 0.0;
    //  double denom   = 0.0;
    //  for (int i = 0; i < npoints; i++) {
    //    k           = kmin + ((double)i + 0.5) * deltak;
    //    double Igam = I_gamma_1(t, E0, kmin);
    //    num += k * Igam * deltak;
    //    denom += Igam * deltak;
    //  }
    //  return (num / denom);
    //}

    //double Q_equiv_quanta(double t, double kmin, double E0) {
    //  // Number of equivalent quanta for a bremsstrahlung spectrum
    //  // E0 is the electron beam energy
    //  // kmin defines the range of photon energies to average over.
    //  return (Ebrem_avg(t, kmin, E0) / E0);
    //}

    //double k_min_photoproduction(double Ex, double thetax, double mx, double mt, double mn) {
    //  // Minimum photon energy for photoproduction in gamma+p -> x+n
    //  // where x is a hadron and n is a nucleon
    //  // Ex and thetax are the produced hadron's energy and angle
    //  // mx is the hadron mass, mt is the target mass, mn is the recoil nucleon mass.
    //  return ((mn * mn - mx * mx - mt * mt + 2.0 * mt * Ex) /
    //          (2.0 * (mt - Ex + TMath::Sqrt(Ex * Ex - mx * mx) * TMath::Cos(thetax))));
    //}

    double D(double e, double eprime, double theta, double R) {
      double eps = epsilon(e, eprime, theta);
      return ((e - eps * eprime) / (e * (1.0 + eps * R)));
    }

    double Dprime(double e, double eprime, double theta, double R) {
      double y       = (e - eprime) / e;
      double eps     = epsilon(e, eprime, theta);
      double num     = (1. - eps) * (2. - y);
      double den     = y * (1. + eps * R);
      double d_prime = num / den;
      return d_prime;
    }
    double d(double e, double eprime, double theta, double R) {
      double eps = epsilon(e, eprime, theta);
      return (D(e, eprime, theta, R) * TMath::Sqrt(2.0 * eps / (1.0 + eps)));
    }

    double Eta(double e, double eprime, double theta) {
      double eps = epsilon(e, eprime, theta);
      return (eps * TMath::Sqrt(Q2(e, eprime, theta)) / (e - eps * eprime));
    }

    double Xi(double e, double eprime, double theta) {
      double eps = epsilon(e, eprime, theta);
      return (Eta(e, eprime, theta) * (1.0 + eps) / (2.0 * eps));
    }

    double Chi(double e, double eprime, double theta, double phi) {
      return ((eprime * TMath::Sin(theta) * (1.0 / TMath::Cos(phi))) /
              (e - eprime * TMath::Cos(theta)));
    }

    // SLAC E143 R = sig_L/sig_T fit
    double R1998(double x, double Q2) {
      double R = (1. / 3.) * (Ra1998(x, Q2) + Rb1998(x, Q2) + Rc1998(x, Q2));
      return R;
    }

    double RErr1998(double x, double Q2) {
      // First term
      double T1 = 0.0078;
      // Second term
      double T2 = -0.013 * x;
      // Third term
      double a  = 0.070 - 0.39 * x + 0.70 * x * x;
      double b  = 1.7 + Q2;
      double T3 = a / b;
      // Put it together
      double dR = T1 + T2 + T3;
      return dR;
    }
    double Ra1998(double x, double Q2) {
      // First term
      double n1 = 0.0485;
      double d1 = log(Q2 / 0.04);
      double T1 = (n1 / d1) * Theta1998(x, Q2);
      // Second term
      double n2 = 0.5470;
      double Q4 = Q2 * Q2;
      double Q8 = Q2 * Q4;
      double p4 = pow(2.0621, 4.);
      double d2 = pow(Q8 + p4, 1. / 4.);
      double a  = n2 / d2;
      double b  = 1. - 0.3804 * x + 0.5090 * x * x;
      double T2 = a * b * pow(x, -0.0285);
      // Put it together
      double Ra = T1 + T2;
      return Ra;
    }
    double Rb1998(double x, double Q2) {
      // First term
      double n1 = 0.0481;
      double d1 = log(Q2 / 0.04);
      double T1 = (n1 / d1) * Theta1998(x, Q2);
      // Second term
      double n21 = 0.6114;
      double a   = n21 / Q2;
      double Q4  = Q2 * Q2;
      double p   = 0.3;
      double p2  = p * p;
      double n22 = 0.3509;
      double d22 = Q4 + p2;
      double b   = n22 / d22;
      double c   = 1. - 0.4611 * x + 0.7172 * x * x;
      double T2  = (a - b) * c * pow(x, -0.0317);
      // Put it together
      double Rb = T1 + T2;
      return Rb;
    }
    double Rc1998(double x, double Q2) {
      // First term
      double n1 = 0.0577;
      double d1 = log(Q2 / 0.04);
      double T1 = (n1 / d1) * Theta1998(x, Q2);
      // Second term
      double a  = 0.4644;
      double b  = 1.8288;
      double c  = Q2 - 12.3708 * x + 43.1043 * x * x - 41.7415 * x * x * x;
      double b2 = b * b;
      double c2 = c * c;
      double T2 = a * pow(b2 + c2, -0.5);
      // Put it together
      double Rc = T1 + T2;
      return Rc;
    }
    double Theta1998(double x, double Q2) {
      // First term
      double T1 = 1.;
      // Second term
      double a  = 12.;
      double b  = Q2 / (Q2 + 1.);
      double p  = 0.125;
      double p2 = p * p;
      double x2 = x * x;
      double c  = p2 / (p2 + x2);
      double T2 = a * b * c;
      // Put it together
      double Theta = T1 + T2;
      return Theta;
    }

    double BeamDepol(double E1, double E2, double Z) {
      using namespace TMath;
      double res = (2 * Power(E1 - E2, 2) *
                    (-0.6666666666666666 +
                     4 * (-1.202 * Z + 1.0369 * Power(Z, 2) - (1.008 * Power(Z, 3)) / (1 + Z) +
                          Log(183 / Power(Z, 0.3333333333333333))))) /
                   (3. * (4 * (Power(E1, 2) + Power(E2, 2)) *
                              (-1.202 * Z + 1.0369 * Power(Z, 2) - (1.008 * Power(Z, 3)) / (1 + Z) +
                               Log(183 / Power(Z, 0.3333333333333333))) -
                          (2 * E1 * E2 *
                           (-0.6666666666666666 + 4 * (-1.202 * Z + 1.0369 * Power(Z, 2) -
                                                       (1.008 * Power(Z, 3)) / (1 + Z) +
                                                       Log(183 / Power(Z, 0.3333333333333333))))) /
                              3.));
      return res;
    }

    double F_tilde_internal(double Es, double Ep, double theta) {
      using namespace TMath;
      using namespace insane::constants;
      double alpha_over_pi = fine_structure_const / pi;
      double Q2            = 4.0 * Es * Ep * Power(Sin(theta / 2.0), 2.0);
      double res           = 1.0;
      res +=
          2.0 * alpha_over_pi * (-14.0 / 9.0 + 13.0 / 12.0 * Log(Q2 / ((M_e / GeV) * (M_e / GeV))));
      res += -0.5 * alpha_over_pi * (Power(Log(Es / Ep), 2.0));
      res += alpha_over_pi * (pi * pi / 6.0 - DiLog(Power(Cos(theta / 2.0), 2.0)));
      return res;
    }

  } // namespace kine

  double alpha_CT(double Q2) {
    // color transparency parametrized from simple take off from
    // http://inspirehep.net/record/1202504 T = A^(alpha-1) alpha = 0.00857*Q2 + 0.771
    return (0.00857 * Q2 + 0.771);
  }

  namespace dvcs {
    double epsilon(double Q2, double xB, double M) {
      return insane::kine::epsilon_xQ2(xB, Q2,M);
      // return 2.0*xB*M/TMath::Sqrt(Q2);
    }

    double Delta2_min(double Q2, double xB, double M) {
      double eps = epsilon(Q2, xB, M);
      double num = -Q2 * (2.0 * (1.0 - xB) * (1.0 - TMath::Sqrt(1.0 + eps * eps)) + eps * eps);
      double den = 4.0 * xB * (1.0 - xB) + eps * eps;
      return num / den;
    }

    double Delta2_min2(double Q2, double xB, double M) {
      double eps = epsilon(Q2, xB, M);
      double num =
          -Q2 * (2.0 * (1.0 - xB) + eps * eps - 2.0 * TMath::Sqrt(1.0 + eps * eps) * (1.0 - xB));
      double den = 4.0 * xB * (1.0 - xB) + eps * eps;
      return num / den;
    }
    double Delta2_max(double Q2, double xB, double M) {
      double eps = epsilon(Q2, xB, M);
      double num =
          -Q2 * (2.0 * (1.0 - xB) + eps * eps + 2.0 * TMath::Sqrt(1.0 + eps * eps) * (1.0 - xB));
      double den = 4.0 * xB * (1.0 - xB) + eps * eps;
      return num / den;
    }

    double tmin(double Q2, double xB, double M) {
      const double gamma   = 2.0 * xB * M / std::sqrt(Q2);
      const double sqrtgam = std::sqrt(1.0 + gamma * gamma);
      return Q2 * (1.0 - sqrtgam + gamma * gamma / 2.0) /
             (xB * (1.0 - sqrtgam + gamma * gamma / (2.0 * xB)));
    }

    double y_max(double eps) { return 2.0 * (TMath::Sqrt(1.0 + eps * eps) - 1.0) / eps * eps; }

    double Delta2_perp(double xi, double Delta2, double D2m) {
      // approximate form
      return (1.0 - xi * xi) * (Delta2 - D2m);
    }

    double xi_BKM(double xB, double Q2, double Delta2) {
      return (xB * (1.0 + Delta2 / (2.0 * Q2)) / (2.0 - xB + xB * Delta2 / Q2));
    }

    double eta_BKM(double xi, double Q2, double Delta2) {
      return (-xi / (1.0 - Delta2 / (2.0 * Q2)));
    }
  } // namespace dvcs

  namespace Phys {

    double A_pair_corr(double A, double Apair, double Rpair) {
      using namespace TMath;
      double res = (-2.0 * Apair * Rpair) / (-2.0 + Rpair) + (A * (2.0 + Rpair)) / (2 - Rpair);
      return (res);
    }

    double delta_A_pair_corr(double A, double delta_A, double Apair, double delta_Apair,
                             double Rpair, double delta_Rpair) {
      // Systematic error propagation for pair symmetric background correction.
      using namespace TMath;
      double res = (16 * Power(A + Apair, 2) * Power(delta_Rpair, 2) +
                    Power(-2 + Rpair, 2) * (4 * Power(delta_Apair, 2) * Power(Rpair, 2) +
                                            Power(delta_A, 2) * Power(2 + Rpair, 2))) /
                   Power(-2 + Rpair, 4);
      // double res = (4*Power(A - Apair,2)*Power(delta_Rpair,2) + Power(1 +
      // Rpair,2)*(4*Power(delta_Apair,2)*Power(Rpair,2) + Power(delta_A + 3*delta_A*Rpair,2)))/
      //      Power(1 + Rpair,4);
      return (Sqrt(res));
    }

    double A_elastic_sub_corr(double A, double SigmaEl, double SigmaIn, double DelEl) {
      using namespace TMath;
      double res = -(DelEl / SigmaIn) + (A * (SigmaEl + SigmaIn)) / SigmaIn;
      return (res);
    }
    double delta_A_elastic_sub_corr(double A, double deltaA, double SigmaEl, double deltaSigmaEl,
                                    double SigmaIn, double deltaSigmaIn, double DelEl,
                                    double deltaDelEl) {
      // Systematic error for elastic radiative tail correction
      using namespace TMath;
      double res =
          (Power(deltaSigmaIn, 2) * Power(DelEl - A * SigmaEl, 2) +
           (Power(deltaDelEl, 2) + Power(A, 2) * Power(deltaSigmaEl, 2)) * Power(SigmaIn, 2) +
           Power(deltaA, 2) * Power(SigmaIn, 2) * Power(SigmaEl + SigmaIn, 2)) /
          Power(SigmaIn, 4);
      return (Sqrt(res));
    }

    // uncertainties
    double delta_g1(double x, double Q2, double E0, double R, double deltaR, double F1,
                    double deltaF1, double Apara, double deltaApara, double Aperp,
                    double deltaAperp) {

      using namespace TMath;
      double nu   = Q2 / (2.0 * (M_p / GeV) * x);
      double GAM2 = Q2 / (nu * nu);
      double Ep   = insane::kine::Eprime_xQ2y(x, Q2, nu / E0);
      double th   = insane::kine::Theta_xQ2y(x, Q2, nu / E0);
      double eps  = insane::kine::epsilon(E0, Ep, th);
      double aD   = (1.0 / E0) * (E0 - eps * Ep);

      double eta   = insane::kine::Eta(E0, Ep, th);
      double xi    = insane::kine::Xi(E0, Ep, th);
      double dg1_2 = (Power(deltaAperp, 2) * (1 + eps) * Power(F1, 2) *
                      Power(-eta + Sqrt(GAM2), 2) * Power(1 + eps * R, 2)) /
                         (2. * Power(aD, 2) * eps * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2)) +
                     (Power(deltaApara, 2) * Power(F1, 2) * Power(1 + eps * R, 2) *
                      Power(1 + Sqrt(GAM2) * xi, 2)) /
                         (Power(aD, 2) * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2)) +
                     (Power(deltaR, 2) * eps * (1 + eps) * Power(F1, 2) *
                      Power(Sqrt(2) * Aperp * (-eta + Sqrt(GAM2)) +
                                2 * Apara * Sqrt(eps / (1 + eps)) * (1 + Sqrt(GAM2) * xi),
                            2)) /
                         (4. * Power(aD, 2) * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2)) +
                     (Power(deltaF1, 2) * (1 + eps) * Power(1 + eps * R, 2) *
                      Power(Sqrt(2) * Aperp * (-eta + Sqrt(GAM2)) +
                                2 * Apara * Sqrt(eps / (1 + eps)) * (1 + Sqrt(GAM2) * xi),
                            2)) /
                         (4. * Power(aD, 2) * eps * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2));
      return (Sqrt(dg1_2));
    }

    double delta_g2(double x, double Q2, double E0, double R, double deltaR, double F1,
                    double deltaF1, double Apara, double deltaApara, double Aperp,
                    double deltaAperp) {

      using namespace TMath;
      double nu   = Q2 / (2.0 * (M_p / GeV) * x);
      double GAM2 = Q2 / (nu * nu);
      double Ep   = insane::kine::Eprime_xQ2y(x, Q2, nu / E0);
      double th   = insane::kine::Theta_xQ2y(x, Q2, nu / E0);
      double eps  = insane::kine::epsilon(E0, Ep, th);
      double aD   = (1.0 / E0) * (E0 - eps * Ep);

      double eta = insane::kine::Eta(E0, Ep, th);
      double xi  = insane::kine::Xi(E0, Ep, th);
      double dg2_2 =
          (Power(deltaAperp, 2) * (1 + eps) * Power(F1, 2) * Power(1 + eta * Sqrt(GAM2), 2) *
           Power(1 + eps * R, 2)) /
              (2. * Power(aD, 2) * eps * GAM2 * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2)) +
          (Power(deltaApara, 2) * Power(F1, 2) * Power(1 + eps * R, 2) *
           Power(-Sqrt(GAM2) + xi, 2)) /
              (Power(aD, 2) * GAM2 * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2)) +
          (Power(deltaR, 2) * eps * (1 + eps) * Power(F1, 2) *
           Power(Sqrt(2) * Aperp * (1 + eta * Sqrt(GAM2)) +
                     2 * Apara * Sqrt(eps / (1 + eps)) * (-Sqrt(GAM2) + xi),
                 2)) /
              (4. * Power(aD, 2) * GAM2 * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2)) +
          (Power(deltaF1, 2) * (1 + eps) * Power(1 + eps * R, 2) *
           Power(Sqrt(2) * Aperp * (1 + eta * Sqrt(GAM2)) +
                     2 * Apara * Sqrt(eps / (1 + eps)) * (-Sqrt(GAM2) + xi),
                 2)) /
              (4. * Power(aD, 2) * eps * GAM2 * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2));
      return (Sqrt(dg2_2));
    }


  } // namespace Phys




} // namespace insane

