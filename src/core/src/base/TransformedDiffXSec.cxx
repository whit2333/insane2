#include "insane/base/TransformedDiffXSec.h"

//#include "InclusiveDiffXSec.h"


namespace insane {
  namespace physics {

    //void xs_transform_test() {

    //  // Q2 as a function of x and y
    //  auto Q2_func = [=](const InitialState& is, const std::array<double,2>& vars){
    //    double s = is.s();
    //    double m2= is.p2().M2();
    //    double xv = vars[0];
    //    double yv = vars[1];
    //    return (s-m2)*xv*yv; };

    //  // x as a function of x (trivial)
    //  auto x_func = [=](const InitialState& is, const std::array<double,2>& vars){
    //    double xv = vars[0];
    //    return xv; };

    //  // y as a function  of x and Q2
    //  auto y_func = [=](const InitialState& is, const std::array<double,2>& vars){
    //    double s = is.s();
    //    double m2= is.p2().M2();
    //    double xv = vars[0];
    //    double Q2v = vars[1];
    //    return Q2v/((s-m2)*xv); };

    //  KinematicRelationFunction<decltype(y_func)>  k1(y_func);

    //  PSV<> v1("v1"), v2("v2");
    //  PSV<KinematicRelationFunction<decltype(y_func)>> v3(y_func);

    //  PSV<> y2("y2");
    //  PSV<KinematicRelationFunction<decltype(Q2_func)>> y3(Q2_func);


    //  auto diff2        = NDiff<2,decltype(v1),decltype(v2),decltype(v3)>(v1, v2, v3);
    //  auto diff3        = NDiff<2,decltype(v1),decltype(y2),decltype(y3)>(v1, y2, y3);

    //  auto phase_space  = PS<decltype(diff2),decltype(v3)>(diff2, v3);
    //  
    //  InitialState init_state(12.0,0.938);

    //  auto xs = CrossSection<decltype(phase_space),2>(phase_space);

    //  double Q2_0 = 5.0;
    //  double x_0  = 0.4;
    //  double y_0  = Q2_0/(init_state.s()-(0.938*0.938))/x_0;
    //  std::cout << "     Q2_0 " << Q2_0 << "\n";
    //  std::cout << "      x_0 " << x_0  << "\n";
    //  std::cout << "      y_0 " << y_0  << "\n";
    //  std::cout << " Q2_0/y_0 " << Q2_0/y_0 << "\n";

    //  std::cout << " XS = "  << xs(init_state, {x_0,Q2_0}) << std::endl;

    //  Jacobian<decltype(diff2), decltype(diff3),decltype(x_func), decltype(Q2_func)> jm(diff2, diff3,
    //                                                                                    x_func, Q2_func);

    //  std::cout <<  " DET = " << jm.Det(init_state, {x_0,Q2_0}) << std::endl;

    //  InitialState init_state2(12.0,0.938);

    //  auto xs2 = make_jacobian_transform(xs, init_state, jm, phase_space);

    //  std::cout << " XS2= "  << xs2(init_state2, {x_0,y_0}) << std::endl;

    //  // ----------------------------------------------------------------------
    //  auto xs0 = make_cross_section( make_phase_space( make_diff(v1,v2), v3) );

    //  auto jm0 = make_jacobian(make_diff(v1,v2),
    //                           make_diff(v1,y2),
    //                           x_func,
    //                           Q2_func );

    //  auto xs0_prime = make_jacobian_transform(xs0,
    //                                           init_state,
    //                                           jm0,
    //                                           make_phase_space( make_diff(v1,y2), y3) );

    //  auto xs1_prime = make_jacobian_transform(make_cross_section( make_phase_space( make_diff(v1,v2), v3) ), 
    //                                           init_state, 
    //                                           std::make_tuple(x_func, Q2_func), 
    //                                           make_phase_space( make_diff(v1,y2), y3) );

    //  std::cout << " XS0= "  << xs0_prime(init_state2, {x_0,y_0}) << std::endl;
    //  std::cout << " XS1= "  << xs1_prime(init_state2, {x_0,y_0}) << std::endl;

    //}
    ////______________________________________________________________________________

    //void xs_transform_test2() {

    //  // Q2 as a function of x and y
    //  auto Q2_func = [=](const InitialState& is, const std::array<double,2>& vars){
    //    double s = is.s();
    //    double m2= is.p2().M2();
    //    double xv = vars[0];
    //    double yv = vars[1];
    //    return (s-m2)*xv*yv; };

    //  // x as a function of x (trivial)
    //  auto x_func = [=](const InitialState& is, const std::array<double,2>& vars){
    //    double xv = vars[0];
    //    return xv; };

    //  // y as a function  of x and Q2
    //  auto y_func = [=](const InitialState& is, const std::array<double,2>& vars){
    //    double s = is.s();
    //    double m2= is.p2().M2();
    //    double xv = vars[0];
    //    double Q2v = vars[1];
    //    return Q2v/((s-m2)*xv); };

    //  //  as a function of x and nu
    //  auto nu_func = [=](const InitialState& is, const std::array<double,2>& vars){
    //    double s = is.s();
    //    double m2= is.p2().M2();
    //    double xv = vars[0];
    //    double yv = vars[1];
    //    double Q2 = (s-m2)*xv*yv;
    //    return Q2/(2.0*TMath::Sqrt(m2)*xv);
    //  };

    //  PSV<Invariant> v1(Invariant::x , "x" );
    //  PSV<Invariant> v2(Invariant::Q2, "Q2");
    //  IPSV nu("nu");
    //  PSV<KinematicRelationFunction<decltype(y_func)>> v3(y_func ,"y");

    //  IPSV y2("y");
    //  PSV<KinematicRelationFunction<decltype(Q2_func)>> Q2_2(Q2_func, "Q2");
    //  PSV<KinematicRelationFunction<decltype(nu_func)>> nu_2(nu_func, "nu");

    //  InitialState init_state(5.0, 50.0);
    //  InitialState boosted_state   = init_state.GetNewFrame(ROOT::Math::LorentzRotation(ROOT::Math::BoostZ(0.9)));
    //  InitialState unboosted_state = boosted_state.GetCMFrame();
    //  InitialState rest_state      = init_state.GetSecondaryRestFrame();

    //  init_state.Print();
    //  boosted_state.Print();
    //  unboosted_state.Print();
    //  rest_state.Print();

    //  double Q2_0 = 5.0;
    //  double x_0  = 0.4;
    //  double y_0  = Q2_0/(boosted_state.s()-(0.938*0.938))/x_0;
    //  std::cout << "        s " << boosted_state.s() << "\n";
    //  std::cout << "     Q2_0 " << Q2_0 << "\n";
    //  std::cout << "      x_0 " << x_0  << "\n";
    //  std::cout << "      y_0 " << y_0  << "\n";
    //  std::cout << " Q2_0/y_0 " << Q2_0/y_0 << "\n";

    //  InitialState init_state2(12.0,0.938);

    //  auto ps0 = make_phase_space( make_diff(v1,v2), v3);
    //  ps0.Print();

    //  auto xs0 = make_cross_section( make_phase_space( make_diff(v1,v2), v3) );

    //  auto jm0 = make_jacobian(make_diff(v1,v2),
    //                           make_diff(v1,y2),
    //                           x_func,
    //                           Q2_func );
    //  //auto jm0 = make_jacobian(make_diff(v1,v2),
    //  //                         make_diff(v1,v2),
    //  //                         x_func,
    //  //                         [=](const InitialState& is, const std::array<double,2>& vars){
    //  //                           double Q2v = vars[1];
    //  //                           return Q2v; }
    //  //                        );
    //  jm0.PrintInfo();
    //  jm0.PrintDebug(boosted_state, {x_0,y_0});

    //  auto xs0_prime = make_jacobian_transform(xs0,
    //                                           init_state,
    //                                           jm0,
    //                                           make_phase_space( make_diff(v1,y2), Q2_2) );

    //  auto xs1_prime = make_jacobian_transform(make_cross_section( make_phase_space( make_diff(v1,v2), v3) ), 
    //                                           init_state, 
    //                                           std::make_tuple(x_func, Q2_func), 
    //                                           make_phase_space( make_diff(v1,y2), Q2_2) );

    //  std::cout << " XS0= "  << xs0_prime(init_state2, {x_0,y_0}) << std::endl;
    //  std::cout << " XS1= "  << xs1_prime(init_state2, {x_0,y_0}) << std::endl;

    //}
    //______________________________________________________________________________

  }
}




