#include "insane/base/Particle.h"

namespace insane {
Particle::Particle() {
   Clear();
}
//______________________________________________________________________________
Particle::~Particle() {  }
//______________________________________________________________________________
//Particle::Particle(const Particle& rhs):TParticle(rhs) {
//   (*this) = rhs;
//}
////______________________________________________________________________________
//Particle& Particle::operator=(const Particle &rhs) {
//   if(this!=&rhs) {
//      TParticle::operator=(rhs);
//      fHelicity = rhs.fHelicity;
//      fMomentum4Vector = rhs.fMomentum4Vector;
//   }
//   return(*this);
//}
//______________________________________________________________________________
void Particle::CalculateVectors() {
   fMomentum4Vector.SetXYZT(Px(), Py(), Pz(), Energy());
}
//______________________________________________________________________________
void Particle::Clear(Option_t * opt ) {
   TParticle::Clear(opt);
   fHelicity = 0;
   fXSId = 0;
   fMomentum4Vector.SetXYZT(0.0, 0.0, 0.0, 0.0);
}
//______________________________________________________________________________

}
