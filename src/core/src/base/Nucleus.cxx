#include "insane/base/Nucleus.h"

#include "TGeoManager.h"
#include "TGeoElement.h"
#include "insane/base/SystemOfUnits.h"

using namespace insane::units;

namespace insane {

Nucleus * Nucleus::fgProton   = nullptr;
Nucleus * Nucleus::fgNeutron  = nullptr;
Nucleus * Nucleus::fgDeuteron = nullptr;
Nucleus * Nucleus::fgHe3      = nullptr;
Nucleus * Nucleus::fgHe4      = nullptr;
Nucleus * Nucleus::fgTriton   = nullptr;
Nucleus * Nucleus::fgFe56     = nullptr;
Nucleus * Nucleus::fgC12      = nullptr;

const Nucleus& Nucleus::Proton() {
  // We use a definition standard (set here) to set a new obect
  if (!fgProton) {
    fgProton = new Nucleus(1, 1);
    fgProton->SetType(kProton);
  }
  return *fgProton;
}

const Nucleus& Nucleus::Neutron() {
  // We use a definition standard (set here) to set a new obect
  if (!fgNeutron) {
    fgNeutron = new Nucleus(0, 1);
    fgNeutron->SetType(kNeutron);
  }
  return *fgNeutron;
}

const Nucleus& Nucleus::Deuteron() {
  // We use a definition standard (set here) to set a new obect
  if (!fgDeuteron) {
    fgDeuteron = new Nucleus(1, 2);
    fgDeuteron->SetType(kDeuteron);
  }
  return *fgDeuteron;
}
//______________________________________________________________________________

const Nucleus &  Nucleus::He3(){
   // We use a definition standard (set here) to set a new obect
   if(!fgHe3){
      fgHe3 = new Nucleus(2,3);
      fgHe3->SetType(k3He);
   }
   return *fgHe3;
}
//______________________________________________________________________________

const Nucleus &  Nucleus::He4(){
   // We use a definition standard (set here) to set a new obect
   if(!fgHe4){
      fgHe4 = new Nucleus(2,4);
      fgHe4->SetName("He-4"); 
      fgHe4->SetMass(3.728);               // 4.0026 amu  
      fgHe4->SetBindingEnergy(0.0283);     // 28.3 MeV  what? 
   }
   return *fgHe4;
}
//______________________________________________________________________________

const Nucleus &  Nucleus::C12(){
   // We use a definition standard (set here) to set a new obect
   if(!fgC12){
      fgC12 = new Nucleus(6,12);
      fgC12->SetName("12C"); 
      fgC12->SetMass(11.18);               // 12.010 amu   
      fgC12->SetBindingEnergy(0.00768);     // per nucleon/
   }
   return *fgC12;
}
//______________________________________________________________________________

const Nucleus &  Nucleus::Fe56(){
   // We use a definition standard (set here) to set a new obect
   // Numbers from http://hyperphysics.phy-astr.gsu.edu/hbase/pertab/fe.html
   if(!fgFe56){
      fgFe56 = new Nucleus(26,56);
      fgFe56->SetName("Iron-56"); 
      fgFe56->SetMass(52.0902); 
      fgFe56->SetBindingEnergy(0.49226);   
   }
   return *fgFe56;
}
//______________________________________________________________________________

double Nucleus::CalcMass()
{
   double M = 0.0;
   if( (fZ==1)&&(fA==1)&&(fL==0) ) {
      M = double(fZ)*M_p/GeV + double(fA-fZ)*M_n/GeV ;
   } else if( (fZ==0)&&(fA==1)&&(fL==0) ) {
      M = double(fZ)*M_p/GeV + double(fA-fZ)*M_n/GeV ;
   } else {
      if(!gGeoManager) {
         gGeoManager = new TGeoManager("geomanager","geomanager");
      }
      TGeoElementTable *table = gGeoManager->GetElementTable();
      fElementRN = table->GetElementRN(fA,fZ);
      if(fElementRN) {
         M = (fElementRN->MassNo()*insane::units::amu_c2 + fElementRN->MassEx())/insane::units::GeV;
      } else {
         Warning("CalcMass","Nucleus(Z=%d,A=%d) not found!",fZ,fA);
         M = double(fZ)*M_p/GeV + double(fA-fZ)*M_n/GeV ;
      }
   }
   return M;
}
//______________________________________________________________________________
      
int    Nucleus::CalcPdgCode() const
{
   int code = 0;
   if( (fZ==1)&&(fA==1)&&(fL==0) ) {
      code = 2212;
   } else if( (fZ==0)&&(fA==1)&&(fL==0) ) {
      code = 2112;
   } else {
      //     10LZZZAAAI
      code = 1000000000
           +   10000000*fL
           +      10000*fZ
           +         10*fA;
   }
   return code;
}
//______________________________________________________________________________

Nucleus::Nucleus(Int_t Z, Int_t A, Int_t L) : fZ(Z), fA(A), fL(L)
{
   SetNameTitle("nucleus","a nucleus");
   fType  = kOther;
   fN     = fA - fZ - fL;
   fDelta = 0.001;
   fs     = 0.5;
   fm_a   = fZ*M_p/GeV + fN*M_n/GeV - fA*fDelta;
   fP_p   = 0.0;
   fP_n   = 0.0;
}
//______________________________________________________________________________
//Nucleus& Nucleus::operator=(const Nucleus& rhs){
//   if (this == &rhs)      // Same object?
//      return *this;       // Yes, so skip assignment, and just return *this.
//   SetNameTitle(rhs.GetName(),rhs.GetTitle());
//   fType  = rhs.fType;
//   fN     = rhs.fN;
//   fL     = rhs.fL     ;
//   fA     = rhs.fA     ;
//   fZ     = rhs.fZ     ;
//   fDelta = rhs.fDelta ;
//   fs     = rhs.fs     ;
//   fm_a   = rhs.fm_a     ;
//   fP_p   = rhs.fP_p   ;
//   fP_n   = rhs.fP_n   ;
//   return *this;
//}
//______________________________________________________________________________

Nucleus&   Nucleus::operator+=(const Nucleus& rhs)
{
   fType  = kOther;
   fN     += rhs.fN;
   fL     += rhs.fL     ;
   fZ     += rhs.fZ     ;
   fA     += rhs.fA     ;
   fDelta = 0.001;
   fs     = 0.5;
   fm_a   = fZ*M_p/GeV + fN*M_n/GeV - fA*fDelta;
   fP_p   = 0.0;
   fP_n   = 0.0;
   fPdgCode = CalcPdgCode();
   return *this;
}
//__________________________________________________________________________

Nucleus&   Nucleus::operator-=(const Nucleus& rhs)
{
   fType  = kOther;
   fN     -= rhs.fN;
   fL     -= rhs.fL     ;
   fZ     -= rhs.fZ     ;
   fA     -= rhs.fA     ;
   fDelta = 0.001;
   fs     = 0.5;
   fm_a   = fZ*M_p/GeV + fN*M_n/GeV - fA*fDelta;
   fP_p   = 0.0;
   fP_n   = 0.0;
   fPdgCode = CalcPdgCode();
   return *this;
}
//__________________________________________________________________________

const Nucleus Nucleus::operator+(const Nucleus &other) const 
{ 
   Nucleus result = *this;
   result += other;
   return result;
}
//______________________________________________________________________________

const Nucleus Nucleus::operator-(const Nucleus &other) const
{ 
   Nucleus result = *this;
   result -= other;
   return result;
}
//______________________________________________________________________________

bool Nucleus::operator==(const Nucleus& other) const {
   if( fN       != other.GetN() ) return false ;
   if( fZ       != other.GetZ() ) return false ;
   if( fA       != other.GetA() ) return false ;
   if( fL       != other.GetL() ) return false ;
   if( fPdgCode != other.GetPdgCode() ) return false ;
   return true;
}
//______________________________________________________________________________

void Nucleus::SetType(NucleusType i)
{
   ///\deprecated Do not use when defining nuclei. 
   /// DO NOT ADD TO THIS!!!!!
   /// Put everything in static method. 
   /// this just exists because of historical reasons and should be removed.

   fType = i;

   switch(fType){
      case kProton:  
         fName  = Form("Proton");
         fA     = 1;
         fZ     = 1;
         fL     = 0; 
         fs     = 1.0/2.0;
         fP_p   = 0.0;
         fP_n   = 0.0;
         fm_a   = M_p/GeV;
         fDelta = 1.0;
         break;
      case kNeutron:  
         fName  = Form("Neutron");
         fA     = 1;
         fZ     = 0;
         fL     = 0; 
         fs     = (1./2.);
         fP_p   = 0.;
         fP_n   = 0.;
         fm_a   = M_n/GeV;
         fDelta = 1.0;
         break;
      case kDeuteron:  
         fName  = Form("Deuteron");
         fA     = 2;
         fZ     = 1;
         fL     = 0;
         fs     = 1.;
         fP_p   = 0.; /// \todo Add correct values for effective polarization of p,n in deuteron  
         fP_n   = 0.;
         fm_a   = M_d/GeV;
         fDelta = 1.0;
         break;
      case k3He:   
         fName  = Form("Helium-3");
         fA     = 3;
         fZ     = 2;
         fL     = 0;
         fs     = 1./2.;
         fP_p   = -0.027;             /// Numbers from Phys Rev C 42, 2310–2314 (1990) 
         fP_n   =  0.87; 
         fm_a     = M_3He/GeV;
         fDelta = 1.0;
         break;
      case k56Fe: 
         fName  = Form("Iron-56");
         fA     = 56; 
         fZ     = 26; 
         fL     = 0;
         fs     = 0.;
         fP_p   = 0.;  
         fP_n   = 0.;  
         fm_a   = 52.0902;          /// Numbers from http://hyperphysics.phy-astr.gsu.edu/hbase/pertab/fe.html  
         fDelta = 0.49226;    
         break; 
         /// \todo Add generic Z, A, mass calc
      default:
         std::cout << "[Nucleus::SetType]: Invalid target!  Exiting..." << std::endl;
         exit(1);
   }

   fN = fA - fZ - fL;  

}
//______________________________________________________________________________

void Nucleus::Print(const Option_t * opt) const
{
   // option S shrinks and doesn't print extra space in first 6 columns
   bool space = false;
   if(strcmp("S",opt) || strcmp("s",opt)) space = true;

   //std::cout << "----------------- Nucleus Parameters -----------------" << std::endl;
   std::cout << "             Nucleus  : " << GetName() << std::endl;;
   //std::cout << "Name:   " << fName << std::endl;
   if(space) std::cout << "             ";
   std::cout << "      A  = " << fA    << std::endl;
   if(space) std::cout << "             ";
   std::cout << "      Z  = " << fZ    << std::endl;
   if(space) std::cout << "             ";
   std::cout << "      N  = " << fN    << std::endl;
   if(space) std::cout << "             ";
   std::cout << "      L  = " << fL    << std::endl;
   if(space) std::cout << "             ";
   std::cout << "      M  = " << fm_a    << std::endl;
   if(space) std::cout << "             ";
   std::cout << "   spin  = " << fs    << std::endl;
   if(space) std::cout << "             ";
   std::cout << "   P_p   = " << fP_p  << std::endl;
   if(space) std::cout << "             ";
   std::cout << "   P_n   = " << fP_n  << std::endl;
   if(space) std::cout << "             ";
   std::cout << "  DeltaE = " << fDelta << std::endl;
   if(space) std::cout << "             ";
   std::cout << " PdgCode = " << fPdgCode << std::endl;
   //std::cout << "------------------------------------------------------" << std::endl;

}
//______________________________________________________________________________

void Nucleus::Print(std::ostream& stream) const
{
   // option S shrinks and doesn't print extra space in first 6 columns
   bool space = true;

   //std::cout << "----------------- Nucleus Parameters -----------------" << std::endl;
   stream << "             Nucleus  : " << GetName() << std::endl;
   //stream << "Name:   " << fName << std::endl;
   if(space) stream << "             ";
   stream << "      A  = " << fA    << std::endl;
   if(space) stream << "             ";
   stream << "      Z  = " << fZ    << std::endl;
   if(space) stream << "             ";
   stream << "      N  = " << fN    << std::endl;
   if(space) stream << "             ";
   stream << "      L  = " << fL    << std::endl;
   if(space) stream << "             ";
   stream << "      M  = " << fm_a    << std::endl;
   if(space) stream << "             ";
   stream << "   spin  = " << fs    << std::endl;
   if(space) stream << "             ";
   stream << "   P_p   = " << fP_p  << std::endl;
   if(space) stream << "             ";
   stream << "   P_n   = " << fP_n  << std::endl;
   if(space) stream << "             ";
   stream << "  DeltaE = " << fDelta << std::endl;
   if(space) stream << "             ";
   stream << " PdgCode = " << fPdgCode << std::endl;
   //stream << "------------------------------------------------------" << std::endl;

}
//______________________________________________________________________________

}

