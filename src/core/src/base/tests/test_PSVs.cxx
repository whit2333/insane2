#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <typeinfo>

#include "insane/base/PhaseSpaceVariables.h"
#include "insane/base/InitialState.h"

#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"

SCENARIO( "Kinematic relations", "[KinematicRelationFunction]" ) {

  using namespace insane::physics;
  using namespace insane::helpers;
  double eps = 1.0e-7;
  double P1 = 5.0;
  double m1 = 0.000511;
  double P2 = 0.0;
  double m2 = 0.938;
  double E1 = std::sqrt(P1*P1+m1*m1);
  double E2 = std::sqrt(P2*P2+m2*m2);
  //double s_calc = m2*m2 + m1*m1 + 2.0*E1*E2;
  
  InitialState init_state(P1, P2, m2);
  double Q2_0 = 5.0;
  double x_0  = 0.4;
  double y_0  = Q2_0/(init_state.s()-(0.938*0.938))/x_0;
  std::cout << "     Q2_0 " << Q2_0 << "\n";
  std::cout << "      x_0 " << x_0  << "\n";
  std::cout << "      y_0 " << y_0  << "\n";
  std::cout << " Q2_0/y_0 " << Q2_0/y_0 << "\n";
  std::array<double,2> vars = {x_0,y_0};

  GIVEN( "a lambda returning four vector" ) {
    // Q2 as a function of x and y
    auto Q2_func = [=](const InitialState& is, const std::array<double,2>& vars){
      double s = is.s();
      double m2= is.p2().M2();
      double xv = vars[0];
      double yv = vars[1];
      return (s-m2)*xv*yv; };

    WHEN( "the signature is (const InitialState& is, const std::array<double,1>& x)" ) {
      THEN( "KinematicRelationFunction can be constructed" ) {
        KinematicRelationFunction<decltype(Q2_func)> kine_rel(Q2_func);
        REQUIRE( std::abs(kine_rel(init_state,vars)-5.0) < eps  );
      }
      THEN( "the KinematicRelationFunction helper works" ) {
        auto kine_rel = make_kinematic_relation(Q2_func);
        REQUIRE( std::abs(kine_rel(init_state,vars)-5.0) < eps  );
      }

    }
  }
}


SCENARIO( "PSV", "[PSV]" ) {
  using namespace insane::physics;
  using namespace insane::helpers;
  double eps = 1.0e-7;

  using namespace insane::physics::variables;

  Energy  E1 = 4.6_GeV;
  Energy  E2 = 4600.0_MeV;

  //std::cout << " E1 " << E1 << std::endl;
  //std::cout << " E2 " << E2 << std::endl;

  using XS = variables::AVar< struct XS_tag >;
  XS  xs(4.5);
  std::cout << "      XS : " << xs << std::endl;

  auto squared = [](XS xx) { return xx*xx; };
  std::cout << " squared : " << squared(xs) << std::endl;

  GIVEN( "an indepdent phase space variable" ) {
    WHEN( " the limits are supplied" ) {
      IPSV ipsv({0.1,10.0},"psName","psTitle");
      //ipsv.Print();
      THEN( " the normalized var should be zero at the lower limit" ) {
        REQUIRE( std::abs( ipsv.NormalizedVar(ipsv.Min()) ) < eps );
      }
      THEN( " the normalized var should be zero at the upper limit" ) {
        REQUIRE( std::abs( ipsv.NormalizedVar(ipsv.Max()) - 1.0 ) < eps );
      }
      THEN( " the var computed  with the scaled variable min (0) should be lower limit" ) {
        REQUIRE( std::abs( ipsv.Var(0.0) - ipsv.Min()) < eps );
      }
      THEN( " the var computed  with the scaled variable max (1) should be upper limit" ) {
        REQUIRE( std::abs( ipsv.Var(1.0) - ipsv.Max()) < eps );
      }
    }
  }
}

