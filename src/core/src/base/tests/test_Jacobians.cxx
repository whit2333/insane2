#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <typeinfo>

#include "TMath.h"
#include "insane/base/PhaseSpaceVariables.h"
#include "insane/base/NFoldDifferential.h"
#include "insane/base/Jacobians.h"

#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"

SCENARIO( "DIS Differential", "[NFoldDifferential]" ) {

  INFO("The DIS cross section in the form (dsigma / dx dQ2) can be converted \n"
       "to a differential in (dx dy) with the jacobian \n"
       "d(x,Q2)/d(x,y) = dQ2/dy = (s-M^2)x=Q2/y\n"
       );

  using namespace insane::physics;
  using namespace insane::helpers;

  double test_prec = 1.0e-7;

  // Prepare the initial state
  double P1 = 5.0;
  double m1 = 0.000511;
  double E1 = std::sqrt(P1*P1+m1*m1);

  double P2 = 0.0;
  double m2 = 0.938;
  double E2 = std::sqrt(P2*P2+m2*m2);

  InitialState init_state(P1, P2, m2);

  // Final state kinematics
  double Q2_0 = 5.0;
  double x_0  = 0.4;

  double y_0  = Q2_0/(init_state.s()-(m2*m2))/x_0;

  std::cout << "     Q2_0 " << Q2_0 << "\n";
  std::cout << "      x_0 " << x_0  << "\n";
  std::cout << "      y_0 " << y_0  << "\n";
  std::cout << " |J| = d(x,Q2)/d(x,y) = Q2_0/y_0 " << Q2_0/y_0 << "\n";
  std::array<double,2> vars = {x_0,y_0};


  GIVEN( "a lambdas returning variable" ) {

    // Q2 as a function of x and y
    auto Q2_func = [=](const InitialState& is, const std::array<double,2>& vars){
      double s = is.s();
      double m2= is.p2().M2();
      double xv = vars[0];
      double yv = vars[1];
      return (s-m2)*xv*yv; };


    // x as a function of x (trivial)
    auto x_func = [=](const InitialState& is, const std::array<double,2>& vars){
      double xv = vars[0];
      return xv; };

    // x and Q2 as a function of x and y
    auto x_and_Q2_func = [=](const InitialState& is, const std::array<double,2>& vars){
      double s = is.s();
      double m2= is.p2().M2();
      double xv = vars[0];
      double yv = vars[1];
      return std::array<double,2>{xv,(s-m2)*xv*yv}; };



    // x as a function of x (trivial)
    auto simple_func = [=](const InitialState& is, const std::array<double,1>& vars){
      double xv = vars[0];
      return xv; };

    auto simple_func2 = [=](const InitialState& is, const std::array<double,2>& vars){
      double xv = vars[0];
      return xv; };
    // y as a function  of x and Q2
    auto y_func = [=](const InitialState& is, const std::array<double,2>& vars){
      double s = is.s();
      double m2= is.p2().M2();
      double xv = vars[0];
      double Q2v = vars[1];
      return Q2v/((s-m2)*xv); };

    //  as a function of x and nu
    auto nu_func = [=](const InitialState& is, const std::array<double,2>& vars){
      double s = is.s();
      double m2= is.p2().M2();
      double xv = vars[0];
      double yv = vars[1];
      double Q2 = (s-m2)*xv*yv;
      return Q2/(2.0*TMath::Sqrt(m2)*xv);
    };

    PSV<Invariant> v1(Invariant::x , "x" );
    PSV<Invariant> v2(Invariant::Q2, "Q2");
    IPSV nu("nu");
    PSV<KinematicRelationFunction<decltype(y_func)>> v3(y_func ,"y");

    IPSV y2("y");
    PSV<KinematicRelationFunction<decltype(Q2_func)>> Q2_2(Q2_func, "Q2");
    PSV<KinematicRelationFunction<decltype(nu_func)>> nu_2(nu_func, "nu");

    InitialState boosted_state   = init_state.GetNewFrame(ROOT::Math::LorentzRotation(ROOT::Math::BoostZ(0.9)));
    InitialState unboosted_state = boosted_state.GetCMFrame();
    InitialState rest_state      = init_state.GetSecondaryRestFrame();


    double Q2_0 = 5.0;
    double x_0  = 0.4;
    double y_0  = Q2_0/(boosted_state.s()-(0.938*0.938))/x_0;
    std::cout << "        s " << boosted_state.s() << "\n";
    std::cout << "     Q2_0 " << Q2_0 << "\n";
    std::cout << "      x_0 " << x_0  << "\n";
    std::cout << "      y_0 " << y_0  << "\n";
    std::cout << " Q2_0/y_0 " << Q2_0/y_0 << "\n";

    InitialState init_state2(12.0,0.938);

    auto vs = std::make_tuple(x_func, Q2_func);
    auto ds = derivatives< typename std::decay<decltype(vs)>::type >(vs);
    //auto dm = get_DM(x_func, Q2_func);

    DerivativeMatrix_impl<decltype(ds),2> dm(ds);

    auto dm2 = get_DM(vs);
    
    //auto ps0 = make_phase_space( make_diff(v1,v2), v3);
    auto ps_1 = make_phase_space(make_diff(v2));
    auto ps_2 = make_phase_space(make_diff(y2));

    auto d_1 = make_diff(v2);
    auto d_2 = make_diff(y2);

    auto jm = make_jacobian(d_1,d_2,std::make_tuple(simple_func));

    jm.PrintInfo();
    //ps0.Print();

    // 
    auto d2_0 = make_diff(v1,v2);
    auto d2_1 = make_diff(v1,y2);

    auto jm2 = make_jacobian(d2_0, d2_1,
                             std::make_tuple(x_func, Q2_func) );
    jm2.PrintInfo();

    //auto jm2_2 = make_jacobian(d2_0, d2_1,
    //                         x_and Q2_func );
    //jm2_2.PrintInfo();

    //auto xs0 = make_cross_section( make_phase_space( make_diff(v1,v2), v3) );
    //auto jm0 = make_jacobian(make_diff(v1,v2),
    
    auto const_var1 = [=](const InitialState& is, const std::array<double,2>& vars){
                               double Q2v = vars[0];
                               return Q2v; };
    auto const_var2 = [=](const InitialState& is, const std::array<double,2>& vars){
                               double Q2v = vars[1];
                               return Q2v; };
    auto jm0 = make_jacobian(make_diff(v1,v2),
                             make_diff(v1,v2),
                             std::make_tuple(const_var1,const_var2)
                            );
    jm0.PrintInfo();
    jm0.PrintDebug(boosted_state, {x_0,y_0});

    REQUIRE( std::abs(jm0.Det(boosted_state, {x_0,y_0}) - 1.0) < test_prec );

    auto jm4 = make_jacobian(make_diff(v1,v2),
                             make_diff(v1,y2),
                             std::make_tuple(x_func,
                             Q2_func ));
    double Q2_over_y = Q2_0/y_0;

    jm4.PrintDebug(init_state, {x_0,y_0});

    REQUIRE( std::abs(jm4.Det(init_state, {x_0,y_0}) - Q2_over_y) < test_prec );

    jm4.PrintDebug(boosted_state, {x_0,y_0});

    REQUIRE( std::abs(jm4.Det(boosted_state, {x_0,y_0}) - Q2_over_y) < test_prec );

    //auto xs0_prime = make_jacobian_transform(xs0,
    //                                         init_state,
    //                                         jm0,
    //                                         make_phase_space( make_diff(v1,y2), Q2_2) );

    //auto xs1_prime = make_jacobian_transform(make_cross_section( make_phase_space( make_diff(v1,v2), v3) ), 
    //                                         init_state, 
    //                                         std::make_tuple(x_func, Q2_func), 
    //                                         make_phase_space( make_diff(v1,y2), Q2_2) );

    //std::cout << " XS0= "  << xs0_prime(init_state2, {x_0,y_0}) << std::endl;
    //std::cout << " XS1= "  << xs1_prime(init_state2, {x_0,y_0}) << std::endl;

  }
}


