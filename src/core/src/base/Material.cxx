#include "insane/base/Material.h"
#include "TString.h"


namespace insane {

using namespace units;

Material::Material(std::string Name, double Z, double A, double L, double T, double Rho, double TR,
                   double X0) {
  fName = Name;
  fZ    = Z;
  fA    = A;
  fL    = L;
  fT    = T;
  fRho  = Rho;
  fTR   = TR;
  fX0   = X0;
}

Material::~Material() {}

void Material::Clear() {
  fA    = 0;
  fZ    = 0;
  fL    = 0;
  fT    = 0;
  fTR   = 0;
  fX0   = 0;
  fRho  = 0;
  fName = "Unknown";
}

void Material::Process() {

  fT = fL * fRho;

  if (fX0 > 1e-10) {
    fTR = fT / fX0;
  } else {
    std::cout << "[Material]: WARNING: X0 = 0!  Cannot calculate TR = T/X0." << std::endl;
  }
}

void Material::CalculateRadiationLength() {
  // For mixtures.  Need Z and A!
  // NOTE: The user needs to call this method on their own if they wish to use it.

  using namespace insane::constants;
  double alpha = fine_structure_const;
  double a     = alpha * fZ;
  double a2    = a * a;
  double a4    = a2 * a2;
  double a6    = a2 * a4;
  double f_Z   = a2 * (1. / (1 + a2) + 0.20206 - 0.0369 * a2 + 0.0083 * a4 - 0.002 * a6);
  double Lrad, Lrad_prime;

  if (fZ == 1) {
    Lrad       = 5.31;
    Lrad_prime = 6.144;
  } else if (fZ == 2) {
    Lrad       = 4.79;
    Lrad_prime = 5.621;
  } else if (fZ == 3) {
    Lrad       = 4.74;
    Lrad_prime = 5.805;
  } else if (fZ == 4) {
    Lrad       = 4.71;
    Lrad_prime = 5.924;
  } else {
    Lrad       = log(184.15 * pow(fZ, -1. / 3.));
    Lrad_prime = log(1194. * pow(fZ, -2. / 3.));
  }

  double T1, T2;
  if ((fZ != 0) && (fA != 0)) {
    T1  = 716.408 * fA;
    T2  = fZ * fZ * (Lrad - f_Z) + fZ * Lrad_prime;
    fX0 = 1. / (T1 * T2);
  } else {
    fX0 = 0;
  }
}
//______________________________________________________________________________________
void Material::Print() {

  std::cout << "------------------------------------------" << std::endl;
  std::cout << "Name: " << fName << std::endl;
  std::cout << "Z:    " << Form("%.2f", fZ) << " [-]" << std::endl;
  std::cout << "A:    " << Form("%.2f", fA) << " [g/mol]" << std::endl;
  std::cout << "L:    " << Form("%.2f", fL) << " [cm]" << std::endl;
  std::cout << "rho:  " << Form("%.3E", fRho) << " [g/cm^3]" << std::endl;
  std::cout << "T:    " << Form("%.3E", fT) << " [g/cm^2]" << std::endl;
  std::cout << "X0:   " << Form("%.3E", fX0) << " [g/cm^2]" << std::endl;
  std::cout << "TR:   " << Form("%.3E", fTR) << " [-]" << std::endl;
}
} // namespace insane
