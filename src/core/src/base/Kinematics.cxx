#include "insane/base/Kinematics.h"

#include <iostream>
#include <complex>
#include <cmath>
#include "TMath.h"

#include "Math/Vector3D.h"
#include "Math/Vector4D.h"
#include "Math/VectorUtil.h"

#include "Math/Transform3D.h"
#include "Math/Rotation3D.h"
#include "Math/RotationY.h"
#include "Math/RotationZ.h"

namespace insane {

  namespace kinematics {
    using namespace insane::units;

      /** \f$ \nu_0 = -Q^2 \f$  */
      double v0(double en, double enprime, double q)
      {
         return((en - enprime) * (en - enprime) - q * q);
      }
      double vL(double Q2, double q)
      {
         return((Q2 / (q * q)) * (Q2 / (q * q)));
      }
      double vT(double Q2, double q, double theta)
      {
         return(0.5 * Q2 / (q * q)  + TMath::Power(TMath::Tan(theta / 2.0), 2));
      }
      double vTT(double Q2, double q)
      {
         return((-Q2 / (q * q)) * 0.5);
      }
      double vTL(double Q2, double q, double theta)
      {
         return(-Q2 / (q * q) * TMath::Sqrt((Q2 / (q * q) + TMath::Power(TMath::Tan(theta / 2.0), 2)) / 2.0));
      }
      double vTprime(double Q2, double q, double theta)
      {
         return(TMath::Sqrt(Q2 / (q * q) + TMath::Power(TMath::Sin(theta / 2.0), 2)) * TMath::Tan(theta / 2.0));
      }
      double vTLprime(double Q2, double q, double theta)
      {
         return(-Q2 / (q * q) * TMath::Tan(theta / 2.0) / TMath::Sqrt(2.0));
      }

      //double tau(double Q2, double Mtarg)
      //{
      //   return(Q2 / (4.0 * Mtarg * Mtarg));
      //}

      //double Sig_Mott(double en, double theta)
      //{
      //   double res =  (1.0/137.0)*TMath::Cos(theta/2.0)/(2.0*en*TMath::Power(TMath::Sin(theta/2.0),2.0));
      //   return res*res;
      //}

      //double fRecoil(double en, double theta, double Mtarg)
      //{
      //   return(1.0 + 2.0 * en * TMath::Power(TMath::Sin(theta / 2.0), 2) / (Mtarg));
      //}


      ElasticKinematics::ElasticKinematics(double Ee, double Ep, variables::MomentumTransfer Qsq)
          : E_e(Ee), E_p(Ep), Q2(Qsq.get())
      {
    using namespace insane::units;
        double k0    = E_e;
        m2    = M_p * M_p / (GeV * GeV);
        me2   = 0.000511 * 0.000511;
        tau          = Q2 / (4.0 * m2);
        P_p          = std::sqrt(std::abs(E_p * E_p - m2));
        P_e          = std::sqrt(std::abs(E_e * E_e - me2));
        xi           = E_e * E_p + E_e * P_p;
        s            = m2 + me2 + 2.0 * xi;
        E_tot        = E_e + E_p;
        P_tot        = std::abs(P_e - P_p);
        kp           = k0 + ((P_p - k0) * Q2) / (2.0 * k0 * (E_p + P_p));
        double costh = -1.0 * (2.0 * k0 * k0 * (E_p + P_p) - (E_p + k0) * Q2) /
                       (2.0 * k0 * k0 * (E_p + P_p) + (P_p - k0) * Q2);
        theta_e      = std::acos(-costh);
        theta_e_p    = pi - theta_e;
        cos_th       = std::cos(theta_e);

        P_p_prime    = std::sqrt(kp * kp + P_tot * P_tot - 2.0 * P_tot * kp * cos_th);
        theta_p      = std::asin(kp*std::sin(theta_e)/P_p_prime);
        //theta_p      = std::acos((kp * (-1.0*costh) - P_tot) / P_p_prime);
        double sinth = std::sin(theta_e);
        sin_th       = sinth;
        double a     = E_tot * E_tot - P_tot * P_tot * costh * costh;
        double b     = (me2 + xi) * P_tot * costh;
        double sroot = std::sqrt(xi * xi - me2 * (m2 + P_tot * P_tot * sinth * sinth));
        kp_check     = (b + E_tot * sroot) / a;

        // momentum transfer
        omega        = E_e - kp;
        P_q          = std::sqrt(kp*kp + P_e*P_e  + 2.0*kp*P_e*costh); //(Q2 + omega);
        P_q2         = std::sqrt(Q2 + omega*omega);
        theta_q      = std::asin(kp*sinth / P_q2);
        theta_q_p    = pi - theta_q;

        beta_e = P_e/E_e;
        beta_p = P_p/E_p;

        tilde_epsilon                = xi / m2;
        double t0                    = ((E_e + kp) * E_p + (E_e + kp * cos_th) * P_p) / (2.0 * m2);
        V0                           = 4.0 * m2 * (t0 * t0 - tau * (1.0 + tau));
        tan2theta2_prime             = Q2 / V0;
        double Epsilon_prime_inverse = (1.0 + 2.0 * (1.0 + tau) * tan2theta2_prime);
        tilde_Epsilon_prime          = 1.0 / Epsilon_prime_inverse;
      }

      ElasticKinematics::ElasticKinematics(double Ee, double Ep, variables::Theta th)
          : E_e(Ee), E_p(Ep), theta_e(th.get())
      {
    using namespace insane::units;
        double k0    = E_e;
        m2    = M_p * M_p / (GeV * GeV);
        me2   = 0.000511 * 0.000511;

        P_p          = std::sqrt(std::abs(E_p * E_p - m2));
        P_e          = std::sqrt(std::abs(E_e * E_e - me2));
        xi           = E_e * E_p + E_e * P_p;
        s            = m2 + me2 + 2.0 * xi;
        E_tot        = E_e + E_p;
        P_tot        = std::abs(P_e - P_p);

        double sinth  = std::sin(theta_e);
        sin_th       = sinth;
        cos_th       = std::cos(theta_e);
        double a     = E_tot * E_tot - P_tot * P_tot * cos_th * cos_th;
        double b     = (me2 + xi) * P_tot * cos_th;
        double sroot = std::sqrt(xi * xi - me2 * (m2 + P_tot * P_tot * sinth * sinth));
        kp           = (b + E_tot * sroot) / a;
        Q2           = 2.0*kp*E_e*(1.0-cos_th);
        tau          = Q2 / (4.0 * m2);
        kp_check     = k0 + ((P_p - k0) * Q2) / (2.0 * k0 * (E_p + P_p));
        theta_e_p    = pi - theta_e;

        P_p_prime    = std::sqrt(kp * kp + P_tot * P_tot - 2.0 * P_tot * kp * cos_th);
        theta_p      = std::asin(kp*std::sin(theta_e)/P_p_prime);

        // momentum transfer
        omega        = E_e - kp;
        P_q          = std::sqrt(kp*kp + P_e*P_e  - 2.0*kp*P_e*cos_th); //(Q2 + omega);
        P_q2         = std::sqrt(Q2 + omega*omega);
        theta_q      = std::asin(kp*sinth / P_q2);
        theta_q_p    = pi - theta_q;

        beta_e = P_e/E_e;
        beta_p = P_p/E_p;

        tilde_epsilon                = xi / m2;
        double t0                    = ((E_e + kp) * E_p + (E_e + kp * cos_th) * P_p) / (2.0 * m2);
        V0                           = 4.0 * m2 * (t0 * t0 - tau * (1.0 + tau));
        tan2theta2_prime             = Q2 / V0;
        double Epsilon_prime_inverse = (1.0 + 2.0 * (1.0 + tau) * tan2theta2_prime);
        tilde_Epsilon_prime          = 1.0 / Epsilon_prime_inverse;
      }

      void ElasticKinematics::Print() const {
        std::cout << " ---------------------------------\n";
        std::cout << " E_e           = " << E_e << " GeV\n";
        std::cout << " E_p           = " << E_p << " GeV\n";
        std::cout << " omega         = " << omega << " GeV\n";
        std::cout << " E_tot         = " << E_tot << " GeV\n";
        std::cout << " Q2            = " << Q2 << " GeV^2\n";
        std::cout << " xi            = " << xi << " GeV^2\n";
        std::cout << " theta_e       = " << theta_e/degree << " deg\n";
        std::cout << " theta_q       = " << theta_q/degree << " deg\n";
        std::cout << " theta_e_p     = " << theta_e_p/degree << " deg\n";
        std::cout << " theta_p       = " << theta_p/degree << " deg\n";
        std::cout << " theta_q_p     = " << theta_q_p/degree << " deg\n";
        std::cout << " E_e + E_p     = " << E_e  + E_p << "\n";
        std::cout << " E(e') + E(p') = " << std::sqrt(kp*kp+me2)  + std::sqrt(P_p_prime*P_p_prime + m2) << "\n";
        std::cout << " kp - kp_check = " << kp  - kp_check <<  "\n";
        std::cout << " P_tot         = " << P_tot << " GeV\n";
        std::cout << " P_e           = " << P_e << " GeV\n";
        std::cout << " P_p           = " << P_p << " GeV\n";
        std::cout << " kp            = " << kp << " GeV\n";
        std::cout << " kp_check      = " << kp_check << " GeV\n";
        std::cout << " P_p_prime     = " << P_p_prime << " GeV\n";
        std::cout << " P_q           = " << P_q << " GeV\n";
        std::cout << " P_q2          = " << P_q2 << " GeV\n";
      }
      //__________________________________________________________________


      double F_recoil(const ElasticKinematics& kin) {
        double F_rec = 1.0 + (kin.E_e * kin.kp - kin.kp * (kin.E_e - kin.P_p) * kin.cos_th) /
                                 (kin.E_p * kin.kp);
        return F_rec;
      }

      double Mott_XS(const ElasticKinematics& kin) {
    using namespace insane::units;
    using namespace insane::constants;
        // From http://inspirehep.net/record/895827
        // Equation 121
        //double k_prime = Q2/(4.0*E_e*sin_th2*sin_th2);
        double gamma_e = 1.0/std::sqrt(1.0-kin.beta_e*kin.beta_e);
        double gamma_p = 1.0/std::sqrt(1.0-kin.beta_p*kin.beta_p);
        double tau     = kin.Q2/(4.0*kin.m2);
        double T1      = (fine_structure_const/kin.Q2);
        double t0      = ((kin.E_e+kin.kp)*kin.E_p+(kin.E_e+kin.kp*kin.cos_th)*kin.P_p)/(2.0*kin.m2);
        double V0      = 4.0*kin.m2*(t0*t0 - tau*(1.0+tau));
        double res     = T1*T1*(kin.kp/kin.E_e)*V0*(kin.beta_e/(kin.beta_e+kin.beta_p))*(1.0/(gamma_p*gamma_p));
        return res;
      }

    double rho2(double x, double Q2) {
      return( 1.0+4.0*x*x*((M_p/GeV)*(M_p/GeV))/Q2 );
    }

    double rho(double x, double Q2) {
      return( std::sqrt(rho(x,Q2)) );
    }

    double xi_Nachtmann(double x, double Q2) {
      // Nachtmann scaling variable, xi
      return( 2.0*x/(1.0+std::sqrt(1.0+4.0*x*x*((M_p/GeV)*(M_p/GeV))/Q2)) );
    }
    //__________________________________________________________________

    SIDISKinematics::SIDISKinematics(double E0, double Mhad, const std::array<double, 6>& vars,
                                     variables::SIDIS_x_y_z_phih_phie)
        : E_l1(E0), Mh(Mhad), x(vars[0]), y(vars[1]), z(vars[2]), P_hPerp(vars[3]), phi_h(vars[4]), phi_l2(vars[5]), isValid(true) {
      using namespace insane::units;
      using namespace ROOT::Math;
      E_p = M_p_GeV;
      P_l1 = E_l1;
      Mp  = M_p_GeV;
      Mp2 = Mp * Mp;
      E_tot = E_p + E_l1;
      P_tot = P_p + P_l1;
      phi_q = phi_l2+pi;

      // DIS kinematics
      s   = Mp2 + 2.0 * Mp * E_l1;
      Q2  = (s - Mp2) * x * y;
      nu  = y*E_l1;
      E_l2  = E_l1 - nu;
      P_l2  = E_l2;
      P_q           = std::sqrt(Q2 + nu*nu);
      gamma         = 2.0 * x * Mp / std::sqrt(Q2);
      //double sin_th = gamma * std::sqrt((1.0-y-y*y*gamma*gamma / 4.0) / (1.0 + gamma * gamma));
      double sin_th = -2.0*std::asin(std::sqrt(Mp*x*y/(2.0*nu*y)));
      theta_l2      = std::abs(std::asin(sin_th));
      epsilon       = (1.0-y-y*y*gamma*gamma/4.0)/(1.0 - y + y * y / 2.0 + y * y * gamma * gamma / 4.0);
      theta_q       = std::asin(E_l2*sin_th / P_q);
      P_dot_q       = Mp*nu; // = M*nu

      // Compute the hadron kinematics
      P_dot_Ph           = z * P_dot_q; // = M*E_p2
      E_p2               = P_dot_Ph * Mp;
      double Ph2         = E_p2 * E_p2 - Mh * Mh;
      P_p2               = std::sqrt(Ph2);
      P_hPara            = std::sqrt(Ph2 - P_hPerp * P_hPerp);
      double sin_theta_h = P_hPerp / P_p2;
      theta_h            = std::asin(sin_theta_h);
      PT_h               = P_hPerp;
      if( std::isnan(P_p2) || std::isnan(P_hPara) ){
        isValid = false;
      }

      Polar3DVector h(P_p2, theta_h, phi_h);
      auto h_lab = Rotation3D(RotationZ(phi_l2)) * Rotation3D(RotationY(theta_q)) * h;
      theta_p2   = h_lab.Theta();
      phi_p2     = h_lab.Phi();

      W2      = Mp2 + 2.0*Mp*nu - Q2;
      Wprime2 = (Mp2 + nu - E_p2)*(Mp2 + nu - E_p2) - (P_q*P_q + P_p2*P_p2 - 2.0*P_q*P_hPara);

    }

    SIDISKinematics::SIDISKinematics(double E0, double Mhad, const std::array<double, 6>& vars,
                                     variables::SIDIS_x_Q2_z_phih_phie)
        : E_l1(E0), Mh(Mhad), x(vars[0]), Q2(vars[1]), z(vars[2]), P_hPerp(vars[3]), phi_p2(vars[4]), phi_l2(vars[5]), isValid(true)
    {
      using namespace insane::units;
      E_p = M_p_GeV;
      P_p = 0.0;
      P_l1 = E_l1;
      Mp  = M_p_GeV;
      Mp2 = Mp * Mp;
      E_tot = E_p + E_l1;
      P_tot = P_p + P_l1;

      phi_q = phi_l2+pi;
      // DIS kinematics
      s     = Mp2 + 2.0 * Mp * E_l1;
      y     = Q2/((s - Mp2) * x);
      nu    = y*E_l1;
      E_l2  = E_l1 - nu;
      P_l2  = E_l2;
      P_q           = std::sqrt(Q2 + nu*nu);
      gamma         = 2.0 * x * Mp / std::sqrt(Q2);
      //double sin_th = gamma * std::sqrt((1.0-y-y*y*gamma*gamma / 4.0) / (1.0 + gamma * gamma));
      double sin_th = -2.0*std::asin(std::sqrt(Mp*x*y/(2.0*nu*y)));
      theta_l2      = std::abs(std::asin(sin_th));
      epsilon       = (1.0-y-y*y*gamma*gamma/4.0)/(1.0 - y + y * y / 2.0 + y * y * gamma * gamma / 4.0);
      theta_q       = std::asin(E_l2*sin_th / P_q);
      P_dot_q       = Mp*nu; // = M*nu

      // Compute the hadron kinematics
      P_dot_Ph           = z * P_dot_q; // = M*E_p2
      E_p2                = P_dot_Ph * Mp;
      double Ph2         = E_p2 * E_p2 - Mh * Mh;
      P_p2                = std::sqrt(Ph2);
      P_hPara            = std::sqrt(Ph2 - P_hPerp * P_hPerp);
      double sin_theta_h = P_hPerp / P_p2;
      theta_h            = std::asin(sin_theta_h);

      PT_h               = P_hPerp;
      if( std::isnan(P_p2) || std::isnan(P_hPara) ){
        isValid = false;
      }

      W2      = Mp2 + 2.0*Mp*nu - Q2;
      Wprime2 = (Mp2 + nu - E_p2)*(Mp2 + nu - E_p2) - (P_q*P_q + P_p2*P_p2 - 2.0*P_q*P_hPara);
    }

    SIDISKinematics::SIDISKinematics(double E0, double Mhad, const std::array<double, 6>& vars, variables::SIDIS_eprime_ph)
        : E_l1(E0), Mh(Mhad), P_l2(vars[0]), theta_l2(vars[1]), phi_l2(vars[2]), P_p2(vars[3]), theta_p2(vars[4]), phi_p2(vars[5]), isValid(true)
    {
      using namespace insane::units;
      using namespace ROOT::Math;
      E_p = M_p_GeV;
      P_p = 0.0;
      P_l1 = E_l1;
      E_tot = E_p + E_l1;
      P_tot = P_p + P_l1;
      E_p2 = std::sqrt(P_p2*P_p2 + Mh*Mh);
      E_l2 = P_l2;
      Mp  = M_p_GeV;
      Mp2 = Mp * Mp;

      Polar3DVector kp(P_l2, theta_l2, phi_l2);
      Polar3DVector ph(P_p2,  theta_p2, phi_p2);

      XYZTVector p1(0.0, 0.0, 0.0, E_p);
      XYZTVector l1(0.0, 0.0, P_l1, E_l1);
      XYZTVector l2(kp.X(), kp.Y(), kp.Z(), E_l2);
      XYZTVector h(ph.X(), ph.Y(), ph.Z(), E_p2);
      XYZTVector qvec(l1 - l2);

      //std::cout << "l1: " << l1 << "\n";
      //std::cout << "l2: " << l2 << "\n";
      //std::cout << "q : " << qvec << "\n";
      //std::cout << "p : " << p1 << "\n";

      nu      = E_l1 - E_l2;
      P_q     = std::sqrt(Q2 + nu * nu);
      theta_q = qvec.Theta();
      phi_q   = qvec.Phi();

      // Invariants
      P_dot_Ph = p1.Dot(h);
      P_dot_q  = p1.Dot(qvec);
      s        = Mp2 + 2.0 * Mp * E_l1;
      Q2       = -1.0*(qvec.Dot(qvec));
      x        = Q2/(2.0*P_dot_q);
      y        = P_dot_q/(p1.Dot(l1));
      z        = P_dot_Ph/(P_dot_q);

      gamma         = 2.0 * x * Mp / std::sqrt(Q2);
      epsilon       = (1.0-y-y*y*gamma*gamma/4.0)/(1.0 - y + y * y / 2.0 + y * y * gamma * gamma / 4.0);

      // Compute the hadron kinematics
      auto q_norm_dir  = (qvec.Vect().Cross(l2.Vect())).Unit();
      auto p2_norm_dir = (ph.Cross(qvec.Vect())).Unit();
      phi_h            = ROOT::Math::VectorUtil::Angle(q_norm_dir,p2_norm_dir);
      auto qdir = qvec.Vect().Unit();
      P_hPerp            = (ph - qdir.Dot(ph) * qdir).R();
      PT_h               = P_hPerp;
      P_hPara            = qdir.Dot(ph);
      double sin_theta_h = P_hPerp / P_p2;
      theta_h            = std::asin(sin_theta_h);

      if( std::isnan(P_p2) || std::isnan(P_hPara) ){
        isValid = false;
      }

      W2      = Mp2 + 2.0*Mp*nu - Q2;
      Wprime2 = (Mp2 + nu - E_p2)*(Mp2 + nu - E_p2) - (P_q*P_q + P_p2*P_p2 - 2.0*P_q*P_hPara);

    }

    void SIDISKinematics::Print() const {

      std::cout << "=== SIDISKinematics ===\n";
      std::cout << " E_l1       : " << E_l1      <<   "\n";
      std::cout << " P_l1       : " << P_l1      <<   "\n";
      std::cout << " E_p       : " << E_p      <<   "\n";
      std::cout << " P_p       : " << P_p      <<   "\n";
      std::cout << " E_l2      : " << E_l2     <<   "\n";
      std::cout << " P_l2      : " << P_l2     <<   "\n";
      std::cout << " E_p2      : " << E_p2      <<   "\n";
      std::cout << " P_p2      : " << P_p2      <<   "\n";
      std::cout << " nu        : " << nu       <<   "\n";
      std::cout << " P_q       : " << P_q      <<   "\n";
      std::cout << " E_tot     : " << E_tot    <<   "\n";
      std::cout << " P_tot     : " << P_tot    <<   "\n";
      std::cout << " theta_l2  : " << theta_l2/degree <<   " deg\n";
      std::cout << " phi_l2    : " << phi_l2  /degree <<   " deg\n";
      std::cout << " theta_p2  : " << theta_p2/degree <<   " deg\n";
      std::cout << " phi_p2    : " << phi_p2  /degree <<   " deg\n";
      std::cout << " theta_q   : " << theta_q /degree <<   " deg\n";
      std::cout << " phi_q     : " << phi_q   /degree <<   " deg\n";
      std::cout << " psi       : " << psi     /degree <<   " deg\n";
      std::cout << " P_hPerp   : " << P_hPerp  <<   "\n";
      std::cout << " P_hPara   : " << P_hPara  <<   "\n";
      std::cout << " phi_S     : " << phi_S    /degree <<   " deg\n";
      std::cout << " phi_p2    : " << phi_p2   /degree <<   " deg\n";
      std::cout << " theta_h   : " << theta_h  /degree <<   " deg\n";
      std::cout << " phi_h     : " << phi_h    /degree <<   "\n";
      std::cout << " sin_phi_h : " << sin_phi_h<<   "\n";
      std::cout << " cos_phi_h : " << cos_phi_h<<   "\n";
      std::cout << " S_para    : " << S_para   <<   "\n";
      std::cout << " s         : " << s        <<   "\n";
      std::cout << " Q2        : " << Q2       <<   "\n";
      std::cout << " x         : " << x        <<   "\n";
      std::cout << " y         : " << y        <<   "\n";
      std::cout << " z         : " << z        <<   "\n";
      std::cout << " gamma     : " << gamma    <<   "\n";
      std::cout << " epsilon   : " << epsilon  <<   "\n";
      std::cout << " P_dot_Ph  : " << P_dot_Ph <<   "\n";
      std::cout << " P_dot_q   : " << P_dot_q  <<   "\n";
      std::cout << " Mp        : " << Mp       <<   "\n";
      std::cout << " Mp2       : " << Mp2      <<   "\n";
      std::cout << " Mh        : " << Mh       <<   "\n";
    }
    //__________________________________________________________________

    double SIDISKinematics::SIDIS_xs_term() const {
      using namespace insane::units;
      using namespace insane::constants;
      return (alpha2*y*y)/(x*y*Q2*(1.0-epsilon))*(1.0-gamma*gamma/(2.0*x))*hbarc2_gev_nb;
    }

    double SIDISKinematics::JacobianTransform_1() const {
      return 2.0*E_l2*y/(4.0*x);
    }
  }
}

