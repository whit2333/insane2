#ifndef FragmentationFunctions_HH
#define FragmentationFunctions_HH

#include "TNamed.h"
#include "insane/base/Physics.h"

namespace insane::physics {

  /** \addtogroup fragmentationFunctions Fragmentation Functions
   *
   *  \ingroup physics
   */

  /** Abstract base class for Fragmentation Functions.
   *
   * @ingroup fragmentationFunctions
   */
  class FragmentationFunctions : public TNamed {
  public:
    FragmentationFunctions();
    virtual ~FragmentationFunctions();

    double D1(Parton q, Hadron h, double z, double pt, double Q2) const;

    virtual double D1_pi_plus(Parton q, double z, double pt, double Q2) const  ;
    virtual double D1_pi_minus(Parton q, double z, double pt, double Q2) const ;
    virtual double D1_K_plus(Parton q, double z, double pt, double Q2) const  ;
    virtual double D1_K_minus(Parton q, double z, double pt, double Q2) const ;

    virtual double D1_u_pi_plus(double z, double pt, double Q2) const     = 0;
    virtual double D1_u_pi_minus(double z, double pt, double Q2) const    = 0;
    virtual double D1_ubar_pi_plus(double z, double pt, double Q2) const  = 0;
    virtual double D1_ubar_pi_minus(double z, double pt, double Q2) const = 0;
    virtual double D1_d_pi_plus(double z, double pt, double Q2) const     = 0;
    virtual double D1_d_pi_minus(double z, double pt, double Q2) const    = 0;
    virtual double D1_dbar_pi_plus(double z, double pt, double Q2) const  = 0;
    virtual double D1_dbar_pi_minus(double z, double pt, double Q2) const = 0;
    virtual double D1_s_pi_plus(double z, double pt, double Q2) const     = 0;
    virtual double D1_s_pi_minus(double z, double pt, double Q2) const    = 0;
    virtual double D1_sbar_pi_plus(double z, double pt, double Q2) const  = 0;
    virtual double D1_sbar_pi_minus(double z, double pt, double Q2) const = 0;

    virtual double D1_u_K_plus(double z, double pt, double Q2) const     = 0;
    virtual double D1_u_K_minus(double z, double pt, double Q2) const    = 0;
    virtual double D1_ubar_K_plus(double z, double pt, double Q2) const  = 0;
    virtual double D1_ubar_K_minus(double z, double pt, double Q2) const = 0;
    virtual double D1_d_K_plus(double z, double pt, double Q2) const     = 0;
    virtual double D1_d_K_minus(double z, double pt, double Q2) const    = 0;
    virtual double D1_dbar_K_plus(double z, double pt, double Q2) const  = 0;
    virtual double D1_dbar_K_minus(double z, double pt, double Q2) const = 0;
    virtual double D1_s_K_plus(double z, double pt, double Q2) const     = 0;
    virtual double D1_s_K_minus(double z, double pt, double Q2) const    = 0;
    virtual double D1_sbar_K_plus(double z, double pt, double Q2) const  = 0;
    virtual double D1_sbar_K_minus(double z, double pt, double Q2) const = 0;


    // pt integrated pion FFs
    double D(Parton q, Hadron h, double z, double Q2) const;

    virtual double D_pi_plus(Parton q, double z,  double Q2) const  ;
    virtual double D_pi_minus(Parton q, double z,  double Q2) const ;
    virtual double D_K_plus(Parton q, double z,  double Q2) const  ;
    virtual double D_K_minus(Parton q, double z,  double Q2) const ;

    virtual double D_u_piplus(double z, double Q2) const     = 0;
    virtual double D_u_piminus(double z, double Q2) const    = 0;
    virtual double D_ubar_piplus(double z, double Q2) const  = 0;
    virtual double D_ubar_piminus(double z, double Q2) const = 0;
    virtual double D_d_piplus(double z, double Q2) const     = 0;
    virtual double D_d_piminus(double z, double Q2) const    = 0;
    virtual double D_dbar_piplus(double z, double Q2) const  = 0;
    virtual double D_dbar_piminus(double z, double Q2) const = 0;
    virtual double D_s_piplus(double z, double Q2) const     = 0;
    virtual double D_s_piminus(double z, double Q2) const    = 0;
    virtual double D_sbar_piplus(double z, double Q2) const  = 0;
    virtual double D_sbar_piminus(double z, double Q2) const = 0;

    // kaon FFs
    virtual double D_u_Kplus(double z, double Q2) const     = 0;
    virtual double D_u_Kminus(double z, double Q2) const    = 0;
    virtual double D_ubar_Kplus(double z, double Q2) const  = 0;
    virtual double D_ubar_Kminus(double z, double Q2) const = 0;
    virtual double D_d_Kplus(double z, double Q2) const     = 0;
    virtual double D_d_Kminus(double z, double Q2) const    = 0;
    virtual double D_dbar_Kplus(double z, double Q2) const  = 0;
    virtual double D_dbar_Kminus(double z, double Q2) const = 0;
    virtual double D_s_Kplus(double z, double Q2) const     = 0;
    virtual double D_s_Kminus(double z, double Q2) const    = 0;
    virtual double D_sbar_Kplus(double z, double Q2) const  = 0;
    virtual double D_sbar_Kminus(double z, double Q2) const = 0;

    // pi0
    // D_q^{pi0} = (D_q^{pi+} + D_q^{pi-})/2
    virtual double D_u_pi0(double z, double Q2) const;
    virtual double D_ubar_pi0(double z, double Q2) const;
    virtual double D_d_pi0(double z, double Q2) const;
    virtual double D_dbar_pi0(double z, double Q2) const;
    virtual double D_s_pi0(double z, double Q2) const;
    virtual double D_sbar_pi0(double z, double Q2) const;

    ClassDef(FragmentationFunctions, 1)
  };
} // namespace insane::physics
#endif
