#ifndef TransverseMomentumDistributions_HH
#define TransverseMomentumDistributions_HH

#include "TNamed.h"
#include "insane/base/Physics.h"

namespace insane::physics {

  /** \addtogroup tmds Transverse momentum dependent PDFs
   *
   *  \ingroup physics
   */

  /** Base class for TMDs
   *
   * @ingroup tmds
   */
  class TransverseMomentumDistributions : public TNamed {

  public:
    TransverseMomentumDistributions();
    virtual ~TransverseMomentumDistributions();

    virtual double f1(Nuclei target, Parton q, double x, double kt, double Q2) const;

    virtual double f1_p(Parton q, double x, double kt, double Q2) const ;
    virtual double f1_n(Parton q, double x, double kt, double Q2) const ;

    virtual double f1(Parton q, double x, double kt, double Q2) const {
      return f1_p(q,x,kt,Q2);
    }

    virtual double f1_u(double x, double kt, double Q2)   const  = 0;
    virtual double f1_d(double x, double kt, double Q2)   const  = 0;
    virtual double f1_ubar(double x, double kt, double Q2)const  = 0;
    virtual double f1_dbar(double x, double kt, double Q2)const  = 0;

    ClassDef(TransverseMomentumDistributions, 1)
  };

} // namespace insane::physics

#endif

