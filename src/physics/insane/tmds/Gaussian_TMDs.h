#ifndef insane_tmds_Gaussian_TMDs_HH
#define insane_tmds_Gaussian_TMDs_HH

#include "insane/tmds/TransverseMomentumDistributions.h"
#include "insane/base/Physics.h"
#include "insane/base/Math.h"

namespace insane::physics {

  /** Gaussian approxiamtion from PDFs
   *
   * @ingroup tmds
   */
  template<class PDFs>
  class Gaussian_TMDs : public TransverseMomentumDistributions {
  private:

      double kt2_mean = 0.25 ; // GeV^2

  public:
      PDFs pdfs;

  public:
    Gaussian_TMDs(){}
    virtual ~Gaussian_TMDs(){}

    virtual double f1_u(double x, double kt, double Q2) const {
      double gaussian_part = std::exp(-kt * kt / kt2_mean) / (M_PI * kt2_mean);
      //std::cout << pdfs.Get(Parton::u,x,Q2) << "\n";
      return  gaussian_part*pdfs.Get(Parton::u,x,Q2);
    }
    virtual double f1_d(double x, double kt, double Q2) const {
      double gaussian_part = std::exp(-kt * kt / kt2_mean) / (M_PI * kt2_mean);
      return  gaussian_part*pdfs.Get(Parton::d,x,Q2);
    }
    virtual double f1_ubar(double x, double kt, double Q2) const {
      double gaussian_part = std::exp(-kt * kt / kt2_mean) / (M_PI * kt2_mean);
      return  gaussian_part*pdfs.Get(Parton::ubar,x,Q2);
    }
    virtual double f1_dbar(double x, double kt, double Q2) const {
      double gaussian_part = std::exp(-kt * kt / kt2_mean) / (M_PI * kt2_mean);
      return  gaussian_part*pdfs.Get(Parton::dbar,x,Q2);
    }

    ClassDef(Gaussian_TMDs, 1)
  };

} // namespace insane::physics

#endif

