#ifndef insane_tmds_DSS_FrFs_HH
#define insane_tmds_DSS_FrFs_HH

extern"C" {
  void fdss_(int* IH, int* IC, int* IO, double* X, double* Q2, double* U, double* UB, double* D,
             double* DB, double* S, double* SB, double* C, double* B, double* GL);
//  void fdssini_(int* IH, int* IC, int* IO);
}

#include "insane/tmds/FragmentationFunctions.h"
#include "TMutex.h"
#include "TVirtualMutex.h"

namespace insane::physics {

extern TMutex g_DSS_FrF_mutex;

/** DSS Fragmentation Functions.
 * http://inspirehep.net/record/746992
 *
 * @ingroup fragfuncs
 */
class DSS_FrFs : public FragmentationFunctions {
   protected:

      double fPar_piplus_u_plus_ubar[5];
      double fPar_piplus_d_plus_dbar[5];
      double fPar_piplus_ubar_eq_d[5];
      double fPar_piplus_s_plus_sbar[5];
      double fPar_piplus_c_plus_cbar[5];
      double fPar_piplus_b_plus_bbar[5];
      double fPar_piplus_g[5];

      double fPar_Kplus_u_plus_ubar[5];
      double fPar_Kplus_d_plus_dbar[5];
      double fPar_Kplus_s_plus_sbar[5];
      double fPar_Kplus_ubar_eq_s[5];
      double fPar_Kplus_c_plus_cbar[5];
      double fPar_Kplus_b_plus_bbar[5];
      double fPar_Kplus_g[5];

      double fNprime;

      // Model function (at input scale)
      double D_model(double z,double *pars) const ;

      double kt2_mean = 0.25 ; // GeV^2
      double pt2_mean = 0.20 ; // GeV^2


   public:

      DSS_FrFs();
      virtual ~DSS_FrFs();

      virtual double D1_u_pi_plus(double z, double pt, double Q2) const     ;
      virtual double D1_u_pi_minus(double z, double pt, double Q2) const    ;
      virtual double D1_ubar_pi_plus(double z, double pt, double Q2) const  ;
      virtual double D1_ubar_pi_minus(double z, double pt, double Q2) const ;
      virtual double D1_d_pi_plus(double z, double pt, double Q2) const     ;
      virtual double D1_d_pi_minus(double z, double pt, double Q2) const    ;
      virtual double D1_dbar_pi_plus(double z, double pt, double Q2) const  ;
      virtual double D1_dbar_pi_minus(double z, double pt, double Q2) const ;
      virtual double D1_s_pi_plus(double z, double pt, double Q2) const     ;
      virtual double D1_s_pi_minus(double z, double pt, double Q2) const    ;
      virtual double D1_sbar_pi_plus(double z, double pt, double Q2) const  ;
      virtual double D1_sbar_pi_minus(double z, double pt, double Q2) const ;

      virtual double D1_u_K_plus(double z, double pt, double Q2) const     ;
      virtual double D1_u_K_minus(double z, double pt, double Q2) const    ;
      virtual double D1_ubar_K_plus(double z, double pt, double Q2) const  ;
      virtual double D1_ubar_K_minus(double z, double pt, double Q2) const ;
      virtual double D1_d_K_plus(double z, double pt, double Q2) const     ;
      virtual double D1_d_K_minus(double z, double pt, double Q2) const    ;
      virtual double D1_dbar_K_plus(double z, double pt, double Q2) const  ;
      virtual double D1_dbar_K_minus(double z, double pt, double Q2) const ;
      virtual double D1_s_K_plus(double z, double pt, double Q2) const     ;
      virtual double D1_s_K_minus(double z, double pt, double Q2) const    ;
      virtual double D1_sbar_K_plus(double z, double pt, double Q2) const  ;
      virtual double D1_sbar_K_minus(double z, double pt, double Q2) const ;

      virtual double D_u_piplus(double z, double Q2) const;
      virtual double D_u_piminus(double z, double Q2) const;
      virtual double D_ubar_piplus(double z, double Q2) const;
      virtual double D_ubar_piminus(double z, double Q2) const;
      virtual double D_d_piplus(double z, double Q2) const;
      virtual double D_d_piminus(double z, double Q2) const;
      virtual double D_dbar_piplus( double z, double Q2) const ;
      virtual double D_dbar_piminus(double z, double Q2) const ;
      virtual double D_s_piplus( double z, double Q2)    const ;
      virtual double D_s_piminus(double z, double Q2)    const ;
      virtual double D_sbar_piplus( double z, double Q2) const ;
      virtual double D_sbar_piminus(double z, double Q2) const ;

      // kaon FFs
      virtual double D_u_Kplus( double z, double Q2)    const ;
      virtual double D_u_Kminus(double z, double Q2)    const ;
      virtual double D_ubar_Kplus( double z, double Q2) const ;
      virtual double D_ubar_Kminus(double z, double Q2) const ;
      virtual double D_d_Kplus( double z, double Q2)    const ;
      virtual double D_d_Kminus(double z, double Q2)    const ;
      virtual double D_dbar_Kplus( double z, double Q2) const ;
      virtual double D_dbar_Kminus(double z, double Q2) const ;
      virtual double D_s_Kplus( double z, double Q2)    const ;
      virtual double D_s_Kminus(double z, double Q2)    const ;
      virtual double D_sbar_Kplus( double z, double Q2) const ;
      virtual double D_sbar_Kminus(double z, double Q2) const ;


   ClassDef(DSS_FrFs,1)
};
} // namespace insane::physics
#endif


