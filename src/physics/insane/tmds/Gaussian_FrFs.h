#ifndef insane_tmds_Gaussian_FrFs_HH
#define insane_tmds_Gaussian_FrFs_HH

#include "insane/tmds/FragmentationFunctions.h"

namespace insane::physics {

  class Gaussian_FrFs : public FragmentationFunctions {
  public:
    Gaussian_FrFs() {}
    virtual ~Gaussian_FrFs() {}

    virtual double D1_pi_plus(Parton q, double z, double pt, double Q2) const {
      switch (q) {
      case Parton::u:
        return D1_u_pi_plus(z, pt, Q2);
        break;
      case Parton::d:
        return D1_d_pi_plus(z, pt, Q2);
        break;
      case Parton::ubar:
        return D1_ubar_pi_plus(z, pt, Q2);
        break;
      case Parton::dbar:
        return D1_dbar_pi_plus(z, pt, Q2);
        break;
      default:
        return 0.0;
        break;
      }
    }
    virtual double D1_pi_minus(Parton q, double z, double pt, double Q2) const { return 0.0;}
    virtual double D1_K_plus(Parton q, double z, double pt, double Q2) const   { return 0.0;}
    virtual double D1_K_minus(Parton q, double z, double pt, double Q2) const  { return 0.0;}
  };
} // namespace insane::physics
