#ifndef DSSFragmentationFunctions_HH
#define DSSFragmentationFunctions_HH

extern"C" {
void fdss_(int* IH, int* IC, int* IO, double* X, double* Q2, double* U, double* UB, double* D,
           double* DB, double* S, double* SB, double* C, double* B, double* GL);
//  void fdssini_(int* IH, int* IC, int* IO);
}

#include "TMutex.h"
#include "TVirtualMutex.h"
#include "insane/tmds/FragmentationFunctions.h"

namespace insane::physics {

  extern TMutex g_DSS_mutex;

  /** DSS Fragmentation Functions.
   * http://inspirehep.net/record/746992
   *
   * @ingroup fragfuncs
   */
  class DSSFragmentationFunctions : public FragmentationFunctions {

  protected:
    double fPar_piplus_u_plus_ubar[5];
    double fPar_piplus_d_plus_dbar[5];
    double fPar_piplus_ubar_eq_d[5];
    double fPar_piplus_s_plus_sbar[5];
    double fPar_piplus_c_plus_cbar[5];
    double fPar_piplus_b_plus_bbar[5];
    double fPar_piplus_g[5];

    double fPar_Kplus_u_plus_ubar[5];
    double fPar_Kplus_d_plus_dbar[5];
    double fPar_Kplus_s_plus_sbar[5];
    double fPar_Kplus_ubar_eq_s[5];
    double fPar_Kplus_c_plus_cbar[5];
    double fPar_Kplus_b_plus_bbar[5];
    double fPar_Kplus_g[5];

    double fNprime;

    // Model function (at input scale)
    double D_model(double z, double* pars) const;

  public:
    DSSFragmentationFunctions();
    virtual ~DSSFragmentationFunctions();

    virtual double D_u_piplus(double z, double Q2) const;
    virtual double D_u_piminus(double z, double Q2) const;
    virtual double D_ubar_piplus(double z, double Q2) const;
    virtual double D_ubar_piminus(double z, double Q2) const;
    virtual double D_d_piplus(double z, double Q2) const;
    virtual double D_d_piminus(double z, double Q2) const;
    virtual double D_dbar_piplus(double z, double Q2) const;
    virtual double D_dbar_piminus(double z, double Q2) const;
    virtual double D_s_piplus(double z, double Q2) const;
    virtual double D_s_piminus(double z, double Q2) const;
    virtual double D_sbar_piplus(double z, double Q2) const;
    virtual double D_sbar_piminus(double z, double Q2) const;

    // kaon FFs
    virtual double D_u_Kplus(double z, double Q2) const;
    virtual double D_u_Kminus(double z, double Q2) const;
    virtual double D_ubar_Kplus(double z, double Q2) const;
    virtual double D_ubar_Kminus(double z, double Q2) const;
    virtual double D_d_Kplus(double z, double Q2) const;
    virtual double D_d_Kminus(double z, double Q2) const;
    virtual double D_dbar_Kplus(double z, double Q2) const;
    virtual double D_dbar_Kminus(double z, double Q2) const;
    virtual double D_s_Kplus(double z, double Q2) const;
    virtual double D_s_Kminus(double z, double Q2) const;
    virtual double D_sbar_Kplus(double z, double Q2) const;
    virtual double D_sbar_Kminus(double z, double Q2) const;

    ClassDef(DSSFragmentationFunctions, 1)
  };
} // namespace insane::physics
#endif

