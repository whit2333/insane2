#ifndef insane_kinematics_Elastic_HH
#define insane_kinematics_Elastic_HH

#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/Core.h"

namespace insane::kine {

  /** Elastic scattering kinematic variables.
   * Strongly typed kinematic variables.
   * \ingroup kinematics
   */
  namespace elastic {

    /** @addtogroup elasticKinematics Elastic Kinematics 
     *  @{
     *   Elastic scattering kinematics.
     *  \f[  e + N \rightarrow e + N \f]
     *
     *  Variables for computing elastic radiative tails with various 
     *  approximations are also defined.
     *
     *  \ingroup kinematics
     */

    /** @name Scattered electron variables.
     *  Electron kinematics.
     * @{
     */
    using M_target_v = Var<struct M_target_tag>; ///< \f$  M_{target} \f$
    using E_beam_v   = Var<struct E_beam_tag>;   ///< \f$ E_{beam} = k_1^0  \f$
    using E_prime_v  = Var<struct E_prime_tag>;  ///< \f$ E^{\prime} = k_2^0 \f$
    using theta_v    = Var<struct theta_tag>;    ///< \f$ \theta_{e}\f$
    using phi_v      = Var<struct phi_tag>;      ///< \f$ \phi_{e}  \f$
    //@}

    /** @name Four-vectors.
     * @{
     */
    using k_prime_vec = FourVec<struct k_prime_tag>;  ///< \f$ k_2 \f$
    using k_vec       = FourVec<struct k_tag>; ///< \f$ k_1 \f$
    using q_vec       = FourVec<struct q_tag>; ///< \f$ q = k_1 - k_2 \f$
    //@}

    /** @name Elastic scattered recoil.
     *  Recoil kinematics.
     * @{
     */
    using M_recoil_v     = Var<struct M_recoil_tag>;  ///< Mass of elastic recoil. Most of the time is the same as M_target_v
    using E_recoil_v     = Var<struct E_recoil_tag>; ///< Energy of elastic recoil.
    using P_recoil_v     = Var<struct P_recoil_tag>; ///< Momentum of elastic recoil.
    using T_recoil_v     = Var<struct P_recoil_tag>; ///< Relativistic KE of elastic recoil.
    using theta_recoil_v = Var<struct theta_recoil_tag>; ///< angle of recoil
    using phi_recoil_v   = Var<struct phi_recoil_tag>;  ///< azimuthal angle of recoil
    using p_recoil_vec   = FourVec<struct p_recoil_tag>; ///< Four vector for elastic recoil.
    //@}

    /** @name Elastic invariants and other variables.
     * @{
     */
    using Q2_v      = Var<struct Q2_tag>;      ///< \f$ Q^2 = -q^2 \f$
    using W2_v      = Var<struct W2_tag>;      ///< \f$ W^2 = M^2 + Q^2/x - Q^2 \f$
    using W_v       = Var<struct W_tag>;       ///< \f$ W = \sqrt{M^2 + Q^2/x - Q^2} \f$
    using epsilon_v = Var<struct epsilon_tag>; ///< \f$ \epsilon = \frac{1-y}{1 - y +y^2/2} \f$
    using nu_v      = Var<struct nu_tag>;      ///< \f$ \nu = E-E^{\prime} \f$
    using nu_cm_v   = Var<struct nu_cm_tag>;   ///< \f$ \nu_{cm} \f$
    using q_v       = Var<struct q_tag>;       ///< \f$ |\vec{q}|  \f$
    using q_cm_v    = Var<struct q_cm_tag>;    ///< \f$ |\vec{q}_{cm}| \f$
    using theta_q_v = Var<struct theta_q_tag>; ///< \f$ \theta_q \f$ virtual photon angle.
    using tau_v     = Var<struct tau_tag>;     ///< \f$ \tau = Q^2/4M^2  \f$
    //@}

    /** \name Elastic Radiative tail variables.
     * @{
     */
    using E_prime_elastic_v    = insane::kine::Var<struct E_prime_elastic_tag>;
    using Q2_ep_elastic_v     = insane::kine::Var<struct Q2_ep_elastic_tag>;

    using Es_v             = insane::kine::Var<struct V_Es_tag>; ///< Beam energy after radiating photon.
    using Q2_prime_v       = insane::kine::Var<struct V_Q2_prime_tag>; ///< Effective Q2 for beam with energy Es

    using omega_s_v        = insane::kine::Var<struct omega_s_tag>; ///< C.11 from Tsai SLAC-PUB-0408, 1971
    using omega_p_v        = insane::kine::Var<struct omega_p_tag>; ///< C.12 from Tsai SLAC-PUB-0408, 1971
    //@}


    inline auto add_tau = [](const auto& v) constexpr {
      const auto&  M     = std::get<M_target_v>(v);
      const auto&  Q2    = std::get<Q2_v>(v);
      return std::make_tuple(tau_v{Q2/(4.0*M*M)});
    };

    using ElasticPrimaryVars = typename std::tuple<M_recoil_v, E_prime_v, Q2_v, nu_v, W_v>;

    /**  Add the main elastic kinematic variables.
     *
     *  Here is how it could be used.
     * \code{.cpp}
     *   auto initial_vars = make_independent_vars<theta_v, phi_v>()
     *                       .add<E_beam_v>([](const auto& v) constexpr { return 10.0; });
     *   auto elastic_vars = initial_vars.append(add_ElasticPrimaryVars) ;
     * \endcode
     * Now the variable stack elastic_vars has independent variables theta and phi. Using
     * append() allows a generic lambda to be used. The only restriction on the lambda is
     * that it must be explicit in the return type. Here it is the tuple ElasticPrimaryVars.
     *
     *  \ingroup elasticKinematics
     */
    inline auto add_ElasticPrimaryVars = [](const auto& v) constexpr {
      const auto&  E0    = std::get<E_beam_v>(v);
      const auto&  M     = insane::masses::M_p / insane::units::GeV; // std::get<M_recoil_v>(v);
      const auto&  th    = std::get<theta_v>(v);
      const double sinth = std::sin(th / 2.0);
      const auto&  Ep    = E0 / (1.0 + (2.0 * E0 / M) * (sinth * sinth));
      const auto&  Q2    = 4.0 * E0 * Ep * sinth * sinth;
      const auto&  nu    = E0 - Ep;
      const auto&  W     = std::sqrt(M * M - Q2 + 2.0 * M * nu);
      return ElasticPrimaryVars{M, Ep, Q2, nu, W};
    };

    using Elastic_independent_vars = decltype(make_independent_vars<E_beam_v, theta_v, phi_v>());

    inline auto construct_Elastic_variables() {
      return make_independent_vars<E_beam_v, theta_v, phi_v>()
          .add<M_recoil_v>(
              [](const auto& v) constexpr { return insane::masses::M_p / insane::units::GeV; })
          .add<M_target_v>(
              [](const auto& v) constexpr { return insane::masses::M_p / insane::units::GeV; })
          .add<E_prime_v>([](const auto& v) constexpr {
            const auto&  E0    = std::get<E_beam_v>(v);
            const auto&  M     = std::get<M_recoil_v>(v);
            const auto&  th    = std::get<theta_v>(v);
            const double sinth = std::sin(th / 2.0);
            return E0 / (1.0 + (2.0 * E0 / M) * (sinth * sinth));
          })
          .add<Q2_v, nu_v>(
              [](const auto& v) constexpr {
                const auto&  E0    = std::get<E_beam_v>(v);
                const auto&  Ep    = std::get<E_prime_v>(v);
                const auto&  th    = std::get<theta_v>(v);
                const double sinth = std::sin(th / 2.0);
                return 4.0 * E0 * Ep * sinth * sinth;
              },
              [](const auto& v) constexpr {
                const auto& E0 = std::get<E_beam_v>(v);
                const auto& Ep = std::get<E_prime_v>(v);
                return E0 - Ep;
              })
          .add<tau_v>([](const auto& v) constexpr {
            const auto& M  = std::get<M_target_v>(v);
            const auto& Q2 = std::get<Q2_v>(v);
            return Q2 / (4.0 * M * M);
          })
          .add<W_v>([](const auto& v) constexpr {
            // calculate W
            const auto& M_p = std::get<M_recoil_v>(v);
            const auto& Q2  = std::get<Q2_v>(v);
            const auto& nu  = std::get<nu_v>(v);
            return std::sqrt(M_p * M_p - Q2 + 2.0 * M_p * nu);
          })
          .add<k_vec>([](const auto& v) constexpr {
            const double M_e = 0.000511;
            const auto&  E0  = std::get<E_beam_v>(v);
            return ROOT::Math::XYZTVector{0.0, 0.0, std::sqrt(E0 * E0 - M_e * M_e), E0};
          })
          .add<k_prime_vec>([](const auto& v) constexpr {
            const double M_e   = 0.000511;
            const auto&  E0    = std::get<E_beam_v>(v);
            const auto&  Ep    = std::get<E_prime_v>(v);
            const auto&  th    = std::get<theta_v>(v);
            const auto&  ph    = std::get<phi_v>(v);
            const double sinth = std::sin(th);
            const double costh = std::cos(th);
            const double sinph = std::sin(ph);
            const double cosph = std::cos(ph);
            return ROOT::Math::XYZTVector{sinth * cosph * Ep, sinth * sinph * Ep, costh * Ep, Ep};
          })
          .add<q_vec>([](const auto& v) constexpr {
            const ROOT::Math::XYZTVector& k  = std::get<k_vec>(v);
            const ROOT::Math::XYZTVector& kp = std::get<k_prime_vec>(v);
            return ROOT::Math::XYZTVector(k - kp);
          })
          .add<p_recoil_vec>([](const auto& v) constexpr {
            const ROOT::Math::XYZTVector& q   = std::get<q_vec>(v);
            const auto&                   M_p = std::get<M_recoil_v>(v);
            return ROOT::Math::XYZTVector{0.0, 0.0, 0.0, M_p} + q;
          })
          //.add<E_recoil_v,P_recoil_v,theta_recoil_v,phi_recoil_v >([](const auto& v) constexpr {
          //  return
          //})
          ;
    }

    using ElasticVectorVars = std::tuple<k_vec, k_prime_vec, q_vec, p_recoil_vec>;

    inline auto add_ElasticVectorVars = [](const auto& v) {
      const auto&  E0    = std::get<E_beam_v>(v);
      const auto&  M     = insane::masses::M_p / insane::units::GeV; // std::get<M_recoil_v>(v);
      const auto&  th    = std::get<theta_v>(v);
      const double sinth = std::sin(th / 2.0);
      const auto&  Ep    = E0 / (1.0 + (2.0 * E0 / M) * (sinth * sinth));
      const auto&  Q2    = 4.0 * E0 * Ep * sinth * sinth;
      const auto&  nu    = E0 - Ep;
      const auto&  W     = std::sqrt(M * M - Q2 + 2.0 * M * nu);
      const double M_e   = 0.000511;
      const auto&  M_p   = std::get<M_recoil_v>(v);
      const auto&  ph    = std::get<phi_v>(v);
      const double costh = std::cos(th);
      const double sinph = std::sin(ph);
      const double cosph = std::cos(ph);
      ROOT::Math::XYZTVector k{0.0, 0.0, std::sqrt(E0 * E0 - M_e * M_e), E0};
      ROOT::Math::XYZTVector kp{sinth * cosph * Ep, sinth * sinph * Ep, costh * Ep, Ep};
      ROOT::Math::XYZTVector q = k - kp;
      return ElasticVectorVars{k, kp, q, ROOT::Math::XYZTVector{0.0, 0.0, 0.0, M_p} + q};
    };

    using Elastic_vars = decltype(construct_Elastic_variables());
    //@}
  } // namespace elastic

} // namespace insane::kine

#endif

