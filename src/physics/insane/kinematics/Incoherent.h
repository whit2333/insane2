#ifndef insane_kinematics_Incoherent_HH
#define insane_kinematics_Incoherent_HH

#include "insane/kinematics/Inelastic.h"
#include "insane/kinematics/Elastic.h"

namespace insane::kine {

  /** Incoherent kinematics.
   * \ingroup kinematics
   */
  namespace incoherent {

    /** \addtogroup incoherentKinematics Incoherent kinematics
     *
     * Kinematics for scattering off nuclear substructure.
     *
     *  \f[ M_{target}(A) \simeq M_{recoil}(N)  +  M_{spectator}(A-1) \f]
     *
     *  \f[ \vec{p}_{1,IA} = - \vec{p}_{spectator} \f]
     *
     *  IA is "impulse approximation"
     *
     *  \ingroup kinematics
     *  @{
     */

      using M_target_v    = typename insane::kine::dis::M_target_v; ///< M(A)
      using M_recoil_v    = typename insane::kine::elastic::M_recoil_v; ///< Typically M_N, but not always.
      using M_spectator_v = insane::kine::Var<struct M_spectator_tag>; ///< Typicall M(A-1).

      using p_recoil_vec    = typename insane::kine::elastic::p_recoil_vec;  ///< momentum of struck system (typically a nucleon)
      using p_initial_vec   = insane::kine::FourVec<struct p_initial_vec_tag>;  ///< initial momentum of recoil system (typically a nucleon)
      using p_spectator_vec = insane::kine::FourVec<struct p_spectator_vec_tag>; ///< momentum of spectator system (A-1)

      using P_fermi_v     = insane::kine::Var<struct P_fermi_tag>;
      using theta_fermi_v = insane::kine::Var<struct theta_fermi_tag>;
      using phi_fermi_v   = insane::kine::Var<struct phi_fermi_tag>;

      // using p_spec_vec    = insane::kine::FourVec<struct p_spec_vec_tag>;

      inline auto add_incoherent_target_system = [](const auto& v) constexpr {
        const auto&                  p     = std::get<P_fermi_v>(v);
        const auto&                  th    = std::get<theta_fermi_v>(v);
        const auto&                  ph    = std::get<phi_fermi_v>(v);
        const double                 M     = std::get<M_recoil_v>(v);
        const double                 M2    = std::get<M_spectator_v>(v);
        const double                 sinth = std::sin(th);
        const double                 costh = std::cos(th);
        const double                 sinph = std::sin(ph);
        const double                 cosph = std::cos(ph);
        return std::tuple<p_spectator_vec, p_initial_vec>{
            ROOT::Math::XYZTVector{-1.0 * sinth * cosph * p, -1.0 * sinth * sinph * p,
                                      -1.0 * costh * p, std::sqrt(p * p + M2 * M2)},
            ROOT::Math::XYZTVector{sinth * cosph * p, sinth * sinph * p, costh * p,
                                      std::sqrt(p * p + M * M)}};
      };

      inline auto construct_incoherent_target_system() {
        using namespace insane::physics;
        return make_independent_vars<M_recoil_v ,M_spectator_v,P_fermi_v, theta_fermi_v,
                                     phi_fermi_v>()
                                     //.add<M_recoil_v ,M_spectator_v>([](const auto& v) constexpr {
                                     //  return std::tuple<M_recoil_v ,M_spectator_v>{0.938, 3.7};
                                     //})
            .add<p_spectator_vec, p_initial_vec>([](const auto& v) constexpr {
              const auto&                  p     = std::get<P_fermi_v>(v);
              const auto&                  th    = std::get<theta_fermi_v>(v);
              const auto&                  ph    = std::get<phi_fermi_v>(v);
              const double                 M     = std::get<M_recoil_v>(v);
              const double                 M2    = std::get<M_spectator_v>(v);
              const double                 sinth = std::sin(th);
              const double                 costh = std::cos(th);
              const double                 sinph = std::sin(ph);
              const double                 cosph = std::cos(ph);
              return std::tuple<p_spectator_vec, p_initial_vec>{
                ROOT::Math::XYZTVector{-1.0 * sinth * cosph * p, -1.0 * sinth * sinph * p,
                  -1.0 * costh * p, std::sqrt(p * p + M2 * M2)},
                ROOT::Math::XYZTVector{sinth * cosph * p, sinth * sinph * p, costh * p,
                  std::sqrt(p * p + M * M)}};
            });
      }
      //@}
  } // namespace incoherent
} // namespace insane::kine
#endif
