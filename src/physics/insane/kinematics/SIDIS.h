#ifndef insane_kinematics_SIDIS_HH
#define insane_kinematics_SIDIS_HH

#include "insane/kinematics/Core.h"
#include "insane/kinematics/Inelastic.h"
#include "Math/GenVector/RotationY.h"
#include "Math/GenVector/RotationZ.h"
#include "Math/GenVector/Rotation3D.h"

namespace insane::kine {

  /** SIDIS Kinematics.
   * Semi-inclusive Deep inelastic scattering.
   * \ingroup kinematics
   */
  namespace sidis {

    /** @addtogroup sidisKinematics SIDIS kinematics.
     *  Kinematics for Semi-inclusive Deep inelastic scattering (SIDIS).
     *
     *  \f[  e + N \rightarrow e + h + X  \f]
     *
     *  \ingroup kinematics
     *  @{
     */

    using namespace insane::kine::dis;

    /** @name SIDIS hadron variables.
     */
    //@{
    // scattered hadron kinematics
    using E_h_v = Var<struct E_h_tag>; ///<  produced hadron energy, \f$ E_{h} \f$
    using P_h_v = Var<struct P_h_tag>; ///< produced hadron momentum \f$ P_{h} = |\vec{P}_{h}| \f$

    using theta_h_v = Var<struct theta_h_tag>; ///< \f$ \theta_{h} \f$
    using phi_h_v = Var<struct phi_h_tag>;      ///<  Hadron  azimuthal angle relative to the lepton
                                                ///<  scattering plane \f$  \varphi_{h}  \f$
    using p_h_vec   = FourVec<struct p_h_tag>; ///< \f$ \vec{P}_{h} \f$

    using theta_h_lab_v = Var<struct theta_h_lab_tag>; ///< \f$ \theta_{h} \f$
    using phi_h_lab_v   = Var<struct phi_h_lab_tag>;   ///<  Hadron  azimuthal angle in the lab
    using p_h_lab_vec   = FourVec<struct p_h_lab_tag>; ///< \f$ \vec{P}_{h} \f$
    //@}

    /** @name SIDIS variables.
     *  SIDIS hadron related invariants.
     */
    //@{
    using M_h_v        = Var<struct M_h_tag>;
    using z_v          = Var<struct z_tag>;
    using W_prime_pi_v = Var<struct W_prime_pi_tag>;

    //@}
    /** @name Azimuthal angles in Trento convention.
     */
    //@{
    using S_perp_v = Var<struct S_perp_tag>;    ///<  Transverse target spin \f$  S_{\perp}  \f$
    using phi_S_v = Var<struct phi_S_tag>;      ///< Target spin azimuthal angle \f$  \varphi_{S}  \f$

    using P_hPerp_v = Var<struct P_hPerp_tag>;  ///<  Transverse hadron momentum \f$  P_{h\perp}  \f$
    using P2_hPerp_v = Var<struct P2_hPerp_tag>;///<  Transverse hadron momentum  squared \f$  P^{2}_{h\perp}  \f$

    using gamma_v      = Var<struct gamma_tag>;      ///< 2Mx/Q

    //@}

    /// Upolarized SIDIS independent variables
    using SIDIS_primary_vars = typename std::tuple<x_v, y_v, z_v, P_hPerp_v, phi_h_v, phi_v>;

    using SIDIS_independent_vars = decltype(
        make_independent_vars<E_beam_v, M_h_v, x_v, y_v, z_v, P_hPerp_v, phi_h_v, phi_v>());

    /** Constructs the sidis kinematics from the measured electron and pion momenta.
     */
    auto construct_SIDIS_variables() {
    using namespace insane::kine::dis;
      return make_independent_vars<E_beam_v, M_h_v, x_v, Q2_v, z_v, P_hPerp_v, phi_h_v, phi_v>()
          .add<M_target_v>([](const auto& v) constexpr {
            const double M_p = insane::masses::M_p / insane::units::GeV;
            return std::tuple<M_target_v>(M_p);
          })
          .add<nu_v, gamma_v>([](const auto& v) constexpr {
            const auto& M  = std::get<M_target_v>(v);
            const auto& Q2 = std::get<Q2_v>(v);
            const auto& xB = std::get<xB_v>(v);
            const double nu  = Q2 / (2.0 * M * xB);
            const double gamma = 2.0*M*xB/std::sqrt(Q2);
            return std::make_tuple(nu_v{nu}, gamma_v{gamma});
          })
          .add<E_prime_v, theta_v, y_v>([](const auto& v) constexpr {
            const auto&  E0 = std::get<E_beam_v>(v);
            const auto&  M  = std::get<M_target_v>(v);
            const auto&  nu = std::get<nu_v>(v);
            const auto&  Q2 = std::get<Q2_v>(v);
            const auto&  xB = std::get<xB_v>(v);
            const double y  = nu / E0;
            return std::tuple<E_prime_v, theta_v, y_v>(
                E0 - nu, 2.0 * std::asin(M * xB * y / std::sqrt(Q2 * (1 - y))), y);
          })
          .add<epsilon_v>([](const auto& v) constexpr {
            const auto& gamma = std::get<Q2_v>(v);
            const auto& y  = std::get<y_v>(v);
            return (1.0 - y - 0.25*gamma*gamma*y*y)/(1.0 -y + 0.5*y*y + 0.25*gamma*gamma*y*y);
          })
          .add<W_v>([](const auto& v) constexpr {
            const auto& M  = std::get<M_target_v>(v);
            const auto& Q2 = std::get<Q2_v>(v);
            const auto& y  = std::get<y_v>(v);
            const auto& x = std::get<xB_v>(v);
            return std::sqrt(M*M - Q2 +Q2/x);
          })
          .add<k_vec>([](const auto& v) constexpr {
            const double M_e = 0.000511;
            const auto&  E0  = std::get<E_beam_v>(v);
            return ROOT::Math::XYZTVector{0.0, 0.0, std::sqrt(E0 * E0 - M_e * M_e), E0};
          })
          .add<k_prime_vec>([](const auto& v) constexpr {
            const double M_e   = 0.000511;
            const auto&  E0    = std::get<E_beam_v>(v);
            const auto&  Ep    = std::get<E_prime_v>(v);
            const auto&  th    = std::get<theta_v>(v);
            const auto&  ph    = std::get<phi_v>(v);
            const double sinth = std::sin(th);
            const double costh = std::cos(th);
            const double sinph = std::sin(ph);
            const double cosph = std::cos(ph);
            return ROOT::Math::XYZTVector{sinth * cosph * Ep, sinth * sinph * Ep, costh * Ep, Ep};
          })
          .add<q_vec>([](const auto& v) constexpr {
            const ROOT::Math::XYZTVector& k  = std::get<k_vec>(v);
            const ROOT::Math::XYZTVector& kp = std::get<k_prime_vec>(v);
            return ROOT::Math::XYZTVector(k - kp);
          })
          .add<theta_q_v>([](const auto& v) constexpr {
            const ROOT::Math::XYZTVector& q  = std::get<q_vec>(v);
            return q.Theta();
          })
          .add<E_h_v,P_h_v,theta_h_v>([](const auto& v) constexpr {
            const double M_pi = std::get<M_h_v>(v);
            const double z    = std::get<z_v>(v);
            const auto&  nu   = std::get<nu_v>(v);
            const auto&  P_hPerp   = std::get<P_hPerp_v>(v);
            const double Ep   = z*nu;
            const double Pp   = std::sqrt(Ep * Ep - M_pi * M_pi);
            const double PL = std::sqrt(Pp*Pp - P_hPerp*P_hPerp);
            return std::tuple<E_h_v, P_h_v,theta_h_v>(Ep, Pp, acos(PL/Pp));
          })
          .add<p_h_vec>([](const auto& v) constexpr {
            const auto&  Ep    = std::get<E_h_v>(v);
            const double Pp    = std::get<P_h_v>(v);
            const auto&  th    = std::get<theta_h_v>(v);
            const auto&  ph    = std::get<phi_h_v>(v);
            const double sinth = std::sin(th);
            const double costh = std::cos(th);
            const double sinph = std::sin(ph);
            const double cosph = std::cos(ph);
            return ROOT::Math::XYZTVector{sinth * cosph * Pp, sinth * sinph * Pp, costh * Pp, Ep};
          })
          .add<p_h_lab_vec>([](const auto& v) constexpr {
            const auto& th_q    = std::get<theta_q_v>(v);
            const auto& phi     = std::get<phi_v>(v);
            const ROOT::Math::XYZTVector& ph    = std::get<p_h_vec>(v);
            const ROOT::Math::XYZTVector  p_h_lab =
                ROOT::Math::Rotation3D(ROOT::Math::RotationZ(phi)) *
                ROOT::Math::Rotation3D(ROOT::Math::RotationY(-th_q)) * ph;
            return p_h_lab;
          })
          .add<theta_h_lab_v>([](const auto& v) constexpr {
            const ROOT::Math::XYZTVector& p  = std::get<p_h_lab_vec>(v);
            return p.Theta();
          })
          .add<phi_h_lab_v>([](const auto& v) constexpr {
            const ROOT::Math::XYZTVector& p  = std::get<p_h_lab_vec>(v);
            return p.Phi();
          })
          .add<W_prime_pi_v>([](const auto& v) constexpr {
            const double                  M_p = 0.938;
            const ROOT::Math::XYZTVector& q   = std::get<q_vec>(v);
            const ROOT::Math::XYZTVector& ph  = std::get<p_h_lab_vec>(v);
            return std::sqrt((ROOT::Math::XYZTVector{0.0, 0.0, 0.0, M_p} + q - ph)
                                 .Dot(ROOT::Math::XYZTVector{0.0, 0.0, 0.0, M_p} + q - ph));
          });
    }

    using SIDIS_vars = decltype(construct_SIDIS_variables());

    //@}
  } // namespace sidis

} // namespace insane::kine

#endif
