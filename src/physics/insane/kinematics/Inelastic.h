#ifndef insane_kinematics_Inelastic_HH
#define insane_kinematics_Inelastic_HH

#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/Elastic.h"
#include "insane/base/Helpers.h"

namespace insane::kine {

  /** DIS Kinematics.
   * Inelastic kinematics.
   * \ingroup kinematics
   */
  namespace dis {

    /** \addtogroup disKinematics DIS Kinematics
     *  Kinematic variables for Inelastic scattering.
     *  Note that these are also used in semi-inclusive and deep-exclusive kinematics.
     *
     *  \ingroup kinematics
     *  @{
     */

    /** @name Scattered electron variables.
     *  Electron kinematics.
     */
    //@{
    using insane::kine::elastic::M_target_v;///<
    using insane::kine::elastic::E_beam_v ; ///< \f$ E_{beam} = k_1^0  \f$
    using insane::kine::elastic::E_prime_v; ///< \f$ E^{\prime} = k_2^0 \f$
    using insane::kine::elastic::theta_v  ; ///< \f$ \theta_{e}\f$
    using insane::kine::elastic::phi_v    ; ///< \f$ \phi_{e}  \f$
    //@}

    /** @name Electron scattering vectors.
     */
    //@{
    using insane::kine::elastic::k_prime_vec;
    using insane::kine::elastic::k_vec;
    using insane::kine::elastic::q_vec;
    //@}

    /** @name DIS invariants and other variables.
     */
    //@{
    using x_v            = Var<struct x_tag>;            ///< \f$ x_B = q^2/p\cdot q \f$
    using xB_v           = x_v;//Var<struct x_tag>;            ///< \f$ x_B = q^2/p\cdot q \f$
    using y_v            = Var<struct y_tag>;            ///< \f$ y = p\cdot q/p\cdot k \f$
    using xi_Nachtmann_v = Var<struct xi_Nachtmann_tag>; ///<  \f$ \xi_{Nachtmann} = \frac{2 x}{1 +
    using xN_v = xi_Nachtmann_v;
                                                         ///<  \sqrt{1 + 4 x^2 M^2/Q^2}} \f$

    using  insane::kine::elastic::Q2_v      ; ///< \f$ Q^2 = -q^2 \f$
    using  insane::kine::elastic::nu_v      ; ///< \f$ \nu = E-E^{\prime} \f$
    using  insane::kine::elastic::nu_cm_v   ; ///< \f$ \nu_{cm} =  \f$
    using  insane::kine::elastic::epsilon_v ; ///< \f$ \epsilon = \frac{1-y}{1 - y +y^2/2} \f$
    using  insane::kine::elastic::W2_v      ; ///< \f$ W^2 = M^2 + Q^2/x - Q^2 \f$
    using  insane::kine::elastic::W_v       ; ///< \f$ W = \sqrt{M^2 + Q^2/x - Q^2} \f$
    using  insane::kine::elastic::q_v       ; ///< \f$ |\vec{q}|  \f$
    using  insane::kine::elastic::q_cm_v    ; ///< \f$ |\vec{q}_{cm}| \f$
    using  insane::kine::elastic::theta_q_v ; ///< \f$ \theta_q \f$ virtual photon angle.

    //@}

    using InelasticPrimaryVars = typename std::tuple<Q2_v, nu_v, W_v, x_v, y_v>;

    inline auto add_InelasticPrimaryVars = [](const auto& v) constexpr {
      if constexpr(insane::helpers::has_type_v<E_prime_v,decltype(v)>) {
      const auto&  E0    = std::get<E_beam_v>(v);
      const auto&  Ep    = std::get<E_prime_v>(v);
      const auto&  th    = std::get<theta_v>(v);
      const double sinth = std::sin(th / 2.0);
      const double Q2    = 4.0 * E0 * Ep * sinth * sinth;
      const double nu    = E0 - Ep;
      const auto&  M = insane::masses::M_p / insane::units::GeV; // std::get<M_recoil_v>(v);
      const double x = Q2 / (2.0 * M * nu);
      const double y = E0 / nu;
      const double W = std::sqrt(M * M - Q2 + 2.0 * M * nu);
      return InelasticPrimaryVars{Q2, nu, W, x, y};
      } else {
      return InelasticPrimaryVars{0.0,0.0,0.0,0.0,0.0};
      }
    };

    using InelasticVectorVars = std::tuple<k_vec, k_prime_vec, q_vec>;

    inline auto add_InelasticVectorVars = [](const auto& v) {
      const auto&  E0 = std::get<E_beam_v>(v);
      const auto&  M  = insane::masses::M_p / insane::units::GeV; // std::get<M_recoil_v>(v);
      const auto&  th = std::get<theta_v>(v);
      const double sinth = std::sin(th / 2.0);
      const auto&  Ep    = E0 / (1.0 + (2.0 * E0 / M) * (sinth * sinth));
      const auto&  Q2    = 4.0 * E0 * Ep * sinth * sinth;
      const auto&  nu    = E0 - Ep;
      const auto&  W     = std::sqrt(M * M - Q2 + 2.0 * M * nu);
      const double M_e   = 0.000511;
      const auto&  M_p   = std::get<M_target_v>(v);
      const auto&  ph    = std::get<phi_v>(v);
      const double costh = std::cos(th);
      const double sinph = std::sin(ph);
      const double cosph = std::cos(ph);
      const ROOT::Math::XYZTVector k{0.0, 0.0, std::sqrt(E0 * E0 - M_e * M_e), E0};
      const ROOT::Math::XYZTVector kp{sinth * cosph * Ep, sinth * sinph * Ep, costh * Ep, Ep};
      const ROOT::Math::XYZTVector q = k - kp;
      return InelasticVectorVars{k, kp, q};
    };

    using Inelastic_independent_vars =
        decltype(make_independent_vars<E_beam_v, E_prime_v, theta_v, phi_v>());

    inline auto construct_Inelastic_variables() {
      return make_independent_vars<E_beam_v, E_prime_v, theta_v, phi_v>()
          .add<Q2_v, nu_v>(
              [](const auto& v) constexpr {
                const auto&  E0    = std::get<E_beam_v>(v);
                const auto&  Ep    = std::get<E_prime_v>(v);
                const auto&  th    = std::get<theta_v>(v);
                const double sinth = std::sin(th / 2.0);
                return 4.0 * E0 * Ep * sinth * sinth;
              },
              [](const auto& v) constexpr {
                const auto& E0 = std::get<E_beam_v>(v);
                const auto& Ep = std::get<E_prime_v>(v);
                return E0 - Ep;
              }) // note that we are chaining here.
          .add<x_v, y_v>([](const auto& v) constexpr {
            const auto& E0  = std::get<E_beam_v>(v);
            const auto& Q2  = std::get<Q2_v>(v);
            const auto& nu  = std::get<nu_v>(v);
            double      M_p = 0.938;
            return std::tuple<x_v, y_v>(Q2 / (2.0 * M_p * nu), nu/E0);
          })
          .add<W_v>([](const auto& v) constexpr {
            const double M_p = 0.938;
            const auto&  Q2  = std::get<Q2_v>(v);
            const auto&  nu  = std::get<nu_v>(v);
            return std::sqrt(M_p * M_p - Q2 + 2.0 * M_p * nu);
          })
          .add<epsilon_v>([](const auto& v) constexpr {
            const double M_p = 0.938;
            const auto&  Q2  = std::get<Q2_v>(v);
            const auto&  nu  = std::get<nu_v>(v);
            const auto&  th    = std::get<theta_v>(v);
            const double tanth2 = std::tan(th/2.0);
            return 1.0/(1.0 + 2.0*(1.0+nu*nu/Q2)*tanth2*tanth2);
          })
          .add<k_vec>([](const auto& v) constexpr {
            const double M_e = 0.000511;
            const auto&  E0  = std::get<E_beam_v>(v);
            return ROOT::Math::XYZTVector{0.0, 0.0, std::sqrt(E0 * E0 - M_e * M_e), E0};
          })
          .add<k_prime_vec>([](const auto& v) constexpr {
            const double M_e   = 0.000511;
            const auto&  E0    = std::get<E_beam_v>(v);
            const auto&  Ep    = std::get<E_prime_v>(v);
            const auto&  th    = std::get<theta_v>(v);
            const auto&  ph    = std::get<phi_v>(v);
            const double sinth = std::sin(th);
            const double costh = std::cos(th);
            const double sinph = std::sin(ph);
            const double cosph = std::cos(ph);
            return ROOT::Math::XYZTVector{sinth * cosph * Ep, sinth * sinph * Ep, costh * Ep, Ep};
          })
          .add<q_vec>([](const auto& v) constexpr {
            const ROOT::Math::XYZTVector& k  = std::get<k_vec>(v);
            const ROOT::Math::XYZTVector& kp = std::get<k_prime_vec>(v);
            return ROOT::Math::XYZTVector(k - kp);
          })
          .add<theta_q_v>([](const auto& v) constexpr {
            const ROOT::Math::XYZTVector& q  = std::get<q_vec>(v);
            return q.Theta();
          });
    }

    using Inelastic_vars = decltype(construct_Inelastic_variables());
    //@}

  } // namespace dis
} // namespace insane::kine

#endif
