#ifndef insane_kinematics_DeepExclusive_HH
#define insane_kinematics_DeepExclusive_HH

#include "insane/kinematics/Inelastic.h"
#include "insane/kinematics/Elastic.h"
#include "Math/GenVector/VectorUtil.h"
#include "Math/GenVector/Rotation3D.h"
#include "Math/GenVector/RotationZ.h"
#include "Math/GenVector/AxisAngle.h"
#include "Math/Vector3D.h"

namespace insane::kine {
  /** DVCS Kinematics.
   * \ingroup kinematics
   */
  namespace dvcs {


    /** \addtogroup dvcsKinematics DVCS Kinematics
     *
     *  Deeply Virtual Compton Scattering (DVCS) kinematics.
     *
     *  \f[ e + N \rightarrow e + N + \gamma \f]
     *
     *  \ingroup kinematics
     *  @{
     */

    double epsilon(    double Q2, double xB, double M=0.938);
    double Delta2_min( double Q2, double xB, double M=0.938) ;
    double Delta2_min2(double Q2, double xB, double M=0.938);
    double Delta2_max( double Q2, double xB, double M=0.938);
    double Delta2_perp(double xi, double Delta2, double D2m);

    double y_max(  double eps);
    double xi_BKM( double xB, double Q2, double Delta2);
    double eta_BKM(double xi, double Q2, double Delta2);


    using  insane::kine::dis::M_target_v;
    using  insane::kine::dis::E_beam_v;
    using  insane::kine::dis::E_prime_v;
    using  insane::kine::dis::theta_v;
    using  insane::kine::dis::phi_v;
    using  insane::kine::dis::nu_v;

    /** @name   Recoil kinematics.
     * @{
     */
    using insane::kine::elastic::M_recoil_v    ;
    using insane::kine::elastic::E_recoil_v    ;
    using insane::kine::elastic::P_recoil_v    ;
    using insane::kine::elastic::theta_recoil_v;
    using insane::kine::elastic::phi_recoil_v  ;
    //@}

    using insane::kine::dis::Q2_v;
    using insane::kine::dis::W_v; ///< \f$ W \f$
    using insane::kine::dis::y_v;
    using xB_v = typename insane::kine::dis::x_v; /// xB is not the same as DVCS x
    using x_v     = insane::kine::Var<struct x_dvcs_tag>;  ///< DVCS x
    using xi_v = insane::kine::Var<struct xi_dvcs_tag>; ///< DVCS xi
    using gamma_v = insane::kine::Var<struct gamma_dvcs_tag>; ///< \f$ \gamma = 2xM/Q = Q/\nu \f$

    /** Mandelstam.
     */
    using s_v     = insane::kine::Var<struct s_tag>;
    using t_v     = insane::kine::Var<struct t_tag>;
    using t_min_v = insane::kine::Var<struct t_min_tag>;
    using t_max_v = insane::kine::Var<struct t_max_tag>;
    using tau_v   = insane::kine::Var<struct tau_tag>;

    using Delta2_v   = insane::kine::Var<struct Delta2_tag>;
    using y_max_v    = insane::kine::Var<struct y_max_tag>;
    using eps_v      = insane::kine::Var<struct eps_tag>;
    using phi_dvcs_v = insane::kine::Var<struct phi_dvcs_tag>; ///< angle between lepton and photon/hadron planes in VP frame.
    using phi_S_v = insane::kine::Var<struct phi_S_tag>; ///< Target spin angle in VP frame.

    /** \name photon variables
     *  Note the different frames.
     * @{
     */
    using theta_q1_v  = insane::kine::Var<struct theta_q1_tag>; ///< lab frame angle
    using nu1_cm_v    = insane::kine::Var<struct nu1_cm_tag>;   ///< CM frame energy
    using nu2_cm_v    = insane::kine::Var<struct nu2_cm_tag>; /// CM frame energy
    using theta_q2_v  = insane::kine::Var<struct theta_q2_tag>; ///< angle in the virtual photon frame
    using phi_q2_v  = insane::kine::Var<struct phi_q2_tag>; ///< phi angle in VP frame (and cm frame).

    using q1_v        = insane::kine::Var<struct q1_tag>;
    using q1_cm_v     = insane::kine::Var<struct q1_cm_tag>;
    using q2_v        = insane::kine::Var<struct q2_tag>;
    using q2_cm_v     = insane::kine::Var<struct q2_cm_tag>;
    //@}

    /** \name Virutal photon frame vectors.
     *  Here the virtual photon is in the -z direction and the electron vectors are in in the x-z plane.
     *  See https://inspirehep.net/literature/1724966.
     *  @{
     */
    using k1_vec      = insane::kine::FourVec<struct k1_vec_tag>;
    using k2_vec      = insane::kine::FourVec<struct k2_vec_tag>;
    using q1_vec      = insane::kine::FourVec<struct q1_vec_tag>;
    using q2_vec      = insane::kine::FourVec<struct q2_vec_tag>;
    using p1_vec      = insane::kine::FourVec<struct p1_vec_tag>;
    using p2_vec      = insane::kine::FourVec<struct p2_vec_tag>;
    //@}

    /** \name Lab frame (target at rest) vectors.
     * The electron beam is along z and target is at rest.
     *@{
     */
    using k1_lab_vec = insane::kine::FourVec<struct k1_labvec_tag>;
    using k2_lab_vec = insane::kine::FourVec<struct k2_labvec_tag>;
    using p1_lab_vec = insane::kine::FourVec<struct p1_labvec_tag>;
    using p2_lab_vec = insane::kine::FourVec<struct p2_labvec_tag>;
    using q1_lab_vec = insane::kine::FourVec<struct q1_labvec_tag>;
    using q2_lab_vec = insane::kine::FourVec<struct q2_labvec_tag>;
    //@}

    using DVCSPrimaryVars = typename std::tuple<E_beam_v, xB_v, Q2_v, t_v, phi_dvcs_v, phi_S_v,phi_v >;
    using DVCSExpVars     = typename std::tuple<E_beam_v, E_prime_v, theta_v, phi_v, q2_v, theta_q2_v,phi_q2_v>;

    //auto add_DVCSPrimaryVars = [](const auto& v) constexpr {
    //  const auto&  E0    = std::get<E_beam_v>(v);
    //  const auto&  M     = insane::masses::M_p / insane::units::GeV; // std::get<M_recoil_v>(v);
    //  const auto&  th    = std::get<theta_v>(v);
    //  const double sinth = std::sin(th / 2.0);
    //  const auto&  Ep    = E0 / (1.0 + (2.0 * E0 / M) * (sinth * sinth));
    //  const auto&  Q2    = 4.0 * E0 * Ep * sinth * sinth;
    //  const auto&  nu    = E0 - Ep;
    //  const auto&  W     = std::sqrt(M * M - Q2 + 2.0 * M * nu);
    //  return ElasticPrimaryVars{M,Ep,Q2,nu,W};
    //};
    inline auto construct_DVCS_Kinematics(){
      //return make_independent_vars<E_beam_v, E_prime_v, theta_v, phi_v, q2_v, theta_q2_v, phi_q2_v > ()
      return make_independent_vars<E_beam_v, xB_v, Q2_v, t_v, phi_dvcs_v, phi_S_v, phi_v>()
          .add<M_target_v, M_recoil_v>([](const auto& v) constexpr {
            const double M_p = insane::masses::M_p / insane::units::GeV;
            return std::tuple<M_target_v, M_recoil_v>(M_p, M_p);
          })
          .add<nu_v, gamma_v>([](const auto& v) constexpr {
            const auto& M  = std::get<M_target_v>(v);
            const auto& Q2 = std::get<Q2_v>(v);
            const auto& xB = std::get<xB_v>(v);
            const double nu  = Q2 / (2.0 * M * xB);
            const double gamma = std::sqrt(Q2)/nu;
            return std::make_tuple(nu_v{nu}, gamma_v{gamma});
          })
          .add<q2_v>([](const auto& v) constexpr {
            const auto& M  = std::get<M_target_v>(v);
            const auto& Q2 = std::get<Q2_v>(v);
            const auto& t  = std::get<t_v>(v);
            const auto& nu = std::get<nu_v>(v);
            const double nu2 = nu*(1.0 + t/(2.0*M*nu));
            return std::make_tuple(q2_v{nu2});
          })
          .add<theta_q2_v>([](const auto& v) constexpr {
            const auto& M  = std::get<M_target_v>(v);
            const auto& Q2 = std::get<Q2_v>(v);
            const auto& t  = std::get<t_v>(v);
            const auto& xB = std::get<xB_v>(v);
            const auto& gamma = std::get<gamma_v>(v);
            return std::acos( (-1.0/std::sqrt(1.0 + gamma*gamma))*(1.0 + (gamma*gamma/2.0)*(1.0+t/Q2)/(1.0+xB*t/Q2)));
          })
          .add<E_prime_v, theta_v, y_v>([](const auto& v) constexpr {
            const auto&  E0 = std::get<E_beam_v>(v);
            const auto&  M  = std::get<M_target_v>(v);
            const auto&  nu = std::get<nu_v>(v);
            const auto&  Q2 = std::get<Q2_v>(v);
            const auto&  xB = std::get<xB_v>(v);
            const double y  = nu / E0;
            return std::tuple<E_prime_v, theta_v, y_v>(
                E0 - nu, 2.0 * std::asin(M * xB * y / std::sqrt(Q2 * (1 - y))), y);
          })
          .add<s_v>([](const auto& v) constexpr {
            const auto& M  = std::get<M_target_v>(v);
            const auto& Q2 = std::get<Q2_v>(v);
            const auto& y  = std::get<y_v>(v);
            const auto& x = std::get<xB_v>(v);
            return Q2/(x*y)  + M*M;
          })
          .add<W_v>([](const auto& v) constexpr {
            const auto& M  = std::get<M_target_v>(v);
            const auto& Q2 = std::get<Q2_v>(v);
            const auto& y  = std::get<y_v>(v);
            const auto& x = std::get<xB_v>(v);
            return std::sqrt(M*M - Q2 +Q2/x);
          })
          .add<k1_vec, k2_vec, q1_vec, q2_vec, p1_vec, p2_vec>(
              [](const auto& v) {
                const auto&  E0           = std::get<E_beam_v>(v);
                const auto&  t            = std::get<t_v>(v);
                const auto&  gamma        = std::get<gamma_v>(v);
                const auto&  y            = std::get<y_v>(v);
                const auto&  xB           = std::get<xB_v>(v);
                const auto&  Q2           = std::get<Q2_v>(v);
                const double cos_theta_l1 = -1.0/std::sqrt(1.0 + gamma*gamma)*(1.0 + y*gamma*gamma/2.0);
                const double sin_theta_l1 = gamma*std::sqrt((1.0-y - y*y*gamma*gamma/4.0)/(1.0 + gamma*gamma));
                const double sin_theta_l2 = sin_theta_l1 / (1.0 - y);
                const double cos_theta_l2 = (cos_theta_l1 + y*std::sqrt(1.0 + gamma*gamma))/(1.0 - y);
                /// \todo check that E' = Q/(gamma*y)
                const double Ep = E0*(1.0-y);
                //const double cos_theta =
                //    ((-1.0 / std::sqrt(1.0 + gamma * gamma)) *
                //     (1.0 + (gamma * gamma / 2.0) * (1.0 + t / Q2) / (1.0 + xB * t / Q2)));
                //const double q2 = (std::sqrt(Q2)/gamma)*(1.0+ xB*t/Q2);
                const auto& nu1      = std::get<nu_v>(v);
                const auto& nu2      = std::get<q2_v>(v);
                const auto& theta_q2 = std::get<theta_q2_v>(v);
                const auto&                  phi   = std::get<phi_dvcs_v>(v);
                const ROOT::Math::XYZTVector k1(E0 * sin_theta_l1,0.0, E0 * cos_theta_l1, E0);
                const ROOT::Math::XYZTVector k2(Ep * sin_theta_l2,0.0, Ep * cos_theta_l2, Ep);
                const ROOT::Math::XYZTVector q1(0.0,0.0, -nu1*std::sqrt(1.0+gamma*gamma), nu1);
                const ROOT::Math::XYZTVector q2(nu2 * std::cos(phi) * std::sin(theta_q2),
                                                nu2 * std::sin(phi) * std::sin(theta_q2),
                                                nu2 * std::cos(theta_q2), nu2);
                const auto&  M_p          = std::get<M_target_v>(v);
                const ROOT::Math::XYZTVector p1(0,0,0, M_p);
                const auto p2 = k1 - k2 + p1 - q2;
                /// \todo check that  p2.p2 = Mp^2
                return std::tuple<k1_vec, k2_vec, q1_vec, q2_vec, p1_vec, p2_vec>(
                                    k1, k2, q1, q2, p1, p2 );
              })
          .add<k1_lab_vec, k2_lab_vec, q1_lab_vec, q2_lab_vec, p1_lab_vec, p2_lab_vec>(
              [](const auto& v) {
                using ROOT::Math::XYZTVector;
                using ROOT::Math::Rotation3D;
                using ROOT::Math::RotationZ;
                using ROOT::Math::AxisAngle;
                const auto&  M_p          = std::get<M_target_v>(v);
                const auto&  E0           = std::get<E_beam_v>(v);
                const auto&  t            = std::get<t_v>(v);
                const auto&  gamma        = std::get<gamma_v>(v);
                const auto&  y            = std::get<y_v>(v);
                const auto&  xB           = std::get<xB_v>(v);
                const auto&  Q2           = std::get<Q2_v>(v);
                const double cos_theta_ee = (1.0 - y - y * y * gamma * gamma / 2.0) / (1.0 - y);
                const double theta        = std::acos(cos_theta_ee);
                const auto&  phi          = std::get<phi_v>(v);
                const double Ep           = E0 * (1.0 - y);
                const XYZTVector& k1_VPF = std::get<k1_vec>(v);
                const XYZTVector& q2_VPF = std::get<q2_vec>(v);
                const XYZTVector& p2_VPF = std::get<q2_vec>(v);

                const XYZTVector k1(0.0, 0.0, E0, E0);
                const XYZTVector k2(Ep * std::cos(phi) * std::sin(theta),
                                    Ep * std::sin(phi) * std::sin(theta), Ep * std::cos(theta), Ep);
                const XYZTVector q1 = k1 - k2;
                const Rotation3D rot( Rotation3D(RotationZ(phi)) *
                    AxisAngle(k1.Vect().Cross(k1_VPF.Vect()),
                                          ROOT::Math::VectorUtil::Angle(k1.Vect(), k1_VPF.Vect())));
                const XYZTVector q2 = rot*q2_VPF;

                const XYZTVector p1(0,0,0, M_p);
                const XYZTVector p2 = k1 - k2 + p1 - q2;
                /// \todo  check the inv. mass
                return std::tuple<k1_lab_vec, k2_lab_vec, q1_lab_vec, q2_lab_vec, p1_lab_vec,
                                  p2_lab_vec>(k1, k2, q1, q2, p1, p2);
              })
          //.add<Q2_v, xB_v, x_v, y_v, t_v, s_v>([](const auto& v) constexpr {
          //  const ROOT::Math::XYZTVector& k1 = std::get<k1_lab_vec>(v);
          //  const ROOT::Math::XYZTVector& p1 = std::get<p1_lab_vec>(v);
          //  const ROOT::Math::XYZTVector& p2 = std::get<p2_lab_vec>(v);
          //  const ROOT::Math::XYZTVector& q1 = std::get<q1_lab_vec>(v);
          //  const ROOT::Math::XYZTVector& q2 = std::get<q2_lab_vec>(v);
          //  const double&                 Q2 = -1.0 * (q1.mag2());
          //  const double&                 xB = Q2 / (2.0 * (p1.Dot(q1)));
          //  const double&                 x  = (q1 + q2).mag2() / (4.0 * ((q1 + q2).Dot(p1 + p2)));
          //  const double&                 y  = (p1.Dot(q1)) / (p1.Dot(k1));
          //  const double&                 t  = (p1 - p2).mag2();
          //  const double&                 s  = (p1 + q1).mag2();
          //  return std::make_tuple(Q2_v{Q2}, xB_v{xB}, x_v{x}, y_v{y}, t_v{t}, s_v{s});
          //})
          .add<Delta2_v, xi_v>([](const auto& v) constexpr {
            const auto&  t  = std::get<t_v>(v);
            const auto&  xB = std::get<xB_v>(v);
            const auto&  Q2 = std::get<Q2_v>(v);
            const double x  = xB / (2.0 - xB);
            const double xi = xB * (1.0 + t / (2.0 * Q2)) / (2.0 - xB + xB * t / Q2);
            return std::tuple<Delta2_v, xi_v>(std::abs(t), xi);
          })
          //.add<phi_dvcs_v>([](const auto& v) constexpr {
          //  const ROOT::Math::XYZTVector& k1 = std::get<k1_lab_vec>(v);
          //  const ROOT::Math::XYZTVector& q1 = std::get<q1_lab_vec>(v);
          //  const ROOT::Math::XYZTVector& q2 = std::get<q2_lab_vec>(v);
          //  return ROOT::Math::VectorUtil::DeltaPhi(q2.Vect().Cross(q1.Vect()),
          //                                          q1.Vect().Cross(k1.Vect()));
          //})
          .add<nu1_cm_v, q1_cm_v>([](const auto& v) constexpr {
            const auto&  E0 = std::get<E_beam_v>(v);
            const auto&  M  = std::get<M_target_v>(v);
            const auto&  nu = std::get<nu_v>(v);
            const auto&  Q2 = std::get<Q2_v>(v);
            const auto&  xB = std::get<xB_v>(v);
            const auto&  y  = std::get<y_v>(v);
            //const double s  = M * M - Q2 + 2 * M * nu; // W^2
            //const auto&  s = std::get<s_v>(v);
            const double nu_cm =
                std::sqrt(std::pow(M * nu - Q2, 2.0) / (M * M + 2.0 * M * nu - Q2));
            const double q_cm = std::sqrt(nu_cm * nu_cm + Q2);
            return std::tuple<nu1_cm_v, q1_cm_v>(nu_cm, q_cm);
          })
          .add<t_min_v, t_max_v, y_max_v>(
              [](const auto& v) constexpr {
                using namespace insane::physics;
                const auto& M     = std::get<M_target_v>(v);
                const auto& xB    = std::get<xB_v>(v);
                const auto& gamma  = std::get<gamma_v>(v);
                const auto& Q2    = std::get<Q2_v>(v);
                const auto& nu_cm = std::get<nu1_cm_v>(v);
                const auto& q_cm  = std::get<q1_cm_v>(v);
                const auto& nu = std::get<nu_v>(v);
                const double s = -Q2+M*M + 2.0*M*nu; // s for q1+p1 (not electron)
                const double sqrtgam = std::sqrt(1.0+gamma*gamma);
                const double tmin = Q2*(1.0-sqrtgam + gamma*gamma/2.0)/(xB*(1.0-sqrtgam+gamma*gamma/(2.0*xB)));
                return std::tuple<t_min_v, t_max_v, y_max_v>(
                    //tmin,
                    -Q2 - ((s - M * M) / (std::sqrt(s))) * (nu_cm - q_cm),
                    -Q2 - ((s - M * M) / (std::sqrt(s))) * (nu_cm + q_cm),
                    y_max(epsilon(Q2, xB, M)));
              })
          .add<eps_v>([](const auto& v) constexpr {
            using namespace insane::physics;
            const auto& M = std::get<M_target_v>(v);
            // const auto& s     = std::get<s_v>(v);
            const auto& xB = std::get<xB_v>(v);
            const auto& Q2 = std::get<Q2_v>(v);
            return epsilon(Q2, xB, M);
          })
          //.add<theta_q1_v, theta_q2_v>([](const auto& v) constexpr {
          //  const ROOT::Math::XYZTVector& q1 = std::get<q1_lab_vec>(v);
          //  const ROOT::Math::XYZTVector& q2 = std::get<q2_lab_vec>(v);
          //  return std::make_tuple(theta_q1_v{q1.Theta()}, theta_q2_v{q2.Theta()});
          //})
          //.add<q2_vec, p2_vec>( // Four vectors k and k'
          //    [](const auto& v) {
          //      const auto&                   M   = std::get<M_target>(v);
          //      const auto&                   t   = std::get<t>(v);
          //      const auto&                   Q2  = std::get<Q2>(v);
          //      const auto&                   nu  = std::get<nu>(v);
          //      const auto&                   phi = std::get<phi>(v);
          //      const ROOT::Math::XYZTVector& k1  = std::get<k_beam_vec>(v);
          //      const ROOT::Math::XYZTVector& k2  = std::get<k_prime_vec>(v);
          //      const ROOT::Math::XYZTVector& q1  = std::get<q1_vec>(v);
          //      const ROOT::Math::XYZTVector  p1(0.0, 0.0, 0.0, M);
          //      const ROOT::Math::XYZVector   n_gamma = k1.Vect().Cross(k2.Vect());
          //      const ROOT::Math::AxisAngle   rot_to_gamma_z(n_gamma, q1.Theta());
          //      const ROOT::Math::AxisAngle   rot_from_gamma_z(n_gamma, -q1.Theta());
          //      const ROOT::Math::XYZTVector  q1_B  = rot_to_gamma_z(q1);
          //      const double                  E2_A  = (2.0 * M * M - t) / (2.0 * M);
          //      const double                  P2_A  = TMath::Sqrt(E2_A * E2_A - M * M);
          //      const double                  nu2_A = M + nu - E2_A;
          //      const double                  cosTheta_qq2_B =
          //          (t + Q2 + 2.0 * nu * nu2_A) / (2.0 * nu2_A * q1.Vect().r());
          //      const double theta_qq2_B = TMath::ACos(cosTheta_qq2_B);
          //      const double sintheta_qp2_B =
          //          nu2_A * TMath::Sin(theta_qq2_B) / TMath::Sqrt(E2_A * E2_A - M * M);
          //      const double                 theta_qp2_B = TMath::ASin(sintheta_qp2_B);
          //      const double                 phi_p2      = q1_B.Phi() + phi;
          //      const ROOT::Math::XYZTVector p2_B(P2_A * std::cos(phi_p2) * std::sin(theta_qp2_B),
          //                                        P2_A * std::sin(phi_p2) * std::sin(theta_qp2_B),
          //                                        P2_A * std::cos(theta_qp2_B), E2_A);
          //      const ROOT::Math::XYZTVector q2_B(
          //          nu2_A * std::cos(phi_p2 + M_PI) * std::sin(theta_qq2_B),
          //          nu2_A * std::sin(phi_p2 + M_PI) * std::sin(theta_qq2_B),
          //          nu2_A * std::cos(theta_qq2_B), nu2_A);
          //      return std::tuple<q2_vec, p2_vec>(rot_from_gamma_z(q2_B), rot_from_gamma_z(p2_B));
          //    })
          ;
    }

      //@}
  } // namespace dvcs
} // namespace insane::kine
#endif
