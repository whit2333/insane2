#ifndef insane_kinematics_KinematicFunctions_HH
#define insane_kinematics_KinematicFunctions_HH

#include "insane/base/SystemOfUnits.h"
#include "insane/base/PhysicalConstants.h"
#include <cmath>
#include "TMath.h"

/** \addtogroup kinematics
 *
 */
namespace insane::kine {

  using units::GeV;
  using units::M_p;
  using units::M_pion;

  /** @name DIS Kinematic functions
   *  Various kinematic functions for DIS experiments.
   *
   * \ingroup DIS
   *  @{
   */
  double Q2(double en, double enprime, double theta);
  double Q2_EEprimeTheta(double en, double enprime, double theta);
  // double Q2_max(double en, double x, ouble Mtarg = M_p/GeV);
  double Q2_xW(double x, double W, double Mtarg = M_p / GeV);

  double xBjorken(double Qsq, double nu, double Mtarg = M_p / GeV);
  double xBjorken_EEprimeTheta(double e, double eprime, double theta);
  double xi_Nachtmann(double x, double Q2);

  double nu(double en, double enprime);
  double nu_WsqQsq(double Wsq, double Qsq, double Mtarg = M_p / GeV);

  double W2(double Qsq, double nu, double Mtarg = M_p / GeV);

  double epsilon(double en, double enprime, double theta);
  double epsilon_xQ2(double x, double Q2, double Mtarg = M_p / GeV);

  // double W2_xQsq(double x, double Qsq, double Mtarg = M_p/GeV);
  double W_xQsq(double x, double Qsq, double Mtarg = M_p / GeV);
  double W_xQ2(double x, double Q2, double Mtarg = M_p / GeV);
  double W_EEprimeTheta(double e, double eprime, double theta);

  double xBjorken_WQsq(double W, double Qsq, double Mtarg = M_p / GeV);
  double xBjorken_WQ2(double W, double Q2, double Mtarg = M_p / GeV);

  double BeamEnergy_xQ2y(double x, double Q2, double y, double Mtarg = M_p / GeV);
  double BeamEnergy_epsilonQ2W2(double eps, double Q2, double W2, double Mtarg = M_p / GeV);

  double Eprime_xQ2y(double x, double Q2, double y, double Mtarg = M_p / GeV);
  double Eprime_W2theta(double W2, double theta, double Ebeam, double Mtarg = M_p / GeV);
  double Eprime_epsilonQ2W2(double eps, double Q2, double W2, double Mtarg = M_p / GeV);

  double Theta_xQ2y(double x, double Q2, double y, double Mtarg = M_p / GeV);
  double Theta_epsilonQ2W2(double eps, double Q2, double W2, double Mtarg = M_p / GeV);
  //@}

  /// \f$ \tau = \frac{Q^2}{4M^2} \f$
  double tau(double Qsq, double Mtarg = M_p / GeV);

  /// \f$ |\vec{q}| = \sqrt{Q^2(1 + \tau)} \f$
  double q_abs(double Qsq, double Mtarg = M_p / GeV);

  double Sig_Mott(double en, double theta);
  double fRecoil(double en, double theta, double Mtarg = M_p / GeV);

  /** Hand convention for the virtual photon flux factor.
   * K is the energy requred for a real photon to create the final state
   * L.N. Hand Phys.Rev. 129 (1963) 1834-1846
   *
   * \ingroup electroproduction
   */
  double K_Hand(double x, double Q2);

  /**  Virtual photon flux.
   * \f$ \Gamma_v \f$.
   *
   * \ingroup electroproduction
   */
  inline double Gamma_v(double Ebeam, double Eprime, double Q2) {
    double nu      = Ebeam - Eprime;
    double q       = std::sqrt(Q2 + nu * nu);
    double epsilon = 1.0 / (1.0 + 2.0 * (Q2 + nu * nu) / (4.0 * Ebeam * Eprime - Q2));
    return (insane::constants::alpha / (2 * M_PI * M_PI)) * (Eprime / Ebeam) *
           (q / (1.0 - epsilon)) / Q2;
  }

} // namespace insane::kine

#endif
