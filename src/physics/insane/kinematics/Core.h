#ifndef insane_kinematics_core_HH
#define insane_kinematics_core_HH
#include <array>
#include <cmath>
#include <functional>
#include <iostream>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

// See the excellent Fluentcpp blog series on strong types.
// https://www.fluentcpp.com/2017/05/23/strong-types-inheriting-functionalities-from-underlying/
#include "insane/base/NamedType/named_type.hpp"
#include "insane/base/NamedType/underlying_functionalities.hpp"

#include "insane/base/Helpers.h"
#include "insane/kinematics/kinematics_impl.h"

#define InSANE_Var(vname) using vname##_v =  insane::kine::Var<struct vname##_tag>

namespace insane {

  /** \ingroup kinematics
   * Kinematic variables namespace.
   *
   * Includes the main functionality for defining kinematic variables.
   * Each variable has an associated unique type defined via a type tag.
   *
   */
  namespace kine {

    /** \addtogroup kinematics  Core kinematics library
     *
     * Strongly typed kinematic variables.
     *
     * Each variable has an associated unique type defined via a type tag.
     *
     *  @{
     */

    /** \name Variable helpers
     * @{
     */
    /// Define a double variable type.
    template <typename Tag>
    using Var = impl::Var<Tag>;

    /// Define a three-vector (3 double) variable type.
    template <typename Tag, int N>
    using VarVec = impl::VarVec<Tag, N>;

    /// Define a four-vector (4 double) variable type.
    template <typename Tag>
    using FourVec = impl::FourVec<Tag>;

    template <typename T, typename... Vs>
    auto make_vars() {
      return impl::Definable<impl::FVariables<T, Vs...>, impl::ConstructedFunctionSet>{};
    }

    /** Helper making independent vars using FVariables specialization.
     * \ingroup Kinematics
     */
    template <typename... Vs>
    auto make_independent_vars() {
      return impl::Definable<impl::FVariables<std::tuple<Vs...>>, impl::ConstructedFunctionSet>{}();
    }

    //@}


    //template <typename V1, typename V2, typename FuncSet>
    //struct VariableDerivative {
    //  static_assert(insane::helpers::has_type<V1,typename FuncSet::Vars_t>::value, "type V1 is missing from FunctionSet");
    //  static_assert(insane::helpers::has_type<V2,typename FuncSet::Vars_t>::value, "type V2 is missing from FunctionSet");

    //  using InputVars_t            = std::decay_t<typename FuncSet::InputVars_t>;
    //};

   //   double operator()(const VarArray& args ) {
   //     // 1D function
   //     double x0 = std::get<N>(args);
   //     auto fx = [=](double xx) { 
   //       VarArray dv = args; //copy args
   //       std::get<N>(dv) = xx; // set the arg to xx 
   //       return func(is,dv);
   //     };
   //     ROOT::Math::Functor1D wf(fx);
   //     ROOT::Math::Derivator der;
   //     der.SetFunction(wf);
   //     return der.Eval(x0);
   //   }

  } // namespace kine
} // namespace insane

#endif
