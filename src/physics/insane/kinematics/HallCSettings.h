#ifndef insane_hallc_settings_HH
#define insane_hallc_settings_HH 1

#include <array>
#include <iostream>
#include <map>
#include <tuple>
#include <vector>
#define _USE_MATH_DEFINES
#include <cmath>

namespace insane {
  namespace hallc {

    namespace shms {
      const double SHMS_dtheta      = 0.024;                         // 24 mrad
      const double SHMS_dphi        = 0.040;                         // 40 mrad
      const double SHMS_dP_low      = 0.1;                           // -10%
      const double SHMS_dP_high     = 0.22;                          //  22%
      const double SHMS_solid_angle = 4.0 * SHMS_dtheta * SHMS_dphi; // 4 msr
    }
    namespace hms {
      const double HMS_dtheta      = 0.04;                        //
      const double HMS_dphi        = 0.04;                        //
      const double HMS_dP_low      = 0.09;                        // -9%
      const double HMS_dP_high     = 0.09;                        //  9%
      const double HMS_solid_angle = 4.0 * HMS_dtheta * HMS_dphi; // 4 msr
    }

    /** Hall C Spectrometer Settings.
     *
     * SHMS:
     *  Central Momentum:   Momentum Acceptance:     Momentum Resolution:
     *       2 to 11 GeV/c       -10% < δ < +22%          0.03%-0.08%
     *  Scattering Angle:  Horizontal acceptance:    Vertical acceptance:
     *       5.5 to 40°         +- 24mrad                 +- 40 mrad
     *  Solid angle acceptance:
     *       4.0 msr
     *  Vertex Length (90deg):  Vertex length res (Ytar):
     *       30cm                    0.1-0.3 cm
     *  Horizontal resolution (YPtar): Vertical resolution (XPtar):
     *       0.5-1.2 mrad                   0.3-1.1 mrad
     *
     * HMS:
     * https://www.jlab.org/Hall-C/equipment/HMS/performance.html
     *
     *
     * Coordinate system used:
     * z = beam direction
     * x = SHMS (beam left)
     * y = up (towards hall ceiling)
     *
     */
    struct HallCSetting {

      // HMS detects electron
      double HMS_theta = 14.5 * M_PI / 180.0; //*degree;
      double HMS_p0    = 5.0;                 // GeV/c
      double HMS_phi   = M_PI;
      // SHMS detects hadron
      double SHMS_theta = 13.5 * M_PI / 180.0; //*degree;
      double SHMS_p0    = 3.0;                 // GeV/c
      double SHMS_phi   = 0.0;                 // SHMS sits on the +x side

      const double SHMS_dtheta      = shms::SHMS_dtheta;
      const double SHMS_dphi        = shms::SHMS_dphi;
      const double SHMS_dP_low      = shms::SHMS_dP_low;
      const double SHMS_dP_high     = shms::SHMS_dP_high;
      const double SHMS_solid_angle = shms::SHMS_solid_angle;
      const double HMS_dtheta       = hms::HMS_dtheta;
      const double HMS_dphi         = hms::HMS_dphi;
      const double HMS_dP_low       = hms::HMS_dP_low;
      const double HMS_dP_high      = hms::HMS_dP_high;
      const double HMS_solid_angle  = hms::HMS_solid_angle;

      double HMS_P_min() const { return HMS_p0 * (1.0 - HMS_dP_low); }
      double HMS_P_max() const { return HMS_p0 * (1.0 + HMS_dP_high); }
      double SHMS_P_min() const { return SHMS_p0 * (1.0 - SHMS_dP_low); }
      double SHMS_P_max() const { return SHMS_p0 * (1.0 + SHMS_dP_high); }

      double HMS_phi_min() const { return HMS_phi - HMS_dphi; }
      double HMS_phi_max() const { return HMS_phi + HMS_dphi; }
      double SHMS_phi_min() const { return SHMS_phi - SHMS_dphi; }
      double SHMS_phi_max() const { return SHMS_phi + SHMS_dphi; }

      double HMS_theta_min() const { return HMS_theta - HMS_dtheta; }
      double HMS_theta_max() const { return HMS_theta + HMS_dtheta; }
      double SHMS_theta_min() const { return SHMS_theta - SHMS_dtheta; }
      double SHMS_theta_max() const { return SHMS_theta + SHMS_dtheta; }

      void Print() const {
        std::cout << "HMS:  \n";
        std::cout << "   P0    = " << HMS_p0 << " GeV/c\n";
        std::cout << "   theta = " << HMS_theta * 180.0 / M_PI << " deg\n";
        std::cout << "   phi   = " << HMS_phi * 180.0 / M_PI << " deg\n";
        std::cout << "SHMS:  \n";
        std::cout << "   P0    = " << SHMS_p0 << " GeV/c\n";
        std::cout << "   theta = " << SHMS_theta * 180.0 / M_PI << " deg\n";
        std::cout << "   phi   = " << SHMS_phi * 180.0 / M_PI << " deg\n";
      }
    };

  } // namespace hallc
} // namespace insane

#endif
