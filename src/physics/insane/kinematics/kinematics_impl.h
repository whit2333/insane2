#ifndef insane_kinematics_kinematics_impl_HH
#define insane_kinematics_kinematics_impl_HH
#include <array>
#include <cmath>
#include <functional>
#include <iostream>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include "Math/Vector4D.h"
#include "Math/Vector3D.h"

// See the excellent Fluentcpp blog series on strong types.
// https://www.fluentcpp.com/2017/05/23/strong-types-inheriting-functionalities-from-underlying/
#include "insane/base/NamedType/named_type.hpp"
#include "insane/base/NamedType/underlying_functionalities.hpp"
#include "insane/base/Helpers.h"

namespace insane {

  namespace kine {

    namespace impl {

    using namespace fluent;

    /** Named strong type helper.
     */
    template <typename Tag>
    using Var = NamedType<double, Tag, KinVarFuncs>;

    template <typename Tag,int N>
    using VarVec = NamedType<std::array<double,N>, Tag, KinVarVecFuncs>;
    //, ImplicitlyConvertibleTo<std::array<double,N>> >;

    /** Named strong type helper.
     */
    template <typename Tag>
    using FourVec = NamedType<ROOT::Math::XYZTVector, Tag, Addable, Printable,
                          ImplicitlyConvertibleTo<ROOT::Math::XYZTVector>::templ>;



    /** Forward declaration of mixin.
     */
    template <typename>
    struct ConstructedFunctionSet;

    /** FunctionSet: new variables defined by functions.
     *
     *  \param FVars a FVariables template class.
     *  \param Fs    a std::tuple of lambdas that calculate values of NewVars_t
     * types.
     *
     *  The \c FunctionSet is the class that aggregates the named variables and their
     *  corresponding functions. The operator() is used to evalute the \c FunctionSet.
     *  \c FunctionSet s are chained together using add from the \c Definable template with
     *  the mixin class \c ConstructedFunctionSet.
     */
    template <typename FVars, typename Fs,
              typename = typename std::enable_if<helpers::is_tuple<Fs>::value>::type>
    struct FunctionSet : FVars {
      ///\todo Clean these up. Find better names.
      using IndependentVariables_t = std::decay_t<typename FVars::IndependentVariables_t>;
      using InputVars_t            = std::decay_t<typename FVars::InputVars_t>;
      using NewVars_t              = std::decay_t<typename FVars::NewVars_t>;
      using Vars_t                 = std::decay_t<typename FVars::Vars_t>;
      using VariableFuncs_t        = Fs;

      static const size_t N_funcs      = std::tuple_size<VariableFuncs_t>::value;
      static const size_t N_input_vars = FVars::N_input_vars;
      static const size_t N_new_vars   = FVars::N_new_vars;
      static const size_t N_vars       = FVars::N_vars;

      VariableFuncs_t functions;

      FunctionSet(const FVars& u, const Fs& f) : FVars(u), functions(f) {}
      FunctionSet(const FunctionSet& fs) : FVars(fs), functions(fs.functions) {}

      constexpr Vars_t ComputeValues(const IndependentVariables_t& v) const {
        if constexpr (std::is_same<IndependentVariables_t, InputVars_t>::value) {
          return std::tuple_cat(v, ComputeNewVars(v));
        } else {
          auto vars = FVars::input_variable_set.ComputeValues(v);
          return std::tuple_cat(vars, ComputeNewVars(vars));
        }
      }

      /** Operator().
       *  Templated return type because for the starting case we have
       *  IndependentVariables_t == InputVars_t.
       */
      template <class T = IndependentVariables_t>
      constexpr typename std::enable_if<!std::is_same<T, InputVars_t>::value, Vars_t>::type
      operator()(const IndependentVariables_t& v) const {
        return ComputeValues(v);
      }

      template <std::size_t I, typename F, typename Args>
      static constexpr auto eval_vars2(const F& f, const Args& a) {
        using Return_t      = std::decay_t<typename std::tuple_element<I, NewVars_t>::type>;
        using FuncReturn_t  = std::decay_t<insane::helpers::result_t<F>>;
        using ReturnTuple_t = std::decay_t<
            typename std::conditional<insane::helpers::is_tuple<FuncReturn_t>::value, FuncReturn_t,
                                      std::tuple<FuncReturn_t>>::type>;
        if constexpr (insane::helpers::is_tuple<FuncReturn_t>::value){
          return FuncReturn_t{f(a)};
        } else {
          return std::make_tuple(Return_t{f(a)});
        }
      }
      template <typename Funcs, typename Args, std::size_t... I>
      static constexpr auto eval_vars(const Funcs& f, const Args& a, std::index_sequence<I...>) {
        return std::tuple_cat(eval_vars2<I>(std::get<I>(f), a)...);
      }
      constexpr auto ComputeNewVars(const InputVars_t& v) const {
        return eval_vars(functions, v, std::make_index_sequence<N_funcs>{});
      }
      constexpr auto operator()(const InputVars_t& v) const { return ComputeNewVars(v); }
    };

    template <class...>
    struct is_FunctionSet : public std::false_type {};

    template <class... Vs>
    struct is_FunctionSet<ConstructedFunctionSet<Vs...>> : public std::true_type {};

    template <class... Vs>
    struct is_FunctionSet<FunctionSet<Vs...>> : public std::true_type {};

    //template <class...>
    //struct is_FVariables : public std::false_type {};
    //template <class... Vs>
    //struct is_FVariables<ConstructedFunctionSet<Vs...>> : public std::true_type {};
    //template <class... Vs>
    //struct is_FVariables<FunctionSet<Vs...>> : public std::true_type {};


    /** Definable Variables from functions.
     *
     *  Used to construct a FunctionSet from lambdas and wraps the lambda
     *  arguments with a set of standard variables.
     *
     * \param FVars a set of variables, FVariables.
     * \param Mixin Mixin class with functionality to declare or add more variables to FVariables..
     *
     * The new \c FunctionSet comes as a \c Mixin<FunctionSet> constructed using Definable::operator().
     */
    template <typename FVars, template <typename> class Mixin>
    struct Definable : FVars {
      using InputVars_t = std::decay_t<typename FVars::InputVars_t>;
      using NewVars_t   = std::decay_t<typename FVars::NewVars_t>;

      template <typename... Ts>
      Definable(Ts&&... t) : FVars(std::forward<Ts>(t)...) {}

      template <typename... Funcs>
      constexpr auto operator()(const Funcs&... fs) const noexcept {
        return t2t_impl(*this, std::make_tuple(fs...), std::make_index_sequence<sizeof...(fs)>{});
      }

      template <typename TUP, std::size_t... I>
      static constexpr auto t2t_impl(const Definable<FVars, Mixin>& fd, const TUP& t,
                                     std::index_sequence<I...>) noexcept {
        constexpr auto l_wrap = [](const auto& f) constexpr noexcept {
          return [&](const InputVars_t& args) { return f(args); };
        };
        using type = std::decay_t<decltype(std::make_tuple(l_wrap(std::get<I>(t))...))>;
        return Mixin<FunctionSet<FVars, type>>(FVars{fd},
                                               std::make_tuple(l_wrap(std::get<I>(t))...));
      }
    };


    /** Function Variables (to be defined with a lambda).
     *
     *  \param VarSet     is the previously defined variables
     *  \param NewVars... is a pack of new types computed from the InputVars.
     *
     *  Note the functions relating the new to the old have not yet been
     * specified.
     *
     */
    template <typename VarSet, typename... NewVars>
    struct FVariables {
      static_assert(is_FunctionSet<VarSet>::value,
                    "FVariables requires FunctionSet as first template argument");
      static_assert(std::tuple_size<std::tuple<NewVars...>>::value != 0,
                    "FVariables requires more than one new variables to be defined, hence, needs a second template argument.");
      using VarSet_t               = VarSet;
      using IndependentVariables_t = std::decay_t<typename VarSet::IndependentVariables_t>;
      using InputVars_t            = std::decay_t<typename VarSet::Vars_t>;
      using NewVars_t              = std::tuple<NewVars...>;
      using Vars_t =
          std::decay_t<decltype(std::tuple_cat(std::declval<InputVars_t>(), std::declval<NewVars_t>()))>;
      static const size_t N_input_vars = std::tuple_size<InputVars_t>::value;
      static const size_t N_new_vars   = std::tuple_size<NewVars_t>::value;
      static const size_t N_vars       = std::tuple_size<Vars_t>::value;

      VarSet_t input_variable_set;

      FVariables() noexcept {}
      FVariables(const VarSet& f) noexcept : input_variable_set(f) {}
      FVariables(const FVariables& t) noexcept : input_variable_set(t.input_variable_set) {}
    };

    /** Independent FVariables specialization.
     *
     *  Function variable specialization where the variable is an independent variable and 
     *  therefore has no lambda to calculate it. The pack Vs should be left empty.
     */
    template <typename... InVars, typename... Vs>
    struct FVariables<std::tuple<InVars...>, Vs...> {
      static_assert(std::tuple_size<std::tuple<Vs...>>::value == 0,
                    "FVariables requires Vs to be empty parameter pack");
      using IndependentVariables_t = std::tuple<InVars...>;
      using InputVars_t            = IndependentVariables_t;
      using NewVars_t              = std::tuple<Vs...>;
      using Vars_t =
          std::decay_t<decltype(std::tuple_cat(std::declval<InputVars_t>(), std::declval<NewVars_t>()))>;
      static const size_t N_input_vars = std::tuple_size<InputVars_t>::value;
      static const size_t N_new_vars   = std::tuple_size<NewVars_t>::value;
      static const size_t N_vars       = std::tuple_size<Vars_t>::value;

      constexpr FVariables() noexcept {}
      // void Print() const {
      //  // std::cout << __PRETTY_FUNCTION__ << "\n";
      //}
    };

    template <typename VarSet, typename... NewVars>
    struct FVariables<VarSet, std::tuple<NewVars...>> {
      static_assert(is_FunctionSet<VarSet>::value,
                    "FVariables requires FunctionSet as first template argument");
      static_assert(std::tuple_size<std::tuple<NewVars...>>::value != 0,
                    "FVariables requires more than one new variables to be defined, hence, needs a second template argument.");
      using VarSet_t               = VarSet;
      using IndependentVariables_t = std::decay_t<typename VarSet::IndependentVariables_t>;
      using InputVars_t            = std::decay_t<typename VarSet::Vars_t>;
      using NewVars_t              = std::tuple<NewVars...>;
      using Vars_t =
          std::decay_t<decltype(std::tuple_cat(std::declval<InputVars_t>(), std::declval<NewVars_t>()))>;
      static const size_t N_input_vars = std::tuple_size<InputVars_t>::value;
      static const size_t N_new_vars   = std::tuple_size<NewVars_t>::value;
      static const size_t N_vars       = std::tuple_size<Vars_t>::value;

      VarSet_t input_variable_set;

      FVariables() noexcept {}
      FVariables(const VarSet& f) noexcept : input_variable_set(f) {}
      FVariables(const FVariables& t) noexcept : input_variable_set(t.input_variable_set) {}
    };


    /** Mixin class for FunctionSet that adds the ability to add more variables.
     *
     * \param M is a FunctionSet< FV<FS,NewVars>, lambdas... >
     *
     */
    template <typename M>
    struct ConstructedFunctionSet : M {

      ConstructedFunctionSet(const ConstructedFunctionSet& fs) : M(fs) {}

      template <typename... Ts>
      constexpr ConstructedFunctionSet(Ts&&... t) noexcept : M(std::forward<Ts>(t)...) {}

      template <typename... Vs>
      constexpr auto make_vars() const noexcept {
        return Definable<FVariables<M, Vs...>, ConstructedFunctionSet>(M(*this));
      }
      template <typename... Vs, typename... Fs>
      constexpr auto add(const Fs&... fs) const noexcept {
        return make_vars<Vs...>()(fs...);
      }

      /** Add multiple variables with a single function.
       *  Deduces the returned tuple to call add.
       */
      template <typename Fs>
      constexpr auto append(const Fs& fs) const noexcept {
        using input_vars =  typename M::Vars_t;
        //using Result_t    = std::decay_t<insane::helpers::result_t<Fs>>;
        using lambda_type = std::decay_t<decltype(fs(std::declval<input_vars>()))>;
        return make_vars<lambda_type>()(fs);
      }
    };


    template <typename T, typename... Vs>
    auto make_vars() {
      return Definable<FVariables<T, Vs...>, ConstructedFunctionSet>{};
    }

    /** Helper making independent vars using FVariables specialization.
     */
    template <typename... Vs>
    auto make_independent_vars() {
      return Definable<FVariables<std::tuple<Vs...>>, ConstructedFunctionSet>{}();
    }
    //template <typename... Vs, typename... Cs>
    //auto make_independent_vars(const std::tuple<Cs...>& constants) {
    //  return Definable<FVariables<std::tuple<Vs...>>, ConstructedFunctionSet>{}().add<Cs,...>(
    //      [=](const auto& v) { return constants; });
    //}
    template <typename T, typename = typename std::enable_if<helpers::is_tuple<T>::value>::type>
    auto make_independent_vars2() {
      return Definable<FVariables<T>, ConstructedFunctionSet>{}();
    }


    } // namespace impl
  } // namespace kine
} // namespace insane

#endif

