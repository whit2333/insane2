#ifndef insane_xsec_InclusiveScattering_HH
#define insane_xsec_InclusiveScattering_HH

#include "fmt/core.h"

#include <iostream>
#include <random>
#include <thread>
#include <tuple>

#include "insane/base/SystemOfUnits.h"
#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/Core.h"
#include "insane/xsec/XSPhaseSpace.h"
#include "insane/base/FortranWrappers.h"
#include "insane/kinematics/KinematicFunctions.h"


namespace insane::xsec::inclusive {

  /** \addtogroup inclusive Inclusive Scattering
   *
   * Inclusive pion production: \f$ (e,\pi)X \f$.
   *
   * \ingroup xsections
   * @{
   */

  /** \name Cross Section Variables
   * @{
   */
  /** \f$ \frac{d\sigma}{dP_x d\Omega_x} \f$.
   */
  using V_dsigma_dPdOmega = insane::kine::Var<struct V_dsigma_dPdOmega_tag>;
  //@}

  /** \name Kinematic Variables
   * @{
   */
  using V_Mx         = insane::kine::Var<struct V_Mx_tag>;
  using V_Ex         = insane::kine::Var<struct V_Ex_tag>;
  using V_Px         = insane::kine::Var<struct V_Px_tag>;
  using V_theta      = insane::kine::Var<struct V_theta_tag>;
  using V_phi        = insane::kine::Var<struct V_phi_tag>;
  using V_ebeam      = insane::kine::Var<struct V_ebeam_tag>;
  using V_M_target   = insane::kine::Var<struct V_M_target_tag>;
  using V_k_beam_vec = insane::kine::FourVec<struct V_k_beam_vec_tag>;
  using V_Px_vec     = insane::kine::FourVec<struct V_Px_vec_tag>;

  using V_k1_labvec = insane::kine::FourVec<struct V_k1_labvec_tag>;
  using V_p1_labvec = insane::kine::FourVec<struct V_p1_labvec_tag>;
  using V_Px_labvec = insane::kine::FourVec<struct V_Px_labvec_tag>;
  //@}
  //@}

  /** \addtogroup wiser Wiser inclusive hadron production.
   *
   *  Inclusive electro production of pions, kaons, and nucleons.
   *   Uses the fit from wiser and virtual photon spectrum from Traitor-Wright Nuc.Phys. A379.
   *   The recoil factor (eq13) is also included.
   *
   * @{
   */
  inline int GetWiserType(int PDGcode) {
    if (PDGcode == 111)
      return 0;
    else if (PDGcode == 211)
      return 1;
    else if (PDGcode == -211)
      return 2;
    else if (PDGcode == 321)
      return 3;
    else if (PDGcode == -321)
      return 4;
    else if (PDGcode == 2212)
      return 5;
    else if (PDGcode == -2212)
      return 6;
    else {
      std::cout << " Bad particle code, " << PDGcode << ",  for Wiser inclusive cross section.\n";
    }
    return 1;
  }

  /** Wiser electroproduction cross section.
   */
  inline double WiserElectroproduction(double Ebeam, double Mx, double Px, double theta,
                                       double pdgcode = 211, bool onNeutron = false) {
    using namespace insane::units;
    double RES  = 0.0;
    auto   PART = GetWiserType(pdgcode);
    if (onNeutron) {
      // if target is a neutron use isospin to get the result
      if (pdgcode != 111) // make sure it is not set to pi0
        PART = GetWiserType(-1 * pdgcode);
    }
    double EBEAM = Ebeam;
    double Epart = std::sqrt(Px * Px + Mx * Mx);
    double Ppart = Px;
    double THETA = theta;

    using namespace insane::masses;
    // Solution to Tiator-wright Eqn.5 with theta_e=0
    double num = TMath::Power(Ppart, 2.0) + TMath::Power(M_p / GeV, 2.0) -
                 TMath::Power(M_p / GeV - Epart, 2.0);
    double denom = 2.0 * (Ppart * TMath::Cos(THETA) + M_p / GeV - Epart);

    double AMT = M_p / MeV;             // is target mass?
    double AM1 = M_pion / MeV;          // is produced particle mass (MeV/c2)
    double EI = EBEAM * 1000.0;         // is the electron beam energy (MeV)
    double W0 = (num / denom) * 1000.0; // is the theta=0 photon energy (MeV)
    double TP =
        (Epart - M_pion / GeV) * 1000.0; // is the kinetic energy of the produced particle (MeV)
    double TH = THETA;                   // is the angle of the produced particle (radians)
    double GN = 0.0;                     // the result of Ne*R/w0 of eqn.12 in Nuc.Phys.A379

    if (W0 < 0.0 || W0 > EI) {
      GN = 0.0;
    } else {
      vtp_(&AMT, &AM1, &EI, &W0, &TP, &TH, &GN);
      GN *= 1000.0; // because GN has units 1/MeV
    }

    // std::cout << "W0 = " << W0 << std::endl;
    // std::cout << "GN = " << GN << std::endl;
    // wiser_all_sig_(&RES,&PART,&EBEAM,&PscatteredPart,&THETA);
    double tot = 0.0;
    if (PART == 0) {
      // pi0 = (pi+ + pi-)/2.0
      PART = 1;
      wiser_fit_(&PART, &Ppart, &THETA, &EBEAM, &RES);
      tot += RES;
      PART = 2;
      wiser_fit_(&PART, &Ppart, &THETA, &EBEAM, &RES);
      tot += RES;
      tot = tot / 2.0;
    } else {
      wiser_fit_(&PART, &Ppart, &THETA, &EBEAM, &RES);
      tot += RES;
    }
    // RES is Ex*dsig/dp^3 with units of GeV-ub/(GeV/c)^2
    if (tot < 0.0)
      tot = 0.0;
    tot *= (Ppart);   // Takes dp^3 to dEdOmega
    tot *= GN * 1000; // conv
    if (TMath::IsNaN(tot))
      tot = 0.0;
    // std::cout << " wiser result is " << RES << " nb/GeV*str "
    //<< " for p,theta " << PscatteredPart << "," << THETA << "\n";
    return tot*insane::units::nanobarn/GeV; // converts nb to mb
  }
  //@}

} // namespace insane::xsec::inclusive

#endif
