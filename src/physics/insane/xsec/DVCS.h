#ifndef insane_xsec_DVCS_HH
#define insane_xsec_DVCS_HH

#include "fmt/core.h"

#include <iostream>
#include <random>
#include <thread>
#include <tuple>

#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/Core.h"
#include "insane/kinematics/DVCS.h"
#include "insane/xsec/XSPhaseSpace.h"

#include "Math/GenVector/VectorUtil.h"
#include "Math/Vector3D.h"
#include "TMath.h"


#include <complex>

/** @brief Useful collection of DVCS and GPD related functions.
 *
 *  DVCS.h is a collection of useful functions for DVCS such as:
 *   - Bethe-Heitler (BH) Amplitudes
 *   - Fourier harmonics (BH, DVCS, I)
 *   - Kinmatic coefficents
 *   - Compton form factors (CFFs)
 *   - GPD relations
 *
 *
 *  References:
 *  1. http://inspirehep.net/record/537446
 *     "Twist three analysis of photon electroproduction off pion"
 *     "Belitsky, Mueller, Kirchner, Schafer"
 *  2. http://inspirehep.net/record/796841
 *     "Refined analysis of photon leptoproduction off spinless target"
 *  3. http://inspirehep.net/record/679716
 *     "Unraveling hadron structure with generalized parton distributions"
 *     "Belitsky, Radyushkin"
 */
namespace insane::xsec::dvcs {

    /** DVCS variables. 
     *
     */
    struct DVCS_KinematicVariables {
      double xB;
      double Q2;
      double t;
      double phi;
      double xi;
      double E0;
      double M = 0.938;
      double y;
      double nu;
      double eps;
      double K;
      double J;
    };

    //double epsilon(    double Q2, double xB, double M=0.938);
    //double Delta2_min( double Q2, double xB, double M=0.938) ;
    //double Delta2_min2(double Q2, double xB, double M=0.938);
    //double Delta2_max( double Q2, double xB, double M=0.938);
    //double Delta2_perp(double xi, double Delta2, double D2m);

    //double y_max(  double eps);
    //double xi_BKM( double xB, double Q2, double Delta2);
    //double eta_BKM(double xi, double Q2, double Delta2);

    //double F3_plus(double x, double xi);
    //double F3_minus(double x, double xi);

    /** @brief Form Factors and Compton Form Factors. */
    struct DVCS_FormFactors {
      double               F1     = 0.0;
      double               F2     = 0.0;
      std::complex<double> H      = 0.0;
      std::complex<double> E      = 0.0;
      std::complex<double> Htilde = 0.0;
      std::complex<double> Etilde = 0.0;
      std::complex<double> HT;
      std::complex<double> ET;
      std::complex<double> HTtilde;
      std::complex<double> ETtilde;
      std::complex<double> Heff;
      std::complex<double> Eeff;
      std::complex<double> Hefftilde;
      std::complex<double> Eefftilde;
    };

    /** @brief Form Factors and Compton Form Factors. */
    struct DVCS_CFFs {
      double               F1     = 0.0;
      double               F2     = 0.0;
      std::complex<double> H      = 0.0;
      std::complex<double> E      = 0.0;
      std::complex<double> Htilde = 0.0;
      std::complex<double> Etilde = 0.0;
    };

    //------------------------------------------
    // Propagators
    //------------------------------------------
    double K_DVCS( double E0, double Q2, double xB, double Delta2, double M=0.938);
    double K_DVCS2(double E0, double Q2, double xB, double Delta2, double M=0.938);
    double J_DVCS( double E0, double Q2, double xB, double Delta2, double M=0.938);

    double K_DVCS( const DVCS_KinematicVariables& vars);
    double K_DVCS2(const DVCS_KinematicVariables& vars);
    double J_DVCS( const DVCS_KinematicVariables& vars);

    double P1(double E0, double Q2, double xB, double Delta2, double phi, double M=0.938);
    double P2(double E0, double Q2, double xB, double Delta2, double phi, double M=0.938);
    double P1(const DVCS_KinematicVariables& vars);
    double P2(const DVCS_KinematicVariables& vars);

    //------------------------------------------
    // Fourier Harmonics
    //------------------------------------------
    
    /** @name Spin-1/2 target
     *  Fourier Harmonics for spin-1/2 target.
     */
    ///@{
    double c0_BH_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double c1_BH_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double c2_BH_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);

    double c0_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F);
    double c1_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F);
    double s1_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F);
    double c2_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F);
    double s2_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F);

    double c0_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F);
    double c1_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F);
    double s1_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double c2_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double s2_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double c3_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double s3_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    ///@}

    /** @name Spin-0 target
     *  Fourier Harmonics for spin-0 target.
     */
    ///@{
    double c0_BH_spin0(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double c1_BH_spin0(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double c2_BH_spin0(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    ///@}

    //------------------------------------------
    // Angular Harmonics
    //------------------------------------------
    std::complex<double> C_angular_DVCS_unp(  const DVCS_KinematicVariables& vars, const DVCS_CFFs& F, const DVCS_CFFs& Fstar);
    std::complex<double> C_angular_I_unp(     const DVCS_KinematicVariables& vars, const DVCS_CFFs& F);
    std::complex<double> C_angular_I_T_unp(   const DVCS_KinematicVariables& vars, const DVCS_CFFs& F);
    std::complex<double> DeltaC_angular_I_unp(const DVCS_KinematicVariables& vars, const DVCS_CFFs& F);


  /** \addtogroup DVCS Deeply Virtual Compton Scattering
   * \ingroup xsections
   *
   * \f$ p(e,e' \gamma)p \f$
   *
   * @{
   */

  /** \name Cross section variables.
   * @{
   */

  using namespace insane::kine::dvcs;

  /// \f$ \frac{d\sigma}{dx_B dQ^{2} dt d\phi d\phi_e} \f$
  using dsigma_d_xB_Q2_t_phi_phie_v = insane::kine::Var<struct dsigma_d_xB_Q2_t_phi_phie_tag>;

  /// \f$ \frac{d\sigma}{dx dQ^{2} dt d\phi d\phi_e} \f$
  using dsigma_d_x_Q2_t_phi_phie_v = insane::kine::Var<struct dsigma_d_x_Q2_t_phi_phie_tag>;


  //@}
  
  using dsigma_DVCS_v = insane::kine::Var<struct dsigma_DVCS_tag>;
  using dsigma_BH_v = insane::kine::Var<struct dsigma_BH_tag>;
  using dsigma_INT_v = insane::kine::Var<struct dsigma_INT_tag>;

  /** \name Elastic Form Factors
   * Dirac and Pauli form factors.
   * @{
   */
  using F1_v                          = insane::kine::Var<struct F1_tag>;
  using F2_v                          = insane::kine::Var<struct F2_tag>;
  //@}

  //@}

  /** DVCS cross section.
   *
   * In the frame where the initial nucleon is at rest.
   *
   *  \f[ \frac{d\sigma}{dt dx dQ^{2} d\phi d\phi_{e\prime}} \f]
   *
   */
  auto ep_unpolarized_DVCS_dsigma =
      insane::xsec::make_dsigma<dsigma_d_xB_Q2_t_phi_phie_v>([](const auto& v) {
        using namespace TMath;
        using namespace insane::physics;
        using namespace insane::units;
        const auto&  F1            = std::get<F1_v>(v);
        const auto&  F2            = std::get<F2_v>(v);
        const auto&  E0            = std::get<E_beam_v>(v);
        const auto&  Ep            = std::get<E_prime_v>(v);
        const auto&  M             = std::get<M_target_v>(v);
        const auto&  Q2            = std::get<Q2_v>(v);
        const auto&  t             = std::get<t_v>(v);
        const auto&  tmin          = std::get<t_min_v>(v);
        const auto&  tmax          = std::get<t_max_v>(v);
        const auto&  y             = std::get<y_v>(v);
        const auto&  ymax          = std::get<y_max_v>(v);
        const auto&  phi           = std::get<phi_v>(v);
        const auto&  phi_dvcs      = std::get<phi_dvcs_v>(v);
        const auto&  Delta2        = std::get<Delta2_v>(v);
        const auto&  xB            = std::get<xB_v>(v);
        const auto&  xi            = std::get<xi_v>(v);
        const auto&  eps           = std::get<eps_v>(v);
        const auto&  nu            = std::get<nu_v>(v);
        const auto&  theta         = std::get<theta_v>(v);
        const double tanthetaOver2 = std::tan(theta / 2.0);
        // Rosenbluth formula
        // const double res =
        //    mottXSec * recoil_factor *
        //    ((GE2 + tau * GM2) / (1.0 + tau) + 2.0 * tau * GM2 * tanthetaOver2 * tanthetaOver2) *
        //    hbarc2_gev_nb;

        if( t < tmax ) return -1.0;
        if( t > tmin ) return -2.0;
        //if( y > ymax ) return -3.0;

        const double beamSign = 1.0; // negative for a positron beam
        // double       xB       = x[9];
        // double       t        = x[10];
        // double       Q2       = x[11];
        // double       phi      = x[12];
        // double       M        = M_p / GeV;
        // double       E0       = GetBeamEnergy();
        // double       eps      = epsilon(Q2, xB, M);
        // double       nu       = Q2 / (2.0 * M * xB);
        // double       y        = nu / E0;
        // double       Delta2   = t;
        // double       xi       = xi_BKM(xB, Q2, Delta2);

        const double t0 =
            std::pow(1.0 / 137.0, 3.0) * xB / (16.0 * M_PI * M_PI * Q2 * Q2 * std::sqrt(1.0 + eps * eps));

        DVCS_KinematicVariables dvcs_vars{xB, Q2, t, phi_dvcs, xi, E0, M, y, nu, eps};
        DVCS_FormFactors        dvcs_ffs;
        dvcs_ffs.F1     = F1;
        dvcs_ffs.F2     = F2;
        dvcs_ffs.H      = 0.0;
        dvcs_ffs.Htilde = 0.0;

        // double p1 = P1(E0, Q2, xB, Delta2, phi, M);
        // double p2 = P2(E0, Q2, xB, Delta2, phi, M);
        dvcs_vars.K     = K_DVCS(dvcs_vars);
        dvcs_vars.J     = J_DVCS(dvcs_vars);
        const double p1 = P1(dvcs_vars);
        const double p2 = P2(dvcs_vars);

        // -------------------------------
        // BH
        const double c0BH = c0_BH_unp(dvcs_vars, dvcs_ffs);
        const double c1BH = c1_BH_unp(dvcs_vars, dvcs_ffs);
        const double c2BH = c2_BH_unp(dvcs_vars, dvcs_ffs);

        const double den_BH = std::pow(xB * (1.0 + eps * eps), 2.0) * Delta2 * p1 * p2;
        const double num_BH = c0BH + c1BH * Cos(phi_dvcs) + c2BH * Cos(2.0 * phi_dvcs); 
        // -------------------------------
        // DVCS
        const double c0DVCS   = c0_DVCS_unp(dvcs_vars, dvcs_ffs);
        const double c1DVCS   = c1_DVCS_unp(dvcs_vars, dvcs_ffs);
        const double c2DVCS   = c2_DVCS_unp(dvcs_vars, dvcs_ffs);
        const double s1DVCS   = s1_DVCS_unp(dvcs_vars, dvcs_ffs);
        const double s2DVCS   = s2_DVCS_unp(dvcs_vars, dvcs_ffs);
        const double den_DVCS = Q2;
        const double num_DVCS = c0DVCS + c1DVCS * Cos(phi_dvcs) + c2DVCS * Cos(2.0 * phi_dvcs) +
                                s1DVCS * Sin(phi_dvcs) + s2DVCS * Sin(2.0 * phi_dvcs);

        // -------------------------------
        // Interference
        const double c0I   = c0_I_unp(dvcs_vars, dvcs_ffs);
        const double c1I   = c1_I_unp(dvcs_vars, dvcs_ffs);
        const double c2I   = c2_I_unp(dvcs_vars, dvcs_ffs);
        const double s1I   = s1_I_unp(dvcs_vars, dvcs_ffs);
        const double s2I   = s2_I_unp(dvcs_vars, dvcs_ffs);
        const double den_I = xB * y * y * y * Delta2 * p1 * p2;
        const double num_I = beamSign * (c0I + c1I * Cos(phi_dvcs) + c2I * Cos(2.0 * phi_dvcs) +
                                         s1I * Sin(phi_dvcs) + s2I * Sin(2.0 * phi_dvcs));

        const double sig_BH = t0* (num_BH / den_BH); // + num_DVCS / den_DVCS + num_I / den_I);
        const double res    = sig_BH ;//* TMath::Power(hbarc2_gev_nb, 1.5);
        //if(res < 0.0) return 0.0;
        //if(std::isnan(res)) return 0.0;

        return res;
      });

  //void PrintDVCSKinematics(const auto& v) {
  //  using namespace insane::units;
  //  fmt::print("=================\n");
  //  fmt::print("{:<12} = {: <8f} {}\n", "Q2", std::get<Q2_v>(v) / GeV / GeV, "GeV^{2}");
  //  fmt::print("{:<12} = {: <8f} {}\n", "xB", std::get<xB_v>(v) , "       ");
  //  fmt::print("{:<12} = {: <8f} {}\n", "t", std::get<t_v>(v) / GeV / GeV, "GeV^{2}");
  //  fmt::print("{:<12} = {: <8f} {}\n", "t_min", std::get<t_min_v>(v) / GeV / GeV, "GeV^{2}");
  //  fmt::print("{:<12} = {: <8f} {}\n", "t_max", std::get<t_max_v>(v) / GeV / GeV, "GeV^{2}");
  //  fmt::print("{:<12} = {: <8f} {}\n", "y", std::get<y_v>(v),  " ");
  //  fmt::print("{:<12} = {: <8f} {}\n", "y_max", std::get<y_max_v>(v),  " ");
  //  fmt::print("{:<12} = {: <8f} {}\n", "E0", std::get<E_beam_v>(v) / GeV, "GeV");
  //  fmt::print("{:<12} = {: <8f} {}\n", "E_e'", std::get<E_prime_v>(v) / GeV, "GeV");
  //  fmt::print("{:<12} = {: <8f} {}\n", "th_e", std::get<theta_v>(v) / degree, "degree");
  //  fmt::print("{:<12} = {: <8f} {}\n", "phi_e", std::get<phi_v>(v) / degree, "degree");
  //  fmt::print("{:<12} = {: <8f} {}\n", "nu", std::get<nu_v>(v) / GeV, "GeV");
  //  fmt::print("{:<12} = {: <8f} {}\n", "x", std::get<x_v>(v) , "       ");
  //  fmt::print("{:<12} = {: <8f} {}\n", "xi", std::get<xi_v>(v) , "       ");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "eps", std::get<epsilon_v>(v) , "       ");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "P_recoil", std::get<V_P_recoil>(v) / GeV, "GeV");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "th_recoil", std::get<theta_recoil_v>(v) / degree, "degree");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "phi_recoil", std::get<phi_recoil_v>(v) / degree, "degree");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "P_had1", std::get<p_had1_vec>(v).get().mag() / GeV, "GeV");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "th_had1", std::get<p_had1_vec>(v).get().Theta() / degree, "degree");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "phi_had1", std::get<p_had1_vec>(v).get().Phi() / degree, "degree");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "P_had2", std::get<p_had2_vec>(v).get().mag() / GeV, "GeV");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "th_had2", std::get<p_had2_vec>(v).get().Theta() / degree, "degree");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "phi_had2", std::get<p_had2_vec>(v).get().Phi() / degree, "degree");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "th_rec_max", std::get<theta_recoil_max_v>(v) / degree, "degree");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "theta_q", std::get<theta_q_v>(v) / degree, "degree");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "W", std::get<W_v>(v) / GeV, "GeV");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "y", std::get<y_v>(v), "");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "nu", std::get<nu_v>(v) / GeV, "GeV");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "nu_cm", std::get<nu_cm_v>(v) / GeV, "GeV");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "epsilon", std::get<epsilon_v>(v), "");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "W2", std::get<W2_v>(v) / GeV / GeV, "GeV^{2}");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "dsigma/dt", std::get<dsigma_dt_v>(v), "");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "Gamma", std::get<Gamma_v>(v), "");
  //  fmt::print("\n");

  //}

} // namespace insane::xsec::dvcs
#endif
