#ifndef insane_xsec_QuasiElasticScattering_HH
#define insane_xsec_QuasiElasticScattering_HH

#include "fmt/core.h"

#include <iostream>
#include <random>
#include <thread>
#include <tuple>

#include "insane/xsections/RadiativeEffects.h"
#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/Core.h"
#include "insane/xsec/XSPhaseSpace.h"
#include "insane/kinematics/KinematicFunctions.h"

#include "Math/GenVector/VectorUtil.h"
#include "Math/Vector3D.h"
#include "TMath.h"

namespace insane::xsec::quasielastic {

  /** \addtogroup quasielasticScattering Quasi-Elastic Scattering
   * Quasi elastic scattering of electrons off bound nucleons in Nuclei
   * \ingroup xsections
   *
   * @{
   */
  /** \name Differential Cross Section
   * @{
   */
  /// \f$ \frac{d\sigma}{dx_B dt dQ^{2} d\phi d\phi_e} \f$
  using dsigma_dQ2    = insane::kine::Var<struct dsigma_dQ2_tag>;
  using dsigma_dOmega = insane::kine::Var<struct dsigma_dOmega_tag>;
  using dsigma_dP_dOmega_recoil = insane::kine::Var<struct dsigma_dP_dOmega_recoil_tag>;
  using dsigma_dE_dOmega_e = insane::kine::Var<struct dsigma_dE_dOmega_e_tag>;
  //@} 
  /** \name Nuclear form factors
   * @{
   */
  /// Proton electric form factor
  using GEp           = insane::kine::Var<struct GEp_tag>;
  /// Proton magnetic form factor
  using GMp           = insane::kine::Var<struct GMp_tag>;
  /// Neutron electric form factor
  using GEn           = insane::kine::Var<struct GEn_tag>;
  /// Neutron magnetic form factor
  using GMn           = insane::kine::Var<struct GMn_tag>;

  /// Energy peaking (s) Proton electric form factor
  using GEp_s           = insane::kine::Var<struct GEp_s_tag>;
  /// Energy peaking (s) Proton magnetic form factor
  using GMp_s           = insane::kine::Var<struct GMp_s_tag>;
  /// Energy peaking (s) Neutron electric form factor
  using GEn_s           = insane::kine::Var<struct GEn_s_tag>;
  /// Energy peaking (s) Neutron magnetic form factor
  using GMn_s           = insane::kine::Var<struct GMn_s_tag>;


  using FCHe4         = insane::kine::Var<struct FCHe4_tag>;
  //@} 

  /** \name Kinematic Variables
   * @{
   */
  using tau           = insane::kine::Var<struct tau_tag>;
  using y             = insane::kine::Var<struct y_tag>;
  using theta         = insane::kine::Var<struct theta_tag>;
  using phi           = insane::kine::Var<struct phi_tag>;
  using eprime        = insane::kine::Var<struct eprime_tag>;
  using ebeam         = insane::kine::Var<struct ebeam_tag>;
  using theta_recoil  = insane::kine::Var<struct theta_recoil_tag>;
  using phi_recoil    = insane::kine::Var<struct phi_recoil_tag>;
  using p_recoil      = insane::kine::Var<struct p_recoil_tag>;
  using E_recoil      = insane::kine::Var<struct E_recoil_tag>;
  using KE_recoil      = insane::kine::Var<struct KE_recoil_tag>;
  using Q2            = insane::kine::Var<struct Q2_tag>;
  using nu            = insane::kine::Var<struct nu_tag>;
  using tau           = insane::kine::Var<struct tau_tag>;
  using M_target      = insane::kine::Var<struct M_target_tag>;
  using M_recoil      = insane::kine::Var<struct M_recoil_tag>;
  using theta_q       = insane::kine::Var<struct theta_q_tag>;
  using nu_cm         = insane::kine::Var<struct nu_cm_tag>;
  using q             = insane::kine::Var<struct q_tag>;
  using q_cm          = insane::kine::Var<struct q_cm_v>;

  /// Beam energy after radiating photon.
  using Es             = insane::kine::Var<struct Es_tag>;
  /// Effective Q2 for beam with energy Es
  using Q2_prime       = insane::kine::Var<struct Q2_prime_tag>;
  using omega_s        = insane::kine::Var<struct omega_s_tag>;
  using omega_p        = insane::kine::Var<struct omega_p_tag>;

  using k_beam_vec    = insane::kine::FourVec<struct k_beam_tag>;
  using k_prime_vec   = insane::kine::FourVec<struct k_prime_tag>;
  using q_vec         = insane::kine::FourVec<struct q_vec_tag>;
  using p_recoil_vec  = insane::kine::FourVec<struct p_prime_tag>;

  using k1_labvec     = insane::kine::FourVec<struct k1_labvec_tag>;
  using k2_labvec     = insane::kine::FourVec<struct k2_labvec_tag>;
  using q1_labvec     = insane::kine::FourVec<struct q1_labvec_tag>;
  using p1_labvec     = insane::kine::FourVec<struct p1_labvec_tag>;
  using p2_labvec     = insane::kine::FourVec<struct p2_labvec_tag>;
  //@}
  

  

  /** Quasi-Elastic radiative tail cross section differential in electron kinematics.
   *
   *  \f$ \frac{d\sigma}{dE_{e}d\Omega_{e}} \f$
   *
   *  The effective incident beam energy, Es, is computed from the exclusive kinematics using 
   *  only the recoil 4He momentum vector.
   *
   */
  auto QuasiElasticTail_dsigma_electron =
      insane::xsec::make_dsigma<V_dsigma_dE_dOmega_e>([](const auto& v) {
        using namespace TMath;
        using namespace insane::physics;
        using namespace insane::units;
        const auto& GEp         = std::get<V_GEp>(v);
        const auto& GMp         = std::get<V_GMp>(v);
        const auto& GEp_s       = std::get<V_GEp_s>(v);
        const auto& GMp_s       = std::get<V_GMp_s>(v);
        const auto& E0          = std::get<V_ebeam>(v);
        const auto& Ep          = std::get<V_eprime>(v);
        const auto& theta       = std::get<V_theta>(v);
        const auto& M           = std::get<V_M_target>(v);
        const auto& Q2          = std::get<V_Q2>(v);
        const auto& Q2_prime    = std::get<V_Q2_prime>(v);
        const auto& ws          = std::get<V_omega_s>(v);
        const auto& wp          = std::get<V_omega_p>(v);
        const double Es = E0 - ws;
        if (Es < 0.0) {
          return 0.0;
        }
        if (Es > E0) {
          return 0.0;
        }

        const double tanthetaOver2 = std::tan(theta / 2.0);
        const double mott1         = (Ep / E0) * insane::Kine::Sig_Mott(E0, theta);
        const double mott2         = (Ep / Es) * insane::Kine::Sig_Mott(Es, theta);
        const double GE2           = GEp * GEp;
        const double GM2           = GMp * GMp;
        const double GE2_s         = GEp_s * GEp_s;
        const double GM2_s         = GMp_s * GMp_s;

        // Rosenbluth formula
        const double tau1 = Q2/(4.0*M*M);
        const double sig1 =
            mott1 *
            ((GE2 + tau1 * GM2) / (1.0 + tau1) + 2.0 * tau1 * GM2 * tanthetaOver2 * tanthetaOver2) *
            insane::constants::hbarc_squared;

        const double tau2 = Q2_prime/(4.0*M*M);
        const double sig2 = mott2 *
                            ((GE2_s + tau2 * GM2_s) / (1.0 + tau2) +
                             2.0 * tau2 * GM2_s * tanthetaOver2 * tanthetaOver2) *
                            insane::constants::hbarc_squared;

        // if (sig_rad < 0.0 || TMath::IsNaN(sig_rad)){
        //  return 0.0;
        //}
        // return sig_rad*insane::constants::hbarc_squared;

        // double E0     = GetBeamEnergy();
        // double M      = M_p/GeV;//fTargetNucleus.GetMass();
        // double A      = fTargetNucleus.GetA();
        // double T_mat  = fTargetMaterial.GetNumberOfRadiationLengths();

        // double Es     = E0;
        // double Ep     = x[0];
        // double theta  = x[1];
        const double A = 4.0; 
        double Qsquared = 4.0 * E0 * Es * TMath::Power(TMath::Sin(theta / 2.0), 2);

        // double sig1  = Sig_el(Es,Ep,theta);
        // double sig2  = Sig_el(Es-ws,Ep,theta);

        double T_X0 = insane::radcor::tr_Tsai(Qsquared);
        double g_per_cm3 = insane::units::g/(insane::units::cm*insane::units::cm*insane::units::cm);
        //double T_mat =  20.0*insane::units::cm*(1.24*g_per_cm3)/insane::rad_length(2,4);
        double T_mat =  20.0*(1.24)/insane::rad_length(2,4);
        //std::cout << T_mat << std::endl;

        double T    = T_X0 +T_mat;
        double bT   = T * 4.0 / 3.0;

        // Eq'ns C.13 of Tsai 1971, SLAC-PUB-848
        double T00 =
            (1.0 + 0.5772 * bT) * Power(ws / Es, bT / 2.0) * Power(wp / (Ep + wp), bT / 2.0);
        double T01 = sig1 * bT * DiLog(wp / (Ep + wp)) / (2.0 * wp);
        double T02 = (sig2 * bT * DiLog(ws / Es) / (2.0 * ws)) *
                     (M + (Es - ws) * (1.0 - Cos(theta))) / (M - Ep * (1.0 - Cos(theta)));

        double sig_rad = A * T00 * (T01 + T02);
        if (sig_rad < 0.0 || TMath::IsNaN(sig_rad))
          sig_rad = 0.0;
        return sig_rad*std::sin(theta);
      });

  /** Print the kinematics.
   *
   */
  void PrintElasticKinematics(const auto& v) {
    using namespace insane::units;
    //fmt::print("{:<12} = {: <8f} {}\n", "Q2", std::get<V_Q2>(v) / GeV / GeV, "GeV^{2}");
    //fmt::print("{:<12} = {: <8f} {}\n", "E0", std::get<V_ebeam>(v) / GeV, "GeV");
    //fmt::print("{:<12} = {: <8f} {}\n", "E_e'", std::get<V_eprime>(v) / GeV, "GeV");
    //fmt::print("{:<12} = {: <8f} {}\n", "th_e", std::get<V_theta>(v) / degree, "degree");
    //fmt::print("{:<12} = {: <8f} {}\n", "phi_e", std::get<V_phi>(v) / degree, "degree");
    //fmt::print("{:<12} = {: <8f} {}\n", "nu", std::get<V_nu>(v) / GeV, "GeV");
    //fmt::print("{:<12} = {: <8f} {}\n", "P_recoil", std::get<V_p_recoil>(v) / GeV, "GeV");
    ////fmt::print("{:<12} = {: <8f} {}\n", "th_recoil", std::get<V_theta_recoil>(v) / degree, "degree");
    ////fmt::print("{:<12} = {: <8f} {}\n", "phi_recoil", std::get<V_phi_recoil>(v) / degree, "degree");
    ////fmt::print("{:<12} = {: <8f} {}\n", "E_recoil", std::get<V_E_recoil>(v).get() / GeV, "GeV");
    //fmt::print("{:<12} = {: <8f} {}\n", "V_FCHe4", std::get<V_FCHe4>(v), "");
    //fmt::print("\n");
  }

  //@}
} // namespace insane::xsec::elastic_4He
#endif

