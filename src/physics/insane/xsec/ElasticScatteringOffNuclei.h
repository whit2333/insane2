#ifndef insane_xsec_ElasticScatteringOffNuclei_HH
#define insane_xsec_ElasticScatteringOffNuclei_HH

#include "fmt/core.h"

#include <iostream>
#include <random>
#include <thread>
#include <tuple>

#include "insane/rad/RadiativeEffects.h"
#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "insane/kinematics/Core.h"
#include "insane/kinematics/Inelastic.h"
#include "insane/kinematics/Elastic.h"
#include "insane/xsec/ElasticScattering.h"

#include "insane/xsec/XSPhaseSpace.h"

#include "Math/GenVector/VectorUtil.h"
#include "Math/Vector3D.h"
#include "TMath.h"

namespace insane::xsec::elastic_4He {

  using namespace insane::kine::elastic;

  /** \addtogroup nuclearElasticScattering Elastic Scattering off of Nuclei
   *
   *  Elastic electron scattering off a Nucleus.
   *  \f$ A(e,e')A \f$
   *
   * \ingroup xsections
   *
   * @{
   */

  /** \name Differential Cross Section
   * @{
   */
  /// \f$ \frac{d\sigma}{dQ^{2}} \f$
  using dsigma_dQ2_v    = insane::kine::Var<struct dsigma_dQ2_tag>;

  /// \f$ \frac{d\sigma}{d\Omega} \f$
  using dsigma_dOmega_v = insane::kine::Var<struct dsigma_dOmega_tag>;

  /** Elastic radiative tail \f$ \frac{d\sigma}{dPd\Omega_{recoil}} \f$.
   *  The phase space is for the recoil nucleus momentum and solid angle.
   */
  using dsigma_dP_dOmega_recoil_v = insane::kine::Var<struct dsigma_dP_dOmega_recoil_tag>;

  /** Elastic radiative tail \f$ \frac{d\sigma}{dPd\Omega_{e}} \f$.
   *  The phase space is for the scattered electron energy and solid angle.
   */
  using dsigma_dE_dOmega_e_v = insane::kine::Var<struct dsigma_dE_dOmega_e_tag>;
  //@}

  /** \name Nuclear form factors
   * @{
   */
  /// Proton electric form factor
  using GEp_v           = insane::kine::Var<struct GEp_tag>;
  /// Proton magnetic form factor
  using GMp_v           = insane::kine::Var<struct GMp_tag>;
  /// Neutron electric form factor
  using GEn_v           = insane::kine::Var<struct GEn_tag>;
  /// Neutron magnetic form factor
  using GMn_v           = insane::kine::Var<struct GMn_tag>;

  /// Energy peaking (s) Proton electric form factor
  using GEp_s_v           = insane::kine::Var<struct GEp_s_tag>;
  /// Energy peaking (s) Proton magnetic form factor
  using GMp_s_v           = insane::kine::Var<struct GMp_s_tag>;
  /// Energy peaking (s) Neutron electric form factor
  using GEn_s_v           = insane::kine::Var<struct GEn_s_tag>;
  /// Energy peaking (s) Neutron magnetic form factor
  using GMn_s_v           = insane::kine::Var<struct GMn_s_tag>;

  using FCHe4_v         = insane::kine::Var<struct FCHe4_tag>;
  //@}

  /** \name Kinematic Variables
   * @{
   */
  using tau           = insane::kine::Var<struct tau_tag>;
  using y             = insane::kine::Var<struct y_tag>;
  using theta         = insane::kine::Var<struct theta_tag>;
  using phi           = insane::kine::Var<struct phi_tag>;
  using eprime        = insane::kine::Var<struct eprime_tag>;
  using ebeam         = insane::kine::Var<struct ebeam_tag>;
  using theta_recoil  = insane::kine::Var<struct theta_recoil_tag>;
  using phi_recoil    = insane::kine::Var<struct phi_recoil_tag>;
  using p_recoil      = insane::kine::Var<struct p_recoil_tag>;
  using E_recoil      = insane::kine::Var<struct E_recoil_tag>;
  using KE_recoil      = insane::kine::Var<struct KE_recoil_tag>;
  using Q2            = insane::kine::Var<struct Q2_tag>;
  using nu            = insane::kine::Var<struct nu_tag>;
  using tau           = insane::kine::Var<struct tau_tag>;
  using M_target      = insane::kine::Var<struct M_target_tag>;
  using M_recoil      = insane::kine::Var<struct M_recoil_tag>;
  using theta_q       = insane::kine::Var<struct theta_q_tag>;
  using nu_cm         = insane::kine::Var<struct nu_cm_tag>;
  using q             = insane::kine::Var<struct q_tag>;
  using q_cm          = insane::kine::Var<struct q_cm_v>;

  /// Beam energy after radiating photon.
  using Es             = insane::kine::Var<struct Es_tag>;
  /// Effective Q2 for beam with energy Es
  using Q2_prime       = insane::kine::Var<struct Q2_prime_tag>;

  using k_beam_vec    = insane::kine::FourVec<struct k_beam_tag>;
  using k_prime_vec   = insane::kine::FourVec<struct k_prime_tag>;
  using q_vec         = insane::kine::FourVec<struct q_vec_tag>;
  using p_recoil_vec  = insane::kine::FourVec<struct p_prime_tag>;

  using k1_labvec     = insane::kine::FourVec<struct k1_labvec_tag>;
  using k2_labvec     = insane::kine::FourVec<struct k2_labvec_tag>;
  using q1_labvec     = insane::kine::FourVec<struct q1_labvec_tag>;
  using p1_labvec     = insane::kine::FourVec<struct p1_labvec_tag>;
  using p2_labvec     = insane::kine::FourVec<struct p2_labvec_tag>;
  //@}

  constexpr double GetPalpha(const double& Es, const double& alpha, const double& M) {
    using namespace TMath;
    const double res = 4.0 * Es * M * (Es + M) * std::cos(alpha) /
                 (Es * Es + 4.0 * Es * M + 2.0 * M * M - Es * Es * std::cos(2.0 * alpha));
    return res;
  }

  /** The electron scattering angle for elastic scattering computed from the target recoil kinematics.
   */
  constexpr double GetTheta_e(const double& Es, const double& alpha, const double& M) {
    using namespace TMath;
    const double res = 2.0 * std::atan((Es + M) * std::sin(alpha) / (M * std::cos(alpha)));
    return res;
  }

  constexpr double ElectronToAlphaJacobian_dOmega(const double& Es, const double& alpha,
                                                  const double& M) {
    using namespace TMath;
    const double theta = GetTheta_e(Es, alpha, M);
    const double dtheta_dalpha =
        2.0 * M * (Es + M) /
        (std::pow(M * std::cos(alpha), 2.0) + std::pow((Es + M) * std::sin(alpha), 2.0));
    const double res = dtheta_dalpha * std::sin(theta);
    return res;
  }

  /** Jacobian for to change from inclusive electron variables to inclusive recoil variables.
   *
   * \f[
   *   \frac{dE_e d\Omega_e}{dP_{\alpha}d\Omega_{\alpha}}
   * \f]
   *
   */
  constexpr double ElectronToAlphaJacobian(const double& Palpha, const double& thetaalpha, const double& M) {
    using namespace TMath;
    //const double dtheta_dalpha =
    //    2.0 * M * (Es + M) /
    //    (std::pow(M * std::cos(alpha), 2.0) + std::pow((Es + M) * std::sin(alpha), 2.0));
    //const double res = dtheta_dalpha * std::sin(theta);
    const double Ealpha =  std::sqrt(std::pow(M,2) + std::pow(Palpha,2));
    const double cosalpha = std::cos(thetaalpha);
    const double res =
        ((std::pow(Palpha, 3) + M * Palpha * Ealpha -
          (2 * std::pow(M, 3) + 3 * M * std::pow(Palpha, 2) + 2 * std::pow(M, 2) * Ealpha +
           2 * std::pow(Palpha, 2) * Ealpha) *
              cosalpha +
          Palpha * (std::pow(Palpha, 2) + 2 * M * Ealpha) * std::pow(cosalpha, 2) +
          std::pow(M, 2) * Palpha * (2 + std::cos(2 * thetaalpha))) *
         std::sin(thetaalpha)) /
        (Ealpha * (Ealpha - Palpha * cosalpha) * (-Palpha + (M + Ealpha) * cosalpha) *
         std::sqrt(((std::pow(Palpha, 2) - 2 * Palpha * (M + Ealpha) * cosalpha +
                (std::pow(Palpha, 2) + 2 * M * (M + Ealpha)) * std::pow(cosalpha, 2)) *
               std::pow(std::sin(thetaalpha), 2)) /
              std::pow(Ealpha - Palpha * cosalpha, 2)));
    return std::abs(res);
  }

  /** @brief Bremsstrahlung energy loss probability.
   *  Tsai, SLAC-PUB-0848 (1971), Eq B.43
   *  https://inspirehep.net/literature/67278
   *
   *  Same as B.43 but we explicitly write out W_b(E,eps)
   *  so that X0 cancels from the formula.
   */
  double Ib(const double& E0, const double& E, const double& t)  {
    const double b     = 4.0 / 3.0; // fKinematics.Getb();
    const double gamma = TMath::Gamma(1. + b * t);
    // Double_t Wb    = W_b(E0,E0-E);
    // Wb is (X0 cancels out when combined with everything in this method):
    // Double_t b     = fKinematicsExt.Getb();
    // Double_t X0    = fX0Eff;
    // Double_t Wb    = (1./X0)*(b/eps)*GetPhi(eps/E0);
    const double v   = (E0 - E) / E0;
    const double phi = (1.0 - v + (3.0 / 4.0) * v * v);
    const double T1  = (b * t / gamma) * (1. / (E0 - E));
    const double T2  = std::pow(v, b * t);
    const double T3  = phi;
    const double ib  = T1 * T2 * T3;
    return ib;
  }

  /** Elastic electron Helium-4 cross section.
   *
   * This is the nonradiative cross section. 
   *
   *  \f[ 
   *  \frac{d\sigma}{d\Omega_e}  = \frac{(Z \alpha)^2}{4 E^3}\frac{E^{\prime}\cos^2(\theta/2)}{\sin^4(\theta/2)} F_c^2(Q^2) 
   *  \f]
   *
   * \ingroup nuclearElasticScattering
   */
  auto e_4He_Elastic_dsigma = insane::xsec::make_dsigma< dsigma_dOmega>([](const auto& v) {
    using namespace TMath;
    using namespace insane::physics;
    using namespace insane::units;
    const auto&  FCHe4          = std::get< FCHe4>(v);
    const auto&  E0             = std::get< ebeam>(v);
    const auto&  Ep             = std::get< eprime>(v);
    const auto&  M              = std::get< M_target>(v);
    const auto&  Q2             = std::get< Q2>(v);
    const auto&  theta          = std::get< theta>(v);
    const double costhetaOtwo_2 = std::pow(std::cos(theta / 2.0), 2);
    const double sinthetaOtwo_4 = std::pow(std::sin(theta / 2.0), 4);
    const double Fc2            = FCHe4 * FCHe4;
    const double alpha          = insane::constants::fine_structure_const;
    const double res = alpha * alpha * Ep * costhetaOtwo_2 * Fc2 / (E0 * E0 * E0 * sinthetaOtwo_4);
    return std::sin(theta) * res*insane::constants::hbarc_squared;
  });


  /** Elastic cross section with radiative corrections applied.
   *
   * The cross section is differential in target recoil variables.
   *
   *  \f[
   *  \frac{d\sigma}{d\Omega} =  \frac{d\sigma^{eff}}{d\Omega} 
   *                               \left[\frac{R\Delta E}{E_s}\right]^{T'} 
   *                               \left[\frac{\Delta E}{E_{p}^{peak}}\right]^{T'} 
   *                               \left(1 - \frac{\xi}{\Delta E}\right)
   *  \f]
   *
   *  "Radiative correction to the jth peak".
   *  Eqn 2.3, Tsai, SLAC-PUB-848 1971
   *
   * \ingroup nuclearElasticScattering
   */
  auto e_4He_Elastic_dsigma_radiated = insane::xsec::make_dsigma< dsigma_dOmega>([](const auto& v) {
    using namespace TMath;
    using namespace insane::physics;
    using namespace insane::units;
    const auto& FCHe4       = std::get< FCHe4>(v);
    const auto& E0          = std::get< ebeam>(v);
    const auto& Ep          = std::get< eprime>(v);
    const auto& M           = std::get< M_target>(v);
    const auto& Q2          = std::get< Q2>(v);
    const auto& theta       = std::get< theta>(v);
    const auto& theta_alpha = std::get< theta_recoil>(v);
    // const double T_mat  = fTargetMaterial.GetNumberOfRadiationLengths();
    // const double theta_alpha = x[4] ;
    const double T_mat =  20.0*(1.24)/insane::rad_length(2,4);
    const double T_X0 = insane::radcor::tr_Tsai(Q2);
    const double T    = T_X0 + T_mat;
    const double bT   = T * 4.0 / 3.0;
    // I is the probability of the incident electron losing
    // energy to be come Ep. This dramatically reduces the cross section
    // far away from the peak.
    const double I = Ib(E0, E0, T / 2.0);
    const double mottXSec = insane::kine::Sig_Mott(E0, theta);
    const double GE2      = FCHe4 * FCHe4;
    const double sig_el   = mottXSec *(Ep/E0) * GE2;
    const double jac      = ElectronToAlphaJacobian_dOmega(E0, theta_alpha, M);
    const double F_RC     = insane::radcor::F_Tsai(E0, Ep, theta);
    const double RC       = insane::radcor::RCToPeak_Tsai(E0, Ep, theta);
    const double res      = RC * F_RC * sig_el * jac;
    return std::sin(theta_alpha) * res*insane::constants::hbarc_squared;
  });

  /** Radiative cross section for elastic scettering.
   *
   *  \f$ \frac{d\sigma}{d\Omega_e} \f$
   *
   *  "Radiative correction to the jth peak".
   *  Eqn 2.3, Tsai, SLAC-PUB-848 1971
   */
  auto e_4He_Elastic_dsigma_electron_radiated = insane::xsec::make_dsigma< dsigma_dE_dOmega_e>([](const auto& v) {
    using namespace TMath;
    using namespace insane::physics;
    using namespace insane::units;
    const auto& FCHe4       = std::get< FCHe4>(v);
    const auto& E0          = std::get< ebeam>(v);
    const auto& Ep          = std::get< eprime>(v);
    const auto& M           = std::get< M_target>(v);
    const auto& Q2          = std::get< Q2>(v);
    const auto& theta       = std::get< theta>(v);
    const auto& theta_alpha = std::get< theta_recoil>(v);
    // const double T_mat  = fTargetMaterial.GetNumberOfRadiationLengths();
    // const double theta_alpha = x[4] ;
    const double T_mat =  20.0*(1.24)/insane::rad_length(2,4);
    const double T_X0 = tr_Tsai(Q2);
    const double T    = T_X0 + T_mat;
    const double bT   = T * 4.0 / 3.0;
    // I is the probability of the incident electron losing
    // energy to be come Ep. This dramatically reduces the cross section
    // far away from the peak.
    //const double I = Ib(E0, E0, T / 2.0);
    const double mottXSec = insane::kine::Sig_Mott(E0, theta);
    const double GE2      = FCHe4 * FCHe4;
    const double sig_el   = mottXSec *(Ep/E0) * GE2;
    const double jac      = ElectronToAlphaJacobian_dOmega(E0, theta_alpha, M);
    const double F_RC     = insane::radcor::F_Tsai(E0, Ep, theta);
    const double RC       = insane::radcor::RCToPeak_Tsai(E0, Ep, theta);
    const double res      = RC * F_RC * sig_el * jac;
    return std::sin(theta_alpha) * res*insane::constants::hbarc_squared;
  });

  /** Elastic radiative tail cross section.
   *
   *  \f$ \frac{d\sigma}{dP_{recoil}d\Omega_{recoil}} \f$
   *
   *  The effective incident beam energy, Es, is computed from the exclusive kinematics using 
   *  only the recoil 4He momentum vector.
   *
   */
  auto e_4He_ElasticTail_dsigma =
      insane::xsec::make_dsigma< dsigma_dP_dOmega_recoil>([](const auto& v) {
        using namespace TMath;
        using namespace insane::physics;
        using namespace insane::units;
        const auto& FCHe4       = std::get< FCHe4>(v);
        const auto& E0          = std::get< ebeam>(v);
        const auto& Ep          = std::get< eprime>(v);
        const auto& Es          = std::get< Es>(v);
        const auto& M           = std::get< M_target>(v);
        const auto& Q2          = std::get< Q2>(v);
        const auto& theta       = std::get< theta>(v);
        const auto& theta_alpha = std::get< theta_recoil>(v);
        const auto& P_alpha     = std::get< p_recoil>(v);
        const auto& E_alpha     = std::get< E_recoil>(v);
        if (Es < 0.0) {
          return 0.0;
        }
        if (Es > E0) {
          return 0.0;
        }
        const double delta_function = Abs(1.0 / (2.0 * (M - E_alpha + P_alpha * Cos(theta_alpha))));
        const double I              = Ib(E0, Es, 0.05);
        // Here the sin(theta)/sin(theta_alpha) is the jacobian for the two solid angles Omega -> sin(theta) dtheta dphi
        // The latter cancels again below which is why it commented out.
        const double jac            = ElectronToAlphaJacobian(P_alpha, theta_alpha, M)*std::sin(theta);// /std::sin(theta_alpha);
        const double mottXSec       = insane::kine::Sig_Mott(Es, theta);
        const double recoil         = insane::kine::fRecoil(Es, theta, M);
        const double GE2            = FCHe4 * FCHe4;
        const double sig_rad = (mottXSec / recoil) * GE2 * delta_function * I * jac; // * std::sin(theta_alpha);
        if (sig_rad < 0.0 || TMath::IsNaN(sig_rad)){
          return 0.0;
        }
        return sig_rad*insane::constants::hbarc_squared;
      });

  /** Elastic radiative tail cross section differential in electron kinematics.
   *
   *  \f$ \frac{d\sigma}{dE_{e}d\Omega_{e}} \f$
   *
   *  The effective incident beam energy, Es, is computed from the exclusive kinematics using 
   *  only the recoil 4He momentum vector.
   *
   */
  auto e_4He_ElasticTail_dsigma_electron =
      insane::xsec::make_dsigma< dsigma_dE_dOmega_e>([](const auto& v) {
        using namespace TMath;
        using namespace insane::physics;
        using namespace insane::units;
        const auto& FCHe4       = std::get< FCHe4>(v);
        const auto& E0          = std::get< ebeam>(v);
        const auto& Ep          = std::get< eprime>(v);
        const auto& theta       = std::get< theta>(v);
        const auto& Es          = std::get< Es>(v);
        const auto& M           = std::get< M_target>(v);
        const auto& Q2_prime    = std::get< Q2_prime>(v);
        if (Es < 0.0) {
          return 0.0;
        }
        if (Es > E0) {
          return 0.0;
        }
        // delta_function is for evaluating the deltafunction with the argument  (W^2 - M^2) = (2 M nu - Q2)
        // when integrated over Es. 
        const double delta_function = Abs(1.0 / (2.0 * M - 4.0*Ep*std::pow(std::sin(theta/2.0),2)));
        const double I              = Ib(E0, Es, 0.05);
        //const double Qsquared       = 4.0 * Ep * Es * TMath::Power(TMath::Sin(theta / 2.0), 2);
        const double mottXSec       = insane::kine::Sig_Mott(Es, theta);
        const double recoil         = insane::kine::fRecoil(Es, theta, M);
        const double GE2            = FCHe4 * FCHe4;
        const double sig_rad = (mottXSec / recoil) * GE2 * delta_function * I * std::sin(theta);
        if (sig_rad < 0.0 || TMath::IsNaN(sig_rad)){
          return 0.0;
        }
        return sig_rad*insane::constants::hbarc_squared;
      });

  ///** Print the kinematics.
  // *
  // */
  //void PrintElasticKinematics(const auto& v) {
  //  using namespace insane::units;
  //  fmt::print("{:<12} = {: <8f} {}\n", "Q2", std::get< Q2>(v) / GeV / GeV, "GeV^{2}");
  //  fmt::print("{:<12} = {: <8f} {}\n", "E0", std::get< ebeam>(v) / GeV, "GeV");
  //  fmt::print("{:<12} = {: <8f} {}\n", "E_e'", std::get< eprime>(v) / GeV, "GeV");
  //  fmt::print("{:<12} = {: <8f} {}\n", "th_e", std::get< theta>(v) / degree, "degree");
  //  fmt::print("{:<12} = {: <8f} {}\n", "phi_e", std::get< phi>(v) / degree, "degree");
  //  fmt::print("{:<12} = {: <8f} {}\n", "nu", std::get< nu>(v) / GeV, "GeV");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "P_recoil", std::get< p_recoil>(v) / GeV, "GeV");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "th_recoil", std::get< theta_recoil>(v) / degree, "degree");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "phi_recoil", std::get< phi_recoil>(v) / degree, "degree");
  //  //fmt::print("{:<12} = {: <8f} {}\n", "E_recoil", std::get< E_recoil>(v).get() / GeV, "GeV");
  //  //fmt::print("{:<12} = {: <8f} {}\n", " FCHe4", std::get< FCHe4>(v), "");
  //  fmt::print("\n");
  //}

  //@}
} // namespace insane::xsec::elastic_4He
#endif

