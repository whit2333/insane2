#ifndef insane_xsec_SIDIS_HH
#define insane_xsec_SIDIS_HH

#include "fmt/core.h"

#include <iostream>
#include <random>
#include <thread>
#include <tuple>

#include "insane/base/PhysicalConstants.h"
#include "insane/xsec/XSPhaseSpace.h"
#include "insane/kinematics/SIDIS.h"
#include "insane/structurefunctions/SIDISlowPt_SFs.h"



/** Semi-inclusive Deep inelastic scattering.
 *
 * \ingroup xsections
 */
namespace insane::xsec::sidis {

  /** \addtogroup SIDIS Semi-inclusive Deep Inelastic Scattering
   *
   * SIDIS \f$ (e,e'h)X \f$.
   *
   *
   * \ingroup xsections
   * @{
   */


  /** \name Cross Section Variables
   * @{
   */
  /** \f[ \frac{d\sigma}{dx dy dz d\phi } \f].
   */
  using dsigma_dx_dy_dz_dphi_v = insane::kine::Var<struct dsigma_dx_dy_dz_dphi_tag>;

  ///** \f[ \frac{d\sigma}{dx dQ^{2} dz dP_hperp d\phi_e} \f].
  // */
  using dsigma_dx_dQ2_dz_dphi_dphih_dPhperp2_v = insane::kine::Var<struct dsigma_dx_dQ2_dz_dphi_dphih_dPhperp2_tag>;
  ///** \f[ \frac{d\sigma}{dE d\Omega } \f].
  // */
  //using dsigma_dE_dOmega_v = insane::kine::Var<struct V_dsigma_dE_dOmega_tag>;
  //using dsigma_dE_dOmega_e_v = insane::kine::Var<struct V_dsigma_dE_dOmega_e_tag>;
  //@}

  using namespace insane::kine::sidis;

  /** \name Ph_perp integrated structure functions.
   *
   * @{
   */
  using FUU_T_v       = insane::kine::Var<struct FUU_T_tag      >; ///< \f$F_{UU,T}\f$
  using FUU_L_v       = insane::kine::Var<struct FUU_L_tag      >; ///< \f$F_{UU,L}\f$
  using FUU_cosphi_v  = insane::kine::Var<struct FUU_cosphi_tag >; ///< \f$F_{UU}^{\cos\phi}\f$
  using FUU_cos2phi_v = insane::kine::Var<struct FUU_cos2phi_tag>; ///< \f$F_{UU}^{\cos2\phi}\f$
  // and more..
  //  FLU FUL FLL  FUT FLT ... and all the angles
  //@}

  //@}



  /** SIDIS diferential cross section.
   *
   *            d(sigma)
   *   --------------------------
   *   d(x)d(y)d(z)d(phi)d(phi_e)
   *   
   *   Note: "phi" is the angle between the leptonic and hadronic planes. 
   *         See http://inspirehep.net/record/677636 for details.
   *         "phi_e" is the scattered electron's azimuthal angle.
   *
   */
  inline auto add_dsigma_dx_dQ2_dz_dphi_dphih_dPhperp2 = [](const auto& v) constexpr {
    using namespace insane::physics;
    using namespace insane::units;
    using namespace insane::constants;
    const auto& FUU_T        = std::get<FUU_T_v>(v);
    const auto& FUU_cosphih  = std::get<FUU_cosphi_v>(v);
    const auto& E0           = std::get<E_beam_v>(v);
    const auto& Ep           = std::get<E_prime_v>(v);
    const auto& eps          = std::get<epsilon_v>(v);
    const auto& gamma        = std::get<gamma_v>(v);
    const auto& M            = std::get<M_target_v>(v);
    const auto& Q2           = std::get<Q2_v>(v);
    const auto& x            = std::get<x_v>(v);
    const auto& y            = std::get<y_v>(v);
    const auto& z            = std::get<z_v>(v);
    const auto& phi_h        = std::get<phi_h_v>(v);
    const auto  coef_cos_phi = std::cos(phi_h) * std::sqrt(2.0 * eps * (1.0 + eps));
    // following matches/mismatches paper
    // note: using jacobian dy/dQ2 = y/Q2
    const double sig0 = (alpha * alpha / (x * y * Q2)) * (y * y / (2.0 * (1.0 - eps))) *
                        (1.0 + gamma * gamma / (2.0 * x)) * (y / Q2);

    const double SF_terms = FUU_T + coef_cos_phi*FUU_cosphih;
    return dsigma_dx_dQ2_dz_dphi_dphih_dPhperp2_v{insane::units::hbarc2_GeV_nb * sig0 * SF_terms};
  };

} // namespace insane::xsec::sidis

#endif
