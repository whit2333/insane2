#ifndef insane_xsec_IncoherentScattering_HH
#define insane_xsec_IncoherentScattering_HH

#include "fmt/core.h"

#include <iostream>
#include <random>
#include <thread>
#include <tuple>

#include "insane/base/SystemOfUnits.h"
#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/Incoherent.h"


namespace insane::xsec::incoherent {


  using namespace insane::kine::incoherent;
  /** \addtogroup incoherent Incoherent Scattering
   *
   * Inclusive pion production: \f$ (e,\pi)X \f$.
   *
   * \ingroup xsections
   * @{
   */

  /** \name Cross Section Variables
   * @{
   */
  /** \f$ \frac{d\sigma}{dP_x d\Omega_x} \f$.
   */
  using V_dsigma_dPdOmega = insane::kine::Var<struct V_dsigma_dPdOmega_tag>;

  //@}

  /** \name Fermi momentum.
   * @{
   */

  using n_momentum_dist_v = insane::kine::Var<struct n_momentum_dist_tag>;

  /** \f$ \int_0^\infty 4\pi k^2 n(k) dk = 1 \f$
   */
  using dn_dp3_v = insane::kine::Var<struct dn_dp3_tag>;
  //@}

  auto add_fermi_motion = [](const auto& v) constexpr {
    using namespace insane::physics;
    const auto& n  = std::get<n_momentum_dist_v>(v);
    const auto& p  = std::get<P_fermi_v>(v);
    const auto& th = std::get<theta_fermi_v>(v);
    return dn_dp3_v{n* p * p * std::sin(th)};
  };

  //@}

} // namespace insane::xsec::incoherent
#endif
