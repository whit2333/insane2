#ifndef insane_xsec_PionElectroproduction_HH
#define insane_xsec_PionElectroproduction_HH

#include "fmt/core.h"

#include <iostream>
#include <random>
#include <thread>
#include <tuple>

#include "insane/xsections/RadiativeEffects.h"
#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/Core.h"
#include "insane/kinematics/HallCSettings.h"
#include "insane/kinematics/Inelastic.h"
#include "insane/xsec/XSPhaseSpace.h"
#include "insane/base/Math.h"
#include "insane/kinematics/KinematicFunctions.h"

#include "Math/GenVector/VectorUtil.h"
#include "Math/Vector3D.h"

/** \addtogroup xsections
 *  \ingroup Physics
 */
namespace insane::xsec::pielectro {

  /** \addtogroup pionElectroproduction Pion Electroproduction 
   * \ingroup xsections
   *
   * @{
   */
 
  /** \name Elastic scattering variables
   * @{
   */
  /// \f$ \frac{d\sigma}{dQ^{2}} \f$
  using V_dsigma_dQ2    = insane::kine::Var<struct V_dsigma_dQ2_tag>;

  /// \f$ \frac{d\sigma}{d\Omega} \f$
  using V_dsigma_dOmega = insane::kine::Var<struct V_dsigma_dOmega_tag>;

  /** \f$ \frac{d\sigma}{dE^{\prime}d\Omega} \f$.
   * Cross section variable for radiative tail.
   */
  using V_dsigma_dE_dOmega_e = insane::kine::Var<struct V_dsigma_dE_dOmega_e_tag>;


  /** \name Sachs Form Factors
   * @{
   */
  /// Proton electric form factor
  using V_GEp           = insane::kine::Var<struct V_GEp_tag>;
  /// Proton magnetic form factor
  using V_GMp           = insane::kine::Var<struct V_GMp_tag>;
  /// Neutron electric form factor
  using V_GEn           = insane::kine::Var<struct V_GEn_tag>;
  /// Neutron magnetic form factor
  using V_GMn           = insane::kine::Var<struct V_GMn_tag>;

  /// Energy peaking (s) Proton electric form factor
  using V_GEp_s           = insane::kine::Var<struct V_GEp_s_tag>;
  /// Energy peaking (s) Proton magnetic form factor
  using V_GMp_s           = insane::kine::Var<struct V_GMp_s_tag>;
  /// Energy peaking (s) Neutron electric form factor
  using V_GEn_s           = insane::kine::Var<struct V_GEn_s_tag>;
  /// Energy peaking (s) Neutron magnetic form factor
  using V_GMn_s           = insane::kine::Var<struct V_GMn_s_tag>;

  /// Proton electric form factor
  using V_GEp_ep_elastic           = insane::kine::Var<struct V_GEp_ep_elastic_tag>;
  /// Proton magnetic form factor
  using V_GMp_ep_elastic           = insane::kine::Var<struct V_GMp_ep_elastic_tag>;
  /// Neutron electric form factor
  using V_GEn_ep_elastic           = insane::kine::Var<struct V_GEn_ep_elastic_tag>;
  /// Neutron magnetic form factor
  using V_GMn_ep_elastic           = insane::kine::Var<struct V_GMn_ep_elastic_tag>;
  //@}

  /** \name Elastic kinematic variables
   * @{
   */
  using V_x           = insane::kine::Var<struct V_x_tag>;
  using V_tau           = insane::kine::Var<struct V_tau_tag>;
  using V_y             = insane::kine::Var<struct V_y_tag>;
  using V_theta         = insane::kine::Var<struct V_theta_tag>;
  using V_phi           = insane::kine::Var<struct V_phi_tag>;
  using V_eprime        = insane::kine::Var<struct V_eprime_tag>;
  /// Energy of elastic electron 
  using V_ep_elastic    = insane::kine::Var<struct V_ep_elastic_tag>;
  using V_Q2_ep_elastic       = insane::kine::Var<struct V_Q2_ep_elastic_tag>;
  using V_ebeam         = insane::kine::Var<struct V_ebeam_tag>;
  using V_Q2            = insane::kine::Var<struct V_Q2_tag>;
  using V_nu            = insane::kine::Var<struct V_nu_tag>;
  using V_W           = insane::kine::Var<struct V_W_tag>;
  using V_eps         = insane::kine::Var<struct V_eps_tag>;
  using V_tau           = insane::kine::Var<struct V_tau_tag>;
  using V_M_target      = insane::kine::Var<struct V_M_target_tag>;
  using V_M_recoil      = insane::kine::Var<struct V_M_recoil_tag>;
  using V_theta_q       = insane::kine::Var<struct V_theta_q_tag>;
  using V_nu_cm         = insane::kine::Var<struct V_nu_cm_tag>;
  using V_q             = insane::kine::Var<struct V_q_tag>;
  using V_q_cm          = insane::kine::Var<struct V_q_cm_v>;

  /// Beam energy after radiating photon.
  using V_Es             = insane::kine::Var<struct V_Es_tag>;
  /// Effective Q2 for beam with energy Es
  using V_Q2_prime       = insane::kine::Var<struct V_Q2_prime_tag>;

  /// C.11 from Tsai SLAC-PUB-0408, 1971 
  using V_omega_s        = insane::kine::Var<struct V_omega_s_tag>;
  /// C.12 from Tsai SLAC-PUB-0408, 1971 
  using V_omega_p        = insane::kine::Var<struct V_omega_p_tag>;


  using V_k_beam_vec    = insane::kine::FourVec<struct V_k_beam_tag>;
  using V_k_prime_vec   = insane::kine::FourVec<struct V_k_prime_tag>;
  using V_q_vec         = insane::kine::FourVec<struct V_q_vec_tag>;
  using V_p_recoil_vec  = insane::kine::FourVec<struct V_p_prime_tag>;

  using V_k1_labvec     = insane::kine::FourVec<struct V_k1_labvec_tag>;
  using V_k2_labvec     = insane::kine::FourVec<struct V_k2_labvec_tag>;
  using V_q1_labvec     = insane::kine::FourVec<struct V_q1_labvec_tag>;
  using V_p1_labvec     = insane::kine::FourVec<struct V_p1_labvec_tag>;
  using V_p2_labvec     = insane::kine::FourVec<struct V_p2_labvec_tag>;
  //@}
  /** \name Nucleon Structure Functions
   * F1 and F2 .
   * @{
   */
  using V_F1p                          = insane::kine::Var<struct V_F1p_tag>;
  using V_F2p                          = insane::kine::Var<struct V_F2p_tag>;
  using V_F1n                          = insane::kine::Var<struct V_F1n_tag>;
  using V_F2n                          = insane::kine::Var<struct V_F2n_tag>;
  //@}

  auto DIS_dsigma_dE_dOmega = insane::xsec::make_dsigma<V_dsigma_dE_dOmega_e>([](const auto& v) {
    using namespace insane::physics;
    const auto&  F1            = std::get<V_F1p>(v);
    const auto&  F2            = std::get<V_F2p>(v);
    const auto&  E0            = std::get<V_ebeam>(v);
    const auto&  Ep            = std::get<V_eprime>(v);
    const auto&  M             = std::get<V_M_target>(v);
    const auto&  Q2            = std::get<V_Q2>(v);
    const auto&  y             = std::get<V_y>(v);
    const auto&  nu            = std::get<V_nu>(v);
    const auto&  th            = std::get<V_theta>(v);
    const double tanthetaOver2 = std::tan(th / 2.0);
    double       SIN      = std::sin(th / 2.);
    double       COS      = std::cos(th / 2.);
    double       SIN2     = SIN * SIN;
    double       COS2     = COS*COS;
    double       TAN2     = SIN2 / COS2;
    const double W1       = (1. / M) * F1;
    const double W2       = (1. / nu) * F2;
    double       alpha    = insane::constants::alpha;
    double       num      = alpha * alpha * COS2;
    double       den      = 4. * E0 * E0 * SIN2 * SIN2;
    double       MottXS   = insane::constants::hbarc_squared* num / den;
    double       fullXsec = MottXS * (W2 + 2.0 * tanthetaOver2 * W1);
    if(std::isnan(fullXsec) ) return  0.0;
    if(fullXsec < 0 ) return  0.0;
    return fullXsec*std::sin(th);
  };

  ///** Elastic electron scattering cross section off proton.
  // *
  // *  \f[ \frac{d\sigma}{dQ^{2}d\phi_e} \f]
  // *
  // *  Nonradiative cross section. 
  // */
  //auto Elastic_dsigma_dOmega = insane::xsec::make_dsigma<V_dsigma_dOmega>([](const auto& v) {
  //  using namespace insane::physics;
  //  const auto&           GEp   = std::get<V_GEp_ep_elastic>(v);
  //  const auto&           GMp   = std::get<V_GMp_ep_elastic>(v);
  //  const auto&           E0    = std::get<V_ebeam>(v);
  //  //const auto&           Ep    = std::get<V_eprime>(v);
  //  const auto&           M     = std::get<V_M_target>(v);
  //  const auto&           Q2    = std::get<V_Q2_ep_elastic>(v);
  //  //const auto&           tau   = std::get<V_tau>(v);
  //  const auto&           theta = std::get<V_theta>(v);
  //  const auto&           Ep    = E0/(1.0+(2.0*E0/M)*std::pow(std::sin(theta/2.0),2));
  //  if (Ep > E0)
  //    return 0.0;
  //  const double tau = Q2/(4.0*M*M);
  //  const double mottXSec      = insane::kine::Sig_Mott(E0, theta);
  //  const double recoil_factor = Ep / E0;
  //  const double GE2           = GEp*GEp;
  //  const double GM2           = GMp*GMp;
  //  const double tanthetaOver2 = std::tan(theta / 2.0);
  //  // Rosenbluth formula
  //  const double res =
  //      mottXSec * recoil_factor *
  //      ((GE2 + tau * GM2) / (1.0 + tau) + 2.0 * tau * GM2 * tanthetaOver2 * tanthetaOver2) *
  //      insane::constants::hbarc_squared;
  //  return res*std::sin(theta);
  //});

  ///** Radiative elastic electron scattering cross section off proton.
  // *
  // *  \f[ \frac{d\sigma}{dQ^{2}d\phi_e} \f]
  // *
  // *  "Radiative correction to the jth peak".
  // *  Eqn 2.3, Tsai, SLAC-PUB-848 1971
  // */
  //auto RadiativeElastic_dsigma_dOmega = insane::xsec::make_dsigma<V_dsigma_dOmega>([](const auto& v) {
  //  using namespace insane::physics;
  //  const auto&           GEp   = std::get<V_GEp_ep_elastic>(v);
  //  const auto&           GMp   = std::get<V_GMp_ep_elastic>(v);
  //  const auto&           E0    = std::get<V_ebeam>(v);
  //  const auto&           M     = std::get<V_M_target>(v);
  //  const auto&           Q2    = std::get<V_Q2_ep_elastic>(v);
  //  const auto&           theta = std::get<V_theta>(v);
  //  const auto&           Ep    = E0/(1.0+(2.0*E0/M)*std::pow(std::sin(theta/2.0),2));
  //  if (Ep > E0)
  //    return 0.0;
  //  const double tau = Q2/(4.0*M*M);
  //  const double mottXSec      = insane::kine::Sig_Mott(E0, theta);
  //  const double recoil_factor = Ep / E0;
  //  const double GE2           = GEp*GEp;
  //  const double GM2           = GMp*GMp;
  //  const double tanthetaOver2 = std::tan(theta / 2.0);

  //  const double T_mat =  20.0*(1.24)/insane::rad_length(2,4);
  //  const double T_X0 = insane::radcor::tr_Tsai(Q2);
  //  const double T    = T_X0 + T_mat;
  //  const double bT   = T * 4.0 / 3.0;


  //  // Rosenbluth formula
  //  const double sig_el =
  //      mottXSec * recoil_factor *
  //      ((GE2 + tau * GM2) / (1.0 + tau) + 2.0 * tau * GM2 * tanthetaOver2 * tanthetaOver2) *
  //      insane::constants::hbarc_squared;

  //  const double F_RC     = insane::radcor::F_Tsai(E0, Ep, theta);
  //  const double RC       = insane::radcor::RCToPeak_Tsai(E0, Ep, theta);
  //  const double res      = RC * F_RC * sig_el *std::sin(theta);
  //  return (res);
  //});

  ///** Radiative tail from elastic electron-proton scattering.
  // *
  // *  \f[
  // *  \frac{d\sigma}{dE_{e}d\Omega_{e}}
  // *  \f]
  // *
  // *  The effective incident beam energy, Es, is computed from the exclusive kinematics using 
  // *  only the recoil 4He momentum vector.
  // */
  //auto ElasticTail_dsigma_electron =
  //    insane::xsec::make_dsigma<V_dsigma_dE_dOmega_e>([](const auto& v) {
  //      using namespace TMath;
  //      using namespace insane::physics;
  //      using namespace insane::units;
  //      const auto& GEp         = std::get<V_GEp>(v);
  //      const auto& GMp         = std::get<V_GMp>(v);
  //      const auto& GEp_s       = std::get<V_GEp_s>(v);
  //      const auto& GMp_s       = std::get<V_GMp_s>(v);
  //      const auto& E0          = std::get<V_ebeam>(v);
  //      const auto& Ep          = std::get<V_eprime>(v);
  //      const auto& Ep_elastic  = std::get<V_ep_elastic>(v);
  //      const auto& theta       = std::get<V_theta>(v);
  //      const auto& phi         = std::get<V_phi>(v);
  //      const auto& M           = std::get<V_M_target>(v);
  //      const auto& Q2          = std::get<V_Q2>(v);
  //      const auto& Q2_prime    = std::get<V_Q2_prime>(v);
  //      const auto& ws          = std::get<V_omega_s>(v);
  //      const auto& wp          = std::get<V_omega_p>(v);
  //      const double Es          = E0;
  //      const double Esprime     = E0 - ws;
  //      const double Ep_elastic2 =
  //          Esprime / (1.0 + (2.0 * Esprime / M) * std::pow(std::sin(theta / 2.0), 2));
  //      //if (Esprime < 0.0) {
  //      //  return 0.0;
  //      //}
  //      if (Ep > E0) {
  //        return 0.0;
  //      }
  //      const double tanthetaOver2 = std::tan(theta / 2.0);
  //      const double mott1         = (Ep / E0) * insane::kine::Sig_Mott(E0, theta);
  //      const double mott2         = (Ep / Esprime) * insane::kine::Sig_Mott(Esprime, theta);
  //      const double GE2           = GEp * GEp;
  //      const double GM2           = GMp * GMp;
  //      const double GE2_s         = GEp_s * GEp_s;
  //      const double GM2_s         = GMp_s * GMp_s;

  //      double tr_1 = insane::radcor::tr_Tsai(Q2/GeV);
  //      double tr_2 = insane::radcor::tr_Tsai(Q2_prime/GeV);
  //      double T_mat = 20.0*(0.125)/insane::rad_length(2,4);
  //      double T1    = tr_1 +T_mat/2.0;
  //      double bT   = T1 * 4.0 / 3.0;
  //      double T2    = tr_2 +T_mat/2.0;
  //      double bT2   = T2 * 4.0 / 3.0;

  //      // Rosenbluth formula
  //      const double tau1 = Q2/(4.0*M*M);
  //      double F_RC1 = insane::radcor::F_Tsai(E0, Ep, theta,tr_1 +T_mat);
  //      const double RC1       = insane::radcor::RCToPeak_Tsai(E0, Ep, theta);
  //      if(Ep> Ep_elastic) 
  //        F_RC1 = 0.0;
  //      const double sig1 =
  //          mott1 * F_RC1 *
  //          ((GE2 + tau1 * GM2) / (1.0 + tau1) + 2.0 * tau1 * GM2 * tanthetaOver2 * tanthetaOver2) *
  //          insane::constants::hbarc_squared;

  //      const double tau2  = Q2_prime / (4.0 * M * M);
  //      double F_RC2 = insane::radcor::F_Tsai(Esprime, Ep, theta,tr_1 +T_mat);
  //      const double RC2       = insane::radcor::RCToPeak_Tsai(Esprime, Ep, theta);
  //      if(Ep> Ep_elastic2) 
  //        F_RC2 = 0.0;
  //      const double sig2  = mott2 * F_RC2 *
  //                          ((GE2_s + tau2 * GM2_s) / (1.0 + tau2) +
  //                           2.0 * tau2 * GM2_s * tanthetaOver2 * tanthetaOver2) *
  //                          insane::constants::hbarc_squared;

  //      const double A = 4.0; 

  //      //std::cout << "T    = " << T << "\n";
  //      //std::cout << "Tmat = " << T_mat << "\n";
  //      //std::cout << "T_x0 = " << T_X0 << "\n";

  //      // Eq'ns C.13 of Tsai 1971, SLAC-PUB-848
  //      //double T00 =
  //      //    (1.0 + 0.5772 * bT) * Power(ws / Es, bT / 2.0) * Power(wp / (Ep + wp), bT / 2.0);
  //      //double T01 = sig1 * bT * DiLog(wp / (Ep + wp)) / (2.0 * wp);
  //      //double T02 = (sig2 * bT * DiLog(ws / Es) / (2.0 * ws)) *
  //      //             (M + (Es - ws) * (1.0 - Cos(theta))) / (M - Ep * (1.0 - Cos(theta)));

  //      //std::cout << "vs = " << ws / Es << "\n";
  //      //std::cout << "vp = " << (wp/(Ep+wp)) << "\n";
  //      double xi  = insane::radcor::LandauStraggling_xi(T_mat,1,1);
  //      // Eq'ns 3.1 of Tsai 1971, SLAC-PUB-848
  //      double costh = std::cos(theta);
  //      double T00 = std::pow((ws/Es)*(wp/(Ep+wp)),bT);
  //      double T01 = sig1 * (bT * DiLog(wp / (Ep + wp)) / ( wp)  + xi/(2.0*wp*wp));
  //      double T02 = sig2 * ((M+(Es-ws)*(1.0-costh))/(M-Ep*(1.0-costh)))*(bT/(ws)*DiLog(ws/Es) +xi/(2.0*ws*ws) );

  //      // multi photon exchange term
  //      //std::array<double, 2> ffs =  {GEp, GMp};
  //      //double integral_result = insane::integrate::simple_threads(
  //      //    [=](double x) {
  //      //      return insane::radcor::InternalIntegrand_MoTsai69(x, Es/GeV, Ep/GeV, theta, phi, M/GeV, ffs);
  //      //    },
  //      //    -1.0, 1.0,200);

  //      double alpha = insane::constants::alpha;
  //      double sigma_internal = 0.0;//(alpha * alpha * alpha / (4.0 * M_PI * M_PI)) * (Ep / Es / M) *
  //                              //integral_result * insane::constants::hbarc_squared/GeV/GeV;

  //      double sig_rad = T00 * (T01 + T02);
  //      if (sig_rad < 0.0 || TMath::IsNaN(sig_rad))
  //        sig_rad = 0.0;
  //      return (sigma_internal + sig_rad) * std::sin(theta);
  //      });

  //      void PrintElasticKinematics(const auto& v) {
  //        using namespace insane::units;
  //        fmt::print("{:<12} = {: <8f} {}\n", "Q2", std::get<V_Q2>(v) / GeV / GeV, "GeV^{2}");
  //        fmt::print("{:<12} = {: <8f} {}\n", "E0", std::get<V_ebeam>(v) / GeV, "GeV");
  //        fmt::print("{:<12} = {: <8f} {}\n", "E_e'", std::get<V_eprime>(v) / GeV, "GeV");
  //        fmt::print("{:<12} = {: <8f} {}\n", "th_e", std::get<V_theta>(v) / degree, "degree");
  //        fmt::print("{:<12} = {: <8f} {}\n", "phi_e", std::get<V_phi>(v) / degree, "degree");
  //        fmt::print("{:<12} = {: <8f} {}\n", "nu", std::get<V_nu>(v) / GeV, "GeV");
  //        // fmt::print("{:<12} = {: <8f} {}\n", "P_recoil", std::get<V_p_recoil>(v) / GeV, "GeV");
  //        // fmt::print("{:<12} = {: <8f} {}\n", "th_recoil", std::get<V_theta_recoil>(v) / degree,
  //        // "degree"); fmt::print("{:<12} = {: <8f} {}\n", "phi_recoil", std::get<V_phi_recoil>(v) /
  //        // degree, "degree"); fmt::print("{:<12} = {: <8f} {}\n", "E_recoil",
  //        // std::get<V_E_recoil>(v).get() / GeV, "GeV"); fmt::print("{:<12} = {: <8f} {}\n",
  //        // "V_FCHe4", std::get<V_FCHe4>(v), "");
  //        fmt::print("\n");
  //}

  //@}
} // namespace insane::xsec::elastic

#endif
