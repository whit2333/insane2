#ifndef insane_xsec_TaggedDVCS_HH
#define insane_xsec_TaggedDVCS_HH

#include "fmt/core.h"

#include <iostream>
#include <random>
#include <thread>
#include <tuple>

#include "insane/base/SystemOfUnits.h"
#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/Incoherent.h"
#include "insane/kinematics/DVCS.h"
#include "insane/xsec/Incoherent.h"
#include "insane/xsec/DVCS.h"


namespace insane::xsec::tagged_dvcs {

  using namespace insane::kine::incoherent;

  /** \addtogroup taggedDVCS Tagged DVCS
   *
   * Tagged DVCS  \f$ A(e,e' \gamma +N)A-1 \f$.
   *
   * \ingroup xsections
   * @{
   */

  /** \name Cross Section Variables
   * @{
   */

  /// \f$ \frac{d\sigma}{dx_B dQ^{2} dt d\phi d\phi_e} \f$
  using dsigma_d_xB_Q2_t_phi_phie_v = insane::kine::Var<struct dsigma_d_xB_Q2_t_phi_phie_tag>;


  //@}
  //@}


  //@}

} // namespace insane::xsec::tagged_dvcs
#endif
