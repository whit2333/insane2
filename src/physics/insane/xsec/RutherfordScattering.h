#ifndef insane_xsec_RutherfordScattering.h_HH
#define insane_xsec_RutherfordScattering.h_HH

#include "fmt/core.h"

#include <iostream>
#include <random>
#include <thread>
#include <tuple>

#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "insane/base/Math.h"
#include "insane/kinematics/Elastic.h"
#include "insane/xsec/XSPhaseSpace.h"

#include "Math/GenVector/VectorUtil.h"
#include "Math/Vector3D.h"

/** Rutherford scattering.
 * \addtogroup xsections
 */
namespace insane::xsec::rutherford {

  using namespace insane::kine::var;


  /** \name Rutherford Scattering
   *  \ingroup xsections
   * @{
   *
   * Scattering from a fixed point charge of \f$ Z \f$.
   *
   *  \f[ \frac{d\sigma}{d\Omega}  = \left( \frac{z Z \alpha}{2 E \sin^2\theta/2}(\right)^2 \f]
   *
   *  We also compute the Mott cross section which is
   *
   *  \f[ \frac{d\sigma}{d\Omega}  = \left( \frac{z Z \alpha}{2 E \sin^2\theta/2}(\right)^2 \cos^2\theta/2 \f]
   *
   */

  /// \f$ \frac{d\sigma}{d\Omega} \f$
  using dsigma_dOmega_v = insane::kine::Var<struct dsigma_dOmega_tag>;
  /// \f$ \frac{d\sigma}{d\Omega}_{Mott} \f$
  using dsigma_dOmega_Mott_v = insane::kine::Var<struct dsigma_dOmega_Mott_tag>;

  using RutherfordMottCrossSections = typename std::tuple<dsigma_dOmega_v, dsigma_dOmega_Mott_v>;

  /** Rutherford scattering cross section.
   *
   *  This lambda is used to add the cross section to a variable stack.
   *
   *  Here we also compute the Mott cross section which includes the spin-1/2 nature of the electron.
   *
   */
  auto Rutherford_dsigma_dOmega =
      insane::xsec::rutherford::make_dsigma<dsigma_dOmega_v>([](const auto& v) {
        using namespace insane::physics;
        const double& E0               = std::get<E_beam_v>(v);
        const double& theta            = std::get<theta_v>(v);
        const double  sin_theta_over_2 = std::sin(theta / 2.0);
        const double  cos_theta_over_2 = std::cos(theta / 2.0);
        const double  res =
            insane::constants::alpha / (2.0 * E0 * sin_theta_over_2 * sin_theta_over_2);
        const double rutherford_xs = res * res * insane::constants::hbarc_squared;
        const double mott_xs       = rutherford_xs * cos_theta_over_2 * cos_theta_over_2;
        // note that we have to be explicit about the return tuple types here to inject the values
        // into an arbitrary variable stack.
        return RutherfordMottCrossSections{rutherford_xs, mott_xs};
      });

  //@}
} // namespace insane::xsec::rutherford

#endif
