#ifndef insane_xsec_MollerScattering_HH
#define insane_xsec_MollerScattering_HH

#include "fmt/core.h"

#include <iostream>
#include <random>
#include <thread>
#include <tuple>

#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "insane/base/Math.h"
#include "insane/kinematics/Elastic.h"
#include "insane/xsec/XSPhaseSpace.h"

// these should not be included
//#include "insane/xsections/RadiativeEffects.h"
//#include "insane/kinematics/HallCSettings.h"

#include "Math/GenVector/VectorUtil.h"
#include "Math/Vector3D.h"

/** Moller scattering cross-section.
 *  electron-electron scattering.
 *
 * \ingroup xsections
 */
namespace insane::xsec::moller {

  using namespace insane::kine::elastic;

  /** \addtogroup MollerScattering Moller Scattering.
   *  Elastic electron-electron scattering.
   * \ingroup xsections
   * @{
   */

  /** \name Differential Cross sections
   * @{
   */
  /// \f$ \frac{d\sigma}{dQ^{2}} \f$
  using dsigma_dQ2_v    = insane::kine::Var<struct V_dsigma_dQ2_tag>;

  /** \f$ \frac{d\sigma}{d\Omega} \f$.
   * \$[
   *  \frac{d \sigma}{d \Omega} = \frac{\alpha^2}{ E_{CM}^2 p^4 \sin^4 \theta} \Big[ 4(m^2 + 2p^2)^2 + \big( 4p^4 - 3(m^2 + 2p^2)^2 
   *  \big) \sin^2 \theta + p^4 \sin^4 \theta \Big]. \$]
   *
   */
  using dsigma_dOmega_v = insane::kine::Var<struct V_dsigma_dOmega_tag>;

  /** \f$ \frac{d\sigma}{dE^{\prime}d\Omega} \f$.
   * Cross section variable for radiative tail.
   */
  using dsigma_dE_dOmega_e_v = insane::kine::Var<struct V_dsigma_dE_dOmega_e_tag>;
  using dsigma_dP_dOmega_v = insane::kine::Var<struct V_dsigma_dP_dOmega_tag>;
  //@}


  /** \name Sachs Form Factors
   * See \ref FormFactors.
   * @{
   */
  using GEp_v           = insane::kine::Var<struct V_GEp_tag>; ///< Proton electric form factor
  using GMp_v           = insane::kine::Var<struct V_GMp_tag>; ///< Proton magnetic form factor
  using GEn_v           = insane::kine::Var<struct V_GEn_tag>; ///< Neutron electric form factor
  using GMn_v           = insane::kine::Var<struct V_GMn_tag>; ///< Neutron magnetic form factor

  using GEp_s_v           = insane::kine::Var<struct V_GEp_s_tag>; ///< Energy peaking (s) Proton electric form factor
  using GMp_s_v           = insane::kine::Var<struct V_GMp_s_tag>; ///< Energy peaking (s) Proton magnetic form factor
  using GEn_s_v           = insane::kine::Var<struct V_GEn_s_tag>; ///< Energy peaking (s) Neutron electric form factor
  using GMn_s_v           = insane::kine::Var<struct V_GMn_s_tag>; ///< Energy peaking (s) Neutron magnetic form factor

  using GEp_ep_elastic_v           = insane::kine::Var<struct V_GEp_ep_elastic_tag>; ///< Proton electric form factor
  using GMp_ep_elastic_v           = insane::kine::Var<struct V_GMp_ep_elastic_tag>; ///< Proton magnetic form factor
  using GEn_ep_elastic_v           = insane::kine::Var<struct V_GEn_ep_elastic_tag>; ///< Neutron electric form factor
  using GMn_ep_elastic_v           = insane::kine::Var<struct V_GMn_ep_elastic_tag>; ///< Neutron magnetic form factor
  //@}

  /** \name Elastic kinematic variables
   * @{
   */
  using E_prime_elastic_v    = insane::kine::Var<struct E_prime_elastic_tag>;
  using Q2_ep_elastic_v     = insane::kine::Var<struct Q2_ep_elastic_tag>;

  using Es_v             = insane::kine::Var<struct V_Es_tag>; ///< Beam energy after radiating photon.
  using Q2_prime_v       = insane::kine::Var<struct V_Q2_prime_tag>; ///< Effective Q2 for beam with energy Es

  using omega_s_v        = insane::kine::Var<struct omega_s_tag>; ///< C.11 from Tsai SLAC-PUB-0408, 1971
  using omega_p_v        = insane::kine::Var<struct omega_p_tag>; ///< C.12 from Tsai SLAC-PUB-0408, 1971

  //@}

  /** The electron scattering angle for elastic scattering computed from the target recoil kinematics.
   */
  inline double GetTheta_e(const double& Es, const double& alpha, const double& M) {
    return 2.0 * std::atan((Es + M) * std::sin(alpha) / (M * std::cos(alpha)));
  }

  /** Elastic electron scattering cross section off proton.
   *
   *  \f[ \frac{d\sigma}{dQ^{2}d\phi_e} \f]
   *
   *  Nonradiative cross section.
   */
  auto Elastic_dsigma_dOmega = insane::xsec::make_dsigma<dsigma_dOmega_v>([](const auto& v) {
    using namespace insane::physics;
    const auto& GEp = std::get<GEp_ep_elastic_v>(v);
    const auto& GMp = std::get<GMp_ep_elastic_v>(v);
    const auto& E0  = std::get<E_beam_v>(v);
    // const auto&           Ep    = std::get<V_eprime>(v);
    const auto& M  = std::get<M_target_v>(v);
    const auto& Q2 = std::get<Q2_ep_elastic_v>(v);
    // const auto&           tau   = std::get<V_tau>(v);
    const auto& theta = std::get<theta_v>(v);
    const auto& Ep    = E0 / (1.0 + (2.0 * E0 / M) * std::pow(std::sin(theta / 2.0), 2));
    if (Ep > E0)
      return 0.0;
    const double tau           = Q2 / (4.0 * M * M);
    const double MollerXSec      = insane::kine::Sig_Moller(E0, theta);
    const double recoil_factor = Ep / E0;
    const double GE2           = GEp * GEp;
    const double GM2           = GMp * GMp;
    const double tanthetaOver2 = std::tan(theta / 2.0);
    // Rosenbluth formula
    const double res =
        MollerXSec * recoil_factor *
        ((GE2 + tau * GM2) / (1.0 + tau) + 2.0 * tau * GM2 * tanthetaOver2 * tanthetaOver2) *
        insane::constants::hbarc_squared;
    return res * std::sin(theta);
  });

  ///** Radiative elastic electron scattering cross section off proton.
  // *
  // *  \f[ \frac{d\sigma}{dQ^{2}d\phi_e} \f]
  // *
  // *  "Radiative correction to the jth peak".
  // *  Eqn 2.3, Tsai, SLAC-PUB-848 1971
  // */
  //auto RadiativeElastic_dsigma_dOmega = insane::xsec::make_dsigma<dsigma_dOmega_v>([](const auto& v) {
  //  using namespace insane::physics;
  //  const auto&           GEp   = std::get<GEp_ep_elastic_v>(v);
  //  const auto&           GMp   = std::get<GMp_ep_elastic_v>(v);
  //  const auto&           E0    = std::get<E_beam_v>(v);
  //  const auto&           M     = std::get<M_target_v>(v);
  //  const auto&           Q2    = std::get<Q2_ep_elastic_v>(v);
  //  const auto&           theta = std::get<theta_v>(v);
  //  const auto&           Ep    = E0/(1.0+(2.0*E0/M)*std::pow(std::sin(theta/2.0),2));
  //  if (Ep > E0)
  //    return 0.0;
  //  const double tau = Q2/(4.0*M*M);
  //  const double MollerXSec      = insane::kine::Sig_Moller(E0, theta);
  //  const double recoil_factor = Ep / E0;
  //  const double GE2           = GEp*GEp;
  //  const double GM2           = GMp*GMp;
  //  const double tanthetaOver2 = std::tan(theta / 2.0);

  //  const double T_mat =  20.0*(1.24)/insane::rad_length(2,4);
  //  const double T_X0 = insane::radcor::tr_Tsai(Q2);
  //  const double T    = T_X0 + T_mat;
  //  const double bT   = T * 4.0 / 3.0;


  //  // Rosenbluth formula
  //  const double sig_el =
  //      MollerXSec * recoil_factor *
  //      ((GE2 + tau * GM2) / (1.0 + tau) + 2.0 * tau * GM2 * tanthetaOver2 * tanthetaOver2) *
  //      insane::constants::hbarc_squared;

  //  const double F_RC     = insane::radcor::F_Tsai(E0, Ep, theta);
  //  const double RC       = insane::radcor::RCToPeak_Tsai(E0, Ep, theta);
  //  const double res      = RC * F_RC * sig_el *std::sin(theta);
  //  return (res);
  //});

  /** Radiative tail from elastic electron-proton scattering.
   *
   *  \f[
   *  \frac{d\sigma}{dE_{e}d\Omega_{e}}
   *  \f]
   *
   *  The effective incident beam energy, Es, is computed from the exclusive kinematics using 
   *  only the recoil 4He momentum vector.
   */
  //auto ElasticTail_dsigma_electron =
  //    insane::xsec::make_dsigma<dsigma_dE_dOmega_e_v>([](const auto& v) {
  //      using namespace TMath;
  //      using namespace insane::physics;
  //      using namespace insane::units;
  //      const auto& GEp         = std::get<GEp_v>(v);
  //      const auto& GMp         = std::get<GMp_v>(v);
  //      const auto& GEp_s       = std::get<GEp_s_v>(v);
  //      const auto& GMp_s       = std::get<GMp_s_v>(v);
  //      const auto& E0          = std::get<E_beam_v>(v);
  //      const auto& Ep          = std::get<E_prime_v>(v);
  //      const auto& Ep_elastic  = std::get<E_prime_elastic_v>(v);
  //      const auto& theta       = std::get<theta_v>(v);
  //      const auto& phi         = std::get<phi_v>(v);
  //      const auto& M           = std::get<M_target_v>(v);
  //      const auto& Q2          = std::get<Q2_v>(v);
  //      const auto& Q2_prime    = std::get<Q2_prime_v>(v);
  //      const auto& ws          = std::get<omega_s_v>(v);
  //      const auto& wp          = std::get<omega_p_v>(v);
  //      const double Es          = E0;
  //      const double Esprime     = E0 - ws;
  //      const double Ep_elastic2 =
  //          Esprime / (1.0 + (2.0 * Esprime / M) * std::pow(std::sin(theta / 2.0), 2));
  //      //if (Esprime < 0.0) {
  //      //  return 0.0;
  //      //}
  //      if (Ep > E0) {
  //        return 0.0;
  //      }
  //      const double tanthetaOver2 = std::tan(theta / 2.0);
  //      const double Moller1         = (Ep / E0) * insane::kine::Sig_Moller(E0, theta);
  //      const double Moller2         = (Ep / Esprime) * insane::kine::Sig_Moller(Esprime, theta);
  //      const double GE2           = GEp * GEp;
  //      const double GM2           = GMp * GMp;
  //      const double GE2_s         = GEp_s * GEp_s;
  //      const double GM2_s         = GMp_s * GMp_s;

  //      double tr_1 = insane::radcor::tr_Tsai(Q2/GeV);
  //      double tr_2 = insane::radcor::tr_Tsai(Q2_prime/GeV);
  //      double T_mat = 20.0*(0.125)/insane::rad_length(2,4);
  //      double T1    = tr_1 +T_mat/2.0;
  //      double bT   = T1 * 4.0 / 3.0;
  //      double T2    = tr_2 +T_mat/2.0;
  //      double bT2   = T2 * 4.0 / 3.0;

  //      // Rosenbluth formula
  //      const double tau1 = Q2/(4.0*M*M);
  //      double F_RC1 = insane::radcor::F_Tsai(E0, Ep, theta,tr_1 +T_mat);
  //      const double RC1       = insane::radcor::RCToPeak_Tsai(E0, Ep, theta);
  //      if(Ep> Ep_elastic)
  //        F_RC1 = 0.0;
  //      const double sig1 =
  //          Moller1 * F_RC1 *
  //          ((GE2 + tau1 * GM2) / (1.0 + tau1) + 2.0 * tau1 * GM2 * tanthetaOver2 * tanthetaOver2) *
  //          insane::constants::hbarc_squared;

  //      const double tau2  = Q2_prime / (4.0 * M * M);
  //      double F_RC2 = insane::radcor::F_Tsai(Esprime, Ep, theta,tr_1 +T_mat);
  //      const double RC2       = insane::radcor::RCToPeak_Tsai(Esprime, Ep, theta);
  //      if(Ep> Ep_elastic2)
  //        F_RC2 = 0.0;
  //      const double sig2  = Moller2 * F_RC2 *
  //                          ((GE2_s + tau2 * GM2_s) / (1.0 + tau2) +
  //                           2.0 * tau2 * GM2_s * tanthetaOver2 * tanthetaOver2) *
  //                          insane::constants::hbarc_squared;

  //      const double A = 4.0;

  //      //std::cout << "T    = " << T << "\n";
  //      //std::cout << "Tmat = " << T_mat << "\n";
  //      //std::cout << "T_x0 = " << T_X0 << "\n";
  //      // Eq'ns C.13 of Tsai 1971, SLAC-PUB-848
  //      //double T00 =
  //      //    (1.0 + 0.5772 * bT) * Power(ws / Es, bT / 2.0) * Power(wp / (Ep + wp), bT / 2.0);
  //      //double T01 = sig1 * bT * DiLog(wp / (Ep + wp)) / (2.0 * wp);
  //      //double T02 = (sig2 * bT * DiLog(ws / Es) / (2.0 * ws)) *
  //      //             (M + (Es - ws) * (1.0 - Cos(theta))) / (M - Ep * (1.0 - Cos(theta)));
  //      //std::cout << "vs = " << ws / Es << "\n";
  //      //std::cout << "vp = " << (wp/(Ep+wp)) << "\n";
  //      double xi  = insane::radcor::LandauStraggling_xi(T_mat,1,1);
  //      // Eq'ns 3.1 of Tsai 1971, SLAC-PUB-848
  //      double costh = std::cos(theta);
  //      double T00 = std::pow((ws/Es)*(wp/(Ep+wp)),bT);
  //      double T01 = sig1 * (bT * DiLog(wp / (Ep + wp)) / ( wp)  + xi/(2.0*wp*wp));
  //      double T02 = sig2 * ((M+(Es-ws)*(1.0-costh))/(M-Ep*(1.0-costh)))*(bT/(ws)*DiLog(ws/Es) +xi/(2.0*ws*ws) );

  //      // multi photon exchange term
  //      //std::array<double, 2> ffs =  {GEp, GMp};
  //      //double integral_result = insane::integrate::simple_threads(
  //      //    [=](double x) {
  //      //      return insane::radcor::InternalIntegrand_MoTsai69(x, Es/GeV, Ep/GeV, theta, phi, M/GeV, ffs);
  //      //    },
  //      //    -1.0, 1.0,200);
  //      double alpha = insane::constants::alpha;
  //      double sigma_internal = 0.0;//(alpha * alpha * alpha / (4.0 * M_PI * M_PI)) * (Ep / Es / M) *
  //                              //integral_result * insane::constants::hbarc_squared/GeV/GeV;
  //      double sig_rad = T00 * (T01 + T02);
  //      if (sig_rad < 0.0 || TMath::IsNaN(sig_rad))
  //        sig_rad = 0.0;
  //      return (sigma_internal + sig_rad) * std::sin(theta);
  //      });

  /** Jacobian for to change from inclusive electron variables to inclusive recoil variables.
   *
   * \f[
   *   \frac{dE_e d\Omega_e}{dP_{\alpha}d\Omega_{\alpha}}
   * \f]
   *
   */
  inline double ElectronToAlphaJacobian(const double& Palpha, const double& thetaalpha, const double& M) {
    using namespace TMath;
    //const double dtheta_dalpha =
    //    2.0 * M * (Es + M) /
    //    (std::pow(M * std::cos(alpha), 2.0) + std::pow((Es + M) * std::sin(alpha), 2.0));
    //const double res = dtheta_dalpha * std::sin(theta);
    const double Ealpha =  std::sqrt(std::pow(M,2) + std::pow(Palpha,2));
    const double cosalpha = std::cos(thetaalpha);
    const double res =
        ((std::pow(Palpha, 3) + M * Palpha * Ealpha -
          (2 * std::pow(M, 3) + 3 * M * std::pow(Palpha, 2) + 2 * std::pow(M, 2) * Ealpha +
           2 * std::pow(Palpha, 2) * Ealpha) *
              cosalpha +
          Palpha * (std::pow(Palpha, 2) + 2 * M * Ealpha) * std::pow(cosalpha, 2) +
          std::pow(M, 2) * Palpha * (2 + std::cos(2 * thetaalpha))) *
         std::sin(thetaalpha)) /
        (Ealpha * (Ealpha - Palpha * cosalpha) * (-Palpha + (M + Ealpha) * cosalpha) *
         std::sqrt(((std::pow(Palpha, 2) - 2 * Palpha * (M + Ealpha) * cosalpha +
                (std::pow(Palpha, 2) + 2 * M * (M + Ealpha)) * std::pow(cosalpha, 2)) *
               std::pow(std::sin(thetaalpha), 2)) /
              std::pow(Ealpha - Palpha * cosalpha, 2)));
    return std::abs(res);
  }

  //?/** Radiative tail from elastic electron-proton scattering.
  //? *
  //? *  \f[
  //? *  \frac{d\sigma}{dP_{p}d\Omega_{p}}
  //? *  \f]
  //? *
  //? */
  //?auto ElasticTail_dsigma_proton =
  //?    insane::xsec::make_dsigma<dsigma_dP_dOmega_v>([](const auto& v) {
  //?      using namespace TMath;
  //?      using namespace insane::physics;
  //?      using namespace insane::units;
  //?      //const auto& FCHe4       = std::get<V_FCHe4>(v);
  //?      const auto& E0          = std::get<E_beam_v>(v);
  //?      const auto& Ep          = std::get<E_prime_v>(v);
  //?      const auto& Es          = std::get<Es_v>(v);
  //?      const auto& M           = std::get<M_target_v>(v);
  //?      const auto& Q2          = std::get<Q2_v>(v);
  //?      const auto& theta       = std::get<theta_v>(v);
  //?      const auto& theta_alpha = std::get<theta_recoil_v>(v);
  //?      const auto& P_alpha     = std::get<P_recoil_v>(v);
  //?      const auto& E_alpha     = std::get<E_recoil_v>(v);
  //?      //const auto& GEp         = std::get<V_GEp>(v);
  //?      //const auto& GMp         = std::get<V_GMp>(v);
  //?      if (Es < 0.0) {
  //?        return 0.0;
  //?      }
  //?      if (Es > E0) {
  //?        return 0.0;
  //?      }
  //?      const auto& GEp         = std::get<GEp_v>(v);
  //?      const auto& GMp         = std::get<GMp_v>(v);


  //?      const double delta_function = Abs(1.0 / (2.0 * (M - E_alpha + P_alpha * Cos(theta_alpha))));
  //?      const double I              = insane::radcor::Ib(E0, Es, 0.05);
  //?      // Here the sin(theta)/sin(theta_alpha) is the jacobian for the two solid angles Omega -> sin(theta) dtheta dphi
  //?      // The latter cancels again below which is why it commented out.
  //?      const double jac            = ElectronToAlphaJacobian(P_alpha, theta_alpha, M)*std::sin(theta);// /std::sin(theta_alpha);
  //?      const double tau = Q2/(4.0*M*M);
  //?      const double MollerXSec      = insane::kine::Sig_Moller(E0, theta);
  //?      const double recoil_factor = Ep / E0;
  //?      const double GE2           = GEp*GEp;
  //?      const double GM2           = GMp*GMp;
  //?      const double tanthetaOver2 = std::tan(theta / 2.0);
  //?      // Rosenbluth formula
  //?      const double res =
  //?          MollerXSec * recoil_factor * delta_function * I * jac *
  //?          ((GE2 + tau * GM2) / (1.0 + tau) + 2.0 * tau * GM2 * tanthetaOver2 * tanthetaOver2) *
  //?          insane::constants::hbarc_squared;
  //?      if(res < 0.0 ) return 0.0;
  //?      if(std::isnan(res))  return 0.0;
  //?      return res;//*std::sin(theta_alpha);
  //?    });


  //inline void PrintElasticKinematics(const auto& v) {
  //  using namespace insane::units;
  //        fmt::print("{:<12} = {: <8f} {}\n", "Q2", std::get<Q2_v>(v) / GeV / GeV, "GeV^{2}");
  //        fmt::print("{:<12} = {: <8f} {}\n", "E0", std::get<E_beam_v>(v) / GeV, "GeV");
  //        fmt::print("{:<12} = {: <8f} {}\n", "E_e'", std::get<E_prime_v>(v) / GeV, "GeV");
  //        fmt::print("{:<12} = {: <8f} {}\n", "th_e", std::get<theta_v>(v) / degree, "degree");
  //        fmt::print("{:<12} = {: <8f} {}\n", "phi_e", std::get<phi_v>(v) / degree, "degree");
  //        fmt::print("{:<12} = {: <8f} {}\n", "nu", std::get<nu_v>(v) / GeV, "GeV");
  //        // fmt::print("{:<12} = {: <8f} {}\n", "P_recoil", std::get<V_p_recoil>(v) / GeV, "GeV");
  //        // fmt::print("{:<12} = {: <8f} {}\n", "th_recoil", std::get<V_theta_recoil>(v) / degree,
  //        // "degree"); fmt::print("{:<12} = {: <8f} {}\n", "phi_recoil", std::get<V_phi_recoil>(v) /
  //        // degree, "degree"); fmt::print("{:<12} = {: <8f} {}\n", "E_recoil",
  //        // std::get<V_E_recoil>(v).get() / GeV, "GeV"); fmt::print("{:<12} = {: <8f} {}\n",
  //        // "V_FCHe4", std::get<V_FCHe4>(v), "");
  //        fmt::print("\n");
  //}

  //@}
} // namespace insane::xsec::elastic

#endif
