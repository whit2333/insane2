#ifndef insane_xsec_DIS_HXX
#define insane_xsec_DIS_HXX

namespace insane::xsec::dis {

  inline auto add_polarized_DIS_coefficients = [](const auto& v) {
    using namespace insane::kine::dis;
    const auto&  eps   = std::get<epsilon_v>(v);
    const auto&  E0    = std::get<E_beam_v>(v);
    const auto&  Ep    = std::get<E_prime_v>(v);
    const auto&  R     = std::get<R_v>(v);
    const auto&  Q2    = std::get<Q2_v>(v);
    const double D     = (E0 - eps * Ep) / (E0 * (1.0 + eps * R));
    const double d     = D * std::sqrt(2.0 * eps / (1.0 + eps));
    const double eta   = eps * std::sqrt(Q2) / (E0 - eps * Ep);
    const double ksi   = eta * (1 + eps) / (2.0 * eps);
    const auto&  th    = std::get<theta_v>(v);
    const auto&  ph    = std::get<phi_v>(v);
    const double sinth = std::sin(th);
    const double costh = std::cos(th);
    const double chi   = Ep * sinth / ((E0 - Ep * costh) * std::cos(ph));
    return std::tuple<D_coef_v, d_coef_v, eta_coef_v, ksi_coef_v, chi_coef_v>{D, d, eta, ksi, chi};
  };

}

#endif
