#ifndef insane_xsec_Maid_HH
#define insane_xsec_Maid_HH

#include "insane/base/FortranWrappers.h"
#include "fmt/core.h"
#include "insane/kinematics/KinematicFunctions.h"

#include <iostream>
#include <random>
#include <thread>
#include <tuple>

#include "insane/xsections/RadiativeEffects.h"
#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/Core.h"
#include "insane/kinematics/HallCSettings.h"
#include "insane/kinematics/Inelastic.h"
#include "insane/xsec/XSPhaseSpace.h"
#include "insane/base/Math.h"

#include "Math/GenVector/VectorUtil.h"
#include "Math/Vector3D.h"

namespace insane::xsec::maid {

  /** \addtogroup pionElectroproduction Pion Electroproduction 
   * \ingroup xsections
   *
   * @{
   */

  /** \name Virtual Photo absorption cross sections.
   * @{
   */
  /// \f$ \frac{d\sigma}{dQ^{2}} \f$
  using V_sigma_T       = insane::kine::Var<struct V_sigma_T_tag>;
  using V_sigma_L       = insane::kine::Var<struct V_sigma_L_tag>;
  using V_sigma_LT      = insane::kine::Var<struct V_sigma_LT_tag>;
  using V_sigma_LTp     = insane::kine::Var<struct V_sigma_LTp_tag>;
  using V_sigma_TT      = insane::kine::Var<struct V_sigma_TT_tag>;
  using V_sigma_TTp     = insane::kine::Var<struct V_sigma_TTp_tag>;
  using V_sigma_L0      = insane::kine::Var<struct V_sigma_L0_tag>;
  using V_sigma_LT0     = insane::kine::Var<struct V_sigma_LT0_tag>;
  using V_sigma_LT0p    = insane::kine::Var<struct V_sigma_LT0p_tag>;

  using V_sigma_VPACs = std::tuple<V_sigma_T, V_sigma_L, V_sigma_LT, V_sigma_LTp, V_sigma_TT,
                                   V_sigma_TTp, V_sigma_L0, V_sigma_LT0, V_sigma_LT0p>;
  //@}

  V_sigma_VPACs ProtonVPACs(double x, double Q2) {
    using namespace insane::units;
    double sigT    = 0.0;
    double sigL    = 0.0;
    double sigLT   = 0.0;
    double sigLTp  = 0.0;
    double sigTTp  = 0.0;
    double sigL0   = 0.0;
    double sigLT0  = 0.0;
    double sigLT0p = 0.0;

    V_sigma_VPACs res(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

    double q2  = Q2 / GeV / GeV;
    double W   = insane::kine::W_xQsq(x, Q2 / GeV / GeV) * 1000.0; // W in MeV
    int    iso = 1;                                                // gamma + p -> pi0 + p

    if (q2 >= 0.0 && q2 < 5.0) {
      if (W > 1073.2 && W <= 1800.0) {
        iso = 1; // gamma + p -> pi+ + n
        maid07tot_(&iso, &W, &q2, &sigT, &sigL, &sigLT, &sigLTp, &sigTTp, &sigL0, &sigLT0, &sigLT0p);
        std::get<V_sigma_T>(res) += (sigT / 1000.0);
        std::get<V_sigma_L>(res) += (sigL / 1000.0);
        std::get<V_sigma_LT>(res) += (sigLT / 1000.0);
        std::get<V_sigma_LTp>(res) += (sigLTp / 1000.0);
        std::get<V_sigma_TT>(res) += (sigTT / 1000.0);
        std::get<V_sigma_TTp>(res) += (sigTTp / 1000.0);
        std::get<V_sigma_L0>(res) += (sigL0 / 1000.0);
        std::get<V_sigma_LT0>(res) += (sigLT0 / 1000.0);
        std::get<V_sigma_LT0p>(res) += (sigLT0p / 1000.0);
      }

      // pi+ threshold is slightly larger than pi0
      if (W > 1079.1 && W <= 1800.0) {
        iso = 3; // gamma + p -> pi+ + n
        maid07tot_(&iso, &W, &q2, &sigT, &sigL, &sigLT, &sigLTp, &sigTTp, &sigL0, &sigLT0, &sigLT0p);
        std::get<V_sigma_T>(res) += (sigT / 1000.0);
        std::get<V_sigma_L>(res) += (sigL / 1000.0);
        std::get<V_sigma_LT>(res) += (sigLT / 1000.0);
        std::get<V_sigma_LTp>(res) += (sigLTp / 1000.0);
        std::get<V_sigma_TT>(res) += (sigTT / 1000.0);
        std::get<V_sigma_TTp>(res) += (sigTTp / 1000.0);
        std::get<V_sigma_L0>(res) += (sigL0 / 1000.0);
        std::get<V_sigma_LT0>(res) += (sigLT0 / 1000.0);
        std::get<V_sigma_LT0p>(res) += (sigLT0p / 1000.0);
      }
    }
    return res;
  }

  auto NeutronVPACs(Double_t x, Double_t Q2) {
    using namespace insane::units;
    double sigT    = 0.0;
    double sigL    = 0.0;
    double sigLT   = 0.0;
    double sigLTp  = 0.0;
    double sigTTp  = 0.0;
    double sigL0   = 0.0;
    double sigLT0  = 0.0;
    double sigLT0p = 0.0;

    V_sigma_VPACs res(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

    double q2  = Q2 / GeV / GeV;
    double W   = insane::kine::W_xQsq(x, Q2 / GeV / GeV) * 1000.0; // W in MeV
    int    iso = 1;                                    // gamma + p -> pi0 + p

    if (Q2 >= 0.0 && Q2 < 5.0) {
      if (W > 1073.2 && W <= 1800.0) {
        iso = 2; // gamma + n -> pi0 + n
        maid07tot_(&iso, &W, &q2, &sigT, &sigL, &sigLT, &sigLTp, &sigTTp, &sigL0, &sigLT0,
                   &sigLT0p);
        std::get<V_sigma_T>(res) += (sigT / 1000.0);
        std::get<V_sigma_L>(res) += (sigL / 1000.0);
        std::get<V_sigma_LT>(res) += (sigLT / 1000.0);
        std::get<V_sigma_LTp>(res) += (sigLTp / 1000.0);
        std::get<V_sigma_TT>(res) += (sigTT / 1000.0);
        std::get<V_sigma_TTp>(res) += (sigTTp / 1000.0);
        std::get<V_sigma_L0>(res) += (sigL0 / 1000.0);
        std::get<V_sigma_LT0>(res) += (sigLT0 / 1000.0);
        std::get<V_sigma_LT0p>(res) += (sigLT0p / 1000.0);
      }

      // pi+ threshold is slightly larger than pi0
      if (W > 1079.1 && W <= 1800.0) {
        iso = 4; // gamma + n -> pi- + p
        maid07tot_(&iso, &W, &q2, &sigT, &sigL, &sigLT, &sigLTp, &sigTTp, &sigL0, &sigLT0,
                   &sigLT0p);
        std::get<V_sigma_T>(res) += (sigT / 1000.0);
        std::get<V_sigma_L>(res) += (sigL / 1000.0);
        std::get<V_sigma_LT>(res) += (sigLT / 1000.0);
        std::get<V_sigma_LTp>(res) += (sigLTp / 1000.0);
        std::get<V_sigma_TT>(res) += (sigTT / 1000.0);
        std::get<V_sigma_TTp>(res) += (sigTTp / 1000.0);
        std::get<V_sigma_L0>(res) += (sigL0 / 1000.0);
        std::get<V_sigma_LT0>(res) += (sigLT0 / 1000.0);
        std::get<V_sigma_LT0p>(res) += (sigLT0p / 1000.0);
      }
    }
    return res;
  }
} // namespace insane::xsec::maid
#endif
