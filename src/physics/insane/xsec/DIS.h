#ifndef insane_xsec_DIS_HH
#define insane_xsec_DIS_HH

#include "fmt/core.h"

#include <iostream>
#include <random>
#include <thread>
#include <tuple>

#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/Core.h"
#include "insane/kinematics/Inelastic.h"
#include "insane/xsec/XSPhaseSpace.h"


/** Deep inelastic scattering.
 *
 * \ingroup xsections
 */
namespace insane::xsec::dis {

  /** \addtogroup DIS Deep Inelastic Scattering
   *
   * DIS \f$ (e,e')X \f$.
   *
   * \f[ \frac{d^2 \sigma }{dx\,dy}=\frac{4 \pi  \alpha^2
   *  }{x\, y\, Q^2} \left\{  xy^2 F_1(x,Q^2) +
   *  \left[1-y-\frac{M^2x^2 y^2}{Q^2}\right] F_2(x,Q^2)  \right\}, \f]
   *
   *  \f[ \frac{d^2\sigma^{\rightarrow}_{\Leftarrow}}{dx\,dy}- \frac{d^2\sigma^{\rightarrow}_{\Rightarrow}}{ dx\,dy} 
   *  = \frac{16\pi\alpha^2}{Q^2}\Biggl[\left(1-\frac{y}{2}-\frac{y^2 M^2 x^2} {Q^2}\right)\,g_1(x,Q^2)  
   *  - \frac{2M^2x^2y}{Q^2}g_2(x,Q^2) \Biggl] \,,\f]
   *
   * \ingroup xsections
   * @{
   */

  using namespace insane::kine::dis;

  /** \name Cross Section Variables
   * @{
   */

  /** \f[ \frac{d\sigma}{dx dy} \f].
   */
  using dsigma_dx_dy_v = insane::kine::Var<struct dsigma_dx_dy_tag>;

  /** \f[ \frac{d\sigma}{dx dQ^{2} d\phi_e} \f].
   */
  using dsigma_dx_dQ2_dphi_v = insane::kine::Var<struct dsigma_dx_dQ2_dphi_tag>;

  /** \f[ \frac{d\sigma}{dE d\Omega } \f].
   */
  using dsigma_dE_dOmega_v = insane::kine::Var<struct dsigma_dE_dOmega_tag>;
  using dsigma_dE_dOmega_e_v = insane::kine::Var<struct dsigma_dE_dOmega_e_tag>;
  //@}

  /** \name Nucleon Structure Functions
   * F1 and F2 .
   * @{
   */
  using F1p_v                          = insane::kine::Var<struct F1p_tag>;
  using F2p_v                          = insane::kine::Var<struct F2p_tag>;
  using F1n_v                          = insane::kine::Var<struct F1n_tag>;
  using F2n_v                          = insane::kine::Var<struct F2n_tag>;
  using R_v                            = insane::kine::Var<struct R_tag>;
  //@}

  /** \name Nucleon Spin Structure Functions
   * g1 and g2 .
   * @{
   */
  using g1p_v                          = insane::kine::Var<struct g1p_tag>;
  using g2p_v                          = insane::kine::Var<struct g2p_tag>;
  using g1n_v                          = insane::kine::Var<struct g1n_tag>;
  using g2n_v                          = insane::kine::Var<struct g2n_tag>;

  using alpha_target_v =
      insane::kine::Var<struct alpha_target_tag>;          ///< polarized target polar angle
  using g1_coef_v = insane::kine::Var<struct g1_coef_tag>; ///<  coefficient for g1
  using g2_coef_v = insane::kine::Var<struct g2_coef_tag>; ///<  coefficient for g2
  using D_coef_v  = insane::kine::Var<struct D_coef_tag>;  ///<  coefficient for g2
  using d_coef_v  = insane::kine::Var<struct d_coef_tag>;  ///<  coefficient for g2
  using eta_coef_v =
      insane::kine::Var<struct eta_coef_tag>; ///<  coefficient for polarized DIS (see Anselmino et
                                              ///<  al  Physics Reports 261(1995) Appendix A)
  using ksi_coef_v =
      insane::kine::Var<struct ksi_coef_tag>; ///<  coefficient for polarized DIS (see Anselm
                                              ///<  al  Physics Reports 261(1995) Appendix A)
  using chi_coef_v = insane::kine::Var<struct chi_coef_tag>; ///<  coefficient for g2

  //@}
  //@}

  /** Adds the coefficients D,d,eta,ksi commonly used in polarized DIS.
   */
  auto add_polarized_DIS_coefficients = [](const auto& v) {
    const auto&  eps   = std::get<epsilon_v>(v);
    const auto&  E0    = std::get<E_beam_v>(v);
    const auto&  Ep    = std::get<E_prime_v>(v);
    const auto&  R     = std::get<R_v>(v);
    const auto&  Q2    = std::get<Q2_v>(v);
    const double D     = (E0 - eps * Ep) / (E0 * (1.0 + eps * R));
    const double d     = D * std::sqrt(2.0 * eps / (1.0 + eps));
    const double eta   = eps * std::sqrt(Q2) / (E0 - eps * Ep);
    const double ksi   = eta * (1 + eps) / (2.0 * eps);
    const auto&  th    = std::get<theta_v>(v);
    const auto&  ph    = std::get<phi_v>(v);
    const double sinth = std::sin(th);
    const double costh = std::cos(th);
    const double chi   = Ep * sinth / ((E0 - Ep * costh) * std::cos(ph));
    return std::tuple<D_coef_v, d_coef_v, eta_coef_v, ksi_coef_v, chi_coef_v>{D, d, eta, ksi, chi};
  };

  /** This is for the inplane cross section difference.
   */
  auto add_g1_g2_coefficients = [](const auto& v) {
    const auto plane_dir = 1.0; // or -1.0
    const auto&  alpha    = std::get<alpha_target_v>(v);
    const auto&  E0       = std::get<E_beam_v>(v);
    const auto&  Ep       = std::get<E_prime_v>(v);
    const auto&  M        = insane::masses::M_p / insane::units::GeV;
    const auto&  th       = std::get<theta_v>(v);
    const double sinth    = std::sin(th);
    const double costh    = std::cos(th);
    const double sinalpha = std::sin(alpha);
    const double cosalpha = std::cos(alpha);
    const auto&  nu       = E0 - Ep;
    const double cg1 =
        (E0 * cosalpha + Ep * (plane_dir * sinth * sinalpha + costh * cosalpha)) / (M * nu);
    const double cg2 =
        2.0 * E0 * Ep * (plane_dir * sinth * sinalpha + costh * cosalpha) / (M * nu * nu);
    return std::tuple<g1_coef_v, g2_coef_v>{cg1, cg2};
  };

  auto add_unpolarized_dsigma_xQ2 = [](const auto& v) constexpr {
    using namespace insane::physics;
    const auto&  F1            = std::get<F1p_v>(v);
    const auto&  F2            = std::get<F2p_v>(v);
    const auto&  E0            = std::get<E_beam_v>(v);
    const auto&  x             = std::get<x_v>(v);
    const auto&  M             = std::get<M_target_v>(v);
    const auto&  Q2            = std::get<Q2_v>(v);
    const auto&  y             = std::get<y_v>(v);
    const double s             = Q2 / (x * y) + M * M;
    const double alpha         = insane::constants::alpha;
    const double sig0 = 2.0*alpha*alpha*s/(Q2*Q2);
    const double rest = 0.5*(1.0+(1-y)*(1-y))*2*x*F1 + (1-y)*(F2-2*x*F1) - M*x*y*F2/(2.0*E0);
    return dsigma_dx_dQ2_dphi_v{insane::units::hbarc2_GeV_nb*sig0*rest*y/Q2};
  };


  inline auto add_dsigma_dE_dOmega = [](const auto& v) constexpr {
    using namespace insane::physics;
    const auto&  F1            = std::get<F1p_v>(v);
    const auto&  F2            = std::get<F2p_v>(v);
    const auto&  E0            = std::get<E_beam_v>(v);
    const auto&  Ep            = std::get<E_prime_v>(v);
    const auto&  M             = std::get<M_target_v>(v);
    const auto&  Q2            = std::get<Q2_v>(v);
    const auto&  y             = std::get<y_v>(v);
    const auto&  nu            = std::get<nu_v>(v);
    const auto&  th            = std::get<theta_v>(v);
    const double tanthetaOver2 = std::tan(th / 2.0);
    const double SIN           = std::sin(th / 2.);
    const double COS           = std::cos(th / 2.);
    const double SIN2          = SIN * SIN;
    const double COS2          = COS * COS;
    const double TAN2          = SIN2 / COS2;
    const double W1            = (1. / M) * F1;
    const double W2            = (1. / nu) * F2;
    const double alpha         = insane::constants::alpha;
    const double num           = alpha * alpha * COS2;
    const double den           = 4. * E0 * E0 * SIN2 * SIN2;
    const double MottXS        = num / den;
    const double fullXsec      = MottXS * (W2 + 2.0 * tanthetaOver2 * W1);
    if (std::isnan(fullXsec))
      return dsigma_dE_dOmega_e_v{0.0};
    if (fullXsec < 0)
      return dsigma_dE_dOmega_e_v{0.0};
    return dsigma_dE_dOmega_e_v{insane::units::hbarc2_GeV_nb*fullXsec * std::sin(th)};
  };
  auto DIS_dsigma_dE_dOmega = insane::xsec::make_dsigma<dsigma_dE_dOmega_e_v>(add_dsigma_dE_dOmega);


} // namespace insane::xsec::dis

// Doesn't work... too much tmp
//#include "insane/xsec/DIS.hxx"

#endif
