#ifndef insane_xsec_CoherentJpsiElectroproduction_HH
#define insane_xsec_CoherentJpsiElectroproduction_HH

#include "fmt/core.h"

#include <iostream>
#include <random>
#include <thread>
#include <tuple>

#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/Core.h"
#include "insane/kinematics/HallCSettings.h"
#include "insane/kinematics/Inelastic.h"
#include "insane/xsec/XSPhaseSpace.h"

#include "Math/GenVector/VectorUtil.h"
#include "Math/Vector3D.h"

namespace insane::xsec::jpsi_production {

  /** \addtogroup jpsiElectro Coherent J/psi Electroproduction
   * \ingroup xsections
   *
   * Coherent J/\f$\psi\f$ electropduction on a nucleus of A nucleons.
   *
   * \f[ A(e,e' J/\psi)A \f]
   *
   * @{
   */

  /** \name Differential Cross Sections
   * @{
   */
      using dsigma_dt_v                     = Var<struct dsigma_dt_tag>;
      using dsigma_dt1_v                    = Var<struct dsigma_dt1_tag>;
      using dsigma_dt2_v                    = Var<struct dsigma_dt2_tag>;
      using dsigma_dEdOmega_v               = Var<struct dsigma_dEdOmega_tag>;
      using dsigma_dEdOmega_dOmegaRecoil_v  = Var<struct dsigma_dEdOmega_dPdOmegaRecoil_tag>;
      using dsigma_dEdOmega_dOmegaRecoil1_v = Var<struct dsigma_dEdOmega_dPdOmegaRecoil1_tag>;
      using dsigma_dEdOmega_dOmegaRecoil2_v = Var<struct dsigma_dEdOmega_dPdOmegaRecoil2_tag>;
      using dsigma_dEdOmega_dt_v            = Var<struct dsigma_dEdOmega_dt_tag>;
      //@}

      /** \name Kinematic Variables 
       * @{
       */
      using M_target_v  = Var<struct M_target_tag>;
      using M_recoil_v  = Var<struct M_recoil_tag>;
      using k_beam_vec  = FourVec<struct k_beam_tag>;
      using k_prime_vec = FourVec<struct k_prime_tag>;
      using q_vec       = FourVec<struct q_vec_tag>;
      using theta_q_v   = Var<struct theta_q_tag>;
      using nu_cm_v     = Var<struct nu_cm_tag>;
      using q_v         = Var<struct q_tag>;
      using q_cm_v      = Var<struct P_init_cm_tag>;

      // exclusive kinematics:
      using nu_threshold_v = Var<struct nu_threshold_tag>;
      using t_max_v        = Var<struct t_max_tag>;
      using t_min_v        = Var<struct t_min_tag>;
      using t_v            = Var<struct t_tag>;
      using t1_v           = Var<struct t1_tag>;
      using t2_v           = Var<struct t2_tag>;
      using s01_v          = Var<struct s01_tag>;
      using s02_v          = Var<struct s02_tag>;
      using s_v            = Var<struct s_tag>;
      using s2_v           = Var<struct s2_tag>;

      using M_recoil_v         = Var<struct M_recoil_tag>;
      using E_recoil_v         = Var<struct E_recoil_tag>;
      using P_recoil_v         = Var<struct P_recoil_tag>;
      using P_recoil1_v        = Var<struct P_recoil1_tag>;
      using P_recoil2_v        = Var<struct P_recoil2_tag>;
      using P_recoil_cm_v      = Var<struct P_recoil_cm_tag>;
      using theta_recoil_v     = Var<struct theta_recoil_tag>;
      using theta_recoil_max_v = Var<struct theta_recoil_max_tag>;
      using phi_recoil_v       = Var<struct phi_recoil_tag>;
      using KE_recoil_v        = Var<struct KE_recoil_tag>;
      using KE_recoil1_v       = Var<struct KE_recoil1_tag>;
      using KE_recoil2_v       = Var<struct KE_recoil2_tag>;
      using theta_pq_v         = Var<struct theta_pq_tag>;

      using M_psi_v  = Var<struct M_psi_tag>;
      using P_had_v  = Var<struct P_had_tag>;
      using P_had1_v = Var<struct P_had1_tag>;
      using P_had2_v = Var<struct P_had2_tag>;

      using p_recoil_vec  = FourVec<struct p_recoil_vec_tag>;
      using p_recoil1_vec = FourVec<struct p_recoil1_vec_tag>;
      using p_recoil2_vec = FourVec<struct p_recoil2_vec_tag>;
      using p_had_vec     = FourVec<struct p_had_vec_tag>;
      using p_had1_vec    = FourVec<struct p_had1_vec_tag>;
      using p_had2_vec    = FourVec<struct p_had2_vec_tag>;

      using W1_v = Var<struct W1_tag>;
      using W2_v = Var<struct W2_tag>;

      using epsilon_v = Var<struct epsilon_tag>;
      using Gamma_v   = Var<struct Gamma_tag>;

      //@}

      auto CoherentJpsiElectroproduction_kinematics() {

        template <typename T>
        using Lim = insane::xsec::VarLimits<T>;
        template <typename T>
        using ULim  = insane::xsec::VarLimits<T, true>;
        using HallC = insane::hallc::HallCSetting;

        using insane::units::degree;
        using insane::units::GeV;
        // using ROOT::Math::XYZTVector;

        using Lee4HeJpsiPhotoproduction = insane::physics::Lee4HeJpsiPhotoproduction;
        using namespace insane::kine;
        using namespace insane::kine::var;

        // --------------------------------------------------------------------------
        //
        HallC hc;
        hc.SHMS_p0 = 1.5;

        //
        // --------------------------------------------------------------------------
        //
        std::random_device               rd;
        std::mt19937                     gen(rd());
        std::uniform_real_distribution<> dist(hc.SHMS_P_min() * GeV, hc.SHMS_P_max() * GeV);
        //
        // --------------------------------------------------------------------------
        //
        auto ind_vars =
            make_independent_vars<E_prime_v, theta_v, phi_v, theta_recoil_v, phi_recoil_v>();

        auto var_stack =
            ind_vars
                .add<M_target_v, M_recoil_v, E_beam_v, M_psi_v>([](const auto& v) constexpr {
                  // const double M_psi = 3.096916*GeV;
                  const double M_psi = 3.096916 * GeV;
                  const double M_4He = insane::masses::M_4He;
                  return std::tuple<M_target_v, M_recoil_v, E_beam_v, M_psi_v>(M_4He, M_4He,
                                                                               10.6 * GeV, M_psi);
                })
                .add<k_beam_vec, k_prime_vec, q_vec>(
                    // Four vectors k and k'
                    [](const auto& v) constexpr {
                      const auto&                  E0    = std::get<E_beam_v>(v);
                      const auto&                  Ep    = std::get<E_prime_v>(v);
                      const auto&                  th    = std::get<theta_v>(v);
                      const auto&                  phi   = std::get<phi_v>(v);
                      const double                 M_p   = std::get<M_target_v>(v);
                      const double                 sinth = std::sin(th / 2.0);
                      const ROOT::Math::XYZTVector kbeam(0.0, 0.0, E0, E0);
                      const ROOT::Math::XYZTVector kprime(Ep * std::cos(phi) * std::sin(th),
                                                          Ep * std::sin(phi) * std::sin(th),
                                                          Ep * std::cos(th), Ep);
                      return std::tuple<k_beam_vec, k_prime_vec, q_vec>(kbeam, kprime,
                                                                        kbeam - kprime);
                    })
                .add<Q2_v, nu_v>(
                    // Compute Q2 and nu (note separate lambdas)
                    [](const auto& v) constexpr {
                      const auto&  E0    = std::get<E_beam_v>(v);
                      const auto&  Ep    = std::get<E_prime_v>(v);
                      const auto&  th    = std::get<theta_v>(v);
                      const double sinth = std::sin(th / 2.0);
                      return 4.0 * E0 * Ep * sinth * sinth;
                    },
                    [](const auto& v) constexpr {
                      const auto& E0 = std::get<E_beam_v>(v);
                      const auto& Ep = std::get<E_prime_v>(v);
                      return E0 - Ep;
                    })
                .add<nu_threshold_v>(
                    // Compute nu threshold
                    [](const auto& v) constexpr {
                      const double M_p   = std::get<M_recoil_v>(v);
                      const double M_psi = std::get<M_psi_v>(v);
                      const auto&  nu    = std::get<nu_v>(v);
                      const auto&  Q2    = std::get<Q2_v>(v);
                      return (M_psi * M_psi + 2.0 * M_p * M_psi + Q2) / (2.0 * M_p);
                    })
                .add<nu_cm_v, q_cm_v>([](const auto& v) constexpr {
                  // photon energy boosted to CM system
                  const double M_p = std::get<M_target_v>(v);
                  const auto&  Q2  = std::get<Q2_v>(v);
                  const auto&  nu  = std::get<nu_v>(v);
                  const double nu_cm =
                      std::sqrt(std::pow(M_p * nu - Q2, 2) / (M_p * M_p + 2.0 * M_p * nu - Q2));
                  return std::tuple<nu_cm_v, q_cm_v>(nu_cm, std::sqrt(nu_cm * nu_cm + Q2));
                })
                .add<x_v, y_v, q_v>([](const auto& v) constexpr {
                  const auto&  Q2  = std::get<Q2_v>(v);
                  const auto&  nu  = std::get<nu_v>(v);
                  const auto&  E0  = std::get<E_beam_v>(v);
                  const double M_p = insane::masses::M_p;
                  return std::tuple<x_v, y_v, q_v>(Q2 / (2.0 * M_p * nu), nu / E0,
                                                   std::sqrt(nu * nu + Q2));
                })
                .add<theta_q_v>([](const auto& v) constexpr {
                  const auto&  q   = std::get<q_v>(v);
                  const auto&  E0  = std::get<E_beam_v>(v);
                  const auto&  Ep  = std::get<E_prime_v>(v);
                  const double arg = (E0 * E0 + q * q - Ep * Ep) / (2.0 * q * E0);
                  return std::acos(arg);
                })
                .add<W_v>([](const auto& v) constexpr {
                  const double M_p = std::get<M_target_v>(v);
                  const auto&  Q2  = std::get<Q2_v>(v);
                  const auto&  nu  = std::get<nu_v>(v);
                  return std::sqrt(M_p * M_p - Q2 + 2.0 * M_p * nu);
                })
                .add<P_recoil_cm_v>([](const auto& v) constexpr {
                  // final state recoil momentum in CM system
                  const double M_p   = std::get<M_recoil_v>(v);
                  const double M_psi = std::get<M_psi_v>(v);
                  const auto&  Q2    = std::get<Q2_v>(v);
                  const auto&  nu    = std::get<nu_v>(v);
                  double       num   = (M_p * M_p + Q2 + 2.0 * M_p * (M_psi - nu)) *
                               (M_p * M_p + Q2 - 2.0 * M_p * (M_psi + nu));
                  return std::sqrt(num / (M_p * M_p + 2.0 * M_p * nu - Q2)) / 2.0;
                })
                .add<theta_recoil_max_v>([](const auto& v) constexpr {
                  const auto&  M_p   = std::get<M_recoil_v>(v);
                  const auto&  M_psi = std::get<M_psi_v>(v);
                  const auto&  Q2    = std::get<Q2_v>(v);
                  const auto&  nu    = std::get<nu_v>(v);
                  const auto&  q     = std::get<q_v>(v);
                  const double theta_max =
                      std::acos((std::sqrt(std::pow(M_psi, 2) + std::pow(q, 2) - std::pow(nu, 2)) *
                                 std::sqrt(4 * std::pow(M_p, 2) - std::pow(M_psi, 2) -
                                           std::pow(q, 2) + 4 * M_p * nu + std::pow(nu, 2))) /
                                (2. * M_p * q));
                  return theta_max;
                })
                .add<theta_pq_v, E_recoil_v, P_recoil_v, P_recoil1_v, P_recoil2_v>([](
                    const auto& v) constexpr {
                  const auto&                   nu    = std::get<nu_v>(v);
                  const auto&                   q     = std::get<q_v>(v);
                  const auto&                   th    = std::get<theta_recoil_v>(v);
                  const auto&                   phi   = std::get<phi_recoil_v>(v);
                  const auto&                   M_p   = std::get<M_recoil_v>(v);
                  const auto&                   M_psi = std::get<M_psi_v>(v);
                  const ROOT::Math::XYZTVector& qv    = std::get<q_vec>(v);
                  const ROOT::Math::XYZTVector  p_rec_vec(std::cos(phi) * std::sin(th),
                                                         std::sin(phi) * std::sin(th), std::cos(th),
                                                         std::sqrt(1.0 + M_p * M_p));
                  const double theta_pq = ROOT::Math::VectorUtil::Angle(qv, p_rec_vec);

                  const double Eproton_part1 =
                      (M_p + nu) * (2 * std::pow(M_p, 2) - std::pow(M_psi, 2) - std::pow(q, 2) +
                                    2 * M_p * nu + std::pow(nu, 2));
                  const double Eproton_part2 =
                      q * std::abs(std::cos(theta_pq)) *
                      std::sqrt(
                          -2 * std::pow(M_p, 2) *
                              (2 * std::pow(M_psi, 2) + std::pow(q, 2) - 2 * std::pow(nu, 2)) -
                          4 * M_p * nu * (std::pow(M_psi, 2) + std::pow(q, 2) - std::pow(nu, 2)) +
                          std::pow(std::pow(M_psi, 2) + std::pow(q, 2) - std::pow(nu, 2), 2) +
                          2 * std::pow(M_p, 2) * std::pow(q, 2) * std::cos(2 * theta_pq));
                  const double Eproton_denom =
                      (2. *
                       (std::pow(M_p + nu, 2) - std::pow(q, 2) * std::pow(std::cos(theta_pq), 2)));
                  const double Eproton1 = (Eproton_part1 + Eproton_part2) / (Eproton_denom);
                  const double Eproton2 = (Eproton_part1 - Eproton_part2) / (Eproton_denom);
                  const double Pproton1 = std::sqrt(Eproton1 * Eproton1 - M_p * M_p);
                  const double Pproton2 = std::sqrt(Eproton2 * Eproton2 - M_p * M_p);
                  return std::tuple<theta_pq_v, E_recoil_v, P_recoil_v, P_recoil1_v, P_recoil2_v>(
                      theta_pq, Eproton2, Pproton2, Pproton1, Pproton2);
                })
                .add<p_recoil_vec, p_recoil1_vec, p_recoil2_vec>([](const auto& v) constexpr {
                  const auto&                  nu    = std::get<nu_v>(v);
                  const auto&                  q     = std::get<q_v>(v);
                  const auto&                  Pp    = std::get<P_recoil_v>(v);
                  const auto&                  Pp1   = std::get<P_recoil1_v>(v);
                  const auto&                  th    = std::get<theta_recoil_v>(v);
                  const auto&                  phi   = std::get<phi_recoil_v>(v);
                  const auto&                  M_p   = std::get<M_recoil_v>(v);
                  const auto&                  M_psi = std::get<M_psi_v>(v);
                  const ROOT::Math::XYZTVector p_rec_vec(
                      Pp * std::cos(phi) * std::sin(th), Pp * std::sin(phi) * std::sin(th),
                      Pp * std::cos(th), std::sqrt(Pp * Pp + M_p * M_p));
                  const ROOT::Math::XYZTVector p_rec1_vec(
                      Pp1 * std::cos(phi) * std::sin(th), Pp1 * std::sin(phi) * std::sin(th),
                      Pp1 * std::cos(th), std::sqrt(Pp1 * Pp1 + M_p * M_p));
                  return std::tuple<p_recoil_vec, p_recoil1_vec, p_recoil2_vec>(
                      p_rec_vec, p_rec1_vec, p_rec_vec);
                })
                .add<p_had_vec, p_had1_vec, p_had2_vec>([](const auto& v) constexpr {
                  const double                  M_p = std::get<M_recoil_v>(v);
                  const ROOT::Math::XYZTVector  p0(0, 0, 0, M_p);
                  const ROOT::Math::XYZTVector& kbeam  = std::get<k_beam_vec>(v);
                  const ROOT::Math::XYZTVector& kprime = std::get<k_prime_vec>(v);
                  const ROOT::Math::XYZTVector& Pp1    = std::get<p_recoil1_vec>(v);
                  const ROOT::Math::XYZTVector& Pp2    = std::get<p_recoil2_vec>(v);
                  const ROOT::Math::XYZTVector  Ppsi1  = kbeam + p0 - kprime - Pp1;
                  const ROOT::Math::XYZTVector  Ppsi2  = kbeam + p0 - kprime - Pp2;
                  return std::tuple<p_had_vec, p_had1_vec, p_had2_vec>(Ppsi2, Ppsi1, Ppsi2);
                })
                .add<s01_v, s02_v, s_v, s2_v, t_v, t1_v, t2_v>([](const auto& v) constexpr {
                  // calculate s and t (both ways)
                  const auto&                   M_p    = std::get<M_recoil_v>(v);
                  const ROOT::Math::XYZTVector& kbeam  = std::get<k_beam_vec>(v);
                  const ROOT::Math::XYZTVector& q      = std::get<q_vec>(v);
                  const ROOT::Math::XYZTVector& kprime = std::get<k_prime_vec>(v);
                  const ROOT::Math::XYZTVector& Ppsi   = std::get<p_had_vec>(v);
                  const ROOT::Math::XYZTVector& Ppsi1  = std::get<p_had1_vec>(v);
                  const ROOT::Math::XYZTVector  p0(0, 0, 0, M_p);
                  const ROOT::Math::XYZTVector& Pp = std::get<p_recoil_vec>(v);
                  return std::tuple<s01_v, s02_v, s_v, s2_v, t_v, t1_v, t2_v>(
                      (kbeam + p0).mag2(), (kprime + Ppsi + Pp).mag2(), (q + p0).mag2(),
                      (Ppsi + Pp).mag2(), (Pp - p0).mag2(), (Ppsi1 - q).mag2(), (Ppsi - q).mag2());
                })
                .add<t_min_v, t_max_v>([](const auto& v) constexpr {
                  const double M_p   = std::get<M_recoil_v>(v);
                  const double M_psi = std::get<M_psi_v>(v);
                  const double s     = std::get<s_v>(v);
                  const double p_i   = std::get<q_cm_v>(v);
                  const double p_f   = std::get<P_recoil_cm_v>(v);
                  const double t_mid = 2.0 * M_p * M_p - 2.0 * std::sqrt((M_p * M_p + p_i * p_i) *
                                                                         (M_p * M_p + p_f * p_f));
                  const double t_ang = 2.0 * p_i * p_f;
                  return std::tuple<t_min_v, t_max_v>(t_mid + t_ang, std::max(-s, t_mid - t_ang));
                })
                .add<KE_recoil_v, KE_recoil1_v, KE_recoil2_v>([](const auto& v) constexpr {
                  const double& p_rec   = std::get<P_recoil_v>(v);
                  const double& p_rec1  = std::get<P_recoil1_v>(v);
                  const double& m_rec   = std::get<M_recoil_v>(v);
                  const double  KE_rec  = std::sqrt(p_rec * p_rec + m_rec * m_rec) - m_rec;
                  const double  KE_rec1 = std::sqrt(p_rec1 * p_rec1 + m_rec * m_rec) - m_rec;
                  return std::tuple<KE_recoil_v, KE_recoil1_v, KE_recoil2_v>(KE_rec, KE_rec1,
                                                                             KE_rec);
                })
                .add<W1_v, W2_v>([](const auto& v) constexpr {
                  // static HallC hc;
                  // hc.SHMS_p0 = 2.5;
                  // static std::random_device               rd;
                  // static std::mt19937                     gen(rd());
                  // static std::uniform_real_distribution<> dist(hc.SHMS_P_min() * GeV,
                  // hc.SHMS_P_max() * GeV);
                  const auto&                   M_p = std::get<M_recoil_v>(v);
                  const ROOT::Math::XYZTVector  p0(0, 0, 0, M_p);
                  const ROOT::Math::XYZTVector& q  = std::get<q_vec>(v);
                  const ROOT::Math::XYZTVector& k1 = std::get<k_beam_vec>(v);
                  const ROOT::Math::XYZTVector& k2 = std::get<k_prime_vec>(v);
                  // auto                          k2_3    = k2.Vect();
                  // ROOT::Math::Polar3DVector k2_rand(dist(gen), k2_3.theta(), k2_3.phi());
                  // k2_rand.SetR( dist(gen) );
                  // k2.SetCoordinates(k2_rand.x(), k2_rand.y(),k2_rand.z(),k2_rand.r());
                  const ROOT::Math::XYZTVector& Prec1 = std::get<p_recoil1_vec>(v);
                  const ROOT::Math::XYZTVector& Prec2 = std::get<p_recoil2_vec>(v);
                  return std::tuple<W1_v, W2_v>((q + p0 - Prec1).mag(),
                                                ((k1 - k2) + p0 - Prec1).mag());
                })
                .add<epsilon_v>([](const auto& v) constexpr {
                  const double& E0      = std::get<E_beam_v>(v);
                  const double& Ep      = std::get<E_prime_v>(v);
                  const double& th      = std::get<theta_v>(v);
                  const double& Q2      = std::get<Q2_v>(v);
                  const double& q       = std::get<q_v>(v);
                  const double  EEsinth = 2.0 * E0 * E0 * Ep * Ep * std::sin(th) * std::sin(th);
                  return EEsinth / (EEsinth + Q2 * q * q);
                })
                .add<dsigma_dt_v, dsigma_dt1_v, dsigma_dt2_v>([](const auto& v) constexpr {
                  const double alpha_A = 0.33;
                  const double mu_A    = 0.26;
                  const double t       = std::get<t_v>(v) / GeV / GeV;
                  const double t1      = std::get<t1_v>(v) / GeV / GeV;
                  const double t2      = std::get<t2_v>(v) / GeV / GeV;
                  const double sigma =
                      4.0 * M_PI * alpha_A * alpha_A / std::pow(-1.0 * t + mu_A * mu_A, 2);
                  const double sigma1 =
                      4.0 * M_PI * alpha_A * alpha_A / std::pow(-1.0 * t1 + mu_A * mu_A, 2);
                  const double sigma2 =
                      4.0 * M_PI * alpha_A * alpha_A / std::pow(-1.0 * t2 + mu_A * mu_A, 2);
                  return std::tuple<dsigma_dt_v, dsigma_dt1_v, dsigma_dt2_v>(sigma, sigma1, sigma2);
                  // static const Lee4HeJpsiPhotoproduction xs;
                  // const auto& Mt = std::get<M_target_v>(v);
                  // const auto& W2 = std::get<s_v>(v);
                  // const auto& t  = std::get<t_v>(v);
                  // return xs(W2/GeV/GeV, t/GeV/GeV, Mt/GeV);
                })
                .add<Gamma_v>([](const auto& v) constexpr {
                  const double& E0    = std::get<E_beam_v>(v);
                  const double& Ep    = std::get<E_prime_v>(v);
                  const auto    M     = insane::masses::M_p;
                  const auto&   nu    = std::get<nu_v>(v);
                  const auto&   q     = std::get<q_v>(v);
                  const auto&   Q2    = std::get<Q2_v>(v);
                  const auto&   eps   = std::get<epsilon_v>(v);
                  const double  K     = nu - Q2 / (2. * M);
                  const auto    alpha = insane::constants::fine_structure_const;
                  const double  T1    = alpha / (2. * M_PI * M_PI);
                  const double  T2    = Ep / E0;
                  const double  T3    = K / Q2;
                  const double  T4    = 1. / (1. - eps);
                  const double  gamma = T1 * T2 * T3 * T4;
                  return gamma;
                })
                .add<dsigma_dEdOmega_dOmegaRecoil_v, dsigma_dEdOmega_dOmegaRecoil1_v,
                     dsigma_dEdOmega_dOmegaRecoil2_v>([&](const auto& v) {
                  using ReturnType =
                      std::tuple<dsigma_dEdOmega_dOmegaRecoil_v, dsigma_dEdOmega_dOmegaRecoil1_v,
                                 dsigma_dEdOmega_dOmegaRecoil2_v>;
                  const double& theta_max = std::get<theta_recoil_max_v>(v);
                  const double& th        = std::get<theta_recoil_v>(v);
                  const double& nu_thresh = std::get<nu_threshold_v>(v);
                  const double& nu        = std::get<nu_v>(v);
                  const double& t_min     = std::get<t_min_v>(v);
                  const double& t         = std::get<t_v>(v);
                  const double& dsigma    = std::get<dsigma_dt_v>(v);
                  const double& dsigma1   = std::get<dsigma_dt1_v>(v);
                  const double& dsigma2   = std::get<dsigma_dt2_v>(v);
                  const auto&   pv        = std::get<p_had_vec>(v);
                  const auto&   pv1       = std::get<p_had1_vec>(v);
                  const auto&   pv2       = std::get<p_had2_vec>(v);
                  const auto&   qv        = std::get<q_vec>(v);
                  const double& gamma     = std::get<Gamma_v>(v);
                  const double& eps       = std::get<epsilon_v>(v);
                  const double  jaco      = M_PI / (pv.get().R() * qv.get().R());
                  const double  jaco1     = M_PI / (pv1.get().R() * qv.get().R());
                  const double  jaco2     = M_PI / (pv2.get().R() * qv.get().R());
                  if (th > theta_max) {
                    return ReturnType(0.0, 0.0, 0.0);
                  }
                  if (nu < nu_thresh) {
                    return ReturnType(0.0, 0.0, 0.0);
                  }
                  if (std::isnan(t)) {
                    return ReturnType(0.0, 0.0, 0.0);
                  }
                  if (std::isnan(t_min)) {
                    return ReturnType(0.0, 0.0, 0.0);
                  }
                  if (std::isnan(dsigma)) {
                    return ReturnType(0.0, 0.0, 0.0);
                  }
                  if (std::isnan(gamma)) {
                    return ReturnType(0.0, 0.0, 0.0);
                  }
                  if (std::isnan(jaco)) {
                    return ReturnType(0.0, 0.0, 0.0);
                  }
                  const double sigma_res  = dsigma * gamma * jaco;
                  const double sigma_res1 = dsigma1 * gamma * jaco1;
                  const double sigma_res2 = dsigma2 * gamma * jaco2;
                  // std::cout << "th     "  << th << "\n";
                  // std::cout << "th_max "  << theta_max << "\n";
                  // std::cout << "dsigma   "  << dsigma << "\n";
                  // std::cout << "jaco   "  << jaco << "\n";
                  // std::cout << "gamma "  << gamma << "\n";
                  ////std::cout << "eps   "  << eps << "\n";
                  // std::cout << "sig   "  << sigma_res << "\n";
                  return ReturnType(sigma_res, sigma_res1, sigma_res2);
                });

        return var_stack;
      }
} // namespace insane::xsec::jpsi_production
#endif

