#ifndef insane_xsec_XSPhaseSpace_HH
#define insane_xsec_XSPhaseSpace_HH 1

#include "insane/base/Helpers.h"
#include <variant>

#include "TObject.h"
#include "TFoam.h"
#include "TFoamIntegrand.h"
#include "TRandom3.h"
#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/Functor.h"
#include "Math/GaussIntegrator.h"
#include "Math/IFunction.h"
#include "Math/Derivator.h"
#include <functional>
#include <numeric>
#include <utility>
#include <type_traits>

#include <Eigen/Dense>

namespace insane {

  /** Cross sections
   */
  namespace xsec {

    namespace helper = helpers;
    namespace detail = helpers::detail;

    using namespace insane::helpers;

    /**
     * \todo Add compile time checks on types.
     */
    template <typename... DiffVars>
    struct DifferentialCrossSection {};

    template <typename... X>
    struct is_DifferentialCrossSection : std::false_type {};

    template <typename... Vs>
    struct is_DifferentialCrossSection<DifferentialCrossSection<Vs...>> : std::true_type {};

    /** @brief Differential cross section using var/func stack.
     *
     * Stack should be a Definable<...> returned from <>.add()
     * Stack contains all variables. DiffVars identifies the (independent) variables that make the cross section
     * differential, eg., \f$ \frac{d\sigma}{dE\prime d\Omega} \f$ has differential
     * variables \f$ E\prime \f$, \f$ \Omega \f$
     */
    //template <typename Stack, typename SigmaType, typename... DiffVars>
    //struct DifferentialCrossSection<Stack, SigmaType, std::tuple<DiffVars...>> : public Stack {
    //  /// \todo need to check
    //  /// 1. DiffVars are contained within the Stack vriables
    //  /// 2. 

    //  using Stack_t                = Stack;
    //  using Sigma_t                = SigmaType;
    //  using DifferentialVars_t     = typename std::tuple<DiffVars...>;
    //  using IndependentVariables_t = std::decay_t<typename Stack::IndependentVariables_t>;
    //  using Vars_t                 = std::decay_t<typename Stack::Vars_t>;

    //  DifferentialCrossSection(const Stack& s) : Stack(s) {}

    //  double operator()(const IndependentVariables_t& v) const {
    //    auto res = Stack::ComputeValues(v);
    //    return std::get<Sigma_t>(res);
    //  }

    //  auto ComputeValues(const IndependentVariables_t& v) const {
    //    auto res = Stack::ComputeValues(v);
    //    return res;
    //  }
    //};

    /** Another cross section implementation.
     *  Here the XS is given explicity.
     */
    template <typename SigmaType, typename F>//, typename... DiffVars>
    struct XS_dSigma_dVars {
      using Sigma_t                = SigmaType;
      //using DifferentialVars_t     = typename std::tuple<DiffVars...>;
      //using Vars_t                 = typename Stack::Vars_t;
      F _sigma_func;
      XS_dSigma_dVars(F&& f) : _sigma_func(std::forward<F>(f)) {}

      template <typename Stack, typename IndependentVariables_t = typename Stack::IndependentVariables_t>
      double operator()(const Stack& stack, const IndependentVariables_t& v) const {
        // kinematic stack result
        //std::cout << "\n";
        //insane::helpers::print_tuple(v);
        //std::cout << "\n";
        auto kine_res = stack.ComputeValues(v);
        // compute the generic lambda which expects certain kinematic variables:
        auto res = _sigma_func(kine_res);
        return res;
      }

      template <typename Stack, typename FixedVars_t, typename DiffVars_t>
      double operator()(const Stack& stack, const FixedVars_t& v1, const DiffVars_t& v2) const {
        using IndependentVariables_t = std::decay_t<typename Stack::IndependentVariables_t>;
        //static_assert(insane::helpers::contains_tuple_types_v<IndependentVariables_t, FixedVars_t> == true);
        //static_assert(insane::helpers::contains_tuple_types_v<IndependentVariables_t, DiffVars_t> == true);
        IndependentVariables_t v;
        assign_tuple_by_types(v,v1);
        assign_tuple_by_types(v,v2);
        //std::cout << "\n";
        //insane::helpers::print_tuple(v);
        //std::cout << "\n";
        //insane::helpers::print_tuple(v1);
        //std::cout << "\n";
        //insane::helpers::print_tuple(v2);
        //std::cout << "\n";
        // kinematic stack result
        auto kine_res = stack.ComputeValues(v);
        // compute the generic lambda which expects certain kinematic variables:
        auto res = _sigma_func(kine_res);
        return res;
      }

      //, typename = typename std::enable_if_v<helpers::is_tuple<KineResult>::value>
      template <typename KineResult>
      double operator()(const KineResult& v) const {
        // compute the generic lambda which expects certain kinematic variables:
        auto res = _sigma_func(v);
        return res;
      }
    };
    template <typename SigmaType, typename F>
    constexpr auto make_dsigma(F&& f) {
      return XS_dSigma_dVars<SigmaType, decltype(f)>(std::forward<F>(f));
    }

    //template <typename SigmaType, typename Tup, typename Stack >
    //constexpr auto make_DifferentialCrossSection(const Stack& s) {
    //  return DifferentialCrossSection<Stack, SigmaType, Tup>(s);
    //}

    /** Jacobian Determanent
     *
     *
     */
    namespace jacobian {

      namespace helper = helpers;
      namespace detail = helpers::detail;
      namespace notstd = helpers::notstd;

      /** @name derivativematrix Derivative matrix.
       * Implementation of the derivative matrix.
       * @{
       */

      /** Function Derivative.
       *  Computes \f$ \frac{d\mathrm{FVar}}/{d\mathrm{Var}} \f$.
       *  Enabled if the Stack::IndependentVariables_t contains Var.
       *
       * @tparam FVar function  to be differentiated
       * @tparam Var derivative varioable.
       *
       * Note: This class uses ROOT::Math::Derivator in the derivative implementation.
       *
       */
      template <typename FVar, typename Var, typename Stack>
      struct FunctionDerivative : public Stack {
        using Stack_t                = std::decay_t<Stack>;
        using Func_t                 = FVar;
        using DerivativeVar_t        = Var;
        using IndependentVariables_t = std::decay_t<typename Stack::IndependentVariables_t>;
        using Vars_t                 = std::decay_t<typename Stack::Vars_t>;

        ///\todo Add compile-time check that FVar and Var are contained Vars_t.
        // static_assert(
        //    std::conditional<((helpers::has_type<Func_t, IndependentVariables_t>::value) &&
        //                      (helpers::has_type<DerivativeVar_t,
        //                      IndependentVariables_t>::value)),
        //                     typename std::is_same<Func_t, DerivativeVar_t>::type,
        //                     std::false_type>::type::value,
        //    "Partial derivative between 2 independent variables. Need a "
        //    "computed/dependent variable.");
        static_assert(((helpers::has_type<Func_t, IndependentVariables_t>::value) ||
                       (helpers::has_type<DerivativeVar_t, IndependentVariables_t>::value)),
                      "Partial derivative does not involve any independent variables");

        FunctionDerivative(const Stack& s) : Stack(s) {}
        FunctionDerivative(Stack&& s) : Stack(std::forward<Stack>(s)) {}

        double operator()(const IndependentVariables_t& args) {
          if constexpr (std::is_same<FVar, Var>::value) {
            return 1.0;
          } else if constexpr (helpers::has_type<DerivativeVar_t, IndependentVariables_t>::value) {
            auto x0 = std::get<Var>(args);
            auto fx = [=, this](double xx) {
              auto arg_copy = args;
              // substitute the value into the arg tuple
              std::get<Var>(arg_copy) = Var(xx);
              auto res                = this->Stack::ComputeValues(arg_copy);
              return std::get<FVar>(res).get();
            };
            ROOT::Math::Functor1D wf(fx);
            ROOT::Math::Derivator der;
            der.SetFunction(wf);
            return der.Eval(x0);
          } else if constexpr (helpers::has_type<Func_t, IndependentVariables_t>::value) {
            auto x0 = std::get<FVar>(args);
            auto fx = [=, this](double xx) {
              auto arg_copy = args;
              // substitute the value into the arg tuple
              std::get<FVar>(arg_copy) = FVar(xx);
              auto res                 = this->Stack::ComputeValues(arg_copy);
              return std::get<Var>(res).get();
            };
            ROOT::Math::Functor1D wf(fx);
            ROOT::Math::Derivator der;
            der.SetFunction(wf);
            double r = der.Eval(x0);
            if (r == 0.0)
              return 0.0;
            return 1.0 / r;
          } else {
            return 0.0;
          }
        }
      };

      template <typename FVar, typename Var, typename Stack>
      constexpr auto make_function_derivative(const Stack& s) {
        return FunctionDerivative<FVar, Var, Stack>(s);
      }

      /** Calculate each derivative of F.
       *  Again using pack expansion to populate the make_tuple.
       */
      template <typename FVar, typename Vars, typename Stack, size_t... Nx>
      constexpr auto derivative_vector_impl(const Stack& s, std::index_sequence<Nx...>) {
        return std::make_tuple(
            FunctionDerivative<FVar, typename std::tuple_element<Nx, Vars>::type, Stack>(s)...);
      }

      /** Returns std::tuple<dF/dx_1,  dF/dx_2, ...>.
       * This returns a sequence of derivates.
       * Each derivative is done with respecto to the Nth  argument of the array vector
       * (which are doubles)
       * Note here we have hard coded the argumetns of all the functions, but
       * this could be passed from the template instead of just the size.
       * ( array<double,N>)
       */
      template <typename FVar, typename Vars, typename Stack>
      constexpr auto derivative_vector(const Stack& s) {
        return derivative_vector_impl<FVar, Vars>(
            s, std::make_index_sequence<std::tuple_size<Vars>::value>{});
      }

      /** Imp helper for derivatives.
       *  Uses pack expansion to get the paramerter pack for make_tuple.
       *  This is just a tuple of tuples.
       */
      template <typename T1, typename T2, typename Stack, size_t... IFunc>
      constexpr auto make_derivative_matrix_impl(const Stack& s, std::index_sequence<IFunc...>) {
        return std::make_tuple(
            derivative_vector<typename std::tuple_element<IFunc, T1>::type, T2, Stack>(s)...);
      }

      /** Derivative matrix = std::tuple<std::tuple<...> >.
       *  Returns (not real code)
       *  {{ dT1[1]/dT2[1], dT1[1]/dT2[2], ...}
       *   { dT1[2]/dT2[1], dT1[2]/dT2[2], ...}
       *   {     :        ,  :           ,  : }
       *   { dT1[N]/dT2[1], ...          , dT1[N]/dT2[N]}
       *
       */
      template <typename T1, typename T2, typename Stack>
      constexpr auto make_derivative_matrix(const Stack& s) {
        return make_derivative_matrix_impl<T1, T2, Stack>(
            s, std::make_index_sequence<std::tuple_size<T1>::value>{});
      }

      //@}


      template<typename... R, typename ...Args>
      constexpr std::tuple<R...> sub_tuple(const std::tuple<Args...>& original) {
        return std::make_tuple(std::get<R>(original)...);
      }
      template<typename... Vs> struct SubTuple{};

      template<typename... Vs>
      struct SubTuple<std::tuple<Vs...>> : public std::tuple<Vs...>  {
        template<typename ...Args>
        SubTuple(const std::tuple<Args...>& arg_tup) : std::tuple<Vs...>(sub_tuple<Vs...>(arg_tup)){}
        std::tuple<Vs...> operator()(){return *this;}
      };

      /** Jacobian Matrix Determminant.
       *
       * Constructed with two differentials and lambdas which fix the order of
       * the new variables. These functions compute the old variables and are
       * functions of the new variables.
       *
       * \f$ J_{ij} = \partial f_{i}/\partial x_{j} \f$
       * \f$ |J| = (\frac{\partial(f_1, ..., f_n)}{\partial(x_1, ..., x_m)}) \f$
       *
       * \code
       *      | (0  ,0), (0,1),   ... (0  ,N-1) |
       *   J= | ...               ... ...       |
       *      | (N-1,0), (N-1,1), ... (N-1,N-1) |
       * \endcode
       */
      template <typename D1, typename D2, typename Stack>
      class Jacobian {
      public:
        static_assert(helpers::is_tuple<D1>::value, "D1 is not a tuple");
        static_assert(helpers::is_tuple<D2>::value, "D2 is not a tuple");
        static constexpr std::size_t N1 = std::tuple_size<D1>::value;
        static constexpr std::size_t N2 = std::tuple_size<D2>::value;
        static_assert(N1 == N2, "is not a square matrix");

        using Matrix_t    = Eigen::Matrix<double, N1, N1>;
        using DerMatrix_t = std::decay_t<decltype(make_derivative_matrix<D1, D2, Stack>(std::declval<Stack>()))>;
        using IndependentVariables_t = std::decay_t<typename Stack::IndependentVariables_t>;
        using Vars_t                 = std::decay_t<typename Stack::Vars_t>;

        // I am not sure why mutable is needed here...
        mutable DerMatrix_t _dm;

      public:
        Jacobian(const Jacobian&) = default;
        Jacobian(const Stack& s) : _dm(make_derivative_matrix<D1, D2>(s)) {}

        /** Det helpers.
         * Getting the functions from nested std::tuples
         */
        template <size_t I, size_t J>
        constexpr auto Det_impl3(Matrix_t& mat, const IndependentVariables_t& v) const {
          //std::cout << I << J << " " << std::get<I>(std::get<J>(_dm))(v) << "\n";
          return mat(I, J) = std::get<I>(std::get<J>(_dm))(v);
        }

        template <size_t I, size_t... J>
        constexpr auto Det_impl2(Matrix_t& mat, const IndependentVariables_t& v,
                                 std::index_sequence<J...>) const {
          return std::make_tuple(Det_impl3<I, J>(mat, v)...);
        }

        template <size_t... I>
        constexpr auto Det_impl1(Matrix_t& mat, const IndependentVariables_t& v,
                                 std::index_sequence<I...>) const {
          return std::make_tuple(Det_impl2<I>(mat, v, std::make_index_sequence<N1>{})...);
        }
        /** Compute the determinant.
         *
         * \code
         *      | (0  ,0), (0,1),   ... (0  ,N-1) |
         *   J= | ...               ... ...       |
         *      | (N-1,0), (N-1,1), ... (N-1,N-1) |
         * \endcode
         */
        //double Det(const IndependentVariables_t& v) {
        //  Matrix_t mat;
        //  Det_impl1(mat, v, std::make_index_sequence<N1>{});
        //  return mat.determinant();
        //}
        double Det(const IndependentVariables_t& v) const {
          Matrix_t mat;
          Det_impl1(mat, v, std::make_index_sequence<N1>{});
          return mat.determinant();
        }

        template<typename T>
        double Det(const T& v) const {
          return this->Det(SubTuple<IndependentVariables_t>(v)());
        }
      };

      /** Transform Differential XS with jacaobian.
       *
       */
      template <typename XS, typename NewSigmaType, typename NewVars,
                typename = typename std::enable_if<is_DifferentialCrossSection<XS>::value>::type>
      struct TransformedDifferentialCrossSection : public XS {

        using Stack_t    = std::decay_t<typename XS::Stack_t>;
        using Sigma_t    = NewSigmaType;
        using OldSigma_t = std::decay_t<typename XS::Sigma_t>;

        using DifferentialVars_t    = NewVars;
        using OldDifferentialVars_t = std::decay_t<typename XS::DifferentialVars_t>;

        using IndependentVariables_t = std::decay_t<typename Stack_t::IndependentVariables_t>;
        using Vars_t                 = std::decay_t<typename Stack_t::Vars_t>;

        using XS_t       = XS;
        using Jacobian_t = Jacobian<NewVars, std::decay_t<typename XS::DifferentialVars_t>, std::decay_t<typename XS::Stack_t>>;

        Jacobian_t _jm;

        TransformedDifferentialCrossSection(const XS& xs) : XS(xs), _jm(xs) {}

        double operator()(const IndependentVariables_t& v) const {
          auto        res  = Stack_t::ComputeValues(v);
          const auto& sig0 = std::get<OldSigma_t>(res);
          double      jaco = std::abs(_jm.Det(v));
          //std::cout << "jaco " <<  jaco << "\n";
          //std::cout << "sig0 " <<  sig0 << "\n";
          return jaco*sig0;
        }
        auto ComputeValues(const IndependentVariables_t& v) const {
          return Stack_t::ComputeValues(v);
        }
      };

      template <typename XS, typename NewXS, typename NewVars,
                typename = typename std::enable_if<is_DifferentialCrossSection<XS>::value>::type>
      constexpr auto make_transformed_xs(const XS& xs) {
        using NewXS_t    = NewXS;
        using OldXS_t    = std::decay_t<typename XS::Sigma_t>;
        using XS_t       = XS;
        using NewVars_t  = NewVars;
        using OldVars_t  = std::decay_t<typename XS::IndependentVariables_t>;
        using Stack_t    = std::decay_t<typename XS::Stack_t>;
        using Jacobian_t = Jacobian<NewVars_t, OldVars_t, Stack_t>;
        auto newstack         = xs.template add<NewXS_t>([=](const auto& vs) {
          static const Jacobian_t jm(xs);
          const auto& xs0 = std::get<OldXS_t>(vs);
          return jm.Det(vs)*xs0;
        });
        return make_DifferentialCrossSection<decltype(newstack), NewXS_t,NewVars_t>(newstack);
      }

      //@}
    } // namespace jacobian

    /** \name Defining a phase space.
     * @{
     */

    /** Variable Limits.
     * Used to define a phase space of variables.
     */
    template <class T, bool UniformVar = false>
    struct VarLimits {
      using VarType                  = T;
      std::array<double, 2> _limits  = {0.0, 1.0};
      static constexpr bool fUniform = UniformVar;

      constexpr double NormalizedScale() const { return (_limits.at(1) - _limits.at(0)); }
      constexpr double NormalizedOffset() const { return (_limits.at(0)); }
      constexpr double NormalizedVar(double x) const {
        return (x - NormalizedOffset()) / (NormalizedScale());
      }
      /// Compute the variable from the normalized coordinate [0,1]
      constexpr double Var(double y) const { return (y * NormalizedScale() + NormalizedOffset()); }
      constexpr double Min() const { return _limits.at(0); }
      constexpr double Max() const { return _limits.at(1); }
      bool             IsUniform() const { return fUniform; }
      // void             SetUniform(bool v) { fUniform = v; }
      constexpr bool IsContained(double v) const {
        return ((v >= _limits.at(0)) && (v <= _limits.at(1))) ? true : false;
      }
    };

    /** N-Fold Differential.
     *
     *  (dx_1 dx_2 ... dx_N)
     *  Constructor takes \b only \b independent \b variables.
     *  These variables fix the size, N (or N_I), of the std::array<double,N>
     *  passed to subsequent functions, such as, dependent variables, cross
     *  sections, acceptances, and Jacobians. using the alias VarArray.
     *
     */
    template <class... Vs>
    class PhaseSpace {
    public:
      static constexpr std::size_t N = sizeof...(Vs);
      using VarArray_t               = std::array<double, N>;
      using VarTypes_t               = std::tuple<std::decay_t<typename Vs::VarType>...>;
      using Limits_t                 = std::tuple<Vs...>;

      Limits_t fIndVars;

      ///\todo  check that  Vs are VarLimits
      ///\todo use cpptaskflow  instead of dispatch to multithread initializations.
    public:
      PhaseSpace(Vs... vs) : fIndVars(std::make_tuple(vs...)) {}

      /** Test whether the independent variables are within their limits.
       *
       *  Returns array of type std::array<bool,N>
       */
      constexpr auto IndVarsInPhaseSpace(const VarTypes_t& v) const {
        auto dispatcher = helper::make_index_dispatcher<bool, N>();
        return dispatcher(
            [&](auto idx) { return std::get<idx>(fIndVars).IsContained(std::get<idx>(v)); });
      }
      constexpr auto GetIndVarsMin() const {
        auto dispatcher = helper::make_index_dispatcher<double, N>();
        return dispatcher([&](auto idx) { return std::get<idx>(fIndVars).Min(); });
      }
      constexpr auto GetIndVarsMax() const {
        auto dispatcher = helper::make_index_dispatcher<double, N>();
        return dispatcher([&](auto idx) { return std::get<idx>(fIndVars).Max(); });
      }
      /** Get array indicating which variables are unformily distributied.
       * Returns std::array<bool,N> where the corresponding
       * uniform variables are set to true. See PSVBase::IsUniform().
       */
      constexpr auto IndVarsUniform() const {
        auto dispatcher = helper::make_index_dispatcher<bool, N>();
        return dispatcher([&](auto idx) { return std::get<idx>(fIndVars).IsUniform(); });
      }
      /** Compute rescaled independent variables.
       *
       * The argument are values of the \b normalized \b variables which span
       * [0,1]. This function returns the rescaled (physical) values which are
       * defined by the PSV's min and max.
       *
       * Returns array of type std::array<double,N>.
       */
      constexpr auto RescaledVars(const VarTypes_t& v) const {
        auto dispatcher = helper::make_index_dispatcher<double, N>();
        return dispatcher([&](auto idx) { return std::get<idx>(fIndVars).Var(std::get<idx>(v)); });
      }
      /** Compute scaled-variable jacobian.
       *
       *   \f[ \frac{\partial(y_1,...,y_I)}{\partial(x_1,...,x_{N})} =
       *       \sum_{i=0}^{N} (x_{i}^{\mathrm{max}}-x_{i}^{\mathrm{min}}) \f]
       *
       *   where y are normalized to the range [0,1] and x are the regular
       *   independent variables.
       *   This is just a diagonal matrix with (max-min) on the diagonal,
       *   since the scaled variables are y = x*(max-min).
       *
       */
      constexpr auto ScaledVarJacobian() const {
        auto dispatcher = helper::make_index_dispatcher<double, N>();
        auto res = dispatcher([&](auto idx) { return std::get<idx>(fIndVars).NormalizedScale(); });
        return std::accumulate(res.begin(), res.end(), 1.0, std::multiplies<double>());
      }
      /** Print for debugging.
       */
      void Print(Option_t* opt = "") const {
        std::cout << "NDiff :  (N = " << N << ")\n";
        auto dispatcher = helper::make_index_dispatcher<int, N>();
        dispatcher([&](auto idx) {
          std::get<idx>(fIndVars).Print();
          return 0;
        });
      }
    };

    //@}

    /** Integrated Cross Section.
     *
     *  Stores the total cross section (computed from a PSSampler or
     *  some other numerical integration).
     *  Passed to the phase space sampler as the function to sample.
     */
    template <class XS, class PS>
    class IntegratedCrossSection : public TFoamIntegrand, public ROOT::Math::IBaseFunctionMultiDim {
    public:
      static constexpr std::size_t N = PS::N;
      using XS_t                     = XS;
      using PS_t                     = PS;
      using VarArray                 = std::array<double, N>;
      using VarTypes_t               = std::decay_t<typename PS_t::VarTypes_t>;
      //using DiffVars_t               = std::decay_t<typename XS_t::DifferentialVars_t>; // not used?
      using IndependentVariables_t   = std::decay_t<typename XS_t::IndependentVariables_t>;
      using Vars_t                   = std::decay_t<typename XS_t::Vars_t>;

      XS_t   fXS;
      PS_t   fPS;
      double fTotalXS;

    public:
      IntegratedCrossSection(const XS_t& xs, const PS_t& ps, double tot = 0.0)
          : fXS(xs), fPS(ps), fTotalXS(tot) { }
      IntegratedCrossSection(XS_t&& xs, PS_t&& ps, double tot = 0.0)
          : fXS(std::forward<XS_t>(xs)), fPS(std::forward<PS_t>(ps)), fTotalXS(tot) {}
      const PS_t& ConstPhaseSpace() const { return fPS; }
      auto&       PhaseSpace() { return fPS; }

      /** Compute scaled variable jacobian.
       *
       *   \f[ \frac{\partial(y_1,...,y_I)}{\partial(x_1,...,x_{N})} =
       *       \sum_{i=0}^{N} (x_{i}^{\text{max}}-x_{i}^{\text{min}}) \f]
       *   where y are normalized to the range [0,1] and x are the regular
       *   independent variables.
       *
       */
      constexpr auto ScaledVarJacobian() const { return fPS.ScaledVarJacobian(); }

      /** Compute re-scaled independent variables.
       *
       * The argument is an array of \b normalized \b variables which each span [0,1].
       * This function returns the rescaled (physical) values which are defined by the
       * PSV's min and max.
       *
       * Returns array of type std::array<double,N_I>.
       */
      constexpr auto RescaledVars(const VarTypes_t& v) const { return fPS.RescaledVars(v); }
      // constexpr auto RescaledVars(const VarArray& v) const { return fPS.RescaledVars(v); }

      /** Density function needed by FOAM.
       *  The x arguments are the \b normalized \b variables which span [0,1].
       *  They are converted into regular variables before evaluating the cross section.
       */
      virtual Double_t Density(Int_t ndim, Double_t* x) {
        const auto x_arr = reinterpret_cast<std::array<double, N>*>(x);
        VarTypes_t args  = to_tuple<VarTypes_t>(*x_arr);
        return fXS(to_tuple<VarTypes_t>(RescaledVars(args)));
      }

      auto ComputeValues(const IndependentVariables_t& v) const { return fXS.ComputeValues(v); }

      void   SetTotalXS(double xs) { fTotalXS = xs; }
      double TotalXS() const { return fTotalXS; }

      virtual IntegratedCrossSection* Clone(const char* newname) const {
        auto* copy = new IntegratedCrossSection<XS_t, PS_t>(fXS, fPS, fTotalXS);
        //(*copy) = (*this);
        return copy;
      }
      virtual IntegratedCrossSection* Clone() const { return (Clone("")); }
      virtual unsigned int            NDim() const { return N; }

      void Print(Option_t* opt = "") const {
        std::cout << "IntegratedCrossSection : \n";
        // fXS.Print();
        std::cout << " total XS : " << fTotalXS << std::endl;
      }

      double CalculateTotalXS() {
        ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS, 1.0e-8);
        ig2.SetFunction(*this);
        auto a = fPS.GetIndVarsMin();
        auto b = fPS.GetIndVarsMax();
        // auto bval = std::begin(b);
        // for(auto val : a) {
        //  std::cout << "(" << val << ", " << *bval << ")\n";
        //  bval++;
        //}

        fTotalXS = ig2.Integral(&a[0], &b[0]);
        return fTotalXS;
      }

    private:
      virtual double DoEval(const double* x) const {
        const auto x_arr = reinterpret_cast<const std::array<double, N>*>(x);
        return fXS(to_tuple<VarTypes_t>(*x_arr));
      }
    };

    /** CrossSectionIntegrand.
     *
     *  Used with TotalCrossSection below.
     */
    template <typename TotXS, typename Stack>
    class CrossSectionIntegrand : public TFoamIntegrand, public ROOT::Math::IBaseFunctionMultiDim {
    public:
      static constexpr std::size_t N = TotXS::PS_t::N;
      using VarTypes_t               = std::decay_t<typename TotXS::PS_t::VarTypes_t>;
      using IndependentVariables_t   = std::decay_t<typename Stack::IndependentVariables_t>;
      using FixedVars_t              = std::decay_t<typename TotXS::FixedVars_t>;

      const TotXS& total_xs;
      const Stack& _kine_var;

      CrossSectionIntegrand(const TotXS& x, const Stack& s) : total_xs(x), _kine_var(s) {}

      virtual CrossSectionIntegrand* Clone(const char* newname) const {
        auto* copy = new CrossSectionIntegrand<TotXS, Stack>(total_xs, _kine_var);
        return copy;
      }
      virtual CrossSectionIntegrand* Clone() const { return (Clone("")); }
      virtual unsigned int           NDim() const { return N; }

      /** Density function needed by FOAM.
       *  The x arguments are the \b normalized \b variables which span [0,1].
       *  They are converted into regular variables before evaluating the cross section.
       */
      virtual Double_t Density(Int_t ndim, Double_t* x) {
        const auto x_arr = reinterpret_cast<std::array<double, N>*>(x);
        VarTypes_t args  = to_tuple<VarTypes_t>(*x_arr);
        return total_xs.fXS(_kine_var, total_xs.FixedVars(), to_tuple<VarTypes_t>(total_xs.RescaledVars(args)));
      }

    private:
      virtual double DoEval(const double* x) const {
        const auto x_arr = reinterpret_cast<const std::array<double, N>*>(x);
        return total_xs.fXS(_kine_var, total_xs.FixedVars(), to_tuple<VarTypes_t>(*x_arr));
      }
    };

    /** Total Cross Section.
     *
     *  Computes integral over the entire phase space.
     */
    template <class XS, class PS, typename FV = std::tuple<> >
    class TotalCrossSection {
    public:
      static constexpr std::size_t N = PS::N;
      using XS_t                     = XS;
      using PS_t                     = PS;
      using Sigma_t                  = std::decay_t<typename XS_t::Sigma_t>;
      using VarTypes_t               = std::decay_t<typename PS_t::VarTypes_t>; ///< differential variables
      using FixedVars_t              = FV; ///< independent but not differential (e.g. beam energy)

      const XS_t&        fXS;
      const PS_t&        fPS;
      const FixedVars_t  fFV;
      double             fTotalXS;

    public:
      TotalCrossSection(const XS_t& xs, const PS_t& ps, const FixedVars_t& fv, double tot = 0.0)
          : fXS(xs), fPS(ps), fFV(fv), fTotalXS(tot) {}

      constexpr auto ScaledVarJacobian() const { return fPS.ScaledVarJacobian(); }
      constexpr auto RescaledVars(const VarTypes_t& v) const { return fPS.RescaledVars(v); }
      void           SetTotalXS(double xs) { fTotalXS = xs; }
      double         TotalXS() const { return fTotalXS; }

      FixedVars_t    FixedVars() const { return fFV; }

      template <typename Stack>
      double CalculateTotalXS(const Stack& s) {
        ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS, 1.0e-8);
        CrossSectionIntegrand<TotalCrossSection<XS, PS, FV>, Stack> integrand(*this, s);
        ig2.SetFunction(integrand);
        auto a   = fPS.GetIndVarsMin();
        auto b   = fPS.GetIndVarsMax();
        fTotalXS = ig2.Integral(&a[0], &b[0]);
        return fTotalXS;
      }
    };

    template <class XS, class PS, class FV>
    constexpr auto make_total_xs(const XS& xs, const PS& ps, const FV& fv) {
      return TotalCrossSection<XS, PS,FV>(xs, ps,fv);
    }
    template <class XS, class PS>
    constexpr auto make_total_xs(const XS& xs, const PS& ps) {
      return TotalCrossSection<XS, PS,std::tuple<>>(xs, ps,{});
    }

    ///** ReducedCrossSectionIntegrand.
    // *  Used with ReducedCrossSection.
    // */
    //template <typename RedXS, typename Stack, typename Unintegrated = void>
    //class ReducedCrossSectionIntegrand : public TFoamIntegrand,
    //                                     public ROOT::Math::IBaseFunctionMultiDim {
    //public:
    //  static constexpr std::size_t N = RedXS::PS_t::N;
    //  using VarTypes_t               = std::decay_t<typename RedXS::PS_t::VarTypes_t>;
    //  using IndependentVariables_t   = typename Stack::IndependentVariables_t;

    //  template <typename T>
    //  using is_integrated_var =
    //      std::negation<typename insane::helpers::has_type<T, Unintegrated>::type>;

    //  template <typename... T>
    //  using integrated_var_filter =
    //      typename insane::helpers::Filter<is_integrated_var, std::tuple<T...>>::type;

    //  using IntegratedVars_t = typename integrated_var_filter<IndependentVariables_t>::type;

    //  const RedXS& _tot_xs;
    //  const Stack& _kine_var;

    //  ReducedCrossSectionIntegrand(const RedXS& x, const Stack& s) : _tot_xs(x), _kine_var(s) {}

    //  virtual ReducedCrossSectionIntegrand* Clone(const char* newname) const {
    //    auto* copy = new ReducedCrossSectionIntegrand<RedXS, Stack>(_tot_xs, _kine_var);
    //    return copy;
    //  }
    //  virtual ReducedCrossSectionIntegrand* Clone() const { return (Clone("")); }
    //  virtual unsigned int                  NDim() const { return N; }

    //  /** Density function needed by FOAM.
    //   *  The x arguments are the \b normalized \b variables which span [0,1].
    //   *  They are converted into regular variables before evaluating the cross section.
    //   */
    //  virtual Double_t Density(Int_t ndim, Double_t* x) {
    //    const auto x_arr = reinterpret_cast<std::array<double, N>*>(x);
    //    VarTypes_t args  = to_tuple<VarTypes_t>(*x_arr);
    //    return _tot_xs.fXS(_kine_var, to_tuple<VarTypes_t>(_tot_xs.RescaledVars(args)));
    //  }

    //private:
    //  virtual double DoEval(const double* x) const {
    //    const auto x_arr = reinterpret_cast<const std::array<double, N>*>(x);
    //    return _tot_xs.fXS(_kine_var, to_tuple<VarTypes_t>(*x_arr));
    //  }
    //};

    ///** Integrated Cross Section.
    // *  Computes integral over **part** of the phase space.
    // */
    //template <class XS, class PS>
    //class ReducedCrossSection {
    //public:
    //  static constexpr std::size_t N = PS::N;
    //  using XS_t                     = XS;
    //  using PS_t                     = PS;
    //  using Sigma_t                  = std::decay_t<typename XS_t::Sigma_t>;
    //  using VarTypes_t               = std::decay_t<typename PS_t::VarTypes_t>;

    //  const XS_t& fXS;
    //  const PS_t& fPS;
    //  double      fTotalXS;

    //public:
    //  ReducedCrossSection(const XS_t& xs, const PS_t& ps, double tot = 0.0)
    //      : fXS(xs), fPS(ps), fTotalXS(tot) {}

    //  constexpr auto ScaledVarJacobian() const { return fPS.ScaledVarJacobian(); }
    //  constexpr auto RescaledVars(const VarTypes_t& v) const { return fPS.RescaledVars(v); }
    //  void           SetTotalXS(double xs) { fTotalXS = xs; }
    //  double         TotalXS() const { return fTotalXS; }

    //  /** Calculate the reduced cross section by integrating over the partical phase space.
    //   *
    //   * \tparam UnintegratedVars is tuple of the variables not integrated..
    //   */
    //  template <typename Stack, typename UnintegratedVars>
    //  double CalculateTotalXS(const Stack& s, const UnintegratedVars& u) {
    //    ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS, 1.0e-8);
    //    ReducedCrossSectionIntegrand<ReducedCrossSection<XS, PS>, Stack> integrand(*this, s);
    //    ig2.SetFunction(integrand);
    //    auto a   = fPS.GetIndVarsMin();
    //    auto b   = fPS.GetIndVarsMax();
    //    fTotalXS = ig2.Integral(&a[0], &b[0]);
    //    return fTotalXS;
    //  }
    //};

    /** \name Phase space generator
     *
     * @{
     */

    /** FinalStateParticle.
     */
    struct FinalStateParticle {
      ROOT::Math::XYZTVector momentum;
      ROOT::Math::XYZTVector vertex;
      int64_t                pid;
    };

    /** Abstract base class for samplers.
     *  Useful for putting samplers into containers.
     */
    class SamplerBase : public TObject {
      using FSParticleVec_t = std::vector<FinalStateParticle>;

    public:
      SamplerBase() {}
      virtual ~SamplerBase() {}
      /** Generates normalized variables. These have values in the range [0,1].
       */
      virtual std::vector<double> GenerateEventNorm() const = 0;
      virtual std::vector<double> GenerateEvent() const     = 0;
      // virtual FSParticleVec_t     MakeFinalState(const std::vector<double>& vars) const = 0;
    };

    /** TFoam options.
     */
    struct FoamOptions {
      int    fFoamCells    = 2000; // default(1000) No of allocated number of cells,
      int    fFoamSample   = 1000; // default(200)  No. of MC events in the cell MC exploration
      int    fFoamBins     = 40; // default(8)    No. of bins in edge-histogram in cell exploration
      int    fFoamOptRej   = 1;  // Switch =0 for weighted events; =1 for unweighted events in MC
      int    fFoamOptDrive = 1;  // (D=2) Option, type of Drive =0,1,2 for TrueVol,Sigma,WtMax
      int    fFoamEvPerBin = 50; // maximum no. of EFFECTIVE event per bin, =0 option is inactive
      double fFoamMaxWtRej = 1.05; // Maximum weight in rejection for getting wt=1 events
      int    fFoamChat     = 0;    // 0,1,2
    };

    /** Phase Space sampler.
     *  A phase space sampler is associated with generating random values in the phase space.
     *  A "generator" is when the sampled variables are translated into output events.
     */
    template <class TotXS>
    class PhaseSpaceSampler : public SamplerBase {
    public:
      static constexpr std::size_t N = TotXS::N; // N indepedent variables
      using XS_t                     = std::decay_t<typename TotXS::XS_t>;
      using PS_t                     = std::decay_t<typename TotXS::PS_t>;
      using VarTypes_t               = std::decay_t<typename TotXS::VarTypes_t>; /// PS variable types
      using FixedVars_t              = std::decay_t<typename TotXS::FixedVars_t>; ///< independent but not differential (e.g. beam energy)

      static_assert(std::is_same<VarTypes_t,typename TotXS::PS_t::VarTypes_t>::value == true, " Var types differ!");

      FoamOptions fOptions;
      TotXS&      total_xs;

    protected:
      std::shared_ptr<TFoam> fFoam;
      double                 fWeight{1.0}; // To accomodate different relative luminosities
                           // (from different material thicknesses), per nucleon cross sections, etc
    public:
      PhaseSpaceSampler(TotXS& xs)
          : total_xs(xs), fFoam(std::make_shared<TFoam>("FoamX")), fWeight(1.0) {}

      const auto& ConstIntegratedXS() const { return total_xs; }
      auto&       IntegratedXS() { return total_xs; }

      /** Init sampler.
       *   - Create the CrossSectionIntegrand for FOAM
       *   - Setup FOAM options
       *   - create rng seed from time
       *   - create rng TRandom3
       *   - Set foam integrand and rng
       *   - Set the uniform variables in FOAM (inhibit cell division)
       *   - Init FOAM
       *   - Compute total XS from FOAM integral
       */
      template <typename Stack>
      double Init(const Stack& s) {
        std::cout << " PS samp Init \n";
        using IntegrandType = CrossSectionIntegrand<TotXS, Stack>;
        auto* integrand     = new IntegrandType(total_xs, s);
        // fFoam = std::make_unique("FoamX");   // Create Simulators
        fFoam->SetkDim(N); // No. of dimensions, obligatory!
        fFoam->SetnCells(fOptions.fFoamCells);
        fFoam->SetnSampl(fOptions.fFoamSample);     // optional
        fFoam->SetChat(fOptions.fFoamChat);         // optional
        fFoam->SetnBin(fOptions.fFoamBins);         // optional
        fFoam->SetOptRej(fOptions.fFoamOptRej);     // optional
        fFoam->SetOptDrive(fOptions.fFoamOptDrive); // optional
        fFoam->SetMaxWtRej(fOptions.fFoamMaxWtRej); // optional
        fFoam->SetEvPerBin(fOptions.fFoamEvPerBin); // optional
        fFoam->SetRho(integrand);                   // Set n-dim distribution,
        TRandom* PseRan = new TRandom3(time(NULL)); // Create random number generator
        fFoam->SetPseRan(PseRan);                   // Set random number generator
        // Set the variables that are uniform
        int ivar = 0;
        for (const auto& is_uni : total_xs.fPS.IndVarsUniform()) {
          if (is_uni) {
            fFoam->SetInhiDiv(ivar, 1);
          }
          ivar++;
        }
        fFoam->Initialize(); // Initialize simulator, takes a few seconds...
        //// Note here we are using the weight in calculating the total cross section
        //// This could be passed in as luminosity which means that totalXSection is
        //// actually a rate.
        // fTotalXSection =
        // fWeight*fFoam->GetPrimary()*fDiffXSec->GetPhaseSpace()->GetScaledIntegralJacobian();
        double foam_integral = fFoam->GetPrimary();
        double jac           = total_xs.ScaledVarJacobian();
        double tot           = fWeight * jac * foam_integral;
        total_xs.SetTotalXS(tot);
        return tot;
      }

      /** Generate normalized random variables.
       */
      virtual std::vector<double> GenerateEventNorm() const {
        std::vector<double> fMCvect; // Array for getting normalized variables from FOAM
        // static std::array<double, N> fVars; // Array for getting normalized variables from FOAM
        fFoam->MakeEvent();               // generate MC event
        fFoam->GetMCvect(fMCvect.data()); // get generated vector (x,y)
        return fMCvect;
      }

      /** Generate random variables.
       */
      virtual std::vector<double> GenerateEvent() const {
        std::array<double, N> fMCvect; // Array for getting normalized variables from FOAM
        std::array<double, N> fVars;   // Array for getting normalized variables from FOAM
        fFoam->MakeEvent();                   // generate MC event
        fFoam->GetMCvect(fMCvect.data());     // get generated vector (x,y)
        fVars = total_xs.RescaledVars(to_tuple<VarTypes_t>(fMCvect));
        // auto res = total_xs.ComputeValues(to_tuple<VarTypes_t>(fVars));
        return std::vector<double>(std::begin(fVars), std::end(fVars));
      }

      /** Generate event and return PS variables.
       * Returns tuple with types matching PS types and generated values.
       */
      auto Generate() const {
        std::array<double, N> fMCvect; // Array for getting normalized variables from FOAM
        // static std::array<double, N> fVars; // Array for getting normalized variables from FOAM
        fFoam->MakeEvent();               // generate MC event
        fFoam->GetMCvect(fMCvect.data()); // get generated vector (x,y)
        // fVars    = total_xs.RescaledVars(to_tuple<VarTypes_t>(fMCvect));
        // auto res = total_xs.ComputeValues(to_tuple<VarTypes_t>(fVars));
        return to_tuple<VarTypes_t>(fMCvect);
      }

      /** Generate event and evaluate stack. 
       * Returns the result of stack.ComputeValues()
       */
      template <typename Stack>
      auto Generate(const Stack& s) const {
        using IndependentVariables_t   = std::decay_t<typename Stack::IndependentVariables_t>;
        static std::array<double, N> fMCvect; // Array for getting normalized variables from FOAM
        static std::array<double, N> fVars;   // Array for getting normalized variables from FOAM
        fFoam->MakeEvent();                   // generate MC event
        fFoam->GetMCvect(fMCvect.data());     // get generated vector (x,y)
        fVars    = total_xs.RescaledVars(to_tuple<VarTypes_t>(fMCvect));
        IndependentVariables_t v;
        assign_tuple_by_types(v,fVars);
        assign_tuple_by_types(v,total_xs.FixedVars_t());
        auto res = s.ComputeValues(to_tuple<VarTypes_t>(v));
        return res;
      }

      /** Foam Option helpers
       *
       */
      FoamOptions GetFoamOptions() const { return fOptions; }
      void        SetFoamOptions(const FoamOptions& opt) {
        fOptions = opt;
        fFoam->SetnCells(fOptions.fFoamCells);
        fFoam->SetnSampl(fOptions.fFoamSample);
        fFoam->SetnBin(fOptions.fFoamBins);
        fFoam->SetOptRej(fOptions.fFoamOptRej);
        fFoam->SetOptDrive(fOptions.fFoamOptDrive);
        fFoam->SetEvPerBin(fOptions.fFoamEvPerBin);
        fFoam->SetChat(fOptions.fFoamChat);
        fFoam->SetMaxWtRej(fOptions.fFoamMaxWtRej);
      }
      void SetFoamCells(Int_t v) {
        fOptions.fFoamCells = v;
        if (fFoam)
          fFoam->SetnCells(fOptions.fFoamCells);
      }
      void SetFoamSample(Int_t v) {
        fOptions.fFoamSample = v;
        if (fFoam)
          fFoam->SetnSampl(fOptions.fFoamSample);
      }
      void SetFoamBins(Int_t v) {
        fOptions.fFoamBins = v;
        if (fFoam)
          fFoam->SetnBin(fOptions.fFoamBins);
      }
      void SetFoamOptRej(Int_t v) {
        fOptions.fFoamOptRej = v;
        if (fFoam)
          fFoam->SetOptRej(fOptions.fFoamOptRej);
      }
      void SetFoamOptDrive(Int_t v) {
        fOptions.fFoamOptDrive = v;
        if (fFoam)
          fFoam->SetOptDrive(fOptions.fFoamOptDrive);
      }
      void SetFoamEvPerBin(Int_t v) {
        fOptions.fFoamEvPerBin = v;
        if (fFoam)
          fFoam->SetEvPerBin(fOptions.fFoamEvPerBin);
      }
      void SetFoamChat(Int_t v) {
        fOptions.fFoamChat = v;
        if (fFoam)
          fFoam->SetChat(fOptions.fFoamChat);
      }
      void SetFoamMaxWtRej(Double_t v) {
        fOptions.fFoamMaxWtRej = v;
        if (fFoam)
          fFoam->SetMaxWtRej(fOptions.fFoamMaxWtRej);
      }
    };

    template <class TotXS>
    constexpr auto make_pssampler(TotXS& xs) {
      return PhaseSpaceSampler<TotXS>(xs);
    }
    template <class TotXS>
    constexpr auto make_ps_sampler(TotXS& xs) {
      return PhaseSpaceSampler<TotXS>(xs);
    }

    template <typename T>
    struct is_PSSampler : std::false_type {
      using type = T;
    };

    template <typename T>
    struct is_PSSampler<PhaseSpaceSampler<T>> : std::true_type {
      using type = T;
    };

    template <class... Fs>
    struct overload : Fs... {
      template <class... Ts>
      overload(Ts&&... ts) : Fs{std::forward<Ts>(ts)}... {}
      using Fs::operator()...;
    };
    template <class... Ts>
    overload(Ts&&...) -> overload<std::remove_reference_t<Ts>...>;

    /** Abstract base class for samplers.
     *  Useful for putting samplers into containers.
     */
    class GeneratorBase { //: public TObject {
    public:
      GeneratorBase() {}
      virtual ~GeneratorBase() {}
      virtual std::vector<FinalStateParticle>
      MakeFinalState(const std::vector<double>& vars) const = 0;
      // ClassDef(GeneratorBase,1)
    };

    /** PhaseSpaceGenerator.
     * Combine generators for the same integrated phase space.
     *
     * This is differs form an event generator which consists of samplers with different phase
     * spaces. This allows the final state particles to be defined efficiently for the phase space.
     *
     * \param F Function with argument (const std::vector<double>&) which returns std::vector<FinalStateParticle>
     * \param PSSamplers PS samplers with assciated with the same phase space
     */
    template <typename F, typename... PSSamplers>
    struct PhaseSpaceGenerator : public GeneratorBase {
      static constexpr std::size_t Ns = sizeof...(PSSamplers);
      using Samplers_t                = std::tuple<PSSamplers...>;
      using Variant_t                 = std::variant<PSSamplers...>;
      using PS_t                      =  typename std::tuple_element_t<0,Samplers_t>::PS_t;
      using FixedVars_t               =  typename std::tuple_element_t<0,Samplers_t>::FixedVars_t;

      // FixedVars_t + PS_t  = IndependentVariable_t ?
      // using VarsVariant_t             = std::variant<typename PSSamplers::Vars_t...>;

      // Samplers_t                _pss;
      double                    _TotalXS = 0.0;
      std::vector<Variant_t>    _pss;
      std::vector<SamplerBase*> _base_pss;
      std::array<double, Ns>    _SamplerTotalXS;
      std::array<double, Ns>    _CumTotalXS;

      /// Func maps the 
      F _func;

      PhaseSpaceGenerator(F&& f, PSSamplers&... samps)
          : _pss({Variant_t(PSSamplers(samps))...}), _base_pss({&samps...}),
            _func(std::forward<F>(f)) {}

      template <typename Stack>
      double Init(const Stack& s) {
        auto dispatcher = helper::make_index_dispatcher<double, Ns>();
        _SamplerTotalXS = dispatcher(
            [&](auto idx) { return std::visit([&](auto&& v) { return v.Init(s); }, _pss[idx]); });
        for (int i = 0; i < Ns; i++) {
          auto xs = _SamplerTotalXS[i];
          std::cout << "xs = " << xs << "\n";
          _TotalXS += xs;
          _CumTotalXS[i] = _TotalXS;
        }
        std::cout << "Total XS = " << _TotalXS << "\n";
        return _TotalXS;
      }

      std::vector<double> GenerateEvent() const {
        // static std::random_device rd;  //Will be used to obtain a seed for the random number
        // engine static std::mt19937_64 gen(rd()); //Standard mersenne_twister_engine seeded with
        // rd() static std::uniform_real_distribution<> dis(0.0, _TotalXS); double xs_rand =
        // dis(gen);
        static TRandom3 r;
        double          xs_rand = r.Uniform(0.0, _TotalXS);
        // maybe use visit here?
        for (size_t i = 0; i < Ns; i++) {
          if (xs_rand < _CumTotalXS[i]) {
            return _base_pss[i]->GenerateEvent();
          }
        }
        return _base_pss[Ns - 1]->GenerateEvent();
        // shouldn't make it here.
        // return std::visit([&](auto&& v) { return VarsVariant_t(v.Generate()); }, _pss.at(Ns-1));
      }

      virtual std::vector<FinalStateParticle>
      MakeFinalState(const std::vector<double>& vars) const {
        return _func(vars);
      }
      std::vector<FinalStateParticle> operator()() const {
        return MakeFinalState(GenerateEvent());
      }
    };
      //[&](const std::vector<double>& v) {
      //  using IndVar = typename decltype(var_stack)::IndependentVariables_t;
      //  using PSVar  = typename decltype(tot_xs1)::VarTypes_t;
      //  IndVar sv;
      //  insane::helpers::assign_tuple_by_types(sv,insane::helpers::to_tuple<PSVar>(v));
      //  insane::helpers::assign_tuple_by_types(sv,tot_xs1.FixedVars());
      //  auto values   = var_stack.ComputeValues(sv);
      //  std::vector<insane::xsec::FinalStateParticle> fs = {
      //      {std::get<p_spectator_vec>(values).get(), {0.0, 0.0, 0.0, 0.0}, 2212}//,
      //      //{std::get<p_initial_vec>(values).get(), {0, 0, 0, 0}, 2212}
      //  };
      //  return fs;
      //},


    template <typename F, class... PSSamplers>
    constexpr auto make_ps_generator(F&& f, PSSamplers&... pss) {
      return PhaseSpaceGenerator<F, PSSamplers...>(std::forward<F>(f), pss...);
    }
    //@}

  } // namespace xsec
} // namespace insane
#endif
