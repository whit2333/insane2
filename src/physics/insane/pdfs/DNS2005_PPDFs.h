#ifndef insane_physics_DNS2005_PPDFs_HH
#define insane_physics_DNS2005_PPDFs_HH 1 

#include "insane/pdfs/PPDFs.h"
#include "insane/pdfs/FortranWrappers.h"

namespace  insane {
  namespace physics {

/** DNS2005 LO and NLO Polarized PDFs
 *
 *  Uses the subroutine POLFIT which returns the valence up and down quark polarized pdfs,
 *  the anti-up and anti-down quark polarized pdfs and also the strange and gluon polarized
 *  pdfs are calculated. The full quark distributions are calculated from the valence and
 *  anti-quark distributions following the formula
 *  \f$ \Delta q(x) = \Delta q_v(x) + \Delta \bar{q}(x)\f$
 *
 *
 *
 * \ingroup ppdfs
 */
    class DNS2005_PPDFs : public PPDFs {
   private:

    public:
      DNS2005_PPDFs(); 
      virtual ~DNS2005_PPDFs(); 

      /** Virtual method should get all values of pdfs and set
       *  values of fX and fQsquared
       *
       *  Calls
       *  POLFIT( MODE,X,Q2,
       *          DUV,DDV,
       *          DUBAR,DDBAR,
       *          DSTR,DGLU,
       *          G1P,G1N)
       *
       */
      virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
      virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(DNS2005_PPDFs,2)
    };
  }
}

#endif

