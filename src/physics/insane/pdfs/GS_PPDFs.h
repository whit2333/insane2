#ifndef insane_physics_GS_PPDFs_HH
#define insane_physics_GS_PPDFs_HH 1 

#include "insane/pdfs/PPDFs.h"
#include "insane/pdfs/FortranWrappers.h"

namespace  insane::physics {

    /**  GS Polarized PDFs.
     * T. Gehrmann and W.J. Stirling: "Polarized Parton Distributions
     *   of the Nucleon", Phys.Rev. D53 (1996) 6100.
     *
     *  Makes use of the subroutine polnlo which returns the
     *  valence quark polarized pdfs along with the anti-quark distributions.
     *  The full quark pdfs are calculated using the valence and q-bar distributions as
     *  \f$ \Delta u = \Delta u_v + \Delta \bar{q} \f$
     *  since
     *  \f$ \Delta u_v = \Delta u - \Delta \bar{q} \f$
     *
     *
     * \ingroup ppdfs
     */
    class GS_PPDFs : public PPDFs {

    public:
      GS_PPDFs();
      virtual ~GS_PPDFs();

      /** Virtual method should get all values of pdfs and set
       *  values of fX and fQsquared
       *
       * @param iflag = selects gluon set    0:A, 1:B, 2:C
       * @param x     = xbjorken
       * @param q2    = Q^2
       * OUTPUT parameters (note that always x*distribution is returned)
       * @param uval  = u-ubar
       * @param dval  = d-dbar
       * @param glue
       * @param ubar  = 1/2 usea
       * @param dbar = 1/2 dsea
       * @param str = sbar = 1/2 strsea
       *
       *  Calls
       *  polnlo(iflag,x,q2,uval,dval,glue,ubar,dbar,str)
       *
       *  \todo check that the strange is calculated correctly... look at paper..
       */
      virtual const std::array<double, NPartons>& Calculate(double x, double Q2) const;
      virtual const std::array<double, NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(GS_PPDFs, 2)
    };
} // namespace insane::physics

#endif
