#ifndef insane_physics_Stat2015_PPDFs_HH
#define insane_physics_Stat2015_PPDFs_HH 2 

#include "insane/pdfs/FortranWrappers.h"
#include "insane/pdfs/PolarizedPDFs.h"

namespace insane {
  namespace physics {

    /** Statistical 2015 PDFs.
     * C. BOURRELY and J. SOFFER
     * arXiv:1502.02517[hep-ph] submitted to EPJC
     * http://inspirehep.net/record/1343508
     *
     * \ingroup ppdfs
     */
    class Stat2015_PPDFs : public PolarizedPDFs {

      private:
        mutable int    fiPol;
        mutable int    fiSingle;
        mutable int    fiNum;
        mutable double pdfs[13];

      public:

        Stat2015_PPDFs(); 
        virtual ~Stat2015_PPDFs(); 

        virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
        virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

        ClassDef(Stat2015_PPDFs,2)
    };
  }
}

#endif

