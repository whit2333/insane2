#ifndef InSANE_JAM_T3DFs_HH
#define InSANE_JAM_T3DFs_HH 1

#include "insane/pdfs/Twist3DistributionFunctions.h"

namespace insane::physics {

  /** JAM twist-3 PDFs.
   *
   * \ingroup t3dfs
   */
  class JAM_T3DFs : public Twist3DistributionFunctions {

  public:
    JAM_T3DFs();
    virtual ~JAM_T3DFs();

    virtual const std::array<double, NPartons>& Calculate(double x, double Q2) const;
    virtual const std::array<double, NPartons>& Uncertainties(double x, double Q2) const;

    ClassDef(JAM_T3DFs, 1)
  };
} // namespace insane::physics

#endif
