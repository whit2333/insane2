#ifndef InSANE_JAM_T4DFs_HH
#define InSANE_JAM_T4DFs_HH

#include "insane/pdfs/Twist4DistributionFunctions.h"

namespace insane {
  namespace physics {

    class JAM_T4DFs : public Twist4DistributionFunctions {

      public:
        JAM_T4DFs();
        virtual ~JAM_T4DFs();

        virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
        virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

        ClassDef(JAM_T4DFs,1)
    };
  }
}

#endif

