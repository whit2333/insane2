#ifndef PartonDistributionFunctions_HH
#define PartonDistributionFunctions_HH 1
#include "insane/base/PhysicalConstants.h"
#include "TMath.h"
#include "insane/pdfs/FortranWrappers.h"
#include "TString.h"
#include <vector>
#include "TNamed.h"
#include "TAttLine.h"
#include "TAttMarker.h"

namespace insane {
namespace physics {

#define NPARTONDISTS 13

/** \todo create DGLAP evolution framework here.
 */

/** Base class for PDFs */
class PDFBase : public TNamed , public TAttLine, public TAttMarker {

   private:
      TString    fLabel;

   protected:
      double   fQsquared;
      double   fXbjorken;

      double   fxPlotMin;
      double   fxPlotMax;

      double   fModelMin_x;     // Minimum value of x for which model is valid
      double   fModelMax_x;     // Maximum value of x for which model is valid
      double   fModelMin_W;     // Min W for model. This should be the cut applied to the data for fitting
      double   fModelMax_W;     // Usually unbound
      double   fModelMin_Q2;    // Min W for model. This should be the cut applied to the data for fitting
      double   fModelMax_Q2;    // Usually unbound

      double   fPDFValues[NPARTONDISTS];  /// f(x), PDF not multiplied by x!
      double   fPDFErrors[NPARTONDISTS];
      //TF1 *      fFunctions[NPARTONDISTS]; //! For plotting only

   public :

      PDFBase();
      virtual ~PDFBase();

      void        SetLabel(const char * l){ fLabel = l;}
      const char* GetLabel(){ return fLabel; }

      typedef enum  { 
         kUP          = 0,
         kDOWN        = 1,
         kSTRANGE     = 2,
         kCHARM       = 3,
         kBOTTOM      = 4,
         kTOP         = 5,
         kGLUON       = 6,
         kANTIUP      = 7,
         kANTIDOWN    = 8,
         kANTISTRANGE = 9,
         kANTICHARM   = 10,
         kANTIBOTTOM  = 11,
         kANTITOP     = 12
      } PartonFlavor;


      /** Resets x,Q2,and the pdf values to zero. */
      void ClearValues();

      /** Virtual method should get all values of pdfs and set
       *  values of fX and fQsquared.
       */
      virtual double * GetPDFs(double x, double Qsq) {return fPDFValues;}
      virtual double * GetPDFErrors(double x, double Qsq) = 0;

      double GetXBjorken() const {  return(fXbjorken); }
      double GetQSquared() const {  return(fQsquared); }

      ClassDef(PDFBase, 4)
};



/** ABC for parton distributions
 *
 *  By default the s,sbar,c, and cbar return 0 and
 *  the up and down are pure virtual methods
 *
 * \ingroup partondistributions
 */
class PartonDistributionFunctions : public PDFBase {
  protected:
     static PartonDistributionFunctions* fgPartonDistributionFunctions;
   public:
      PartonDistributionFunctions() { }
      virtual ~PartonDistributionFunctions() { }

     static PartonDistributionFunctions* GetPartonDistributionFunctions() {return fgPartonDistributionFunctions;}

      /** @name Simple PDF access
       *  After calling GetPDFs(x,Qsq), the following return the values of the PDFs for
       *  those values of x and Qsq.
       *
       *  The quark labeling for fPDFValues is
       *  \f$ (0,1,2,3,4,5,6,7,,8,9,10,11) = (u,d,s,c,b,t,g,\bar{u},\bar{d},\bar{s},\bar{c},\bar{b},\bar{t}) \f$
       *  @{
       */
      double u() { return(fPDFValues[0]); }
      double d() { return(fPDFValues[1]); }
      double s() { return(fPDFValues[2]); }
      double c() { return(fPDFValues[3]); }
      double b() { return(fPDFValues[4]); }
      double t() { return(fPDFValues[5]); }
      double g() { return(fPDFValues[6]); }
      double ubar() { return(fPDFValues[7]); }
      double dbar() { return(fPDFValues[8]); }
      double sbar() { return(fPDFValues[9]); }
      double cbar() { return(fPDFValues[10]); }
      double bbar() { return(fPDFValues[11]); }
      double tbar() { return(fPDFValues[12]); }
      // Errors 
      double uError() { return(fPDFErrors[0]); }
      double dError() { return(fPDFErrors[1]); }
      double sError() { return(fPDFErrors[2]); }
      double cError() { return(fPDFErrors[3]); }
      double bError() { return(fPDFErrors[4]); }
      double tError() { return(fPDFErrors[5]); }
      double gError() { return(fPDFErrors[6]); }
      double ubarError() { return(fPDFErrors[7]); }
      double dbarError() { return(fPDFErrors[8]); }
      double sbarError() { return(fPDFErrors[9]); }
      double cbarError() { return(fPDFErrors[10]); }
      double bbarError() { return(fPDFErrors[11]); }
      double tbarError() { return(fPDFErrors[12]); }

      //@}

      /** up quark distribution, \f$ u(x) \f$ */
      virtual double u(double x, double Qsq) {
         GetPDFs(x,Qsq);
         return(u());
      }

      /** down quark distribution, \f$ d(x) \f$  */
      virtual double d(double x, double Qsq) {
         GetPDFs(x,Qsq);
         return(d());
      }

      /** up anti-quark distribution, \f$ \bar{u}(x) \f$  */
      virtual double uBar(double x, double Qsq) {
         GetPDFs(x,Qsq);
         return(ubar());
      }

      /** down anti-quark distribution, \f$ \bar{d}(x) \f$  */
      virtual double dBar(double x, double Qsq) {
         GetPDFs(x,Qsq);
         return(dbar());
      }

      /** Strange quark dist is zero by default. */
      virtual double s(double x, double Qsq) {
         GetPDFs(x,Qsq);
         return(s());
      }
      virtual double sBar(double x, double Qsq) {
         GetPDFs(x,Qsq);
         return(sbar());
      }
      virtual double c(double x, double Qsq)  {
         GetPDFs(x,Qsq);
         return(c());
      }
      virtual double cBar(double x, double Qsq) {
         GetPDFs(x,Qsq);
         return(cbar());
      }

      virtual double g(double x, double Qsq) {
         GetPDFs(x,Qsq);
         return(g());
      }

      /** up quark distribution, \f$ u(x) \f$ */
      virtual double uError(double x, double Qsq) {
         GetPDFErrors(x,Qsq);
         return(uError());
      }

      /** down quark distribution, \f$ d(x) \f$  */
      virtual double dError(double x, double Qsq) {
         GetPDFErrors(x,Qsq);
         return(dError());
      }

      /** up anti-quark distribution, \f$ \bar{u}(x) \f$  */
      virtual double uBarError(double x, double Qsq) {
         GetPDFErrors(x,Qsq);
         return(ubarError());
      }

      /** down anti-quark distribution, \f$ \bar{d}(x) \f$  */
      virtual double dBarError(double x, double Qsq) {
         GetPDFErrors(x,Qsq);
         return(dbarError());
      }

      /** Strange quark dist is zero by default. */
      virtual double sError(double x, double Qsq) {
         GetPDFErrors(x,Qsq);
         return(sError());
      }
      virtual double sBarError(double x, double Qsq) {
         GetPDFErrors(x,Qsq);
         return(sbarError());
      }
      virtual double cError(double x, double Qsq)  {
         GetPDFErrors(x,Qsq);
         return(cError());
      }
      virtual double cBarError(double x, double Qsq) {
         GetPDFErrors(x,Qsq);
         return(cbarError());
      }

      virtual double gError(double x, double Qsq) {
         GetPDFErrors(x,Qsq);
         return(gError());
      }


      ///** Makes a simple plot of each parton distribution function */
      //virtual void PlotPDFs(double Qsq = 5.0, Int_t log = 0);

      ///** Uses TF1 functions to make a plot returns the canvas */
      //TCanvas * PlotPDF(double Qsq = 5.0, Int_t log = 0);

      ///** @name Useful for using as ROOT functions, TF1 etc...
      // *
      // * @{
      // */
      ///** Use with TF1. The arguments are x=x[0], Q^2=p[0] */
      //double Evaluate_u(double *x, double *p) {
      //   GetPDFs(x[0], p[0]);
      //   return( u() );
      //}
      //double Evaluatexu(double *x, double *p) {
      //   GetPDFs(x[0], p[0]);
      //   return(x[0]*u());
      //}
      //double Evaluatexd(double *x, double *p) {
      //   GetPDFs(x[0], p[0]);
      //   return(x[0]*d());
      //}
      //double Evaluate_d(double *x, double *p) {
      //   GetPDFs(x[0], p[0]);
      //   return(x[0]*d());
      //}
      //double Evaluatexs(double *x, double *p) {
      //   GetPDFs(x[0], p[0]);
      //   return(x[0]*s());
      //}
      //double Evaluatexg(double *x, double *p) {
      //   GetPDFs(x[0], p[0]);
      //   return(x[0]*g());
      //}
      //double Evaluatexubar(double *x, double *p) {
      //   GetPDFs(x[0], p[0]);
      //   return(x[0]*ubar());
      //}
      //double Evaluatexdbar(double *x, double *p) {
      //   GetPDFs(x[0], p[0]);
      //   return(x[0]*dbar());
      //}
      //double Evaluatexsbar(double *x, double *p) {
      //   GetPDFs(x[0], p[0]);
      //   return(x[0]*sbar());
      //}
      ////@}

      ///** Returns a TF1 functions for easy plotting and manipulation
      // *  Argument q is the quark u=1,d=2,... gluon=6,anti-u=7, anti-d=8 ...
      // *
      // */
      //TF1 * GetFunction(PDFBase::PartonFlavor q = PDFBase::kUP);

      void GetValues(TObject *obj, double Q2, PDFBase::PartonFlavor = PDFBase::kUP );
      /** Get error band.
       *  Argument obj is assumed to be a TH1. 
       */ 
      void GetErrorBand(TObject *obj, double Q2, PDFBase::PartonFlavor = PDFBase::kUP );

      ClassDef(PartonDistributionFunctions, 2)
};

}}



#endif

