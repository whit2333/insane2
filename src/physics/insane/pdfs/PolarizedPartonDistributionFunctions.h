#ifndef PolarizedPartonDistributionFunctions_HH
#define PolarizedPartonDistributionFunctions_HH

#include "insane/pdfs/PartonDistributionFunctions.h"
//#include "TH1F.h"
namespace insane {

  using namespace units;
namespace physics {


/**  ABC for Polarized Parton Distributions
 *
 *  By default polarized charm quark distributions return zero.
 *
 * \ingroup partondistributions
 */
class PolarizedPartonDistributionFunctions : public PDFBase {

   protected:
     static PolarizedPartonDistributionFunctions* fgPolarizedPartonDistributionFunctions;
      Int_t fNintegrate;  // number of divisions for doing WW integrals

      //double   fTwist3PDFValues[NPARTONDISTS];  /// f(x), PDF not multiplied by x!
      double fxDu_Twist3 = 0.0;
      double fxDd_Twist3 = 0.0;

      double fxHu_Twist4 = 0.0;
      double fxHd_Twist4 = 0.0;

      double fxHp_Twist4 = 0.0;
      double fxHn_Twist4 = 0.0;

   public:
      PolarizedPartonDistributionFunctions();
      virtual ~PolarizedPartonDistributionFunctions();

     static PolarizedPartonDistributionFunctions* GetPolarizedPartonDistributionFunctions() {return fgPolarizedPartonDistributionFunctions;}

      /** @name Simple PDF access
       *  After calling GetPDFs(x,Qsq), the following return the values of the PDFs for
       *  those values of x and Qsq.
       *  The quark labeling for fPDFValues is
       *  \f$ (0,1,2,3,4,5,6,7,,8,9,10,11) = 
       *  (\Delta u ,\Delta d,\Delta s,\Delta c,\Delta b,\Delta t,\Delta g,\Delta \bar{u},\Delta \bar{d},\Delta \bar{s},\Delta \bar{c},\Delta \bar{b},\Delta \bar{t}) \f$
       *  @{
       */
      double Deltau()    const ;
      double Deltad()    const ;
      double Deltas()    const ;
      double Deltac()    const ;
      double Deltab()    const ;
      double Deltat()    const ;
      double Deltag()    const ;
      double Deltaubar() const ;
      double Deltadbar() const ;
      double Deltasbar() const ;
      double Deltacbar() const ;
      double Deltabbar() const ;
      double Deltatbar() const ;
      // Errors 
      double DeltauError()    const ;
      double DeltadError()    const ;
      double DeltasError()    const ;
      double DeltacError()    const ;
      double DeltabError()    const ;
      double DeltatError()    const ;
      double DeltagError()    const ;
      double DeltaubarError() const ;
      double DeltadbarError() const ;
      double DeltasbarError() const ;
      double DeltacbarError() const ;
      double DeltabbarError() const ;
      double DeltatbarError() const ;

      //@}

      /** \f$ \Delta u(x) \f$
       *
       *  Note that calling GetPDFs calculates all pdfs at the same time
       *  so using many times in a row for the same x and Qsq is
       *  redundant and wastes time.
       */
      virtual double Deltau(    double x, double Qsq) ;
      virtual double DeltauBar( double x, double Qsq) ;
      virtual double Deltad(    double x, double Qsq) ;
      virtual double DeltadBar( double x, double Qsq) ;
      virtual double Deltas(    double x, double Qsq) ;
      virtual double DeltasBar( double x, double Qsq) ;
      virtual double Deltag(    double x, double Qsq) ;
      virtual double Deltac(    double x, double Qsq) ;
      virtual double DeltacBar( double x, double Qsq) ;
      virtual double Deltab(    double x, double Qsq) ;
      virtual double DeltabBar( double x, double Qsq) ;
      virtual double Deltat(    double x, double Qsq) ;
      virtual double DeltatBar( double x, double Qsq) ;

      virtual double DeltauError(    double x, double Qsq) ;
      virtual double DeltauBarError( double x, double Qsq) ;
      virtual double DeltadError(    double x, double Qsq) ;
      virtual double DeltadBarError( double x, double Qsq) ;
      virtual double DeltasError(    double x, double Qsq) ;
      virtual double DeltasBarError( double x, double Qsq) ;
      virtual double DeltacError(    double x, double Qsq) ;
      virtual double DeltacBarError( double x, double Qsq) ;
      virtual double DeltabError(    double x, double Qsq) ;
      virtual double DeltabBarError( double x, double Qsq) ;
      virtual double DeltatError(    double x, double Qsq) ;
      virtual double DeltatBarError( double x, double Qsq) ;

      // Twist 3 quark distribution functions
      virtual double D_u(double,double)   ;
      virtual double D_d(double,double)   ;
      virtual double D_s(double,double)   { return 0.0; }
      virtual double D_ubar(double,double){ return 0.0; }
      virtual double D_dbar(double,double){ return 0.0; }
      virtual double D_sbar(double,double){ return 0.0; }

      // Twist 3 quark-gluon distribution functions
      virtual double Dp_Twist3(double,double)   ;
      virtual double Dn_Twist3(double,double)   ;
      virtual double Dd_Twist3(double,double)   { return 0.0; }
      virtual double DHe3_Twist3(double,double) { return 0.0; }

      //  x^2 moment ofTwist 3 quark-gluon distribution functions
      virtual double Moment_Dp_Twist3(int n, double Q2, double x1 = 0.1, double x2 = 0.99)   ;

      // Twist 4 quark-gluon distribution functions
      virtual double Hp_Twist4(double,double)   { return 0.0; }
      virtual double Hn_Twist4(double,double)   { return 0.0; }
      virtual double Hd_Twist4(double,double)   { return 0.0; }
      virtual double HHe3_Twist4(double,double) { return 0.0; }

   public:

      // ------------------------------------
      // Wandzura Wilczek Relation (Twist-2)
      //
      virtual double g2pWW(   double , double );
      virtual double g2nWW(   double , double );
      virtual double g2dWW(   double , double );
      virtual double g2He3WW( double , double );

      // ------------------------------------
      // Blumlein Tkabladze Relation (Twist-3)
      //
      virtual double g1p_BT_Twist3(   double , double );
      virtual double g1n_BT_Twist3(   double , double );
      virtual double g1d_BT_Twist3(   double , double );
      virtual double g1He3_BT_Twist3( double , double );

      virtual double Dp_BT(double x, double Q2);

      // ------------------------------------
      // Twist expansion with target mass M=0
      // ------------------------------------
      // Twist 2
      // g1 twist-2  M=0 (no TMC)
      virtual double g1p_Twist2(double   x, double Q2) ;
      virtual double g1n_Twist2(double   x, double Q2) ;
      virtual double g1d_Twist2(double   x, double Q2) ;
      virtual double g1He3_Twist2(double x, double Q2) ;

      // g2 twist 2 : g2WW
      virtual double g2p_Twist2(double x,double Q2)  { return g2pWW(x,Q2); }
      virtual double g2n_Twist2(double x,double Q2)  { return g2nWW(x,Q2); }
      virtual double g2d_Twist2(double x,double Q2)  { return g2dWW(x,Q2); }
      virtual double g2He3_Twist2(double x,double Q2){ return g2He3WW(x,Q2); }

      // ------------------------------------
      // Twist 3
      // g1 twist 3 - 
      virtual double g1p_Twist3(  double, double){ return 0.0;}
      virtual double g1n_Twist3(  double, double){ return 0.0;}
      virtual double g1d_Twist3(  double, double){ return 0.0;}
      virtual double g1He3_Twist3(double, double){ return 0.0;}

      // g2 twist 3 
      virtual double g2p_Twist3(  double, double);
      virtual double g2n_Twist3(  double, double);
      virtual double g2d_Twist3(  double, double);
      virtual double g2He3_Twist3(double, double);

      // ------------------------------------
      // Twist 4
      // g1 twist 4 - typically takes the form h(x)/Q^2
      virtual double g1p_Twist4(  double x, double Q2) {return Hp_Twist4(x,Q2)/Q2;}
      virtual double g1n_Twist4(  double x, double Q2) {return Hn_Twist4(x,Q2)/Q2;}
      virtual double g1d_Twist4(  double x, double Q2) {return Hd_Twist4(x,Q2)/Q2;}
      virtual double g1He3_Twist4(double x, double Q2) {return HHe3_Twist4(x,Q2)/Q2;}

      // g2 twist 4 - typically takes the form h(x)/Q^2
      virtual double g2p_Twist4(double,double)  {return 0.0;}
      virtual double g2n_Twist4(double,double)  {return 0.0;}
      virtual double g2d_Twist4(double,double)  {return 0.0;}
      virtual double g2He3_Twist4(double,double){return 0.0;}


      // ----------------------------------------------------------
      // SSFs with finite Target Mass Corrections (TMCs)
      // ----------------------------------------------------------
      // Twist 2
      // g1 twist2 + TMC
      virtual double g1p_Twist2_TMC  ( double x, double Q2);
      virtual double g1n_Twist2_TMC  ( double x, double Q2);
      virtual double g1d_Twist2_TMC  ( double x, double Q2);
      virtual double g1He3_Twist2_TMC( double x, double Q2);

      // g2 twist2 + TMC
      virtual double g2p_Twist2_TMC  ( double x, double Q2);
      virtual double g2n_Twist2_TMC  ( double x, double Q2) {return 0.0;}
      virtual double g2d_Twist2_TMC  ( double x, double Q2) {return 0.0;}
      virtual double g2He3_Twist2_TMC( double x, double Q2) {return 0.0;}

      // ------------------------------------
      // Twist 3
      // g1 twist3 + TMC
      virtual double g1p_Twist3_TMC(double x, double Q2);
      virtual double g1n_Twist3_TMC(double x, double Q2)   { return 0.0; }
      virtual double g1d_Twist3_TMC(double x, double Q2)   { return 0.0; }
      virtual double g1He3_Twist3_TMC(double x, double Q2) { return 0.0; }

      // g2 twist3 + TMC
      virtual double g2p_Twist3_TMC(double x, double Q2);
      virtual double g2n_Twist3_TMC(double x, double Q2)  { return 0.0; }
      virtual double g2d_Twist3_TMC(double x, double Q2)  { return 0.0; }
      virtual double g2He3_Twist3_TMC(double x, double Q2){ return 0.0; }

      // ------------------------------------
      // Twist 4
      // g1 twist 4 - typically takes the form h(x)/Q^2
      virtual double g1p_Twist4_TMC(  double x, double Q2)   {return Hp_Twist4(x,Q2)/Q2;}
      virtual double g1n_Twist4_TMC(  double x, double Q2)   {return Hn_Twist4(x,Q2)/Q2;}
      virtual double g1d_Twist4_TMC(  double x, double Q2)   {return Hd_Twist4(x,Q2)/Q2;}
      virtual double g1He3_Twist4_TMC(double x, double Q2) {return HHe3_Twist4(x,Q2)/Q2;}

      // g2 twist 4 - typically takes the form h(x)/Q^2
      virtual double g2p_Twist4_TMC(double,double)  {return 0.0;}
      virtual double g2n_Twist4_TMC(double,double)  {return 0.0;}
      virtual double g2d_Twist4_TMC(double,double)  {return 0.0;}
      virtual double g2He3_Twist4_TMC(double,double){return 0.0;}

      // ------------------------------------
      // Input scale functions
      // ------------------------------------
      //g1 twist 4 - typically takes the form h(x)/Q^2
      virtual double g1p_Twist4_Q20(double   x) {return 0;}
      virtual double g1n_Twist4_Q20(double   x) {return 0;}
      virtual double g1d_Twist4_Q20(double   x) {return 0;}
      virtual double g1He3_Twist4_Q20(double x) {return 0;}

      //g2 twist 3 
      virtual double g2p_Twist3_Q20(double   x) {return 0;}
      virtual double g2n_Twist3_Q20(double   x) {return 0;}
      virtual double g2d_Twist3_Q20(double   x) {return 0;}
      virtual double g2He3_Twist3_Q20(double x) {return 0;}

      //g2 twist 4 
      virtual double g2p_Twist4_Q20(double   x) {return 0;}
      virtual double g2n_Twist4_Q20(double   x) {return 0;}
      virtual double g2d_Twist4_Q20(double   x) {return 0;}
      virtual double g2He3_Twist4_Q20(double x) {return 0;}


      // useful for TMD type calculations
      double gT_u_WW(    double x, double Q2);
      double gT_d_WW(    double x, double Q2);
      double gT_ubar_WW( double x, double Q2);
      double gT_dbar_WW( double x, double Q2);

   public:

      /** @name Useful for using as ROOT functions, TF1 etc...
       *
       * @{
       */
      /** Use with TF1. The arguments are x=x[0], Q^2=p[0] */
      double EvaluatexDeltau(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*Deltau());
      }
      double EvaluatexDeltad(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*Deltad());
      }
      double EvaluatexDeltas(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*Deltas());
      }
      double EvaluatexDeltag(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*Deltag());
      }
      double EvaluatexDeltaubar(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*Deltaubar());
      }
      double EvaluatexDeltadbar(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*Deltadbar());
      }
      double EvaluatexDeltasbar(double *x, double *p) {
         GetPDFs(x[0], p[0]);
         return(x[0]*Deltasbar());
      }
      //@}


      ///** Returns a TF1 functions for easy plotting and manipulation
      // *  Argument q is the quark u=1,d=2,... gluon=6,anti-u=7, anti-d=8 ...
      // *
      // */
      //TF1 * GetFunction(PDFBase::PartonFlavor q = PDFBase::kUP);

      void GetValues(TObject *obj, double Q2, PDFBase::PartonFlavor = PDFBase::kUP );
      /** Get error band.
       *  Argument obj is assumed to be a TH1. 
       */ 
      void GetErrorBand(TObject *obj, double Q2, PDFBase::PartonFlavor = PDFBase::kUP );

      /** Makes a simple plot of each parton distribution function */
      //virtual void PlotPDFs(double Qsq = 5.0, Int_t log = 0);

      ClassDef(PolarizedPartonDistributionFunctions, 2)
};

}}


#endif

