#ifndef INSANE_PHYSICS_POLARIZEDPDFS_HH
#define INSANE_PHYSICS_POLARIZEDPDFS_HH

#include "insane/pdfs/PDFBase.h"

namespace insane::physics {

  using namespace units;

  /** \addtogroup ppdfs Helicity Dependent Parton Distributions.
   *
   *  \ingroup physics
   */

  /** PolarizedPDFs base. 
   *
   * \ingroup ppdfs
   */
  class PolarizedPDFs : public PDFBase2 {
  public:
    PolarizedPDFs();
    virtual ~PolarizedPDFs();

    double g1(double x, double Q2, Nuclei n) const;
    double g2(double x, double Q2, Nuclei n) const;

    double g1_TMC(double x, double Q2, Nuclei n) const;
    double g2_TMC(double x, double Q2, Nuclei n) const;

    double g1p_Twist2(double x, double Q2) const;
    double g1n_Twist2(double x, double Q2) const;
    double g1d_Twist2(double x, double Q2) const;
    double g1He3_Twist2(double x, double Q2) const;

    double g2p_Twist2(double x, double Q2) const { return g2p_WW(x, Q2); }
    double g2n_Twist2(double x, double Q2) const { return g2n_WW(x, Q2); }
    double g2d_Twist2(double x, double Q2) const { return g2d_WW(x, Q2); }
    double g2He3_Twist2(double x, double Q2) const { return g2He3_WW(x, Q2); }

    double g1p_Twist2_TMC(double x, double Q2) const;
    double g1n_Twist2_TMC(double x, double Q2) const;
    double g1d_Twist2_TMC(double x, double Q2) const;
    double g1He3_Twist2_TMC(double x, double Q2) const;

    double g2p_Twist2_TMC(double x, double Q2) const;
    double g2n_Twist2_TMC(double x, double Q2) const;
    double g2d_Twist2_TMC(double x, double Q2) const;
    double g2He3_Twist2_TMC(double x, double Q2) const;

    double g2p_WW(double x, double Q2) const;
    double g2n_WW(double x, double Q2) const;
    double g2d_WW(double x, double Q2) const;
    double g2He3_WW(double x, double Q2) const;

    double Delta_u() const;
    double Delta_d() const;
    double Delta_s() const;
    double Delta_c() const;
    double Delta_b() const;
    double Delta_t() const;
    double Delta_g() const;
    double Delta_ubar() const;
    double Delta_dbar() const;
    double Delta_sbar() const;
    double Delta_cbar() const;
    double Delta_bbar() const;
    double Delta_tbar() const;

    double Delta_u_uncertainty() const;
    double Delta_d_uncertainty() const;
    double Delta_s_uncertainty() const;
    double Delta_c_uncertainty() const;
    double Delta_b_uncertainty() const;
    double Delta_t_uncertainty() const;
    double Delta_g_uncertainty() const;
    double Delta_ubar_uncertainty() const;
    double Delta_dbar_uncertainty() const;
    double Delta_sbar_uncertainty() const;
    double Delta_cbar_uncertainty() const;
    double Delta_bbar_uncertainty() const;
    double Delta_tbar_uncertainty() const;

    ClassDef(PolarizedPDFs, 1)
  };
} // namespace insane::physics

#endif
