#ifndef insane_pdfs_FortranWrappers_HH
#define insane_pdfs_FortranWrappers_HH 1

/*! \page fortran2C Importing Fortran Subroutines

  \section f2cinstructions Instructions
   Assuming there are not any common blocks the following instructions show how to compile
   and link a subroutine found in a .f file :
   - include in the header (note the underscore):
     extern"C"  {void subroutineName_(double *arg1, double * arg2, etc...);}
   - In the LinkDef file:
     #pragma link C++ function subroutineName_(double * ,double * ,etc... )+;
   - Add lib/subroutineFile.o to list of object files, FortranLibObjs,  in build/General.mk
   - Make sure subroutineFile.f is in src directory

  \section f2cExamples Examples
   - class F1F209eInclusiveDiffXSec uses subroutine cross_tot found in \ref F1F209.f
   - class HallCPolarizedElectronBeam uses a subroutine in \ref sane_pol.f
*/
/**  SUBROUTINE CROSS_TOT(Z,A,E0,EP,TH,SIGMA,f1_full,f2_full,q2,wsq)
 *
 */
extern"C" {
   void inif1f209_();
   void cross_tot_(double * Z, double * A, double * E0, double * EP, double * TH, double * SIGMA, double * f1_full, double * f2_full, double * q2, double * wsq);
   void cross_qe_(double * Z, double * A, double * E0, double * EP, double * TH, double * SIGMA, double * f1_full, double * f2_full, double * q2, double * wsq);

   void cross_tot_mod_(double * Z, double * A, double * E0, double * EP, double * TH, double * SIGMA, double * f1_full, double * f2_full, double * q2, double * wsq);
   void cross_qe_mod_(double * Z, double * A, double * E0, double * EP, double * TH, double * SIGMA, double * f1_full, double * f2_full, double * q2, double * wsq);
   void emc_09_(float * X, float * A, int * goodfit, float * res);
}

/** NMC95 unpolarized structure functions. 
 *  DOUBLE PRECISION FUNCTION f1psfun(x,Q2) 
 *  DOUBLE PRECISION FUNCTION f2psfun(x,Q2) 
 *  DOUBLE PRECISION FUNCTION f1nsfun(x,Q2) 
 *  DOUBLE PRECISION FUNCTION f2nsfun(x,Q2) 
 *  DOUBLE PRECISION FUNCTION f1dsfun(x,Q2) 
 *  DOUBLE PRECISION FUNCTION f2dsfun(x,Q2) 
 *  DOUBLE PRECISION FUNCTION f1hesfun(x,Q2) 
 *  DOUBLE PRECISION FUNCTION f2hesfun(x,Q2) 
 *
 * @param x       bjorken x 
 * @param Q2      Q2 (GeV^2)  
 * 
 *  \ingroup NMC95 
 */
extern"C" {
   double f1psfun_(double *,double *); 
   double f2psfun_(double *,double *); 
   double f1nsfun_(double *,double *); 
   double f2nsfun_(double *,double *); 
   double f1dsfun_(double *,double *); 
   double f2dsfun_(double *,double *); 
   double f1hesfun_(double *,double *); 
   double f2hesfun_(double *,double *); 
   double r1998_(double *,double *); 
} 

/**  SUBROUTINE F1F209IN(Z,A,q2,Wsq,F1,F2,R)
 *
 */
extern"C" {
   void f1f2qe09_(double * Z, double * A, double * q2, double * wsq, double * F1, double * F2);
   void f1f2in09_(double * Z, double * A, double * q2, double * wsq, double * F1, double * F2, double * R);
   void f1f2in09_mod_(double * Z, double * A, double *E0,double * q2, double * wsq, double * F1, double * F2, double * R);
}

/** SUBROUTINE FORMC(Q2). 
 *  3He form factor sum-of-gaussians fit from Amroun et al 
 * 
 * @param Q2      Q2(fm-2) 
 */
extern"C" {
   double formc_(double *Q2); 
   double formm_(double *Q2); 
}

/** SUBROUTINE DSSVFIT(x,Q2,duv,ddv,dubar,ddbar,dstr,dglu) 
 *
 * @param x       bjorken x 
 * @param Q2      Q2 (GeV) 
 * @param duv     delta u_v 
 * @param ddv     delta d_v 
 * @param dubar   delta u-bar
 * @param ddbar   delta d-bar
 * @param dstr    delta s (= s-bar) 
 * @param dglu    delta g 
 *
 * \ingroup DSSV 
 */ 

extern "C"{
   // void dssvini_();
   void dssvini_(int *); 
   void dssvfit_(double *x,double *Q2,double *duv,double *ddv,double *dubar,double *ddbar,double *dstr,double *dglu);
}

/** MRST2006.
 * source: ./src/mrst2006.f 
 * @param i      PDF set (0 = central value)   
 * @param x      bjorken x 
 * @param q      sqrt(Q2) (GeV) 
 * @param uv     u_v 
 * @param dv     d_v 
 * @param ubar   u-bar
 * @param dbar   d-bar
 * @param str    s  
 * @param sbar   s-bar 
 * @param chm    charm 
 * @param bot    bottom 
 * @param glu    g 
 *
 */ 
extern "C"{
   void mrstpdfs_(int *i,double *x,double *q,double *uv,double *dv,double *ubar,double *dbar,double *str,double *sbar,double *chm,double *bot,double *glu); 
}

/** MRST2002.
 * source: ./src/mrst2002.f 
 * @param i      PDF set (1 = NLO, 2 = NNLO)   
 * @param x      bjorken x 
 * @param q      sqrt(Q2) (GeV) 
 * @param uv     u_v 
 * @param dv     d_v 
 * @param ubar   u-bar
 * @param dbar   d-bar
 * @param str    strange 
 * @param chm    charm 
 * @param bot    bottom 
 * @param glu    gluon 
 *
 */ 
extern "C"{
   void mrst2002_(int *i,double *x,double *q,double *uv,double *dv,double *ubar,double *dbar,double *str,double *chm,double *bot,double *glu); 
}

/** MRST2001.
 * source: ./src/mrst2001.f 
 * @param i      PDF set (0 = central value)   
 * @param x      bjorken x 
 * @param q      sqrt(Q2) (GeV) 
 * @param uv     u_v 
 * @param dv     d_v 
 * @param ubar   u-bar
 * @param dbar   d-bar
 * @param str    strange 
 * @param chm    charm 
 * @param bot    bottom 
 * @param glu    gluon 
 *
 */ 
extern "C"{
   void mrst2001e_(int *i,double *x,double *q,double *uv,double *dv,double *ubar,double *dbar,double *str,double *chm,double *bot,double *glu); 
}

/** REAL FUNCTION PARTONDF*8(x,Q2,J) 
 *
 * @param x   bjorken x 
 * @param Q2  Q2 (GeV^2) 
 * @param J   quark type (see ./src/partondf.f for details) 
 * 
 * \ingroup BBS 
 */ 
extern "C"{
   double partondf_(double *x,double *Q2,int *J);  
   void partonevol_(double *x, double *Q2, int *ipol, double * pdf, int * isingle, int *num);
}

/**  Wrapper to legacy Wiser Fortran code (wiser_func.f)
 *
 * @param E0MM       is electron beam energy, OR max of Brem spectra
 * @param PMM        is scattered particle  momentum
 * @param THETA_DEG  is kaon angle in degrees
 * @param RAD_LEN (%)is the radiation length of target, including internal
 *              (typically 5%)
 *             = .5 *(target radiation length in %) +5.
 *     ***  =100. IF BREMSTRULUNG PHOTON BEAM OF 1 EQUIVIVENT QUANTA  ***
 * @param TYPE:     1 for pi+;  2 for pi-, 3=k+, 4=k-, 5=p, 6=p-bar
 * @param SIGMA      is output cross section in nanobars/GeV-str
 *
 * \ingroup wiser
 */
extern"C" {
   void wiser_all_sig_(double * E0MM, double * PMM, double * THETA_DEG,
                       double * RAD_LEN, int *  TYPE, double * SIGMA);
}

/**  Same as wiser_all_sig_ except it includes pi0 production.
 *
 * @param E0MM       is electron beam energy, OR max of Brem spectra
 * @param PMM        is scattered particle  momentum
 * @param THETA_DEG  is kaon angle in degrees
 * @param RAD_LEN (%)is the radiation length of target, including internal
 *              (typically 5%)
 *             = .5 *(target radiation length in %) +5.
 *     ***  =100. IF BREMSTRULUNG PHOTON BEAM OF 1 EQUIVIVENT QUANTA  ***
 * @param TYPE:     0 for pi0, 1 for pi+;  2 for pi-, 3=k+, 4=k-, 5=p, 6=p-bar
 * @param SIGMA      is output cross section in nanobars/GeV-str
 *
 * \ingroup wiser
 */
extern"C" {
   void wiser_all_sig0_(double * E0MM, double * PMM, double * THETA_DEG,
                        double * RAD_LEN, int *  TYPE, double * SIGMA);
}

/**  Wiser fit to mono energetic photo production.
 *    (wiser_fit.f)
 *
 * \ingroup wiser
 */
extern"C" {
   void wiser_fit_(int * ITYPE, double * P, double * THETA, double * E_GAMMA, double * SIG);
}

/** Virtual photon spectrum.
 *  TIATOR-WRIGHT VIRTUAL PHOTON SPECTRUM
 *  PHYS. REV. C26,2349(1982) AND NUC. PHYS. A379,407(1982)
 *
 *  @param AMT is target mass?
 *  @param AM1 is produced particle mass (MeV/c2)
 *  @param EI  is the electron beam energy (MeV)
 *  @param W0  is the photon energy (MeV)
 *  @param TP  is the kinetic energy of the produced particle (MeV)
 *  @param TH  is the angle of the produced particle (radians)
 *  @param GN  the result of Ne*R/w0 of eqn.12 in Nuc.Phys.A379
 */
extern"C" {
   void vtp_(double *AMT,double *AM1,double *EI,double *W0,double *TP,double *TH,double *GN);
}

/**  Wrapper to legacy Wiser Fortran code (wiser_func.f)
 *
 * \ingroup wiser
 */
extern"C" {
   void wiser_sub_(double * res, int * ITYPE, double * EE, double * EP, double * THP);
}

/** Wrapper to legacy QFS/nQFS Fortran code
 *
 *  CALCULATE QUASIELASTIC, TWO-NUCLEON, RESONANCE, AND X-SCALING
 *  ELECTRON SCATTERING CROSS SECTION FOR ARBITRARY NUCLEI
 *
 *  WRITTEN BY J.W. LIGHTBODY JR. AND J.S. O'CONNELL
 *  NATIONAL BUREAU OF STANDARDS, GAITHERSBURG, MD 20899
 *  OCTOBER 1987
 *
 *  The arguments are:
 *    - RES: Returned result in units of (pb/(sr-MeV))
 *    - Z: The target nucleus charge
 *    - N: The nucleus neutron number
 *    - EBEAM: The incident electron beam energy (MeV)
 *    - PCENTRAL: The central value for the scattered electron momentum acceptance (MeV/c)
 *    - DELP: Symmetric momentum acceptance range given as a fraction of the central momentum, ie, Pmin = PCENTRAL*(1.0-DELP) and Pmax = PCENTRAL*(1.0+DELP)
 *    - THETA: Central scattering angle (radians)
 *    - DELTHETA: Scattered angle acceptance in (radians?)
 *
 * \ingroup QFS
 */
extern"C" {
   void qfs_(double* RES, double* Z, double* N, double* EBEAM, double* PCENTRAL, double* DELP, double* THETA, double* DELTHETA);
}


/** QFS Born Cross Section (no radiative corrections)
 *  CALCULATE QUASIELASTIC, TWO-NUCLEON, RESONANCE, AND X-SCALING
 *  ELECTRON SCATTERING CROSS SECTION FOR ARBITRARY NUCLEI
 *
 *  WRITTEN BY J.W. LIGHTBODY JR. AND J.S. O'CONNELL
 *  NATIONAL BUREAU OF STANDARDS, GAITHERSBURG, MD 20899
 *  OCTOBER 1987
 *
 *  The arguments are:
 *    - RES: Returned result in units of (pb/(sr-MeV))
 *    - Z: The target nucleus charge
 *    - N: The nucleus neutron number
 *    - EPS: Nucleon separation energy (MeV) 
 *    - EPSD: Delta separation energy (MeV) 
 *    - PF: Fermi momentum (MeV) 
 *    - EBEAM: The incident electron beam energy (MeV)
 *    - PCENTRAL: The central value for the scattered electron momentum acceptance (MeV/c)
 *    - DELP: Symmetric momentum acceptance range given as a fraction of the central momentum, ie, Pmin = PCENTRAL*(1.0-DELP) and Pmax = PCENTRAL*(1.0+DELP)
 *    - THETA: Central scattering angle (radians)
 *    - DELTHETA: Scattered angle acceptance in (radians?)
 *
 * \ingroup QFS
 */
extern"C" {
   void qfs_born_(double* RES, double* Z, double* N, double *EPS,double *EPSD,double *PF,double* EBEAM, double* PCENTRAL, double* THETA,int* XSTYPE);
}

/** QFS Born Cross Section (WITH radiative corrections)
 *  CALCULATE QUASIELASTIC, TWO-NUCLEON, RESONANCE, AND X-SCALING
 *  ELECTRON SCATTERING CROSS SECTION FOR ARBITRARY NUCLEI
 *
 *  WRITTEN BY J.W. LIGHTBODY JR. AND J.S. O'CONNELL
 *  NATIONAL BUREAU OF STANDARDS, GAITHERSBURG, MD 20899
 *  OCTOBER 1987
 *
 *  Radiative corrections ... (add details here)
 *
 *  The arguments are:
 *    - RES: Returned result in units of (pb/(sr-MeV))
 *    - Z: The target nucleus charge
 *    - N: The nucleus neutron number
 *    - EBEAM: The incident electron beam energy (MeV)
 *    - PCENTRAL: The central value for the scattered electron momentum acceptance (MeV/c)
 *    - DELP: Symmetric momentum acceptance range given as a fraction of the central momentum, ie, Pmin = PCENTRAL*(1.0-DELP) and Pmax = PCENTRAL*(1.0+DELP)
 *    - THETA: Central scattering angle (radians)
 *    - DELTHETA: Scattered angle acceptance in (radians?)
 *
 * \ingroup QFS
 */
extern"C" {
   void qfs_radiated_(double* RES, double* Z, double* N, double* EBEAM, double* PCENTRAL, double* DELP, double* THETA, double* DELTHETA);
}

/** AAC08PDF(Q2,X,ISET,XPPDF,GRAD)
 *
 */
extern"C" {
   void aac08pdf_(double* Q2, double* X, int* ISET, double* XPPDF, double** GRAD);
}

/** PPDF(ISET, X, Q2, UV, DUV, DV, DDV, GL, DGL, QB, DQB, G1P, DG1P, G1N, DG1N)
 */
extern"C" {
   void ppdf_(int* , double*, double*, double*, double*, double*, double*, double*, double*, double*, double*, double*, double*, double*, double*);
}

/** JAM15.
 *     Authors:                                                   
 *     Nobuo Sato         (Jefferson Lab)                         
 *     Jake Ethier        (College of William and Mary)           
 *     Wally Melnitchouk  (Jefferson Lab)                         
 *     Alberto Accardi    (Hampton University and Jefferson Lab)
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
 *     FUNCTION get_xF(x,Q2,flav)
 *     INPUT: x,Q^2, and flavor
 *     flav (character*2): 'up','dp','sp','cp','bp','gl','u','d','s'
 *                         'c','b','ub','db','sb','cb','bb','p','n'
 *     Flavors 'qp' = q + qb (e.g. 'up' = u + ubar)
 *             'p'  = proton (for T4PPDF and structure functions)
 *             'n'  = neutron (for T4PPDF and structure functions)
 *
 *     Returns x times the function (PPDF,FF,etc.)
 *
 *     GRID_INIT must be called only once before using FUNCTION get_xF!
 *
 *     SUBROUTINE GRID_INIT requires three inputs:
 *
 *     - lib (character*10): library (JAM15,JAM16,etc.)
 *     - dist (character*10): distribution type (PPDF,FFpion,FFkaon,etc.)
 *     - ipos (integer): posterior number (0 to 199) from MC analysis
 */
extern"C" {
   void jam_xf_(double *res, double* x,double* Q2, char* flav);
   void grid_init_(char*, char*, char*, int*);
}

/** POLFIT(MODE,X,Q2,DUV,DDV,DUBAR,DDBAR,DSTR,DGLU,G1P,G1N)
 */
extern"C" {
   void polfit_(int* , double*, double*, double*, double*, double*, double*, double*, double*, double*, double*);
}
/** INI()
 *  Initializes the grids used by POLFIT
 */
extern"C" {
   void ini_();
}

/**  LSS2006(ISET,X,Q2,UUB,DDB,SSB,GL,UV,DV,UB,DB,ST,g1pLT,g1p,g1nLT,g1n)
 */
extern"C" {
   void lss2006_(int* , double*, double*, double*, double*, double*, double*, double*, double*, double*, double*, double*, double*, double*, double*, double*);
}

/** LSS2006INIT
 *  Subroutine called for initializing LSS2006 grids
 */
extern"C" {
   void lss2006init_();
}

/**  LSS2010(ISET,X,Q2,UUB,DDB,UV,DV,UB,DB,ST,GL)
 */
extern"C" {
   void lss2010_(int*,double*,double*,double*,double*,double*,double*,double*,double*,double*,double*);
}

/** LSS2010INIT
 *  Subroutine called for initializing LSS2010 grids
 */
extern"C" {
   void lss2010init_();
}

/**  polnlo(iflag,x,q2,uval,dval,glue,ubar,dbar,str)
 *
 */
extern"C" {
   void polnlo_(int* , double*, double*, double*, double*, double*, double*, double*, double*);
}
/** Initializes polnlo
 *
 */
extern"C" {
   void nloini_();
}

/**  CTEQ6
 *
 */
extern"C" {
   void setctq6_(int*);
   void getctq6_(int* , double*, double*, double*);
}

/** CTEQ10. 
  * source file: ./src/cteq10.f 
  */
extern"C" {
   void setct10_(int*);
   double getct10_(int*,double*,double*,double*);  
}

/** CTEQ-JLab 12 (CJ12).  
 *  source file: ./src/CJ12pdf.f 
 */ 
extern"C" {
   void setcj_(int *); 
   double cjpdf_(int *,double *,double *); 
}
/** ABKM09 unpolarized PDFs.
  * source file: ./src/abkm09.f 
  * 
  */
extern"C"{
   void getabkm09_(int*,double*,double*,int*,int*,int*,double*);
}
/** MSTW08. 
  * source file: ./src/mstwpdf.f 
  */
extern"C"{
   void getmstw08_(int*,int*,double*,double*,double*);
}

/** EPCV.
 *  
 *  @param PART or string is Particle maybe either 'P','N','PI+','PI-','PI0'
 *  @param len_string is the length of the particle string (needed for C function call Fortran)
 *  @param Z    Number of protons in nuclei
 *  @param N    Number of neturons in nuclei
 *  @param E1   Beam Energy in MeV
 *  @param PTP  Scattered particle momentum in MeV/c
 *  @param THP  Scattered particle angle in degrees
 *  @param RES  Returned cross section result in ub/Mev-Sr
 *
 * \ingroup EPCV
 */
extern"C" {
   void epcv_single_(char* string,  int * len_string , double* Z, double* N, double* E1, double* PTP, double* THP, double* RES);
}

extern"C" {
   void epcv_single_v3_(char* string,  int * len_string , double* Z, double* N, double* E1, double* PTP, double* THP, double* RES);
}

extern"C" {
   void gpc_single_v3_(char* string,  int * len_string , double* Z, double* N, double* E1, double* PTP, double* THP, double* RES);
}


extern"C" {
   void maid07tot_(int *, double*,double*,  double*,double*,double*,double*,double*,double*,double*,double*); 
}

#endif

