#ifndef insane_physics_LSS2006_PPDFs_HH
#define insane_physics_LSS2006_PPDFs_HH 1 

#include "insane/pdfs/PPDFs.h"
#include "insane/pdfs/FortranWrappers.h"

namespace  insane {
  namespace physics {

    class LSS2006_PPDFs : public PPDFs {
    protected:
    //  mutable LSS2006PolarizedPDFs   old_pdfs;
      int fiSet;
      /** Virtual method should get all values of pdfs and set
       *  values of fX and fQsquared.
       *
       * \code
       *   OUTPUT:  UUB = x *(DELTA u + DELTA ubar)
       *            DDB = x *(DELTA d + DELTA dbar)
       *            SSB = x *(DELTA s + DELTA sbar)
       *            GL  = x * DELTA GLUON
       *            UV  = x * DELTA uv
       *            DV  = x * DELTA dv
       *            UB  = x * DELTA ubar
       *            DB  = x * DELTA dbar
       *            ST  = x * DELTA sbar
       *            g1p = x*g_1^proton = x*g1p_LT + x*g1p_HT
       *            g1pLT = x*g1p_{NLO+TMC}
       *            g1n = x*g_1^neutron = x*g1n_LT + x*g1n_HT
       *            g1nLT = x*g1n_{NLO+TMC}
       *
       *      NOTE: The valence parts DELTA uv, DELTA dv
       *            DELTA uv = (DELTA u + DELTA ubar) - 2*DELTA ubar
       *            DELTA dv = (DELTA d + DELTA dbar) - 2*DELTA dbar
       * \endcode
       *  Calls subroutine
       *  LSS2006(ISET,X,Q2,UUB,DDB,SSB,GL,UV,DV,UB,DB,ST,g1pLT,g1p,g1nLT,g1n).
       */
   private: 
      double Extrapolate(double,double,double,double,double) const;
    public:
      LSS2006_PPDFs(); 
      virtual ~LSS2006_PPDFs(); 

      virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
      virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(LSS2006_PPDFs,2)
    };
  }
}

#endif

