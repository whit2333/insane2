#ifndef INSANE_PHYSICS_UnpolarizedPDFs_HH
#define INSANE_PHYSICS_UnpolarizedPDFs_HH

#include "insane/pdfs/PDFBase.h"

namespace insane {

  namespace physics {

    /** \addtogroup pdfs Parton Distributions
     *
     *  \ingroup physics
     */

    /** Parton Distribution functions.
     *
     * \ingroup pdfs
     */
    class UnpolarizedPDFs : public PDFBase2 {
      public:
        UnpolarizedPDFs();
        virtual ~UnpolarizedPDFs();

        double F1(double x, double Q2, Nuclei n ) const;
        double F2(double x, double Q2, Nuclei n ) const;

        double F1_TMC(double x, double Q2, Nuclei n) const;
        double F2_TMC(double x, double Q2, Nuclei n) const;

        double F1p_Twist2(  double x, double Q2) const;
        double F1n_Twist2(  double x, double Q2) const;
        double F1d_Twist2(  double x, double Q2) const;
        double F1He3_Twist2(double x, double Q2) const;

        double F2p_Twist2(  double x, double Q2) const;
        double F2n_Twist2(  double x, double Q2) const;
        double F2d_Twist2(  double x, double Q2) const;
        double F2He3_Twist2(double x, double Q2) const;

        double F1p_Twist2_TMC(  double x, double Q2) const;
        double F1n_Twist2_TMC(  double x, double Q2) const;
        double F1d_Twist2_TMC(  double x, double Q2) const;
        double F1He3_Twist2_TMC(double x, double Q2) const;

        double F2p_Twist2_TMC(  double x, double Q2) const;
        double F2n_Twist2_TMC(  double x, double Q2) const;
        double F2d_Twist2_TMC(  double x, double Q2) const;
        double F2He3_Twist2_TMC(double x, double Q2) const;

        double u()    const ;
        double d()    const ;
        double s()    const ;
        double c()    const ;
        double b()    const ;
        double t()    const ;
        double g()    const ;
        double ubar() const ;
        double dbar() const ;
        double sbar() const ;
        double cbar() const ;
        double bbar() const ;
        double tbar() const ;

        double u_uncertainty()    const ;
        double d_uncertainty()    const ;
        double s_uncertainty()    const ;
        double c_uncertainty()    const ;
        double b_uncertainty()    const ;
        double t_uncertainty()    const ;
        double g_uncertainty()    const ;
        double ubar_uncertainty() const ;
        double dbar_uncertainty() const ;
        double sbar_uncertainty() const ;
        double cbar_uncertainty() const ;
        double bbar_uncertainty() const ;
        double tbar_uncertainty() const ;

        ClassDef(UnpolarizedPDFs,1)
    };
  }
}

#endif

