#ifndef BBSQuarkHelicityDistributions_H
#define BBSQuarkHelicityDistributions_H

#include <cstdlib> 
#include <iostream> 
#include <iomanip>
#include <cmath>  
#include "TMath.h"
#include "insane/pdfs/PartonHelicityDistributions.h"
namespace insane {
namespace physics {

/** Quark helicity distribtuions at FIXED Q2 = 4.0 GeV2. 
 *
 */
class BBSQuarkHelicityDistributions : public PartonHelicityDistributions {

   private:
      /// polynomial coefficients 
      double fAu,fAd,fAs,fAg;
      double fBu,fBd,fBs,fBg;
      double fCu,fCd,fCs;
      double fDu,fDd,fDs;
      double fAlpha,fAlpha_g; 

   public: 
      BBSQuarkHelicityDistributions(); 
      virtual ~BBSQuarkHelicityDistributions();

      double uPlus(double);  
      double uMinus(double);  
      double dPlus(double);  
      double dMinus(double);  
      double sPlus(double);  
      double sMinus(double);  
      double gPlus(double);  
      double gMinus(double);  

      virtual Int_t CalculateDistributions(double x, double Q2);

      ClassDef(BBSQuarkHelicityDistributions,1)
 
};
}
}

#endif 

