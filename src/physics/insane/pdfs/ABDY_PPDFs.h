#ifndef insane_physics_ABDY_PPDFs_HH
#define insane_physics_ABDY_PPDFs_HH 1 

#include "insane/pdfs/ABDY_PHDs.h"
#include "insane/pdfs/PPDFs.h"

namespace  insane {
  namespace physics {

/** Avakian polarized parton distribution functions.  
  * A fit from H. Avakian, S. Brodsky, A. Deur and F. Yuan
  *
  * Paper reference: Phys. Rev. Lett. 99, 082001 (2007)  
  * 
  * \ingroup ppdfs
  */
    class ABDY_PPDFs : public PPDFs {
   private: 
      ABDY_PHDs fqhd; 
    public:
      ABDY_PPDFs(); 
      virtual ~ABDY_PPDFs(); 

      virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
      virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(ABDY_PPDFs,2)
    };
  }
}

#endif

