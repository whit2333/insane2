#ifndef insane_physics_PDFBase2_HH
#define insane_physics_PDFBase2_HH 

#include "TNamed.h"
#include "TAttLine.h"
#include "TAttMarker.h"
#include "TAttFill.h"
#include <array>
#include <string>
#include "insane/base/Physics.h"

namespace insane {

  using namespace units;
  namespace physics {

    /** Object for passing PDF values.
     */
    struct PDFValues {
      double                       fQ2;
      double                       fx;
      std::array<double, NPartons> fValues;
      std::array<double, NPartons> fUncertainties;
      ClassDef(PDFValues, 1)
    };


    class PDFBase2 : public TNamed , public virtual TAttLine, public virtual TAttFill, public virtual TAttMarker {
      public:
        //static const size_t NPartons = 13;
        //enum class PartonFlavor { 
        //  kUP          = 0,
        //  kDOWN        = 1,
        //  kSTRANGE     = 2,
        //  kCHARM       = 3,
        //  kBOTTOM      = 4,
        //  kTOP         = 5,
        //  kGLUON       = 6,
        //  kANTIUP      = 7,
        //  kANTIDOWN    = 8,
        //  kANTISTRANGE = 9,
        //  kANTICHARM   = 10,
        //  kANTIBOTTOM  = 11,
        //  kANTITOP     = 12,
        //  FIRST        = kUP,
        //  LAST         = kANTITOP
        //} ;

        //static constexpr std::array<double,NPartons> e_q2 = { 
        //  4.0/9.0, 1.0/9.0, 1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0,
        //  0.0,
        //  4.0/9.0, 1.0/9.0, 1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0
        //};


      protected:
        std::string      fLabel;
        //mutable double   fQsquared;
        //mutable double   fXbjorken;

        //mutable std::array<double, NPartons> fValues;
        //mutable std::array<double, NPartons> fUncertainties;
        mutable  PDFValues m_PDFValues;

        double   fxPlotMin;
        double   fxPlotMax;

        double   fModelMin_x;     // Minimum value of x for which model is valid
        double   fModelMax_x;     // Maximum value of x for which model is valid
        double   fModelMin_W;     // Min W for model. This should be the cut applied to the data for fitting
        double   fModelMax_W;     // Usually unbound
        double   fModelMin_Q2;    // Min W for model. This should be the cut applied to the data for fitting
        double   fModelMax_Q2;    // Usually unbound

        //double   fPDFValues[NPartons];  /// f(x), PDF not multiplied by x!
        //double   fPDFErrors[NPartons];

        bool IsComputed(double x, double Q2) const;

      public :

        PDFBase2();
        virtual ~PDFBase2();

        void        SetLabel(const char * l){ fLabel = l;}
        const char* GetLabel() const { return fLabel.c_str(); }

        double GetXBjorken() const {  return(m_PDFValues.fx); }
        double GetQSquared() const {  return(m_PDFValues.fQ2); }
        double Getx() const {  return(m_PDFValues.fx); }
        double GetQ2() const {  return(m_PDFValues.fQ2); }

        /** Resets x,Q2,and the pdf values to zero. */
        void Reset();

        /** Return all values.
         * Returns all pdf values and uncertainties via the the PDFValues class.
         * Note it does check the given x and Q2 value.
         */
        PDFValues  Get(double x, double Q2) const ;

        /** Calculate and return distribution value.
         * Returns the current value for flavor f but checks that the 
         * distributions have already been calculated at (x,Q2). 
         * It uses IsComputed(x,Q2) to do this check.
         */
        double  Get(const Parton& f, double x, double Q2) const ;

        //double  Get(Parton f, double x, double Q2) const ;

        /** Get current distribution value.
         * Note: this method should only be used to get the stored values 
         * after Calculate(x,Q2) or Get(f,x,Q2) has been used at the desired (x,Q2).
         */ 
        double  Get(const Parton& f) const ;

        ///** Get current distribution value.
        // * Note: this method should only be used to get the stored values 
        // * after Calculate(x,Q2) or Get(f,x,Q2) has been used at the desired (x,Q2).
        // */ 
        //double  Get(Parton f) const ;

        /** Virtual method should get all values of pdfs and set
         *  values of fX and fQsquared.
         */
        virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
        virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

        ClassDef(PDFBase2, 5)
    };
  }
}

#endif

