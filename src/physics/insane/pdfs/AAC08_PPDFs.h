#ifndef insane_physics_AAC08_PPDFs_HH
#define insane_physics_AAC08_PPDFs_HH 1

//#include "insane/pdfs/AAC08PolarizedPDFs.h"
#include "insane/pdfs/FortranWrappers.h"
#include "insane/pdfs/PPDFs.h"
#include "insane/pdfs/PartonDistributionFunctions.h"
#include "insane/pdfs/PolarizedPartonDistributionFunctions.h"

namespace insane::physics {

  /** AAC08 Polarized PDFs.
   *
   */
  class AAC08_PPDFs : public PolarizedPDFs {
    // protected:
    //   mutable AAC08PolarizedPDFs   old_pdfs;
  protected:
  public:
    /** C'tor initializes the "pointer to a pointer" gradient matrix, fGrad,
     *  which returned by the subroutine AAC08PDF
     */
    AAC08_PPDFs();
    virtual ~AAC08_PPDFs();

    /** Method should get all values of pdfs and set
     *  values of fX and fQsquared
     *
     *  \code
     *  !!    XPPDF(I) --> AAC08 polarized PDFs.
     *  !!     I = -3 ... s-bar quark   --> Carray[0]
     *  !!         -2 ... d-bar quark   --> Carray[1]
     *  !!         -1 ... u-bar quark   --> Carray[2]
     *  !!          0 ... gluon D_g(x)  --> Carray[3]
     *  !!          1 ... u quark       --> Carray[4]
     *  !!          2 ... d quark       --> Carray[5]
     *  !!          3 ... s quark       --> Carray[6]
     *  \endcode
     */
    virtual const std::array<double, NPartons>& Calculate(double x, double Q2) const;
    virtual const std::array<double, NPartons>& Uncertainties(double x, double Q2) const;

    ClassDef(AAC08_PPDFs, 2)
  };
} // namespace insane::physics
#endif
