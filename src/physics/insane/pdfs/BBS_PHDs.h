#ifndef insane_pdfs_BBS_PHDs_H
#define insane_pdfs_BBS_PHDs_H

#include "TMath.h"
#include "insane/pdfs/PartonHelicityDistributions.h"
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <iostream>
namespace insane {
  namespace physics {

    /** Quark helicity distribtuions at FIXED Q2 = 4.0 GeV2.
     *
     */
    class BBS_PHDs : public PartonHelicityDistributions {

    private:
      /// polynomial coefficients
      double fAu, fAd, fAs, fAg;
      double fBu, fBd, fBs, fBg;
      double fCu, fCd, fCs;
      double fDu, fDd, fDs;
      double fAlpha, fAlpha_g;

    public:
      BBS_PHDs();
      virtual ~BBS_PHDs();

      double uPlus(double) const;
      double uMinus(double) const;
      double dPlus(double) const;
      double dMinus(double) const;
      double sPlus(double) const;
      double sMinus(double) const;
      double gPlus(double) const;
      double gMinus(double) const;

      virtual Int_t CalculateDistributions(double x, double Q2) const;

      ClassDef(BBS_PHDs, 1)
    };
  } // namespace physics
} // namespace insane

#endif
