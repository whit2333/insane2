set(aname pdfs)
set(subdirname physics)
set(needs_libs InSANEbase )
set(libname "InSANE${aname}")
set(dictname "${libname}Dict")
set(lib_LINKDEF "${PROJECT_SOURCE_DIR}/src/${subdirname}/${aname}/include/LinkDef.h")
set(lib_DICTIONARY_SRC "${libname}Dict.cxx")

# ROOT6 generates pcm files that should install with into the lib directory
# todo: fix the install of this
set(lib_PCM_FILE "${PROJECT_BINARY_DIR}/src/${subdirname}/${aname}/lib${libname}Dict_rdict.pcm")
#set(lib_PCM_FILE "${libname}Dict_rdict.pcm")

include_directories(${PROJECT_SOURCE_DIR}/src/${subdirname}/${aname}/include)

# add src file names here (without the extention or src/ prefix)
set(lib_files
  PartonDistributionFunctions 
  PolarizedPartonDistributionFunctions 
  StatisticalQuarkFits     
  StatisticalUnpolarizedPDFs 
  StatisticalPolarizedPDFs 
  Stat2015UnpolarizedPDFs 
  Stat2015PolarizedPDFs 
  BBSQuarkHelicityDistributions     
  BBSUnpolarizedPDFs 
  BBSPolarizedPDFs
  AvakianQuarkHelicityDistributions     
  AvakianUnpolarizedPDFs 
  AvakianPolarizedPDFs 
  DSSVPolarizedPDFs 
  AAC08PolarizedPDFs 
  JAM15PolarizedPDFs 
  BBPolarizedPDFs 
  JAMPolarizedPDFs 
  MHKPolarizedPDFs 
  GSPolarizedPDFs 
  DNS2005PolarizedPDFs 
  LSS2006PolarizedPDFs 
  LSS2010PolarizedPDFs 
  #LHAPDFUnpolarizedPDFs 
  #LHAPDFPolarizedPDFs 
  CTEQ6UnpolarizedPDFs 
  CJ12UnpolarizedPDFs 
  LCWFPartonDistributionFunctions 
  LSS98QuarkHelicityDistributions
  LSS98UnpolarizedPDFs
  LSS98PolarizedPDFs
  PartonHelicityDistributions
  PartonDistributionFunctionsFromPHDs
  ABKM09UnpolarizedPDFs
  CTEQ10UnpolarizedPDFs
  MSTW08UnpolarizedPDFs
  MRST2001UnpolarizedPDFs
  MRST2002UnpolarizedPDFs
  MRST2006UnpolarizedPDFs
  #Physics
  PDFBase
  PolarizedPDFs
  UnpolarizedPDFs
  CTEQ10_UPDFs
  PPDFs
  JAM_PPDFs
  Stat2015_UPDFs
  Stat2015_PPDFs
  Twist3DistributionFunctions
  Twist4DistributionFunctions
  T3DFsFromTwist3SSFs
  JAM_T3DFs
  LCWF_T3DFs
  JAM_T4DFs
  AAC08_PPDFs
  BB_PPDFs
  BBS_PPDFs
  DSSV_PPDFs
  LSS2006_PPDFs
  LSS2010_PPDFs
  DNS2005_PPDFs
  GS_PPDFs
  ABDY_PPDFs
  )

set(lib_Fortran_files
   ppdf 
   Cteq6Pdf2010 
   CJ12pdf 
   aac08 
   LSS2006pdf_g1 
   LSS2010_pdfs 
   polnlo 
   JAMLIB
   DSSV-2008 
   mrst2001 
   mrst2002 
   mrst2006 
   partondf 
   #nmc_org 
   #r1998 
   readgrid 
   abkm09
   cteq10
   mstwpdf
   evolparton
   )

set(lib_SRCS)
set(lib_HEADERS)
foreach(infileName ${lib_files})
   SET(lib_SRCS ${lib_SRCS} ${PROJECT_SOURCE_DIR}/src/${subdirname}/${aname}/src/${infileName}.cxx)
   SET(lib_HEADERS ${lib_HEADERS} ${PROJECT_SOURCE_DIR}/src/${subdirname}/${aname}/include/${infileName}.h)
endforeach(infileName)

set(lib_Fortran_SRCs)
foreach(infileName ${lib_Fortran_files})
   SET(lib_Fortran_SRCs ${lib_Fortran_SRCs} "${PROJECT_SOURCE_DIR}/src/${subdirname}/${aname}/src/${infileName}.f")
endforeach(infileName)


#MESSAGE("(${dictname} ${lib_HEADERS} LINKDEF ${lib_LINKDEF}) ")
ROOT_GENERATE_DICTIONARY(${dictname} ${lib_HEADERS} LINKDEF ${lib_LINKDEF} OPTIONS -p)

SET(lib_HEADERS ${lib_HEADERS})
SET(lib_SRCS ${lib_Fortran_SRCs} ${lib_SRCS} ${lib_DICTIONARY_SRC})

SET(lib_VERSION "${${PROJECT_NAME}_VERSION}")
SET(lib_MAJOR_VERSION "${${PROJECT_NAME}_MAJOR_VERSION}")
SET(lib_LIBRARY_PROPERTIES 
    VERSION "${lib_VERSION}"
    SOVERSION "${lib_MAJOR_VERSION}"
    SUFFIX ".so")

ADD_CUSTOM_TARGET(${aname}_ROOTDICTS DEPENDS ${lib_SRCS} ${lib_HEADERS} ${lib_DICTIONARY_SRC} ${lib_DICTIONARY_HEADER})

add_library(${libname} SHARED ${lib_SRCS})
target_link_libraries(${libname} ${LINK_LIBRARIES} ${needs_libs})
set_target_properties(${libname} PROPERTIES ${lib_LIBRARY_PROPERTIES})
target_compile_features(${libname}
  PUBLIC cxx_auto_type
  PUBLIC cxx_trailing_return_types
  #PUBLIC cxx_std_14
  PUBLIC cxx_std_17
  PRIVATE cxx_variadic_templates
  )

add_dependencies(${libname}
  ${needs_libs}
  ${aname}_ROOTDICTS)

install(
  TARGETS ${libname} 
  EXPORT ${PROJECT_NAME}Targets
  DESTINATION lib )

install(
  FILES ${lib_PCM_FILE} 
  DESTINATION lib )

install(
   FILES ${lib_HEADERS} 
   DESTINATION include/${PROJECT_NAME} )

#install(TARGETS foo
#  # IMPORTANT: Add the foo library to the "export-set"
#  RUNTIME DESTINATION "${INSTALL_BIN_DIR}" COMPONENT bin
#  LIBRARY DESTINATION "${INSTALL_LIB_DIR}" COMPONENT shlib
#  PUBLIC_HEADER DESTINATION "${INSTALL_INCLUDE_DIR}/foo"
#    COMPONENT dev)
