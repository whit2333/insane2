#ifndef INSANE_PHYSICS_T3DFsFromTwist3SSFs_HH
#define INSANE_PHYSICS_T3DFsFromTwist3SSFs_HH


#include "insane/pdfs/Twist3DistributionFunctions.h"

namespace insane {
  namespace physics {

    /** Twist-3 distributions calculated from dynamical twist-3 functions g2_tw3  .
     *
     *  Taking the derivative of both sides
     *  g2_tw3 = D(x) - \int_x^1 (dy/y) D(y) 
     *  and solving the differential equation with the boundary condition
     *  D(1) = 1
     *  gives
     *  D(x) = (-1/x) \int_x^1 (y[dg2/dx](y))
     */ 
    class T3DFsFromTwist3SSFs : public Twist3DistributionFunctions {

      protected:
        double fInputScale; // Model input scale
        std::array<double,5> fParam_p;
        std::array<double,5> fParam_n;

      protected:

        virtual double g2_model(double x, const std::array<double,5>& p) const;

      public:
        T3DFsFromTwist3SSFs();
        virtual ~T3DFsFromTwist3SSFs();

        double g2pTwist3_model(double x) const ;
        double g2nTwist3_model(double x) const ;

        virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const ;
        virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const ;

        // override these so that they define the twist-3 distributions 
        virtual double g2p_Twist3(    double x, double Q2) const ;
        virtual double g2n_Twist3(    double x, double Q2) const ;

        ClassDef(T3DFsFromTwist3SSFs,1)
    };
  }
}

#endif

