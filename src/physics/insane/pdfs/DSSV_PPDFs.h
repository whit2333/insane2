#ifndef insane_physics_DSSV_PPDFs_HH
#define insane_physics_DSSV_PPDFs_HH 1 

#include "insane/pdfs/PPDFs.h"
#include "insane/pdfs/FortranWrappers.h"

namespace  insane {
  namespace physics {

    /** DSSV Polarized parton distruction functions.
     *
     * \ingroup ppdfs
     */
    class DSSV_PPDFs : public PPDFs {
    private:
      // quark distributions
      // NOTE: In the class PolarizedPDFs, the labeling in the array fPDFValues is:
      //  \f$ (0,1,2,3,4,5,6,7,8,9,10,11) =
      //  (u,d,s,c,b,t,g,\bar{u},\bar{d},\bar{s},\bar{c},\bar{b},\bar{t}) \f$

      // void Fit(double x,double Q2);
      double Extrapolate(double, double, double, double, double) const;

    protected:
      int fiSet;

    public:
      DSSV_PPDFs(int iset = 0);
      virtual ~DSSV_PPDFs();

      // virtual double * GetPDFs(     double x, double Q2);
      // virtual double * GetPDFErrors(double x, double Q2);

      /// for switching between PDF sets (to calculate errors on observables that are combos of the
      /// PDFs).
      void SetGrid(int i) {
        fiSet = i;
        dssvini_(&fiSet);
      }

      virtual const std::array<double, NPartons>& Calculate(double x, double Q2) const;
      virtual const std::array<double, NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(DSSV_PPDFs, 2)
    };
  } // namespace physics
} // namespace insane

#endif

