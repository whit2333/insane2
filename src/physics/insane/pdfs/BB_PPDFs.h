#ifndef insane_physics_BB_PPDFs_HH
#define insane_physics_BB_PPDFs_HH 1

//#include "insane/pdfs/BBPolarizedPDFs.h"
#include "insane/pdfs/PPDFs.h"

namespace insane::physics {

  /** BB Polarized PDFs.
   *
   * Bluemlein and Boettcher's LO and NLO Polarized PDFs
   *
   *  Makes use of the subroutine PPDF which returns (with errors) the
   *  valence quark polarized pdfs along with the anti-quark distributions.
   *  The full quark pdfs are calculated using the valence and q-bar distributions as
   *  \f$ \Delta u = \Delta u_v + \Delta \bar{q} \f$
   *  since
   *  \f$ \Delta u_v = \Delta u - \Delta \bar{q} \f$
   *
   *
   * \ingroup ppdfs
   */
  class BB_PPDFs : public PPDFs {
  private:
    // int fiSet;
    // double fg1p, fDg1p, fg1n, fDg1n;
    // protected:
    //  mutable BBPolarizedPDFs   old_pdfs;
  public:
    /** C'tor initializes the "pointer to a pointer" gradient matrix, fGrad,
     *  which returned by the fortran subroutine.
     */
    BB_PPDFs();
    virtual ~BB_PPDFs();

    virtual const std::array<double, NPartons>& Calculate(double x, double Q2) const;
    virtual const std::array<double, NPartons>& Uncertainties(double x, double Q2) const;

    ClassDef(BB_PPDFs, 3)
  };
} // namespace insane::physics

#endif

