#ifndef Twist3DistributionFunctions_HH
#define Twist3DistributionFunctions_HH 1

#include "insane/base/Math.h"
#include "insane/pdfs/PDFBase.h"
#include <array>
#include "TMath.h"

namespace insane {
  namespace physics {

  /** \addtogroup t3dfs Twist-3 Distribution Functions.
   *
   *  \ingroup physics
   */

    /** Twist distributions for constructing the twist 3
     *  spin structure functions.
     *
     *  Following the Blumlein/JAM formalism. In the massless limit
     *  the twist three parts are 
     *
     * \f$  g_2^{tw3}(x,Q^2) = D(x) - \int D(y) \frac{dy}{y} \f$
     * 
     * and
     *
     * \f$ g_1^{tw3}(x,Q^2) = 0 \f$
     *
     * \ingroup t3dfs
     */
    class Twist3DistributionFunctions  : public PDFBase2 {
      public:
        Twist3DistributionFunctions();
        virtual ~Twist3DistributionFunctions();

        double g1(double x, double Q2, Nuclei n) const;
        double g2(double x, double Q2, Nuclei n) const;

        double g1_TMC(double x, double Q2, Nuclei n) const;
        double g2_TMC(double x, double Q2, Nuclei n) const;

        virtual double g2p_Twist3(    double x, double Q2) const;
        virtual double g2p_Twist3_TMC(double x, double Q2) const;
        virtual double g1p_Twist3(    double x, double Q2) const{ return 0.0; }
        virtual double g1p_Twist3_TMC(double x, double Q2) const;

        virtual double g2n_Twist3(    double x, double Q2) const;
        virtual double g2n_Twist3_TMC(double x, double Q2) const;
        virtual double g1n_Twist3(    double x, double Q2) const{ return 0.0; }
        virtual double g1n_Twist3_TMC(double x, double Q2) const;

        // Twist 3 quark distribution functions
        virtual double D_u(   double, double) const;
        virtual double D_d(   double, double) const;
        virtual double D_s(   double, double) const;
        virtual double D_ubar(double, double) const;
        virtual double D_dbar(double, double) const;
        virtual double D_sbar(double, double) const;

        // Twist 3 quark-gluon distribution functions
        virtual double Dp_Twist3(  double, double) const;
        virtual double Dn_Twist3(  double, double) const;
        virtual double Dd_Twist3(  double, double) const { return 0.0; }
        virtual double DHe3_Twist3(double, double) const { return 0.0; }

        double d2p(    double Q2,double x1 = 0.01,double x2 = 0.99) ;
        double d2p_TMC(double Q2,double x1 = 0.01,double x2 = 0.99) ;


        ClassDef(Twist3DistributionFunctions,1)
    };

  }
}

#endif

