#ifndef insane_physics_BBS_PPDFs_HH
#define insane_physics_BBS_PPDFs_HH 1 

#include "insane/pdfs/BBS_PHDs.h"
#include "insane/pdfs/PPDFs.h"

namespace insane {
  namespace physics {

    /** BBS polarized parton distribution functions.
     * A fit from S.J. Brodsky, M. Burkardt and Ivan Schmidt (BBS)
     *
     * From abstract: For polarized quark and gluon distributions in the nucleon
     * at low Q2.  Utilizes constraints obtained from requiring
     * color coherence of gluon couplings at x ~ 0 and helicity
     * retention properties of pQCD couplings at x ~ 1.
     *
     * Paper reference: Nucl. Phys. B 441 (1995) 197--214
     * DOI: 10.1016/0550-3213(95)00009-H
     * e-Print: hep-ph/9401328
     *
     * \ingroup ppdfs
     */
    class BBS_PPDFs : public PPDFs {
      // protected:
      //  mutable BBSPolarizedPDFs   old_pdfs;
    private:
      BBS_PHDs fqhd;

    public:
      BBS_PPDFs();
      virtual ~BBS_PPDFs();

      virtual const std::array<double, NPartons>& Calculate(double x, double Q2) const;
      virtual const std::array<double, NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(BBS_PPDFs, 2)
    };
  } // namespace physics
} // namespace insane

#endif
