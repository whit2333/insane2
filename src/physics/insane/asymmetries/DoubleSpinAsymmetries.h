#ifndef insane_physics_DoubleSpinAsymmetries_HH
#define insane_physics_DoubleSpinAsymmetries_HH

#include <cmath>

namespace insane {
  namespace physics {

    /** Elastic double spin asymmetry.
     *
     * \f[
     * A = - \frac{2\sqrt{\tau(1+\tau)}\tan(\theta/2)}{G_E^2 + \frac{\tau}{\epsilon} G_M^2}
     * \Big(\sin\theta^* \cos\phi^* G_E G_M + \sqrt{\tau(1+(1+\tau)\tan^2(\theta/2))} \cos\theta^* G_M^2 \Big)
     * \f]
     *
     * \f[\epsilon = \big[ 1+2(1+\tau)\tan^2(\theta/2) \big]^{-1} \f]
     * \f$ \tau = Q2/4M^2\f$, \f$ \theta^*\f$ and \f$ \phi^* \f$ are the polarization direction angles
     * relative to lepton scattering plane.
     *
     */
    template <class FF>
    double A_elastic(const FF& ff, double Q2, double th_e, double th_star, double phi_star) {
      double tau      = Q2/(4.0*0.938*0.938);
      double tanTheta = std::tan(th_e/2.0);
      double eps_den  = 1.0+2.0*(1.0+tau)*tanTheta*tanTheta;
      double eps      = 1.0/eps_den;
      double GE       = ff.GEp(Q2);
      double GM       = ff.GMp(Q2);
      double t1       = -2.0*std::sqrt(tau*(1.0+tau))*tanTheta/(GE*GE + (tau/eps)*GM*GM);
      double t2       = std::sin(th_star)*std::cos(phi_star)*GE*GM;
      double t3       = std::sqrt(tau*(1.0+(1.0+tau)*tanTheta*tanTheta))*std::cos(th_star)*GM*GM;
      return t1*(t2+t3);
    }

  }
}

#endif
