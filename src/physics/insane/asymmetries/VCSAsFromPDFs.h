#ifndef insane_physics_VCSAsFromPDFs_HH
#define insane_physics_VCSAsFromPDFs_HH 1

#include "insane/asymmetries/VirtualComptonScatteringAsymmetries.h"
#include "insane/structurefunctions/SSFsFromPDFs.h"
#include "insane/structurefunctions/SFsFromPDFs.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "TMath.h"

namespace insane {
  namespace physics {

    using namespace insane::units;

    /** Virtual Compton scattering asymmetries.
     *  \f$ A_1 = \frac{g_1 - (4M^2x^2/Q^2)g_2}{F_1} \f$
     *  \f$ A_2 = \frac{2Mx}{\sqrt{Q^2}} \frac{g_1 + g_2}{F_1} \f$
     *
     */
    //template<class updf_types , class ppdf_types> double A1(

    template< class...T1, class...T2 >
    double A1(const std::tuple<T1...>&& updfs, const std::tuple<T2...>&& ppdfs, Nuclei target, double x, double Q2){
      double F_1 = F1_TMC(std::forward<T1...>(updfs), target, x, Q2);
      double g_1 = g1_TMC(std::forward<T2...>(ppdfs), target, x, Q2);
      double g_2 = g2_TMC(std::forward<T2...>(ppdfs), target, x, Q2);
      double M   = M_p/GeV;
      double gam2= 4.0*M*M*x*x/Q2;
      //std::cout << "g1 : " << g_1 << std::endl;
      //std::cout << "g2 : " << g_2 << std::endl;
      //std::cout << "F1 : " << F_1 << std::endl;
      return( (g_1 - gam2*g_2)/F_1 );
    } 

    template< class...T1, class...T2 >
    double A2(const std::tuple<T1...>&& updfs, const std::tuple<T2...>&& ppdfs, Nuclei target, double x, double Q2){
      double F_1 = F1_TMC(std::forward<T1...>(updfs), target, x, Q2);
      double g_1 = g1_TMC(std::forward<T2...>(ppdfs), target, x, Q2);
      double g_2 = g2_TMC(std::forward<T2...>(ppdfs), target, x, Q2);
      double M   = M_p/GeV;
      double gam = 2.0*M*x/TMath::Sqrt(Q2);
      //std::cout << "A2 : " << g_1 << std::endl;
      return( gam*(g_1 + g_2)/F_1 );
    } 

    template< class...T1, class...T2 >
    double A1_NoTMC(const std::tuple<T1...>&& updfs, const std::tuple<T2...>&& ppdfs, Nuclei target, double x, double Q2){
      double F_1 = F1(std::forward<T1...>(updfs), target, x, Q2);
      double g_1 = g1(std::forward<T2...>(ppdfs), target, x, Q2);
      double g_2 = g2(std::forward<T2...>(ppdfs), target, x, Q2);
      double M   = M_p/GeV;
      double gam2= 4.0*M*M*x*x/Q2;
      return( (g_1 - gam2*g_2)/F_1 );
    } 

    template< class...T1, class...T2 >
    double A2_NoTMC(const std::tuple<T1...>&& updfs, const std::tuple<T2...>&& ppdfs, Nuclei target, double x, double Q2){
      double F_1 = F1(std::forward<T1...>(updfs), target, x, Q2);
      double g_1 = g1(std::forward<T2...>(ppdfs), target, x, Q2);
      double g_2 = g2(std::forward<T2...>(ppdfs), target, x, Q2);
      double M   = M_p/GeV;
      double gam = 2.0*M*x/TMath::Sqrt(Q2);
      return( gam*(g_1 + g_2)/F_1 );
    } 

    template< class...T1, class...T2 >
    double A1(const std::tuple<T1...>& updfs, const std::tuple<T2...>& ppdfs, Nuclei target, double x, double Q2){
      double F_1 = F1_TMC(updfs, target, x, Q2);
      double g_1 = g1_TMC(ppdfs, target, x, Q2);
      double g_2 = g2_TMC(ppdfs, target, x, Q2);
      double M   = M_p/GeV;
      double gam2= 4.0*M*M*x*x/Q2;
      //std::cout << "g1 : " << g_1 << std::endl;
      //std::cout << "g2 : " << g_2 << std::endl;
      //std::cout << "F1 : " << F_1 << std::endl;
      return( (g_1 - gam2*g_2)/F_1 );
    } 

    template< class...T1, class...T2 >
    double A2(const std::tuple<T1...>& updfs, const std::tuple<T2...>& ppdfs, Nuclei target, double x, double Q2){
      double F_1 = F1_TMC(updfs, target, x, Q2);
      double g_1 = g1_TMC(ppdfs, target, x, Q2);
      double g_2 = g2_TMC(ppdfs, target, x, Q2);
      double M   = M_p/GeV;
      double gam = 2.0*M*x/TMath::Sqrt(Q2);
      //std::cout << "A2 : " << g_1 << std::endl;
      return( gam*(g_1 + g_2)/F_1 );
    } 

    template< class...T1, class...T2 >
    double A1_NoTMC(const std::tuple<T1...>& updfs, const std::tuple<T2...>& ppdfs, Nuclei target, double x, double Q2){
      double F_1 = F1(updfs, target, x, Q2);
      double g_1 = g1(ppdfs, target, x, Q2);
      double g_2 = g2(ppdfs, target, x, Q2);
      double M   = M_p/GeV;
      double gam2= 4.0*M*M*x*x/Q2;
      return( (g_1 - gam2*g_2)/F_1 );
    } 

    template< class...T1, class...T2 >
    double A2_NoTMC(const std::tuple<T1...>& updfs, const std::tuple<T2...>& ppdfs, Nuclei target, double x, double Q2){
      double F_1 = F1(updfs, target, x, Q2);
      double g_1 = g1(ppdfs, target, x, Q2);
      double g_2 = g2(ppdfs, target, x, Q2);
      double M   = M_p/GeV;
      double gam = 2.0*M*x/TMath::Sqrt(Q2);
      return( gam*(g_1 + g_2)/F_1 );
    } 

    // Soffer-Teryaev  Positivity bound.
    // |A_2| < \sqrt{R(1+A_1)/2}
    template< class...T1, class...T2 >
    double A2_PositivityBound(const std::tuple<T1...>& updfs, const std::tuple<T2...>& ppdfs, Nuclei target, double x, double Q2){
      double F_1 = F1(updfs, target, x, Q2);
      double F_2 = F2(updfs, target, x, Q2);
      double RLT = insane::physics::R(F_1,F_2,x,Q2);
      double A_2 = A2_NoTMC(updfs,ppdfs,target,x,Q2);
      return TMath::Sqrt( RLT*(1.0+A_2)/2.0 );
    }

    template<class...> 
      class VCSAsFromPDFs{};

    template<class T1,class T2> 
      class VCSAsFromPDFs<T1,T2> : public VCSAsFromPDFs<std::tuple<T1>,std::tuple<T2>>{};

    template< 
      template<class...> class T1, class...T1args,
      template<class...> class T2, class...T2args>
    class VCSAsFromPDFs<T1<T1args...>,T2<T2args...>> : public VirtualComptonScatteringAsymmetries {
      public:
        T1<T1args...> fPDFs;
        T2<T2args...> fPPDFs;
        //PDF  fPDFs;
        //HT   fHTs;

      public:
        VCSAsFromPDFs(){
          //std::cout << "DERP\n";
        }
        virtual ~VCSAsFromPDFs(){}

        //const PDF&    GetPDFs() const {return std::get<0>(fDFs);}
        //const std::tuple<PDF, T...>&   GetDFs() const { return fDFs;}

        virtual double Calculate    (double x, double Q2,
            std::tuple<ComptonAsymmetry,Nuclei> sf,
            Twist t                                = Twist::All,
            OPELimit l                             = OPELimit::MassiveTarget) const;
        virtual double Uncertainties(double x, double Q2,
            std::tuple<ComptonAsymmetry,Nuclei> sf,
            Twist t                                = Twist::All,
            OPELimit l                             = OPELimit::MassiveTarget) const;

        virtual double A1(Nuclei target, double x, double Q2) const {
          return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A1,target));
        }
        virtual double A2(Nuclei target, double x, double Q2) const {
          return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A2,target));
        }

        virtual double A1_NoTMC(Nuclei target, double x, double Q2) const {
          return Calculate(x, Q2,
              std::make_tuple(ComptonAsymmetry::A1,target),
              Twist::All,
              OPELimit::MasslessTarget);
        }
        virtual double A2_NoTMC(Nuclei target, double x, double Q2) const {
          return Calculate(x, Q2,
              std::make_tuple(ComptonAsymmetry::A2,target),
              Twist::All,
              OPELimit::MasslessTarget);
        }

        virtual double A1p(double x, double Q2) const{ return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A1,Nuclei::p)); }
        virtual double A2p(double x, double Q2) const{ return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A2,Nuclei::p)); }
        virtual double A1n(double x, double Q2) const{ return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A1,Nuclei::n)); }
        virtual double A2n(double x, double Q2) const{ return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A2,Nuclei::n)); }
        virtual double A1p_NoTMC(double x, double Q2) const{ return A1_NoTMC(Nuclei::p, x, Q2); }
        virtual double A2p_NoTMC(double x, double Q2) const{ return A2_NoTMC(Nuclei::p, x, Q2); }
        virtual double A1n_NoTMC(double x, double Q2) const{ return A1_NoTMC(Nuclei::n, x, Q2); }
        virtual double A2n_NoTMC(double x, double Q2) const{ return A2_NoTMC(Nuclei::n, x, Q2); }

        ClassDef(VCSAsFromPDFs,1)
    };


  }
}

#include "VCSAsFromPDFs.hxx"
//#include "VCSAsFromSFs.h"
//#include "MAID_VCSAs.h"


#endif

