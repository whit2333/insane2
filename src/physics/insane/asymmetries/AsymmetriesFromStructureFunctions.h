#ifndef ASYMMETRIESFROMSTRUCTUREFUNCTIONS_H 
#define ASYMMETRIESFROMSTRUCTUREFUNCTIONS_H 4 

#include <cstdlib>
#include <iostream> 
#include <iomanip> 
#include <fstream>
#include "insane/base/PhysicalConstants.h"
#include "insane/asymmetries/AsymmetryBase.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "insane/pdfs/CTEQ6UnpolarizedPDFs.h"
#include "insane/pdfs/BBPolarizedPDFs.h"
#include "insane/pdfs/DNS2005PolarizedPDFs.h"
#include "insane/pdfs/DSSVPolarizedPDFs.h"
#include "insane/structurefunctions/F1F209StructureFunctions.h"
#include "insane/structurefunctions/StructureFunctionsFromPDFs.h"
#include "insane/structurefunctions/PolarizedStructureFunctionsFromPDFs.h"

namespace insane {
  namespace physics {

    /** Double spin asymmetries and virtual Compton scattering asymmetries from 
     *  polarized and unpolarized structure functions.
     *  
     */
    class AsymmetriesFromStructureFunctions : public AsymmetryBase {

    private:
      StructureFunctions            * fUnpolSFs;
      PolarizedStructureFunctions   * fPolSFs; 

      void SetValues(Double_t,Double_t,Double_t &,Double_t &,Double_t &,Double_t &,Double_t &); 

    public:
      AsymmetriesFromStructureFunctions();
      virtual ~AsymmetriesFromStructureFunctions(); 

      virtual Double_t A1(Double_t,Double_t); 
      virtual Double_t A2(Double_t,Double_t); 

      /** Measured asymmetry for an arbitrary target polarization. 
       *  Optional argument of phi is the angle between scattering plane and the plane 
       *  with which the target polarization is rotated.
       */
      virtual Double_t AMeasured(Double_t targ_angle , Double_t x ,Double_t Qsq, Double_t y,Double_t phie = 0);
      virtual Double_t AMeasuredFixedBeamEnergy(Double_t targ_angle , Double_t x ,Double_t Qsq, Double_t E0,Double_t phie = 0);

      /** Measured asymmetry as a function of E,E',theta,phi. */
      Double_t A_Measured(Double_t targ_angle , Double_t E_beam ,Double_t E_prime, Double_t theta,Double_t phie = 0);

      /** \todo Parameters? */
      // double EvaluateAMeasured_p(double *x,double *p){
      //    Nucleus::NucleusType t = Nucleus::NucleusType(p[0]); 
      //    SetTargetType(t); 
      //    return AMeasured(x[0],p[1],p[1]);
      // }
      // double EvaluateAMeasuredFixedBeamEnergy_p(double *x,double *p){
      //    Nucleus::NucleusType t = Nucleus::NucleusType(p[0]); 
      //    SetTargetType(t); 
      //    return AMeasuredFixedBeamEnergy(x[0],p[1],p[2]);
      // }

      void SetUnpolarizedSFs(StructureFunctions *usfs){
        fUnpolSFs = usfs;
        TString l = "";
        if(fUnpolSFs) l += fUnpolSFs->GetLabel();
        l += " " ;
        if(fPolSFs) l+= fPolSFs->GetLabel();
        SetLabel(l);
      } 
      void SetPolarizedSFs(PolarizedStructureFunctions *psfs){
        fPolSFs = psfs;
        TString l = "";
        if(fUnpolSFs) l+= fUnpolSFs->GetLabel();
        l+= " " ;
        if(fPolSFs) l+= fPolSFs->GetLabel();
        SetLabel(l);
      } 

      ClassDef(AsymmetriesFromStructureFunctions,4) 
    };
  }
}

#endif 
