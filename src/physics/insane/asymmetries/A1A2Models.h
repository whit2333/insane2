#ifndef insane_physics_A1A2Models_HH 
#define insane_physics_A1A2Models_HH 

#include "insane/asymmetries/VirtualComptonAsymmetries.h"

namespace insane {
   namespace physics {
      /** (p0 + p1 x + p2 x^2 + p3 x^3) x^p4
      */
      class A1A2Model1 : public VirtualComptonAsymmetries {

         protected:
            Double_t fP_A1p[5];
            Double_t fP_A2p[5];
            TF1 *    fA1pFunc;
            TF1 *    fA2pFunc;

            Double_t ModelFunction(Double_t x, Double_t Q2, const Double_t *p) const {
               double   W  = insane::kine::W_xQsq(x, Q2);
               Double_t t1 = p[0] + p[1]/W + 0.0*p[2]/(W*W) + p[3]/TMath::Sqrt(Q2) + 0.0*p[4]*TMath::Sqrt(Q2) ;
               return t1;
               //return( t1*TMath::Power(x,p[4]) );
            }

         public: 
            A1A2Model1(){
               fP_A1p[0] =  1.0;
               fP_A1p[1] =  0.0;
               fP_A1p[2] = -0.2;
               fP_A1p[3] = -0.6;
               fP_A1p[4] =  0.7;
               fA1pFunc = new TF1("A1p",this, &A1A2Model1::EvaluateA1p,
                     0.0,1.0, 1,"A1A2Model1","EvaluateA1p");

               fP_A2p[0] =  0.25;
               fP_A2p[1] = -0.1;
               fP_A2p[2] =  0.3;
               fP_A2p[3] = -0.3;
               fP_A2p[4] =  0.9;
               fA2pFunc = new TF1("A2p",this, &A1A2Model1::EvaluateA2p,
                     0.0, 1.0, 1,"A1A2Model1","EvaluateA2p");
            }
            virtual ~A1A2Model1(){ }

            virtual Double_t * GetA1pParameters(){ return fP_A1p;}
            virtual Double_t * GetA2pParameters(){ return fP_A2p;}
            virtual TF1 * GetA1pFunction(){ return fA1pFunc; }
            virtual TF1 * GetA2pFunction(){ return fA2pFunc; }

            virtual Double_t A1p(Double_t x, Double_t Q2){
               return( ModelFunction(x,Q2,fP_A1p) ) ;
            } 
            virtual Double_t A2p(Double_t x, Double_t Q2){ 
               return( ModelFunction(x,Q2,fP_A2p) ) ;
            } 

            ClassDef(A1A2Model1,1)
      };
   }
}

#endif
