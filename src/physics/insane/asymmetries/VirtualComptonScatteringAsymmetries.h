#ifndef insane_physics_VirtualComptonScatteringAsymmetries_HH 
#define insane_physics_VirtualComptonScatteringAsymmetries_HH 1

#include "TNamed.h"
#include "TAttLine.h"
#include "TAttMarker.h"
#include "TAttFill.h"
#include "insane/base/Physics.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "insane/structurefunctions/StructureFunctions.h"

namespace insane {
  namespace physics {

    /** Spin Structure Functions interface.
     *  At this level an implementation not involving the pdfs,
     *  e.g., an empircal fit, will ignore the twist argument.
     *  The implementation SSFsFromPDFs will not ignore the twist argument. 
     */
    class VirtualComptonScatteringAsymmetries : public TNamed , public virtual TAttLine, public virtual TAttFill, public virtual TAttMarker {

      protected:
        std::string      fLabel;

        // Storage of the x and Q2 values last used to compute the structure function
        // indexed by enum class insane::physics::SF
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fx_values;
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fQ2_values;
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fValues;
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fUncertainties;

      public:
        VirtualComptonScatteringAsymmetries(){ } 
        virtual ~VirtualComptonScatteringAsymmetries(){ }

        void        SetLabel(const char * l){ fLabel = l;}
        const char* GetLabel() const { return fLabel.c_str(); }

        double GetXBjorken(std::tuple<SF,Nuclei> sf) const
        {
          auto f  = static_cast<int>(std::get<SF>(sf));
          auto n  = static_cast<int>(std::get<Nuclei>(sf));
          return(fx_values.at(n).at(f));
        }
        //______________________________________________________________________________

        double GetQSquared(std::tuple<SF,Nuclei> sf) const
        {
          auto f  = static_cast<int>(std::get<SF>(sf));
          auto n  = static_cast<int>(std::get<Nuclei>(sf));
          return(fQ2_values.at(n).at(f));
        }
        //______________________________________________________________________________

        bool   IsComputed( std::tuple<SF,Nuclei> sf, double x, double Q2) const
        {
          auto f  = static_cast<int>(std::get<SF>(sf));
          auto n  = static_cast<int>(std::get<Nuclei>(sf));
          if(  fx_values.at(n).at(f) != x ) return false;
          if( fQ2_values.at(n).at(f) != Q2 ) return false;
          return true;
        }
        //______________________________________________________________________________

        void Reset()
        {
          for(int n = 0; n<NNuclei;  n++) {
            for(int i = 0; i<NStructureFunctions ; i++) {
              fx_values[n][i]        = 0.0;
              fQ2_values[n][i]       = 0.0;
              fValues[n][i]          = 0.0;
              fUncertainties[n][i]   = 0.0;
            }
          }
        }
        //______________________________________________________________________________

        /** Calculate and return distribution value.
         * Returns the current value for flavor f but checks that the 
         * distributions have already been calculated at (x,Q2). 
         * It uses IsComputed(x,Q2) to do this check.
         */
        double  Get(std::tuple<SF,Nuclei> sf, double x, double Q2) const 
        {
          return 0.0;
        }
        //______________________________________________________________________________

        /** Get current distribution value.
         * Note: this method should only be used to get the stored values 
         * after Calculate(x,Q2) or Get(f,x,Q2) has been used at the desired (x,Q2).
         */ 
        double  Get(std::tuple<SF,Nuclei> sf) const {
          return 0.0;
        }

        /** Virtual method should get all values of SFs. This also sets
         *  internal values of fx and fQsquared for use by IsComputed()
         */
        virtual double  Calculate    (double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const {return 0.0;}
        virtual double  Uncertainties(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const {return 0.0;}


        virtual double Get(double x, double Q2, std::tuple<ComptonAsymmetry,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const 
        {
          auto sftype = std::get<ComptonAsymmetry>(sf);
          auto target = std::get<Nuclei>(sf);
          switch(sftype) {
            case ComptonAsymmetry::A1 : 
              return A1(target,x,Q2);
              break;

            case ComptonAsymmetry::A2 : 
              return A2(target,x,Q2);
              break;

            default:
              return 0.0;
              break;
          }
          return 0.0;
        }

        virtual double A1(    Nuclei target, double x, double Q2, Twist t = Twist::All) const
        {
          switch(target) {
            case Nuclei::p : 
              return A1p(x,Q2);
              break;

            case Nuclei::n : 
              return A1n(x,Q2);
              break;

            default:
              return 0.0;
              break;
          }
          return 0.0;
        }

        virtual double A2(    Nuclei target, double x, double Q2, Twist t = Twist::All) const
        {
          switch(target) {
            case Nuclei::p : 
              return A2p(x,Q2);
              break;

            case Nuclei::n : 
              return A2n(x,Q2);
              break;

            default:
              return 0.0;
              break;
          }
          return 0.0;
        }

        virtual double A1_NoTMC(Nuclei target, double x, double Q2, Twist t = Twist::All) const
        {
          switch(target) {
            case Nuclei::p : 
              return A1p_NoTMC(x,Q2);
              break;

            case Nuclei::n : 
              return A1n_NoTMC(x,Q2);
              break;

            default:
              return 0.0;
              break;
          }
          return 0.0;
        }

        virtual double A2_NoTMC(Nuclei target, double x, double Q2, Twist t = Twist::All) const
        {
          switch(target) {
            case Nuclei::p : 
              return A2p_NoTMC(x,Q2);
              break;

            case Nuclei::n : 
              return A2n_NoTMC(x,Q2);
              break;

            default:
              return 0.0;
              break;
          }
          return 0.0;
        }

        virtual double A1p(double x, double Q2) const {std::cout << "Error";  return 0.0;}
        virtual double A2p(double x, double Q2) const {std::cout << "Error";  return 0.0;}
        virtual double A1n(double x, double Q2) const {std::cout << "Error";  return 0.0;}
        virtual double A2n(double x, double Q2) const {std::cout << "Error";  return 0.0;}
        virtual double A1p_NoTMC(double x, double Q2) const {std::cout << "Error"; return 0.0;}
        virtual double A2p_NoTMC(double x, double Q2) const {std::cout << "Error"; return 0.0;}
        virtual double A1n_NoTMC(double x, double Q2) const {std::cout << "Error"; return 0.0;}
        virtual double A2n_NoTMC(double x, double Q2) const {std::cout << "Error"; return 0.0;}

        /** Soffer-Teryaev  Positivity bound.
         * \f$ |A_2| < \sqrt{R(1+A_1)/2} \f$
         */
        virtual double A2_PositivityBound(Nuclei target, double x, double Q2) const 
        {
          // Soffer-Teryaev  Positivity bound.
          // |A_2| < \sqrt{R(1+A_1)/2}
          using namespace TMath;
          double Rover2 = R(target, x,Q2)/2.0;
          double result = Sqrt(Abs(Rover2*(1.0+A1(target, x,Q2))));
          return result;
        }

        virtual double R(Nuclei target, double x, double Q2) const { return insane::physics::R1998(x,Q2); }

        ClassDef(VirtualComptonScatteringAsymmetries,1)
    };
  }
}

//#include "VirtualComptonScatteringAsymmetries.hxx"

#endif

