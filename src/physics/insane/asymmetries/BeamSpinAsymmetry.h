#ifndef BeamSpinAsymmetry_HH
#define BeamSpinAsymmetry_HH 1
#include <iostream>

#include "TVector3.h"
#include "TRotation.h"

#include "insane/formfactors/DipoleFormFactors.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "insane/structurefunctions/F1F209StructureFunctions.h"
#include "insane/pdfs/AAC08PolarizedPDFs.h"
#include "insane/pdfs/DSSVPolarizedPDFs.h"
#include "insane/pdfs/BBPolarizedPDFs.h"
#include "insane/pdfs/GSPolarizedPDFs.h"
#include "insane/pdfs/DNS2005PolarizedPDFs.h"
#include "insane/pdfs/LSS2006PolarizedPDFs.h"

namespace insane {
namespace physics {

/** Beam spin asymmetry calculated from form factors, structure functions, and kinematic functions.
 *  Useful for calculating a helicity dependent cross section.
 */
class BeamSpinAsymmetry {
   public:
      BeamSpinAsymmetry(): fAsymmetry(0.0) {
         fFormFactors = new DipoleFormFactors();
      }
      virtual ~BeamSpinAsymmetry() {}

      virtual Double_t Calculate() = 0;

      Double_t GetAsymmetry() {
         return fAsymmetry;
      }

   protected:
      Double_t fAsymmetry;

      FormFactors * fFormFactors;

      ClassDef(BeamSpinAsymmetry, 1)
};



/** Beam-target elastic scattering asymmetry which is sensitive to the ratio of GE/GM.
 *  Following the angle convention of Donnelly and Raskin. See ::Kine functions.
 */
class BeamTargetAsymmetry : public BeamSpinAsymmetry {
   public:
      BeamTargetAsymmetry() {
         ff1 = TMath::Sqrt(1.0 / 2.0);
         fBeamEnergy = 5.9;
         fTheta_e = 0.0;
         fPhi_e = 0.0;
         fTheta_star = 0.0;
         fPhi_star = 0.0;
         fTheta_targ = TMath::Pi();
         fPhi_targ = 0.0;
      }
      virtual ~BeamTargetAsymmetry() {}

      void SetThetaPhiStar(double thstar, double phistar) {
         fTheta_star = thstar;
         fPhi_star = phistar;
      }
      void SetEPrimeThetaPhi(double eprime, double theta, double phi) {
         fEPrime = eprime;
         fTheta_e = theta;
         fPhi_e = phi;
      }
      void SetBeamEnergy(double ebeam) {
         fBeamEnergy = ebeam;
      }
      void SetThetaPhiTarget(double th, double phi) {
         fTheta_targ = th;
         fPhi_targ = phi;
      }

      /** Get the angle between q-vector and target polarization vector */
      Double_t GetThetaStar() {
         return fTheta_star;
      }
      /** Get the angle between q-vector and target polarization vector */
      Double_t GetPhiStar() {
         return fPhi_star;
      }
      Double_t GetEPrime() {
         return fEPrime;
      }
      Double_t GetTheta() {
         return fTheta_e;
      }
      Double_t GetPhi() {
         return fPhi_e;
      }
      Double_t GetBeamEnergy() {
         return fBeamEnergy;
      }
      Double_t GetTau() {
         return fTau;
      }
      Double_t GetQsquared() {
         return fQsquared;
      }
      Double_t GetqAbs() {
         return fq_abs;
      }
      Double_t GetqVecTheta() {
         return fTheta_q;
      }
      Double_t GetqVecPhi() {
         return fPhi_q;
      }
      Double_t GetThetaTarget() {
         return fTheta_targ;
      }
      Double_t GetPhiTarget() {
         return fPhi_targ;
      }

      virtual void Print() {
         std::cout << " A            = " << GetAsymmetry() << "\n";
         std::cout << " Qsquared     = " << GetQsquared() << "\n";
         std::cout << " q_abs        = " << GetqAbs() << "\n";
         std::cout << " tau          = " << GetTau() << "\n";
         std::cout << " energy_beam  = " << GetBeamEnergy() << "\n";
         std::cout << " E_prime      = " << GetEPrime() << "\n";
         std::cout << " theta_e      = " << GetTheta() << "\n";
         std::cout << " phi_e        = " << GetPhi() << "\n";
         std::cout << " theta_q      = " << GetqVecTheta() << "\n";
         std::cout << " phi_q        = " << GetqVecPhi() << "\n";
         std::cout << " theta_targ   = " << GetThetaTarget() << "\n";
         std::cout << " phi_targ     = " << GetPhiTarget() << "\n";
         std::cout << " theta_star   = " << GetThetaStar() << "\n";
         std::cout << " phi_star     = " << GetPhiStar() << "\n";
      }

   protected:

      void CalculateStarAngles() {
         TVector3 k(0.0, 0.0, GetBeamEnergy());
         TVector3 k2(0.0, 0.0, 0.0);
         k2.SetMagThetaPhi(GetEPrime(), GetTheta(), GetPhi());
         TVector3 q = k - k2;
         TVector3 uy = k.Cross(k2);
         uy.SetMag(1.0);
         fTheta_q = q.Theta();
         fPhi_q = q.Phi();
         TRotation rot_to_q;
         rot_to_q.SetToIdentity();
         rot_to_q.Rotate(-1.0 *(q.Theta()), uy);
         /*      rot_to_q.SetXEulerAngles(q.Phi(),q.Theta(),0.0);*/
         TVector3 P(0.0, 0.0, 0.0);
         P.SetMagThetaPhi(1.0, GetThetaTarget(), GetPhiTarget());
         P.Transform(rot_to_q);
         fTheta_star = P.Theta();
         fPhi_star = P.Phi();
      }

      /// Angle between q-vector and target polarization vector
      Double_t fTheta_star;
      /// Angle between q-vector and target polarization vector
      Double_t fPhi_star;

      Double_t fBeamEnergy;
      Double_t fEPrime;
      Double_t fTheta_e;
      Double_t fPhi_e;

      Double_t fTheta_q;
      Double_t fPhi_q;


      /// Target polarization angles in lab frame
      Double_t fTheta_targ;
      Double_t fPhi_targ;

      ///rank-1 Fano tensor for spin-1/2 target
      Double_t ff1;

      Double_t fQsquared;
      Double_t fTau;
      Double_t fq_abs;

   public:
      virtual Double_t Calculate() {
         using namespace insane::Kine;
         using namespace TMath;

         CalculateStarAngles();

         fQsquared = Qsquared(fBeamEnergy, fEPrime, fTheta_e);
         fTau = tau(fQsquared);
         fq_abs = q_abs(fQsquared);

         fAsymmetry = (-2 * ff1 * fFormFactors->GMp(fQsquared) *
               (-2 * Cos(fPhi_star) * fFormFactors->GEp(fQsquared) *
                Sin(fTheta_star) * Sqrt(fTau * (1 + fTau)) *
                vTLprime(fQsquared, fq_abs, fTheta_e) +
                Sqrt(2) * Cos(fTheta_star) * fFormFactors->GMp(fQsquared) * fTau *
                vTprime(fQsquared, fq_abs, fTheta_e))) /
            (Power(fFormFactors->GEp(fQsquared), 2) * (1 + fTau) *
             vL(fQsquared, fq_abs) +
             2 * Power(fFormFactors->GMp(fQsquared), 2) * fTau *
             vT(fQsquared, fq_abs, fTheta_e));
         return(fAsymmetry);
      }

   public:
      /** @name Useful functions for plotting...
       *  @{
       */

      /** Asymmetry as a function of energy.
       *  \param x[0] = energy
       *  \param p[0] = theta
       *  \param p[1] = phi
       */
      double EnergyDependentAsymmetry(double *x, double *p) {
         SetEPrimeThetaPhi(x[0], p[0], p[1]);
         return(Calculate());
      }

      /** Asymmetry as a function of momentum.
       *  \param x[0] = momentum
       *  \param p[0] = theta
       *  \param p[1] = phi
       */
      double MomentumDependentAsymmetry(double *x, double *p) {
         SetEPrimeThetaPhi(TMath::Sqrt(x[0]*x[0] + 0.000511 * 0.000511), p[0], p[1]);
         return(Calculate());
      }

      /** Asymmetry as a function of polar angle .
       *  \param x[0] = theta
       *  \param p[0] = energy
       *  \param p[1] = phi
       */
      double PolarAngleDependentAsymmetry(double *x, double *p) {
         SetEPrimeThetaPhi(p[0], x[0], p[1]);
         return(Calculate());
      }
      /** Asymmetry as a function of polar angle .
       *  \param x[0] = phi
       *  \param p[0] = energy
       *  \param p[1] = theta
       */
      double AzimuthalAngleDependentAsymmetry(double *x, double *p) {
         SetEPrimeThetaPhi(p[0], p[1], x[0]);
         return(Calculate());
      }

      //@}

      ClassDef(BeamTargetAsymmetry, 1)
};



/** Polarized DIS scattering asymmetry for polarized target and beam.
 */
class PolarizedDISAsymmetry : public BeamTargetAsymmetry {
   public:
      PolarizedDISAsymmetry() {
         fUnpolSFs = new F1F209StructureFunctions();
         fPolSFs = new PolarizedStructureFunctionsFromPDFs();
         ((PolarizedStructureFunctionsFromPDFs*)fPolSFs)->SetPolarizedPDFs(new BBPolarizedPDFs);
      }
      virtual ~PolarizedDISAsymmetry() {}

      /** Returns the bjorken scaling variable. */
      Double_t GetxBjorken() {
         return(fx);
      }
      /** Returns the bjorken scaling variable. */
      Double_t Getx() {
         return(fx);
      }

   protected:
      /// Bjorken scaling variable.
      Double_t fx;
      Double_t  fG1;
      Double_t  fG2;
      Double_t  fW1;
      Double_t  fW2;

   public:

      virtual Double_t Calculate() {
         using namespace insane::Kine;
         using namespace TMath;

         CalculateStarAngles();

         fx = xBjorken(fQsquared, nu(fBeamEnergy, fEPrime));
         fQsquared = Qsquared(fBeamEnergy, fEPrime, fTheta_e);
         fTau = tau(fQsquared);
         fq_abs = q_abs(fQsquared);
         Double_t M = M_p/GeV;//fUnpolSFs->M_p/GeV;

         if (fx > 0.0 && fx < 1.0) {
            fG1 = fPolSFs->G1p(fx, fQsquared);
            fG2 = fPolSFs->G2p(fx, fQsquared);
            fW1 = fUnpolSFs->W1p(fx, fQsquared);
            fW2 = fUnpolSFs->W2p(fx, fQsquared);
         } else {
            fG1 = 0.0;
            fG2 = 0.0;
            fW1 = 0.00001;
            fW2 = 0.00001;
         }


         fAsymmetry = (-2 * fQsquared * (Cos(fTheta_targ) * (-2 * fBeamEnergy * fEPrime * fG2 + fBeamEnergy * fG1 * M +
                     fEPrime * (2 * fBeamEnergy * fG2 + fG1 * M) * Cos(fTheta_e)) +
                  fEPrime * (2 * fBeamEnergy * fG2 + fG1 * M) * Cos(fPhi_targ - fPhi_e) * Sin(fTheta_targ) *
                  Sin(fTheta_e))) /
            (fQsquared * (2 * fW1 - fW2) - 4 * (Power(fBeamEnergy, 2)
                                                - 3 * fBeamEnergy * fEPrime
                                                + Power(fEPrime, 2)) * fW2);
         return(fAsymmetry);
      }

   protected:

      PolarizedStructureFunctions * fPolSFs;
      StructureFunctions *          fUnpolSFs;

      ClassDef(PolarizedDISAsymmetry, 1)
};

}
}

#endif

