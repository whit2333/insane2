#ifndef insane_physics_VCSAsFromSFs_HHXX
#define insane_physics_VCSAsFromSFs_HHXX

// ------------------------------------------
namespace insane {
  namespace physics {

    template<class T1, class T2> 
    double  VCSAsFromSFs<T1,T2>::Calculate(double x, double Q2, std::tuple<ComptonAsymmetry,Nuclei> sf, Twist t, OPELimit l) const
    {
      auto sftype = std::get<ComptonAsymmetry>(sf);
      auto target = std::get<Nuclei>(sf);
      bool use_tmc = true;
      if(l == OPELimit::MasslessTarget) {
        use_tmc = false;
      }

      //constexpr int n_HT   = sizeof...(HT);
      double        result = 0.0;

      switch(sftype) {

        case ComptonAsymmetry::A1 :
          if(use_tmc) {
            result = insane::physics::A1_SFs(fSFs, fSSFs,target, x, Q2); 
          } else {
            result = insane::physics::A1_NoTMC_SFs(fSFs, fSSFs,target, x, Q2); 
          }
          //std::cout << " A1 = " << result << std::endl;
          break;

        case ComptonAsymmetry::A2 :
          if(use_tmc) {
            result = insane::physics::A2_SFs(fSFs, fSSFs,target, x, Q2); 
          } else {
            result = insane::physics::A2_NoTMC_SFs(fSFs, fSSFs,target, x, Q2); 
          }
          //std::cout << " A2 = " << result << std::endl;
          break;

        default:
          result =  0.0;
          break;

      }
      return result;
    }
    //______________________________________________________________________________

    template<class T1, class T2> 
    double   VCSAsFromSFs<T1,T2>::Uncertainties(double x, double Q2, std::tuple<ComptonAsymmetry,Nuclei> sf, Twist t, OPELimit l) const
    {
      return 0.0;
    }
    //______________________________________________________________________________
    
    //template<class T1, class T2> 
    //double VCSAsFromSFs<T1,T2>::R(Nuclei target, double x, double Q2) const
    //{
    //  return fSFs.R(x,Q2,target);
    //}


  }
}

#endif

