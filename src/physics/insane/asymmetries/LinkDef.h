#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;


#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;

#pragma link C++ class insane::physics::VCSABase+;
#pragma link C++ class insane::physics::VirtualComptonAsymmetries+;
#pragma link C++ class insane::physics::VirtualComptonAsymmetriesModel1+;

#pragma link C++ class insane::physics::AsymmetryBase+;
#pragma link C++ class insane::physics::AsymmetriesFromStructureFunctions+;
#pragma link C++ class insane::physics::VirtualComptonScatteringAsymmetries+;
//#pragma link C++ class insane::physics::VCSAsFromSFs<insane::physics::F1F209_SFs,insane::physics::SpinStructureFunctions>+;

#pragma link C++ class insane::physics::VCSAsFromSFs<insane::physics::SFsFromPDFs<Stat2015_UPDFs>, insane::physics::SSFsFromPDFs<Stat2015_PPDFs>>+;
#pragma link C++ class insane::physics::VCSAsFromSFs<insane::physics::F1F209_SFs, insane::physics::SSFsFromPDFs<Stat2015_PPDFs>>+;
#pragma link C++ class insane::physics::VCSAsFromSFs<insane::physics::NMC95_SFs, insane::physics::SSFsFromPDFs<Stat2015_PPDFs>>+;
//#pragma link C++ class insane::physics::VCSAsFromPDFs<insane::physics::Stat2015_UPDFs, insane::physics::Stat2015_PPDFs>+;
#pragma link C++ class insane::physics::MAID_VCSAs+;
#pragma link C++ class insane::physics::MAIDVirtualComptonAsymmetries+;

//#pragma link C++ class std::map<int, double >+;
//#pragma link C++ class std::pair<int, double >+;
//#pragma link C++ class std::vector<int, std::pair<int, double > >+;
#pragma link C++ class insane::physics::A1A2Model1+;
#pragma link C++ class insane::physics::RSSAsymmetryFits+;
#pragma link C++ class insane::physics::RSSAsymmetryFits2+;

#endif

