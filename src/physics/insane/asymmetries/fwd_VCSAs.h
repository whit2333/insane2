#ifndef insane_physics_fwd_VCSAs_HH
#define insane_physics_fwd_VCSAs_HH 1

#include "insane/structurefunctions/SSFsFromPDFs.h"
#include "insane/structurefunctions/SFsFromPDFs.h"
#include "insane/structurefunctions/fwd_SFs.h"

#include "insane/asymmetries/VCSAsFromPDFs.h"
#include "insane/asymmetries/VCSAsFromSFs.h"

#include "insane/structurefunctions/SFsFromVPACs.h"
#include "insane/structurefunctions/SSFsFromVPACs.h"
#include "insane/structurefunctions/F1F209_SFs.h"
#include "insane/xsections/MAID_VPACs.h"

namespace insane {
  namespace physics {

    /** Structure function aliases. 
     *
     * These aliases mirror those in structurefunctions/include/LinkDef.h 
     *
     */

    /** @name Virtual Compton Scattering Asymmtries.
     *  Some alises for VCSAs.
     */
    //@{
    using Stat2015_VCSAs =
        VCSAsFromSFs<insane::physics::SFsFromPDFs<insane::physics::Stat2015_UPDFs>,
                     insane::physics::SSFsFromPDFs<insane::physics::Stat2015_PPDFs>>;

    using F1F209_JAM_VCSAs = insane::physics::VCSAsFromSFs<
        insane::physics::F1F209_SFs,
        insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs, insane::physics::JAM_T3DFs,
                                      insane::physics::JAM_T4DFs>>;
    using CTEQ10_JAM_VCSAs = insane::physics::VCSAsFromSFs<
        insane::physics::SFsFromPDFs<insane::physics::CTEQ10_UPDFs>,
        insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs, insane::physics::JAM_T3DFs,
                                      insane::physics::JAM_T4DFs>>;
    //@}


  }
}

#endif

