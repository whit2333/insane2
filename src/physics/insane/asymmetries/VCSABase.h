#ifndef INSANE_PHYSICS_VCSABase_HH
#define INSANE_PHYSICS_VCSABase_HH 1

//#include "TNamed.h"
//#include "TAttLine.h"
//#include "TAttMarker.h"
//#include "TAttFill.h"
//#include <array>
//#include <string>
//#include <tuple>
#include "insane/asymmetries/VirtualComptonAsymmetries.h"

//namespace insane {
//  namespace physics {
//
//    class VCSABase : public TNamed , public virtual TAttLine, public virtual TAttFill, public virtual TAttMarker {
//
//      protected:
//        std::string      fLabel;
//
//        // Storage of the x and Q2 values last used to compute the structure function
//        // indexed by enum class insane::physics::SF
//        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fx_values;
//        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fQ2_values;
//        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fValues;
//        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fUncertainties;
//
//      public :
//
//        VCSABase(){}
//        virtual ~VCSABase(){}
//
//        void        SetLabel(const char * l){ fLabel = l;}
//        const char* GetLabel() const { return fLabel.c_str(); }
//
//        double GetXBjorken(std::tuple<SF,Nuclei> sf) const
//        {
//          auto f  = static_cast<int>(std::get<SF>(sf));
//          auto n  = static_cast<int>(std::get<Nuclei>(sf));
//          return(fx_values.at(n).at(f));
//        }
//        //______________________________________________________________________________
//
//        double GetQSquared(std::tuple<SF,Nuclei> sf) const
//        {
//          auto f  = static_cast<int>(std::get<SF>(sf));
//          auto n  = static_cast<int>(std::get<Nuclei>(sf));
//          return(fQ2_values.at(n).at(f));
//        }
//        //______________________________________________________________________________
//
//        bool   IsComputed( std::tuple<SF,Nuclei> sf, double x, double Q2) const
//        {
//          auto f  = static_cast<int>(std::get<SF>(sf));
//          auto n  = static_cast<int>(std::get<Nuclei>(sf));
//          if(  fx_values.at(n).at(f) != x ) return false;
//          if( fQ2_values.at(n).at(f) != Q2 ) return false;
//          return true;
//        }
//        //______________________________________________________________________________
//
//        void Reset()
//        {
//          for(int n = 0; n<NNuclei;  n++) {
//            for(int i = 0; i<NStructureFunctions ; i++) {
//              fx_values[n][i]        = 0.0;
//              fQ2_values[n][i]       = 0.0;
//              fValues[n][i]          = 0.0;
//              fUncertainties[n][i]   = 0.0;
//            }
//          }
//        }
//        //______________________________________________________________________________
//
//        /** Calculate and return distribution value.
//         * Returns the current value for flavor f but checks that the 
//         * distributions have already been calculated at (x,Q2). 
//         * It uses IsComputed(x,Q2) to do this check.
//         */
//        double  Get(std::tuple<SF,Nuclei> sf, double x, double Q2) const 
//        {
//          return 0.0;
//        }
//        //______________________________________________________________________________
//
//        /** Get current distribution value.
//         * Note: this method should only be used to get the stored values 
//         * after Calculate(x,Q2) or Get(f,x,Q2) has been used at the desired (x,Q2).
//         */ 
//        double  Get(std::tuple<SF,Nuclei> sf) const {
//          return 0.0;
//        }
//
//        /** Virtual method should get all values of SFs. This also sets
//         *  internal values of fx and fQsquared for use by IsComputed()
//         */
//        virtual double  Calculate    (double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const {return 0.0;}
//        virtual double  Uncertainties(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const {return 0.0;}
//
//        ClassDef(VCSABase,1)
//    };
//
//
//  }
//}

//#include "VCSABase.hxx"

#endif

