#ifndef MAIDVirtualComptonAsymmetries_H 
#define MAIDVirtualComptonAsymmetries_H 

#include <iostream> 
#include "insane/asymmetries/VirtualComptonAsymmetries.h"
#include "insane/pdfs/FortranWrappers.h"

namespace insane {
  namespace physics {

    /** MAID Compton asymmetries.
     * 
     * Paper reference: J. Phys. G: Nucl. Part. Phys 18, 449 (1992)  
     * Uses the total cross sections for each channel and sums. 
     *
     * M A I D  2007    (18.05.2007)
     * D. Drechsel, S.S. Kamalov, L. Tiator
     * Institut fuer Kernphysik, Universitaet Mainz
     * 
     *  sigT=1/2(sig_1/2 + sig_3/2), sigTT'=1/2(sig_3/2 - sig_1/2)
     *  sigL0=sigL*(wcm/Q)**2, sigLT0=sigLT*(wcm/Q), sigLT0'=sigLT'*(wcm/Q)
     *
     * \ingroup physics  
     */ 
    class MAIDVirtualComptonAsymmetries : public VirtualComptonAsymmetries {

    private:
      double fSigT;
      double fSigL;
      double fSigLT;
      double fSigLTp;
      double fSigTTp;
      double fSigL0;
      double fSigLT0;
      double fSigLT0p;

    public: 
      MAIDVirtualComptonAsymmetries();
      virtual ~MAIDVirtualComptonAsymmetries();

      virtual Double_t A1p(   Double_t x, Double_t Q2);
      virtual Double_t A2p(   Double_t x, Double_t Q2);
      virtual Double_t A1n(   Double_t x, Double_t Q2);
      virtual Double_t A2n(   Double_t x, Double_t Q2);
      //virtual Double_t A1d(   Double_t x, Double_t Q2);
      //virtual Double_t A2d(   Double_t x, Double_t Q2);
      //virtual Double_t A1He3( Double_t x, Double_t Q2);
      //virtual Double_t A2He3( Double_t x, Double_t Q2);

      //virtual Double_t A1p_Error(   Double_t x, Double_t Q2);
      //virtual Double_t A2p_Error(   Double_t x, Double_t Q2);
      //virtual Double_t A1n_Error(   Double_t x, Double_t Q2);
      //virtual Double_t A2n_Error(   Double_t x, Double_t Q2);
      //virtual Double_t A1d_Error(   Double_t x, Double_t Q2);
      //virtual Double_t A2d_Error(   Double_t x, Double_t Q2);
      //virtual Double_t A1He3_Error( Double_t x, Double_t Q2);
      //virtual Double_t A2He3_Error( Double_t x, Double_t Q2);


      ClassDef(MAIDVirtualComptonAsymmetries,1)
    };
  }}
#endif 
