#ifndef RSSAsymmetryFits2_HH
#define RSSAsymmetryFits2_HH

#include <array>
#include <iostream>
#include "TMath.h"
#include "insane/base/PhysicalConstants.h"

namespace insane {
   namespace physics {

      class RSSAsymmetryFits2 {

         protected:

            double               A1_alpha =  0.031;
            double               A2_alpha =  0.458;
            std::array<double,4> A1_beta  = {{ 0.186, -0.032, -0.393, 0.957 }};
            std::array<double,4> A2_beta  = {{ 0.100, 0.094, -0.119, -0.025 }};

            // Parameters from Stein et.al., Table IX
            std::array<double,4> L_res = {{    1.0,    2.0,    3.0,    3.0 }};
            std::array<double,4> J_res = {{    1.0,    1.0,    2.0,    2.0 }};
            std::array<double,4> X_res = {{  0.160,  0.350,  0.350,  0.350 }};
            std::array<double,4> M_res = {{ 1236.0, 1520.0, 1688.0, 1950.0 }};

            // RSS parameters
            // A1 Parameters
            std::array<double,4> a_rss = {{-0.562, 0.401, 0.537, 0.246 }}; // amplitude
            std::array<double,4> w_rss = {{ 1.204, 1.345, 1.544, 1.734 }}; // mass
            std::array<double,4> g_rss = {{ 0.161, 0.063, 0.283, 0.125 }}; // width

            // fit to Q2 dependence of data in Stein, table XI
            // These values are the b's in a fit of the form a*exp(b*Q2)
            std::array<double,4> q2_dep = {{-1.46603, -1.17034, -0.921664, -1.59678}};

            // A2 Parameters
            std::array<double,4> a2_rss = {{-0.147, 0.220, 0.158, 0.173 }}; // amplitude
            std::array<double,4> w2_rss = {{ 1.214, 1.338, 1.479, 1.653 }}; // mass
            std::array<double,4> g2_rss = {{ 0.137, 0.071, 0.113, 0.129 }}; // width

            double m_pi  = insane::units::M_pi0/insane::units::GeV;
            double M     = insane::units::M_p/insane::units::GeV;

            double q_cm(double W) const ;
            double kappa_cm( double Q2, double W) const ;

            double q_i(    int i, double Q2) const ;
            double kappa_i(int i, double Q2) const ;
            double q2_i(    int i, double Q2) const ;
            double kappa2_i(int i, double Q2) const ;

            double Gamma_i(int i, double Q2, double W) const ;
            double Gamma_gamma_i(int i, double Q2, double W) const ;
            double Gamma2_i(int i, double Q2, double W) const ;
            double Gamma2_gamma_i(int i, double Q2, double W) const ;

            double BW( int i, double Q2, double W) const ;
            double BW2(int i, double Q2, double W) const ;


         public:
            RSSAsymmetryFits2();
            virtual ~RSSAsymmetryFits2();

            void SetA1Amplitudes(const std::array<double,4>& A1_amps);
            void SetA2Amplitudes(const std::array<double,4>& A2_amps);

            std::array<double,4> GetA1Amplitudes() const { return a_rss;  }
            std::array<double,4> GetA2Amplitudes() const { return a2_rss; }

            double A1_fit(double* x, double* pars) const ;
            double A1_fit_x(double* x, double* pars) const ;
            double A1_x(double x, double Q2) const ;
            double A1_W(double W, double Q2) const ;

            double A2_fit(double* x, double* pars) const ;
            double A2_fit_x(double* x, double* pars) const ;
            double A2_x(double x, double Q2) const ;
            double A2_W(double W, double Q2) const ;

            ClassDef(RSSAsymmetryFits2,1)

      };

   }

}

#endif

