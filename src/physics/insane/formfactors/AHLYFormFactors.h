#ifndef AHLYFormFactors_HH
#define AHLYFormFactors_HH 1

#include "insane/formfactors/DipoleFormFactors.h"

namespace insane {
  namespace physics {


    /** AHLY fit to electric and magnetic form factors for the proton.
     *  
     *  This model first corrects the measurements with two photon exchange corrections
     *  then fits the data.
     *
     *  \ingroup formfactors
     */
    class AHLYFormFactors : public DipoleFormFactors {

    protected:

    public:
      AHLYFormFactors() ;
      virtual ~AHLYFormFactors();

      /** \f$ G_{Ep} = \frac{1+\sum_{i=1}^{n} a_i \tau^i }{1+\sum_{i=1}^{n+2} b_i \tau^i} \f$
       *   where \f$ \tau= Q^2/4M^2 \f$
       */
      virtual double GEp(double Qsq) const ;

      /** \f$ G_{Mp}/\mu_{p} = \frac{1+\sum_{i=1}^{n} a_i \tau^i }{1+\sum_{i=1}^{n+2} b_i \tau^i} \f$ */
      virtual double GMp(double Qsq) const ;

      virtual double GEn(double Qsq) ;
      virtual double GMn(double Qsq) ;

      virtual double GEp_unc(double Qsq) ;
      virtual double GMp_unc(double Qsq) ;
      virtual double GEn_unc(double Qsq) ;
      virtual double GMn_unc(double Qsq) ;

    public:

      /** Method provided by Zhihong Ye.
       *
       *  Zhihong Ye, John R. Arrington, Richard J. Hill, and Gabriel Lee
       *  To be published in PLB. 
       *
       *  Parameterized Form Factor Central Value and Error
       * 
       *  ID = 1 for GEp, 2 for GMp, 3 for GEn, 4 for GMn, 
       *  Q2 in GeV^2
       * 
       *  The parameterization formula returns the uncertainty devided by G(0)*GD, where 
       *   GD(Q2) = 1./(1+Q2/0.71)^2
       *  and GEp(0) = 1, GMp(0) = 2.79284356, GEn(0) = 1, GMn(0) = -1.91304272,
       * 
       *  The parameterization formula for the Form Factor value is:
       *   $$ GN(z) = sum_{i=0}^{N=12}(a_i * z^i) 
       *  Note that the return value has been divided by (G(Q2=0)*G_Dip)
       * 
       *  The parameterization formula for the Form Factor error is:
       *  $$ log_{10}\frac{\delta G}{G_D} = (L+c_0)\Theta_a(L_1-L) 
       *                                  +\sum_{i=1}^{N}(c_i+d_i L)[\Theta_a(L_i-L)-\Theta_a(L_{i+1}-L)]
       *                                  +log_{10}(E_{\inf})\Theta_a(L-L_{N+1})$$
       *  where $L=log_{10}(Q^2)$, $\Theta_{a}(x)=[1+10^{-ax}]^{-1}$. $a=1$.
       */
      int GetFF(const int kID, const double kQ2, double& GNGD_Fit, double& GNGD_Err) const;

      ClassDef(AHLYFormFactors, 1)
    };
  }
}

#endif

