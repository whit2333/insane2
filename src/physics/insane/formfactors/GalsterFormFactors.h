#ifndef GalsterFormFactors_HH
#define GalsterFormFactors_HH 1

#include "insane/formfactors/DipoleFormFactors.h"

namespace insane {
namespace physics {

/** Galster fit to the electric form factor for the neutron. 
 *  Nucl. Phys. B 32, 221–237 (1971) 
 *
 *  \ingroup formfactors  
 */
class GalsterFormFactors: public DipoleFormFactors{

   private:
      Double_t a_GE[2]; 

   public: 
      GalsterFormFactors(){
         SetNameTitle("GlasterFormFactors","Glaster Form Factors");
         SetLabel("Glaster FFs");
         a_GE[0] = Mu_n; 
         a_GE[1] = 5.6;  
      }

      ~GalsterFormFactors(){}

      virtual Double_t GEn(Double_t Q2){
         Double_t M   = M_n/GeV;  
         Double_t tau = Q2/(4.*M*M); 
         Double_t num = a_GE[0]*tau; 
         Double_t den = 1. + a_GE[1]*tau; 
         Double_t T1  = (-1.0)*num/den;
         Double_t T2  = DipoleFormFactor(Q2);
         Double_t GE  = T1*T2;   
         return GE; 
      }

      /// Don't define the others -- just utilize the dipole form... 
      // virtual Double_t GMn(Double_t Q2){return 0;}   
      // virtual Double_t GEp(Double_t Q2){return 0;} 
      // virtual Double_t GMp(Double_t Q2){return 0;} 
      // virtual Double_t GEd(Double_t Q2){return 0;} 
      // virtual Double_t GMd(Double_t Q2){return 0;} 
      // virtual Double_t GEHe3(Double_t Q2){return 0;} 
      // virtual Double_t GMHe3(Double_t Q2){return 0;} 


      ClassDef(GalsterFormFactors,1)
}; 
}
}

#endif

