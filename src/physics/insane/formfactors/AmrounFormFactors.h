#ifndef AmrounFormFactors_H 
#define AmrounFormFactors_H 

#include <cstdlib> 
#include <iostream> 
#include "insane/formfactors/DipoleFormFactors.h"
#include "insane/pdfs/FortranWrappers.h"

namespace insane {
namespace physics {
/** 3He form factor fit from Amroun et al. 
 * Paper reference: Nucl. Phys. A579, 596-626 (1994)
 * A switch is included to use the original fortran code...
 *
 * \ingroup formfactors 
 */ 
class AmrounFormFactors: public DipoleFormFactors {

   private:
      bool fIsFortran;                    /// switch to use the fortran version of the code (default = false) 
      Double_t fA,fZ;  
      Double_t fR[12],fQCh[12],fQM[12]; 
      Double_t GetFormFactor(Int_t,Double_t);

   public: 
      AmrounFormFactors();
      ~AmrounFormFactors(); 

      void UseFortran(bool t){fIsFortran = t;} 

      //Double_t GEp(Double_t Q2){return 0;}
      //Double_t GMp(Double_t Q2){return 0;}
      //Double_t GEn(Double_t Q2){return 0;}
      //Double_t GMn(Double_t Q2){return 0;}
      //Double_t GEd(Double_t Q2){return 0;}
      //Double_t GMd(Double_t Q2){return 0;}
      Double_t GEHe3(Double_t);
      Double_t GMHe3(Double_t);

      ClassDef(AmrounFormFactors,1)


};

}
}

#endif 
