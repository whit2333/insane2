#ifndef insane_physics_fwd_FFs_HH
#define insane_physics_fwd_FFs_HH

#include "insane/formfactors/DipoleFormFactors.h"
#include "insane/formfactors/GalsterFormFactors.h"
#include "insane/formfactors/AHLYFormFactors.h"
#include "insane/formfactors/AMTFormFactors.h"
#include "insane/formfactors/AmrounFormFactors.h"
#include "insane/formfactors/BilenkayaFormFactors.h"
#include "insane/formfactors/BostedFormFactors.h"
#include "insane/formfactors/KellyFormFactors.h"
#include "insane/formfactors/MSWFormFactors.h"
#include "insane/formfactors/RiordanFormFactors.h"
//#include "F1F209QuasiElasticFormFactors.h"


namespace insane {
  namespace physics {

    using AMT_FFs_t   =  insane::physics::AMTFormFactors;
    using AHLY_FFs_t  =  insane::physics::AHLYFormFactors;
    using Kelly_FFs_t =  insane::physics::KellyFormFactors;

  }
}
#endif

