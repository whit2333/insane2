#ifndef AMTFormFactors_HH
#define AMTFormFactors_HH 1

#include "insane/formfactors/FormFactors.h"
#include "insane/formfactors/DipoleFormFactors.h"

namespace insane::physics {

  /** AMT fit to electric and magnetic form factors for the proton.
   *  Phys. Rev. C 76, 035205 (2007)
   *  This model first corrects the measurements with two photon exchange corrections
   *  then fits the data.
   *
   *  \ingroup Formfactors
   */
  class AMTFormFactors : public DipoleFormFactors {

  protected:
    double a_GE[3];
    double b_GE[5];
    double a_GM_over_Mu[3];
    double b_GM_over_Mu[5];

  public:
    AMTFormFactors();
    virtual ~AMTFormFactors();

    /** \f$ G_{Ep} = \frac{1+\sum_{i=1}^{n} a_i \tau^i }{1+\sum_{i=1}^{n+2} b_i \tau^i} \f$
     *   where \f$ \tau= Q^2/4M^2 \f$
     */
    virtual double GEp(double Qsq) const;

    /** \f$ G_{Mp}/\mu_{p} = \frac{1+\sum_{i=1}^{n} a_i \tau^i }{1+\sum_{i=1}^{n+2} b_i \tau^i}
     * \f$ */
    virtual double GMp(double Qsq) const;

    ClassDef(AMTFormFactors, 1)
  };
} // namespace insane::physics

#endif
