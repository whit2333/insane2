#ifndef insane_physics_FormFactors_HH 
#define insane_physics_FormFactors_HH 1

#include "TNamed.h"
#include "TAttLine.h"
#include "TAttMarker.h"
#include "TAttFill.h"
#include "insane/base/Physics.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "TMath.h"

/** \defgroup FormFactors Form Factors 
 *
 * Electromagnetic Form Foctors of the nucleon.
 *
 * <h2>Proton and Neutron Form Factors</h2>
 *
 * Dirac and Pauli form factors: 
 * \f[ F_1 = \frac{G_E + G_M*(Q^2/4M^2)}{1+(Q^2/4M^2)} \f]
 *  \f[ F_2 = \frac{G_M - G_E}{1+(Q^2/4M^2)} \f]
 *
 * Sachs form factors:
 * \f[ G_E = F_1 - \tau \kappa F_2 \f]
 * \f[ G_M = F_1  + \kappa F_2 \f]
 *
 * Limits: 
 * \f[ F_{1p}(0)=1\ \ \ \mbox{and}\ \ \ F_{2p}(0)=\kappa_p, \ \ \  F_{1n}(0)=0\ \ \  \mbox{and}\ \ \  F_{2n}(0)=\kappa_n \f]
 *
 * \f$\kappa_p = \mu_p -1\f$  and \f$\kappa_n = \mu_n\f$ : anomalous magnetic moment of the proton and neutron
 * \f[ \mu_p=2.7928 \ \ \ \mbox{and}\ \ \  \mu_n=-1.9130  \f]
 *
 * \ingroup physics
 */
namespace insane {
  namespace physics {

    using units::M_p;
    using units::M_pion;
    using units::M_pi0;
    using units::GeV;
    using insane::units::Mu_p;
    using insane::units::Mu_n;
    using insane::units::Mu_d;

    /** Base class for form factors
     *
     * \ingroup FormFactors
     */
    class FormFactors : public TNamed {

    protected:
      TString fLabel;

    public:

      FormFactors();
      virtual ~FormFactors();

      void        SetLabel(const char * l){ fLabel = l;}
      const char* GetLabel() const { return fLabel; }

      /** @name Nucleon Form Factors
       */
      //@{ 
      virtual double GEp(double Qsq) const = 0;
      virtual double GMp(double Qsq) const = 0;
      virtual double GEn(double Qsq) = 0;
      virtual double GMn(double Qsq) = 0;

      virtual double FLp(double Qsq) {
        double tau = Qsq / (4.0 * M_p/GeV * M_p/GeV);
        return((1.0 + tau) * GEp(Qsq) / (TMath::Sqrt(4.0 * TMath::Pi())));
      }
      virtual double FTp(double Qsq) {
        double tau = Qsq / (4.0 * M_p/GeV * M_p/GeV);
        return(-1.0 * TMath::Sqrt((2.0 * tau * (1.0 + tau)) / (4.0 * TMath::Pi())) * GMp(Qsq));
      }
      //@} 

      /** @name Deuteron Form Factors
       *
       *
       *  - \f$F_{C0}\f$ : charge   monopole
       *  - \f$F_{M1}\f$ : magnetic dipole
       *  - \f$F_{C2}\f$ : charge   quadrupole
       *
       * \todo Add IA relations that use the deuteron WF to calculate form factors
       * from proton and neutron form factors.
       * 
       * References: 
       *
       *  1. https://inspirehep.net/record/565071
       *
       */
      //@{ 

      virtual double F_C0(double Qsq) {return 0.0;}
      virtual double F_M1(double Qsq) {return 0.0;}
      virtual double F_C2(double Qsq) {return 0.0;}

      virtual double GEd(double Qsq) = 0;
      virtual double GCd(double Qsq) { return (GEd(Qsq)); }
      virtual double GMd(double Qsq) = 0;
      virtual double GQd(double Qsq) { return 0.0 ; }

      virtual double Ad(double Qsq);
      virtual double Bd(double Qsq);
      virtual double T20d(double Qsq);

      //{   double eta = Qsq/(4.0*(insane::units::M_d*insane::units::M_d)/(GeV*GeV));
      //   double GC = GCd(Qsq);
      //   double GM = GMd(Qsq);
      //   double GQ = GQd(Qsq);
      //   double T = -1.0/(TMath::Sqrt(2.0)*(Ad(Qsq)+Bd(Qsq)*TMath::Tan
      //   return( );
      //}
      //@}

      /** @name 3He Form Factors
       */
      //@{
      virtual double GEHe3(double Qsq) = 0;
      virtual double GMHe3(double Qsq) = 0;
      //@}

      /** @name 4He Form Factors
       */
      //@{
      virtual double GEHe4(double Qsq) { return FCHe4(Qsq); }
      virtual double FCHe4(double Qsq) { return 0.0; }
      //@}

      /** @name Quasi-elastic form factors
       */
      //@{
      virtual double GEdQE(  double /*x*/,double /*Qsq*/){return 0.0;}
      virtual double GMdQE(  double /*x*/,double /*Qsq*/){return 0.0;}
      virtual double GEHe3QE(double /*x*/,double /*Qsq*/){return 0.0;}
      virtual double GMHe3QE(double /*x*/,double /*Qsq*/){return 0.0;}
      //@}


      /** @name Form Factors for Nuclei with Z > 6.
       *
       *  Forms for GE and GM derived from expressions for W1 and W2
       *  from Stein et al, Phys Rev D 12, 1884 (1975)
       */
      //@{ 
      virtual double GENuclear(double Q2, double Z, double A);
      virtual double GMNuclear(double Q2, double Z, double A);
      virtual double GENuclear2(double Q2, double Z, double A);
      virtual double GMNuclear2(double Q2, double Z, double A);

      virtual double GE_A(double Z,double A,double Q2);
      virtual double GM_A(double Z,double A,double Q2);
      //@}

      /** Sachs Form Factors.
       *
       * \f[ F_1 = (G_E + G_M*(Q^2/4M^2))/(1+(Q^2/4M^2)) \f]
       * \f[ F_2 = (G_M - G_E)/(1+(Q^2/4M^2)) \f]
       *
       */
      //@{ 
      double F1p(double Q2) {
        double GE = GEp(Q2);
        double GM = GMp(Q2);
        double t  = Q2/(4.0*0.938*0.938);
        return (GE + GM*t)/(1+t);
      }
      double F2p(double Q2) {
        double GE = GEp(Q2);
        double GM = GMp(Q2);
        double t  = Q2/(4.0*0.938*0.938);
        return (GM - GE)/(1+t);
      }

      /** Sachs Form Factors. 
       * F1 = (GE + GM*(Q^2/4M^2))/(1+(Q^2/4M^2))
       * F2 = (GM - GE)/(1+(Q^2/4M^2))
       */
      double F1n(double Q2) {
        double GE = GEn(Q2);
        double GM = GMn(Q2);
        double t  = Q2/(4.0*0.938*0.938);
        return (GE + GM*t)/(1+t);
      }
      double F2n(double Q2) {
        double GE = GEn(Q2);
        double GM = GMn(Q2);
        double t  = Q2/(4.0*0.938*0.938);
        return (GM - GE)/(1+t);
      }
      //@} 

      /** @name Spin structure functions at x=1.
       *
       */
      //@{ 
      double g1p_el(double Qsq) ;
      double g2p_el(double Qsq) ;
      double g1n_el(double Qsq) ;
      double g2n_el(double Qsq) ;

      double g1p_el_TMC(double Qsq) ;
      double g2p_el_TMC(double Qsq) ;
      //double g1n_el_TMC(double Qsq) ;
      //double g2n_el_TMC(double Qsq) ;
      //@}


      /** @name Moments
       *
       * The elastic contribution to the moments evaluated at x=0.
       *
       */
      //@{ 
      
      //@{ 
      /** Mellin moments. 
       *  Argument n doesn't matter since elastic moments are only evaluated at
       *  x=0.  It is just there to mirror the interface from the 
       *  structure functions.
       *
       *  \f[ \Gamma_i^n = \int dx g_i^{n-1} \f]
       */
      double Mellin_g1p(int n, double Q2){ return g1p_el(Q2);}
      double Mellin_g2p(int n, double Q2){ return g2p_el(Q2);}
      double Mellin_g1n(int n, double Q2){ return g1n_el(Q2);}
      double Mellin_g2n(int n, double Q2){ return g2n_el(Q2);}
      double Mellin_mn_p( int m, int n, double Q2);

      double Mellin_mn_TMC_p( int m, int n, double Q2);

      //@} 

      //@{ 
      /**  Cornwall-Norton d_2. */
      double d2p_tilde(double Q2){return d2p_el(Q2); }
      double d2n_tilde(double Q2){return d2n_el(Q2); }

      double d2p_tilde_TMC(double Q2){return d2p_el(Q2); }
      double d2n_tilde_TMC(double Q2){return d2n_el(Q2); }

      double d2p_el(double Qsq) ;
      double d2n_el(double Qsq) ;
      //@} 



      /** @name Nachtmann moments. 
       *   
       *   \f$ M_1^n(Q^2) \f$ for n=1,3,..., and  \f$ M_2^n(Q^2) \f$
       *   for n=3,5....
       *
       */
      //@{ 
      double d2p_el_Nachtmann(double Qsq) ;
      double d2n_el_Nachtmann(double Qsq) ;

      //@{ 
      /** Nachtmann moments M_n^m */
      double Mmn_p( int m, int n, double Q2 );
      double Mmn_n( int m, int n, double Q2 );
      double Mmn_TMC_p( int m, int n, double Q2);
      //double Mmn_TMC_n( int m, int n, double Q2);
      //@} 

      double M1n_p( int n, double Q2 );
      double M2n_p( int n, double Q2 );
      double M1n_n( int n, double Q2 );
      double M2n_n( int n, double Q2 );

      double M1n_TMC_p( int n, double Q2);
      double M2n_TMC_p( int n, double Q2);
      //double M1n_TMC_n( int n, double Q2);
      //double M2n_TMC_n( int n, double Q2);
      //@} 

      ClassDef(FormFactors,3)
    };



    /** Form Factors interface.
    */
    class FormFactors2 : public TNamed , public virtual TAttLine, public virtual TAttFill, public virtual TAttMarker {

    protected:
      std::string      fLabel;

      // Storage of the Q2 values last used to compute the structure function
      // indexed by enum class insane::physics::FormFactor
      mutable std::array<std::array<double, NFormFactors>, NNuclei> fQ2_values;
      mutable std::array<std::array<double, NFormFactors>, NNuclei> fValues;
      mutable std::array<std::array<double, NFormFactors>, NNuclei> fUncertainties;

    public:
      FormFactors2();
      virtual ~FormFactors2();

      void        SetLabel(const char * l){ fLabel = l;}
      const char* GetLabel() const { return fLabel.c_str(); }

      double GetQSquared(std::tuple<FormFactor, Nuclei> ff) const;
      bool   IsComputed( std::tuple<FormFactor, Nuclei> ff, double Q2) const;

      void Reset();

      /** Calculate and return distribution value.
       * Returns the current value for flavor f but checks that the 
       * distributions have already been calculated at (Q2). 
       * It uses IsComputed(Q2) to do this check.
       */
      virtual double Get(double Q2, std::tuple<FormFactor, Nuclei> ff) const ;

      /** Get current distribution value.
       * Note: this method should only be used to get the stored values 
       * after Calculate(Q2) or Get(f,Q2) has been used at the desired (Q2).
       */ 
      //double  Get(std::tuple<FormFactor, Nuclei> ff) const ;

      /** Virtual method should get all values of form factors. 
       * This also sets internal values of fQsquared for use by IsComputed()
       */
      virtual double  Calculate    (double Q2, std::tuple<FormFactor, Nuclei> ff) const {return 0.0;}
      virtual double  Uncertainties(double Q2, std::tuple<FormFactor, Nuclei> ff) const {return 0.0;}

      virtual double G_E(double Q2, Nuclei target) const {return 0.0;}
      virtual double G_M(double Q2, Nuclei target) const {return 0.0;}
      virtual double G_Q(double Q2, Nuclei target) const {return 0.0;}
      //virtual double G_M(double Q2, Nuclei target) const = 0;
      //
      virtual double F1(double Q2, Nuclei target) const;
      virtual double F2(double Q2, Nuclei target) const;

      virtual double F1p(double Q2) const {return 0.0;}
      virtual double F2p(double Q2) const {return 0.0;}
      virtual double F1n(double Q2) const {return 0.0;}
      virtual double F2n(double Q2) const {return 0.0;}
      virtual double F1p_TMC(double Q2) const {return 0.0;}
      virtual double F2p_TMC(double Q2) const {return 0.0;}
      virtual double F1n_TMC(double Q2) const {return 0.0;}
      virtual double F2n_TMC(double Q2) const {return 0.0;}

      /**
       * \f$ \rho = \sqrt{1+ \frac{4M^2x^2}{Q^2}} \f$
       */
      //virtual double FL(double Q2, Nuclei target, Twist t = Twist::All) const;

      /** 
       * \f$ R = \sigma_L/\sigma_T \f$
       */
      //virtual double R(double Q2, Nuclei target, Twist t = Twist::All) const;
      //

      ClassDef(FormFactors2,1)
    };

  }
}

#endif
