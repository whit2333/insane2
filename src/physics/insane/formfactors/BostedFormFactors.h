#ifndef BostedFormFactors_HH
#define BostedFormFactors_HH 1

#include "insane/formfactors/DipoleFormFactors.h"

namespace insane {
namespace physics {
/** Bosted's fits to GE and GM for the proton and neutron. 
 *  Phys. Rev. C 51, 409 (1995)
 *  Also included is the smearing factor Psi taken from: 
 *  Phys. Rev. C 71, 015501 (2005) [arXiv: nucl-th/0409078]
 *  This smearing factor allows for building up a QE form factor. 
 *  A similar approach is used in the F1F209 source code (see ~/src/F1F209.f)    
 *
 *  \ingroup formfactors  
 */
class BostedFormFactors: public DipoleFormFactors{

   protected:
      Double_t fa,fb;           // Form factor parameters for the neutron 
      Double_t fp[6],fn[5];     // Form factor parameters for the proton and neutron 
      Double_t fKF,fES;         // KF = Fermi momentum, ES = E_shift [GeV]  
      bool fUseScalingFunction; // Momentum scaling function  

   public: 
      BostedFormFactors(){
         SetNameTitle("BostedFormFactors","Bosted Form Factors");
         SetLabel("Bosted FFs");
         InitializeFitParameters(); 
      }
      ~BostedFormFactors(){} 

      virtual Double_t GEp(Double_t Qsq){
         Double_t Q   = TMath::Sqrt(Qsq);
         Double_t num = 1.; 
         Double_t den = 0.;
         for(int i=0;i<6;i++){
            den += fp[i]*TMath::Power( Q,Double_t(i) ); 
         }
         Double_t GE = num/den;
         return GE;  
      } 

      virtual Double_t GMp(Double_t Qsq){
         Double_t GE = GEp(Qsq); 
         Double_t GM = Mu_p*GE; 
         return GM;  
      } 

      virtual Double_t GEn(Double_t Qsq){
           Double_t M   = M_n/GeV; 
           Double_t tau = Qsq/(4.*M*M);
           Double_t num = (-1.)*fa*Mu_n*tau*DipoleFormFactor(Qsq); 
           Double_t den = 1. + fb*tau; 
           Double_t GE  = num/den; 
           return GE;  
      } 

      virtual Double_t GMn(Double_t Qsq){
         Double_t Q   = TMath::Sqrt(Qsq);
         Double_t num = 1.; 
         Double_t den = 0.;
         for(int i=0;i<5;i++){
            den += fn[i]*TMath::Power( Q,Double_t(i) ); 
         }
         Double_t GM = Mu_n*num/den;
         return GM;  
      } 

      void SetParameters(Int_t A){
         if(A==2){
            fKF = 0.085; 
            fES = 0.0022; 
         }else if(A==3){
            fKF = 0.115;
            fES = 0.001;  
         }else if( (A>3)&&(A<8) ){
            fKF = 0.190;
            fES = 0.017;  
         }else if( (A>7)&&(A<17) ){
            fKF = 0.228;
            fES = 0.0165;  
         }else if( (A>16)&&(A<26) ){
            fKF = 0.230;
            fES = 0.023;  
         }else if( (A>25)&&(A<39) ){
            fKF = 0.236;
            fES = 0.018;  
         }else if( (A>38)&&(A<56) ){
            fKF = 0.241;
            fES = 0.028;  
         }else if( (A>55)&&(A<61) ){
            fKF = 0.241;
            fES = 0.023;  
         }else if(A>60){
            fKF = 0.245;
            fES = 0.018;  
         }
      } 

      void PrintParameters(){
         std::cout << "--------------------- Parameters ---------------------" << std::endl;
         for(int i=0;i<6;i++){
            std::cout << "fp[" << i << "] = " << fp[i] << std::endl;
         }
         for(int i=0;i<5;i++){
            std::cout << "fn[" << i << "] = " << fn[i] << std::endl;
         }
         std::cout << "fa = " << fa << std::endl;
         std::cout << "fb = " << fb << std::endl;
         std::cout << "KF = " << fKF << std::endl;
         std::cout << "ES = " << fES << std::endl;
         std::cout << "------------------------------------------------------" << std::endl;
      }

      void UseScalingFunction(bool ans=true){ 
         fUseScalingFunction = ans; 
         if(fUseScalingFunction){ 
            std::cout << "[BostedFormFactors]: Will use the scaling function." << std::endl; 
         }else{
            std::cout << "[BostedFormFactors]: Will NOT use the scaling function." << std::endl; 
         }
      } 

      Double_t PauliSuppression(Double_t qVect){
         Double_t ps=0;
         if(qVect < 2.*fKF){
            ps = 0.75*(qVect/fKF)*( 1. - (1./12.)*TMath::Power(qVect/fKF,2.) ); 
         }else{
            ps = 1.;
         }
         return ps;
      }

      Double_t Psi(Double_t x,Double_t Qsq,Double_t Es=0){
         // For psi', use Es != 0 
         Double_t M      = M_p/GeV; 
         Double_t nu     = Qsq/(2.*M*x); 
         Double_t qVect  = TMath::Sqrt(nu*nu + Qsq); 
         Double_t kappa  = qVect/(2.*M); 
         Double_t lambda = (nu-Es)/(2.*M); 
         Double_t tau    = kappa*kappa - lambda*lambda; 
         Double_t eta_F  = fKF/M; 
         Double_t xi_F   = TMath::Sqrt(1. + eta_F*eta_F) - 1.;
         // First term  
         Double_t T1     = 1./TMath::Sqrt(xi_F);
         // Second term 
         Double_t num    = lambda - tau;
         Double_t arg    = (1.+lambda)*tau + kappa*TMath::Sqrt( tau*(1.+tau) );  
         Double_t den    = 0.;
         if(arg>0) den   = TMath::Sqrt(arg); 
         Double_t T2     = 0.; 
         if(den!=0) T2 = num/den; 
         // Put it together  
         Double_t psi    = T1*T2; 
         return psi; 
      }

      Double_t Delta(Double_t x,Double_t Qsq){
         Double_t M      = M_p/GeV; 
         Double_t nu     = Qsq/(2.*M*x); 
         Double_t qVect  = TMath::Sqrt(nu*nu + Qsq); 
         Double_t kappa  = qVect/(2.*M); 
         Double_t tau    = Qsq/(4.*M*M); 
         Double_t eta_F  = fKF/M; 
         Double_t xi_F   = TMath::Sqrt(1. + eta_F*eta_F) - 1.;
         Double_t psi    = Psi(x,Qsq); 
         Double_t T1     = xi_F*(1.-psi*psi);
         Double_t T2     = TMath::Sqrt( tau*(1.+tau) )/kappa; 
         Double_t T3     = (1./3.)*xi_F*(1.-psi*psi)*( tau/(kappa*kappa) );
         Double_t delta  = T1*(T2+T3);
         return delta;  
      }

      Double_t ScalingFunction(Double_t x,Double_t Qsq){
         /// see Bosted and Mamyan, arXiv: 1203.2262v2, Eq 11
         //Double_t M      = M_p/GeV; 
         //Double_t nu     = Qsq/(2.*M*x); 
         // Fudge factor to align the QE peak with data (on the cross section) 
         //Double_t x_0    = 0.05; 
         Double_t x_p0   =  1.53985;    
         Double_t x_p1   = -6.2427; 
         Double_t x_1    = TMath::Exp(x_p0 + x_p1*Qsq);  
         Double_t x_pr   = x + x_1; 
         /// First three terms 
         Double_t psi_pr = Psi(x_pr,Qsq,fES);    
         Double_t T1     = 1.5576/fKF; 
         Double_t T2     = 1. + TMath::Power(1.7720*(psi_pr+ 0.3014),2.);  
         Double_t T3     = 1. + TMath::Exp(-2.4291*psi_pr);
         /// Fourth and fifth terms (not in original scaling function)
         // Fudge factor to get the normalization on the cross section right at the QE peak (x = 1)  
         // WARNING: Tends to break down for Es > 7 GeV!    
         Double_t p0  =  0.44609;    
         Double_t p1  = -0.760684;    
         Double_t p2  =  0.120695;    
         Double_t p3  = -0.00754428;  
         Double_t arg = p0 + p1*Qsq + p2*Qsq*Qsq + p3*Qsq*Qsq*Qsq;
         Double_t T4  = TMath::Exp(arg); 
         // x-dependence: x^{\alpha} 
         // This is for the lower x region 
         Double_t T5   = TMath::Power(x,1.7); 
         // for the shift in x... 
         // Double_t x_p0  = -0.397525;     
         // Double_t x_p1  =  0.280507;  
         // Double_t x_0   = x_p0 + x_p1*Qsq; 
         // Double_t x_arg = x + x_0;  
         // if(x_arg<0){
         //    std::cout << "x_arg < 0! " << std::endl;
         //    std::cout << "x = " << x << "\t" << "x_0 = " << x_0 << std::endl;
         //    x_arg = 0.;
         //    std::cout << "---------------------------------------------------------------" << std::endl; 
         // } 
         // Double_t T5    = TMath::Power(x_arg,1.7); 
         // put it together  
         Double_t sf   = T5*T4*(T1/T2/T3);
         return sf; 
      }

      virtual Double_t GEdQE(Double_t x,Double_t Qsq){
         SetParameters(2); // 2 = Deuteron 
         Double_t ge = GE(1.,2.,x,Qsq);  
         return ge;
      }  
      virtual Double_t GMdQE(Double_t x,Double_t Qsq){
         SetParameters(2); // 2 = Deuteron 
         Double_t gm = GM(1.,2.,x,Qsq);
         return gm; 
      }   

      virtual Double_t GEHe3QE(Double_t x,Double_t Qsq){
         SetParameters(3);  // 3 = He3 
         Double_t ge = GE(2.,3.,x,Qsq);
         return ge;
      } 

      virtual Double_t GMHe3QE(Double_t x,Double_t Qsq){
         SetParameters(3);  // 3 = He3
         Double_t gm = GM(2.,3.,x,Qsq);
         return gm;
      }

      Double_t F1He3QE(Double_t x,Double_t Qsq){
         SetParameters(3); 
         Double_t M  = M_p/GeV; 
         Double_t F1 = 0.5*M*ScalingFunction(x,Qsq)*GT(2.,3.,x,Qsq);
         return F1; 
      } 

      Double_t F2He3QE(Double_t x,Double_t Qsq){
         SetParameters(3); 
         Double_t M     = M_p/GeV; 
         Double_t tau   = Qsq/(4.*M*M);
         Double_t nu    = Qsq/(2.*M*x);
         Double_t qVect = TMath::Sqrt(nu*nu + Qsq);
         Double_t kappa = qVect/(2.*M);
         Double_t nuL   = TMath::Power(tau/(kappa*kappa),2.);
         Double_t nuT   = tau/(2.*kappa*kappa);  // see note in F1F209 fortran code! 
         Double_t F2    = nu*ScalingFunction(x,Qsq)*( nuL*GL(2.,3.,x,Qsq) + nuT*GT(2.,3.,x,Qsq) );
         return F2; 
      } 

      Double_t GL(Double_t Z,Double_t A,Double_t x,Double_t Qsq){
         Double_t M     = M_p/GeV;
         Double_t tau   = Qsq/(4.*M*M);
         Double_t nu    = Qsq/(2.*M*x); 
         Double_t qVect = TMath::Sqrt(nu*nu + Qsq); 
         Double_t kappa = qVect/(2.*M); 
         Double_t ge    = GE(Z,A,x,Qsq);  
         Double_t gm    = GM(Z,A,x,Qsq);
         Double_t W2    = (ge*ge + tau*gm*gm)/(1. + tau);
         Double_t delta = Delta(x,Qsq);
         Double_t beta  = Beta(x,Qsq);  
         Double_t num   = kappa*kappa*(ge*ge + delta*W2);
         Double_t den   = tau*beta;
         Double_t gl    = num/den; 
         return gl;  
      }

      Double_t GT(Double_t Z,Double_t A,Double_t x,Double_t Qsq){
         Double_t M     = M_p/GeV;
         Double_t tau   = Qsq/(4.*M*M);
         //Double_t nu    = Qsq/(2.*M*x); 
         //Double_t qVect = TMath::Sqrt(nu*nu + Qsq); 
         Double_t ge    = GE(Z,A,x,Qsq);  
         Double_t gm    = GM(Z,A,x,Qsq);
         Double_t W2    = (ge*ge + tau*gm*gm)/(1. + tau);
         Double_t delta = Delta(x,Qsq);
         Double_t beta  = Beta(x,Qsq); 
         Double_t num   = 2.*tau*gm*gm + delta*W2;
         Double_t den   = beta;
         Double_t gt    = num/den; 
         return gt;  
      }

      Double_t Beta(Double_t x,Double_t Q2){
         Double_t M      = M_p/GeV;
         Double_t nu     = Q2/(2.*M*x);
         Double_t qVect  = TMath::Sqrt(nu*nu + Q2);
         Double_t kappa  = qVect/(2.*M);
         Double_t eta_F  = fKF/M;
         Double_t xi_F   = TMath::Sqrt(1. + eta_F*eta_F) - 1.;
         Double_t psi    = Psi(x,Q2,0.);
         Double_t beta   = 2.*kappa*( 1. + 0.5*xi_F*(1. + psi*psi) );
         return beta;
      }

   private: 
      Double_t GE(Double_t Z,Double_t A,Double_t x,Double_t Qsq){
         Double_t M      = M_p/GeV; 
         Double_t nu     = Qsq/(2.*M*x); 
         Double_t qVect  = TMath::Sqrt(nu*nu + Qsq); 
         Double_t ps     = PauliSuppression(qVect);
         Double_t sf     = ScalingFunction(x,Qsq);
         Double_t arg    = ps*( Z*GEp(Qsq)*GEp(Qsq) + (A-Z)*GEn(Qsq)*GEn(Qsq) );
         if(fUseScalingFunction) arg *= sf; 
         Double_t ge     = TMath::Sqrt(arg);
         return ge;
      } 

      Double_t GM(Double_t Z,Double_t A,Double_t x,Double_t Qsq){
         Double_t M      = M_p/GeV; 
         Double_t nu     = Qsq/(2.*M*x); 
         Double_t qVect  = TMath::Sqrt(nu*nu + Qsq); 
         Double_t ps     = PauliSuppression(qVect);
         Double_t sf     = ScalingFunction(x,Qsq);
         Double_t arg    = ps*( Z*GMp(Qsq)*GMp(Qsq) + (A-Z)*GMn(Qsq)*GMn(Qsq) );
         if(fUseScalingFunction) arg *= sf; 
         Double_t gm     = TMath::Sqrt(arg);
         return gm;
      } 

      void InitializeFitParameters(){
         // proton coefficients
         fp[0] =  1.00; 
         fp[1] =  0.14; 
         fp[2] =  3.01; 
         fp[3] =  0.02; 
         fp[4] =  1.20; 
         fp[5] =  0.32; 
         // neutron coefficients 
         fn[0] =  1.00; 
         fn[1] = -1.74; 
         fn[2] =  9.29; 
         fn[3] = -7.63; 
         fn[4] =  4.63; 
         // neutron GEn coefficients 
         fa   = 0.94; 
         fb   = 10.4; // +/- 0.6
         // fermi momentum, energy shift 
         fKF = 0.; 
         fES = 0.; 
         // scaling function 
         fUseScalingFunction = false;  
      }

      ClassDef(BostedFormFactors,1) 

}; 
}
}

#endif
