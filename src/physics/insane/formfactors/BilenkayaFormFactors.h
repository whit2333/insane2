#ifndef BILENKAYAFORMFACTORS_H
#define BILENKAYAFORMFACTORS_H 1 

#include "insane/formfactors/DipoleFormFactors.h"

namespace insane {
namespace physics {
/** Proton form factors from the fortran version of POLRAD.
 *  Paper reference: SI Bilenkaya et al, Pisma Zhetf 19, 613 (1974) 
 *
 * \ingroup formfactors 
 */ 
class BilenkayaFormFactors: public DipoleFormFactors{

   private: 
      Double_t fa_e,fb_e,fc_e,fd_e;
      Double_t fa_m,fb_m,fc_m,fd_m; 

   public: 
      BilenkayaFormFactors();
      virtual ~BilenkayaFormFactors();

      virtual Double_t GEp(Double_t);  
      virtual Double_t GMp(Double_t);  
      //Double_t GEn(Double_t Q2){return 0;} 
      //Double_t GMn(Double_t Q2){return 0;}   
      //Double_t GEd(Double_t Q2){return 0;} 
      //Double_t GMd(Double_t Q2){return 0;} 
      //Double_t GEHe3(Double_t Q2){return 0;} 
      //Double_t GMHe3(Double_t Q2){return 0;} 

      ClassDef(BilenkayaFormFactors,1) 

};

}
}

#endif 
