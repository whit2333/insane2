#ifndef KellyFormFactors_HH
#define KellyFormFactors_HH 1

#include "insane/formfactors/DipoleFormFactors.h"

namespace insane::physics {
  /** Kelly form factors.
   *
   * paper reference: Phys. Rev. C 70, 068202 (2004)
   *
   * \ingroup FormFactors
   */
  class KellyFormFactors : public DipoleFormFactors {

  private:
    double fa_gep[2], fb_gep[4];
    double fa_gmp[2], fb_gmp[4];
    double fa_gmn[2], fb_gmn[4];
    double fA, fB;

  protected:
    double G(int type, double Q2);

  public:
    KellyFormFactors();

    virtual ~KellyFormFactors();

    virtual double GEp(double Q2);
    virtual double GMp(double Q2);
    virtual double GEn(double Q2);
    virtual double GMn(double Q2);

    ClassDef(KellyFormFactors, 1)
  };
} // namespace insane::physics
#endif
