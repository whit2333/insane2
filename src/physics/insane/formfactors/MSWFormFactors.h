#ifndef MSWFormFactors_H 
#define MSWFormFactors_H 1 

#include "insane/formfactors/DipoleFormFactors.h"
namespace insane {
namespace physics {

/** 3He form factor fit from McCarthy, Sick and Whitney. 
 *  Paper reference: Phys Rev C 15, 1396 (1977) 
 *
 * \ingroup formfactors
 */ 
class MSWFormFactors: public DipoleFormFactors {

   private:
      Double_t fA,fZ;
      Double_t ff_0,ff_m,fd_f;  

      void CalculateFunctions(Double_t);

   public: 
      MSWFormFactors();
      virtual ~MSWFormFactors();

      //Double_t GEp(Double_t Q2){return 0;} 
      //Double_t GMp(Double_t Q2){return 0;} 
      //Double_t GEn(Double_t Q2){return 0;} 
      //Double_t GMn(Double_t Q2){return 0;} 
      //Double_t GEd(Double_t Q2){return 0;} 
      //Double_t GMd(Double_t Q2){return 0;} 
      Double_t GEHe3(Double_t); 
      Double_t GMHe3(Double_t); 

      ClassDef(MSWFormFactors,1)

};
}}
#endif 
