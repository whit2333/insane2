#ifndef RiordanFormFactors_HH
#define RiordanFormFactors_HH 1

#include "insane/formfactors/KellyFormFactors.h"
namespace insane {
namespace physics {

/** Riordan form factors. 
  *  
  * paper reference: Phys. Rev. Lett. 105, 262302 (2010)
  * NOTE: We derive from Kelly form factors, because this paper
  * uses Kelly's GMn in their fits.  This makes things convenient when
  * using these form factors.  
  *
  * \ingroup formfactors 
  */ 
class RiordanFormFactors: public KellyFormFactors{

   private:
      Double_t a_GE[2]; 

   public: 
      RiordanFormFactors(){
         SetNameTitle("RiordanFormFactors","Riordan Form Factors");
         SetLabel("Riordan FFs");
         a_GE[0] = 1.39; 
         a_GE[1] = 2.00;  
      }

      ~RiordanFormFactors(){}

      virtual Double_t GEn(Double_t Q2){
         Double_t M   = M_n/GeV;  
         Double_t tau = Q2/(4.*M*M); 
         Double_t num = a_GE[0]*tau; 
         Double_t den = 1. + a_GE[1]*tau; 
         Double_t T1  = num/den;
         Double_t T2  = DipoleFormFactor(Q2);
         Double_t GE  = T1*T2;   
         return GE; 
      }

      ClassDef(RiordanFormFactors,1) 
};
}}
#endif

