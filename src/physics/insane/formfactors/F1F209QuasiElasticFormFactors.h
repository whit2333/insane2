#ifndef F1F209QuasiElasticFormFactors_H 
#define F1F209QuasiElasticFormFactors_H 

#include "insane/formfactors/FormFactors.h"
#include "insane/structurefunctions/F1F209StructureFunctions.h"
namespace insane {
namespace physics {

/** Quasi-elastic form factors. 
  * This class returns quasi-elastic form factors, derived 
  * from W1, W2 written in terms of inelastic structure functions
  * and form factors. The quasi-elastic component of F1F209 
  * is used to evaluate F1 and F2 at a given (x,Q2). 
  * 
  * \ingroup formfactors
  */ 
class F1F209QuasiElasticFormFactors: public FormFactors {

   private:
      Double_t fKF,fES,fP[23]; 
      F1F209QuasiElasticStructureFunctions fSF; 

      Double_t GE(Int_t,Double_t,Double_t,Double_t,Double_t); 
      Double_t GM(Int_t,Double_t,Double_t,Double_t,Double_t);
      Double_t GE(Double_t,Double_t,Double_t,Double_t); 
      Double_t GM(Double_t,Double_t,Double_t,Double_t);
      Double_t Psi(Double_t,Double_t,Double_t); 
      Double_t F_y(Double_t,Double_t); 
      Double_t Delta(Double_t,Double_t);
      Double_t Beta(Double_t,Double_t);

      void SetParameters(Int_t);  
      
   public:
      F1F209QuasiElasticFormFactors();  
      virtual ~F1F209QuasiElasticFormFactors();
 
      Double_t GEp(Double_t Q2)  {return 0.;} 
      Double_t GMp(Double_t Q2)  {return 0.;} 
      Double_t GEn(Double_t Q2)  {return 0.;} 
      Double_t GMn(Double_t Q2)  {return 0.;} 
      Double_t GEd(Double_t Q2)  {return 0.;}     
      Double_t GMd(Double_t Q2)  {return 0.;}     
      Double_t GEHe3(Double_t Q2){return 0.;}   
      Double_t GMHe3(Double_t Q2){return 0.;}  
      Double_t GEdQE(Double_t,Double_t);    
      Double_t GMdQE(Double_t,Double_t);    
      Double_t GEHe3QE(Double_t,Double_t);  
      Double_t GMHe3QE(Double_t,Double_t);  

      ClassDef(F1F209QuasiElasticFormFactors,1) 
};

}
}
#endif 
