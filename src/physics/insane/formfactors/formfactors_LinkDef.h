#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;


#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;

//#pragma link C++ global insane::physics::fgFormFactors+;
#pragma link C++ class insane::physics::FormFactors+;
#pragma link C++ class insane::physics::BonnFormFactors+;
#pragma link C++ class insane::physics::FormFactors2+;
#pragma link C++ class insane::physics::DipoleFormFactors+;
#pragma link C++ class insane::physics::AMTFormFactors+;
#pragma link C++ class insane::physics::AHLYFormFactors+;
#pragma link C++ class insane::physics::MSWFormFactors+;
#pragma link C++ class insane::physics::GalsterFormFactors+;
#pragma link C++ class insane::physics::KellyFormFactors+;
#pragma link C++ class insane::physics::RiordanFormFactors+;
#pragma link C++ class insane::physics::AmrounFormFactors+;
#pragma link C++ class insane::physics::BilenkayaFormFactors+;
#pragma link C++ class insane::physics::BostedFormFactors+; 

#pragma link C++ typedef insane::physics::DefaultFormFactors+;

#pragma link C++ class insane::physics::F1F209QuasiElasticFormFactors+; 

//#pragma link C++ class insane::physics::GeneralizedPartonDistributions+;
//#pragma link C++ class insane::physics::DoubleDistributionGPDs+;
//#pragma link C++ class insane::physics::GK_GPDs+;

#endif

