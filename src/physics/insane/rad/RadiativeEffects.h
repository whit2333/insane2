#ifndef InSANE_physics_RadiativeEffects_HH
#define InSANE_physics_RadiativeEffects_HH
#include <array>
#include <cmath>
#include "insane/rad/RadCor.h"


namespace insane::rad {

     /** The energy loss formula of Bethe.
      * 
      * Good for energies above ~0.3 MeV.  
      *
      * @param z Incident particle charge
      * @param beta v/c where v is velocity. 
      * @param Z atomic number of material
      * @param A atomic mass 
      * @param rho density
      * 
      * \ingroup RadCor
      */
     constexpr double dEdx_Bethe(const double z, const double beta,
                                 const double Z, const double A, const double rho){
       using namespace insane::units;
       using namespace insane::masses;
       const double elmcsq  = insane::constants::elm_coupling*insane::constants::elm_coupling;
       const double b2 = beta*beta;
       // electron density
       const double Mu = 0.99999999965e-3*kg/mole; // kg/mol
       const double n  = insane::constants::Avogadro * Z * rho / (A * Mu);
       double I = (11.2+11.7*Z)*eV;
       if(Z>13) {
          I = (52.8+8.71*Z)*eV;
       }
       return (4.0 * M_PI / M_e) * (n * z * z / b2) * elmcsq *
              (std::log(2.0 * M_e * b2 / (I * (1.0 - b2))) - b2)/rho ;
     }

     /** F(q^2,T) for to multiply cross section to produce \f$ \sigma^{eff} \f$.
      *
      *  Tsai, SLAC-PUB-848 1971, Eqn 2.8.
      *  Usage found in Eqn 2.4.
      *
      * \ingroup RadCor
      */
     double F_Tsai(double Es, double Ep, double theta, double T=0.05);

     /** All internal terms for F(q^2,T).
      *
      * Tsai, SLAC-PUB-848 1971, Eqn 2.8 without the \f$1+ 0.5772 bT  \f$ term
      *  Used as in Eqn 2.4.
      *
      * \ingroup RadCor
      */
     double F_tilde_internal(double Es, double Ep, double theta);


     //double BremShape_phi(double v);

     /** Multiplicative factor for the radiative correction to the jth peak.
      *  Eqn 2.3, Tsai, SLAC-PUB-848 1971 -- this function
      *  returns the term multiplying sig_j^eff on the RHS.
      * \ingroup RadCor
      *
      *  \ingroup ElasticScattering
      */
     double RCToPeak_Tsai(double Es, double EpPeak, double theta, double T = 0.05, double Z = 2.0,
                          double A = 4.0);

     /** Landau Straggling parameter.
      *  Eqn 2.5, Tsai, SLAC-PUB-848 1971
      * \ingroup RadCor
      */
     double LandauStraggling_xi(double T, double Z, double A);

     /** Integral of Ib near tip.
      *  Eqn B.36, Tsai, SLAC-PUB-848 1971
      * \ingroup RadCor
      */
     double Ib_integral(double E0, double Eprime, double DeltaE, double T = 0.05, double Z = 2.0,
                        double A = 4.0);

     /** \f$ \Delta_{s/p} \f$Most probable energy loss due to ionization of an
      * electron passing through material of thickness t.
      * For Es, use t_b; for Ep, use t_a
      * Tsai, SLAC-PUB-0848 (1971), Eq 1.8
      * \ingroup RadCor
      */
     double Delta_Tsai71(double E, double t);

     /** Most probable energy loss due to ionization.
      * Tsai, SLAC-PUB-0848 (1971) Eq B22.
      * \ingroup RadCor
      */
     double Delta_0_Tsai71(double E, double t);

     ///** Strip approximation for inelastic radiative tail. 
     // * Mo and Tsai 1971 C.23, the Es' integrand. 
     // */
     //double StripApproxIntegrand1_Tsai71(double Es, double Ep, double theta, double T,
     //                                    double EsPrime);
     ///** Strip approximation for inelastic radiative tail. 
     // * Mo and Tsai 1971 C.23, the Ep' integrand. 
     // */
     //double StripApproxIntegrand2_Tsai71(double Es, double Ep, double theta, double T,
     //                                    double EpPrime);
     
     /**  Eqn 2.7, Tsai, SLAC-PUB-848 1971.
      *
      * \ingroup RadCor
      */
     double tr_Tsai(double Q2);

     /** Energy peaking approximation.
      *
      *  Equations C.11 and C12 of Tsai 1971, SLAC-PUB-848
      *
      * \ingroup RadCor
      * @{
      */
     double omega_s_Tsai(double Es, double Ep, double theta, double M);
     double omega_p_Tsai(double Es, double Ep, double theta, double M);
     //@}

     /** @brief Bremsstrahlung energy loss probability.
      *  Tsai, SLAC-PUB-0848 (1971), Eq B.43
      *  https://inspirehep.net/literature/67278
      *
      *  Same as B.43 but we explicitly write out W_b(E,eps)
      *  so that X0 cancels from the formula.
      * \ingroup RadCor
      */
     double Ib(const double& E0, const double& E, const double& t);

     /** Internal RC integrand (MT69).
      *
      * \ingroup RadCor
      */
     double InternalIntegrand_MoTsai69(double COSTHK, double Es, double Ep, double theta,
                                       double phi, double M, std::array<double, 2> ffs);


     ///** Returns the radition length in g/cm^2. */
     //double rad_length(int Z, int A);

} // namespace insane::rad

#endif
