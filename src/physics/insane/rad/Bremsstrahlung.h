#ifndef insane_Breamsstrahlung_HH
#define insane_Breamsstrahlung_HH

#include "insane/base/PhysicalConstants.h"
#include "insane/base/SystemOfUnits.h"

using namespace insane::units;


/** \defgroup Brem Bremsstrahlung Radiation and Beams
 *
 * <h2>References</h2>
 *
 *  - <a href="https://inspirehep.net/literature/49850">Thick Target Bremsstrahlung and Target Consideration for Secondary Particle Production by Electrons</a>
 *
 * \ingroup physics
 */
namespace insane::brem {

    using units::GeV;
    using units::M_p;
    using units::M_pion;

    /** @defgroup bremsstrahlung Bremsstrahlung Beams.
     *
     *  Photon spectrum from bremsstralung beams and virtual photon spectra
     *  for calculating electroproduction cross sections
     *
     * \ingroup xsections
     * \ingroup photoproduction
     * \ingroup RadCor
     * \ingroup Brem
     *  @{
     */

    /** Bremsstrahlung bremsstrahlung spectrum.
     *  Tsai and Whitis, SLAC-PUB-184 1966 eqn.24
     */
    double I_gamma_1(double t, double E0, double k);
    double I_gamma_1_test(double t, double E0, double k);
    double I_gamma_1_approx(double t, double E0, double k);

    /** Number of photons from bremsstrahlung.  */
    double N_gamma(double dOverX0, double kmin, double kmax, double E0 = -1.0);

    /** The average photon energy from a bremsstrahlung beam. */
    double Ebrem_avg(double t, double kmin, double E0);
    double I_gamma_1_k_avg(double t, double kmin, double E0);

    /** Number of equivalent quanta for bremsstrahlung photoproduction. */
    double Q_equiv_quanta(double t, double kmin, double E0);

    /** Minimum photon energy needed for the reaction gamma+Mt -> x+n. */
    double k_min_photoproduction(double Ex, double thetax, double mx = M_pion / GeV,
                                 double mt = M_p / GeV, double mn = M_p / GeV);

    //@}
} // namespace insane::brem

#endif
