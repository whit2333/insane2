#ifndef insane_rad_RadCor_HH
#define insane_rad_RadCor_HH

#include "insane/base/PhysicalConstants.h"
#include "insane/base/SystemOfUnits.h"

/** \defgroup RadCor Radiative Corrections and Radiative Effects
 *
 * <h2>References</h2>
 *
 *  - <a href="https://inspirehep.net/literature/52657">Radiative Corrections to Elastic and Inelastic e-p and mu-p Scattering</a>
 *  - <a href="https://inspirehep.net/literature/67278">Radiative Corrections to Electron Scatterings</a>
 *  - <a href="https://inspirehep.net/literature/49850">Thick Target Bremsstrahlung and Target Consideration for Secondary Particle Production by Electrons</a>
 *  - <a href="https://inspirehep.net/literature/100597">Electron Scattering at 4-Degrees with Energies of 4.5-GeV - 20-GeV</a>
 *
 * \ingroup physics
 */
namespace insane::rad {

  /** radiation length.
   *
   * \ingroup RadCor
   */
  double f_rad_length(double a);

  /** returns radiation length.
   *
   * \ingroup RadCor
   */
  double rad_length(int Z, int A);

}

#endif
