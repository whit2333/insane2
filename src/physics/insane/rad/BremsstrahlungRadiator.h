#ifndef BremsstrahlungRadiator_HH
#define BremsstrahlungRadiator_HH 1

#include <vector>
#include <map>
#include <utility>
#include <algorithm>

#include "insane/rad/RadCor.h"
#include "insane/rad/RadiativeEffects.h"
#include "insane/rad/Bremsstrahlung.h"
#include "insane/base/TargetMaterial.h"


namespace insane {

//class TargetMaterial;

/** Bremsstrahlung photon spectrum for an arbitrary radiator.
 *  Useful for calculating the photo-production cross section
 *  on targets that have more than one material. That is it is used to accumulate
 *  the bremsstralung photons as the beam moves through the target. This provides the correct 
 *  bremsstrahlung spectrum for the photo production cross section to use.
 *
 *  <h4>How this should be used:</h4>
 *     - Construct a "target" (Target). This holds the instance of the bremsstrahlung radiator.
 *     - Each "material" (TargetMaterial) of the  should be assigned a unique number
 *     - the materials are (arbitrarily constructed) and added to the target. 
 *     - When added the
 *
 * \ingroup materials
 * \ingroup Brem
 */
class BremsstrahlungRadiator {

   public:

      std::multimap<double,int>             fZPositions_map;
      std::vector< std::pair<int,double> >  fZPositions;
      std::map<int,double>                  fRadiatorLengths;
      Double_t                              fTotalRL;
      std::map<int,double>                  fCumulativeRL;  // raditor length for all up stream materials (not including the indexed material)

   public :
      BremsstrahlungRadiator();
      virtual ~BremsstrahlungRadiator();
      BremsstrahlungRadiator(const BremsstrahlungRadiator&) = default;               // Copy constructor
      BremsstrahlungRadiator(BremsstrahlungRadiator&&) = default;                    // Move constructor
      BremsstrahlungRadiator& operator=(const BremsstrahlungRadiator&) & = default;  // Copy assignment operator
      BremsstrahlungRadiator& operator=(BremsstrahlungRadiator&&) & = default;       // Move assignment operato

      //BremsstrahlungRadiator(const BremsstrahlungRadiator& v); 
      //BremsstrahlungRadiator& operator=(const BremsstrahlungRadiator& v); 

      void     AddRadiator(TargetMaterial * mat);
      Double_t GetRadiatorLength( Int_t i );

      // I_gamma returns the bremsstrahlung intensity at the
      // target material, id, including all upstream radiators
      // at the photo energy k_gamma, 
      // for an initial electron beam energy E0
      Double_t    I_gamma( Int_t id, Double_t k_gamma, Double_t E0 ) const ;

      void Print(Option_t * opt = "") const;
      void Print(std::ostream &stream) const ;

      ClassDef(BremsstrahlungRadiator,2)
};

}

#endif

