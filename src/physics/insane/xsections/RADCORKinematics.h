#ifndef RADCORKinematics_HH 
#define RADCORKinematics_HH 1

#include <cstdlib> 
#include <iostream> 
#include <vector> 
#include "TMath.h"
#include "TLorentzVector.h"
#include "PhysicalConstants.h"
#include "InSANEMathFunc.h"

namespace insane {
namespace physics {

class RADCORVariables{

   protected: 

      Bool_t fIsElastic; 
      Bool_t fIsInternal; 
      Bool_t fIsExternal; 

      TLorentzVector fs; 
      TLorentzVector fp;
      TLorentzVector ft; 
      TLorentzVector fu; 

      Int_t    fUnits; 
      Double_t fCONV; 
      Double_t fu_0;
      Double_t fuVect;
      Double_t fsVect;
      Double_t fpVect;
      Double_t ftVect; 
      Double_t fEs;
      Double_t fEp;
      Double_t ftheta;
      Double_t fphi;
      Double_t fx;
      Double_t fy;
      Double_t fW;
      Double_t fW2; 
      Double_t fQ2;
      Double_t fm;
      Double_t fM; 
      Double_t fM_A; 
      Double_t fM_pion;
      Double_t fR;    
      Double_t fCFACT; 
      Double_t fTa;       ///< A kinematic?
      Double_t fTb;       ///< A kinematic?
      Double_t fT;
      Double_t fEta;
      Double_t fXi;
      Double_t fb;        ///<  Should not be here! It is just a function of Z
      Double_t fZ;  // we list these here because we need Z for eta and xi  
      Double_t fA;  // we list these here because we need A for eta and xi 

   public: 
      RADCORVariables();
      virtual ~RADCORVariables();

      void Update();
      void Clear();
      void Print();

      friend class RADCORKinematics; 

      ClassDef(RADCORVariables,1) 

};

class RADCORKinematics{

   protected: 
      mutable bool fIsModified; 
      mutable RADCORVariables fVariables;

   public: 
      RADCORKinematics();
      virtual ~RADCORKinematics(); 

      // sanity checks
      bool Valid() const ;

      void Print(){CheckUpdate(); fVariables.Print();} 

      /// If you reset these, everything MUST be recalculated... 
      void DoElastic(bool ans)  {fVariables.fIsElastic  = ans; fIsModified = true;} 
      void UseInternal(bool ans){fVariables.fIsInternal = ans; fIsModified = true;}  
      void UseExternal(bool ans){fVariables.fIsExternal = ans; fIsModified = true;}  
      void SetUnits(int u)      {fVariables.fUnits      = u;   fIsModified = true;} 
      void SetEs(Double_t v)    {fVariables.fEs         = v;   fIsModified = true;} 
      void SetEp(Double_t v)    {fVariables.fEp         = v;   fIsModified = true;} 
      void SetTheta(Double_t v) {fVariables.ftheta      = v;   fIsModified = true;} 
      void SetPhi(Double_t v)   {fVariables.fphi        = v;   fIsModified = true;} 
      void SetZ(Double_t z)     {fVariables.fZ          = z;   fIsModified = true;} 
      void SetA(Double_t a)     {fVariables.fA          = a;   fIsModified = true;} 
      void Sett_b(Double_t t)   {fVariables.fTb         = t;   fIsModified = true;} 
      void Sett_a(Double_t t)   {fVariables.fTa         = t;   fIsModified = true;} 
      void Setm(Double_t m)     {fVariables.fm          = m;   fIsModified = true;} 
      void SetM(Double_t M)     {fVariables.fM          = M;   fIsModified = true;} 
      void SetM_A(Double_t MA)  {fVariables.fM_A        = MA;  fIsModified = true;} 
      void SetM_pion(Double_t M){fVariables.fM_pion     = M;   fIsModified = true;} 

      void CheckUpdate()    const { if(fIsModified){fVariables.Update(); fIsModified = false;} }

      TLorentzVector Gets() const {CheckUpdate(); return fVariables.fs;} 
      TLorentzVector Getp() const {CheckUpdate(); return fVariables.fp;} 
      TLorentzVector Gett() const {CheckUpdate(); return fVariables.ft;} 
      TLorentzVector Getu() const {CheckUpdate(); return fVariables.fu;} 

      Double_t Getu_0()     const {CheckUpdate(); return fVariables.fu_0;  } 
      Double_t GetsVect()   const {CheckUpdate(); return fVariables.fsVect;}
      Double_t GetpVect()   const {CheckUpdate(); return fVariables.fpVect;}
      Double_t GettVect()   const {CheckUpdate(); return fVariables.ftVect;}
      Double_t GetuVect()   const {CheckUpdate(); return fVariables.fuVect;}

      Double_t GetA()       const { CheckUpdate(); return fVariables.fA;}
      Double_t GetZ()       const { CheckUpdate(); return fVariables.fZ;}

      Double_t GetEs()      const { CheckUpdate(); return fVariables.fEs;}
      Double_t GetEp()      const { CheckUpdate(); return fVariables.fEp;}
      Double_t GetTheta()   const { CheckUpdate(); return fVariables.ftheta;}
      Double_t GetPhi()     const { CheckUpdate(); return fVariables.fphi;}
      Double_t Getm()       const { CheckUpdate(); return fVariables.fm; }
      Double_t GetM()       const { CheckUpdate(); return fVariables.fM; }
      Double_t GetM_pion()  const { CheckUpdate(); return fVariables.fM_pion;}
      Double_t GetM_A()     const { CheckUpdate(); return fVariables.fM_A; }
      Double_t GetQ2()      const { CheckUpdate(); return fVariables.fQ2; }
      Double_t GetW()       const { CheckUpdate(); return fVariables.fW; }
      Double_t GetW2()      const { CheckUpdate(); return fVariables.fW2; }
      Double_t Getx()       const { CheckUpdate(); return fVariables.fx; }
      Double_t Gety()       const { CheckUpdate(); return fVariables.fy; }
      Double_t Getb()       const { CheckUpdate(); return fVariables.fb; }
      Double_t Geteta()     const { CheckUpdate(); return fVariables.fEta; }
      Double_t Getxi()      const { CheckUpdate(); return fVariables.fXi; }
      Double_t GetR()       const { CheckUpdate(); return fVariables.fR; }
      Double_t Gett_b()     const { CheckUpdate(); return fVariables.fTb; }
      Double_t Gett_a()     const { CheckUpdate(); return fVariables.fTa; }
      Double_t GetT()       const { CheckUpdate(); return fVariables.fT; }

      ClassDef(RADCORKinematics,1) 

};
}}

#endif 
