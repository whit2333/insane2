#ifndef PolarizedDISXSec_HH
#define PolarizedDISXSec_HH

#include "PolarizedStructureFunctions.h"
#include "InclusiveDiffXSec.h"
#include "Particle.h"
#include "TRandom.h"
//#include "RunManager.h"
namespace insane {
namespace physics {

/** Base class for a double polarization cross section.
 *  This should be used as the main Xsec object. Classes derived from PolarizedDISXSec should be added to it.
 *  \todo This can be generalized to add arbitray number of xsections
 *
 *  By default cross sections are flat.
 *
 * \f$ \frac{d\sigma}{dE^{\prime} d\Omega}(E^{\prime},\theta,\lambda; P_{beam}, \Delta_{Q} ) = \\
 * (1-P_{beam})\sigma_{unpol}(E^{\prime},\theta) + P_{beam}(1+\Delta_{Q})\sigma_{+}(E^{\prime},\theta,\lambda) + P_{beam}(1-\Delta_{Q})\sigma_{-}(E^{\prime},\theta,\lambda)  \f$
 *
 *
 * \ingroup EventGen
 * \ingroup xsections
 */
class PolarizedDiffXSec : public InclusiveDiffXSec {
   private:
      TRandom * fRandom;
   protected:
      Double_t                              fBeamPolarization;
      Double_t                              fTargetPolarization;
      Double_t                              fChargeAsymmetry;
      DiscretePhaseSpaceVariable    * fHelicityPSVar;
      InclusiveDiffXSec             * fCrossSections[3];
      Double_t                              fXSecCoefficients[3];

   public:
      PolarizedDiffXSec() {
         fRandom = gRandom;//RunManager::GetRunManager()->GetRandom();
         fTitle = "PolarizedDiffXSec";
         fnDim = 4;
         fBeamPolarization = 0.7;
         fTargetPolarization = 0.7;
         fChargeAsymmetry = 0.0;
         fHelicityPSVar = nullptr;
         fCrossSections[0] = nullptr;
         fCrossSections[1] = nullptr;
         fCrossSections[2] = nullptr;
         fXSecCoefficients[0] = (1.0 - fBeamPolarization*fTargetPolarization);
         fXSecCoefficients[1] = (fBeamPolarization*fTargetPolarization)*(1.0 + fChargeAsymmetry );
         fXSecCoefficients[2] = (fBeamPolarization*fTargetPolarization)*(1.0 - fChargeAsymmetry );
      }
      virtual ~PolarizedDiffXSec() { }
      virtual PolarizedDiffXSec*  Clone(const char * newname) const {
         std::cout << "PolarizedDiffXSec::Clone()\n";
         auto * copy = new PolarizedDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual PolarizedDiffXSec*  Clone() const { return( Clone("") ); } 

      void DefineEvent(Double_t *vars) {
         Int_t totvars = 0;
         for (int i = 0; i < fParticles.GetEntries(); i++) {
            /// here we are assuming the order E,theta,phi,then others
            /// \todo figure out how to handle vertex.
            auto * part = (Particle*)fParticles.At(i);
            insane::Kine::SetMomFromEThetaPhi((TParticle*)part, &vars[totvars]);
            //((TParticle*)fParticles.At(i))->SetProductionVertex(GetRandomVertex());
            //Set the helicity
            if(fHelicityPSVar) {
               part->fHelicity = fHelicityPSVar->GetDiscreteVariable(vars[3]);
               if(part->fHelicity == 2) part->fHelicity = -1;
               else if(part->fHelicity == 1) part->fHelicity = 1;
               else if(part->fHelicity == 0) {
                  if( fRandom->Uniform(-1.0,1.0) < fChargeAsymmetry ) part->fHelicity = 1;
                  else part->fHelicity = -1;
               }
            }
            totvars += GetNParticleVars(i);
            //part->Dump();
         }
      }
      /** Adds to DiffXSec::Refresh explicit setting of the helicity phase space variable
       *  to improve speed.
       */
      void Refresh() {
         this->DiffXSec::Refresh();
         std::cout << " o Refreshing Polarized XSec \n";
         fHelicityPSVar = (DiscretePhaseSpaceVariable*)GetPhaseSpace()->GetVariableWithName("helicity");
         fXSecCoefficients[0] = (1.0 - fBeamPolarization*fTargetPolarization);
         fXSecCoefficients[1] = (fBeamPolarization*fTargetPolarization)*(1.0 + fChargeAsymmetry );
         fXSecCoefficients[2] = (fBeamPolarization*fTargetPolarization)*(1.0 - fChargeAsymmetry );
         std::cout << "Done refreshing... " << std::endl;
      }

      /** Evaluate Cross Section. Flat cross section returns 1 */
      virtual Double_t EvaluateXSec(const Double_t * x) const;

      void     SetBeamPolarization(Double_t Pol) { fBeamPolarization = Pol; }
      Double_t GetBeamPolarization() { return(fBeamPolarization); }
      void     SetTargetPolarization(Double_t Pol) { fTargetPolarization = Pol; }
      Double_t GetTargetPolarization() {      return(fTargetPolarization); }

      void     SetChargeAsymmetry(Double_t dq) { fChargeAsymmetry = dq; }
      Double_t GetChargeAsymmetry() { return(fChargeAsymmetry); }

      void SetUnpolarizedCrossSection(InclusiveDiffXSec * sigma) { fCrossSections[0] = sigma; }
      void SetPolarizedPositiveCrossSection(InclusiveDiffXSec * sigma) { fCrossSections[1] = sigma; }
      void SetPolarizedNegativeCrossSection(InclusiveDiffXSec * sigma) { fCrossSections[2] = sigma; }

      ClassDef(PolarizedDiffXSec, 1)
};


/** A flat inclusive cross section with added target polarization angle.
 *
 * \ingroup EventGen
 */
class PolarizedDISXSec : public InclusiveDiffXSec {
   protected:
      Double_t fThetaTarget;

   public:
      PolarizedDISXSec() {
         fnDim = 3;
         fThetaTarget = 180.0 * TMath::Pi() / 180.0;
      }

      virtual ~PolarizedDISXSec() { };
      virtual PolarizedDISXSec*  Clone(const char * newname) const {
         std::cout << "PolarizedDISXSec::Clone()\n";
         auto * copy = new PolarizedDISXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual PolarizedDISXSec*  Clone() const { return( Clone("") ); } 

      /** Evaluate Cross Section. Flat cross section returns 1 */
      virtual Double_t EvaluateXSec(const Double_t * x) const {
         return(1.0);
      }

      void SetTargetPolarizationAngle(Double_t theta) { fThetaTarget = theta; }
      Double_t GetTargetPolarizationAngle() { return(fThetaTarget); }

      ClassDef(PolarizedDISXSec, 1)
};


/** Polarized cross section for electron scattering with incident helicity parallel to beam direction.
 *  Cross section is for an arbitrary target polarization direction in in the phi_target=0 plane. 
 *  Calculation by Whitney Armstrong (whit@temple.edu).
 *
 *  Uses W1,W2,G1,G2
 *
 * \ingroup EventGen
 * \ingroup xsections
 */
class PolarizedDISXSecParallelHelicity : public InclusiveDiffXSec {
   protected:
      Double_t fThetaTarget;

   public :
      PolarizedDISXSecParallelHelicity() { 
         fTitle = "PolarizedDISXSecParallelHelicity";
         fThetaTarget = 80.0 * TMath::Pi() / 180.0;
      }
      virtual ~PolarizedDISXSecParallelHelicity() {  }
      virtual PolarizedDISXSecParallelHelicity*  Clone(const char * newname) const {
         std::cout << "PolarizedDISXSecParallelHelicity::Clone()\n";
         auto * copy = new PolarizedDISXSecParallelHelicity();
         (*copy) = (*this);
         return copy;
      }
      virtual PolarizedDISXSecParallelHelicity*  Clone() const { return( Clone("") ); } 
      void     SetTargetPolarizationAngle(Double_t theta) { fThetaTarget = theta; }
      Double_t GetTargetPolarizationAngle() { return(fThetaTarget); }

      virtual Double_t EvaluateXSec(const Double_t * x) const {
         using namespace TMath;
         Double_t xSec = 0.0;
         Double_t Eprime = x[0];
         Double_t theta = x[1];
         Double_t phi = x[2];
         Double_t M = M_p/GeV;//M_p/GeV;
         Double_t E0 = GetBeamEnergy();
         Double_t Qsq = 4.0 * E0 * Eprime * TMath::Power(TMath::Sin(theta / 2.0), 2);
         Double_t xbjorken = Qsq / (2.0 * M * (E0 - Eprime));
         if (xbjorken < 0.0 || xbjorken > 1.0) return(0.0);
         Double_t W1 = fStructureFunctions->W1p(xbjorken, Qsq);
         Double_t W2 = fStructureFunctions->W2p(xbjorken, Qsq);
         Double_t G1 = fPolarizedStructureFunctions->G1p(xbjorken, Qsq);
         Double_t G2 = fPolarizedStructureFunctions->G2p(xbjorken, Qsq);
         xSec = 2*M*(2*Qsq*W1 + 4*E0*Eprime*W2 - Qsq*W2 - 2*Qsq*Cos(fThetaTarget)*
                (-2*E0*Eprime*G2 + E0*G1*M + Eprime*(2*E0*G2 + G1*M)*Cos(theta)) - 
                2*Eprime*(2*E0*G2 + G1*M)*Qsq*Cos(phi)*Sin(fThetaTarget)*Sin(theta));
         xSec = xSec * ((1.0 / 137.0) * (1.0 / 137.0) * Eprime) / (2.0 * M * Qsq * Qsq * E0);

         if (xSec < 0.0) xSec = 0.0;
         xSec = xSec*hbarc2_gev_nb;

	 if ( IncludeJacobian() ) return(xSec * TMath::Sin(theta));
         /*else*/return(xSec);
      }

      ClassDef(PolarizedDISXSecParallelHelicity, 1)
};



/** Polarized cross section for electron scattering with incident helicity anti-parallel to beam direction.
 *  Cross section is for an arbitrary target polarization direction in in the phi_target=0 plane. 
 *  Calculation by Whitney Armstrong (whit@temple.edu).
 *
 *  Uses W1,W2,G1,G2
 *
 * \ingroup EventGen
 * \ingroup xsections
 */
class PolarizedDISXSecAntiParallelHelicity : public InclusiveDiffXSec {
   protected:
      Double_t fThetaTarget;

   public :
      PolarizedDISXSecAntiParallelHelicity() {
         fTitle = "PolarizedDISXSecAntiParallelHelicity";
         fThetaTarget = 80.0 * TMath::Pi() / 180.0;
      }
      virtual ~PolarizedDISXSecAntiParallelHelicity() {  }
      virtual PolarizedDISXSecAntiParallelHelicity*  Clone(const char * newname) const {
         std::cout << "PolarizedDISXSecAntiParallelHelicity::Clone()\n";
         auto * copy = new PolarizedDISXSecAntiParallelHelicity();
         (*copy) = (*this);
         return copy;
      }
      virtual PolarizedDISXSecAntiParallelHelicity*  Clone() const { return( Clone("") ); } 
      void SetTargetPolarizationAngle(Double_t theta) { fThetaTarget = theta; }
      Double_t GetTargetPolarizationAngle() { return(fThetaTarget); }

      virtual Double_t EvaluateXSec(const Double_t * x) const {
         using namespace TMath;
         Double_t xSec = 1.0;
         Double_t Eprime = x[0];
         Double_t theta = x[1];
         Double_t phi = x[2];
         Double_t M = M_p/GeV;//M_p/GeV;
         Double_t E0 = GetBeamEnergy();
         Double_t Qsq = 4.0 * E0 * Eprime * TMath::Power(TMath::Sin(theta / 2.0), 2.0);
         Double_t xbjorken = Qsq / (2.0 * M * (E0 - Eprime));
         if (xbjorken < 0.0 || xbjorken > 1.0) return(0.0);
         Double_t W1 = fStructureFunctions->W1p(xbjorken, Qsq);
         Double_t W2 = fStructureFunctions->W2p(xbjorken, Qsq);
         Double_t G1 = fPolarizedStructureFunctions->G1p(xbjorken, Qsq);
         Double_t G2 = fPolarizedStructureFunctions->G2p(xbjorken, Qsq);
         xSec = 2*M*(2*Qsq*W1 + 4*E0*Eprime*W2 - Qsq*W2 + 2*Qsq*Cos(fThetaTarget)*(-2*E0*Eprime*G2 + E0*G1*M + Eprime*(2*E0*G2 + G1*M)*Cos(theta)) + 2*Eprime*(2*E0*G2 + G1*M)*Qsq*Cos(phi)*Sin(fThetaTarget)*Sin(theta));
         xSec = xSec * ((1.0 / 137.0) * (1.0 / 137.0) * Eprime) / (2.0 * M * Qsq * Qsq * E0);
         if (xSec < 0.0) xSec = 0.0;

         xSec = xSec*hbarc2_gev_nb;

	 if ( IncludeJacobian() ) return(xSec * TMath::Sin(theta));
         /*else*/return(xSec);
      }

      ClassDef(PolarizedDISXSecAntiParallelHelicity, 1)
};
}}
#endif

