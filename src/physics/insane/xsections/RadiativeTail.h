#ifndef RadiativeTail_HH 
#define RadiativeTail_HH 1

#include "RADCOR.h"
#include "POLRAD.h"
#include "InclusiveDiffXSec.h"

namespace insane {
namespace physics {
/** Base class for pulling together all the pieces and calculate a full radiative tail.
 * \deprecated
 */
class RadiativeTail : public InclusiveDiffXSec {

   protected:
      mutable POLRAD * fPOLRAD;  //->  Used to get the (internal) polarized 
      mutable RADCOR * fRADCOR;  //->  Used to get the (internal and external)
      Double_t       fRadLen[2];       ///< [0] before and [1] after scattering

      void CreatePOLRAD() const ;
      void CreateRADCOR() const ;

   public:

      RadiativeTail();
      virtual ~RadiativeTail();

      POLRAD * GetPOLRAD() const { if(!fPOLRAD) CreatePOLRAD(); return fPOLRAD; }
      RADCOR * GetRADCOR() const { if(!fRADCOR) CreateRADCOR(); return fRADCOR; }

      /** If only one argument is given, we assume it is the total target thickness.
       * Note: tb = "target before"
       *       ta = "target after"
       */
      virtual void SetTargetThickness(Double_t tb, Double_t ta = 0.0){
         if(ta == 0.0) {
            fRadLen[0] = tb/2.0;
            fRadLen[1] = tb/2.0;
         } else {
            fRadLen[0] = tb;
            fRadLen[1] = ta;
         }
      }

      /** Set the unpolarized structure functions to be used to calculate W1,W2,F1,F2, etc. */
      void SetUnpolarizedStructureFunctions(StructureFunctions * sf)  {
         GetPOLRAD()->SetUSFs(sf);
         GetRADCOR()->SetUnpolSFs(sf);
         InclusiveDiffXSec::SetUnpolarizedStructureFunctions(sf);
      }

      /** Set the QE structure functions to be used to calculate G1,G2,g1,g2, etc.  */
      void SetQEStructureFunctions(StructureFunctions * sf) {
         GetPOLRAD()->SetUQESFs(sf);
      }
      /** Set the polarized structure functions to be used to calculate G1,G2,g1,g2, etc.  */
      void SetPolarizedStructureFunctions(PolarizedStructureFunctions * sf) {
         GetPOLRAD()->SetPSFs(sf);
         GetRADCOR()->SetPolarizedStructureFunctions(sf);
         InclusiveDiffXSec::SetPolarizedStructureFunctions(sf);
      }

      /** Set the form factors. */
      void SetFormFactors(FormFactors * ff) {
         GetPOLRAD()->SetFFs(ff);
         GetRADCOR()->SetFormFactors(ff);
         InclusiveDiffXSec::SetFormFactors(ff);
      }

      /** Set the form factors used for nuclei. */
      void SetTargetFormFactors(FormFactors * ff) {
         GetPOLRAD()->SetPOLRADTargetFormFactors(ff);
         GetRADCOR()->SetTargetFormFactors(ff);
      }

      void SetTargetNucleus(const Nucleus& t){
         InclusiveDiffXSec::SetTargetNucleus(t);
         GetPOLRAD()->SetTargetNucleus(t); 
         GetRADCOR()->SetTargetNucleus(t);
      }
      void SetVerbosity(int v){
         GetPOLRAD()->SetVerbosity(v);
         GetRADCOR()->SetVerbosity(v);
      } 

      ClassDef(RadiativeTail,1)
};
}}
#endif

