#ifndef insane_physics_MAID_VPACs_HH
#define insane_physics_MAID_VPACs_HH 1

#include "VirtualPhotoAbsorptionCrossSections.h"

namespace insane {
  namespace physics {

    class MAID_VPACs : public VirtualPhotoAbsorptionCrossSections {
      private:
        mutable double sigT;
        mutable double sigL;
        mutable double sigLT;
        mutable double sigLTp;
        mutable double sigTT;
        mutable double sigTTp;
        mutable double sigL0;
        mutable double sigLT0;
        mutable double sigLT0p;

      public:
        MAID_VPACs();
        virtual ~MAID_VPACs();

        virtual void CalculateProton( double x, double Q2) const;
        virtual void CalculateNeutron(double x, double Q2) const;

        ClassDef(MAID_VPACs,1)
    };
  }
}

#endif

