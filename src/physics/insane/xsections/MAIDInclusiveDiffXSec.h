#ifndef MAIDInclusiveDiffXSec_H 
#define MAIDInclusiveDiffXSec_H 

#include <cstdlib> 
#include <iostream> 
#include <cmath> 
#include <vector> 
#include "TString.h"
#include "TVector3.h"
#include "TRotation.h"
#include "MAIDKinematicKey.h"
#include "InclusiveDiffXSec.h"

namespace insane {
namespace physics {
/** MAID cross section.
 * 
 * Paper reference: J. Phys. G: Nucl. Part. Phys 18, 449 (1992)  
 * For the reaction e N --> e' N' pi, where the user specifies pi and N'. 
 * Uses data from the MAID 2007 calculations.
 * 
 * Refs: 
 *    http://wwwkph.kph.uni-mainz.de/MAID//maid2007/maid2007.html
 *    http://arxiv.org/pdf/nucl-th/9909068.pdf
 *
 *
 * \ingroup inclusiveXSec
 */ 
class MAIDInclusiveDiffXSec: public InclusiveDiffXSec{

   private:
      TString fFileName,fConventionName,fprefix; 
      TString fPion,fNucleon,frxn; 

      bool fDebug;

      Int_t fConvention;       ///< Convention for virtual photon flux
      Int_t fNumberOfPoints;   ///< Grid size

      Double_t fh;             ///< Electron helicity
      TVector3         fP_t;   ///< Target polarization vector in lab coordinates (not scatting plane coords)
      mutable Double_t fPx;    ///< Perpendicular to virtual photon in scattering plane
      mutable Double_t fPy;    ///< Perpendicular to the scattering plane
      mutable Double_t fPz;    ///< Parallel to the virtual photon in scattering plane

      mutable TOrdCollection fGridData;
      mutable MAIDKinematicKey *fKineKey;
      mutable std::vector<Double_t> fEprime,fTheta,fNu,fW,fQ2; 
      mutable std::vector<Double_t> fXS_L,fXS_T,fXS_LT,fXS_LTpr,fXS_TTpr,fXS_L0,fXS_LT0,fXS_LT0pr,fXSf;  
      
      Double_t EvaluateFluxDensity(Double_t,Double_t) const; 
      Double_t EvaluateVirtualPhotonFlux(Double_t,Double_t,Double_t) const; 
      Double_t ComputeXSecForBin(Int_t,Int_t,Double_t,Double_t,Double_t,Double_t) const;
      void     BinarySearch(std::vector<Double_t> *,Double_t,Int_t &,Int_t &) const;
 
   public: 
      /** Constructor which sums over final state pions given the target nucleon.
       */
      MAIDInclusiveDiffXSec(const char *target = "p"); 
      /** Constructor which requires the final state pion and nucleon.
       */
      MAIDInclusiveDiffXSec(const char *pion, const char *nucleon); 
      virtual ~MAIDInclusiveDiffXSec();
      //virtual MAIDInclusiveDiffXSec*  Clone(const char * newname) const {
      //   std::cout << "MAIDInclusiveDiffXSec::Clone()\n";
      //   MAIDInclusiveDiffXSec * copy = new MAIDInclusiveDiffXSec();
      //   (*copy) = (*this);
      //   return copy;
      //}
      //virtual MAIDInclusiveDiffXSec*  Clone() const { return( Clone("") ); } 
      Double_t   EvaluateXSec(const Double_t *) const; 
      void       ImportData();
      void       DebugMode(bool ans){fDebug = ans;}   
      void       Print();  
      void       PrintData();  
      void       PrintParameters();  
      void       Clear(); 
      void       SetVirtualPhotonFluxConvention(Int_t i);
      void       SetReactionChannel(TString pion,TString nucleon);
      Double_t   GetHelicity() const { return fh; }
      void       SetHelicity(Double_t h) { 
         if(TMath::Abs(h) > 1.0){
            Error("SetHelicity","Argument too big: |h|<=1.");
         }else{ 
            fh = h;
         }
      }
      TVector3   GetTargetPolarization(){ return fP_t; }
      void       SetTargetPolarization(const TVector3 &P){
         fP_t = P; 
         if(P.Mag() > 1.0) {
            Error("SetTargetPolarization","Magnitude too big. Setting to 1.");
            fP_t.SetMag(1.0);
         }
      }

      ClassDef(MAIDInclusiveDiffXSec,1)
};

}}
#endif 
