#ifndef POLRADQuasiElasticTailDiffXSec_HH
#define POLRADQuasiElasticTailDiffXSec_HH 1

#include "POLRADBornDiffXSec.h"
namespace insane {
namespace physics {

/**  Quasi-Elastic Radiative Taili cross section. 
 *  
 *
 * \ingroup inclusiveXSec
 */
class POLRADQuasiElasticTailDiffXSec: public POLRADBornDiffXSec {

   public:

      POLRADQuasiElasticTailDiffXSec();
      virtual ~POLRADQuasiElasticTailDiffXSec();
      virtual POLRADQuasiElasticTailDiffXSec*  Clone(const char * newname) const {
         std::cout << "POLRADQuasiElasticTailDiffXSec::Clone()\n";
         auto * copy = new POLRADQuasiElasticTailDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual POLRADQuasiElasticTailDiffXSec*  Clone() const { return( Clone("") ); } 
      virtual Double_t EvaluateXSec(const Double_t *x) const ; 

      ClassDef(POLRADQuasiElasticTailDiffXSec,1)
};
//____________________________________________________________________________________

}}
#endif

