#ifndef RADCOR2_HH 
#define RADCOR2_HH 1 

#include "TObject.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/GaussIntegrator.h"
#include "Math/IFunction.h"
#include "Math/Functor.h"

#include "RADCORKinematics.h"
#include "PhysicalConstants.h"
#include "InSANEMathFunc.h"
#include "Nucleus.h"
#include "DiffXSec.h"
#include "InclusiveDiffXSec.h"
#include "FormFactors.h"
#include "StructureFunctions.h"
#include "PolarizedCrossSectionDifference.h"

namespace insane {
namespace physics {

/** Class to handle radiative corrections to elastic and inelastic electron (and muon) scattering.
 *
 * References: 
 *   Mo and Tsai, Rev.Mod.Phys. 1969
 *   Tsai, SLAC-PUB-848. 1971
 *   Stein, et.al.
 */
class RADCOR2 : public TObject{

   private:
      RADCORKinematics     fKinematics;

      DiffXSec  * fXS;           //! Born cross sections
      Double_t                   fDelta;        ///< Energy peaking approximation (strip) width
      Double_t                   fX0;
      Double_t                   fDeltaM;       ///< Mass offset to avoid elastic peak in integration (nuclear target) 

      Int_t                      fIntegThresh;  ///< Integration threshold: 0 = pion threshold (IRT, nucleon target), 
                                                ///<                        1 = QE threshold (nuclear target)  
                                                ///<                        2 = elastic threshold (nuclear target)  
      Bool_t                     fEquivRad;     // If true the equivalent radiator is calculated. Used internally for InternalOnly_ and ExternalOnly_ 

   protected:
      Double_t Delta(Double_t E, Double_t t) const ;            ///< Most probable Eloss due to ionization. Tsai (1971) Eqn 1.8 or Eqn 1.9
      Double_t Ib(Double_t E0, Double_t E, Double_t t) const ;  ///< Straggling function (Eq B43 of Tsai's SLAC-PUB-0848)  

      Double_t GetEpMax(Double_t Es,Double_t theta,Double_t M_thresh = 0.134) const ; ///< IRT integration limit. MoTsai A18
      Double_t GetEsMin(Double_t Ep,Double_t theta,Double_t M_thresh = 0.134) const ; ///< IRT integration limit. MoTsai A19
      Double_t GetEpMaxALT(Double_t Es,Double_t theta,Double_t MT=0.938,Double_t M_thresh = 0.134) const ; ///< Generalized integration limit. MoTsai A18
      Double_t GetEsMinALT(Double_t Ep,Double_t theta,Double_t MT=0.938,Double_t M_thresh = 0.134) const ; ///< Generalized integration limit. MoTsai A19

      Double_t GetFTildeExactInternal(Double_t q2,Double_t Tb=0,Double_t Ta=0);    /// Stein [PRD 12, 1884 (1975) Eq A44]  

      void     SetKinematics(Double_t Ebeam, Double_t Eprime, Double_t theta, Double_t phi=0); 

   public: 
      RADCOR2();
      virtual ~RADCOR2();

      Double_t GetTr(Double_t) const ;                          ///< Equiv.Rad. Approx thickness for internal
      Double_t GetPhi(Double_t) const ;                         ///< shape of bremsstrahlung for small v (complete screening)

      void                      SetCrossSection(DiffXSec *f) {fXS = f;}
      void                      SetXSec(DiffXSec *f){fXS = f;}
      DiffXSec * GetXSec(){return fXS;}
      DiffXSec * GetCrossSection(){return fXS;}

      void                      SetRadiationLengths(const Double_t *); 
      void                      SetRadiationLengths(double, double); 
      RADCORKinematics *  GetKinematics(){ return &fKinematics ; }

      Double_t                  GetDelta() const { return fDelta;}
      void                      SetDelta(Double_t d) {fDelta = d;}
      void                      SetIntegrationThreshold(Int_t thr){fIntegThresh = thr;} 
      void                      SetDeltaM(Double_t m){fDeltaM = m;} 

      //Double_t GetOmegaMax(Double_t);                                   ///< omega_max from Mo & Tsai (Eq B10)  
      //Double_t GetMfSq(Double_t,Double_t);                              ///< Wsq from Mo & Tsai (Eq B9)  
      //Double_t F_soft(Double_t,Double_t,Double_t); 
      //Double_t GetTr(Double_t);                                         ///< Tr(Q2): changes PER INTEGRATION BIN 
      //Double_t GetFTilde(Double_t);                                     ///< FTilde(Q2): changes PER INTEGRATION BIN 
      //Double_t GetFTildeExactInternal(Double_t);                        ///< Same as above, just using exact internal variables 
      //Double_t GetSpence(Double_t);                                     ///< Spence(x), x = arbitrary value
      //Double_t GetSmearFunc(Double_t);                                  ///< Fermi smearing function 

      /** @name For the peaking approximation
       *
       * @{
       */
      //Double_t ElasticTail(Double_t,Double_t,Double_t); 
      //Double_t EnergyPeakingApprox(Double_t,Double_t,Double_t);
      //Double_t CalculateEsIntegral();
      //Double_t CalculateEpIntegral();
      //Double_t EsIntegrand(Double_t &);
      //Double_t EpIntegrand(Double_t &);
      //Double_t sigma_p();                                               ///< Elastic tail [internal] -- NO INTEGRAL  
      //Double_t sigma_b();                                               ///< Elastic tail [external] -- NO INTEGRAL  
      //Double_t sigma_qe_b();                                            ///< Quasi-Elastic tail [external] -- NO INTEGRAL  
      //Double_t sigma_el(Double_t,Double_t); 
      //Double_t sigma_qe(Double_t,Double_t); 
      //Double_t sigma_el_tilde(Double_t,Double_t); 
      //Double_t sigma_qe_tilde(Double_t,Double_t);
      //@} 

      /** @name For the exact integral. 
       * 
       * @{
       */
      ///// Stuff we're using 
      //Double_t Exact(Double_t,Double_t,Double_t);
      //Double_t InternalIntegrand_MoTsai69(Double_t);                    ///< Elastic tail integrand  
      //Double_t InternalIntegrand_Inelastic_MoTsai69(Double_t *);        ///< Inelastic tail integrand 
      //Double_t EpExactIntegrand_4(Double_t *);
      //Double_t ExternalOnly_ExactInelasticIntegrand(Double_t *);
      //Double_t EpExactIntegrand_Inelastic_Term_1(Double_t *);
      //Double_t EpExactIntegrand_Inelastic_Term_2(Double_t *);            
      //Double_t Ib_2(Double_t,Double_t,Double_t);                        ///< Straggling function (Eq B43 of Tsai's SLAC-PUB-0848)  
      //Double_t Delta_r(Double_t);                                       ///< Multiple photon term (from Mo & Tsai) 
      //Double_t Delta_inf_1(Double_t);                                   ///< Multiple photon infrared divergent component (form INFERRED from Mo & Tsai)  
      //Double_t Delta_inf_2();                                           ///< Multiple photon infrared divergent component (form from POLRAD) 
      //Double_t Delta(Double_t,Double_t);                                ///< Energy loss applied before doing full RCs  
      //Double_t Delta_0(Double_t,Double_t);                            
      ///// New stuff  
      //void     SetIntegrationParametersElastic(Int_t,Double_t,Double_t,Double_t,Int_t &,Double_t *,Double_t *); 
      //void     SetIntegrationParametersInelastic(Int_t,Double_t,Double_t,Double_t,Int_t &,Double_t *,Double_t *,Int_t &,Double_t *,Double_t *,Int_t region = 0);
      //Double_t ExactElasticInternalOnly(Double_t,Double_t,Double_t);   
      //Double_t ExactElasticInternalExternal(Double_t,Double_t,Double_t);   
      //Double_t ExactElasticExternalOnly(Double_t,Double_t,Double_t);   
      //Double_t ExactInelasticInternalOnly(Double_t,Double_t,Double_t);   
      //Double_t ExactInelasticInternalExternal(Double_t,Double_t,Double_t);   
      //Double_t ExactInelasticExternalOnly(Double_t,Double_t,Double_t);   


      //Double_t ExternalOnly_Plot(double *x,double *p){
      //   Double_t Ep  = x[0];
      //   Double_t Es  = p[0];
      //   Double_t th  = p[1];
      //   Double_t res = ExternalOnly_ExactElasticTail(Es,Ep,th); 
      //   if( TMath::IsNaN(res) ) res = 0.0;
      //   return res;  
      //}
      Double_t EquivRad2DEnergyIntegral(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0.0);
      Double_t EquivRad2DEnergyIntegral_RegionIV(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0.0);
      Double_t InternalOnly_EquivRad2DEnergyIntegral(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0.0);
      Double_t ExternalOnly_2DEnergyIntegral(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0.0);
      Double_t External2DEnergyIntegral_Integrand(Double_t Es,Double_t Ep,Double_t theta,Double_t phi,Double_t t1,Double_t t2,Double_t Esprime,Double_t EpPrime);
      class External2DEnergyIntegral_IntegrandWrap : public ROOT::Math::IBaseFunctionMultiDim {
         public:
            mutable Double_t Es;
            mutable Double_t Ep;
            mutable Double_t theta;
            mutable Double_t phi;
            mutable Double_t t1;
            mutable Double_t t2;
            mutable RADCOR2 * fRADCOR;
            double DoEval(const double* x) const {
              return fRADCOR->External2DEnergyIntegral_Integrand(Es,Ep,theta,phi,t1,t2,x[0],x[1]);
            }
            unsigned int NDim() const { return 2; }
            ROOT::Math::IBaseFunctionMultiDim* Clone() const { return new External2DEnergyIntegral_IntegrandWrap(); }
            ClassDef(External2DEnergyIntegral_IntegrandWrap,1)
      };

      Double_t EquivRad3DIntegral(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0.0);
      Double_t EquivRad3DIntegral_RegionIV(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0.0);
      Double_t External3DIntegral_Integrand(Double_t Es,Double_t Ep,Double_t theta,Double_t phi,Double_t t1,Double_t t2,Double_t Esprime,Double_t EpPrime);
      class External3DIntegral_IntegrandWrap : public ROOT::Math::IBaseFunctionMultiDim {
      public:
        mutable Double_t Es;
        mutable Double_t Ep;
        mutable Double_t theta;
        mutable Double_t phi;
        mutable Double_t T;
        mutable RADCOR2 * fRADCOR;
        double DoEval(const double* x) const {
          return fRADCOR->External3DIntegral_Integrand(Es,Ep,theta,phi,x[2],T-x[2],x[0],x[1]);
        }
        unsigned int NDim() const { return 3; }
        ROOT::Math::IBaseFunctionMultiDim* Clone() const { return new External3DIntegral_IntegrandWrap(); }
        ClassDef(External3DIntegral_IntegrandWrap,1)
      };

      Double_t ExternalOnly_ElasticPeak(Double_t Es,Double_t Ep,Double_t theta, Double_t phi=0.0);
      Double_t ExternalOnly_ElasticTail(Double_t Es,Double_t Ep,Double_t theta, Double_t phi=0.0);
      Double_t ExternalOnly_ElasticTail_sPeak(Double_t Es,Double_t Ep,Double_t theta, Double_t phi=0.0);

      Double_t InternalOnly_EquivRadContinuumStragglingStripApprox(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0.0);
      Double_t ExternalOnly_EquivRadContinuumStragglingStripApprox(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0.0);
      Double_t EquivRadContinuumStragglingStripApprox(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0.0);
      Double_t StripApproxIntegrand1(Double_t Es,  Double_t Ep, Double_t theta, Double_t T, Double_t EsPrime);
      Double_t StripApproxIntegrand1_withD(Double_t Es,  Double_t Ep, Double_t theta, Double_t T, Double_t EsPrime);
      Double_t StripApproxIntegrand2(Double_t Es,  Double_t Ep, Double_t theta, Double_t T, Double_t EpPrime);

      class StripApproxIntegrand1_withDWrap : public ROOT::Math::IBaseFunctionOneDim {
      public:
        mutable RADCOR2 * fRADCOR;
        mutable Double_t Es;
        mutable Double_t Ep;
        mutable Double_t T;
        mutable Double_t theta;
        double DoEval(double x) const {
          double res = fRADCOR->StripApproxIntegrand1_withD(Es,Ep,theta,T,x);
          //std::cout << " sig1 = " << res << std::endl;
          return res ;
        }
        unsigned int NDim() const { return 1; }
        ROOT::Math::IBaseFunctionOneDim* Clone() const { return new StripApproxIntegrand1_withDWrap(); }
        ClassDef(StripApproxIntegrand1_withDWrap,1)
      };
      class StripApproxIntegrand1Wrap : public ROOT::Math::IBaseFunctionOneDim {
      public:
        mutable RADCOR2 * fRADCOR;
        mutable Double_t Es;
        mutable Double_t Ep;
        mutable Double_t T;
        mutable Double_t theta;
        double DoEval(double x) const {
          double res = fRADCOR->StripApproxIntegrand1(Es,Ep,theta,T,x);
          //std::cout << " sig1 = " << res << std::endl;
          return res ;
        }
        unsigned int NDim() const { return 1; }
        ROOT::Math::IBaseFunctionOneDim* Clone() const { return new StripApproxIntegrand1Wrap(); }
        ClassDef(StripApproxIntegrand1Wrap,1)
      };
      class StripApproxIntegrand2Wrap : public ROOT::Math::IBaseFunctionOneDim {
      public:
        mutable RADCOR2 * fRADCOR;
        mutable Double_t Es;
        mutable Double_t Ep;
        mutable Double_t T;
        mutable Double_t theta;
        double DoEval(double x) const {
          double res = fRADCOR->StripApproxIntegrand2(Es,Ep,theta,T,x);
          //std::cout << " sig2 = " << res << std::endl;
          return res ;
        }
        unsigned int NDim() const { return 1; }
        ROOT::Math::IBaseFunctionOneDim* Clone() const { return new StripApproxIntegrand2Wrap(); }
        ClassDef(StripApproxIntegrand2Wrap,1)
      };
      //class ExternalOnly_ExactInelasticIntegrandWrap : public ROOT::Math::IBaseFunctionMultiDim {
      //   public:
      //      mutable RADCOR2 * fRADCOR;
      //      double DoEval(const double* x) const {
      //         Double_t y[] = {x[0],x[1],x[2]};
      //         return fRADCOR->ExternalOnly_ExactInelasticIntegrand(y);
      //      }
      //      unsigned int NDim() const { return 3; }
      //      ROOT::Math::IBaseFunctionMultiDim* Clone() const { return new ExternalOnly_ExactInelasticIntegrandWrap(); }
      //      ClassDef(ExternalOnly_ExactInelasticIntegrandWrap,1)
      //};


      ClassDef(RADCOR2,1) 
}; 

}
}

#endif 

