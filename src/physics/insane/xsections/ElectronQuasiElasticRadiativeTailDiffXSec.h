#ifndef ElectronQuasiElasticRadiativeTailDiffXSec_H
#define ElectronQuasiElasticRadiativeTailDiffXSec_H 1

#include "TMath.h"
#include "FormFactors.h"
#include "TVector3.h"
#include "QuasiElasticInclusiveDiffXSec.h"
#include "ExternalRadiator.h"

namespace insane {
   namespace physics {
/** Elastic scattering using the Dipole FFs.
 * \ingroup exclusiveXSec
 */
class ElectronQuasiElasticRadiativeTailDiffXSec : public ExternalRadiator<QuasiElasticInclusiveDiffXSec> {
   protected:

   public:
      ElectronQuasiElasticRadiativeTailDiffXSec();
      virtual ~ElectronQuasiElasticRadiativeTailDiffXSec();
      ElectronQuasiElasticRadiativeTailDiffXSec(const ElectronQuasiElasticRadiativeTailDiffXSec& rhs)            = default;
      ElectronQuasiElasticRadiativeTailDiffXSec& operator=(const ElectronQuasiElasticRadiativeTailDiffXSec& rhs) = default;
      ElectronQuasiElasticRadiativeTailDiffXSec(ElectronQuasiElasticRadiativeTailDiffXSec&&)                     = default;
      ElectronQuasiElasticRadiativeTailDiffXSec& operator=(ElectronQuasiElasticRadiativeTailDiffXSec&&)          = default;

      virtual ElectronQuasiElasticRadiativeTailDiffXSec*  Clone(const char * newname) const {
         std::cout << "ElectronQuasiElasticRadiativeTailDiffXSec::Clone()\n";
         auto * copy = new ElectronQuasiElasticRadiativeTailDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual ElectronQuasiElasticRadiativeTailDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual void InitializePhaseSpaceVariables();

      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      virtual Double_t   EvaluateXSec(const Double_t * x) const;
      //virtual void       DefineEvent(Double_t * vars);

      //DiffXSec * GetBornXSec() { return fElasticDiffXSec; }

      double GetTheta_e(double Es, double alpha) const;
      double GetPalpha(double Es, double alpha) const;
      double ElectronToAlphaJacobian(double Es, double alpha) const ;
      double Ib(Double_t E0,Double_t E,Double_t t) const ;

      double Sig_el(double Es, double Ep, double theta) const;

      ClassDef(ElectronQuasiElasticRadiativeTailDiffXSec, 1)
};
}
}


#endif

