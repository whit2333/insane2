#ifndef MAIDPhotoAbsorptionCrossSections_HH
#define MAIDPhotoAbsorptionCrossSections_HH

#include "PhotoAbsorptionCrossSections.h"

namespace insane {
  namespace physics {

    class MAIDPhotoAbsorptionCrossSections : public PhotoAbsorptionCrossSections {

    private:
      double sigT;
      double sigL;
      double sigLT;
      double sigLTp;
      double sigTT;
      double sigTTp;
      double sigL0;
      double sigLT0;
      double sigLT0p;

    public:
      MAIDPhotoAbsorptionCrossSections(
        const char * n = "MAIDPhotoAbsorptionCrossSections",
        const char * t = "MAID07 PhotoAbsorption cross sections");
      virtual ~MAIDPhotoAbsorptionCrossSections();

      virtual void CalculateProton( Double_t x, Double_t Q2);
      virtual void CalculateNeutron(Double_t x, Double_t Q2);

      ClassDef(MAIDPhotoAbsorptionCrossSections,1)
    };
  }}
#endif
