#ifndef InclusiveBornDISXSec_HH
#define InclusiveBornDISXSec_HH 1

#include "insane/base/InclusiveDiffXSec.h"
#include "InelasticRadiativeTail2.h"
#include "insane/base/FortranWrappers.h"
#include "insane/base/InSANEMathFunc.h"


namespace insane {
  namespace physics {

    /** Simply uses any structure function.
     *
     * \ingroup inclusiveXSec
     */
    class InclusiveBornDISXSec : public InclusiveDiffXSec {

    public:
      InclusiveBornDISXSec();
      virtual ~InclusiveBornDISXSec();
      InclusiveBornDISXSec(const InclusiveBornDISXSec& rhs);
      InclusiveBornDISXSec& operator=(const InclusiveBornDISXSec& rhs);
      virtual InclusiveBornDISXSec*  Clone(const char * newname) const;
      virtual InclusiveBornDISXSec*  Clone() const;

      virtual Double_t EvaluateXSec(const Double_t *x) const;

      ClassDef(InclusiveBornDISXSec,1)
    };

    namespace xs {
    }

  }
}

#endif

