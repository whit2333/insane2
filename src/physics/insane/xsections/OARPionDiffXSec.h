#ifndef OARPionDiffXSec_HH
#define OARPionDiffXSec_HH 1

#include "TMath.h"
#include "InclusiveDiffXSec.h"
#include "insane/base/FortranWrappers.h"
#include "CompositeDiffXSec.h"

namespace insane {
  namespace physics {

    /** Oscars fit to pion photo and electro production data.
     *
     *  This appears to be the photoproduction cross section
     *  Only pi0 for now.
     *
     * \ingroup inclusiveXSec
     */
    class OARPionDiffXSec : public InclusiveDiffXSec {

      protected:
        Double_t       fa;
        Double_t       fC;

        Double_t       fa_m;  // pi-
        Double_t       fC_m;  // pi-

        Double_t       fa_p;  // pi+
        Double_t       fC_p;  // pi+

        Double_t       fRadiationLength;

        Double_t OARFitFunc(Double_t Pt) const ;

      public:
        OARPionDiffXSec();
        virtual ~OARPionDiffXSec();
        virtual OARPionDiffXSec*  Clone(const char * newname) const {
          std::cout << "OARPionDiffXSec::Clone()\n";
          auto * copy = new OARPionDiffXSec();
          (*copy) = (*this);
          return copy;
        }
        virtual OARPionDiffXSec*  Clone() const { return( Clone("") ); } 

        Double_t OARFitFunc(Double_t Pt, Double_t a, Double_t C) const ;

        Double_t GetRadiationLength(){ return fRadiationLength; }
        void     SetRadiationLength(Double_t r) { fRadiationLength = r; }

        Double_t EvaluateXSec(const Double_t * x) const ;

        virtual void InitializePhaseSpaceVariables();

        //unsigned int NDim() const { return fnDim; }

        ClassDef(OARPionDiffXSec,1)
    };


    /** Electroproduction.
     *
     * \ingroup inclusiveXSec
     */
    class OARPionElectroDiffXSec : public OARPionDiffXSec  {

      public:
        OARPionElectroDiffXSec();
        virtual ~OARPionElectroDiffXSec();
        virtual OARPionElectroDiffXSec*  Clone(const char * newname) const {
          std::cout << "OARPionElectroDiffXSec::Clone()\n";
          auto * copy = new OARPionElectroDiffXSec();
          (*copy) = (*this);
          return copy;
        }
        virtual OARPionElectroDiffXSec*  Clone() const { return( Clone("") ); } 

        /** Evaluate Cross Section (nbarn/GeV*str).  */
        Double_t EvaluateXSec(const Double_t * x) const ;

        ClassDef(OARPionElectroDiffXSec,1)
    };


    /** Photoproduction.
     *
     * \ingroup inclusiveXSec
     */
    class OARPionPhotoDiffXSec : public OARPionDiffXSec  {

      public:
        OARPionPhotoDiffXSec();
        virtual ~OARPionPhotoDiffXSec();
        virtual OARPionPhotoDiffXSec*  Clone(const char * newname) const {
          std::cout << "OARPionPhotoDiffXSec::Clone()\n";
          auto * copy = new OARPionPhotoDiffXSec();
          (*copy) = (*this);
          return copy;
        }
        virtual OARPionPhotoDiffXSec*  Clone() const { return( Clone("") ); } 

        /** Evaluate Cross Section (nbarn/GeV*str).  */
        Double_t EvaluateXSec(const Double_t * x) const ;

        ClassDef(OARPionPhotoDiffXSec,1)
    };


    /** OAR cross section for arbitrary nuclear target. 
     *
     * \ingroup inclusiveXSec
     */
    class ElectroOARPionDiffXSec : public CompositeDiffXSec {

      public:
        ElectroOARPionDiffXSec();
        virtual ~ElectroOARPionDiffXSec();
        virtual ElectroOARPionDiffXSec*  Clone(const char * newname) const {
          std::cout << "ElectroOARPionDiffXSec::Clone()\n";
          auto * copy = new ElectroOARPionDiffXSec();
          (*copy) = (*this);
          return copy;
        }
        virtual ElectroOARPionDiffXSec*  Clone() const { return( Clone("") ); } 
        virtual Double_t  EvaluateXSec(const Double_t * x) const;

        void SetParticlePDGEncoding(Int_t PDGcode) { SetParticleType(PDGcode); }

        ClassDef(ElectroOARPionDiffXSec,1)
    };


    ///** OAR cross section for arbitrary nuclear target. 
    // *
    // * \ingroup inclusiveXSec
    // */
    //class PhotoOARPionDiffXSec : public CompositeDiffXSec {
    //  public:
    //    PhotoOARPionDiffXSec();
    //    virtual ~PhotoOARPionDiffXSec();
    //    virtual PhotoOARPionDiffXSec*  Clone(const char * newname) const {
    //      std::cout << "PhotoOARPionDiffXSec::Clone()\n";
    //      auto * copy = new PhotoOARPionDiffXSec();
    //      (*copy) = (*this);
    //      return copy;
    //    }
    //    virtual PhotoOARPionDiffXSec*  Clone() const { return( Clone("") ); } 

    //    Double_t GetRadiationLength(){ 
    //      return( ((OARPionDiffXSec*)fProtonXSec)->GetRadiationLength());
    //    }
    //    void   SetRadiationLength(Double_t r) { 
    //      ((OARPionDiffXSec*)fProtonXSec )->SetRadiationLength(r);
    //      ((OARPionDiffXSec*)fNeutronXSec)->SetRadiationLength(r);
    //    }
    //    virtual Double_t  EvaluateXSec(const Double_t * x) const;
    //    ClassDef(PhotoOARPionDiffXSec,1)
    //};

  }
}

#endif

