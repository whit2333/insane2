#ifndef RADCORInternalUnpolarizedDiffXSec_HH 
#define RADCORInternalUnpolarizedDiffXSec_HH 1 

#include "TMath.h"
#include "InclusiveDiffXSec.h"
#include "F1F209eInclusiveDiffXSec.h"
#include "RADCOR.h"
#include "FormFactors.h"
#include "MSWFormFactors.h"

namespace insane {
namespace physics {
/** RADCOR cross section.
 *
 *  Current Status: unknown
 *
 * \ingroup inclusiveXSec
 */
class RADCORInternalUnpolarizedDiffXSec: public InclusiveDiffXSec{

	public: 
		RADCORInternalUnpolarizedDiffXSec();
		virtual ~RADCORInternalUnpolarizedDiffXSec();
      virtual RADCORInternalUnpolarizedDiffXSec*  Clone(const char * newname) const {
         std::cout << "RADCORInternalUnpolarizedDiffXSec::Clone()\n";
         auto * copy = new RADCORInternalUnpolarizedDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual RADCORInternalUnpolarizedDiffXSec*  Clone() const { return( Clone("") ); } 

                Double_t EvaluateXSec(const Double_t *) const;

		RADCOR *fRADCOR;

                void SetVerbosity(int v){fRADCOR->SetVerbosity(v);} 
                void SetMultiplePhoton(Int_t c){fRADCOR->SetMultiplePhoton(c);}

                // Double_t *fEprime_var; //!
                // Double_t *fTh_var;     //!
                // Double_t *fPhi_var;    //!

                // void InitializePhaseSpaceVariables(); 

                ClassDef(RADCORInternalUnpolarizedDiffXSec,1) 

}; 
}}

#endif 
