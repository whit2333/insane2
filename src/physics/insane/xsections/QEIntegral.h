#ifndef QEIntegral_H 
#define QEIntegral_H 1 

#include <cstdlib> 
#include "InclusiveDiffXSec.h"
#include "FormFactors.h"
#include "FermiMomentumDist.h"
#include "Nucleus.h"
#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/GaussIntegrator.h"
#include <Math/IFunction.h>
#include <Math/Functor.h>

namespace insane {
   namespace physics {

/** QE angle integral and used by QuasiElasticInclusiveDiffXSec. 
 *
 *  References: 
 *  See Bosted and Mamyan, arXiv: 1203.2262v2, Eq 11
 */
class QEIntegral{

   public: 
      bool     fIsDebug; 
      Double_t fEs,fEp,fth,fZ,fA,fMT;
      Double_t fKF,fEShift; 

      FormFactors *fFF;

      Nucleus fNucleus; 
      insane::physics::FermiMomentumDist fFermiMomentum;
      Double_t Psi(Double_t,Double_t,Double_t EShift=0);  
      Double_t ScalingFunction(Double_t,Double_t); 
      void SetParameters(Int_t); 

   public: 
      QEIntegral(); 
      virtual ~QEIntegral(); 


      Double_t EvaluateIntegral(Double_t,Double_t,Double_t); 
      Double_t Integrand(Double_t COS_cm); 

      void IsDebug(bool ans){fIsDebug = ans;} 

      void SetTargetNucleus(const Nucleus& t){ 
         fNucleus = t;
         fMT      = fNucleus.GetMass(); 
         fA       = fNucleus.GetA();  
         fZ       = fNucleus.GetZ(); 
         SetParameters( Int_t(fA) );  
      } 

      void SetFormFactors(FormFactors *f){ fFF = f; } 


      ClassDef(QEIntegral,1) 
};

/** Wrapper class!!
 * IBaseFunctionOneDim
 */
class QEFuncWrap : public ROOT::Math::IBaseFunctionOneDim {

   public:
      QEFuncWrap(){}
      virtual ~QEFuncWrap(){}
      mutable QEIntegral *QEXS;

      double DoEval(double x) const {
         double res = QEXS->Integrand(x);
         return res;
      }

      unsigned int NDim() const { return 1; }
      ROOT::Math::IBaseFunctionOneDim* Clone() const { return new QEFuncWrap(); }

      ClassDef(QEFuncWrap,1)
};

}
}

#endif 

