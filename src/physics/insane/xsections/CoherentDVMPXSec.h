#ifndef CoherentDVMPXSec_H
#define CoherentDVMPXSec_H 1

#include "ExclusiveDiffXSec.h"
#include "InclusiveDiffXSec.h"
#include "TMath.h"
#include "FormFactors.h"
#include "TVector3.h"

#include "DipoleFormFactors.h"


namespace insane {
   namespace physics {

      /** Elastic scattering using the Dipole FFs.
       * \ingroup exclusiveXSec
       */
      class CoherentDVMPXSec : public ExclusiveDiffXSec {

         protected:

            struct dvcsvar_t { double Q2  , t    , x , phi; };
            struct dvcspar_t { double Q2_0, alpha, b , beta    , c , d; };
            //______________________________________________________________________________
            //                                 q20  alpha      b     beta    c     d
            //const dvcspar_t PAR_COHDVCS = {  1.0,  2.0,  -8.8,     7.3,   0.3,  0.4}; // b,beta from t-fit to tr
            //const dvcspar_t PAR_INCDVCS   = {  1.0,  2.0,  -1.408,   4.0,   0.3,  0.4};
            dvcspar_t PAR_COHDVCS   = {  1.0,  2.0,  -0.667,  35.1,   0.3,  0.4}; // b,beta from t-fit to tc
            dvcspar_t PAR_INCDVCS   = {  1.0,  1.5,  -1.408,   4.0,   0.2,  0.4};
            dvcspar_t PAR_COHPI0    = {  1.0,  3.0,  -8.8,     7.3,   0.3,  0.0};
            dvcspar_t PAR_INCPI0    = {  1.0,  3.0,  -1.408,   4.0,   0.3,  0.0};

            // What are the cross section units?
            double xsec_e1dvcs(const dvcsvar_t &x,const dvcspar_t &p) const;

            mutable DipoleFormFactors   fDipoleFFs;
            dvcsvar_t fRefDVCSVars  = { 1.0  , -0.001  , 1.0 , 0.0 };

            double xsec_normalization() const {
               double E0       = GetBeamEnergy();
               double M        = 4.0*0.938;
               double nu       = fRefDVCSVars.Q2/(2.0*M);
               double theta    = insane::kine::Theta_xQ2y(1.0,fRefDVCSVars.Q2,nu/E0,M);
               double sig_mott = insane::kine::Sig_Mott(E0,theta)*hbarc2_gev_nb;
               double Fc       = fDipoleFFs.FCHe4(fRefDVCSVars.Q2);
               double sig_el   = Fc*Fc*sig_mott;
               double res      = xsec_e1dvcs(fRefDVCSVars, PAR_COHDVCS);
               return(sig_el/res);
            }

         public:
            CoherentDVMPXSec();
            virtual ~CoherentDVMPXSec();
            CoherentDVMPXSec(const CoherentDVMPXSec& rhs) = default ;
            CoherentDVMPXSec& operator=(const CoherentDVMPXSec& rhs) = default ;
            CoherentDVMPXSec(CoherentDVMPXSec&&) = default;
            CoherentDVMPXSec& operator=(CoherentDVMPXSec&&) & = default; // Move assignment operator

            virtual CoherentDVMPXSec*  Clone(const char * newname) const {
               std::cout << "CoherentDVMPXSec::Clone()\n";
               auto * copy = new CoherentDVMPXSec();
               (*copy) = (*this);
               return copy;
            }
            virtual CoherentDVMPXSec*  Clone() const { return( Clone("") ); } 

            virtual void       InitializePhaseSpaceVariables() ;
            virtual void       DefineEvent(Double_t * vars);
            virtual Double_t * GetDependentVariables(const Double_t * x) const ;
            virtual Double_t   EvaluateXSec(const Double_t * x) const;

            /**  Returns the scattered electron energy using the angle. */
            Double_t GetEPrime(const Double_t theta) const ;

            ClassDef(CoherentDVMPXSec, 1)
      };

   }
}

#endif

