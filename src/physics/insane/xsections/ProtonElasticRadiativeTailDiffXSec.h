#ifndef ProtonElasticRadiativeTailDiffXSec_H
#define ProtonElasticRadiativeTailDiffXSec_H 1

#include "TMath.h"
#include "FormFactors.h"
#include "TVector3.h"
//#include "ProtonElasticDiffXSec.h"
#include "ExternalRadiator.h"
#include "epElasticDiffXSec.h"

namespace insane {
   namespace physics {
/** Elastic scattering using the Dipole FFs.
 * \ingroup exclusiveXSec
 */
class ProtonElasticRadiativeTailDiffXSec : public ExternalRadiator<epElasticDiffXSec> {
   protected:

   public:
      ProtonElasticRadiativeTailDiffXSec();
      virtual ~ProtonElasticRadiativeTailDiffXSec();
      ProtonElasticRadiativeTailDiffXSec(const ProtonElasticRadiativeTailDiffXSec& rhs)            = default;
      ProtonElasticRadiativeTailDiffXSec& operator=(const ProtonElasticRadiativeTailDiffXSec& rhs) = default;
      ProtonElasticRadiativeTailDiffXSec(ProtonElasticRadiativeTailDiffXSec&&)                     = default;
      ProtonElasticRadiativeTailDiffXSec& operator=(ProtonElasticRadiativeTailDiffXSec&&)          = default;

      virtual ProtonElasticRadiativeTailDiffXSec*  Clone(const char * newname) const {
         std::cout << "ProtonElasticRadiativeTailDiffXSec::Clone()\n";
         auto * copy = new ProtonElasticRadiativeTailDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual ProtonElasticRadiativeTailDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual void InitializePhaseSpaceVariables();

      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      virtual Double_t   EvaluateXSec(const Double_t * x) const;
      //virtual void       DefineEvent(Double_t * vars);

      //DiffXSec * GetBornXSec() { return fElasticDiffXSec; }

      double GetTheta_e(double Es, double alpha) const;
      double GetPalpha(double Es, double alpha) const;
      double ElectronToAlphaJacobian(double Es, double alpha) const ;
      double Ib(Double_t E0,Double_t E,Double_t t) const ;

      ClassDef(ProtonElasticRadiativeTailDiffXSec, 1)
};
}
}


#endif

