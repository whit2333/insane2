#ifndef RADCORRadiatedDiffXSec_HH 
#define RADCORRadiatedDiffXSec_HH 1 

#include "TMath.h"
#include "InclusiveDiffXSec.h"
#include "PolarizedStructureFunctionsFromPDFs.h"
#include "DSSVPolarizedPDFs.h"
#include "DNS2005PolarizedPDFs.h"
#include "PolarizedCrossSectionDifference.h"
#include "RADCOR.h"
#include "FormFactors.h"
#include "MSWFormFactors.h"
#include "AmrounFormFactors.h"
#include "F1F209eInclusiveDiffXSec.h"
namespace insane {
namespace physics {

/** Base class for using RADCOR to get a cross section.
 */
class RADCORRadiatedDiffXSec: public InclusiveDiffXSec{

   private: 
      Int_t fApprox;        /// 0 = Exact, 1 = Energy peaking approximation 
      Int_t fMultiPhoton;   /// Multiple photon correction: 0 = off, 1 = on 

   protected:

      RADCOR *fRADCOR;

   public: 
      RADCORRadiatedDiffXSec();
      virtual ~RADCORRadiatedDiffXSec();
      virtual RADCORRadiatedDiffXSec*  Clone(const char * newname) const {
         std::cout << "RADCORRadiatedDiffXSec::Clone()\n";
         auto * copy = new RADCORRadiatedDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual RADCORRadiatedDiffXSec*  Clone() const { return( Clone("") ); } 

      RADCOR * GetRADCOR(){ return fRADCOR; }

      // This should be done by direct acces to RADCOR via GetRADCOR()
      void UseApprox(char ans){
         if(ans=='y'){
            std::cout << "[RADCORRadiatedDiffXSec]: Using peaking approximation." << std::endl;
            fApprox = 1;
         }else if(ans=='n'){
            std::cout << "[RADCORRadiatedDiffXSec]: Using exact integral." << std::endl;
            fApprox = 0;
         }else{
            std::cout << "[RADCORRadiatedDiffXSec]: Invalid choice.  Exiting..." << std::endl;
            exit(1);
         }
      }          

      void UseInternal(bool ans=true){ fRADCOR->UseInternal(ans); }
      void UseExternal(bool ans=true){ fRADCOR->UseExternal(ans); }

      void SetTargetNucleus(const Nucleus& t){DiffXSec::SetTargetNucleus(t); fRADCOR->SetTargetNucleus(t); }

      void SetPolarizedCrossSectionDifference(InclusiveDiffXSec *f){fRADCOR->SetPolDiffXS(f);}
      void SetUnpolarizedCrossSection(InclusiveDiffXSec *f){fRADCOR->SetUnpolXS(f);}
      void SetPolarizationType(int p){fPolType = p; fRADCOR->SetPolarization(p);} 
      void SetVerbosity(int v){fRADCOR->SetVerbosity(v);} 
      void SetThreshold(Int_t t){fRADCOR->SetThreshold(t);}  
      void SetDeltaM(Double_t dM){fRADCOR->SetDeltaM(dM);}
      void SetMultiplePhoton(Int_t c){fRADCOR->SetMultiplePhoton(c);}            
      void SetRadiationLengths(Double_t *T){fRADCOR->SetRadiationLengths(T);}

      Double_t EvaluateXSec(const Double_t *) const;

      ClassDef(RADCORRadiatedDiffXSec,1) 

}; 
}}
#endif 

