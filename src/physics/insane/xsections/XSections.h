#ifndef XSections_HH
#define XSections_HH 1

#include "InclusiveDiffXSec.h"
#include "ElectroProductionXSec.h"
#include "PhysicalConstants.h"

namespace insane {
namespace physics {
/** Inclusive Mott Cross Section scattering off a particle with proton mass.
 *  The mott cross section describes a point-like spinless particle
 *
 * \ingroup inclusiveXSec
 */
class InclusiveMottXSec : public InclusiveDiffXSec {
public:
   InclusiveMottXSec() { };

   virtual ~InclusiveMottXSec() { };

   /**  Needed by ROOT::Math::IBaseFunctionMultiDim */
   unsigned int NDim() const {
      return fnDim;
   }

   Double_t GetEPrime(const Double_t theta)const  {
      return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / (M_p/GeV) * TMath::Power(TMath::Sin(theta / 2.0), 2)));
   }

   /** Evaluate Cross Section (mbarn/sr) */
   virtual Double_t EvaluateXSec(const Double_t * x) const;

   virtual void InitializePhaseSpaceVariables();


   ClassDef(InclusiveMottXSec, 1)
};


/// \deprecated
class PolIncDiffXSec : public InclusiveDiffXSec {
public:
   PolIncDiffXSec() {}
   ~PolIncDiffXSec() {}

   ClassDef(PolIncDiffXSec, 1)
};

}}

#endif

