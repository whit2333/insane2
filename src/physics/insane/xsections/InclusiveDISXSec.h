#ifndef InclusiveDISXSec_HH
#define InclusiveDISXSec_HH 1

#include "CompositeDiffXSec.h"

namespace insane {
namespace physics {
/** Generic DIS cross section which uses whatever structure functions
 *  are set by the function manager.
 *
 * \ingroup inclusiveXSec
 */
class InclusiveDISXSec : public CompositeDiffXSec {

   public:
      InclusiveDISXSec();
      virtual ~InclusiveDISXSec();
      virtual InclusiveDISXSec*  Clone(const char * newname) const ;
      virtual InclusiveDISXSec*  Clone() const { return( Clone("") ); } 
      void SetTargetThickness(Double_t t);

   ClassDef(InclusiveDISXSec,1)
};

}}
#endif

