#ifndef ElectroProductionXSec_HH
#define ElectroProductionXSec_HH 1
#include "InclusiveDiffXSec.h"

namespace insane {
namespace physics {

/** Electro Production Cross Section.
 *  A virtual photon is defined by the angle and energy of the scattered electron.
 *  \f$ e N \rightarrow e' M X \f$
 *
 *  The phase space should have dimension 6.
 *
 * \ingroup EventGen
 * \ingroup xsections
 */
class ElectroProductionXSec : public InclusiveDiffXSec {

   protected:
      // are these needed?
      Double_t * fMomentum_pi;
      Double_t * fTheta_pi;
      Double_t * fPhi_pi;

   public:
      ElectroProductionXSec() ;
      ElectroProductionXSec(const ElectroProductionXSec& rhs) ;
      virtual ~ElectroProductionXSec(); 
      ElectroProductionXSec& operator=(const ElectroProductionXSec& rhs) ;
      virtual ElectroProductionXSec*  Clone(const char * newname) const ;
      virtual ElectroProductionXSec*  Clone() const ;

      /** eN--> gamma* N --> e' P X */
      virtual void InitializePhaseSpaceVariables();

      /** \name kinematics
       *
       * @{
       */
      /** \f$ Q^2 = 4EE^{\prime} Sin^2(\theta/2) \f$ */
      Double_t GetQSquared(Double_t energy, Double_t theta) {
         return(4.0 * energy * GetBeamEnergy() * TMath::Sin(theta / 2.0) * TMath::Sin(theta / 2.0)) ;
      }

      /** \f$ \nu = E-E^{\prime} \f$ */
      Double_t GetPhotonEnergy(Double_t energy, Double_t theta = 0) {
         return(GetBeamEnergy() - energy);
      }

      /** \f$ x = Q^2/2M\nu \f$ */
      Double_t GetBjorkenX(Double_t energy, Double_t theta = 0) {
         return(GetQSquared(energy, theta) / (2.0 * M_p/GeV * GetPhotonEnergy(energy, theta)));
      }

      /** \f$ W^2 = M_{p}^{2}+2 \nu M_p - Q^2  \f$ */
      Double_t GetInvariantMassSquared(Double_t energy, Double_t theta) {
         return(2.0 * GetPhotonEnergy(energy, theta) * M_p/GeV - GetQSquared(energy, theta) + M_p/GeV * M_p/GeV);
      }

      /** \f$ R = \frac{W_{1}}{W_{2}}(1+\frac{\nu^{2}}{Q^{2}})-1 \f$
       *  \f$ R = \frac{F_2}{2xF_{1}}(1+\frac{4M^2 x^2}{Q^2})-1  \f$
       */
      //    Double_t GetR() {
      //       return( fStructureFunctions->F2p(GetBjorkenX(),GetQSquared())/
      //               (2.0*GetBjorkenX()*fStructureFunctions->F1p(GetBjorkenX(),GetQSquared()))*
      //               (1.0+4.0*M_p/GeV*M_p/GeV*TMath::Power(GetBjorkenX(),2)/GetQSquared()) - 1.0 );
      //    }

      /** \f$ D = \frac{E-\epsilon E^{\prime} }{E(1+\epsilon R)} \f$ */
      //    Double_t GetD() {
      //       return( ( GetBeamEnergy()-GetEpsilon()*fEnergy)/(GetBeamEnergy()*(1.0+GetEpsilon()*GetR() )) ) ;
      //    }

      /** \f$ d = D \sqrt{ \frac{2 \epsilon}{1+\epsilon} } \f$ */
      //    Double_t Getd() {
      //       return(GetD()*TMath::Sqrt(2.0*GetEpsilon()/(1.0+GetEpsilon()) ) );
      //    }

      /** \f$ \eta =  \epsilon \frac{\sqrt{Q^2}}{E - \epsilon E^{\prime}} \f$ */
      //   Double_t GetEta() {
      //       return(GetEpsilon()*TMath::Sqrt(GetQSquared())/(GetBeamEnergy()-GetEpsilon()*fEnergy));
      //    }

      /** \f$ \xi = \eta \frac{ 1+\epsilon}{2\epsilon} \f$ */
      //    Double_t GetXi() {
      //       return(GetEta()*(1.0 + GetEpsilon())/(2.0*GetEpsilon()));
      //    }

      /** \f$ 1/\epsilon = 1+ 2(1+\frac{\nu^{2}}{Q^{2}})tan^2(\theta/2) \f$ */
      Double_t GetOneOverEpsilon(Double_t energy, Double_t theta);

      /** \f$ \epsilon = 1/( 1+ 2(1+\frac{\nu^{2}}{Q^{2}})tan^2(\theta/2) ) \f$ */
      Double_t GetEpsilon(Double_t energy, Double_t theta);

      Double_t GetHandFluxFactor(Double_t energy, Double_t theta);

      Double_t GetVirtualPhotonFlux(Double_t energy, Double_t theta) ;
      //@}



      ClassDef(ElectroProductionXSec, 1)
};

}}

#endif
