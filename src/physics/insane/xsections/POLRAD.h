#ifndef POLRAD_HH
#define POLRAD_HH 5 

#include "TMath.h"
#include "TObject.h"
#include "TLorentzVector.h"
#include <cstdlib> 
#include <iostream> 
#include <vector>

#include "InSANEMathFunc.h"
#include "Nucleus.h"
#include "POLRADKinematics.h"
#include "StructureFunctions.h"
#include "PolarizedStructureFunctions.h"
#include "FormFactors.h"
#include "MSWFormFactors.h"
#include "AmrounFormFactors.h"
#include "BilenkayaFormFactors.h"

#include "Math/GSLIntegrator.h"
#include "Math/GaussIntegrator.h"
#include "Math/GaussLegendreIntegrator.h"
#include "Math/VirtualIntegrator.h"
#include "Math/Integrator.h"
#include "Math/IntegratorOptions.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/GaussIntegrator.h"
#include <Math/IFunction.h>
#include <Math/Functor.h>
namespace insane {
namespace physics {

/** Polarized radiative corrections (POLRAD).
 *  Much of this code is copied from the PolRad2.0 manual. 
 *  \ingroup physics 
 */
class POLRAD : public TObject {

   private:

      std::vector<std::vector<std::vector<Double_t> > > fT;
      std::vector<std::vector<std::vector<Double_t> > > fT2;
      Double_t Feta_2plus,Fxi_2plus;
      Double_t Feta_2minus,Fxi_2minus;
      Double_t Feta_d,Fxi_d;
      Double_t Fetaeta_2plus,Fxieta_2plus;
      Double_t Fetaeta_2minus,Fxieta_2minus;
      Double_t Fetaeta_d,Fxieta_d;
      Double_t Feta_1plus;
      Double_t Fetaeta_1plus;
      Double_t Feta;
      Double_t Fetaeta;
      Double_t seta,sxi,reta,rxi;
      Double_t F_IR,Fxi_IR,Feta_IR,Fxieta_IR,Fetaeta_IR;
      Double_t F,F_d,F_1plus,F_2plus,F_2minus,F_i,F_ii;
      Int_t ki[8];                    ///< Following PolRad2.0, Appendix B
      Int_t li[8];                    ///< Following PolRad2.0, Appendix B

   protected:

      mutable Double_t                    fTau;                    ///< Dummy integration variable used when doing double integrals. 
      mutable Int_t                       ffSFIndex;               ///< Index placeholders for integration purposes (for fancy structure functions)
      mutable Int_t                       fThIIndex,fThJIndex;     ///< Index placeholders for integration purposes (for THETAij)  
      mutable bool                        fReCalculateTs;
      //bool                                fIsQEFullCalc;           ///< true = calculate double integral, using QE F1 and F2 from F1F209; default is false  
      POLRADKinematics              fKinematics;             ///< Calculates and holds all variables.

      StructureFunctions           *fUnpolSFs;               //! Unpolarized structure functions (QE is for quasi-elastic only)  
      StructureFunctions           *fUnpolQESFs;             //! Unpolarized structure functions (QE is for quasi-elastic only)  
      PolarizedStructureFunctions  *fPolSFs;                 //! Polarized structure functions
      FormFactors                  *fFFs;                    //! FFT is for the target (for say, 3He which has a different fit than the others)  
      FormFactors                  *fFFTs;                   //! FFT is for the target (for say, 3He which has a different fit than the others)  

      Int_t    fDebug;                ///< Debug switch (0=no debug)
      //bool     fIsAnglePeakingApprox;
      bool     fIsMultiPhoton;        ///< switch to use multiple soft-photon radiation 
      //bool     fIsUltraRel;           ///< switch to use ultra-relativisitic approx (m,M << S,X,Q2)  
      bool     fIsExtTail;            ///< switch to use external radiative tail (need to set radiation lengths too for this to make sense!) 

   public:

      enum  IntegrationMethod { kFull, kPeakingApprox, kAnglePeaking, kUltraRel }; 

   protected:

      IntegrationMethod fERT_IntegrationMethod;
      IntegrationMethod fQRT_IntegrationMethod;
      IntegrationMethod fIRT_IntegrationMethod;

   public:

      void  SetIRTMethod(IntegrationMethod m) { fIRT_IntegrationMethod = m; }
      void  SetQRTMethod(IntegrationMethod m) { fQRT_IntegrationMethod = m; }
      void  SetERTMethod(IntegrationMethod m) { fERT_IntegrationMethod = m; }

      IntegrationMethod  GetIRTMethod() { return fIRT_IntegrationMethod; }
      IntegrationMethod  GetQRTMethod() { return fQRT_IntegrationMethod; }
      IntegrationMethod  GetERTMethod() { return fERT_IntegrationMethod; }

      void  SetMultiPhoton(bool v=true){fIsMultiPhoton = v;}
      bool  IsMultiPhoton(){return fIsMultiPhoton;}

      //void  SetUltraRel(bool v=true){fIsUltraRel = v;} 
      //bool  IsUltraRel(){return fIsUltraRel;}
      bool  IsExtTail(){    return fIsExtTail;}
      void  SetExtTail(bool v=true){fIsExtTail = v;}

      void SetAnglePeakingApprox(bool v=true){ if(v) { SetIRTMethod(kAnglePeaking);SetERTMethod(kAnglePeaking);SetQRTMethod(kAnglePeaking); } }
      //bool IsAnglePeakingApprox(){ return fIsAnglePeakingApprox ; } 

      Double_t sig_dis_born(Double_t S,Double_t X,Double_t Q2);    ///< DIS Born cross section, function of S,X,Q2 
      Double_t sig_el_born(Double_t S,Double_t Q2);                ///< Elastic Born cross section, function of S,Q2 

      //bool IsQEFullCalc(){return fIsQEFullCalc;} 
      //void DoQEFullCalc(bool ans){fIsQEFullCalc = ans;} 
      void DoExternalTailCalc(bool ans){fIsExtTail = ans;} 

      void SetPOLRADUnpolarizedSFs(StructureFunctions *usfs){fUnpolSFs = usfs;}
      void SetPOLRADUnpolarizedQuasiElasticSFs(StructureFunctions *usfs){fUnpolQESFs = usfs;}
      void SetPOLRADPolarizedSFs(PolarizedStructureFunctions *psfs){fPolSFs = psfs;}
      void SetPOLRADFormFactors(FormFactors *ffs){fFFs = ffs;}
      void SetPOLRADTargetFormFactors(FormFactors *ffs){fFFTs = ffs;}
      void SetUSFs(StructureFunctions * sf){SetPOLRADUnpolarizedSFs(sf);}
      void SetUQESFs(StructureFunctions * sf){SetPOLRADUnpolarizedQuasiElasticSFs(sf);}
      void SetPSFs(PolarizedStructureFunctions * sf) {SetPOLRADPolarizedSFs(sf);}
      void SetFFs(FormFactors * ff){SetPOLRADFormFactors(ff);}
      void SetFFTs(FormFactors * ff){SetPOLRADTargetFormFactors(ff);}
      void     SetVerbosity(int v){fDebug = v;}                    ///< Set the level of printing for debugging. 0=off 
      Int_t    GetVerbosity() { return fDebug;}                    ///< Get the debug level.


      /** @name Inelastic radiative tail components
       * IRT.
       * The original sig_F_in() does not work. 
       * Use sig_F_in_2() instead. 
       */
      //@{

      Double_t IRT() ;                                          ///< Inelastic radiative tail 
      Double_t Delta_vert() ;                                   ///< Vertex contribution 
      Double_t Delta_R_IR() ;                                   ///< 
      Double_t Delta_vac_l() ;                                  ///< Vacuum polarization by leptons 
      Double_t Delta_vac_h() ;                                  ///< Vacuum polarization by hadrons 
      Double_t Delta_inf() ;                                    ///<
      Double_t sig_F_in_delta() ;                               ///< Infrared free component of IRT  
      Double_t sig_F_in_Tau_Integrand_delta(Double_t &) ;       ///< Integrand 
      Double_t sig_F_in_2() ;                                   ///< Infrared divergence free component of IRT.
      Double_t sig_F_in_angle_peaking() ;                       ///< Angle peaking does two R integrals of peaked values of tau.
      Double_t sig_F_in_Tau_Integrand_2(Double_t & tau) ;       ///< Tau integrand.
      Double_t sig_F_in_R_Integrand_3(Double_t & R) ;           ///< R integrand.
      Double_t sig_F_in_2D();                                   ///< Calculation of IRT using the 2-D integration method.
      Double_t Spence(Double_t);                                ///< Spence function 
      Double_t Delta_sp();                                      ///<
      Double_t S_phi();                                         ///< From the fortran code, deltas subroutine, spfr term.
      Double_t S_phi(Double_t,Double_t,Double_t,Double_t);      ///< Eq 24 of J Phys G 20, 513 (1994).
      Double_t sig_F_in_2D_Integrand(Double_t R, Double_t tau); /**< 2-D integrand. There is a change of variables in order to avoid
                                                                 *   a dependence on the upper limit of R-integration on tau.  
                                                                 *   \f$ r = (1+\tau)R \f$ which adds a factor of \f$ 1/(1+\tau) \f$ . 
                                                                 */
      ///@} 


      /** @name Elastic radiative tail components. 
       * ERT
       */ 
      ///@{

      Double_t ERT() ;                              ///< Elastic radiative tail 
      Double_t ERT_Tau_Integrand(Double_t &) ;      ///< ERT integrand

      /** ERT tau integrand.
       *  The output matches the results of the subroutine rv3ln
       *  multiplied by (Q2/Sx+tau), which is result of their change
       *  of integration variable to \f$ t' = ln(tau+Q2/Sx). \f$ 
       *  \f$ \sigma_{ERT}=\frac{2 \alpha^3 S_x}{A \sqrt{\lambda_s}} (rv2ln)/(Q^2/S_x +\tau)  \f$
       *  The output of ERT_Tau_Integrand (=res) is such that  
       *  \f$ \sigma_{ERT}=\frac{-\alpha^3 y}{A^2 } res \f$
       *  Thus we need a factor
       * \f$ \frac{-y \sqrt{\lambda_s}}{2 S_x} \f$ 
       * to compare the \f$ res \f$ and \f$ (rv2ln)/(Q^2/S_x+\tau) \f$ .
       */  
      double ERT_Tau_Integrand2(double *x, double * p ){
         Double_t tau = x[0];
         return -1.0*ERT_Tau_Integrand(tau)*fKinematics.Gety_A()/2.0/fKinematics.GetS_xA()*TMath::Sqrt(fKinematics.GetLambda_sA())  ;
      }
      double ERT_Tau_Integrand3(double *x, double * p ){Double_t tau = x[0];return TMath::Abs(ERT_Tau_Integrand(tau))  ;}
      double ERT_Tau_Integrand4(double *x, double * p ){Double_t tau = x[0]; ERT_Tau_Integrand(tau); return fArbValue1; }
      double ERT_Tau_Integrand5(double *x, double * p ){Double_t tau = x[0]; ERT_Tau_Integrand(tau); return fArbValue2; }
      double ERT_Tau_Integrand6(double *x, double * p ){Double_t tau = x[0]; ERT_Tau_Integrand(tau); return fArbValue3; }

      ///@} 

      /** @name Quasi-elastic radiative tail components. 
       * QRT
       */ 
      ///@{

      Double_t QRT() ; 
      Double_t QRT_Full(); 
      Double_t QRT_angle_peaking(); 
      Double_t QRT_Tau_Integrand(Double_t &) ; 
      Double_t QRT_Tau_Integrand_Full(Double_t &) ; 
      Double_t QRT_R_Integrand(Double_t &) ; 
      Double_t QRT_R_Integrand_Full(Double_t &) ; 

      double QRT_Tau_Integrand(double *x,double *p){
         Double_t tau      = x[0];
         Double_t y        = fKinematics.Gety(); 
         //Double_t xb       = fKinematics.Getx(); 
         Double_t S_x      = fKinematics.GetS_x(); 
         Double_t Lambda_s = fKinematics.GetLambda_s();  
         Double_t scale    = -1.0*TMath::Sqrt(Lambda_s)*(y/2./S_x); 
         Double_t qrt      = scale*QRT_Tau_Integrand(tau);//*(xb+tau);
         return qrt; 
      }

      ///@}

      /** @name Multiple (soft) photon contributions 
       * 
       */ 
      ///@{
      Double_t MultiPhoton(Double_t);                      ///<  
      Double_t MultiPhoton_qe(Double_t,Double_t,Double_t); ///< 
      Double_t MultiPhoton_el(Double_t,Double_t,Double_t); ///< 
      ///@} 



   public:

      Nucleus fTargetNucleus;               ///< proton by default
      Double_t fspin2;                            ///< Target spin*2 (0->scaler,1->spin-1/2, 2->spin-1)
      Nucleus::NucleusType fPOLRADTarget;   ///< Target type (kProton, kNeutron,kDeuteron,k3He,kOther)
      Double_t fDeltaE;                           ///< Detector resolution (~10 MeV) 
      Double_t fNucZ;                             ///< Nuclear charge of target. \deprecated
      Double_t fA;                                ///< Atomic mass of target. \deprecated
      Double_t fZ;                                ///< Atomic number of target. \deprecated
      Double_t fTargMom;                          ///< Target momentum (GeV)  
      Double_t fPF;                               ///< Fermi Momentum (GeV) 
      Double_t fP_N;                              ///< Target Nuclei degree of polarization
      Double_t fQ_N;                              ///< Target Nuclei quadrupole moment
      Double_t fP_L;                              ///< Degree of lepton polarization
      Double_t fP_n;                              ///< Degree of polarization of neutron 
      Double_t fP_p;                              ///< Degree of polarization of proton  
      Int_t    fHelicity;                         ///< Lepotn helicity multiplying fXi.
      Double_t ft_b,ft_a;                         ///< Radiation lengths in number of X0 (b = before scattering; a = after scattering) 


      /** @name Fancy-type structure functions (Appendix A1).  
       * Index starts at 1 to agree with the paper. 
       * Index 0 gives 0 for all arrays.  
       */ 
      ///@{
      Double_t fFin[9];      ///< Fancy-type IRT Fs defined in PolRad2.0, eqn (A.3)
      Double_t fFel[9];      ///< Fancy-type ERT Fs defined in PolRad2.0, eqn (A.3)
      Double_t fFqe[9];      ///< Fancy-type QRT Fs defined in PolRad2.0, eqn (A.3)
      ///@} 

      /** @name Set the radiation lengths.  
       *  Units are number of radiation lengths (X0). 
       *  @param tb radiation length before scattering 
       *  @param ta radiation length after scattering                             
       */ 
      void SetRadiationLengths(Double_t tb,Double_t ta){ft_b = tb; ft_a = ta;} 

      /** @name Elastic cross section.  
       *  For use with external radiative corrections.  
       * Paper reference: Phys Rev D 12, 1884 (1975)
       * Eq A55 
       * @param Es    Incident electron energy 
       * @param theta Scattering angle 
       */ 
      Double_t sigma_el_tilde(Double_t Es,Double_t theta); 
  
      /** @name External radiative cross section. 
       * Straggling due to ionization loss and bremsstrahlung. 
       * Paper reference: Phys Rev D 12, 1884 (1975)
       * Eq A49  
       */  
      Double_t sigma_b(); 

      /** @name FTilde. 
       * Correction scale factor. 
       * Paper reference: Phys Rev D 12, 1884 (1975)
       * Eq A44 
       * @param Q2 Q2 in GeV^2  
       */ 
      Double_t FTilde(Double_t); 

      /** @name Projections onto the hyper plane basis.
       * 
       *  Coefficients of PolRad2.0, eqn(6) set equal to eqn(B.10) using eqn(5).
       *  
       *  \f$ \eta_{hyper-plane} = \eta - (\eta_{\mu} \eta_{perp}^{\mu}) \eta_{perp}  = 2 (a k_1 + b k_2 + c p) \f$
       *  
       *  \f$ \eta = u_0 (P/M) + u_1 \eta_L + u_2 \eta_T + u_3 \eta_{perp} \f$
       */
      ///@{
      //Double_t fa_L,fb_L,fc_L;       ///< Coefficients for B.10
      //Double_t fa_T,fb_T,fc_T;       ///< Coefficients for B.10  
      //TLorentzVector fEta_T;         ///< Basis vector for hyperplane coordinates PolRad2.0, eqn(5) 
      //TLorentzVector fEta_L;         ///< Basis vector for hyperplane coordinates PolRad2.0, eqn(5) .
      //TLorentzVector fEta_Perp;      ///< Basis vector for hyperplane coordinates PolRad2.0, eqn(6) 
      //TLorentzVector fXi_L;          ///< Longitudinal component of lepton polarization.
      //TLorentzVector fEta_hp;        ///< \f$ \eta_{hyper-plane} \f$ 
      ///@}


      POLRAD();
      virtual ~POLRAD();

      void     Initialize();
      void     Print(Option_t * opt = "") const ; 

      /** Set the target.
       *  Set the target using an integer 0=p,1=n,2=d,3=He3.
       *  Target A and Z are also set based upon the integer. 
       */
      void     SetTargetNucleus(const Nucleus& n);  
      Double_t GetTargetMass(){ return fKinematics.GetM_A();}    ///< returns M_A.
      void     SetTarget(Int_t Z, Int_t A);                      ///< Set target using Z and A.


      POLRADKinematics* GetKinematics(){return(&fKinematics);} ///< Returns the pointer to the kinematics.  
      void                    SetKinematics(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi = 0); 
      void                    SetKinematicVectors(TLorentzVector k1,TLorentzVector k2,TLorentzVector p);  

      /** Sets the polariztion vectors for target (eta) ,and  beam (xi) helicity. */
      void     SetPolarizationVectors(TLorentzVector eta,Int_t hel = 1){ SetHelicity(hel); fKinematics.SetEta(eta);}
      void     SetPolarizationVectors(TVector3 P,Int_t hel = 1){         SetHelicity(hel); fKinematics.SetEta( TLorentzVector(P,0.0) ); }

      void     SetHelicity(Int_t h = 1) { fHelicity = h; fP_L = fHelicity;}
      void     SetBeamHelicity(Int_t Hel){ SetHelicity(Hel);}
      void     SetTargetPolarization(Double_t p = 1.0){ fP_N = p; }

      /** P_beam, P_target, Q_target (Q = quadrupole moment) . */
      void SetPolarizations(Double_t *);
      void SetPolarizations(Double_t pl, Double_t pt, Double_t pq){
         Double_t p[] = {pl,pt,pq};
         SetPolarizations(p);
      }
      void SetUnpolarized(){fP_L=0.; fP_N = 0.; std::cout << "[POLRAD]: Polarization is OFF." << std::endl;}

      /** @name Fancy structure functions.  
       *  The fancy type structure functions defined in A.1 of POLRAD2.0 manual.
       *  Index zero is not used in order to follow fortran/polrad type indices.  
       *  Calculates 'fancy' structure function (and form factor) combinations.
       */ 
      ///@{
      //void ProcessFancyStructureFunctions(Double_t x,Double_t Q2,Double_t * in,Double_t * qe,Double_t * el);
      void ProcessFancyElasticStructureFunctions(Double_t R,Double_t tau,Double_t *) ;
      void ProcessFancyQuasiElasticStructureFunctions(Double_t R,Double_t tau,Double_t *);
      void ProcessFancyInelasticStructureFunctions(Double_t R,Double_t tau,Double_t *);
      void ProcessFancyInelasticStructureFunctions(Double_t R,Double_t tau,Double_t *,Double_t x,Double_t Q2);

      /** arguments are x and Q2 . */
      Double_t EvaluateFormFactors(Double_t,Double_t) {return(0);}

      /** Calculates structure functions for a given x and Q2.  
       *  This is the mmore detailed description. 
       */ 
      void ProcessStructureFunctions(Double_t x, Double_t Q2);

      /** Calculates form factors for proton, neutron, deuteron and target for a given Q2. 
       */ 
      void ProcessFormFactors(Double_t Q2); 
      ///@}

      Int_t    fSFType;                    ///< Structure Function type
      Double_t ffSFin[9];
      Double_t ffSFqe[9];
      Double_t ffSFel[9];

      /** @name Form factors. 
       *  The fancy type structure functions defined in A.1 of POLRAD2.0 manual.
       */ 
      ///@{
      Double_t fFc;                  ///< Charge     form factor [deuteron]  
      Double_t fFm;                  ///< Magnetic   form factor [deuteron]                        
      Double_t fFq;                  ///< Quadrupole form factor [deuteron]  
      Double_t fGe;                  ///< Electric form factor [target] 
      Double_t fGm;                  ///< Magnetic form factor [target] 
      Double_t fe_p;                 ///< Electric form factor [proton] 
      Double_t fe_n;                 ///< Electric form factor [neutron] 
      Double_t fm_p;                 ///< Magnetic form factor [proton] 
      Double_t fm_n;                 ///< Magnetic form factor [neutron] 
      ///@} 

      /** @name Structure functions.  
       *  The structure fucntions defined in A.1.
       */ 
      ///@{
      Double_t fF1;                  ///< Unpolarized structure function
      Double_t fF2;                  ///< Unpolarized structure function 
      Double_t fF1qe;                ///< Unpolarized structure function [quasi-elastic] 
      Double_t fF2qe;                ///< Unpolarized structure function [quasi-elastic] 
      Double_t fg1;                  ///< Polarized structure function 
      Double_t fg2;                  ///< Polarized structure function 
      Double_t fg1qe;                  ///< Polarized structure function 
      Double_t fg2qe;                  ///< Polarized structure function 
      Double_t fb1;                  ///< Polarized quadrupole structure function
      Double_t fb2;                  ///< Polarized quadrupole structure function
      Double_t fb3;                  ///< Polarized quadrupole structure function
      Double_t fb4;                  ///< Polarized quadrupole structure function 
      Double_t fb1qe;                  ///< Polarized quadrupole structure function
      Double_t fb2qe;                  ///< Polarized quadrupole structure function
      Double_t fb3qe;                  ///< Polarized quadrupole structure function
      Double_t fb4qe;                  ///< Polarized quadrupole structure function 
      ///@}

      /** @name Theta and related functions for nucleon.
       *  These use the nucelon mass, M_p, in calculating kinematic variables
       *  and should be used the IRT and QRT related methods.
       */
      ///@{
      Double_t THETAij(Int_t,Int_t,Double_t) ;
      Double_t Tijk( Double_t) ;
      Double_t aik(Int_t,Int_t);
      Double_t qik(Int_t,Int_t,Double_t);
      Double_t B1(Double_t);
      Double_t B2(Double_t);
      Double_t C1(Double_t);
      Double_t C2(Double_t);
      Double_t Fd(Double_t);
      ///@}

      /** @name Theta and related functions for the nucleus.
       *  These use the nucleus mass, M_A, in calculating kinematic 
       *  variables and should be used for ERT methods.
       */
      ///@{
      Double_t THETAij_A(Int_t,Int_t,Double_t) ;
      Double_t Tijk_A( Double_t) ;
      Double_t aik_A(Int_t,Int_t);
      Double_t qik_A(Int_t,Int_t,Double_t);
      Double_t B1_A(Double_t);
      Double_t B2_A(Double_t);
      Double_t C1_A(Double_t);
      Double_t C2_A(Double_t);
      Double_t Fd_A(Double_t);
      ///@}


      /** @name Functor Wrappers for integrands. */
      ///@{

      /** R integrand.  
       */
      class IRT_R_3 : public ROOT::Math::IBaseFunctionOneDim {
         public:
            mutable POLRAD * fPOLRAD; //!
            double DoEval(double x) const { return x*x; }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new IRT_R_3(); }
            ClassDef(IRT_R_3,1)
      };

      /** IRT 2 dimensional integrand in R and tau. R = x[0] and tau = x[1]. */
      class IRT_2D_integrand: public ROOT::Math::IBaseFunctionMultiDim {
         public:
            IRT_2D_integrand(){}
            ~IRT_2D_integrand(){}
            mutable POLRAD * fPOLRAD; //!
            double DoEval(const double* x) const { return fPOLRAD->sig_F_in_2D_Integrand(x[0],x[1]); }
            unsigned int NDim() const { return 2; }
            ROOT::Math::IBaseFunctionMultiDim* Clone() const { return new IRT_2D_integrand(); } 
            ClassDef(IRT_2D_integrand,1)
      }; 

      /** IRT tau integrand limited to integrate R around the delta. 
       */
      class IRT_TauFuncWrap_delta : public ROOT::Math::IBaseFunctionOneDim {
         public:
            IRT_TauFuncWrap_delta(){}
            ~IRT_TauFuncWrap_delta(){}
            mutable POLRAD * fPOLRAD; //!
            double DoEval(double x) const { double tau = x; return fPOLRAD->sig_F_in_Tau_Integrand_delta(tau); }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new IRT_TauFuncWrap_delta(); }
            ClassDef(IRT_TauFuncWrap_delta,1)
      };

      /** IRT tau integrand. This is the outer integral. */
      class IRT_TauFuncWrap_2 : public ROOT::Math::IBaseFunctionOneDim {
         public:
            mutable POLRAD * fPOLRAD; //!
            double DoEval(double x) const { return fPOLRAD->sig_F_in_Tau_Integrand_2(x); }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new IRT_TauFuncWrap_2(); } 
            ClassDef(IRT_TauFuncWrap_2,1)
      };

      /** IRT tau integrand. 
       */
      class IRT_TauFuncWrap : public ROOT::Math::IBaseFunctionOneDim {
         public:
            IRT_TauFuncWrap(){}
            ~IRT_TauFuncWrap(){}
            mutable POLRAD * fPOLRAD; //!
            double DoEval(double x) const { double tau = x; return fPOLRAD->sig_F_in_Tau_Integrand_2(tau); }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new IRT_TauFuncWrap(); }
            ClassDef(IRT_TauFuncWrap,1)
      };


      /** IRT R integrand for all j.  
       */
      class IRT_RFuncWrap_3 : public ROOT::Math::IBaseFunctionOneDim {
         public:
            mutable POLRAD * fPOLRAD; //!
            double DoEval(double x) const { return fPOLRAD->sig_F_in_R_Integrand_3(x); }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new IRT_RFuncWrap_3(); }
            ClassDef(IRT_RFuncWrap_3,1)
      };
 
      /** QRT tau integrand [full integral]. 
       */
      class QRT_TauFuncWrap_QE_Full : public ROOT::Math::IBaseFunctionOneDim {
         public:
            QRT_TauFuncWrap_QE_Full(){}
            ~QRT_TauFuncWrap_QE_Full(){}
            mutable POLRAD * fPOLRAD; //!
            double DoEval(double x) const { return fPOLRAD->QRT_Tau_Integrand_Full(x); }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new QRT_TauFuncWrap_QE_Full(); }
            ClassDef(QRT_TauFuncWrap_QE_Full,1)
      };    
 
      /** QRT R integrand for all j [full integral]. 
       */
      class QRT_RFuncWrap_QE_Full : public ROOT::Math::IBaseFunctionOneDim {
         public:
            QRT_RFuncWrap_QE_Full(){}
            ~QRT_RFuncWrap_QE_Full(){}
            mutable POLRAD * fPOLRAD; //!
            double DoEval(double x) const { return fPOLRAD->QRT_R_Integrand_Full(x); }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new QRT_RFuncWrap_QE_Full(); }
            ClassDef(QRT_RFuncWrap_QE_Full,1)
      };    
 
      /** Wrapper class!!
       */
      class ERTFuncWrap : public ROOT::Math::IBaseFunctionOneDim {
         public:
            ERTFuncWrap(){}
            ~ERTFuncWrap(){}
            mutable POLRAD * fPOLRAD; //!
            double DoEval(double x) const { return fPOLRAD->ERT_Tau_Integrand(x); }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new ERTFuncWrap(); }
            ClassDef(ERTFuncWrap,1)
      };

      /** Wrapper for QRT tau intgrand function. 
       */
      class QRTTauFuncWrap : public ROOT::Math::IBaseFunctionOneDim {
         public:
            QRTTauFuncWrap(){}
            ~QRTTauFuncWrap(){}
            mutable POLRAD * fPOLRAD; //!
            double DoEval(double x) const { return fPOLRAD->QRT_Tau_Integrand(x); }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new QRTTauFuncWrap(); }
            ClassDef(QRTTauFuncWrap,1)
      };

      /** Wrapper for the QRT R integrand function.
       */
      class QRTRFuncWrap : public ROOT::Math::IBaseFunctionOneDim {
         public:
            QRTRFuncWrap(){}
            ~QRTRFuncWrap(){}
            mutable POLRAD * fPOLRAD; //!
            double DoEval(double x) const { return fPOLRAD->QRT_R_Integrand(x); }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new QRTRFuncWrap(); }
            ClassDef(QRTRFuncWrap,1)
      };
      ///@}
      
      ROOT::Math::IntegratorOneDim      * fIRT_tau_integrator; //!
      IRT_TauFuncWrap_2                   fIRT_tau_func;
      Double_t                            fIRT_tau_AbsErr;
      Double_t                            fIRT_tau_RelErr;
      unsigned int                        fIRT_tau_nCalls;
      unsigned int                        fIRT_tau_Rule;
      ROOT::Math::IntegrationOneDim::Type fIRT_tau_Type;

      ROOT::Math::IntegratorOneDim      * fIRT_R_integrator; //!
      IRT_RFuncWrap_3                     fIRT_R_func;
      Double_t                            fIRT_R_AbsErr;
      Double_t                            fIRT_R_RelErr;
      unsigned int                        fIRT_R_nLegPoints;  // Legendre polynomial degree
      unsigned int                        fIRT_R_nCalls;
      unsigned int                        fIRT_R_Rule;
      ROOT::Math::IntegrationOneDim::Type fIRT_R_Type;

      double IRT_2d_integrand(double *x, double * p ){
         Double_t tau = x[0];
         Double_t   R = x[1];
         fTau = tau; // Set tau to be used when   integrating over R
         fReCalculateTs    = true; // Needed for the new value of tau
         Double_t W2       = fKinematics.GetW2();
         Double_t M        = fKinematics.GetM();
         Double_t M_pion   = fKinematics.GetM_pion();
         //Double_t R_max = (W2 - TMath::Power(1.512,2.0))/(1.0+tau);  
         Double_t R_max = (W2 - TMath::Power(M+M_pion,2.))/(1.+tau);  
         Double_t min = 1.0e-30;
         Double_t max = R_max; 
         if(R > max) return(0.0);
         if(R < min) return(0.0);
         return( -1.0*sig_F_in_R_Integrand_3(R));
      } 

      /** @name Debugging functions. */
      ///@{ 

      ///** Parameters are i,j,k */ 
      //double TijkPlot(double *x, double * p ){
      //   Double_t tau = x[0];
      //   Int_t i = int(p[0]);
      //   Int_t j = int(p[1]);
      //   Int_t k = int(p[2]);
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return fT[i][j][k];
      //}
      ///** Parameters are i,j,k */ 
      //double TijkPlot_A(double *x, double * p ){
      //   Double_t tau = x[0];
      //   Int_t i = int(p[0]);
      //   Int_t j = int(p[1]);
      //   Int_t k = int(p[2]);
      //   ERT_Tau_Integrand(tau);
      //   return fT[i][j][k];
      //}
      //double delta_R_IR_Plot(double *x,double *p){
      //   //Double_t tau = x[0]; 
      //   Double_t delta = Delta_R_IR();
      //   return delta;  
      //}
      //double s_phi_Plot(double *x,double *p){
      //   //Double_t tau = x[0]; 
      //   Double_t sphi = S_phi();
      //   return sphi;  
      //}
      //double delta_vert_Plot(double *x,double *p){
      //   //Double_t tau = x[0]; 
      //   Double_t delta = Delta_vert();  
      //   return delta; 
      //}
      //double delta_vac_h_Plot(double *x,double *p){
      //   //Double_t tau = x[0]; 
      //   Double_t delta = Delta_vac_h();  
      //   return delta; 
      //}
      //double delta_vac_l_Plot(double *x,double *p){
      //   //Double_t tau = x[0]; 
      //   Double_t delta = Delta_vac_l();  
      //   return delta; 
      //}
      ///** Parameters are i,j */ 
      //double THETAij_Plot(double *x, double * p ){
      //   Double_t tau = x[0];
      //   Int_t i      = (int)p[0];
      //   Int_t j      = (int)p[1];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return THETAij(i,j,tau);
      //}
      ///** Parameters are i,j */ 
      //double THETAij_A_Plot(double *x, double * p ){
      //   Double_t tau = x[0];
      //   Int_t i      = (int)p[0];
      //   Int_t j      = (int)p[1];
      //   ERT_Tau_Integrand(tau);
      //   return THETAij_A(i,j,tau);
      //}
      //double rxi_Plot(double *x, double * p ){Double_t tau = x[0];
      //   ERT_Tau_Integrand(tau);
      //   return rxi;
      //}
      //double Rel_Plot(double *x,double *p){
      //   Double_t tau = x[0]; 
      //   ERT_Tau_Integrand(tau); 
      //   return fArbValue4;
      //}
      //double Eta_A_Plot(double *x,double *p){
      //   Double_t tau = x[0]; 
      //   ERT_Tau_Integrand(tau); 
      //   return fArbValue5;
      //}
      //double Eps_Plot(double *x,double *p){
      //   Double_t tau = x[0]; 
      //   QRT_Tau_Integrand(tau); 
      //   return fArbValue7;
      //}
      //double FancyFInelastic_Plot(double *x,double *p){
      //   Double_t tau = x[0];
      //   Int_t i      = Int_t(p[0]); // p[0] = Fancy SF index 
      //   Double_t R   = p[1];
      //   ProcessFancyInelasticStructureFunctions(R,tau,ffSFin);
      //   return ffSFin[i]; 
      //}              
      //// double FancyFQuasiElastic_Plot(double *x,double *p){
      ////    Double_t tau = x[0];
      ////    Int_t i      = Int_t(p[0]); // p[0] = Fancy SF index 
      ////    Double_t R   = p[1];
      ////    ProcessFancyQuasiElasticStructureFunctions(R,tau,ffSFqe);
      ////    return ffSFqe[i]; 
      //// }
      //double FancyFQuasiElastic_Plot(double *x,double *p){
      //   Double_t tau = x[0]; 
      //   Int_t i      = Int_t(p[0]); // p[0] = Fancy SF index 
      //   QRT_Tau_Integrand(tau); 
      //   return ffSFqe[i]; 
      //}
      //// double FancyFElastic_Plot(double *x,double *p){
      ////    Double_t tau = x[0];
      ////    Int_t i      = Int_t(p[0]); // p[0] = Fancy SF index 
      ////    Double_t R   = p[1];
      ////    ProcessFancyElasticStructureFunctions(R,tau,ffSFel);
      ////    return ffSFel[i]; 
      //// }
      //double FancyFElastic_Plot(double *x,double *p){
      //   Double_t tau = x[0]; 
      //   Int_t i      = Int_t(p[0]); // p[0] = Fancy SF index 
      //   ERT_Tau_Integrand(tau); 
      //   return ffSFel[i]; 
      //}
      //double GE_Plot(double *x,double *p){
      //   Double_t tau = x[0]; 
      //   ERT_Tau_Integrand(tau);
      //   return fGe; 
      //}
      //double GM_Plot(double *x,double *p){
      //   Double_t tau = x[0]; 
      //   ERT_Tau_Integrand(tau);
      //   return fGm; 
      //}
      //double t_Plot(double *x,double *p){
      //   Double_t tau = x[0]; 
      //   ERT_Tau_Integrand(tau);
      //   return fArbValue6; 
      //}
      //double reta_Plot(double *x, double * p ){Double_t tau = x[0];
      //   ERT_Tau_Integrand(tau);
      //   return reta;
      //}
      //// other F terms 
      //double Fi_Plot(double *x,double *p){
      //   Double_t tau = x[0]; 
      //   sig_F_in_Tau_Integrand_2(tau); 
      //   return F_i;
      //}
      //double Fii_Plot(double *x,double *p){
      //   Double_t tau = x[0]; 
      //   sig_F_in_Tau_Integrand_2(tau); 
      //   return F_ii;
      //}
      //// no superscript 
      //double F_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return F;
      //}
      //double F_d_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return F_d;
      //}
      //double F_2minus_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return F_2minus;
      //}
      //double F_2plus_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return F_2plus;
      //}
      //double F_1plus_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return F_1plus;
      //}
      //// superscript: xi 
      //double Fxi_d_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Fxi_d;
      //}
      //double Fxi_2plus_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Fxi_2plus;
      //}
      //double Fxi_2minus_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Fxi_2minus;
      //}
      //// superscript: xi eta 
      //double Fxieta_d_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Fxieta_d;
      //}
      //double Fxieta_2plus_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Fxieta_2plus;
      //}
      //double Fxieta_2minus_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Fxieta_2minus;
      //}
      //// superscript: eta 
      //double Feta_d_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Feta_d;
      //}
      //double Feta_2plus_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Feta_2plus;
      //}
      //double Feta_2minus_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Feta_2minus;
      //}
      //double Feta_1plus_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Feta_1plus;
      //}
      //double Feta_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Feta;
      //}
      //// superscript: eta eta 
      //double Fetaeta_d_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Fetaeta_d;
      //}
      //double Fetaeta_2plus_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Fetaeta_2plus;
      //}
      //double Fetaeta_2minus_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Fetaeta_2minus;
      //}
      //double Fetaeta_1plus_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Fetaeta_1plus;
      //}
      //double Fetaeta_Plot(double *x, double * p ){Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   return Fetaeta;
      //}
      //// B and C plots 
      //double B1_A_Plot(double *x, double * p ){
      //   Double_t tau = x[0];
      //   ERT_Tau_Integrand(tau);
      //   Double_t b1 = B1_A(tau); 
      //   return b1;
      //}
      //double B2_A_Plot(double *x, double * p ){
      //   Double_t tau = x[0];
      //   ERT_Tau_Integrand(tau);
      //   Double_t B2 = B2_A(tau); 
      //   return B2;
      //}
      //double C1_A_Plot(double *x, double * p ){
      //   Double_t tau = x[0];
      //   ERT_Tau_Integrand(tau);
      //   Double_t c1 = C1_A(tau); 
      //   return c1;
      //}
      //double C2_A_Plot(double *x, double * p ){
      //   Double_t tau = x[0];
      //   ERT_Tau_Integrand(tau);
      //   Double_t c2 = C2_A(tau); 
      //   return c2;
      //}
      //double B1_Plot(double *x, double * p ){
      //   Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   Double_t b1 = B1(tau); 
      //   return b1;
      //}
      //double B2_Plot(double *x, double * p ){
      //   Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   Double_t b2 = B2(tau); 
      //   return b2;
      //}
      //double C1_Plot(double *x, double * p ){
      //   Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   Double_t c1 = C1(tau); 
      //   return c1;
      //}
      //double C2_Plot(double *x, double * p ){
      //   Double_t tau = x[0];
      //   sig_F_in_Tau_Integrand_2(tau);
      //   Double_t c2 = C2(tau); 
      //   return c2;
      //}

      //double IRT_Tau_Integrand2(double *x, double * p ){
      //   Double_t tau = x[0];
      //   Double_t y        = fKinematics.Gety(); 
      //   Double_t S_x      = fKinematics.GetS_x(); 
      //   Double_t xb       = fKinematics.Getx();
      //   Double_t Lambda_s = fKinematics.GetLambda_s();  
      //   Double_t scale    = -1.0*TMath::Sqrt(Lambda_s)*(y/2./S_x); 
      //   return scale*sig_F_in_Tau_Integrand_2(tau)*(xb+fTau)  ;
      //}
      //double sigma_b_Plot(double *x,double *p){
      //   Double_t result = sigma_b();
      //   return result;
      //}
      /////@}

      //double IRT_R_Integrand1(double *x, double * p ){
      //   fReCalculateTs = true;
      //   Double_t R = x[0];
      //   fTau = p[0];
      //   //Double_t xb = fKinematics.Getx();
      //   return sig_F_in_R_Integrand_3(R) ;
      //}
      Double_t fArbValue1;
      Double_t fArbValue2;
      Double_t fArbValue3;
      Double_t fArbValue4; 
      Double_t fArbValue5;
      Double_t fArbValue6;  
      Double_t fArbValue7;  

      Int_t    fDepth;                     ///< Adaptive Simpson's Method variable: # of iterations                        
      Double_t fErr;                       ///< Adaptive Simpson's Method variable: error threshold  

      /** @name Integration methods */
      ///@{

      Double_t SimpleIntegration(Double_t (POLRAD::*f)(Double_t &) ,Double_t min ,Double_t max,Double_t err,Int_t depth) {
         double delta = max - min;
         int Nstep = depth; 
         double step = delta/double(Nstep);
         double res = 0;
         for(int i = 0; i<Nstep; i++) {
            double x = min + double(i)*step;
            res += step*( (this->*f)(x) ); 
         }
         return(res);
      }
      Double_t AdaptiveSimpson(Double_t (POLRAD::*)(Double_t &) ,Double_t,Double_t,Double_t,Int_t) ;
      Double_t AdaptiveSimpsonAux(Double_t (POLRAD::*)(Double_t &) ,Double_t,Double_t,Double_t,Double_t,Double_t,Double_t,Double_t,Int_t) ;
      ///@}

      ClassDef(POLRAD,7)
};
}}



#endif

