#ifndef insane_physics_CrossSection_HH
#define insane_physics_CrossSection_HH

#include "insane/base/NamedType/crtp.hpp"

#include <iostream>
#include <type_traits>

#include "insane/base/Helpers.h"
#include "insane/formfactors/FormFactors.h"
#include "insane/structurefunctions/SpinStructureFunctions.h"
#include "insane/structurefunctions/StructureFunctions.h"

namespace insane {
  namespace physics {

    template <typename T>
    using is_StructureFunction = std::is_base_of<StructureFunctions, T>;
    template <typename... T>
    using SF_Filter = typename insane::helpers::Filter<is_StructureFunction,
                                                       std::tuple<T...>>::type;

    template <typename T>
    using is_SpinStructureFunction = std::is_base_of<SpinStructureFunctions, T>;
    template <typename... T>
    using SSF_Filter =
        typename insane::helpers::Filter<is_SpinStructureFunction,
                                         std::tuple<T...>>::type;

    template <typename T>
    using is_FormFactors = std::is_base_of<FormFactors, T>;
    template <typename... T>
    using FF_Filter = typename insane::helpers::Filter<is_FormFactors,
                                                       std::tuple<T...>>::type;

    namespace impl {

      /** Helper class.
       * Applies filter to get tuples of FF,SF,SSFs,....
       * Also stores size of tuple.
       */
      template <typename... Args>
      struct FilteredFunctions {
        using SF_t = SF_Filter<Args...>;
        static constexpr size_t N_SF = std::tuple_size<SF_t>::value;

        using SSF_t = SSF_Filter<Args...>;
        static constexpr size_t N_SSF = std::tuple_size<SSF_t>::value;

        using FF_t = FF_Filter<Args...>;
        static constexpr size_t N_FF = std::tuple_size<FF_t>::value;
      };

      template <typename... Args>
      struct FilteredFunctions<std::tuple<Args...>>
          : FilteredFunctions<Args...> {};

      //@{
      /** Function Helpers
       */
      template <bool, typename... T>
      struct has_SF_impl : std::false_type {};

      template <typename... T>
      struct has_SF_impl<true, T...> : std::true_type {
        using Functions = FilteredFunctions<T...>;
      };

      template <class... T>
      constexpr bool args_have_SF() {
        return FilteredFunctions<T...>::N_SF > 0;
      }

      template <typename... T>
      using has_SF = has_SF_impl<args_have_SF<T...>(), T...>;

      template <bool, typename... T>
      struct has_SSF_impl : std::false_type {};

      template <typename... T>
      struct has_SSF_impl<true, T...> : std::true_type {
        using Functions = FilteredFunctions<T...>;
      };

      template <class... T>
      constexpr bool args_have_SSF() {
        return FilteredFunctions<T...>::N_SSF > 0;
      }

      template <typename... T>
      using has_SSF = has_SSF_impl<args_have_SSF<T...>(), T...>;

      template <bool, typename... T>
      struct has_FF_impl : std::false_type {};

      template <typename... T>
      struct has_FF_impl<true, T...> : std::true_type {
        using Functions = FilteredFunctions<T...>;
      };

      template <class... T>
      constexpr bool args_have_FF() {
        return FilteredFunctions<T...>::N_FF > 0;
      }

      template <typename... T>
      using has_FF = has_FF_impl<args_have_FF<T...>(), T...>;
      //@}

      template <template <class...> class R, class... Ts>
      struct is_value_true_impl : std::false_type {};

      template <template <class...> class R, class... Ts>
      struct is_value_true_impl<R, std::tuple<Ts...>> : R<Ts...> {};

      template <class T, template <class...> class... Rs>
      struct has_all_functions_impl {
        using Functions = T;
        using result = insane::helpers::detail::all_true<
            is_value_true_impl<Rs, T>::value...>;
      };

      template <class T, template <class...> class... Rs>
      using has_all_functions =
          typename has_all_functions_impl<T, Rs...>::result;

    } // namespace impl

    template <typename... T>
    using needs_SFs = impl::has_SF<T...>;
    template <typename... T>
    using needs_SSFs = impl::has_SSF<T...>;
    template <typename... T>
    using needs_FFs = impl::has_FF<T...>;

    /** Cross section implementation helper.
     *
     *  This class helps check that the required functions are supplied.
     *
     *  For example the elastic cross section calculation needs the form
     * factors: \code using list_of_functions = std::tuple<Stat2015_SFs,
     * Stat2015_SSFs, CTEQ10_SFs, GS_SSFs, AMT_FFs_t, Kelly_FFs_t>;
     *  CrossSection_impl <list_of_functions, needs_FFs>  elastic_xs; //  SFs
     *  \endcode
     */
    template <size_t Ndim, typename FuncTup, template <class...> class... Reqs>
    struct CrossSection_impl : impl::FilteredFunctions<FuncTup> {
      static_assert(
          insane::helpers::is_tuple<FuncTup>::value,
          "FuncTup should be a tuple of functions (FFs, SFs, SSFs, etc...)");

      static_assert(impl::has_all_functions<FuncTup, Reqs...>::value,
                    "Functions tuple T is missing one or more requirements");

      static_assert(Ndim > 0, "Ndim needs to be greater than 1");
    };

    /** Cross section class.
     *
     * Use this class to build new cross section calculations.
     *
     * For example the elastic cross section calculation needs the form
     * factors:
     * \code
     *  using list_of_functions = std::tuple<Stat2015_SFs,Stat2015_SSFs,CTEQ10_SFs,GS_SSFs,AMT_FFs_t,Kelly_FFs_t>;
     *  CrossSection_impl<list_of_functions, needs_FFs>  elastic_xs; //  SFs
     * \endcode
     */
    template <size_t Ndim, typename FuncTup, template <class...> class... Reqs>
    struct CrossSection : CrossSection_impl<Ndim, FuncTup, Reqs...> {
      using SSF_t = typename impl::FilteredFunctions<FuncTup>::SSF_t;
      using FF_t  = typename impl::FilteredFunctions<FuncTup>::FF_t;
      using SF_t  = typename impl::FilteredFunctions<FuncTup>::SF_t;

      /** dimension: number of independent degrees of freedom. */
      static constexpr size_t N_I = Ndim;
    };

    /** Elastic Cross section.
     *  Two dimensions, for example, (Q2, phi).
     */
    template <typename FuncTup>
    struct Elastic_CrossSection : CrossSection<2, FuncTup, needs_FFs> {};

    /** DIS Cross section.
     *  Three dimensions, for example, (x, Q2, phi).
     */
    template <typename FuncTup>
    struct DIS_CrossSection : CrossSection<3, FuncTup, needs_SFs> {
      using SF_t          = typename impl::FilteredFunctions<FuncTup>::SF_t;
      using StructureFunc = typename std::tuple_element<0, SF_t>::type;
    };

    /** Polarized DIS Cross section.
     *  Four dimensions, for example, (x, Q2, phi, helicity).
     *  Note the cross section would need both the structure fucntions
     *  and the spin structure functions.
     */
    template <typename FuncTup>
    struct PolarizedDIS_CrossSection
        : CrossSection<4, FuncTup, needs_SFs, needs_SSFs> {
      using SF_t          = typename impl::FilteredFunctions<FuncTup>::SF_t;
      using SSF_t         = typename impl::FilteredFunctions<FuncTup>::SSF_t;
      using StructureFunc = typename std::tuple_element<0, SF_t>::type;
      using SpinStructureFunc = typename std::tuple_element<0, SSF_t>::type;
      //StructureFunc sfs;
    };

  } // namespace physics
} // namespace insane

#endif
