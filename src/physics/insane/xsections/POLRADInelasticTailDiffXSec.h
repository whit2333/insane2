#ifndef POLRADInelasticTailDiffXSec_HH
#define POLRADInelasticTailDiffXSec_HH 1

#include "POLRADBornDiffXSec.h"
namespace insane {
namespace physics {

/** Inelastic Radiative Tail (IRT).
 *
 * \ingroup inclusiveXSec
 */
class POLRADInelasticTailDiffXSec: public POLRADBornDiffXSec {

   public:
      POLRADInelasticTailDiffXSec();
      virtual ~POLRADInelasticTailDiffXSec();
      virtual POLRADInelasticTailDiffXSec*  Clone(const char * newname) const {
         std::cout << "POLRADInelasticTailDiffXSec::Clone()\n";
         auto * copy = new POLRADInelasticTailDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual POLRADInelasticTailDiffXSec*  Clone() const { return( Clone("") ); } 
      virtual Double_t EvaluateXSec(const Double_t *x) const ; 

      ClassDef(POLRADInelasticTailDiffXSec,1)
};
}}

#endif

