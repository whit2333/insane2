#ifndef F1F209eInclusiveDiffXSec_H
#define F1F209eInclusiveDiffXSec_H 2 

#include <iostream>
#include "PhysicalConstants.h"
#include "InclusiveDiffXSec.h"
#include "insane/base/FortranWrappers.h"
namespace insane {
namespace physics {

/** Wrapper and InclusiveDiffXSec concrete implementation of P. Bosted's fortran code
 *  See F1F209.f for more details
 *
 *  Uses units of GeV and radians
 *  
 * @ingroup inclusiveXSec 
 */
class F1F209eInclusiveDiffXSec : public InclusiveDiffXSec  {

   public:
      F1F209eInclusiveDiffXSec();
      virtual ~F1F209eInclusiveDiffXSec();
      virtual F1F209eInclusiveDiffXSec*  Clone(const char * newname) const {
         std::cout << "F1F209eInclusiveDiffXSec::Clone()\n";
         auto * copy = new F1F209eInclusiveDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual F1F209eInclusiveDiffXSec*  Clone() const { return( Clone("") ); } 

      char fType;           ///< Depricated switch to use the modified version of the model   
      bool fIsModifiedVersion;     ///< Use the modified version of the model  

      /** Switch to use the modified version of F1F209.
       *  The modified version has its inelastic component of F1 and F2 scaled 
       *  so that the fit is more consistent with JLab E94-010 and E01-012 data.  
       *  Usage: Set fType to 'y' to use. 
       *  Note: You MUST set the beam energy if you want to use this! 
       *  This version of the method is depricated; use the method below
       *  that takes a boolean argument.  
       */
      void UseModifiedModel(char ans){
         fType = ans;
         if(ans=='y'){
            //std::cout << "[F1F209eInclusiveXSec]: Using the MODIFIED model." << std::endl; 
            fIsModifiedVersion = true; 
         }else if(ans=='n'){
            //std::cout << "[F1F209eInclusiveXSec]: Using the original model." << std::endl; 
            fIsModifiedVersion = false; 
         }
      }

      void UseModifiedModel(bool ans){
         fIsModifiedVersion=ans;
         if(fIsModifiedVersion){
            //std::cout << "[F1F209eInclusiveXSec]: Using the MODIFIED model." << std::endl; 
         }else{
            //std::cout << "[F1F209eInclusiveXSec]: Using the original model." << std::endl; 
         }
      }

      /** Evaluate Cross Section.
       *  Note: cross_tot_ returns a xsection in units of (mbarn/GeV/sr)
       *  This method returns (nbarn/GeV/sr)
       */
      Double_t EvaluateXSec(const Double_t *) const;

      ClassDef(F1F209eInclusiveDiffXSec,2)
};

class F1F209QuasiElasticDiffXSec : public F1F209eInclusiveDiffXSec {

   public:

      F1F209QuasiElasticDiffXSec();
      virtual ~F1F209QuasiElasticDiffXSec();
      virtual F1F209QuasiElasticDiffXSec*  Clone(const char * newname) const {
         std::cout << "F1F209QuasiElasticDiffXSec::Clone()\n";
         auto * copy = new F1F209QuasiElasticDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual F1F209QuasiElasticDiffXSec*  Clone() const { return( Clone("") ); } 

      Double_t EvaluateXSec(const Double_t *) const;

   ClassDef(F1F209QuasiElasticDiffXSec,1)
};

}
}
//class F1F209CompositeDiffXSec : public CompositeDiffXSec {
//
//   public:
//      F1F209CompositeDiffXSec(){
//         fProtonXSec  = new F1F209eInclusiveDiffXSec();
//         fNeutronXSec = new F1F209eInclusiveDiffXSec();
//         std::cout << "done" << std::endl;
//      }
//      virtual ~F1F209CompositeDiffXSec(){
//         if(fProtonXSec)  delete fProtonXSec;
//         if(fNeutronXSec) delete fNeutronXSec;
//      }
//
//   ClassDef(F1F209CompositeDiffXSec,1)
//};
#endif

