#ifndef CompositeDiffXSec2_HH
#define CompositeDiffXSec2_HH 1

#include "DiffXSec.h"
#include "InelasticRadiativeTail2.h"
#include "insane/base/FortranWrappers.h"
#include "InSANEMathFunc.h"


namespace insane {
   namespace physics {

      /** Builds a cross section for a target nucleus from the supplied
       *  proton and neutron cross secitons.
       *
       * \ingroup XSec
       */
      class CompositeDiffXSec2 : public DiffXSec {

         protected:
            DiffXSec * fProtonXSec;   //->
            DiffXSec * fNeutronXSec;  //->

         public:

            CompositeDiffXSec2();
            CompositeDiffXSec2(const CompositeDiffXSec2& rhs) ;
            virtual ~CompositeDiffXSec2();
            CompositeDiffXSec2& operator=(const CompositeDiffXSec2& rhs) ;
            CompositeDiffXSec2(CompositeDiffXSec2&&) = default;
            CompositeDiffXSec2& operator=(CompositeDiffXSec2&&) & = default;       // Move assignment operator

            virtual CompositeDiffXSec2*  Clone(const char * newname) const ;
            virtual CompositeDiffXSec2*  Clone() const ; 

            virtual Double_t EvaluateXSec(const Double_t * x) const ;

            // Here we copied a bunch of setters from DiffXSec because we need to set not only this class
            // but the proton and neutron cross sections. The getters are not changed because they should be identical
            // to this class (and really not used).
            virtual void SetTargetMaterial(const TargetMaterial& mat) ;
            virtual void SetTargetNucleus(const Nucleus & targ) ;

            void       SetTargetMaterialIndex(Int_t i) ;

            virtual void  SetUnits(const char * t);
            virtual void  SetBeamEnergy(Double_t en);
            virtual void  SetPhaseSpace(PhaseSpace * ps);
            virtual void  InitializePhaseSpaceVariables();
            virtual void  InitializeFinalStateParticles();
            virtual void  SetParticleType(Int_t pdgcode, Int_t part = 0) ;

            ClassDef(CompositeDiffXSec2,1)
      };

   }
}


#endif

