#ifndef insane_physics_CrossSectionInfo_HH
#define insane_physics_CrossSectionInfo_HH

#include "TObject.h"
#include <map>
#include <vector>
#include <string>

namespace insane {
   namespace physics {

      class CrossSectionInfo {

         protected:
            std::map<int,std::string> fNameMap;

         private: 
            CrossSectionInfo( );
            static CrossSectionInfo* fgCrossSectionInfo;

         public:
            ~CrossSectionInfo();
            static CrossSectionInfo* GetInstance();

            std::string GetName(int n) const {
               std::string res = "";
               auto it = fNameMap.find(n);
               if( it != fNameMap.end() ) {
                  res = fNameMap.at(n);
               }
               return  res;
            }

            ClassDef(CrossSectionInfo,1)
      };

   }
}
 

#endif

