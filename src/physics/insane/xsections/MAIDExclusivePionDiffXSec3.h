#ifndef MAIDExclusivePionDiffXSec3_H 
#define MAIDExclusivePionDiffXSec3_H 1

#include <cstdlib> 
#include <iostream> 
#include <cmath> 
#include <vector> 
#include "TVector3.h"
#include "TRotation.h"
#include "TString.h"
#include "MAIDPolarizedKinematicKey.h"
#include "ExclusiveDiffXSec.h"
#include "maid07.h"

namespace insane {
namespace physics {

/** MAID cross section for (polarized target) threshold (exlusive) pion production.
 * 
 * Paper reference: J. Phys. G: Nucl. Part. Phys 18, 449 (1992)  
 * For the reaction e N --> e' N' pi, where the user specifies pi and N'. 
 * Uses data from the MAID 2007 calculations: polarized target.
 * 
 *
 * \ingroup exclusiveXSec
 */ 
class MAIDExclusivePionDiffXSec3: public ExclusiveDiffXSec{

   protected:
      mutable TVector3        fP_t_cm;
      mutable TVector3        fA_t;
      mutable TVector3        fA_et;
      mutable TVector3        fX_axis;
      mutable TVector3        fY_axis;
      mutable TVector3        fZ_axis;
      mutable TVector3        fYScat_axis; 
      mutable TVector3        fk1_CM;
      mutable TVector3        fk2_CM;
      mutable TVector3        fP1_CM;
      mutable TVector3        fP2_CM;
      mutable TVector3        fkpi_CM;
      mutable TVector3        fq_CM;
      mutable TVector3        fPT_CM;
      mutable TVector3        fBoostToCM;
      mutable TLorentzVector  f4Veck1_CM;
      mutable TLorentzVector  f4Veck2_CM;
      mutable TLorentzVector  f4Vecq_CM;
      mutable TLorentzVector  f4Veckpi_CM;
      mutable TLorentzVector  f4VecP1_CM;
      mutable TLorentzVector  f4VecP2_CM;
      mutable TLorentzVector  f4VecPT_CM;
      mutable TRotation       fRotateToq;

      mutable double x_eval[20];
      TString  fConventionName; 
      TString  fPion,fNucleon,frxn; 
      bool     fDebug;
      Int_t    fConvention;
      Double_t fPx,fPz;

      mutable MAIDPolarizedKinematicKey fKineKey;
     
      Double_t EvaluateVirtualPhotonFlux(Double_t,Double_t) const; 
      Double_t EvaluateFluxFactor(Double_t,Double_t,Double_t) const; 

      mutable TRotation labToScat;
      mutable TRotation scatToLab;
      mutable TVector3  betaToCM; 
      mutable TVector3  betaToLAB;

      mutable TVector3  fPT_scat;  // Target Polarization vector in scat plane coords
      mutable TVector3  fA_target;
      mutable TVector3  fA_beam_target;

      mutable TLorentzVector f4Veck1_LAB;
      mutable TLorentzVector f4Veck2_LAB;
      mutable TLorentzVector f4Vecq_LAB;
      mutable TLorentzVector f4VecP1_LAB;
      mutable TLorentzVector f4VecP2_LAB;
      mutable TLorentzVector f4Veckpi_LAB;

      mutable TVector3 fk1_LAB;
      mutable TVector3 fk2_LAB;
      mutable TVector3 fq_LAB;
      mutable TVector3 fP1_LAB;
      mutable TVector3 fP2_LAB;
      mutable TVector3 fkpi_LAB;

   public:

      Double_t  fM_1;
      Double_t  fM_2;
      Double_t  fM_pi;

      mutable Double_t  fOmega_th_lab;  // thrshold lab erngy
      mutable Double_t  fOmega_pi_lab;  // pion energy in the lab
      mutable Double_t  fOmega_pi_cm;   // pion energy in the cm
      mutable Double_t  fTheta_pi_cm;   // theta pion in cm
      mutable Double_t  fPhi_pi_cm;     // phi pion in cm
      mutable Double_t  fOmega_pi_cm_over_lab;

      mutable Double_t  fTheta_pi_q_lab; // Angle between pion and q momentum in the lab. 

      Double_t GetVirtualPhotonCrossSection(MAIDPolarizedKinematicKey* key, Double_t phi_e) const;
 
   public: 

      MAIDExclusivePionDiffXSec3(const char * pion = "pi0",const char * nucleon = "p"); 
      virtual ~MAIDExclusivePionDiffXSec3();

      virtual Double_t   EvaluateXSec(const Double_t *) const; 
      virtual Double_t * GetDependentVariables(const Double_t * ) const;
      virtual void       InitializePhaseSpaceVariables();
      virtual void SetTargetNucleus(const Nucleus & targ);

      void       DebugMode(bool ans){fDebug = ans;} // doesn't do anything yet...  
      void       Print(Option_t * ){ }
      void       PrintData();  
      void       PrintParameters();  
      void       Clear(Option_t * ); 
      void       SetVirtualPhotonFluxConvention(Int_t i);
      void       SetVirtualPhotonFluxConvention(const char * conv = "Hand" );
      void       SetReactionChannel(TString pion,TString nucleon);

      double EnergyDependentXSec(double *x, double *p) {
         Double_t y[5];
         y[0] = x[0];
         y[1] = p[0];
         y[2] = p[1];
         y[3] = p[2];
         y[4] = p[3];
         return(EvaluateXSec(GetDependentVariables(y)));
      }

      double AngleDependentXSec(double *x, double *p) {
         Double_t y[5];
         y[0] = p[0];
         y[1] = x[0];
         y[2] = p[1];
         y[3] = p[2];
         y[4] = p[3];
         return(EvaluateXSec(GetDependentVariables(y)));
      }
      double PhiAngleDependentXSec(double *x, double *p) {
         Double_t y[5];
         y[0] = p[0];
         y[1] = p[1];
         y[2] = x[0];
         y[3] = p[2];
         y[4] = p[3];
         return(EvaluateXSec(GetDependentVariables(y)));
      }
      double PionAngleDependentXSec(double *x, double *p) {
         Double_t y[5];
         y[0] = p[0];
         y[1] = p[1];
         y[2] = p[2];
         y[3] = x[0];
         y[4] = p[3];
         return(EvaluateXSec(GetDependentVariables(y)));
      }
      double PionPhiAngleDependentXSec(double *x, double *p) {
         Double_t y[5];
         y[0] = p[0];
         y[1] = p[1];
         y[2] = p[2];
         y[3] = p[3];
         y[4] = x[0];
         return(EvaluateXSec(GetDependentVariables(y)));
      }
      ClassDef(MAIDExclusivePionDiffXSec3,1)
};

}}
#endif 
