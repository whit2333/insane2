#ifndef EICInclusiveBornDISXSec_HH
#define EICInclusiveBornDISXSec_HH 1

#include "InclusiveDiffXSec.h"
#include "InelasticRadiativeTail2.h"
#include "insane/base/FortranWrappers.h"
#include "CompositeDiffXSec2.h"


namespace insane {
  namespace physics {
    namespace eic {

      /** Simply uses any structure function.
       *
       * \ingroup inclusiveXSec
       */
      class EICInclusiveBornDISXSec : public InclusiveDiffXSec {

        protected:
          struct IncoherentKinematics {
            TLorentzVector    fP0;
            TLorentzVector    fk1;
            TLorentzVector    fk2;
            TLorentzVector    fP0_Nrest;
            TLorentzVector    fk1_Nrest;
            TLorentzVector    fk2_Nrest;
          };
          mutable IncoherentKinematics fKine;

          CompositeDiffXSec2 * xs = nullptr;

        public:
          EICInclusiveBornDISXSec();
          virtual ~EICInclusiveBornDISXSec();
          EICInclusiveBornDISXSec(const EICInclusiveBornDISXSec& rhs);
          EICInclusiveBornDISXSec& operator=(const EICInclusiveBornDISXSec& rhs);
          virtual EICInclusiveBornDISXSec*  Clone(const char * newname) const;
          virtual EICInclusiveBornDISXSec*  Clone() const;

          virtual void SetIonBeamEnergy(Double_t P0){
            fKine.fP0 = {0.0, 0.0, -TMath::Sqrt(P0*P0-(M_p/GeV)*(M_p/GeV)), P0};
          }
          virtual Double_t GetIonBeamEnergy() const {return fKine.fP0.E();}

          virtual void       InitializePhaseSpaceVariables() ;
          //virtual void       DefineEvent(Double_t * vars);
          //virtual Double_t * GetDependentVariables(const Double_t * x) const ;

          virtual Double_t EvaluateXSec(const Double_t *x) const;

          ClassDef(EICInclusiveBornDISXSec,1)
      };

    }
  }
}

#endif

