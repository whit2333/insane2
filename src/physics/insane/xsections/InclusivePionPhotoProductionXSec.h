#ifndef InclusivePionPhotoProductionXSec_HH
#define InclusivePionPhotoProductionXSec_HH

#include "PhotonDiffXSec.h"
namespace insane {
namespace physics {


/**  Inclusive photo production of pions, kaons, and nucleons from a nucleon target.
 *   For all targets use InclusivePhotoProductionXSec.
 * \ingroup inclusiveXSec
 */
class InclusivePionPhotoProductionXSec : public PhotonDiffXSec {

   protected:
      PhotonDiffXSec * fSig_0;       //->
      PhotonDiffXSec * fSig_plus;    //->
      PhotonDiffXSec * fSig_minus;   //->

   public:
      InclusivePionPhotoProductionXSec();
      InclusivePionPhotoProductionXSec(const InclusivePionPhotoProductionXSec& rhs) ;
      virtual ~InclusivePionPhotoProductionXSec();
      InclusivePionPhotoProductionXSec& operator=(const InclusivePionPhotoProductionXSec& rhs) ;
      virtual InclusivePionPhotoProductionXSec*  Clone(const char * newname) const ;
      virtual InclusivePionPhotoProductionXSec*  Clone() const ; 

      Double_t EvaluateXSec(const Double_t * x) const ;

      void  SetParameters(int i, const std::vector<double>& pars);
      const std::vector<double>& GetParameters() const ;

      virtual void   SetRadiationLength(Double_t r) { 
         ((PhotonDiffXSec*)fSig_0)->SetRadiationLength(r);
         ((PhotonDiffXSec*)fSig_plus)->SetRadiationLength(r);
         ((PhotonDiffXSec*)fSig_minus)->SetRadiationLength(r);
      }
      // Here we copied a bunch of setters from DiffXSec because we need to set not only this class
      // but the proton and neutron cross sections. The getters are not changed because they should be identical
      // to this class (and really not used).
      void       SetTargetMaterial(const TargetMaterial& mat){
         fSig_0->SetTargetMaterial(mat);
         fSig_plus->SetTargetMaterial(mat);
         fSig_minus->SetTargetMaterial(mat);
         fTargetMaterial = mat;
      }
      void SetTargetNucleus(const Nucleus & targ){
         InclusiveDiffXSec::SetTargetNucleus(targ);
         //if(fProtonXSec)   fProtonXSec->SetTargetNucleus(targ);
         //if(fNeutronXSec) fNeutronXSec->SetTargetNucleus(targ);
      }

      virtual void InitializePhaseSpaceVariables();

      void       SetTargetMaterialIndex(Int_t i){
         fSig_0->SetTargetMaterialIndex(i);
         fSig_plus->SetTargetMaterialIndex(i);
         fSig_minus->SetTargetMaterialIndex(i);
         fMaterialIndex = i ;
      }
      virtual void  SetBeamEnergy(Double_t en){
         InclusiveDiffXSec::SetBeamEnergy(en);
         fSig_0->SetBeamEnergy(en);
         fSig_plus->SetBeamEnergy(en);
         fSig_minus->SetBeamEnergy(en);
      }

      virtual void  SetPhaseSpace(PhaseSpace * ps);
      virtual void  InitializeFinalStateParticles();

   ClassDef(InclusivePionPhotoProductionXSec,1)
};

}}

#endif

