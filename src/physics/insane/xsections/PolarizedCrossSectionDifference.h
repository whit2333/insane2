#ifndef PolarizedCrossSectionDifference_H 
#define PolarizedCrossSectionDifference_H 1 

#include <iostream>
#include "Nucleus.h"
#include "DipoleFormFactors.h"
#include "PhysicalConstants.h"
#include "InclusiveDiffXSec.h"
#include "insane/base/FortranWrappers.h"
#include "PolarizedStructureFunctions.h"

namespace insane {
  namespace physics {

    class PolarizedCrossSectionDifference: public InclusiveDiffXSec {

    private:
      bool fUseScalingFunction; 
      Int_t fPhysType;      ///< 0 => elastic, 1 => QE; 2 => inelastic (default)  
      //Nucleus fNucleus; 
      //PolarizedStructureFunctions *fPolSFs;
      //FormFactors *fFF;

      void ProcessPolarizedStructureFunctions(Double_t,Double_t,Double_t &,Double_t &) const;

      Double_t ScalingFunction(Double_t) const; 

    public:
      PolarizedCrossSectionDifference();
      virtual ~PolarizedCrossSectionDifference(); 

      void InitializePhaseSpaceVariables() {
        InclusiveDiffXSec::InitializePhaseSpaceVariables();
      }

      void UseScalingFunction(bool ans=true){
        fUseScalingFunction = ans; 
        if(fUseScalingFunction){
          std::cout << "[PolarizedCrossSectionDifference]: Will use scaling function. " << std::endl;
        }else{
          std::cout << "[PolarizedCrossSectionDifference]: Will not use scaling function. " << std::endl;
        }
      }

      void SetPhysicsType(Int_t t){
        fPhysType = t; 
        switch(fPhysType){
          case 0: 
            std::cout << "[PolarizedCrossSectionDifference]: ";
            std::cout << "Will do ELASTIC calculation.  Be sure to set the appropriate form factors! " << std::endl; 
            break;
          case 1: 
            std::cout << "[PolarizedCrossSectionDifference]: ";
            std::cout << "Will do QE calculation.  Be sure to set the appropriate form factors! " << std::endl; 
            break;
          case 2:  
            std::cout << "[PolarizedCrossSectionDifference]: ";
            std::cout << "Will do INELASTIC calculation.  Be sure to set the appropriate polarized structure functions! " << std::endl; 
            break;
        } 
      }  

      //void SetTargetType(Nucleus::NucleusType i){fNucleus.SetType(i);} 
      //void SetPolarizedStructureFunctions(PolarizedStructureFunctions *psfs){fPolSFs = psfs;}
      //void SetFormFactors(FormFactors *f){ fFF = f; }

      Double_t EvaluateXSec(const Double_t *) const; 
      Double_t EvaluateAsymmetry(const Double_t *) const { return(0.0);} 

      ClassDef(PolarizedCrossSectionDifference,1) 

    };
  }
}
#endif 
