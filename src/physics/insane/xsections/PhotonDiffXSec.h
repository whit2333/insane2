#ifndef PhotonDiffXSec_HH
#define PhotonDiffXSec_HH 1

#include "InclusiveDiffXSec.h"
#include "insane/base/FortranWrappers.h"
#include "CompositeDiffXSec.h"
#include <vector>
namespace insane {
namespace physics {


/** Mono energetic photon beam cross section for inclusive particle production.
 *
 * \ingroup inclusiveXSec
 */
class PhotonDiffXSec : public InclusiveDiffXSec {

   private:
      mutable double      fArgCopy[6];

   protected:
      TParticlePDG *      fParticle; //->
      Double_t            fRadiationLength;
      std::vector<double> fParameters;

   public:
      std::vector<double> fParameters0;
      std::vector<double> fParameters1;
      std::vector<double> fParameters2;

   public:
      PhotonDiffXSec();
      PhotonDiffXSec(const PhotonDiffXSec& rhs);
      virtual ~PhotonDiffXSec();
      PhotonDiffXSec& operator=(const PhotonDiffXSec& rhs);
      virtual PhotonDiffXSec*  Clone(const char * newname) const ;
      virtual PhotonDiffXSec*  Clone() const ; 

      Double_t     GetRadiationLength() {            return fRadiationLength; }
      virtual void SetRadiationLength(Double_t r) { fRadiationLength = r; }

      //TParticlePDG * GetParticlePDG()  { return fParticle; }
      //Int_t          GetParticleType() { return fParticle->PdgCode(); }
      void           SetProductionParticleType(Int_t PDGcode, Int_t i=0);
      void           SetParticlePDGEncoding(Int_t PDGcode) { SetProductionParticleType(PDGcode); }

      void                       SetParameters(const std::vector<double>& pars) { fParameters = pars; }
      const std::vector<double>& GetParameters() const { return fParameters; }
      
      virtual void InitializePhaseSpaceVariables();

      double       FitFunction(double * x, const std::vector<double>& p) const ;
      double       FitFunction2(double * x, const std::vector<double>& p) const ;
      double       FitFunction3(double * x, const std::vector<double>& p) const ;
      double       FitFunction4(double * x, const std::vector<double>& p) const ;
      double       FitFunction5(double * x, const std::vector<double>& p) const ;

      Double_t EvaluateXSec(const Double_t * x) const ;

      ClassDef(PhotonDiffXSec, 1)
};

}}

#endif

