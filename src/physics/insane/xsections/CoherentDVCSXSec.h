#ifndef CoherentDVCSXSec_H
#define CoherentDVCSXSec_H 1

#include "insane/base/ExclusiveDiffXSec.h"
#include "insane/base/InclusiveDiffXSec.h"
#include "TMath.h"
#include "insane/formfactors/FormFactors.h"
#include "TVector3.h"

#include "insane/kinematics/KinematicFunctions.h"
#include "insane/formfactors/DipoleFormFactors.h"


namespace insane {
   namespace physics {

      /** Elastic scattering using the Dipole FFs.
       * \ingroup exclusiveXSec
       */
      class CoherentDVCSXSec : public ExclusiveDiffXSec {

         protected:

            // BSA fit parameters... crude.
            double falpha = 0.3;
            double fbeta  =-0.1;

            double fNuclearTargetMass = 3.7;

            mutable DipoleFormFactors   fDipoleFFs;

         public:
            CoherentDVCSXSec();
            virtual ~CoherentDVCSXSec();
            CoherentDVCSXSec(const CoherentDVCSXSec& rhs) = default ;
            CoherentDVCSXSec& operator=(const CoherentDVCSXSec& rhs) = default ;
            CoherentDVCSXSec(CoherentDVCSXSec&&) = default;
            CoherentDVCSXSec& operator=(CoherentDVCSXSec&&) & = default; // Move assignment operator

            virtual CoherentDVCSXSec*  Clone(const char * newname) const {
               std::cout << "CoherentDVCSXSec::Clone()\n";
               auto * copy = new CoherentDVCSXSec();
               (*copy) = (*this);
               return copy;
            }
            virtual CoherentDVCSXSec*  Clone() const { return( Clone("") ); } 

            virtual void       InitializePhaseSpaceVariables() ;
            virtual void       DefineEvent(Double_t * vars);
            virtual Double_t * GetDependentVariables(const Double_t * x) const ;
            virtual Double_t   EvaluateXSec(const Double_t * x) const;
            virtual Double_t   GetBeamSpinAsymmetry(const Double_t * x) const;

            /**  Returns the scattered electron energy using the angle. */
            Double_t GetEPrime(const Double_t theta) const ;
            virtual Double_t Jacobian(const Double_t * vars) const;

            ClassDef(CoherentDVCSXSec, 1)
      };

   }
}

#endif

