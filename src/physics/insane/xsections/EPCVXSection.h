#ifndef EPCVXSection_HH
#define EPCVXSection_HH 1

#include "PhysicalConstants.h"

#include "InclusiveDiffXSec.h"

namespace insane {
namespace physics {
/** EPCV cross section.
 *  Uses old FORTRAN subroutines which calculate electroproduction crosssections.
 *
 *   ELECTROPRODUCTION YIELDS OF NUCLEONS AND PIONS 
 *   WRITTEN BY J.S. O'CONNELL AND J.W. LIGHTBODY, JR.
 *   NATIONAL BUREAU OF STANDARDS 
 *   SEPTEMBER 1987 
 *
 *  
 * @ingroup inclusiveXSec 
 */
class InclusiveEPCVXSec : public InclusiveDiffXSec {
   protected:
      TParticlePDG   * fParticle; //->
      TString          fEPCVParticle;

   public:
      InclusiveEPCVXSec();

      virtual ~InclusiveEPCVXSec() { }

      /** eN--> gamma* N --> e' P X */
      virtual void InitializePhaseSpaceVariables();

      void PrintPossibleParticles();

      /** returns a pointer to the TParticlePDG object used to determine the
       *  type of particle used in the cross section calculation.
       */
      TParticlePDG * GetParticlePDG() { return fParticle; }

      /** returns the PDG particle encoding */
      Int_t GetParticleType() { return fParticle->PdgCode(); }

      /** Set the particle type by PDG encoding see InclusiveWiserXSec::PrintPossibleParticles() */
      void SetProductionParticleType(Int_t PDGcode) {
         fParticle = TDatabasePDG::Instance()->GetParticle(PDGcode);
         fTitle = Form("Inclusive %s production ", fParticle->GetName());
         fPIDs.clear();
         fPIDs.push_back(PDGcode);
         fEPCVParticle = GetEPCVParticleType(PDGcode);
      }

      void SetParticlePDGEncoding(Int_t PDGcode) {
         SetProductionParticleType(PDGcode);
      }

      /** Using the PDG particle encoding, calculate the particle type number
       *  needed for the legacy EPCV Fortran code.
       *  see PrintPossibleParticles()
       */
      char * GetEPCVParticleType(Int_t PDGcode);

      /** Evaluate Cross Section (mbarn/GeV*str).
       */
      Double_t EvaluateXSec(const Double_t * x) const ;

      /**  Needed by ROOT::Math::IBaseFunctionMultiDim */
      unsigned int NDim() const { return fnDim; }

      virtual double Evaluate2D_p_theta(double *x, double *p){
         using namespace insane::units;
         double pp = x[0];
         double Ep = TMath::Sqrt(x[0]*x[0]+(M_pi0/GeV)*(M_pi0/GeV));
         fFuncArgs[0] = Ep;
         fFuncArgs[1] = x[1];
         fFuncArgs[2] = p[0];
         return( (pp/Ep)*EvaluateXSec(fFuncArgs));
      }

      ClassDef(InclusiveEPCVXSec, 1)
};

/** EPC version 2.0. 
 *  
 * @ingroup inclusiveXSec 
 */
class InclusiveEPCVXSec2 : public InclusiveEPCVXSec {
   public:
      InclusiveEPCVXSec2();
      virtual ~InclusiveEPCVXSec2();

      Double_t EvaluateXSec(const Double_t * x) const ;

   ClassDef(InclusiveEPCVXSec2, 1)
};
}}

#endif
