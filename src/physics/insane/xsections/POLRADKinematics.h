#ifndef POLRADKinematics_HH
#define POLRADKinematics_HH 1 

#include <cstdlib> 
#include <iostream> 
#include <vector>
#include "TMath.h"
#include "TObject.h"
#include "TLorentzVector.h"
namespace insane {
namespace physics {

/** Holds the variables used by POLRAD.
 *
 */
class POLRADVariables {

   private:
      TLorentzVector * fk1;   //!
      TLorentzVector * fk2;   //!
      TLorentzVector * fP;    //!
      TLorentzVector * fXi;   //!
      TLorentzVector * fEta;  //!

      Double_t        fHyperPlaneCoeffs[6]; //-> Coefficients a_eta,b_eta,c_eta,a_xi,b_xi,c_xi in that order
      TLorentzVector  fKappa;
      Double_t        Q2,Sx,S,X,Sp,M2,m2,Q2m;
      Double_t        eta_dot_q,eta_dot_p,eta_dot_kappa,xi_dot_eta,xi_dot_p,k2_dot_xi;

   protected:
      TLorentzVector  fq;
      TLorentzVector  fP_A;
      Double_t fx;
      Double_t fy;
      Double_t fx_A;
      Double_t fy_A;
      Double_t fW;
      Double_t fW2;
      Double_t fQ2;
      Double_t fQ2_m;
      Double_t fS;
      Double_t fS_x;
      Double_t fS_p;
      Double_t fX;
      Double_t fm;
      Double_t fM;
      Double_t fM_pion;
      Double_t fM_A;
      Double_t fLambda_Q;
      Double_t fLambda_s;
      Double_t fLambda_sA;
      Double_t fLambda_m;
      Double_t fLambda_QA;
      Double_t fL_m;
      Double_t fX_A;
      Double_t fS_A;
      Double_t fS_xA;
      Double_t fS_pA;
      Double_t fl_m;
      Double_t fz_s;
      Double_t fz_p;
      Double_t fS_prime ;
      Double_t fX_prime ;
      Double_t fLambda_Sprime;
      Double_t fLambda_Xprime;
      Double_t fL_Sprime;
      Double_t fL_Xprime;
      Double_t fa_prime;
      Double_t fb_prime;
      Double_t fa_eta,fb_eta,fc_eta; /// Coefficients for B.10
      Double_t fa_xi,fb_xi,fc_xi;    /// Coefficients for B.10
      Double_t fa_eta_A,fb_eta_A,fc_eta_A; /// Coefficients for B.10
      Double_t fa_xi_A,fb_xi_A,fc_xi_A;    /// Coefficients for B.10
      Double_t fTau_min,fTau_max,fTau_A_min,fTau_A_max;
      /// The following are for the external tail calculation 
      Double_t fEs,fEp,fTheta; 

   public:
      POLRADVariables();
      virtual ~POLRADVariables();
      void Update();
      void Clear();
      void Print();

      /** Returns array with a_eta,b_eta,c_eta,a_xi,b_xi,c_xi in that order.
       *  Solved for coefficents of B.10 by solving system of equations mostly found in B.11.
       */
      Double_t * GetHPCoefficients();
      Double_t * GetHPCoefficients_A();

      void Setk1(TLorentzVector* v)  {fk1 = v;}
      void Setk2(TLorentzVector* v)  {fk2 = v;}
      void SetP(TLorentzVector* v)   {fP = v;}
      void SetXi(TLorentzVector* v)  {fXi = v;}
      void SetEta(TLorentzVector* v) {fEta = v;}

      friend class POLRADKinematics;

      ClassDef(POLRADVariables,1)
};


/** Kinematic storage for POLRAD.
 *
 */
class POLRADKinematics {
   protected:

      /** @name Variables that need to be set 
       *  @{
       */
      TLorentzVector  fk1;
      TLorentzVector  fk2;
      TLorentzVector  fq;
      TLorentzVector  fP;
      TLorentzVector  fXi;
      TLorentzVector  fEta;
      //@}

      mutable bool fIsModified;
      mutable POLRADVariables fVariables;

   public:
      TLorentzVector Getk1()  const { return(fk1); }
      TLorentzVector Getk2()  const { return(fk2); }
      TLorentzVector GetP()   const { return(fP); }
      TLorentzVector GetXi()  const { return(fXi); }
      TLorentzVector GetEta() const { return(fEta); }

      void  Setm(Double_t M)        { fVariables.fm = M; fIsModified = true; } 
      void  SetM_pion(Double_t M)   { fVariables.fM_pion = M; fIsModified = true; } 
      void  SetM(Double_t M)        { fVariables.fM = M; fIsModified = true; } 
      void  SetM_A(Double_t M_A)    { fVariables.fM_A = M_A; fIsModified = true; } 
      void  Setk1(TLorentzVector v) { fk1 = v; fIsModified = true; } 
      void  Setk2(TLorentzVector v) { fk2 = v; fIsModified = true; } 
      void  SetP(TLorentzVector v)  { fP = v; fIsModified = true; } 
      void  SetXi(TLorentzVector v) { fXi = v; fIsModified = true; } 
      void  SetEta(TLorentzVector v){ fEta = v; fIsModified = true; }

      void SetEs(Double_t Es){fVariables.fEs = Es; fIsModified = true;}  
      void SetEp(Double_t Ep){fVariables.fEp = Ep; fIsModified = true;}  
      void SetTheta(Double_t th){fVariables.fTheta = th; fIsModified = true;}  

      POLRADKinematics();
      virtual ~POLRADKinematics(); 
      void Print(){CheckUpdate();fVariables.Print();}

      void     CheckUpdate()  const { if(fIsModified) { fVariables.Update(); fIsModified = false; } }
      Double_t Getm()         const { CheckUpdate(); return fVariables.fm; }
      Double_t GetM()         const { CheckUpdate(); return fVariables.fM; }
      Double_t GetM_pion()    const { CheckUpdate(); return fVariables.fM_pion; }
      Double_t GetM_A()       const { CheckUpdate(); return fVariables.fM_A; }
      Double_t GetQ2()        const { CheckUpdate(); return fVariables.fQ2; }
      Double_t GetW()         const { CheckUpdate(); return fVariables.fW; }
      Double_t GetW2()        const { CheckUpdate(); return fVariables.fW2; }
      Double_t GetS()         const { CheckUpdate(); return fVariables.fS; }
      Double_t GetX()         const { CheckUpdate(); return fVariables.fX; }
      Double_t GetS_A()       const { CheckUpdate(); return fVariables.fS_A; }
      Double_t GetX_A()       const { CheckUpdate(); return fVariables.fX_A; }
      Double_t GetS_x()       const { CheckUpdate(); return fVariables.fS_x; }
      Double_t GetS_xA()      const { CheckUpdate(); return fVariables.fS_xA; }
      Double_t Getx()         const { CheckUpdate(); return fVariables.fx; }
      Double_t Gety()         const { CheckUpdate(); return fVariables.fy; }
      Double_t Getx_A()       const { CheckUpdate(); return fVariables.fx_A; }
      Double_t Gety_A()       const { CheckUpdate(); return fVariables.fy_A; }
      Double_t GetQ2_m()      const { CheckUpdate(); return fVariables.fQ2_m; }
      Double_t GetS_p()       const { CheckUpdate(); return fVariables.fS_p; }
      Double_t GetS_pA()      const { CheckUpdate(); return fVariables.fS_pA; }
      Double_t GetLambda_m()  const { CheckUpdate(); return fVariables.fLambda_m; }
      Double_t GetLambda_s()  const { CheckUpdate(); return fVariables.fLambda_s; }
      Double_t GetLambda_sA() const { CheckUpdate(); return fVariables.fLambda_sA; }
      Double_t GetLambda_QA() const { CheckUpdate(); return fVariables.fLambda_QA; }
      Double_t GetLambda_Q()  const { CheckUpdate(); return fVariables.fLambda_Q; }
      Double_t GetL_m()       const { CheckUpdate(); return fVariables.fL_m; } 
      Double_t Getl_m()       const { CheckUpdate(); return fVariables.fl_m; }
      Double_t Getz_s()       const { CheckUpdate(); return fVariables.fz_s; }
      Double_t Getz_p()       const { CheckUpdate(); return fVariables.fz_p; }
      Double_t GetS_prime()   const { CheckUpdate(); return fVariables.fS_prime; }
      Double_t GetX_prime()   const { CheckUpdate(); return fVariables.fX_prime; }
      Double_t Geta_prime()   const { CheckUpdate(); return fVariables.fa_prime; }
      Double_t Getb_prime()   const { CheckUpdate(); return fVariables.fb_prime; }
      Double_t GetL_Sprime()  const { CheckUpdate(); return fVariables.fL_Sprime; }
      Double_t GetL_Xprime()  const { CheckUpdate(); return fVariables.fL_Xprime; }
      Double_t GetLambda_Sprime()  const { CheckUpdate(); return fVariables.fLambda_Sprime; }
      Double_t GetLambda_Xprime()  const { CheckUpdate(); return fVariables.fLambda_Xprime; }
      Double_t GetTau_min()   const { CheckUpdate(); return fVariables.fTau_min; }
      Double_t GetTau_max()   const { CheckUpdate(); return fVariables.fTau_max; }
      Double_t GetTau_A_min() const { CheckUpdate(); return fVariables.fTau_A_min; }
      Double_t GetTau_A_max() const { CheckUpdate(); return fVariables.fTau_A_max; }

      Double_t * GetHPCoefficients(){ CheckUpdate();  return(fVariables.GetHPCoefficients()); }
      TLorentzVector& Getq()  const { CheckUpdate();  return(fVariables.fq); }
      TLorentzVector& GetP_A()const { CheckUpdate(); return(fVariables.fP_A); }

      Double_t Geta_xi()      const { CheckUpdate(); return fVariables.fa_xi; }
      Double_t Getb_xi()      const { CheckUpdate(); return fVariables.fb_xi; }
      Double_t Getc_xi()      const { CheckUpdate(); return fVariables.fc_xi; }

      Double_t Geta_eta()     const { CheckUpdate(); return fVariables.fa_eta; }
      Double_t Getb_eta()     const { CheckUpdate(); return fVariables.fb_eta; }
      Double_t Getc_eta()     const { CheckUpdate(); return fVariables.fc_eta; }

      Double_t Geta_xi_A()      const { CheckUpdate(); return fVariables.fa_xi_A; }
      Double_t Getb_xi_A()      const { CheckUpdate(); return fVariables.fb_xi_A; }
      Double_t Getc_xi_A()      const { CheckUpdate(); return fVariables.fc_xi_A; }

      Double_t Geta_eta_A()     const { CheckUpdate(); return fVariables.fa_eta_A; }
      Double_t Getb_eta_A()     const { CheckUpdate(); return fVariables.fb_eta_A; }
      Double_t Getc_eta_A()     const { CheckUpdate(); return fVariables.fc_eta_A; }

      Double_t GetEs()          const { CheckUpdate(); return fVariables.fEs; }  
      Double_t GetEp()          const { CheckUpdate(); return fVariables.fEp; }  
      Double_t GetTheta()       const { CheckUpdate(); return fVariables.fTheta; }  

      ClassDef(POLRADKinematics,1)
};
}}

#endif
