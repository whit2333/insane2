#ifndef MAIDNucleusInclusiveDiffXSec_H 
#define MAIDNucleusInclusiveDiffXSec_H 1

#include <cstdlib> 
#include <iostream> 
#include <cmath> 
#include <vector> 
#include "TString.h"
#include "TVector3.h"
#include "TRotation.h"
#include "MAIDKinematicKey.h"
#include "InclusiveDiffXSec.h"
#include "MAIDInclusiveDiffXSec.h"

namespace insane {
namespace physics {
/** MAID cross section.
 * 
 * Paper reference: J. Phys. G: Nucl. Part. Phys 18, 449 (1992)  
 * For the reaction e N --> e' N' pi, where the user specifies pi and N'. 
 * Uses data from the MAID 2007 calculations.
 * 
 * Refs: 
 *    http://wwwkph.kph.uni-mainz.de/MAID//maid2007/maid2007.html
 *    http://arxiv.org/pdf/nucl-th/9909068.pdf
 *
 *
 * \ingroup inclusiveXSec
 */ 
class MAIDNucleusInclusiveDiffXSec: public InclusiveDiffXSec{

   private:
      bool fDebug,fIsModified; 
      Double_t fh;                                                  ///< Electron helicity 
      TVector3 fP_t;                                                ///< Target polarization (lab frame) 
      mutable MAIDInclusiveDiffXSec *fPi0Neutron,*fPiMinusProton;   ///< neutron target
      mutable MAIDInclusiveDiffXSec *fPi0Proton,*fPiPlusNeutron;    ///< proton target 
      Nucleus fNucleus;

      Double_t ScaleFactor(Double_t *) const;  
 
   public: 
      MAIDNucleusInclusiveDiffXSec(); 
      virtual ~MAIDNucleusInclusiveDiffXSec();
      virtual MAIDNucleusInclusiveDiffXSec*  Clone(const char * newname) const {
         std::cout << "MAIDNucleusInclusiveDiffXSec::Clone()\n";
         auto * copy = new MAIDNucleusInclusiveDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual MAIDNucleusInclusiveDiffXSec*  Clone() const { return( Clone("") ); } 

      Double_t   EvaluateXSec(const Double_t *) const; 
      Double_t   GetHelicity() const { return fh; }
      TVector3   GetTargetPolarization(){ return fP_t; }

      void UseDebugMode(bool ans=true){ fDebug = ans; } 
      void UseModifiedModel(bool ans=true){ fIsModified = ans; }  

      void SetBeamEnergy(Double_t E){
         fBeamEnergy = E; 
         fPi0Neutron->SetBeamEnergy(E); 
         fPiMinusProton->SetBeamEnergy(E); 
         fPi0Proton->SetBeamEnergy(E); 
         fPiPlusNeutron->SetBeamEnergy(E); 
      }

      // FIXME: Not sure if we need this, since the end result is always sig_0, delta_sig_para or delta_sig_perp... 
      //        Or better yet, make a clever option so that you can just get one helicty as an output if you wanted. 
      void SetHelicity(Double_t h) { 
         if(TMath::Abs(h) > 1.0){
            Error("SetHelicity","Argument too big: |h|<=1.");
         }else{
            fh = h;
            fPi0Neutron->SetHelicity(h); 
            fPiMinusProton->SetHelicity(h); 
            fPi0Proton->SetHelicity(h); 
            fPiPlusNeutron->SetHelicity(h);
         } 
      }

      void SetTargetPolarization(const TVector3 &P){
         fP_t = P; 
         fPi0Neutron->SetTargetPolarization(P); 
         fPiMinusProton->SetTargetPolarization(P); 
         fPi0Proton->SetTargetPolarization(P); 
         fPiPlusNeutron->SetTargetPolarization(P); 
      }

      ClassDef(MAIDNucleusInclusiveDiffXSec,1)
};
}}
#endif 
