#ifndef insane_xsections_Lee4HeJpsiPhotoproduction_HH
#define insane_xsections_Lee4HeJpsiPhotoproduction_HH 1

#include "TGraph2D.h"

namespace insane {
  namespace physics {

    struct Lee4HeJpsiPhotoproduction {
      std::unique_ptr<TGraph2D> grid_;
      Lee4HeJpsiPhotoproduction();
      double operator()(const double W2, const double t, const double Mt) const;
    };

    //template<typename Sigma, typename Stack>
    //constexpr auto add_Lee4HeJpsiPhotoproduction(Stack& s) {
    //  s.add<Sigma>([](const auto& v){
    //    static Lee4HeJpsiPhotoproduction xs; 
    //  });
    //}


  }
} // namespace insane



#endif
