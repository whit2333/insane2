#ifndef Helium4ElasticDiffXSec_H
#define Helium4ElasticDiffXSec_H 1

#include "ExclusiveDiffXSec.h"
#include "InclusiveDiffXSec.h"
#include "TMath.h"
#include "FormFactors.h"
#include "TVector3.h"
#include "Helium4ElasticDiffXSec.h"


namespace insane {
   namespace physics {
/** Elastic scattering using the Dipole FFs.
 * \ingroup exclusiveXSec
 */
class Helium4ElasticDiffXSec : public ExclusiveDiffXSec {

   public:
      Helium4ElasticDiffXSec();
      virtual ~Helium4ElasticDiffXSec();
            Helium4ElasticDiffXSec(const Helium4ElasticDiffXSec& rhs)            = default;
            Helium4ElasticDiffXSec& operator=(const Helium4ElasticDiffXSec& rhs) = default;
            Helium4ElasticDiffXSec(Helium4ElasticDiffXSec&&)                     = default;
            Helium4ElasticDiffXSec& operator=(Helium4ElasticDiffXSec&&)          = default;

      virtual Helium4ElasticDiffXSec*  Clone(const char * newname) const {
         std::cout << "Helium4ElasticDiffXSec::Clone()\n";
         auto * copy = new Helium4ElasticDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual Helium4ElasticDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual void InitializePhaseSpaceVariables();

      Double_t GetEPrime(const Double_t theta) const ;
      Double_t GetTheta(double ep) const ;
      double GetTheta_e(double Es, double alpha) const;
      double GetPalpha(double Es, double alpha) const;
      double ElectronToAlphaJacobian(double Es, double alpha) const ;
      double Ib(Double_t E0,Double_t E,Double_t t) const ;
      double GetTheta_alpha(double Es, double P_alpha) const ;

      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      virtual Double_t   EvaluateXSec(const Double_t * x) const;
      virtual void       DefineEvent(Double_t * vars);

      ClassDef(Helium4ElasticDiffXSec, 1)
};
}
}


#endif

