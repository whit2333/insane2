#ifndef QFSInclusiveDiffXSec_HH
#define QFSInclusiveDiffXSec_HH 1

#include "InclusiveDiffXSec.h"
#include "insane/base/FortranWrappers.h"
#include "TObject.h"
#include <iostream>
#include <fstream>
#include "Nucleus.h"
#include "TString.h"
namespace insane {
namespace physics {

/** Configuration of QFS fortran code
 *  This replaces the input.dat file needed
 *
 * \ingroup QFS
 */
class QFSXSecConfiguration : public TObject {

   private:
      TString  fTAG;
      TString  fTarget;
      Double_t fTH;

      TString fInputFileDocumentation;

   public:

      Double_t fPF;
      Double_t fEPS;
      Double_t fEPSD;

      QFSXSecConfiguration();
      virtual ~QFSXSecConfiguration();

      /** Writes current configuration to file, qfs_input.dat.
       *
       *  The file should have the following structure...
       *
       *  '4_SY' 'He3' 15.5 130.0  10.0 15.0
       * - 1)TAG         ! Select cell and energy from E94010.
       * - 2)Target      ! 'He3' : z= 2,A= 3
       *                 ! 'Nit' : z= 7,A=14
       * - 3)TH          ! Scattering angle in degrees
       * - 4)PF          ! FERMI MOMENTUM [MEV/C]           (Affects width of QE)
       * - 5)EPS         ! NUCLEON SEPARATION ENERGY [MEV]. (Shifts QE central value)
       * - 6)EPSD        ! DELTA SEPARATION ENERGY [MEV].   (Shifts Delta central value)
       */
      Int_t WriteQFSInputFile() ;

      void Print(const Option_t * opt = ""); 

      void SetTag(TString tag){fTAG = tag;} 
      void SetTarget(TString t){fTarget = t;} 
      void SetTheta(Double_t th){fTH = th;} 
      void SetFermiMomentum(Double_t pf){fPF = pf;} 
      void SetNucleonSeparationEnergy(Double_t eps){fEPS = eps;} 
      void SetDeltaSeparationEnergy(Double_t epsd){fEPSD = epsd;} 

      double GetFermiMomentum(){ return (double)fPF;} 
      double GetNucleonSeparationEnergy(){ return (double)fEPS;} 
      double GetDeltaSeparationEnergy(){return (double)fEPSD;} 

      ClassDef(QFSXSecConfiguration, 1)
};


/**  Implementation of differential cross section from QFS code
 *
 * \ingroup QFS
 * \ingroup EventGen
 * \ingroup xsections
 */
class QFSInclusiveDiffXSec : public InclusiveDiffXSec {

   public:
      QFSXSecConfiguration * fConfiguration;
      Bool_t fIsConfigured;

   private: 
      Int_t fXSTYPE; ///< 0 = full xs, 1 = QE, 2 = Delta, 3 = DIS, 4 = 1500 MeV resonance, 5 = 1700 MeV resonance, 6 = Dip 

   public :
      QFSInclusiveDiffXSec() ;
      virtual ~QFSInclusiveDiffXSec() ;
      virtual QFSInclusiveDiffXSec*  Clone(const char * newname) const {
         std::cout << "QFSInclusiveDiffXSec::Clone()\n";
         auto * copy = new QFSInclusiveDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual QFSInclusiveDiffXSec*  Clone() const { return( Clone("") ); } 

      /** Should be called once before calling EvaluateXSec().
       *  Tells configuration object to write input file for legacy QFS code.
       */
      void Configure() ;

      void SetFermiMomentum(Double_t pf)            { fConfiguration->SetFermiMomentum(pf);}
      void SetNucleonSeparationEnergy(Double_t eps) { fConfiguration->SetNucleonSeparationEnergy(eps);}
      void SetDeltaSeparationEnergy(Double_t eps)   { fConfiguration->SetDeltaSeparationEnergy(eps);}
      void PrintConfiguration()                     { fConfiguration->Print(); }
      void SetXSType(Int_t i)                       { fXSTYPE = i; }

      virtual Double_t EvaluateXSec(const Double_t * x) const ;

      ClassDef(QFSInclusiveDiffXSec, 1)
};

}}

#endif
