#ifndef MAIDInclusivePionDiffXSec_HH
#define MAIDInclusivePionDiffXSec_HH 1

#include "InclusiveDiffXSec.h"
#include "MAIDExclusivePionDiffXSec3.h"
#include "TMath.h"
#include "Math/GSLIntegrator.h"
#include "Math/GaussIntegrator.h"
#include "Math/GaussLegendreIntegrator.h"
#include "Math/VirtualIntegrator.h"
#include "Math/Integrator.h"
#include "Math/IntegratorOptions.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/GaussIntegrator.h"
#include <Math/IFunction.h>
#include <Math/Functor.h>
#include "CompositeDiffXSec.h"
namespace insane {
namespace physics {


/** Inclusive pion cross section from MAID 5-fold differential cross sections.
 *  The cross section has the form 
 *  \f$ \frac{\sigma}{dE_{\pi}d\Omega_{\pi}} \f$. 
 *
 * \ingroup inclusiveXSec
 */
class MAIDInclusivePionDiffXSec : public InclusiveDiffXSec {

   public:
      class MAIDXSec_5Fold_2D_integrand : public ROOT::Math::IBaseFunctionMultiDim {
         public:
            mutable MAIDExclusivePionDiffXSec3 * fXSec;
            mutable Double_t fE_pi;
            mutable Double_t fArgs[9]; 
            double DoEval(const double* x) const ;
            unsigned int NDim() const { return 2; }
            ROOT::Math::IBaseFunctionMultiDim* Clone() const { return new MAIDXSec_5Fold_2D_integrand(); } 
            ClassDef(MAIDXSec_5Fold_2D_integrand,1)
      }; 

   private:

       ROOT::Math::IntegrationMultiDim::Type fIntType;
       double fAbsErr        ;
       double fRelErr        ;
       unsigned int fNcalls  ;
       int fRule             ;

       Bool_t        fReturnZero;

   public:

      MAIDXSec_5Fold_2D_integrand     fSig0;
      MAIDExclusivePionDiffXSec3    * fXSec0;

   public:

      MAIDInclusivePionDiffXSec(const char * nucleon = "p", const char * pion = "pi0");
      virtual ~MAIDInclusivePionDiffXSec();
      virtual MAIDInclusivePionDiffXSec*  Clone(const char * newname) const {
         std::cout << "MAIDInclusivePionDiffXSec::Clone()\n";
         auto * copy = new MAIDInclusivePionDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual MAIDInclusivePionDiffXSec*  Clone() const { return( Clone("") ); } 

      Double_t   EvaluateXSec(const Double_t *x) const ;
      Double_t * GetDependentVariables(const Double_t * x) const ;
      void       InitializePhaseSpaceVariables();

      virtual void      SetBeamEnergy(Double_t  ebeam)  {
         DiffXSec::SetBeamEnergy(ebeam);
         fXSec0->SetBeamEnergy(ebeam);
      }

      //class MAIDXSec_5Fold_theta : public ROOT::Math::IBaseFunctionOneDim {
      //   public:
      //      mutable MAIDExclusivePionDiffXSec * fXSec;
      //      double DoEval(double x) const { return x*x; }
      //      unsigned int NDim() const { return 1; }
      //      ROOT::Math::IBaseFunctionOneDim* Clone() const { return new MAIDXSec_5Fold_theta(); }
      //      ClassDef(MAIDXSec_5Fold_theta,1)
      //};
      //class MAIDXSec_5Fold_phi : public ROOT::Math::IBaseFunctionOneDim {
      //   public:
      //      mutable MAIDExclusivePionDiffXSec * fXSec;
      //      double DoEval(double x) const { return x*x; }
      //      unsigned int NDim() const { return 1; }
      //      ROOT::Math::IBaseFunctionOneDim* Clone() const { return new MAIDXSec_5Fold_phi(); }
      //      ClassDef(MAIDXSec_5Fold_phi,1)
      //};


ClassDef(MAIDInclusivePionDiffXSec,1)
};


/** Neutral pion cross section for arbitrary nuclear target. 
 *
 * \ingroup inclusiveXSec
 */
class MAIDInclusivePi0DiffXSec : public CompositeDiffXSec {

   public:
      MAIDInclusivePi0DiffXSec(){
         fProtonXSec  = new MAIDInclusivePionDiffXSec("p","pi0");
         fNeutronXSec = new MAIDInclusivePionDiffXSec("n","pi0");
         //std::cout << "done" << std::endl;
      }
      virtual ~MAIDInclusivePi0DiffXSec(){
         if(fProtonXSec)  delete fProtonXSec;
         if(fNeutronXSec) delete fNeutronXSec;
      }

      virtual MAIDInclusivePi0DiffXSec*  Clone(const char * newname) const {
         std::cout << "MAIDInclusivePi0DiffXSec::Clone()\n";
         auto * copy = new MAIDInclusivePi0DiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual MAIDInclusivePi0DiffXSec*  Clone() const { return( Clone("") ); } 
   ClassDef(MAIDInclusivePi0DiffXSec,1)
};


}}
#endif

