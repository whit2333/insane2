#ifndef POLRADUltraRelInelasticTailDiffXSec_HH
#define POLRADUltraRelInelasticTailDiffXSec_HH 1

#include "POLRADBornDiffXSec.h"
namespace insane {
namespace physics {

/** IRT in the Ultra relativistic approximation.
 *
 * \ingroup inclusiveXSec
 */
class POLRADUltraRelInelasticTailDiffXSec: public POLRADBornDiffXSec {
   public:
      POLRADUltraRelInelasticTailDiffXSec();
      virtual ~POLRADUltraRelInelasticTailDiffXSec();
      virtual POLRADUltraRelInelasticTailDiffXSec*  Clone(const char * 
            newname) const ;
      virtual POLRADUltraRelInelasticTailDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual Double_t EvaluateXSec(const Double_t *x) const ; 

      ClassDef(POLRADUltraRelInelasticTailDiffXSec,1)
};

}}
#endif

