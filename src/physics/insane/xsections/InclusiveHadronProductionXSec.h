#ifndef InclusiveHadronProductionXSec_HH
#define InclusiveHadronProductionXSec_HH 1

#include "CompositeDiffXSec.h"
//#include "PhotonDiffXSec.h"
//#include "InclusiveHadronProductionXSec.h"

namespace insane {
namespace physics {

/** Wiser cross section for arbitrary nuclear target. 
 *
 * \ingroup inclusiveXSec
 */
template< class T >
class InclusiveHadronProductionXSec : public CompositeDiffXSec {

   protected:
      Double_t               fAlphaCT; // color transparency

   public:
      InclusiveHadronProductionXSec();
      InclusiveHadronProductionXSec(const InclusiveHadronProductionXSec& rhs) ;
      virtual ~InclusiveHadronProductionXSec();
      InclusiveHadronProductionXSec& operator=(const InclusiveHadronProductionXSec& rhs) ;
      virtual InclusiveHadronProductionXSec*  Clone(const char * newname) const ;
      virtual InclusiveHadronProductionXSec*  Clone() const ; 

      void  SetParameters(int i, const std::vector<double>& pars) {
         ((T*)fProtonXSec)->SetParameters(i,pars);
         ((T*)fNeutronXSec)->SetParameters(i,pars);
      }
      const std::vector<double>& GetParameters() const  {
         return( ((T*)fProtonXSec)->GetParameters() );
      }

      void SetAlphaCT(Double_t a) { fAlphaCT = a; } 
      Double_t GetAlphaCT() const { return fAlphaCT; }

      virtual void SetProductionParticleType(Int_t PDGcode, Int_t part = 0) {
         SetParticleType(PDGcode,part);
         fProtonXSec->SetProductionParticleType(PDGcode,part);
         fNeutronXSec->SetProductionParticleType(PDGcode,part);
      }

      virtual Double_t  EvaluateXSec(const Double_t * x) const ;

   ClassDef(InclusiveHadronProductionXSec,1)
};

}
}
#include "InclusiveHadronProductionXSec.hxx"

#endif

