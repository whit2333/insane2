#ifndef InclusiveElectroProductionXSec_HH
#define InclusiveElectroProductionXSec_HH 1

#include "CompositeDiffXSec.h"
#include "PhotonDiffXSec.h"
#include "InclusiveElectroProductionXSec.h"
namespace insane {
namespace physics {

/**  Inclusive electro production of pions, kaons, and nucleons.
 *   Uses the fit from wiser and virtual photon spectrum from Traitor-Wright Nuc.Phys. A379.
 *   The recoil factor (eq13) is also included.
 *
 * \ingroup inclusiveXSec
 */
class InclusiveElectroProductionXSec : public PhotonDiffXSec {

   protected:
      PhotonDiffXSec * fSig_0;       //->
      PhotonDiffXSec * fSig_plus;    //->
      PhotonDiffXSec * fSig_minus;   //->

   public:
      InclusiveElectroProductionXSec();
      InclusiveElectroProductionXSec(const InclusiveElectroProductionXSec& rhs) ;
      virtual ~InclusiveElectroProductionXSec();
      InclusiveElectroProductionXSec& operator=(const InclusiveElectroProductionXSec& rhs) ;
      virtual InclusiveElectroProductionXSec*  Clone(const char * newname) const ;
      virtual InclusiveElectroProductionXSec*  Clone() const ; 

      void  SetParameters(int i, const std::vector<double>& pars);
      const std::vector<double>& GetParameters() const ;

      virtual void InitializePhaseSpaceVariables();
      Double_t EvaluateXSec(const Double_t * x) const ;

      // Here we copied a bunch of setters from DiffXSec because we need to set not only this class
      // but the proton and neutron cross sections. The getters are not changed because they should be identical
      // to this class (and really not used).
      void       SetTargetMaterial(const TargetMaterial& mat){
         fSig_0->SetTargetMaterial(mat);
         fSig_plus->SetTargetMaterial(mat);
         fSig_minus->SetTargetMaterial(mat);
         fTargetMaterial = mat;
      }
      void SetTargetNucleus(const Nucleus & targ){
         InclusiveDiffXSec::SetTargetNucleus(targ);
         //if(fProtonXSec)   fProtonXSec->SetTargetNucleus(targ);
         //if(fNeutronXSec) fNeutronXSec->SetTargetNucleus(targ);
      }

      void       SetTargetMaterialIndex(Int_t i){
         fSig_0->SetTargetMaterialIndex(i);
         fSig_plus->SetTargetMaterialIndex(i);
         fSig_minus->SetTargetMaterialIndex(i);
         fMaterialIndex = i ;
      }
      virtual void  SetBeamEnergy(Double_t en){
         InclusiveDiffXSec::SetBeamEnergy(en);
         fSig_0->SetBeamEnergy(en);
         fSig_plus->SetBeamEnergy(en);
         fSig_minus->SetBeamEnergy(en);
      }

      virtual void  SetPhaseSpace(PhaseSpace * ps);
      virtual void  InitializeFinalStateParticles();

   ClassDef(InclusiveElectroProductionXSec,1)
};



///** Wiser cross section for arbitrary nuclear target. 
// *
// * \ingroup inclusiveXSec
// */
//class InclusiveElectroProductionXSec : public CompositeDiffXSec {
//
//   protected:
//      Double_t               fAlphaCT; // color transparency
//
//   public:
//      InclusiveElectroProductionXSec();
//      InclusiveElectroProductionXSec(const InclusiveElectroProductionXSec& rhs) ;
//      virtual ~InclusiveElectroProductionXSec();
//      InclusiveElectroProductionXSec& operator=(const InclusiveElectroProductionXSec& rhs) ;
//      virtual InclusiveElectroProductionXSec*  Clone(const char * newname) const ;
//      virtual InclusiveElectroProductionXSec*  Clone() const ; 
//
//      void  SetParameters(int i, const std::vector<double>& pars) {
//         ((InclusiveElectroProductionXSec*)fProtonXSec)->SetParameters(i,pars);
//         ((InclusiveElectroProductionXSec*)fNeutronXSec)->SetParameters(i,pars);
//      }
//      const std::vector<double>& GetParameters() const  {
//         return( ((InclusiveElectroProductionXSec*)fProtonXSec)->GetParameters() );
//      }
//
//      void SetAlphaCT(Double_t a) { fAlphaCT = a; } 
//      Double_t GetAlphaCT() const { return fAlphaCT; }
//
//      //virtual void InitializePhaseSpaceVariables();
//      //TParticlePDG * GetParticlePDG() { return ((InclusiveElectroProductionXSec*)fProtonXSec)->GetParticlePDG(); } 
//      //Int_t          GetParticleType() { return ((InclusiveElectroProductionXSec*)fProtonXSec)->GetParticlePDG()->PdgCode(); }
//
//      virtual void SetProductionParticleType(Int_t PDGcode, Int_t part = 0) {
//         SetParticleType(PDGcode,part);
//         fProtonXSec->SetProductionParticleType(PDGcode,part);
//         fNeutronXSec->SetProductionParticleType(PDGcode,part);
//         switch(PDGcode) {
//            case  111 : fID = fBaseID + 0;   break;
//            case -211 : fID = fBaseID + 100; break;
//            case  211 : fID = fBaseID + 200; break;
//            default   : fID = fBaseID + 300; break;
//         }
//      }
//
//      //void SetParticlePDGEncoding(Int_t PDGcode) { SetProductionParticleType(PDGcode); }
//
//      virtual Double_t  EvaluateXSec(const Double_t * x) const ;
//
//   ClassDef(InclusiveElectroProductionXSec,1)
//};

}}
#endif

