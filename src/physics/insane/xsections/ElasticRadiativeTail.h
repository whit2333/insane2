#ifndef ElasticRadiativeTail_HH 
#define ElasticRadiativeTail_HH 1

#include "RadiativeTail.h"
#include "POLRADInternalPolarizedDiffXSec.h"

namespace insane {
namespace physics {
/** Class for pulling together all the pieces and calculate the full elastic radiative
 *  tail.
 *  
 */
class ElasticRadiativeTail : public RadiativeTail {

   protected:
      POLRADElasticDiffXSec * fElasticDiffXSec; //-> born level cross section 

   public:
      ElasticRadiativeTail();
      virtual ~ElasticRadiativeTail();

      POLRADElasticDiffXSec * GetBornXSec() { return fElasticDiffXSec; }

      void SetPolarizations(Double_t pe, Double_t pt){
         GetPOLRAD()->SetPolarizations(pe,pt,0.0);
         GetBornXSec()->GetPOLRAD()->SetPolarizations(pe,pt,0.0);
      } 


      virtual Double_t EvaluateXSec(const Double_t *x) const ;

      ClassDef(ElasticRadiativeTail,1)
};
}
}
#endif

