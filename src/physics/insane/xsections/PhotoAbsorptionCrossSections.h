#ifndef PhotoAbsorptionCrossSections_HH
#define PhotoAbsorptionCrossSections_HH 1

#include "TNamed.h"
#include <vector>
namespace insane {
namespace physics {

class PhotoAbsorptionCrossSections : public TNamed  {

   public:

      Double_t               fSig_T   ;
      Double_t               fSig_L   ;
      Double_t               fSig_LT  ;
      Double_t               fSig_LTp ;
      Double_t               fSig_TT  ;
      Double_t               fSig_TTp ;
      Double_t               fSig_L0  ;
      Double_t               fSig_LT0 ;
      Double_t               fSig_LT0p;
      std::vector<Double_t>  fSigs    ;

   public:

      PhotoAbsorptionCrossSections(
            const char * n = "PhotoAbsorptionCrossSections",
            const char * t = "Total photo absorption cross sections");
      virtual ~PhotoAbsorptionCrossSections();

      virtual void CalculateProton( Double_t x, Double_t Q2) = 0;
      virtual void CalculateNeutron(Double_t x, Double_t Q2) = 0;

      Double_t Sig_T() const { return fSig_T; }
      Double_t Sig_L() const { return fSig_L; }
      Double_t Sig_LT() const { return fSig_LT; }
      Double_t Sig_TT() const { return fSig_TT; }
      Double_t Sig_LTp() const { return fSig_LTp; }
      Double_t Sig_TTp() const { return fSig_TTp; }

      ClassDef(PhotoAbsorptionCrossSections,1)
};
}}

#endif
