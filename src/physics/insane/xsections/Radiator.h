#ifndef Radiator_H
#define Radiator_H 1

#include "RADCOR2.h"
#include "POLRAD.h"
#include "MollerDiffXSec.h"
#include "F1F209eInclusiveDiffXSec.h"
#include "InclusiveBornDISXSec.h"
#include "POLRADInternalPolarizedDiffXSec.h"
namespace insane {
namespace physics {

/** Radiates inclusive electron cross sections.
 *
 * @ingroup inclusiveXSec 
 */
template <class T>
class Radiator : public T {

   protected:
      mutable POLRAD  * fPOLRAD;       //-> Used to get the (internal) polarized 
      mutable RADCOR2 * fRADCOR;       //-> Used to get the (internal and external)
      Double_t                fRadLen[2];    //   [0] before and [1] after scattering

      Bool_t   fAddRegion4;          ///< adds 2d integral over region IV (see MoTsai 1969)
      Bool_t   fInternalOnly;        ///<
      Bool_t   fExternalOnly;        ///<
      Bool_t   fFullExternal;        ///< Do the full 3D integral of MoTsai(1969) A.12
      Bool_t   fUsePOLRAD;

      void CreatePOLRAD() const ;
      void CreateRADCOR() const ;


   public:
      Radiator();
      virtual ~Radiator();

      void SetRadiationLength(Double_t r) {
         fRadLen[0] = r/2.0;
         fRadLen[1] = r/2.0;
         GetRADCOR()->SetRadiationLengths(fRadLen);
      }
      void SetRadiationLength(Double_t r1,Double_t r2) {
         fRadLen[0] = r1;
         fRadLen[1] = r2;
         GetRADCOR()->SetRadiationLengths(fRadLen);
      }

      Double_t GetRadiationLength(Int_t i = 0) const {
         if(i==0) return (fRadLen[0]+fRadLen[1]);
         if(i==1) return (fRadLen[0]);
         if(i==2) return (fRadLen[1]);
         return 0.0;
      }

      POLRAD  * GetPOLRAD() const { if(!fPOLRAD) CreatePOLRAD(); return fPOLRAD; }
      RADCOR2 * GetRADCOR() const { if(!fRADCOR) CreateRADCOR(); return fRADCOR; }

      void SetFullExternal(bool v = true) { fFullExternal = v; }
      void SetRegion4(bool v = true)      { fAddRegion4 = v; }
      void SetInternalOnly(bool v = true) { fInternalOnly = v; if(v) {fExternalOnly=false;}  }
      void SetExternalOnly(bool v = true) { fExternalOnly = v; if(v) {fInternalOnly=false;} }

      virtual void SetTargetPolarization(const TVector3 &P){
         // make sure to add call to this in override
         GetPOLRAD()->SetPolarizationVectors(this->GetTargetPolarization(),this->GetHelicity());
         //if( POLRADBornDiffXSec * polradXSec = dynamic_cast<POLRADBornDiffXSec*>(this) ) {
         //   POLRADBornDiffXSec::SetTargetPolarization(P);
         //}  else {
            DiffXSec::SetTargetPolarization(P);
         //}
      }
      virtual void       SetHelicity(Double_t h) { 
         // make sure to add call to this in override
         GetPOLRAD()->SetHelicity(h);
         DiffXSec::SetHelicity(h);
      }
      virtual void SetPolarizations(Double_t pe, Double_t pt){
         // make sure to add call to this in override
         GetPOLRAD()->SetPolarizations(pe,pt,0.0);
         TVector3 p = this->GetTargetPolarization();
         p.SetMag(pt);
         SetTargetPolarization(p);
         SetHelicity(pe);
      } 

      void SetIntegrationThreshold(Int_t thr){
         GetRADCOR()->SetIntegrationThreshold(thr); 
      }
      void SetDeltaM(Double_t m){
         GetRADCOR()->SetDeltaM(m); 
      }

      virtual Double_t EvaluateXSec(const Double_t *x) const ;

      virtual Double_t EvaluateBaseXSec(const Double_t *x) const {
         return T::EvaluateXSec(x);
      }

      virtual Int_t InitFromDisk();

      ClassDef(Radiator,1)
};
//______________________________________________________________________________

#include "Radiator.hxx"

}
}
#endif

