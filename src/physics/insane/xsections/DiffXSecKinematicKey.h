#ifndef DiffXSecKinematicKey_HH
#define DiffXSecKinematicKey_HH 1

#include "TObject.h"
#include <iostream>
#include "TOrdCollection.h"


namespace insane {
namespace physics {
class DiffXSecKinematicKey : public TObject {
   public:
      Int_t    fNum;
      Double_t fEbeam;
      Double_t fEprime;
      Double_t fTheta;
      Double_t fPhi;
      Double_t fSigma;

      DiffXSecKinematicKey(Int_t i = 0) {
         fNum    = i;
         fEbeam  = 0.0;
         fEprime = 0.0;
         fTheta  = 0.0;
         fPhi    = 0.0;
      }
      virtual ~DiffXSecKinematicKey() {
      }
      void Print() { 
         std::cout << fNum << "   " 
                   << fEbeam << "   "
                   << fEprime << "   "
                   << fTheta << "   "
                   << fPhi << std::endl;
      }
      //ULong_t Hash() const { return fNum; }
      Bool_t IsEqual(const TObject * obj) const { 
         auto * aKey = (DiffXSecKinematicKey*)obj;
         if(aKey->fEbeam  != fEbeam)  return false;
         if(aKey->fEprime != fEprime) return false;
         if(aKey->fTheta  != fTheta)  return false;
         if(aKey->fPhi    != fPhi)    return false;
         return(true);
      }

      Bool_t  IsSortable() const { return kTRUE; }

      Int_t   Compare0(const TObject *obj) const {
         auto * aKey = (DiffXSecKinematicKey*)obj;
         // Order first by beam energy
         if(aKey->fEbeam  == fEbeam)  return 0;
         if(aKey->fEbeam  > fEbeam)  return -1;
         /*if(aKey->fEbeam  < fEbeam)*/  return  1;
         Error("Compare","Should not have made it here!");
         return -1;
      }
      Int_t   Compare1(const TObject *obj) const {
         auto * aKey = (DiffXSecKinematicKey*)obj;
         // Order first by beam energy
         if(aKey->fEprime  == fEprime)  return 0;
         if(aKey->fEprime   > fEprime)  return -1;
         /*if(aKey->fEprime  < fEprime)*/  return  1;
         Error("Compare","Should not have made it here!");
         return -1;
      }
      Int_t   Compare2(const TObject *obj) const {
         auto * aKey = (DiffXSecKinematicKey*)obj;
         // Order first by beam energy
         if(aKey->fTheta  == fTheta)  return 0;
         if(aKey->fTheta   > fTheta)  return -1;
         /*if(aKey->fEprime  < fEprime)*/  return  1;
         Error("Compare","Should not have made it here!");
         return -1;
      }
      Int_t   Compare3(const TObject *obj) const {
         auto * aKey = (DiffXSecKinematicKey*)obj;
         // Order first by beam energy
         if(aKey->fPhi  == fPhi)  return 0;
         if(aKey->fPhi   > fPhi)  return -1;
         /*if(aKey->fEprime  < fEprime)*/  return  1;
         Error("Compare","Should not have made it here!");
         return -1;
      }

      Int_t   Compare(const TObject *obj) const {
         if( IsEqual(obj) ) return (0);
         auto * aKey = (DiffXSecKinematicKey*)obj;
         // Order first by beam energy
         if(aKey->fEbeam  > fEbeam)  return -1;
         if(aKey->fEbeam  < fEbeam)  return  1;
         // next eprime
         if(aKey->fEprime > fEprime) return -1;
         if(aKey->fEprime < fEprime) return 1;
         // next theta
         if(aKey->fTheta  > fTheta)  return -1;
         if(aKey->fTheta  < fTheta)  return 1;
         // then finally phi
         if(aKey->fPhi    > fPhi)    return -1;
         if(aKey->fPhi    < fPhi)    return 1;

         Error("Compare","Should not have made it here!");
         return -1;
      }

   ClassDef(DiffXSecKinematicKey,1)
};
}
}

#endif

