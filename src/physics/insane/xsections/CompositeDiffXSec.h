#ifndef CompositeDiffXSec_HH
#define CompositeDiffXSec_HH 1

#include "InclusiveDiffXSec.h"
//#include "InelasticRadiativeTail2.h"
#include "insane/base/FortranWrappers.h"
#include "InSANEMathFunc.h"

namespace insane {
namespace physics {

/** Builds a cross section for a target nucleus from the supplied
 *  proton and neutron cross secitons.
 *  Ideally this should inherit from DiffXSec not InclusiveDiffXSec.
 *
 * \ingroup inclusiveXSec
 */
class CompositeDiffXSec : public InclusiveDiffXSec {

   protected:
      InclusiveDiffXSec * fProtonXSec;   //->
      InclusiveDiffXSec * fNeutronXSec;  //->

   public:

      CompositeDiffXSec();
      CompositeDiffXSec(const CompositeDiffXSec& rhs) ;
      virtual ~CompositeDiffXSec();
      CompositeDiffXSec& operator=(const CompositeDiffXSec& rhs) ;
      virtual CompositeDiffXSec*  Clone(const char * newname) const ;
      virtual CompositeDiffXSec*  Clone() const ; 

      virtual Double_t EvaluateXSec(const Double_t * x) const ;

      // Here we copied a bunch of setters from DiffXSec because we need to set not only this class
      // but the proton and neutron cross sections. The getters are not changed because they should be identical
      // to this class (and really not used).
      void SetTargetMaterial(const TargetMaterial& mat);
      void SetTargetNucleus(const Nucleus & targ);
      void SetTargetMaterialIndex(Int_t i);

      virtual void  SetUnits(const char * t);
      virtual void  SetBeamEnergy(Double_t en);
      virtual void  SetPhaseSpace(PhaseSpace * ps);
      virtual void  InitializePhaseSpaceVariables();
      virtual void  InitializeFinalStateParticles();
      virtual void  SetParticleType(Int_t pdgcode, Int_t part = 0) ;

      ClassDef(CompositeDiffXSec,1)
};

}
}


#endif

