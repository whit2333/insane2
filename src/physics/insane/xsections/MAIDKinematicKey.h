#ifndef MAIDKinematicKey_HH
#define MAIDKinematicKey_HH 1

#include "TObject.h"
#include <iostream>
#include "TOrdCollection.h"
namespace insane {
namespace physics {

class MAIDKinematicKey : public TObject {
   public:
      Int_t    fNum;
      Double_t fEbeam;
      Double_t fEprime;
      Double_t fTheta;
      Double_t fPhi;
      Double_t fW; 
      Double_t fQ2;  
      Double_t fSigma_L;
      Double_t fSigma_T;
      Double_t fSigma_LT;
      Double_t fSigma_LTpr;
      Double_t fSigma_TTpr;
      Double_t fSigma_L0;
      Double_t fSigma_LT0;
      Double_t fSigma_LT0pr;

      MAIDKinematicKey(Int_t i = 0) {
         fNum         = i;
         fEbeam       = 0.0;
         fEprime      = 0.0;
         fTheta       = 0.0;
         fPhi         = 0.0;
         fW           = 0.0; 
         fQ2          = 0.0; 
         fSigma_L     = 0.0; 
         fSigma_T     = 0.0; 
         fSigma_LT    = 0.0; 
         fSigma_LTpr  = 0.0; 
         fSigma_TTpr  = 0.0; 
         fSigma_L0    = 0.0; 
         fSigma_LT0   = 0.0; 
         fSigma_LT0pr = 0.0; 
      }

      virtual ~MAIDKinematicKey() {}

      void Print() { 
         std::cout << fNum    << "\t" 
                   << fEbeam  << "\t"
                   << fEprime << "\t"
                   << fTheta  << "\t"
                   << fPhi    << "\t" 
                   << fW      << "\t" 
                   << fQ2     << std::endl; 
      }
      //ULong_t Hash() const { return fNum; }
      Bool_t IsEqual(const TObject * obj) const { 
         auto * aKey = (MAIDKinematicKey*)obj;
         // if(aKey->fEbeam  != fEbeam)  return false;
         // if(aKey->fEprime != fEprime) return false;
         // if(aKey->fTheta  != fTheta)  return false;
         // if(aKey->fPhi    != fPhi)    return false;
         if(aKey->fW      != fW)      return false;
         if(aKey->fQ2     != fQ2)     return false;
         return(true);
      }

      Bool_t  IsSortable() const { return kTRUE; }

      // Int_t   Compare0(const TObject *obj) const {
      //    MAIDKinematicKey * aKey = (MAIDKinematicKey*)obj;
      //    // Order first by beam energy
      //    if(aKey->fEbeam  == fEbeam)  return 0;
      //    if(aKey->fEbeam  > fEbeam)  return -1;
      //    /*if(aKey->fEbeam  < fEbeam)*/  return  1;
      //    Error("Compare","Should not have made it here!");
      //    return -1;
      // }
      // Int_t   Compare1(const TObject *obj) const {
      //    MAIDKinematicKey * aKey = (MAIDKinematicKey*)obj;
      //    // Order first by beam energy
      //    if(aKey->fEprime  == fEprime)  return 0;
      //    if(aKey->fEprime   > fEprime)  return -1;
      //    /*if(aKey->fEprime  < fEprime)*/  return  1;
      //    Error("Compare","Should not have made it here!");
      //    return -1;
      // }
      // Int_t   Compare2(const TObject *obj) const {
      //    MAIDKinematicKey * aKey = (MAIDKinematicKey*)obj;
      //    // Order first by beam energy
      //    if(aKey->fTheta  == fTheta)  return 0;
      //    if(aKey->fTheta   > fTheta)  return -1;
      //    /*if(aKey->fEprime  < fEprime)*/  return  1;
      //    Error("Compare","Should not have made it here!");
      //    return -1;
      // }
      // Int_t   Compare3(const TObject *obj) const {
      //    MAIDKinematicKey * aKey = (MAIDKinematicKey*)obj;
      //    // Order first by beam energy
      //    if(aKey->fPhi  == fPhi)  return 0;
      //    if(aKey->fPhi   > fPhi)  return -1;
      //    /*if(aKey->fEprime  < fEprime)*/  return  1;
      //    Error("Compare","Should not have made it here!");
      //    return -1;
      // }

      Int_t   Compare(const TObject *obj) const {
         if( IsEqual(obj) ) return (0);
         auto * aKey = (MAIDKinematicKey*)obj;
         // if(aKey->fEbeam  > fEbeam)  return -1;
         // if(aKey->fEbeam  < fEbeam)  return  1;
         // // next eprime
         // if(aKey->fEprime > fEprime) return -1;
         // if(aKey->fEprime < fEprime) return 1;
         // // next theta
         // if(aKey->fTheta  > fTheta)  return -1;
         // if(aKey->fTheta  < fTheta)  return 1;
         // // next phi
         // if(aKey->fPhi    > fPhi)    return -1;
         // if(aKey->fPhi    < fPhi)    return 1;
         // Order first by Q2 
         if(aKey->fQ2 > fQ2) return -1;
         if(aKey->fQ2 < fQ2) return 1;
         // next W 
         if(aKey->fW  > fW)  return -1;
         if(aKey->fW  < fW)  return 1;
         Error("Compare","Should not have made it here!");
         return -1;
      }

   ClassDef(MAIDKinematicKey,1)
};
}}
#endif

