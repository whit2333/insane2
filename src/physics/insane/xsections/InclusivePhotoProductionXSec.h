#ifndef InclusivePhotoProductionXSec_HH
#define InclusivePhotoProductionXSec_HH

#include "PhotonDiffXSec.h"
namespace insane {
namespace physics {


/**  Inclusive photo production of pions, kaons, and nucleons from a nucleon target.
 *   For all targets use InclusivePhotoProductionXSec.
 * \ingroup inclusiveXSec
 */
class InclusivePhotoProductionXSec : public PhotonDiffXSec {

   protected:
      PhotonDiffXSec * fSig_0;       //->
      PhotonDiffXSec * fSig_plus;    //->
      PhotonDiffXSec * fSig_minus;   //->

   public:
      InclusivePhotoProductionXSec();
      InclusivePhotoProductionXSec(const InclusivePhotoProductionXSec& rhs) ;
      virtual ~InclusivePhotoProductionXSec();
      InclusivePhotoProductionXSec& operator=(const InclusivePhotoProductionXSec& rhs) ;
      virtual InclusivePhotoProductionXSec*  Clone(const char * newname) const ;
      virtual InclusivePhotoProductionXSec*  Clone() const ; 

      Double_t EvaluateXSec(const Double_t * x) const ;

      void  SetParameters(int i, const std::vector<double>& pars);
      const std::vector<double>& GetParameters() const ;

      virtual void   SetRadiationLength(Double_t r) { 
         ((PhotonDiffXSec*)fSig_0)->SetRadiationLength(r);
         ((PhotonDiffXSec*)fSig_plus)->SetRadiationLength(r);
         ((PhotonDiffXSec*)fSig_minus)->SetRadiationLength(r);
      }
      // Here we copied a bunch of setters from DiffXSec because we need to set not only this class
      // but the proton and neutron cross sections. The getters are not changed because they should be identical
      // to this class (and really not used).
      void       SetTargetMaterial(const TargetMaterial& mat){
         fSig_0->SetTargetMaterial(mat);
         fSig_plus->SetTargetMaterial(mat);
         fSig_minus->SetTargetMaterial(mat);
         fTargetMaterial = mat;
      }
      void SetTargetNucleus(const Nucleus & targ){
         InclusiveDiffXSec::SetTargetNucleus(targ);
         //if(fProtonXSec)   fProtonXSec->SetTargetNucleus(targ);
         //if(fNeutronXSec) fNeutronXSec->SetTargetNucleus(targ);
      }

      virtual void InitializePhaseSpaceVariables();

      void       SetTargetMaterialIndex(Int_t i){
         fSig_0->SetTargetMaterialIndex(i);
         fSig_plus->SetTargetMaterialIndex(i);
         fSig_minus->SetTargetMaterialIndex(i);
         fMaterialIndex = i ;
      }
      virtual void  SetBeamEnergy(Double_t en){
         InclusiveDiffXSec::SetBeamEnergy(en);
         fSig_0->SetBeamEnergy(en);
         fSig_plus->SetBeamEnergy(en);
         fSig_minus->SetBeamEnergy(en);
      }

      virtual void  SetPhaseSpace(PhaseSpace * ps);
      virtual void  InitializeFinalStateParticles();

   ClassDef(InclusivePhotoProductionXSec,1)
};



///** Wiser cross section for arbitrary nuclear target. 
// *
// * \ingroup inclusiveXSec
// */
//class InclusivePhotoProductionXSec : public CompositeDiffXSec {
//
//   public:
//      InclusivePhotoProductionXSec();
//      InclusivePhotoProductionXSec(const InclusivePhotoProductionXSec& rhs) ;
//      virtual ~InclusivePhotoProductionXSec();
//      InclusivePhotoProductionXSec& operator=(const InclusivePhotoProductionXSec& rhs) ;
//      virtual InclusivePhotoProductionXSec*  Clone(const char * newname) const ;
//      virtual InclusivePhotoProductionXSec*  Clone() const ; 
//
//      void  SetParameters(int i, const std::vector<double>& pars) {
//         ((InclusivePhotoProductionXSec*)fProtonXSec)->SetParameters(i,pars);
//         ((InclusivePhotoProductionXSec*)fNeutronXSec)->SetParameters(i,pars);
//      }
//      const std::vector<double>& GetParameters() const  {
//         return( ((InclusivePhotoProductionXSec*)fProtonXSec)->GetParameters() );
//      }
//
//      TParticlePDG * GetParticlePDG() { return ((InclusivePhotoProductionXSec*)fProtonXSec)->GetParticlePDG(); } 
//      Int_t          GetParticleType() { return ((InclusivePhotoProductionXSec*)fProtonXSec)->GetParticlePDG()->PdgCode(); }
//
//      Double_t GetRadiationLength(){ 
//         return( ((InclusivePhotoProductionXSec*)fProtonXSec)->GetRadiationLength());
//      }
//      void   SetRadiationLength(Double_t r) { 
//         ((InclusivePhotoProductionXSec*)fProtonXSec )->SetRadiationLength(r);
//         ((InclusivePhotoProductionXSec*)fNeutronXSec)->SetRadiationLength(r);
//      }
//
//      void SetProductionParticleType(Int_t PDGcode, Int_t part = 0) {
//         SetParticleType(PDGcode);
//         ((InclusivePhotoProductionXSec*)fProtonXSec)->SetProductionParticleType(PDGcode);
//         ((InclusivePhotoProductionXSec*)fNeutronXSec)->SetProductionParticleType(PDGcode);
//         switch(PDGcode) {
//            case  111 : fID = fBaseID + 0;   break;
//            case -211 : fID = fBaseID + 100; break;
//            case  211 : fID = fBaseID + 200; break;
//            default   : fID = fBaseID + 300; break;
//         }
//      }
//
//      void SetParticlePDGEncoding(Int_t PDGcode) { SetProductionParticleType(PDGcode); }
//
//      virtual Double_t  EvaluateXSec(const Double_t * x) const ;
//
//   ClassDef(InclusivePhotoProductionXSec,1)
//};

}
}

#endif

