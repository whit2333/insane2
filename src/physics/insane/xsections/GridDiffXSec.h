#ifndef GridDiffXSec_hh
#define GridDiffXSec_hh

#include "InclusiveDiffXSec.h"
#include "DiffXSecKinematicKey.h"
#include "TOrdCollection.h"
#include <vector>
#include <algorithm>
#include <iostream>

namespace insane {
namespace physics {
class GridXSecValue : public TObject {
   public:
      Double_t fSigma;
      Double_t GetSigma(){return fSigma;}
      void     SetSigma(Double_t s) {fSigma = s;}
      GridXSecValue(Double_t sig = 0.0){fSigma = sig;}
      ~GridXSecValue(){}
   ClassDef(GridXSecValue,1);
};

class GridDiffXSec : public InclusiveDiffXSec {
   protected:
      TString                     fFileName;
      mutable TOrdCollection              fGridData;
      mutable DiffXSecKinematicKey *fKineKey;

      mutable std::vector<Double_t>    fEprime;
      mutable std::vector<Double_t>    fTheta;
      mutable std::vector<Double_t>    fPhi;

   public:
      GridDiffXSec(const char * file = "xsec_test.txt");
      virtual ~GridDiffXSec();
      //virtual GridDiffXSec*  Clone(const char * newname) const {
      //   std::cout << "GridDiffXSec::Clone()\n";
      //   GridDiffXSec * copy = new GridDiffXSec();
      //   (*copy) = (*this);
      //   return copy;
      //}
      //virtual GridDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual Double_t EvaluateXSec(const Double_t *x) const; 

      void BinarySearch(std::vector<Double_t> *array,Double_t key,Int_t &lowerbound,Int_t &upperbound) const; 
      ClassDef(GridDiffXSec,1)
};

}
}
#endif

