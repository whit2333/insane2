#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;


#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;
#pragma link C++ namespace insane::physics::xs;
#pragma link C++ namespace insane::physics::eic;

#pragma link C++ class insane::physics::CrossSectionInfo+;

#pragma link C++ class insane::physics::CompositeDiffXSec+;
#pragma link C++ class insane::physics::CompositeDiffXSec2+;
#pragma link C++ class insane::physics::DVCSDiffXSec+;
#pragma link C++ class insane::physics::IncoherentDVCSXSec+;
#pragma link C++ class insane::physics::IncoherentDISXSec+;
#pragma link C++ class insane::physics::CoherentDVCSXSec+;
#pragma link C++ class insane::physics::CoherentDVMPXSec+;

#pragma link C++ class insane::physics::CompositeDiffXSec+;
//#pragma link C++ class insane::physics::DiffXSecKinematicKey+;
#pragma link C++ class insane::physics::GridDiffXSec+;
#pragma link C++ class insane::physics::GridXSecValue+;

#pragma link C++ class insane::physics::InclusiveDISXSec+;
#pragma link C++ class insane::physics::InclusiveBornDISXSec+;

#pragma link C++ class insane::physics::eic::EICIncoherentDISXSec+;
#pragma link C++ class insane::physics::eic::EICInclusiveBornDISXSec+;

//#pragma link C++ class insane::physics::CTEQ6eInclusiveDiffXSec+;
//#pragma link C++ class insane::physics::F1F209eInclusiveDiffXSec+;
//#pragma link C++ class insane::physics::F1F209QuasiElasticDiffXSec+;
//#pragma link C++ class insane::physics::PolIncDiffXSec+;
//// quasi elastic 
//#pragma link C++ class QuasiElasticInclusiveDiffXSec+; 
//#pragma link C++ class QEIntegral+; 
//#pragma link C++ class QEFuncWrap+; 

#pragma link C++ class insane::physics::QFSInclusiveDiffXSec+;
#pragma link C++ class insane::physics::QFSXSecConfiguration+;

#pragma link C++ class insane::physics::CrossSectionDifference+;

#pragma link C++ class insane::physics::PolarizedDiffXSec+;
#pragma link C++ class insane::physics::PolarizedDISXSec+;
#pragma link C++ class insane::physics::PolarizedDISXSecParallelHelicity+;
#pragma link C++ class insane::physics::PolarizedDISXSecAntiParallelHelicity+;


#pragma link C++ class insane::physics::QuasiElasticInclusiveDiffXSec+; 
#pragma link C++ class insane::physics::QEIntegral+; 
#pragma link C++ class insane::physics::QEFuncWrap+; 

#pragma link C++ class insane::physics::PhotonDiffXSec+;
#pragma link C++ class insane::physics::InclusiveElectroProductionXSec+;
//#pragma link C++ class InclusiveElectroProductionXSec+;
#pragma link C++ class insane::physics::InclusivePhotoProductionXSec+;
//#pragma link C++ class InclusivePhotoProductionXSec+;
#pragma link C++ class insane::physics::InclusivePionElectroProductionXSec+;
#pragma link C++ class insane::physics::InclusivePionPhotoProductionXSec+;
#pragma link C++ class InclusiveHadronProductionXSec<insane::physics::InclusivePionElectroProductionXSec>+;
#pragma link C++ class InclusiveHadronProductionXSec<insane::physics::InclusivePionPhotoProductionXSec>+;

#pragma link C++ class insane::physics::InclusiveWiserXSec+;

#pragma link C++ class insane::physics::WiserInclusivePhotoXSec+;
#pragma link C++ class insane::physics::WiserInclusivePhotoXSec2+;
#pragma link C++ class insane::physics::PhotoWiserDiffXSec+;
#pragma link C++ class insane::physics::PhotoWiserDiffXSec2+;

#pragma link C++ class insane::physics::WiserInclusiveElectroXSec+;
#pragma link C++ class insane::physics::ElectroWiserDiffXSec+;

#pragma link C++ class insane::physics::PhotonDiffXSec+;

#pragma link C++ class insane::physics::InclusivePhotoProductionXSec+;
//#pragma link C++ class insane::physics::InclusivePhotoProductionXSec+;

#pragma link C++ class insane::physics::InclusiveElectroProductionXSec+;
//#pragma link C++ class insane::physics::InclusiveElectroProductionXSec+;

#pragma link C++ class insane::physics::InclusiveEPCVXSec+;
#pragma link C++ class insane::physics::InclusiveEPCVXSec2+;

#pragma link C++ class insane::physics::VirtualPhotoAbsorptionCrossSections+;
#pragma link C++ class insane::physics::PhotoAbsorptionCrossSections+;
#pragma link C++ class insane::physics::MAIDPhotoAbsorptionCrossSections+;
#pragma link C++ class insane::physics::MAID_VPACs+;

#pragma link C++ class insane::physics::SFsFromVPACs<insane::physics::MAID_VPACs>+;
#pragma link C++ class insane::physics::SSFsFromVPACs<insane::physics::MAID_VPACs>+;


//#pragma link C++ class insane::physics::Helium4ElasticDiffXSec+;
//#pragma link C++ class insane::physics::Helium4ElasticRadiativeTailDiffXSec+;
//#pragma link C++ class insane::physics::ProtonElasticRadiativeTailDiffXSec+;
//#pragma link C++ class insane::physics::DeuteronElasticDiffXSec+;

//#pragma link C++ class insane::physics::ExternalRadiator<insane::physics::Helium4ElasticDiffXSec>+;
//#pragma link C++ class insane::physics::ExternalRadiator<insane::physics::epElasticDiffXSec>+;

//#pragma link C++ class insane::physics::ExternalRadiator<insane::physics::QuasiElasticInclusiveDiffXSec>+;
//#pragma link C++ class insane::physics::QuasiElasticRadiativeTailDiffXSec+;
//#pragma link C++ class insane::physics::ElectronQuasiElasticRadiativeTailDiffXSec+;
//#pragma link C++ class insane::physics::ElectronProtonElasticDiffXSec+;
//#pragma link C++ class insane::physics::ProtonElasticDiffXSec+;


//#pragma link C++ class insane::physics::MAIDInclusiveDiffXSec+; 
//#pragma link C++ class insane::physics::MAIDNucleusInclusiveDiffXSec+; 
//#pragma link C++ class insane::physics::MAIDKinematicKey+; 
//#pragma link C++ class insane::physics::MAIDPolarizedTargetDiffXSec+; 
//#pragma link C++ class insane::physics::MAIDPolarizedKinematicKey+; 

#pragma link C++ class insane::physics::VCSABase+; 
#pragma link C++ class insane::physics::VirtualComptonAsymmetries+; 
#pragma link C++ class insane::physics::VirtualComptonAsymmetriesModel1+; 
#pragma link C++ class insane::physics::MAIDVirtualComptonAsymmetries+; 

#pragma link C++ class insane::physics::MAIDExclusivePionDiffXSec+; 
#pragma link C++ class insane::physics::MAIDInclusiveElectronDiffXSec+; 
#pragma link C++ class insane::physics::MAIDInclusiveElectronDiffXSec::MAIDXSec_5Fold_2D_integrand+; 
#pragma link C++ class insane::physics::MAIDInclusiveElectronDiffXSec::MAIDXSec_5Fold_theta+; 
#pragma link C++ class insane::physics::MAIDInclusiveElectronDiffXSec::MAIDXSec_5Fold_phi+; 

#pragma link C++ class insane::physics::MAIDExclusivePionDiffXSec2+; 
#pragma link C++ class insane::physics::MAIDExclusivePionDiffXSec3+;

#pragma link C++ class insane::physics::MAIDInclusivePionDiffXSec+; 
#pragma link C++ class insane::physics::MAIDInclusivePionDiffXSec::MAIDXSec_5Fold_2D_integrand+; 
#pragma link C++ class insane::physics::MAIDInclusivePi0DiffXSec+; 

//#pragma link C++ class insane::physics::PolarizedCrossSectionDifference+; 

//#pragma link C++ class insane::physics::RADCOR+;
//#pragma link C++ class insane::physics::RADCORVariables+;
//#pragma link C++ class insane::physics::RADCORKinematics+;
//
//#pragma link C++ class insane::physics::RADCOR::StripApproxIntegrand1Wrap+;
//#pragma link C++ class insane::physics::RADCOR::StripApproxIntegrand1_withDWrap+;
//#pragma link C++ class insane::physics::RADCOR::StripApproxIntegrand2Wrap+;
//#pragma link C++ class insane::physics::RADCOR::ExternalOnly_ExactInelasticIntegrandWrap+;
//#pragma link C++ class insane::physics::RADCOR::External2DEnergyIntegral_IntegrandWrap+;

#pragma link C++ class insane::physics::EsFuncWrap+;
#pragma link C++ class insane::physics::EpFuncWrap+;
// exact forms 
// internal 
#pragma link C++ class insane::physics::IntRadFuncWrap+;
#pragma link C++ class insane::physics::IntRadOmegaFuncWrap+;
#pragma link C++ class insane::physics::IntRadCosThkFuncWrap+;
// external 
#pragma link C++ class insane::physics::TExactFuncWrap+;
#pragma link C++ class insane::physics::EsExactFuncWrap+;
#pragma link C++ class insane::physics::EpExactFuncWrap+;
#pragma link C++ class insane::physics::EsExactFunc2Wrap+;
#pragma link C++ class insane::physics::EpExactFunc2Wrap+;

// Monte Carlo integration methods 
//#pragma link C++ function insane::physics::RADCOR::MCAcceptReject(const int &,double (*)(double *),double *,double *,int,int,double &,double &); 
//#pragma link C++ function insane::physics::RADCOR::MCSampleMean(const int &,double (*)(double *),double *,double *,int,double &,double &); 
//#pragma link C++ function insane::physics::RADCOR::MCSampleMean(const int &,double (*)(double),double *,double *,int,double &,double &); 
//#pragma link C++ function insane::physics::RADCOR::MCImportanceSampling(const int,double (*)(double *),double (*)(double *,double *,double *),double (*)(int,double *,double *),double *,double *,int,double &,double &); 
////// Adaptive Simpson integration method 
//#pragma link C++ function insane::physics::RADCOR::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
//#pragma link C++ function insane::physics::RADCOR::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 

// #pragma link C++ class insane::physics::RADCOR::Full_Elastic_integrand+;

//#pragma link C++ class insane::physics::RADCORInternalUnpolarizedDiffXSec+;
//
//#pragma link C++ class insane::physics::RADCORRadiatedDiffXSec+;
//#pragma link C++ class insane::physics::RADCORRadiatedUnpolarizedDiffXSec+;
//
//#pragma link C++ class insane::physics::RADCOR2+;
//#pragma link C++ class insane::physics::RADCOR2::StripApproxIntegrand1Wrap+;
//#pragma link C++ class insane::physics::RADCOR2::StripApproxIntegrand1_withDWrap+;
//#pragma link C++ class insane::physics::RADCOR2::StripApproxIntegrand2Wrap+;
//#pragma link C++ class insane::physics::RADCOR2::External2DEnergyIntegral_IntegrandWrap+;
//#pragma link C++ class insane::physics::RADCOR2::External3DIntegral_IntegrandWrap+;
//
//
//#pragma link C++ class insane::physics::POLRADVariables+;
//#pragma link C++ class insane::physics::POLRADKinematics+;
//
//#pragma link C++ class insane::physics::POLRAD+;
////#pragma link C++ class insane::physics::POLRAD::IRT_R_3+;
//#pragma link C++ class insane::physics::POLRAD::IRT_2D_integrand+;
//#pragma link C++ class insane::physics::POLRAD::ERTFuncWrap+;
//#pragma link C++ class insane::physics::POLRAD::QRTTauFuncWrap+;
//#pragma link C++ class insane::physics::POLRAD::QRTRFuncWrap+;
//#pragma link C++ class insane::physics::POLRAD::IRT_TauFuncWrap_delta+;
//#pragma link C++ class insane::physics::POLRAD::IRT_TauFuncWrap_2+;
//#pragma link C++ class insane::physics::POLRAD::IRT_RFuncWrap_3+;
//#pragma link C++ class insane::physics::POLRAD::QRT_TauFuncWrap_QE_Full+;
//#pragma link C++ class insane::physics::POLRAD::QRT_RFuncWrap_QE_Full+;
////#pragma link C++ function insane::physics::POLRAD::SimpleIntegration(double (*)(double &), double,double,double,int); 
////#pragma link C++ function insane::physics::POLRAD::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
////#pragma link C++ function insane::physics::POLRAD::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 
//
//#pragma link C++ class insane::physics::POLRADUltraRelativistic+;
//#pragma link C++ class insane::physics::POLRADUltraRelativistic::SIG_r_2D_integrand+;
////#pragma link off function insane::physics::POLRADUltraRelativistic::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
////#pragma link off function insane::physics::POLRADUltraRelativistic::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 
//
//#pragma link C++ class insane::physics::POLRADInternalPolarizedDiffXSec+;
//#pragma link C++ typedef insane::physics::PolradDiffXSec;
//#pragma link C++ class insane::physics::POLRADElasticDiffXSec+;
//#pragma link C++ class insane::physics::POLRADBornDiffXSec+;
//#pragma link C++ class insane::physics::POLRADRadiatedDiffXSec+;
//#pragma link C++ class insane::physics::POLRADElasticTailDiffXSec+;
//#pragma link C++ class insane::physics::POLRADQuasiElasticTailDiffXSec+;
//#pragma link C++ class insane::physics::POLRADInelasticTailDiffXSec+;
//#pragma link C++ class insane::physics::POLRADUltraRelInelasticTailDiffXSec+;
////#pragma link off function insane::physics::POLRADInternalPolarizedDiffXSec::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
////#pragma link off function insane::physics::POLRADInternalPolarizedDiffXSec::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 
//
//#pragma link C++ class insane::physics::RadiativeCorrections1D+;
//#pragma link C++ class insane::physics::RadiativeTail+;
////#pragma link C++ class insane::physics::RadiativeTail2+;
//#pragma link C++ class insane::physics::ElasticRadiativeTail+;
////#pragma link C++ class insane::physics::QuasielasticRadiativeTail+;
//#pragma link C++ class insane::physics::InelasticRadiativeTail+;
//#pragma link C++ class insane::physics::InelasticRadiativeTail2+;
//#pragma link C++ class insane::physics::FullInelasticRadiativeTail+;
//
//#pragma link C++ class insane::physics::ElectroProductionXSec+;
//
//#pragma link C++ class insane::physics::InclusiveMottXSec+;
////#pragma link C++ class insane::physics::ExclusiveMottXSec+;
//#pragma link C++ class insane::physics::epElasticDiffXSec+;
//#pragma link C++ class insane::physics::MollerDiffXSec+;
//#pragma link C++ class insane::physics::InclusiveMollerDiffXSec+;
//#pragma link C++ class insane::physics::eInclusiveElasticDiffXSec+;
//#pragma link C++ class insane::physics::pInclusiveElasticDiffXSec+;
//#pragma link C++ class insane::physics::RCeInclusiveElasticDiffXSec+;
//
//#pragma link C++ class insane::physics::BeamSpinAsymmetry+;
//#pragma link C++ class insane::physics::BeamTargetAsymmetry+;
//#pragma link C++ class insane::physics::PolarizedDISAsymmetry+;

//#pragma link C++ class insane::physics:OARPionDiffXSec+;
//#pragma link C++ class insane::physics:OARPionElectroDiffXSec+;
//#pragma link C++ class insane::physics:OARPionPhotoDiffXSec+;
//#pragma link C++ class insane::physics:ElectroOARPionDiffXSec+;
//#pragma link C++ class insane::physics:PhotoOARPionDiffXSec+;

#pragma link C++ class insane::physics::InclusiveWiserXSec+;
#pragma link C++ class insane::physics::PhaseSpaceSampler+;

#pragma link C++ function formc_(double *)+; 
#pragma link C++ function formm_(double *)+; 

#pragma link C++ function inif1f209_()+;
#pragma link C++ function emc_09_(float * ,float * ,int *,float *)+;
#pragma link C++ function cross_tot_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_qe_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function f1f2in09_(double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_tot_mod_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_qe_mod_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function f1f2in09_mod_(double * ,double * ,double *,double * ,double * ,double * ,double * ,double * )+;

#pragma link C++ function EMC_Effect(double * ,double *)+;

#pragma link C++ function qfs_(double*,double*,double*,double*,double*, double*,double*, double*)+;
#pragma link C++ function qfs_born_(double*,double*,double*,double*,double*,double*,double*,double*, double*,double*, double*,int*)+;
#pragma link C++ function qfs_radiated_(double*,double*,double*,double*,double*,double*,double*,double*)+;
#pragma link C++ function wiser_sub_(double * ,int * ,double * ,double * , double * )+;
#pragma link C++ function wiser_all_sig_(double * ,double * ,double * ,double * , int *  ,double * )+;
#pragma link C++ function wiser_all_sig0_(double * ,double * ,double * ,double * , int *  ,double * )+;
#pragma link C++ function wiser_fit_(int * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function vtp_(double * ,double * ,double * ,double * ,double * ,double * ,double * )+;

// Add all cross sections that can be radiated
//#pragma link C++ class insane::physics::Radiator<insane::physics::InclusiveDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<insane::physics::InclusiveBornDISXSec>+;
//#pragma link C++ class insane::physics::Radiator<insane::physics::CompositeDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<F1F209eInclusiveDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<F1F209QuasiElasticDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<CTEQ6eInclusiveDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<insane::physics::POLRADBornDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<insane::physics::POLRADInelasticTailDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<F1F209QuasiElasticDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<QFSInclusiveDiffXSec>+;

//#pragma link C++ class insane::physics::ExternalRadiator<insane::physics::InclusiveMollerDiffXSec>+;

#endif

