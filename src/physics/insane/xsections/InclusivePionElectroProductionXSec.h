#ifndef InclusivePionElectroProductionXSec_HH
#define InclusivePionElectroProductionXSec_HH 1

#include "CompositeDiffXSec.h"
#include "PhotonDiffXSec.h"
namespace insane {
namespace physics {

/**  Inclusive electro production of pions, kaons, and nucleons.
 *   Uses the fit from wiser and virtual photon spectrum from Traitor-Wright Nuc.Phys. A379.
 *   The recoil factor (eq13) is also included.
 *
 * \ingroup inclusiveXSec
 */
class InclusivePionElectroProductionXSec : public PhotonDiffXSec {

   protected:
      PhotonDiffXSec * fSig_0;       //->
      PhotonDiffXSec * fSig_plus;    //->
      PhotonDiffXSec * fSig_minus;   //->

   public:
      InclusivePionElectroProductionXSec();
      InclusivePionElectroProductionXSec(const InclusivePionElectroProductionXSec& rhs) ;
      virtual ~InclusivePionElectroProductionXSec();
      InclusivePionElectroProductionXSec& operator=(const InclusivePionElectroProductionXSec& rhs) ;
      virtual InclusivePionElectroProductionXSec*  Clone(const char * newname) const ;
      virtual InclusivePionElectroProductionXSec*  Clone() const ; 

      void  SetParameters(int i, const std::vector<double>& pars);
      const std::vector<double>& GetParameters() const ;

      virtual void InitializePhaseSpaceVariables();
      Double_t EvaluateXSec(const Double_t * x) const ;

      // Here we copied a bunch of setters from DiffXSec because we need to set not only this class
      // but the proton and neutron cross sections. The getters are not changed because they should be identical
      // to this class (and really not used).
      void       SetTargetMaterial(const TargetMaterial& mat){
         fSig_0->SetTargetMaterial(mat);
         fSig_plus->SetTargetMaterial(mat);
         fSig_minus->SetTargetMaterial(mat);
         fTargetMaterial = mat;
      }
      void SetTargetNucleus(const Nucleus & targ){
         InclusiveDiffXSec::SetTargetNucleus(targ);
         //if(fProtonXSec)   fProtonXSec->SetTargetNucleus(targ);
         //if(fNeutronXSec) fNeutronXSec->SetTargetNucleus(targ);
      }

      void       SetTargetMaterialIndex(Int_t i){
         fSig_0->SetTargetMaterialIndex(i);
         fSig_plus->SetTargetMaterialIndex(i);
         fSig_minus->SetTargetMaterialIndex(i);
         fMaterialIndex = i ;
      }
      virtual void  SetBeamEnergy(Double_t en){
         InclusiveDiffXSec::SetBeamEnergy(en);
         fSig_0->SetBeamEnergy(en);
         fSig_plus->SetBeamEnergy(en);
         fSig_minus->SetBeamEnergy(en);
      }

      virtual void  SetPhaseSpace(PhaseSpace * ps);
      virtual void  InitializeFinalStateParticles();

   ClassDef(InclusivePionElectroProductionXSec,1)
};

}
}


#endif

