#ifndef MAIDPolarizedTargetDiffXSec_H 
#define MAIDPolarizedTargetDiffXSec_H 

#include <cstdlib> 
#include <iostream> 
#include <cmath> 
#include <vector> 
#include "TString.h"
#include "TVector3.h"
#include "MAIDPolarizedKinematicKey.h"
#include "InclusiveDiffXSec.h"

namespace insane {
namespace physics {

/** MAID cross section (polarized target).
 * \deprecated
 * Paper reference: J. Phys. G: Nucl. Part. Phys 18, 449 (1992)  
 * For the reaction e N --> e' N' pi, where the user specifies pi and N'. 
 * Uses data from the MAID 2007 calculations: polarized target.
 * 
 *
 * \ingroup inclusiveXSec
 */ 
class MAIDPolarizedTargetDiffXSec: public InclusiveDiffXSec {

   private:

      mutable Double_t                                      x_eval[5];
      mutable std::vector<Double_t>                                      xd;
      mutable std::vector<std::vector<Double_t> >                             y_lim;
      //std::vector<std::vector<std::vector<std::vector<std::vector<Double_t> > > > >  c_5;
      // work around
      mutable std::vector<std::vector<std::vector<std::vector<Double_t> > > >           c_5_4[5];
      mutable std::vector<std::vector<std::vector<std::vector<Double_t> > > >      *    c_5;
      mutable std::vector<std::vector<std::vector<std::vector<Double_t> > > >           c_4;
      mutable std::vector<std::vector<std::vector<Double_t> > >                    c_3;
      mutable std::vector<std::vector<Double_t> >                             c_2;
      mutable std::vector<Double_t>                                      c_1;
      mutable Double_t                                              c_0;

      TString  fFileName,fConventionName,fprefix; 
      TString  fPion,fNucleon,frxn; 
      bool     fDebug;
      Int_t    fConvention;
      Int_t    fNumberOfPoints;  
      Double_t fh,fPx,fPz;

      mutable TOrdCollection fGridData;
      mutable MAIDPolarizedKinematicKey *fKineKey;
      mutable std::vector<Double_t> fEprime,fTheta,fPhi,fepsilon,fNu,fW,fQ2; 
      mutable std::vector<Double_t> fSigma_0,fA_e,fA_1,fA_2,fA_3,fA_e1,fA_e2,fA_e3,fXSf;
      mutable TVector3 fS,fS_pr;    // target polarizaton vectors (S = lab frame, S' = MAID coordinates)  

      void     FillHyperCubeValues() const ;
      Double_t GetInterpolation() const ;
      Double_t EvaluateVirtualPhotonFlux(Double_t,Double_t) const; 
      Double_t EvaluateFluxFactor(Double_t,Double_t,Double_t) const; 
      Double_t ComputeXSecForBin(Int_t,Int_t,Double_t,Double_t,Double_t) const;

      void ConvertTargetPolarization(Double_t E,Double_t Eprime,Double_t theta,Double_t phi=0) const; // convert target polarization to MAID coordinates 
      void BinarySearch(std::vector<Double_t> *,Double_t,Int_t &,Int_t &) const;
 
   public: 

      MAIDPolarizedTargetDiffXSec(const char * pion = "pi0",const char * nucleon = "p"); 
      virtual ~MAIDPolarizedTargetDiffXSec();
      //virtual MAIDPolarizedTargetDiffXSec*  Clone(const char * newname) const {
      //   std::cout << "MAIDPolarizedTargetDiffXSec::Clone()\n";
      //   MAIDPolarizedTargetDiffXSec * copy = new MAIDPolarizedTargetDiffXSec();
      //   (*copy) = (*this);
      //   return copy;
      //}
      //virtual MAIDPolarizedTargetDiffXSec*  Clone() const { return( Clone("") ); } 

      Double_t EvaluateXSec(const Double_t *) const; 

      //void     InitializePhaseSpaceVariables();
      void     Initialize(); 
      void     ImportData();
      void     DebugMode(bool ans){fDebug = ans;} // doesn't do anything yet...  
      void     Print();  
      void     PrintData();  
      void     PrintParameters();  
      void     Clear(); 
      void     SetVirtualPhotonFluxConvention(Int_t i);
      void     SetReactionChannel(TString pion,TString nucleon);
      void     SetTargetPolarization(Double_t *p){
         // components: 
         // 0 => polarization along x axis (+x out of the page)  
         // 1 => polarization along y axis (+y to the right) 
         // 2 => polarization along z axis (e- beam, up) 
         fS.SetXYZ(p[0],p[1],p[2]); 
      } 

      ClassDef(MAIDPolarizedTargetDiffXSec,1)
};

}}
#endif 
