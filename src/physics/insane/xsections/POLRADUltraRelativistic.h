#ifndef POLRADUltraRelativistic_HH
#define POLRADUltraRelativistic_HH 1 

#include "TMath.h"
#include "POLRAD.h"
#include "TLorentzVector.h"
namespace insane {
namespace physics {

/** Polarized radiative corrections, ultra relativistic approximation (POLRAD).
 *  Much of this code is from the PolRad2.0 manual. 
 *
 *  \ingroup physics 
 */
class POLRADUltraRelativistic: public POLRAD {
   private:
      Double_t fSFs_x[8];
      Double_t fSFs_s[8];
      Double_t fSFs_t[8];
      Double_t fxi_int;                        ///< Integration variable

   protected:
      Double_t fX_1;       ///<
      Double_t fXTilde;    ///<
      Double_t fY_plus;    ///<
      Double_t fY_minus;   ///<
      Double_t fEta_min;   ///<

      /** @name Ultrarelativistic approximation
       *  T-functions (Eq 33).   
       *  NOTE: In the following, First term after underscore is the subscript.  
       *       Term after second underscore is superscript. 
       */
      ///@{
      Double_t ft_1,ft_2;  ///< t integration limits
      Double_t ft_s,ft_x;  ///< Kinematic factors for T-, sL- and L-functions (Eq. 33--35)
      Double_t fu,fu_x,fu_s,fQ2_xi,fTT;    ///< Kinematic factors for T-, sL- and L-functions (Eq. 33--35)
      Double_t fT_u_L,fT_pPara_L,fT_pPara_x,fT_q_L;
      Double_t fT_qPerp_L,fT_pPerp_L,fT_pPerp_x; 
      Double_t fsL[5]; // Script-L functions (Eq. 34) 
      ///@}

   public:
      POLRADUltraRelativistic();
      virtual ~POLRADUltraRelativistic();

      Double_t sig_in();
      Double_t Delta_1_in();
      Double_t sig_V_r();
      Double_t sig_s_in();
      Double_t sig_s_in_Integrand(Double_t);
      Double_t sig_s(Double_t);
      Double_t sig_p_in();
      Double_t sig_p_in_Integrand(Double_t);
      Double_t sig_p(Double_t);

      // inelastic
      Double_t sig_r_in_long();
      Double_t sig_r_in_long_2D();

      Double_t sig_r_in_long_Integrand(Double_t);
      Double_t sig_r_in_long_Integrand_1D(Double_t xi);
      Double_t sig_r_in_long_Integrand_1D_2(Double_t t); ///< Uses fxi_int while doing t integral
      Double_t sig_r_in_long_Integrand_2D(Double_t xi, Double_t t);
      Double_t sig_r_in_long_Integrand_2D_1(Double_t xi);
      //Double_t sig_r_in_long_Integrand(Double_t,Double_t);
      Double_t sig_r_in_tran();
      Double_t sig_r_in_tran_Integrand(Double_t);
      //Double_t sig_r_in_tran_Integrand(Double_t,Double_t);
      Double_t sig_u_p();
      Double_t sig_u_p_Integrand(Double_t);
      Double_t sig_p_p_long();
      Double_t sig_p_p_long_Integrand(Double_t);
      Double_t sig_p_p_tran();
      Double_t sig_p_p_tran_Integrand(Double_t);
      Double_t sig_u_d();
      Double_t sig_u_d_Integrand(Double_t);
      Double_t sig_p_d_long();
      Double_t sig_p_d_long_Integrand(Double_t);
      Double_t sig_p_d_tran();
      Double_t sig_p_d_tran_Integrand(Double_t);
      Double_t sig_q_d_long();
      Double_t sig_q_d_long_Integrand(Double_t);
      Double_t sig_q_d_tran();
      //Double_t sig_q_d_tran_Integrand(Double_t);

      // This function is called in each T-function below 
      void ProcessTFuncKin(Double_t xi); 
      void ProcessTFuncKin(Double_t xi, Double_t t); 

      void ProcessUltraRelCoeffs(Double_t);

      // T-functions (Eq. 33) 
      Double_t T_u_L(Double_t);  
      Double_t T_u_L_a(Double_t xi);  
      Double_t T_u_L_b(Double_t xi, Double_t t);  

      Double_t T_pPara_L(Double_t);  
      Double_t T_pPara_L_a(Double_t xi);  
      Double_t T_pPara_L_b(Double_t xi, Double_t t);  

      Double_t T_pPara_x(Double_t);  
      Double_t T_pPara_x_a(Double_t);  
      //Double_t T_pPara_x_b(Double_t xi, Double_t t);  

      Double_t T_q_L(Double_t);  
      Double_t T_q_L_a(Double_t);  
      Double_t T_q_L_b(Double_t xi, Double_t t);  

      Double_t T_pPerp_L(Double_t);  
      Double_t T_pPerp_L_a(Double_t);  
      Double_t T_pPerp_L_b(Double_t xi, Double_t t);  

      Double_t T_pPerp_x(Double_t);  
      Double_t T_pPerp_x_a(Double_t);  
      //Double_t T_pPerp_x_b(Double_t xi, Double_t t);  

      // Script L-functions (Eq. 34) 
      Double_t sL(Int_t,Int_t,Double_t); ///< \deprecated 

      Double_t sL_1_a(Int_t i, Double_t xi); ///< Eq 34. without t integral  
      Double_t sL_2_a(Int_t i, Double_t xi); ///< Eq 34. without t integral  
      Double_t sL_3_a(Int_t i, Double_t xi); ///< Eq 34. without t integral  
      Double_t sL_4_a(Int_t i, Double_t xi); ///< Eq 34. without t integral  
      Double_t sL_5_a(Int_t i, Double_t xi); ///< Eq 34. without t integral  

      Double_t sL_1_b(Int_t i, Double_t xi, Double_t t); ///< Eq 34. with t integral pushed out 
      Double_t sL_2_b(Int_t i, Double_t xi, Double_t t); ///< Eq 34. with t integral pushed out 
      Double_t sL_3_b(Int_t i, Double_t xi, Double_t t); ///< Eq 34. with t integral pushed out 
      Double_t sL_4_b(Int_t i, Double_t xi, Double_t t); ///< Eq 34. with t integral pushed out 
      Double_t sL_5_b(Int_t i, Double_t xi, Double_t t); ///< Eq 34. with t integral pushed out 

      // L-functions (Eq. 35)  without integral
      Double_t L_t(        Int_t i , Double_t xi, Double_t t);
      Double_t L_Y(        Int_t i , Double_t xi, Double_t t);
      Double_t L_s_l_a(    Int_t i , Double_t xi); ///< Eq 35. term without integral
      Double_t L_s_l_b(    Int_t i , Double_t xi, Double_t t); ///< Eq 35. term with integral
      Double_t L_x_l_a(    Int_t i , Double_t xi);
      Double_t L_x_l_b(    Int_t i , Double_t xi, Double_t t);
      Double_t L_s_k_a(    Int_t i , Double_t xi);
      Double_t L_s_k_b(    Int_t i , Double_t xi, Double_t t);
      Double_t L_x_k_a(    Int_t i , Double_t xi);
      Double_t L_x_k_b(    Int_t i , Double_t xi, Double_t t);
      Double_t LTilde_s_k( Int_t i , Double_t xi, Double_t t);
      Double_t LTilde_x_k( Int_t i , Double_t xi, Double_t t);

      // L-function integrands 
      //Double_t L_t_Integrand(Double_t ); 
      //Double_t L_Y_Integrand(Double_t ); 
      //Double_t L_s_l_Integrand(Double_t ); 
      //Double_t L_x_l_Integrand(Double_t ); 
      //Double_t L_s_k_Integrand(Double_t ); 
      //Double_t L_x_k_Integrand(Double_t ); 
      //Double_t LTilde_Integrand(Double_t ); 

      //Double_t GetStructureFunction(Double_t,Double_t); 

      Double_t AdaptiveSimpson(Double_t (POLRADUltraRelativistic::*)(Double_t )    ,Double_t,Double_t,Double_t,Int_t);
      Double_t AdaptiveSimpsonAux(Double_t (POLRADUltraRelativistic::*)(Double_t ) ,Double_t,Double_t,Double_t,Double_t,Double_t,Double_t,Double_t,Int_t);

      /** IRT 2 dimensional integrand in R and tau. R = x[0] and tau = x[1]. */
      class SIG_r_2D_integrand: public ROOT::Math::IBaseFunctionMultiDim {
         public:
            SIG_r_2D_integrand(){}
            ~SIG_r_2D_integrand(){}
            mutable POLRADUltraRelativistic * fPOLRAD;
            double DoEval(const double* x) const { return fPOLRAD->sig_r_in_long_Integrand_2D(x[0],x[1]); }
            unsigned int NDim() const { return 2; }
            ROOT::Math::IBaseFunctionMultiDim* Clone() const { return new SIG_r_2D_integrand(); } 
            ClassDef(SIG_r_2D_integrand,1)
      }; 

      ClassDef(POLRADUltraRelativistic,1)

};
}}


#endif
