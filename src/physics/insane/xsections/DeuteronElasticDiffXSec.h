#ifndef DeuteronElasticDiffXSec_H
#define DeuteronElasticDiffXSec_H 1

#include "ExclusiveDiffXSec.h"
#include "InclusiveDiffXSec.h"
#include "TMath.h"
#include "FormFactors.h"
#include "TVector3.h"


namespace insane {
   namespace physics {
/** Elastic scattering using the Dipole FFs.
 * \ingroup exclusiveXSec
 */
class DeuteronElasticDiffXSec : public ExclusiveDiffXSec {

   public:
      DeuteronElasticDiffXSec();
      virtual ~DeuteronElasticDiffXSec();
            DeuteronElasticDiffXSec(const DeuteronElasticDiffXSec& rhs)            = default;
            DeuteronElasticDiffXSec& operator=(const DeuteronElasticDiffXSec& rhs) = default;
            DeuteronElasticDiffXSec(DeuteronElasticDiffXSec&&)                     = default;
            DeuteronElasticDiffXSec& operator=(DeuteronElasticDiffXSec&&)          = default;

      virtual DeuteronElasticDiffXSec*  Clone(const char * newname) const {
         std::cout << "DeuteronElasticDiffXSec::Clone()\n";
         auto * copy = new DeuteronElasticDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual DeuteronElasticDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual void InitializePhaseSpaceVariables();

      Double_t GetEPrime(const Double_t theta) const ;
      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      virtual Double_t EvaluateXSec(const Double_t * x) const;

      ClassDef(DeuteronElasticDiffXSec, 1)
};
}
}


#endif

