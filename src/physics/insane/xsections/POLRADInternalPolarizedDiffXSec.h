#ifndef POLRADInternalPolarizedDiffXSec_HH
#define POLRADInternalPolarizedDiffXSec_HH 1

#include "PhysicalConstants.h"
#include "DiffXSec.h"
#include "InclusiveDiffXSec.h"
#include "PhaseSpace.h"
#include "POLRAD.h"
#include "POLRADUltraRelativistic.h"
#include "StructureFunctionsFromPDFs.h"
#include "PolarizedStructureFunctionsFromPDFs.h"
#include "FormFactors.h"
#include "MSWFormFactors.h"
#include "AmrounFormFactors.h"
#include "CTEQ6UnpolarizedPDFs.h"
#include "BBPolarizedPDFs.h"
#include "StatisticalPolarizedPDFs.h"
#include "DNS2005PolarizedPDFs.h"
#include "DSSVPolarizedPDFs.h"
#include "F1F209StructureFunctions.h"
#include "NMC95StructureFunctions.h"

namespace insane {
namespace physics {

/** Base Class for POLRAD cross sections with (internal) radiative corrections.
 *  Uses the phase space variables x and y while most of the derived classes 
 *  use E', theta, phi.
 *
 *  All equations are exact and from the POLRAD manual unless specified otherwise.
 *
 * \ingroup inclusiveXSec
 */ 
class POLRADInternalPolarizedDiffXSec: public InclusiveDiffXSec {
   protected:
      POLRAD * fPOLRADCalc; //->

   public: 
      POLRADInternalPolarizedDiffXSec();
      virtual ~POLRADInternalPolarizedDiffXSec();
      //virtual POLRADInternalPolarizedDiffXSec*  Clone(const char * newname) const {
      //   std::cout << "POLRADInternalPolarizedDiffXSec::Clone()\n";
      //   POLRADInternalPolarizedDiffXSec * copy = new POLRADInternalPolarizedDiffXSec();
      //   (*copy) = (*this);
      //   return copy;
      //}
      //virtual POLRADInternalPolarizedDiffXSec*  Clone() const { return( Clone("") ); } 

      /** Defines phase space with variables x and y 
       *  instead of the usual E',theta,phi
       */ 
      virtual void InitializePhaseSpaceVariables();

      virtual Double_t EvaluateXSec(const Double_t *x) const = 0; 

      POLRAD * GetPOLRAD(){ 
         if(!fPOLRADCalc) {
            fPOLRADCalc = new POLRAD();
            fPOLRADCalc->SetVerbosity(1);
            //fPOLRADCalc->DoQEFullCalc(false); 
            fPOLRADCalc->SetTargetNucleus(Nucleus::Proton());
            fPOLRADCalc->fErr   = 1E-1;   // integration error tolerance 
            fPOLRADCalc->fDepth = 3;     // number of iterations for integration 
            fPOLRADCalc->SetMultiPhoton(true); 
            //fPOLRADCalc->SetUltraRel   (false);
         }
         return fPOLRADCalc;
      }

      virtual void       SetHelicity(Double_t h) { 
         if(TMath::Abs(h) > 1.0){
            Error("SetHelicity","Argument too big: |h|<=1.");
         }else{ 
            fHelicity = h;
            GetPOLRAD()->SetHelicity(fHelicity);
         }
         InclusiveDiffXSec::SetHelicity(h);
      }
      
      virtual void SetTargetNucleus(const Nucleus & targ){ 
         fTargetNucleus = targ; 
         GetPOLRAD()->SetTargetNucleus(targ);
      }

      /** Set the unpolarized structure functions to be used to calculate W1,W2,F1,F2, etc. */
      void SetUnpolarizedStructureFunctions(StructureFunctions * sf)  {
         GetPOLRAD()->SetUSFs(sf);
         InclusiveDiffXSec::SetUnpolarizedStructureFunctions(sf);
      }

      /** Set the QE structure functions to be used to calculate G1,G2,g1,g2, etc.  */
      void SetQEStructureFunctions(StructureFunctions * sf) {
         GetPOLRAD()->SetUQESFs(sf);
      }
      /** Set the polarized structure functions to be used to calculate G1,G2,g1,g2, etc.  */
      void SetPolarizedStructureFunctions(PolarizedStructureFunctions * sf) {
         GetPOLRAD()->SetPSFs(sf);
         InclusiveDiffXSec::SetPolarizedStructureFunctions(sf);
      }

      /** Set the form factors. */
      void SetFormFactors(FormFactors * ff) {
         GetPOLRAD()->SetFFs(ff);
         InclusiveDiffXSec::SetFormFactors(ff);
      }

      /** Set the form factors used for nuclei. */
      void SetTargetFormFactors(FormFactors * ff) {
         GetPOLRAD()->SetPOLRADTargetFormFactors(ff);
      }

      //void SetTargetType(Nucleus::NucleusType t){
      //   InclusiveDiffXSec::SetTargetType(t);
      //   fPOLRADCalc->SetTargetType(t); 
      //}
      void SetVerbosity(int v){GetPOLRAD()->SetVerbosity(v);} 


      ClassDef(POLRADInternalPolarizedDiffXSec,3)
};

typedef POLRADInternalPolarizedDiffXSec PolradDiffXSec;
//____________________________________________________________________________________

/** Born Cross Section for elastic scattering.
 *  Used as the base class for all POLRAD cross-sections which use 
 *  the phase space variables E',theta, and phi.
 *
 * \ingroup inclusiveXSec
 */
class POLRADElasticDiffXSec: public POLRADInternalPolarizedDiffXSec {
   public:
      POLRADElasticDiffXSec(){
         SetTitle("POLRADElasticDiffXSec");//,"POLRAD Born cross-section");
         SetPlotTitle("POLRAD Born Elastic XSec");
         fnDim = 2;
      }
      virtual ~POLRADElasticDiffXSec(){ }
      virtual POLRADElasticDiffXSec*  Clone(const char * newname) const {
         std::cout << "POLRADElasticDiffXSec::Clone()\n";
         auto * copy = new POLRADElasticDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual POLRADElasticDiffXSec*  Clone() const { return( Clone("") ); } 

      /** Defines the phase space using E',theta,phi.*/
      virtual void InitializePhaseSpaceVariables() {
         PhaseSpace * ps = GetPhaseSpace();
         if (ps) delete ps;
         ps = nullptr;
         if (!ps) {
            ps = new PhaseSpace();
            auto * varEnergy = new PhaseSpaceVariable();
            varEnergy->SetNameTitle("energy_e", "E_{e'}");
            varEnergy->SetMinimum(0.01); //GeV
            varEnergy->SetMaximum(GetBeamEnergy()); //GeV
            varEnergy->SetDependent(true);
            ps->AddVariable(varEnergy);

            auto *   varTheta = new PhaseSpaceVariable();
            varTheta->SetNameTitle("theta_e", "#theta_{e'}"); // ROOT string latex
            varTheta->SetMinimum( 0.01 *degree ); //
            varTheta->SetMaximum( 180.0*degree ); //
            ps->AddVariable(varTheta);

            auto *   varPhi = new PhaseSpaceVariable();
            varPhi->SetNameTitle("phi_e", "#phi_{e'}"); // ROOT string latex
            varPhi->SetMinimum(-360.0*degree ); //
            varPhi->SetMaximum( 360.0*degree ); //
            ps->AddVariable(varPhi);

            SetPhaseSpace(ps);
         } 
      }

      /** Note the dimension of the differential is 2.
       *  Use this to set the appropriate Eprime given the two
       *  scattering angles are the independent variables. 
       */
      virtual Double_t* GetDependentVariables(const Double_t *x) const {
         Double_t  theta = x[0];
         Double_t  phi   = x[1];
         Double_t  Ebeam = GetBeamEnergy();
         /// \todo fix target mass to whatever...
         Double_t  Eprime = Ebeam/(1.0 + Ebeam*(1.0-TMath::Cos(theta)))/(M_p/GeV);
         fDependentVariables[0] = Eprime;
         fDependentVariables[1] = theta;
         fDependentVariables[2] = phi;
         return(fDependentVariables);
      }
      virtual Double_t EvaluateXSec(const Double_t *x) const {
         if (!VariablesInPhaseSpace(fnDim, x)){
            //std::cout << x[0] << std::endl;
            //std::cout << x[1] << std::endl;
            //std::cout << x[2] << std::endl;
            //std::cout << "[POLRADElasticDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
            return(0.0);
         }

         Double_t Eprime  = x[0];
         Double_t theta   = x[1];
         Double_t Ebeam   = GetBeamEnergy();
         Double_t nu      = Ebeam-Eprime;
         Double_t Mtarg   = M_p/GeV;  
         Double_t S       = 2.0*Ebeam*Mtarg;
         //Double_t X       = 2.0*Eprime*Mtarg;
         Double_t Q2      = 4.0*Eprime*Ebeam*TMath::Sin(theta/2.0)*TMath::Sin(theta/2.0);

         fPOLRADCalc->SetKinematics(Ebeam,Eprime,theta);

         Double_t bornXSec = fPOLRADCalc->sig_el_born(S,Q2); 
         bornXSec = bornXSec * (Eprime/(2.0*pi*Mtarg*nu));
         if(IncludeJacobian()){
            bornXSec *= TMath::Sin(theta);
         }
         return bornXSec*hbarc2_gev_nb;
      } 

      /** Returns scattering angle as a function of energy. */
      Double_t GetTheta(const Double_t Ep) const {
         Double_t E0 = GetBeamEnergy();
         if(Ep > E0) return(0.0);
         Double_t M  = M_p/GeV;
         return( 2.0*TMath::ASin(TMath::Sqrt((M/2.0)*(1.0/Ep - 1.0/E0) )) );
      }

      /**  Returns the scattered electron energy using the angle.  */
      Double_t GetEPrime(const Double_t theta)const  {
         return (GetBeamEnergy() / (1.0 + 2.0 * GetBeamEnergy() / (M_p/GeV) * TMath::Power(TMath::Sin(theta / 2.0), 2.0)));
      }

      /** Cross section as a function of energy.
       *  \param x[0] = eprime 
       *  \param p[0] = phi 
       */
      double EnergyDependentXSec(double *x, double *p) {
         Double_t y[2];
         y[0] = GetTheta(x[0]);
         y[1] = p[0];
         return(EvaluateXSec(GetDependentVariables(y)));
      }

      ClassDef(POLRADElasticDiffXSec,1)
};

}}




#endif

