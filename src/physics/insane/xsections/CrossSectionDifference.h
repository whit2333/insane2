#ifndef CrossSectionDifference_HH
#define CrossSectionDifference_HH 1

#include "InclusiveDiffXSec.h"
#include "TList.h"

namespace insane {
namespace physics {
class CrossSectionDifference : public InclusiveDiffXSec {
   private:
      mutable Double_t           fXSecDiff;        /// X section diff from last EvaluteXSec
      mutable Double_t           fXSecSum;         /// X section sum from last EvaluateXSec
      //DiffXSec           * fXSecs[2];       
      TList fXSecs; 
      Bool_t fReturnAsymmetry;  /// EvaluteXSec returns the asymmetry if true 

   public:
      CrossSectionDifference(){
         fReturnAsymmetry = false;
      }
      ~CrossSectionDifference(){}

      Bool_t GetReturnAsymmetry() const { return fReturnAsymmetry; }
      void   SetReturnAsymmetry(Bool_t v = true){ fReturnAsymmetry = v; }

      Double_t EvaluateXSec(const Double_t * x) const {
         if( (!fXSecs.At(0)) || (!fXSecs.At(1)) ) {
            std::cout << "Something is wrong!" << std::endl;
            return(0);
         }
         if (!VariablesInPhaseSpace(fnDim, x)){
            //std::cout << "[CrossSectionDifference::EvaluateXSec]: Something is wrong!" << std::endl;
            return(0.0);
         }
         Double_t sig0 = ((DiffXSec*)fXSecs.At(0))->EvaluateXSec(x);
         Double_t sig1 = ((DiffXSec*)fXSecs.At(1))->EvaluateXSec(x);
         //Double_t sig1 = fXSecs->EvaluateXSec(x);
         fXSecSum  = sig0 + sig1;
         fXSecDiff = sig0-sig1;

         if(GetReturnAsymmetry()) return( fXSecDiff/(fXSecSum+ 1.0e-10) );

         return( fXSecDiff);
      }

      TList * GetCrossSectionList() { return &fXSecs; }

      Int_t SetCrossSection(DiffXSec * sig,Int_t i){
         if(!sig) return -1;
         if(i==0){
            fXSecs.AddAt(sig,0);
            return(0);
         }
         if(i==1){
            fXSecs.AddAt(sig,1);
            //fXSecs[1] == sig;
            return(0);
         }
         return(-1);
      }

      DiffXSec * GetCrossSection(Int_t i){
         if( fXSecs.GetEntries() != 2 ) {
           Error("GetCrossSection","There are not 2 cross sections"); 
           return (nullptr);
         }
         if(i==0) return((DiffXSec*)fXSecs.At(0));
         if(i==1) return((DiffXSec*)fXSecs.At(1));
         else Error("GetCrossSection","Index out of range");
         return(nullptr);
      }

      Double_t EvaluateAsymmetry(const Double_t *) const {
         return(0);  
      } 

      Double_t GetAsymmetry() {return( fXSecDiff/fXSecSum ); }


ClassDef(CrossSectionDifference,1)
};
}
}

#endif

