#ifndef InelasticRadiativeTail_HH
#define InelasticRadiativeTail_HH 1

#include "RadiativeTail.h"
#include "POLRADInelasticTailDiffXSec.h"
namespace insane {
namespace physics {

/** Inelastic Radiative tail with corrections.
 *  
 * \ingroup xsections
 */
class InelasticRadiativeTail : public RadiativeTail {

   protected:
      POLRADInternalPolarizedDiffXSec * fDiffXSec0;  //  born cross section used by RADCOR to calculate
      Bool_t   fAddRegion4;                                // adds 2d integral over region IV (see Mt 1969)
      Bool_t   fInternalOnly;                              //

   public:
      InelasticRadiativeTail();
      virtual ~InelasticRadiativeTail();

      void SetRegion4(bool v = true) { fAddRegion4 = v; }
      void SetInternalOnly(bool v = true) { fInternalOnly = v; }

      POLRADInternalPolarizedDiffXSec * GetBornXSec() const { return fDiffXSec0; }

      void SetPolarizations(Double_t pe, Double_t pt){
         GetPOLRAD()->SetPolarizations(pe,pt,0.0);
         GetBornXSec()->GetPOLRAD()->SetPolarizations(pe,pt,0.0);
      } 

      virtual Double_t EvaluateXSec(const Double_t *x) const {
         if (!VariablesInPhaseSpace(fnDim, x)){
            //std::cout << "[POLRADElasticTailDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
            return(0.0);
         }

         Double_t Eprime  = x[0];
         Double_t theta   = x[1];
         Double_t phi     = x[2];
         Double_t Ebeam   = GetBeamEnergy();
         GetBornXSec()->SetBeamEnergy(Ebeam);

         //Double_t nu      = Ebeam-Eprime;
         //Double_t Mtarg   = fPOLRAD->GetTargetMass();  

         Double_t sig_rad = 0.0;

         // Using the POLRAD IRT
         //sig_rad = fDiffXSec0->EvaluateXSec(x);

         // Using the Equiv. Rad. Method
         Double_t sig_rad2 = 0.0;
         if(fInternalOnly){
            sig_rad = GetRADCOR()->ContinuumStragglingStripApprox_InternalEquivRad(Ebeam,Eprime,theta,phi);
            if(fAddRegion4) sig_rad2 = GetRADCOR()->Internal2DEnergyIntegral(Ebeam,Eprime,theta,phi);
            sig_rad += sig_rad2;
         } else {
            sig_rad = GetRADCOR()->ContinuumStragglingStripApprox(Ebeam,Eprime,theta,phi);
            if(fAddRegion4) sig_rad2 = GetRADCOR()->Internal2DEnergyIntegral(Ebeam,Eprime,theta,phi);
            sig_rad += sig_rad2;
         }


         if( IncludeJacobian() ) sig_rad = sig_rad*TMath::Sin(theta);
         if(sig_rad <0.0 || TMath::IsNaN(sig_rad)) sig_rad = 0.0;
         return sig_rad;
      } 


      ClassDef(InelasticRadiativeTail,1)
};


/** External and internal.
 *
 * \ingroup xsections
 */
class FullInelasticRadiativeTail : public RadiativeTail {

   protected:
      InelasticRadiativeTail           * fDiffXSec;  //Internally radiated tail
      POLRADInternalPolarizedDiffXSec  * fDiffXSec0;  //  born cross section used by RADCOR to calculate
    
   public:
      FullInelasticRadiativeTail();
      virtual ~FullInelasticRadiativeTail();

      InelasticRadiativeTail * GetInternalXSec(){ return fDiffXSec; }
      POLRADInternalPolarizedDiffXSec * GetPOLRADIRT() { return fDiffXSec0; }

      virtual Double_t EvaluateXSec(const Double_t *x) const {
         if (!VariablesInPhaseSpace(fnDim, x)){
            //std::cout << "[POLRADElasticTailDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
            return(0.0);
         }

         Double_t Eprime  = x[0];
         Double_t theta   = x[1];
         Double_t phi     = x[2];
         Double_t Ebeam   = GetBeamEnergy();
         fDiffXSec->SetBeamEnergy(Ebeam);
         fDiffXSec0->SetBeamEnergy(Ebeam);
         //Double_t nu      = Ebeam-Eprime;
         //Double_t Mtarg   = fPOLRAD->GetTargetMass();  
         Double_t sig_rad = fRADCOR->ContinuumStragglingStripApprox(Ebeam,Eprime,theta,phi);
         //std::cout << " sig_rad  = " << sig_rad << std::endl;
         if( IncludeJacobian() ) sig_rad = sig_rad*TMath::Sin(theta);
         if(sig_rad <0.0 || TMath::IsNaN(sig_rad)) sig_rad = 0.0;
         return sig_rad;
      } 


      ClassDef(FullInelasticRadiativeTail,1)
};
}
}
#endif

