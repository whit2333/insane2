#ifndef ProtonElasticDiffXSec_H
#define ProtonElasticDiffXSec_H 1

#include "ExclusiveDiffXSec.h"
#include "InclusiveDiffXSec.h"
#include "TMath.h"
#include "FormFactors.h"
#include "TVector3.h"
#include "ProtonElasticDiffXSec.h"


namespace insane {
   namespace physics {
/** Elastic scattering using the Dipole FFs.
 * \ingroup exclusiveXSec
 */
class ProtonElasticDiffXSec : public ExclusiveDiffXSec {

   public:
      ProtonElasticDiffXSec();
      virtual ~ProtonElasticDiffXSec();
            ProtonElasticDiffXSec(const ProtonElasticDiffXSec& rhs)            = default;
            ProtonElasticDiffXSec& operator=(const ProtonElasticDiffXSec& rhs) = default;
            ProtonElasticDiffXSec(ProtonElasticDiffXSec&&)                     = default;
            ProtonElasticDiffXSec& operator=(ProtonElasticDiffXSec&&)          = default;

      virtual ProtonElasticDiffXSec*  Clone(const char * newname) const {
         std::cout << "ProtonElasticDiffXSec::Clone()\n";
         auto * copy = new ProtonElasticDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual ProtonElasticDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual void InitializePhaseSpaceVariables();

      Double_t GetEPrime(const Double_t theta) const ;
      Double_t GetTheta(double ep) const ;
      double GetTheta_e(double Es, double alpha) const;
      double GetPalpha(double Es, double alpha) const;
      double ElectronToAlphaJacobian(double Es, double alpha) const ;
      double Ib(Double_t E0,Double_t E,Double_t t) const ;
      double GetTheta_alpha(double Es, double P_alpha) const ;

      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      virtual Double_t   EvaluateXSec(const Double_t * x) const;
      virtual void       DefineEvent(Double_t * vars);

      ClassDef(ProtonElasticDiffXSec, 1)
};
}
}


#endif

