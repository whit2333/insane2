#ifndef WiserXSection_HH
#define WiserXSection_HH 1

#include "InclusiveDiffXSec.h"
#include "insane/base/FortranWrappers.h"
#include "InSANEMathFunc.h"
#include "CompositeDiffXSec.h"

namespace insane {
namespace physics {

//class WiserFit {
//   private:
//      Double_t fPars_piPlus[7];
//      Double_t fPars_piMinus[7];
//
//   public: 
//      WiserFit();
//      virtual ~WiserFit();
//
//      double operator()(double * x, double * p);
//
//      ClassDef(WiserFit);
//};

/**  Wiser Cross Section for inclusive neutral and charged pion, charged kaon, proton and anti-proton
 *   production. The original code was a fit to photoproduction at SLAC.
 *  \todo Figure out how they are actully entering in the radationlength.
 *
 *  A targer radiation length can be retrieved from Target::GetRadiationLength()
 *
 *
 * \ingroup wiser
 */
class InclusiveWiserXSec : public InclusiveDiffXSec {
   protected:
      TParticlePDG * fParticle; //->
      double         fWiserParticleCode;
      Double_t       fRadiationLength;

   public:
      InclusiveWiserXSec();
      virtual ~InclusiveWiserXSec() { }

      Double_t GetRadiationLength(){ return fRadiationLength; }
      void     SetRadiationLength(Double_t r) { fRadiationLength = r; }
      
      /** eN--> gamma* N --> e' P X */
      virtual void InitializePhaseSpaceVariables();

      /** */
      void PrintPossibleParticles();

      /** returns a pointer to the TParticlePDG object used to determine the
       *  type of particle used in the cross section calculation.
       */
      TParticlePDG * GetParticlePDG() { return fParticle; }

      /** returns the PDG particle encoding */
      Int_t GetParticleType() { return fParticle->PdgCode(); }

      /** Set the particle type by PDG encoding see
       *  InclusiveWiserXSec::PrintPossibleParticles()
       */
      void SetProductionParticleType(Int_t PDGcode, Int_t part = 0);

      void SetParticlePDGEncoding(Int_t PDGcode) {
         SetProductionParticleType(PDGcode);
      }

      /** Using the PDG particle encoding, calculate the particle type number
       *  needed for the legacy Wiser Fortran code.
       *  wiser_type=1 for pi+;  2 for pi-, 3=k+, 4=k-, 5=p, 6=p-bar
       */
      Int_t GetWiserParticleType(Int_t PDGcode);

      /** const method not for setting...*/
      Int_t GetWiserType(Int_t PDGcode) const ;

      /** Evaluate Cross Section (nbarn/GeV*str).
       */
      Double_t EvaluateXSec(const Double_t * x) const ;

      /**  Needed by ROOT::Math::IBaseFunctionMultiDim */
      unsigned int NDim() const { return fnDim; }

      ClassDef(InclusiveWiserXSec, 1)
};
}}







#endif

