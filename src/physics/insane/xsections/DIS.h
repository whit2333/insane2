#ifndef insane_physics_DIS_HH
#define insane_physics_DIS_HH 1

#include "insane/xsections/CrossSection.h"
//#include "insane/new_xsec/DiffCrossSection.h"
//#include "insane/new_xsec/Jacobians.h"
//#include "insane/new_xsec/NFoldDifferential.h"
//#include "insane/new_xsec/PSSampler.h"
//#include "insane/new_xsec/PhaseSpaceVariables.h"
#include "insane/base/PhysicalConstants.h"

#include "insane/structurefunctions/F1F209_SFs.h"
#include "insane/structurefunctions/fwd_SFs.h"
#include <functional>

namespace insane {
  namespace physics {

    namespace xs {

      double DIS_xs_func(double E0, double x_bj, double Q2, double F1, double F2) ;

      template <typename FuncTup = std::tuple<insane::physics::F1F209_SFs>>
      struct DIS_XS : DIS_CrossSection<FuncTup> {

        using DIS_Differential = physics::NDiff<physics::PSV<physics::Invariant>,
                                                physics::PSV<Invariant>, physics::IPSV>;
        using DIS_PhaseSpace   = physics::PS<DIS_Differential>;
        using StructureFunc    = typename DIS_CrossSection<FuncTup>::StructureFunc;

        static auto make_DIS_cross_section() {
          using namespace insane::physics;
          PSV<Invariant> x_psv({0.01, 0.99}, Invariant::x, "x");
          PSV<Invariant> Q2_psv({0.5, 10.0}, Invariant::Q2, "Q2");
          IPSV           phi_psv({0.0, insane::units::twopi}, "phi");
          auto           DIS_phase_space = make_phase_space(make_diff(x_psv, Q2_psv, phi_psv));
          auto           dis_xs          = make_diff_cross_section(
              DIS_phase_space, [](const InitialState& is, const std::array<double, 3>& vars) {
                static insane::physics::F1F209_SFs sfs;
                double                               x_bj  = vars.at(0);
                double                               Q2    = vars.at(1);
                double                               phi   = vars.at(2);
                double                               M     = is.p2().M();
                double                               alpha = 1. / 137.;
                double                               s     = is.s();
                double                               E0    = (s - M * M) / (2.0 * M);
                double                               y     = Q2 / (x_bj * (s - M * M));
                double                               nu    = y * E0; // nu_func(is,vars);
                // double M  = 0.938;
                double F1 = sfs.F1p(x_bj, Q2);
                double F2 = sfs.F2p(x_bj, Q2);
                double W1 = (1. / M) * F1;
                double W2 = (1. / nu) * F2;
                //// compute the Mott cross section (units = mb):
                ////Double_t hbarc2 = 0.38939129; // (hbar*c)^2 = 0.38939129
                /// mb*GeV^2
                double Ep   = (Q2 / (2.0 * M * x_bj)) * (1.0 - y) / y;
                double th   = 2.0 * std::asin(M * x_bj * y / std::sqrt(Q2 * (1.0 - y)));
                double COS2 = std::cos(th) * std::cos(th);
                double SIN2 = std::sin(th) * std::sin(th);
                double TAN2 = SIN2 / COS2;
                double num  = alpha * alpha * COS2;
                double den  = 4. * E0 * E0 * SIN2;
                // return 1.0/vars.at(1);
                double MottXS = num / den;
                //// compute the full cross section (units = nb/GeV/sr)
                double fullXsec = MottXS * (W2 + 2.0 * TAN2 * W1) * insane::units::hbarc2_gev_nb;
                return fullXsec;
              });
          return dis_xs;
        }

        // using DIS_Func          = decltype(make_DIS_cross_section());
        // using DIS_CrossSection  = physics::DiffCrossSection<DIS_PhaseSpace,DIS_Func>;
        // using DIS_IntegratedCrossSection  = physics::IntegratedCrossSection<DIS_CrossSection>;
      };

    } // namespace xs
  }   // namespace physics
} // namespace insane

#endif

