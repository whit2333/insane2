#ifndef insane_physics_CompositeSFs_HH
#define insane_physics_CompositeSFs_HH 1

#include "insane/structurefunctions/StructureFunctions.h"
#include "insane/base/Physics.h"
#include "insane/structurefunctions/NMC95_SFs.h"
#include "insane/structurefunctions/F1F209_SFs.h"
//#include "MAID_VPACs.h"
#include "insane/structurefunctions/SFsFromVPACs.h"
#include "insane/structurefunctions/SFsFromPDFs.h"
#include "insane/pdfs/Stat2015_UPDFs.h"

namespace insane {
  namespace physics {

    class  CompositeSFs : public StructureFunctions { 
      protected:
        NMC95_SFs   fAsf;
        //SFsFromVPACs<MAID_VPACs> fBsf;
        F1F209_SFs  fBsf;

      public:
        CompositeSFs();
        virtual ~CompositeSFs();
       
        virtual double F1p(double x, double Q2) const ;
        virtual double F2p(double x, double Q2) const ;
        virtual double F1n(double x, double Q2) const ;
        virtual double F2n(double x, double Q2) const ;
        virtual double F1p_TMC(double x, double Q2) const {return F1p(x,Q2);}
        virtual double F2p_TMC(double x, double Q2) const {return F2p(x,Q2);}
        virtual double F1n_TMC(double x, double Q2) const {return F1n(x,Q2);}
        virtual double F2n_TMC(double x, double Q2) const {return F2n(x,Q2);}

        virtual double R(double x, double Q2, Nuclei target, Twist t = Twist::All) const;

        virtual double GetWeight(int iSF, double x, double Q2) const;

      ClassDef(CompositeSFs,1)
    };

  }
}

#endif

