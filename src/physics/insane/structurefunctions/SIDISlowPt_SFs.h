#ifndef insane_physics_SIDISlowPt_SFs_HH
#define insane_physics_SIDISlowPt_SFs_HH

#include "insane/structurefunctions/SIDISStructureFunctions.h"
#include "insane/tmds/DSS_FrFs.h"
#include "insane/tmds/Gaussian_TMDs.h"
#include "insane/pdfs/Stat2015_UPDFs.h"
#include "insane/base/Physics.h"
#include "insane/base/Math.h"

namespace insane::physics {


    /** FUU_T
     */
    template <class TMD, class FFs>
    double F_UU_T(const std::tuple<TMD,FFs>& fs, Nuclei target, Hadron h, double x, double Q2, double z, double Ph_perp, double phi_h) {
      // proton
      double result = 0.0;
      for (const auto& q : LightQuarks) {
        result += insane::integrate::simple(
            [&](double kt) {
              // kt = pt + Phperp/z
              //  pt = kt - Phperp/z
              double pt  = Ph_perp - z * kt;
              //if(pt<0) return 0.0;
              //auto pt = kt - Ph_perp/z;
              auto eq2 = PartonCharge2[q_id(q)];
              auto f1  = std::get<TMD>(fs).f1(q, x, kt, Q2);
              auto D1  = std::get<FFs>(fs).D1(q, h, z, pt, Q2);
              return x * eq2 * f1 * D1 * std::abs(kt) * 2.0*M_PI;
            },
            0, 1.0, 100);
      }
      return result;
    }

    /** Implements the Cahn effect term 
     * See eq 4.20 in http://arXiv.org/abs/hep-ph/0611265v2 2007 SIDIS at small pt
     *
     */
    template <class TMD, class FFs>
    double F_UU_cosphi_h(const std::tuple<TMD,FFs>& fs, Nuclei target, Hadron h, double x, double Q2, double z, double Ph_perp, double phi_h) {
      const double M = insane::masses::M_p/insane::units::GeV;
      const ROOT::Math::Polar2DVector h_hat(1.0, phi_h);
      const double Q = std::sqrt(Q2);

      const double kt2_mean = 0.25 ; // GeV^2
      const double pt2_mean = 0.20 ; // GeV^2
      const double PT2_mean = pt2_mean + z*z*kt2_mean;
      // proton
      double result = 0.0;
      for (const auto& q : LightQuarks) {

        auto eq2 = PartonCharge2[q_id(q)];
        auto f1  = std::get<TMD>(fs).pdfs.Get(q, x,  Q2);
        auto D1  = std::get<FFs>(fs).D(q, h, z, Q2);

        result +=  x * eq2 * f1 * D1 * (kt2_mean*z*Ph_perp*-4.0 /(PT2_mean*Q)*(1.0/(M_PI*PT2_mean))*std::exp(-Ph_perp*Ph_perp/PT2_mean));
        //result += insane::integrate::simple(
        //    [&](double kt) {
        //      return insane::integrate::simple(
        //          [&](double phi_kt) {
        //            // kt = pt + Phperp/z
        //            //  pt = kt - Phperp/z
        //            double pt = Ph_perp - z * kt;
        //            // if(pt<0) return 0.0;
        //            // auto pt = kt - Ph_perp/z;
        //                   h_hat.Dot(ROOT::Math::Polar2DVector(kt, phi_kt));
        //          },
        //          0, 2.0*M_PI, 50);
        //    },
        //    0, 1.0, 10);
      }
      return result;
    }

  /**
   */
  template<class TMDs,class FFs>
  class SIDISlowPt_SFs : public SIDISStructureFunctions {
  public:
    SIDISlowPt_SFs() {}
    virtual ~SIDISlowPt_SFs() {}

    std::tuple<TMDs,FFs> fs;
    //std::tuple<Gaussian_TMDs<Stat2015_UPDFs>,DSS_FrFs> fs;

    virtual double FUU_T(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const {
      return F_UU_T<TMDs, FFs>(fs, Nuclei::p, h, x, Q2, z, Ph_perp, phi_h);
    }
    virtual double FUU_L(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h)        const  { return 0.0;}
    virtual double FUU_cosphi_h(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const  { 
      return F_UU_cosphi_h<TMDs, FFs>(fs, Nuclei::p, h, x, Q2, z, Ph_perp, phi_h);
    }
    virtual double FUU_cos2phi_h(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h)const  { return 0.0;}
    virtual double FLU_sinphi_h(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const  { return 0.0;}
    virtual double FUL_sinphi_h(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const  { return 0.0;}
    virtual double FUL_sin2phi_h(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h)const  { return 0.0;}

    virtual double FLL(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h)          const { return 0.0;}
    virtual double FLL_cosphi_h(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const { return 0.0;}

    ///  \f$ F_{UT,T}^{\sin(\phi_h - \phi_S)} \f$
    virtual double FUT_T_sinphi_h_s(Hadron h, double Q2, double x, double z, double Ph_perp,
                                    double phi_h) const { return 0.0;}

    /// \f$ F_{UT,L}^{\sin(\phi_h - \phi_S)} \f$
    virtual double FUT_L_sinphi_h_s(Hadron h, double Q2, double x, double z, double Ph_perp,
                                    double phi_h) const { return 0.0;}

    /// \f$ F_{UT}^{\sin(\phi_h + \phi_S)} \f$
    virtual double FUT_sinphi_h_s(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const { return 0.0;}

    /// \f$ F_{UT}^{\sin(3\phi_h - \phi_S)} \f$
    virtual double FUT_sin3phi_h_s(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const { return 0.0;}

    /// \f$ F_{UT}^{\sin(\phi_S)} \f$
    virtual double FUT_sinphi_s(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const { return 0.0;}

    /// \f$ F_{UT}^{\sin(2\phi_h - \phi_S)} \f$
    virtual double FUT_sin2phi_h_s(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const { return 0.0;}

    /// \f$ F_{LT}^{\cos(\phi_h - \phi_S)} \f$
    virtual double FLT_cosphi_h_s(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const { return 0.0;}

    /// \f$ F_{LT}^{\cos( \phi_S)} \f$
    virtual double FLT_cosphi_s(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const { return 0.0;}

    /// \f$ F_{LT}^{\cos(2\phi_h - \phi_S)} \f$
    virtual double FLT_cos2phi_h_s(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const { return 0.0;}
    //ClassDef(SIDISlowPt_SFs, 1)
  };
} // namespace insane::physics
#endif


