#ifndef insane_physics_SSFsFromPDFs_HH
#define insane_physics_SSFsFromPDFs_HH

#include "insane/base/Physics.h"
#include "insane/structurefunctions/SpinStructureFunctions.h"
//#include "PDFBase.h"
#include <tuple>
#include <utility>

namespace insane {
  namespace physics {

    /** Spin structure functions.
     *
     */
    template <class... T>
    double g1(const std::tuple<T...>& dfs, const Nuclei target, double x, double Q2) {
      if (target == Nuclei::_3He) { return g1He3(dfs, x, Q2); }
      if (target == Nuclei::d) { return g1d(dfs, x, Q2); }
      return (std::get<T>(dfs).g1(x, Q2, target) + ...);
    }

    template <class... T>
    double g2(const std::tuple<T...>& dfs, const Nuclei target, double x, double Q2) {
      if (target == Nuclei::_3He) { return g2He3(dfs, x, Q2); }
      if (target == Nuclei::d) { return g2d(dfs, x, Q2); }
      return (std::get<T>(dfs).g2(x, Q2, target) + ...);
    }

    /** Target Mass Corrections.
     */
    template <class... T>
    double g1_TMC(const std::tuple<T...>& dfs, const Nuclei target, double x, double Q2) {
      if (target == Nuclei::_3He) { return g1He3_TMC(dfs, x, Q2); }
      if (target == Nuclei::d) { return g1d_TMC(dfs, x, Q2); }
      return (std::get<T>(dfs).g1_TMC(x, Q2, target) + ...);
    }

    template <class... T>
    double g2_TMC(const std::tuple<T...>& dfs, const Nuclei target, double x, double Q2) {
      if (target == Nuclei::_3He) { return g2He3_TMC(dfs, x, Q2); }
      if (target == Nuclei::d) { return g2d_TMC(dfs, x, Q2); }
      return (std::get<T>(dfs).g2_TMC(x, Q2, target) + ...);
    }

    template <class... T>
    double g1He3(const std::tuple<T...>& dfs, double x, double Q2) {
      auto g1n = g1(dfs, Nuclei::n, x, Q2);
      auto g1p = g1(dfs, Nuclei::p, x, Q2);
      // need to add more here. Non-nucleonic components, shadowing/anti-shadowing.
      const double Pn = 0.879;
      const double Pp = -0.021;
      return Pn*g1n + Pp*g1p;
    }

    template <class... T>
    double g1He3_TMC(const std::tuple<T...>& dfs, double x, double Q2) {
      auto g1n = g1_TMC(dfs, Nuclei::n, x, Q2);
      auto g1p = g1_TMC(dfs, Nuclei::p, x, Q2);
      // need to add more here. Non-nucleonic components, shadowing/anti-shadowing.
      const double Pn = 0.879;
      const double Pp = -0.021;
      return Pn*g1n + Pp*g1p;
    }
    template <class... T>
    double g2He3(const std::tuple<T...>& dfs, double x, double Q2) {
      auto g1n = g1(dfs, Nuclei::n, x, Q2);
      auto g1p = g1(dfs, Nuclei::p, x, Q2);
      // need to add more here. Non-nucleonic components, shadowing/anti-shadowing.
      const double Pn = 0.879;
      const double Pp = -0.021;
      return Pn*g1n + Pp*g1p;
    }

    template <class... T>
    double g2He3_TMC(const std::tuple<T...>& dfs, double x, double Q2) {
      auto g1n = g1_TMC(dfs, Nuclei::n, x, Q2);
      auto g1p = g1_TMC(dfs, Nuclei::p, x, Q2);
      // need to add more here. Non-nucleonic components, shadowing/anti-shadowing.
      const double Pn = 0.879;
      const double Pp = -0.021;
      return Pn*g1n + Pp*g1p;
    }


    template <class... T>
    double g1d(const std::tuple<T...>& dfs, double x, double Q2) {
      auto g1n = g1(dfs, Nuclei::n, x, Q2);
      auto g1p = g1(dfs, Nuclei::p, x, Q2);
      // need to add more here. Non-nucleonic components, shadowing/anti-shadowing.
      const double omega_D = 0.05;
      // Should use F1d/F1p+F1n instead of 2
      return (g1n + g1p)*(1.5-omega_D)/2.0;
    }
    template <class... T>
    double g1d_TMC(const std::tuple<T...>& dfs, double x, double Q2) {
      auto g1n = g1_TMC(dfs, Nuclei::n, x, Q2);
      auto g1p = g1_TMC(dfs, Nuclei::p, x, Q2);
      // need to add more here. Non-nucleonic components, shadowing/anti-shadowing.
      const double omega_D = 0.05;
      // Should use F1d/F1p+F1n instead of 2
      return (g1n + g1p)*(1.5-omega_D)/2.0;
    }
    template <class... T>
    double g2d(const std::tuple<T...>& dfs, double x, double Q2) {
      auto g1n = g1(dfs, Nuclei::n, x, Q2);
      auto g1p = g1(dfs, Nuclei::p, x, Q2);
      // need to add more here. Non-nucleonic components, shadowing/anti-shadowing.
      const double omega_D = 0.05;
      // Should use F1d/F1p+F1n instead of 2
      return (g1n + g1p)*(1.5-omega_D)/2.0;
    }
    template <class... T>
    double g2d_TMC(const std::tuple<T...>& dfs, double x, double Q2) {
      auto g1n = g1_TMC(dfs, Nuclei::n, x, Q2);
      auto g1p = g1_TMC(dfs, Nuclei::p, x, Q2);
      // need to add more here. Non-nucleonic components, shadowing/anti-shadowing.
      const double omega_D = 0.05;
      // Should use F1d/F1p+F1n instead of 2
      return (g1n + g1p)*(1.5-omega_D)/2.0;
    }

    /** SpinStructureFunction implementation for polarized PDFs.
     *  Although we don't need this class to calculate the SSFs, it provides
     *  some usefulness when using with other structure functions.
     *
     * \ingroup StructureFunctions
     */
    template <class PDF, class... T>
    class SSFsFromPDFs : public SpinStructureFunctions {

    public:
      std::tuple<PDF, T...> fDFs;

    public:
      SSFsFromPDFs() {}
      virtual ~SSFsFromPDFs() {}

      PDF*                         GetPDF_ptr() { return &std::get<0>(fDFs); }
      const PDF&                   GetPDFs() const { return std::get<0>(fDFs); }
      const std::tuple<PDF, T...>& GetDFs() const { return fDFs; }

      virtual double Calculate(double x, double Q2, std::tuple<SF, Nuclei> sf, Twist t = Twist::All,
                               OPELimit l = OPELimit::MassiveTarget) const;
      virtual double Uncertainties(double x, double Q2, std::tuple<SF, Nuclei> sf,
                                   Twist    t = Twist::All,
                                   OPELimit l = OPELimit::MassiveTarget) const;

      /** \name  Spin structure functions in massless target limit.
       * @{
       */
      virtual double g1p(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g1, Nuclei::p), Twist::All, OPELimit::Massless);
      }
      virtual double g2p(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g2, Nuclei::p), Twist::All, OPELimit::Massless);
      }
      virtual double g1n(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g1, Nuclei::n), Twist::All, OPELimit::Massless);
      }
      virtual double g2n(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g2, Nuclei::n), Twist::All, OPELimit::Massless);
      }
      virtual double g1d(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g1, Nuclei::d), Twist::All, OPELimit::Massless);
      }
      virtual double g2d(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g2, Nuclei::d), Twist::All, OPELimit::Massless);
      }
      virtual double g1He3(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g1, Nuclei::_3He), Twist::All, OPELimit::Massless);
      }
      virtual double g2He3(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g2, Nuclei::_3He), Twist::All, OPELimit::Massless);
      }
      //@}

      /** \name  Spin structure functions with TMCs
       * @{
       */
      virtual double g1p_TMC(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g1, Nuclei::p));
      }
      virtual double g2p_TMC(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g2, Nuclei::p));
      }
      virtual double g1n_TMC(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g1, Nuclei::n));
      }
      virtual double g2n_TMC(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g2, Nuclei::n));
      }
      virtual double g1d_TMC(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g1, Nuclei::d));
      }
      virtual double g2d_TMC(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g2, Nuclei::d));
      }
      virtual double g1He3_TMC(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g1, Nuclei::_3He));
      }
      virtual double g2He3_TMC(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::g2, Nuclei::_3He));
      }
      //@}


      ClassDef(SSFsFromPDFs, 1)
    };

  } // namespace physics
} // namespace insane

#include "SSFsFromPDFs.hxx"

#endif
