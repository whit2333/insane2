#ifndef insane_physics_SIDISStructureFunctions_HH
#define insane_physics_SIDISStructureFunctions_HH

#include "insane/structurefunctions/StructureFunctions.h"
#include "insane/base/Physics.h"

namespace insane::physics {

  /**
   */
  class SIDISStructureFunctions : public StructureFunctionBase {
  public:
    SIDISStructureFunctions(): StructureFunctionBase("SIDIS Structure Functions") {}
    virtual ~SIDISStructureFunctions() {}

    virtual double FUU_T(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h)        const = 0;
    virtual double FUU_L(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h)        const = 0;
    virtual double FUU_cosphi_h(Hadron h, double Q1, double x, double z, double Ph_perp, double phi_h) const = 0;
    virtual double FUU_cos2phi_h(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h)const = 0;
    virtual double FLU_sinphi_h(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const = 0;
    virtual double FUL_sinphi_h(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const = 0;
    virtual double FUL_sin2phi_h(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h)const = 0;

    virtual double FLL(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h)         const  = 0;
    virtual double FLL_cosphi_h(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h)const  = 0;

    ///  \f$ F_{UT,T}^{\sin(\phi_h - \phi_S)} \f$
    virtual double FUT_T_sinphi_h_s(Hadron h, double Q2, double x, double z, double Ph_perp,
                                    double phi_h)const  = 0;

    /// \f$ F_{UT,L}^{\sin(\phi_h - \phi_S)} \f$
    virtual double FUT_L_sinphi_h_s(Hadron h, double Q2, double x, double z, double Ph_perp,
                                    double phi_h) const = 0;

    /// \f$ F_{UT}^{\sin(\phi_h + \phi_S)} \f$
    virtual double FUT_sinphi_h_s(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const = 0;

    /// \f$ F_{UT}^{\sin(3\phi_h - \phi_S)} \f$
    virtual double FUT_sin3phi_h_s(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const = 0;

    /// \f$ F_{UT}^{\sin(\phi_S)} \f$
    virtual double FUT_sinphi_s(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const = 0;

    /// \f$ F_{UT}^{\sin(2\phi_h - \phi_S)} \f$
    virtual double FUT_sin2phi_h_s(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const = 0;

    /// \f$ F_{LT}^{\cos(\phi_h - \phi_S)} \f$
    virtual double FLT_cosphi_h_s(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const = 0;

    /// \f$ F_{LT}^{\cos( \phi_S)} \f$
    virtual double FLT_cosphi_s(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const = 0;

    /// \f$ F_{LT}^{\cos(2\phi_h - \phi_S)} \f$
    virtual double FLT_cos2phi_h_s(Hadron h, double Q2, double x, double z, double Ph_perp, double phi_h) const = 0;
  };
} // namespace insane::physics
#endif
