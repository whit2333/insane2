#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;


#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;

//#pragma link C++ global insane::physics::fgStructureFunctions+;
//#pragma link C++ global insane::physics::fgSpinStructureFunctions+;
//#pragma link C++ global insane::physics::fgPolarizedStructureFunctions+; // deprecated

#pragma link C++ class insane::physics::MeasuredSpinStructureFunctions+;

#pragma link C++ class insane::physics::StructureFunctionBase+;
#pragma link C++ class insane::physics::StructureFunctions+;
#pragma link C++ class insane::physics::StructureFunctions2+;

#pragma link C++ class insane::physics::SpinStructureFunctions+;
#pragma link C++ class insane::physics::PolarizedStructureFunctions+;

#pragma link C++ class insane::physics::CompositeStructureFunctions+;
#pragma link C++ class insane::physics::CompositePolarizedStructureFunctions+;
#pragma link C++ class insane::physics::LowQ2StructureFunctions+;
#pragma link C++ class insane::physics::LowQ2PolarizedStructureFunctions+;

//#pragma link C++ class insane::physics::StructureFunctionsFromVPCSs+;

#pragma link C++ class insane::physics::F1F209StructureFunctions+;
#pragma link C++ class insane::physics::F1F209QuasiElasticStructureFunctions+;
#pragma link C++ class insane::physics::NMC95StructureFunctions+;

#pragma link C++ class std::vector<insane::physics::StructureFunctions*>+;
#pragma link C++ class std::vector<insane::physics::PolarizedStructureFunctions*>+;
#pragma link C++ class std::vector<insane::physics::StructureFunctionsFromPDFs*>+;
#pragma link C++ class std::vector<insane::physics::PolarizedStructureFunctionsFromPDFs*>+;

#pragma link C++ class insane::physics::PolarizedStructureFunctionsFromVCSAs+;
#pragma link C++ class insane::physics::PolSFsFromComptonAsymmetries+;
//#pragma link C++ class LHAPDFStructureFunctions+;
#pragma link C++ class insane::physics::StructureFunctionsFromPDFs+;
#pragma link C++ class insane::physics::BETAG4StructureFunctions+;

#pragma link C++ class insane::physics::PolarizedStructureFunctionsFromPDFs+;

#pragma link C++ class insane::physics::CompositeSFs+;
#pragma link C++ class insane::physics::CompositeSSFs+;

#pragma link C++ class insane::physics::F1F209_SFs+;
#pragma link C++ class insane::physics::NMC95_SFs+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs,insane::physics::JAM_T4DFs>+;

#pragma link C++ class insane::physics::SFsFromPDFs< insane::physics::CTEQ10_UPDFs>+;

#pragma link C++ class insane::physics::SFsFromPDFs< insane::physics::Stat2015_UPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::Stat2015_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::Stat2015_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::AAC08_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::AAC08_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::BB_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::BB_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::BBS_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::BBS_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::DSSV_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::DSSV_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::LSS2010_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::LSS2010_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::LSS2006_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::LSS2006_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::DNS2005_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::DNS2005_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::GS_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::GS_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::ABDY_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::ABDY_PPDFs,insane::physics::JAM_T3DFs>+;

//#pragma link C++ class insane::physics::SFsFromPDFs< std::tuple<insane::physics::Stat2015_UPDFs>>+;
//#pragma link C++ class insane::physics::SSFsFromPDFs<std::tuple<insane::physics::Stat2015_PPDFs>>+;

//#pragma link C++ class insane::physics::SFsFromVPACs<insane::physics::MAID_VPACs>+;
//#pragma link C++ class insane::physics::SSFsFromVPACs<insane::physics::MAID_VPACs>+;

#endif

