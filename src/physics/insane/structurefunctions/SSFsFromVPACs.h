#ifndef insane_physics_SSFsFromVPACs_HH
#define insane_physics_SSFsFromVPACs_HH 1

#include "insane/structurefunctions/SpinStructureFunctions.h"
#include "insane/xsections/VirtualPhotoAbsorptionCrossSections.h"
#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "TMath.h"

namespace insane {
  namespace physics {

    /** Spin structure functions from virtual photo-absorption cross sections
     *
     * \ingroup StructureFunctions
     */
    template<class T>
    class SSFsFromVPACs : public SpinStructureFunctions {

      protected:
        T  fVPACs;
        double   f4piAlphaOverM = 4.0*TMath::Pi()*TMath::Pi()*insane::constants::fine_structure_const/(M_p/GeV);

      public:
        SSFsFromVPACs(){}
        virtual ~SSFsFromVPACs(){}

        virtual double g1p(   double x, double Q2) const
        {
          fVPACs.CalculateProton(x,Q2);
          double M     = M_p/GeV;
          double K     = insane::kine::K_Hand(x,Q2);
          double gam2  = 4.0*x*x*M*M/Q2;
          double gam   = TMath::Sqrt(gam2);
          double k     = f4piAlphaOverM/K;
          double sLTp  = fVPACs.Sig_LTp();
          double sTTp  = fVPACs.Sig_LTp();
          double g1    = (gam*sLTp + 2.0*sTTp)/(k + gam2*k);
          return g1;
        }

        virtual double g1n(   double x, double Q2) const
        {
          fVPACs.CalculateNeutron(x,Q2);
          double M     = M_p/GeV;
          double K     = insane::kine::K_Hand(x,Q2);
          double gam2  = 4.0*x*x*M*M/Q2;
          double gam   = TMath::Sqrt(gam2);
          double k     = f4piAlphaOverM/K;
          double sLTp  = fVPACs.Sig_LTp();
          double sTTp  = fVPACs.Sig_LTp();
          double g1    = (gam*sLTp + 2.0*sTTp)/(k + gam2*k);
            return g1;
        }

        virtual double g2p(   double x, double Q2) const
        {
          fVPACs.CalculateProton(x,Q2);
          double M     = M_p/GeV;
          double K     = insane::kine::K_Hand(x,Q2);
          double gam2  = 4.0*x*x*M*M/Q2;
          double gam   = TMath::Sqrt(gam2);
          double k     = f4piAlphaOverM/K;
          double sLTp  = fVPACs.Sig_LTp();
          double sTTp  = fVPACs.Sig_LTp();
          double g2    = (sLTp - 2.0*gam*sTTp)/(gam*k + gam2*gam*k);
          return g2;
        }

        virtual double g2n(   double x, double Q2) const
        {
          fVPACs.CalculateNeutron(x,Q2);
          double M     = M_p/GeV;
          double K     = insane::kine::K_Hand(x,Q2);
          double gam2  = 4.0*x*x*M*M/Q2;
          double gam   = TMath::Sqrt(gam2);
          double k     = f4piAlphaOverM/K;
          double sLTp  = fVPACs.Sig_LTp();
          double sTTp  = fVPACs.Sig_LTp();
          double g2    = (sLTp - 2.0*gam*sTTp)/(gam*k + gam2*gam*k);
          return g2;
        }
        //virtual double g1d(   double x, double Q2);
        //virtual double g1He3( double x, double Q2);
        //virtual double g2d(   double x, double Q2);
        //virtual double g2He3( double x, double Q2);
        virtual double g1p_TMC(double x, double Q2) const { return g1p(x,Q2); }
        virtual double g2p_TMC(double x, double Q2) const { return g2p(x,Q2); }
        virtual double g1n_TMC(double x, double Q2) const { return g1n(x,Q2); }
        virtual double g2n_TMC(double x, double Q2) const { return g2n(x,Q2); }

        ClassDef(SSFsFromVPACs,1)
    };
  }
}


#endif
