#ifndef insane_physics_StructureFunctions_HH
#define insane_physics_StructureFunctions_HH

#include "insane/kinematics/KinematicFunctions.h"
#include "insane/base/Physics.h"
#include "TAttFill.h"
#include "TAttLine.h"
#include "TAttMarker.h"
#include "TNamed.h"
#include <string>

namespace insane {

  using namespace units;

  namespace physics {

    /** \addtogroup StructureFunctions Structure Functions
     *
     *  \ingroup physics
     */

    /** \name Ratio of longitudinal to transverse cross sections.
     *
     * \ingroup DIS
     * \ingroup StructureFunctions
     *  @{
     */
    /** Parameterization of the world data of R = sig_L/sig_T .
     *  By the SLAC E143 Collaboration
     */
    double R1998(double x, double Q2);

    /** Uncertatinty of parameterization of the world data of R = sig_L/sig_T .
     *  By the SLAC E143 Collaboration
     */
    double RErr1998(double x, double Q2);

    double Ra1998(double x, double Q2);
    double Rb1998(double x, double Q2);
    double Rc1998(double x, double Q2);
    double Theta1998(double x, double Q2);

    //@}

    /** Structure functions enum class.
     * \ingroup StructureFunctions
     */
    enum class StructureFunctionType {
      kF1p, kF1n, kF1d, kF1He3, kF1_2H, kF1_3He, kF1_3H, kF1_4He,
      kF2p, kF2n, kF2d, kF2He3, kF2_2H, kF2_3He, kF2_3H, kF2_4He,
    } ;

    /** Spin Structure functions enum class.
     * \ingroup StructureFunctions
     */
    enum class SpinStructureFunctionType {
      kg1p  , kg1n  , kg1d  , kg1He3  , kg1_2H, kg1_3He, kg1_3H, kg1_4He,
      kg2p  , kg2n  , kg2d  , kg2He3  , kg2_2H, kg2_3He, kg2_3H, kg2_4He,
    } ;

    /** EMC Effect
     * \ingroup StructureFunctions
     */
    double EMC_Effect(double* x, double* p);

    /** EMC Effect
     * \ingroup StructureFunctions
     */
    double EMC_Effect(double x, double A);

    /** Base class for Structure Functions.
     *
     *  For calculating the nucleon structure functions.
     *
     * \ingroup StructureFunctions
     */
    class StructureFunctionBase {
    protected:
      std::string fLabel;
      std::string fComments;

    public:
      StructureFunctionBase();
      StructureFunctionBase(const std::string& l, const std::string& c);
      StructureFunctionBase(const std::string& l): StructureFunctionBase(l,""){}
      virtual ~StructureFunctionBase();

      void        SetLabel(const char* l) { fLabel = l; }
      const char* GetLabel() const { return fLabel.c_str(); }

      virtual void Print(Option_t* opt = "") const;

      ClassDef(StructureFunctionBase, 1)
    };



    /**  Structure Functions interface.
     *
     *  At this level an implementation not involving the pdfs,
     *  e.g., an empircal fit, will ignore the twist argument.
     *  The implementation SSFsFromPDFs will not ignore the twist argument.
     *
     * \f$ \lim_{Bjorken} M W_1(p.q,Q^2) = F_1(x)  \f$
     *
     * \f$ \lim_{Bjorken} \nu W_2(p.q,Q^2) = F_2(x)   \f$
     *
     *
     * \ingroup StructureFunctions
     */
    class StructureFunctions : public StructureFunctionBase {
    protected:

      // Storage of the x and Q2 values last used to compute the structure
      // function indexed by enum class insane::physics::SF
      mutable std::array<std::array<double, NStructureFunctions>, NNuclei>
          fx_values;
      mutable std::array<std::array<double, NStructureFunctions>, NNuclei>
          fQ2_values;
      mutable std::array<std::array<double, NStructureFunctions>, NNuclei>
          fValues;
      mutable std::array<std::array<double, NStructureFunctions>, NNuclei>
          fUncertainties;

    public:
      StructureFunctions();
      virtual ~StructureFunctions();

      double GetXBjorken(std::tuple<SF, Nuclei> sf) const;
      double GetQSquared(std::tuple<SF, Nuclei> sf) const;

      bool IsComputed(std::tuple<SF, Nuclei> sf, double x, double Q2) const;

      void Reset();

      /** Calculate and return distribution value.
       * Returns the current value for flavor f but checks that the
       * distributions have already been calculated at (x,Q2).
       * It uses IsComputed(x,Q2) to do this check.
       */
      virtual double Get(double x, double Q2, std::tuple<SF, Nuclei> sf,
                         Twist    t = Twist::All,
                         OPELimit l = OPELimit::MassiveTarget) const;

      /** Get current distribution value.
       * Note: this method should only be used to get the stored values
       * after Calculate(x,Q2) or Get(f,x,Q2) has been used at the desired
       * (x,Q2).
       */
      double Get(std::tuple<SF, Nuclei> sf) const;

      /** Virtual method should get all values of SFs.
       * This also sets internal values of fx and fQsquared for use by
       * IsComputed()
       */
      virtual double Calculate(double x, double Q2, std::tuple<SF, Nuclei> sf,
                               Twist    t = Twist::All,
                               OPELimit l = OPELimit::MassiveTarget) const {
        return 0.0;
      }
      virtual double Uncertainties(double x, double Q2,
                                   std::tuple<SF, Nuclei> sf,
                                   Twist                  t = Twist::All,
                                   OPELimit l = OPELimit::MassiveTarget) const {
        return 0.0;
      }

      virtual double F1(double x, double Q2, Nuclei target,
                        Twist t = Twist::All) const;
      virtual double F2(double x, double Q2, Nuclei target,
                        Twist t = Twist::All) const;
      virtual double F1_TMC(double x, double Q2, Nuclei target,
                            Twist t = Twist::All) const;
      virtual double F2_TMC(double x, double Q2, Nuclei target,
                            Twist t = Twist::All) const;

      virtual double F1p(double x, double Q2) const     = 0;
      virtual double F2p(double x, double Q2) const     = 0;
      virtual double F1n(double x, double Q2) const     = 0;
      virtual double F2n(double x, double Q2) const     = 0;
      virtual double F1p_TMC(double x, double Q2) const = 0;
      virtual double F2p_TMC(double x, double Q2) const = 0;
      virtual double F1n_TMC(double x, double Q2) const = 0;
      virtual double F2n_TMC(double x, double Q2) const = 0;

      virtual double F1d(double x, double Q2)  const { return 0.0; }
      virtual double F2d(double x, double Q2)  const { return 0.0; }
      virtual double F1He3(double x, double Q2) const { return 0.0; }
      virtual double F2He3(double x, double Q2) const { return 0.0; }

      virtual double F1p_Error(double x, double Q2) const { return 0.0; }
      virtual double F2p_Error(double x, double Q2) const { return 0.0; }
      virtual double F1n_Error(double x, double Q2) const { return 0.0; }
      virtual double F2n_Error(double x, double Q2) const { return 0.0; }
      virtual double F1d_Error(double x, double Q2) const { return 0.0; }
      virtual double F2d_Error(double x, double Q2) const { return 0.0; }
      virtual double F1He3_Error(double x, double Q2) const { return 0.0; }
      virtual double F2He3_Error(double x, double Q2) const { return 0.0; }


      /** Longiutdinal structure function.
       *
       * \f$ F_L = \rho^2 F_2 - 2x F_1) = 2x F_1 R \f$
       * \f$ \rho = \sqrt{1+ \frac{4M^2x^2}{Q^2}} \f$
       */
      virtual double FL(double x, double Q2, Nuclei target,
                        Twist t = Twist::All) const;

      /** Ratio of longitudinal to transverse cross sections.
       * \f$ R = \sigma_L/\sigma_T \f$
       */
      virtual double R(double x, double Q2, Nuclei target,
                       Twist t = Twist::All) const;

      // Rnp = F2n/F2p
      // virtual double Rnp(double x, double Q2);
      // virtual double Rnp_Error(double x, double Q2);

      virtual double xF1p(double x, double Q2) const;
      virtual double xF2p(double x, double Q2) const;
      virtual double xF1n(double x, double Q2) const;
      virtual double xF2n(double x, double Q2) const;
      virtual double xF1d(double x, double Q2) const;
      virtual double xF2d(double x, double Q2) const;
      virtual double xF1He3(double x, double Q2) const;
      virtual double xF2He3(double x, double Q2) const;

      ClassDef(StructureFunctions, 2)
    };

  } // namespace physics
} // namespace insane

#endif
