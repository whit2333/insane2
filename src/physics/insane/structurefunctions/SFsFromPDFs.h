#ifndef insane_physics_SFsFromPDFs_HH
#define insane_physics_SFsFromPDFs_HH

#include "insane/pdfs/PDFBase.h"
#include "insane/base/Physics.h"
#include "insane/structurefunctions/StructureFunctions.h"
#include <tuple>

namespace insane {
  namespace physics {

    // template<class S, class T = Twist, class L = OPELimit>
    //  struct SFCondition{ };

    // template<class...DF>
    // double(const std::Tuple<DF...>, Nuclei, double, double)
    // SSF(SFCondition c,  {
    // }
    //  template<class S, class T = Twist::All, class L = OPELimit::Massless>
    //  double(const std::Tuple<DF...>, Nuclei, double, double)
    //  SSF(S s, T t, L l);
    // template<class...DF>

    //#pragma GCC diagnostic push
    //#pragma GCC diagnostic ignored "-Wc++17-extensions"

    /** structure functions.
     */
    template <class... T>
    double F1(const std::tuple<T...>& dfs, Nuclei target, double x, double Q2) {
      return (std::get<T>(dfs).F1(x, Q2, target) + ...);
    }

    template <class... T>
    double F2(const std::tuple<T...>& dfs, Nuclei target, double x, double Q2) {
      return (std::get<T>(dfs).F2(x, Q2, target) + ...);
    }

    /** Target Mass Effects included
     */
    template <class... T>
    double F1_TMC(const std::tuple<T...>& dfs, Nuclei target, double x,
                  double Q2) {
      return (std::get<T>(dfs).F1_TMC(x, Q2, target) + ...);
    }
    template <class... T>
    double F2_TMC(const std::tuple<T...>& dfs, Nuclei target, double x,
                  double Q2) {
      return (std::get<T>(dfs).F2_TMC(x, Q2, target) + ...);
    }

    template <class T, class HT>
    double F1(const T& pdfs, const HT& ht, Nuclei target, double x, double Q2) {
      return (pdfs.F1(target, x, Q2) + ht.F1(target, x, Q2));
    }
    template <class T>
    double F1(const T& pdfs, Nuclei target, double x, double Q2) {
      return (pdfs.F1(target, x, Q2));
    }
    template <class T, class HT>
    double F2(const T& pdfs, const HT& ht, Nuclei target, double x, double Q2) {
      return (pdfs.F2(target, x, Q2) + ht.F2(target, x, Q2));
    }
    template <class T>
    double F2(const T& pdfs, Nuclei target, double x, double Q2) {
      return (pdfs.F2(target, x, Q2));
    }
    //______________________________________________________________________________

    //#pragma GCC diagnostic pop
    // template<class T>
    // double R(const T& pdfs, Nuclei target, double x, double Q2){
    //  // R=sigma_L/sigma_T
    //  double res = pdfs.F2p(x,Q2)/(2.0*x*pdfs.F1p(x, Q2));
    //  res = res * (1 + 4.0*(M_p/GeV)*(M_p/GeV)*x*x/Q2)-1.0;
    //  if(TMath::IsNaN(res) ) return 0.0;
    //  return res;
    //}

    /** StructureFunction implementation for polarized PDFs.
     *  Although we don't need this class to calculate the SSFs, it provides
     *  some usefulness when using with other structure functions.
     *
     * \ingroup StructureFunctions
     */
    template <class PDF, class... T>
    class SFsFromPDFs : public StructureFunctions {
    protected:
      std::tuple<PDF, T...> fDFs;
      // PDF  fPDFs;
      // HT   fHTs;

    public:
      SFsFromPDFs() {}
      virtual ~SFsFromPDFs() {}

      const PDF&                   GetPDFs() const { return std::get<0>(fDFs); }
      const std::tuple<PDF, T...>& GetDFs() const { return fDFs; }

      virtual double Calculate(double x, double Q2, std::tuple<SF, Nuclei> sf,
                               Twist    t = Twist::All,
                               OPELimit l = OPELimit::MassiveTarget) const;
      virtual double Uncertainties(double x, double Q2,
                                   std::tuple<SF, Nuclei> sf,
                                   Twist                  t = Twist::All,
                                   OPELimit l = OPELimit::MassiveTarget) const;

      virtual double F1(double x, double Q2, Nuclei target,
                        Twist t = Twist::All) const {
        return Calculate(x, Q2, std::make_tuple(SF::F1, target), t,
                         OPELimit::MasslessTarget);
      }
      virtual double F2(double x, double Q2, Nuclei target,
                        Twist t = Twist::All) const {
        return Calculate(x, Q2, std::make_tuple(SF::F2, target), t,
                         OPELimit::MasslessTarget);
      }
      virtual double F1_TMC(double x, double Q2, Nuclei target,
                            Twist t = Twist::All) const {
        return Calculate(x, Q2, std::make_tuple(SF::F1, target), t,
                         OPELimit::MassiveTarget);
      }
      virtual double F2_TMC(double x, double Q2, Nuclei target,
                            Twist t = Twist::All) const {
        return Calculate(x, Q2, std::make_tuple(SF::F2, target), t,
                         OPELimit::MassiveTarget);
      }

      // virtual double F2_WW(Nuclei target, double x, double Q2) const { return
      // Calculate(x,Q2,std::make_tuple(SF::F2_WW,target)); } virtual double
      // F1_BT(Nuclei target, double x, double Q2) const { return
      // Calculate(x,Q2,std::make_tuple(SF::F2_WW,target)); }

      virtual double F1p(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::F2, Nuclei::p), Twist::All,
                         OPELimit::Massless);
      }
      virtual double F2p(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::F2, Nuclei::p), Twist::All,
                         OPELimit::Massless);
      }
      virtual double F1n(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::F2, Nuclei::n), Twist::All,
                         OPELimit::Massless);
      }
      virtual double F2n(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::F2, Nuclei::n), Twist::All,
                         OPELimit::Massless);
      }
      virtual double F1p_TMC(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::F2, Nuclei::p));
      }
      virtual double F2p_TMC(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::F2, Nuclei::p));
      }
      virtual double F1n_TMC(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::F2, Nuclei::n));
      }
      virtual double F2n_TMC(double x, double Q2) const {
        return Calculate(x, Q2, std::make_tuple(SF::F2, Nuclei::n));
      }

      virtual double R(double x, double Q2, Nuclei target, Twist t = Twist::All) const {
        auto F1 = this->F1(x, Q2, target,t);
        auto F2 = this->F2(x, Q2, target,t);
        return insane::physics::R(F1,F2,x,Q2);
      }

      virtual double R(double x, double Q2) const {
        return this->R(x,Q2,insane::physics::Nuclei::p);
      }

      ClassDef(SFsFromPDFs, 1)
    };

  } // namespace physics
} // namespace insane

#include "SFsFromPDFs.hxx"

#endif

