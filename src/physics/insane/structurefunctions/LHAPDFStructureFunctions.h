#ifndef LHAPDFSTRUCTUREFUNCTIONS_HH
#define LHAPDFSTRUCTUREFUNCTIONS_HH 3 

#include "TNamed.h"
#include "TMath.h"
#include "TString.h"
#include "InSANEinsane/base/FortranWrappers.h"
#include "InSANEStructureFunctions.h"
#include "InSANEPartonDistributionFunctions.h"

#ifndef __CINT__
#include "LHAPDF/LHAPDF.h"
#else
namespace LHAPDF {
   enum SetType { EVOLVE = 0, LHPDF = 0, INTERPOLATE = 1, LHGRID = 1};
}
#endif
namespace insane {
namespace physics {

/** Concrete class for structure functions using LHAPDF
 *
 *  Default data set is MRST2004nlo
 *
 * \ingroup structurefunctions
 */
class LHAPDFStructureFunctions : public InSANEStructureFunctions {

	public:
		LHAPDFStructureFunctions(); 
		virtual ~LHAPDFStructureFunctions();

		/** wrapper to set type = 0 = LHAPDF::LHGRID
		 *   type = 1 (or nonzero) = LHAPDF::EVOLVE
		 */
		void SetPDFType(const char * pdfset, Int_t type = 0) ;

		/** Change LHAPDF data set */
		void SetPDFDataSet(const char * pdfset, LHAPDF::SetType type = LHAPDF::LHGRID, Int_t subset = 0) ;

		virtual double F1p(double x, double Qsq);
		virtual double F2p(double x, double Qsq);
		virtual double F1n(double x, double Qsq);
		virtual double F2n(double x, double Qsq);
		virtual double F1d(double x, double Qsq);
		virtual double F2d(double x, double Qsq);
		virtual double F1He3(double x, double Qsq);
		virtual double F2He3(double x, double Qsq);

		Int_t fSubset;

		ClassDef(LHAPDFStructureFunctions,3)
};

}}
#endif
