#ifndef insane_physics_SFsFromVPACs_HH
#define insane_physics_SFsFromVPACs_HH

#include "insane/structurefunctions/StructureFunctions.h"
#include "insane/xsections/VirtualPhotoAbsorptionCrossSections.h"
#include "insane/base/PhysicalConstants.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "TMath.h"

namespace insane {
  namespace physics {

    using namespace units;

    /** Structure functions from virtual photo-absorption cross sections
     *
     * \ingroup StructureFunctions
     */
    template<class T>
    class SFsFromVPACs : public StructureFunctions {

      protected:
        T fVPACs;
        double   f4piAlphaOverM = 4.0*TMath::Pi()*TMath::Pi()*insane::constants::fine_structure_const/(M_p/GeV);

      public:
        SFsFromVPACs(){ };
        virtual ~SFsFromVPACs(){ }

        virtual double F1p(   double x, double Q2) const
        {
          fVPACs.CalculateProton(x,Q2);
          double M     = M_p/GeV;
          double K     = insane::kine::K_Hand(x,Q2);
          double sig_T = fVPACs.Sig_T();
          double res   = sig_T*K/f4piAlphaOverM;
          return res;
        }

        virtual double F2p(   double x, double Q2) const
        {
          fVPACs.CalculateProton(x,Q2);
          double M     = M_p/GeV;
          double nu    = Q2/(2.0*M*x);
          double K     = insane::kine::K_Hand(x,Q2);
          double B1    = (f4piAlphaOverM/(K*nu))*(1.0+nu*nu/Q2) ;
          double sig_T = fVPACs.Sig_T();
          double sig_L = fVPACs.Sig_L();
          double res   = (sig_L + sig_T)/B1;
          return res;
        }

        virtual double F1n(   double x, double Q2) const
        {
          fVPACs.CalculateNeutron(x,Q2);
          double M     = M_p/GeV;
          double K     = insane::kine::K_Hand(x,Q2);
          double sig_T = fVPACs.Sig_T();
          double res   = sig_T*K/f4piAlphaOverM;
          return res;
        }

        virtual double F2n(   double x, double Q2) const
        {
          fVPACs.CalculateNeutron(x,Q2);
          double M     = M_p/GeV;
          double nu    = Q2/(2.0*M*x);
          double K     = insane::kine::K_Hand(x,Q2);
          double B1    = (f4piAlphaOverM/(K*nu))*(1.0+nu*nu/Q2) ;
          double sig_T = fVPACs.Sig_T();
          double sig_L = fVPACs.Sig_L();
          double res   = (sig_L + sig_T)/B1;
          return res;
        }
        virtual double F1p_TMC(double x, double Q2) const {return F1p(x,Q2);}
        virtual double F2p_TMC(double x, double Q2) const {return F2p(x,Q2);}
        virtual double F1n_TMC(double x, double Q2) const {return F1n(x,Q2);}
        virtual double F2n_TMC(double x, double Q2) const {return F2n(x,Q2);}
        //virtual double F1d(   double x, double Q2);
        //virtual double F2d(   double x, double Q2);
        //virtual double F1He3( double x, double Q2);
        //virtual double F2He3( double x, double Q2);

        ClassDef(SFsFromVPACs,1)
    };
  }
}


#endif

