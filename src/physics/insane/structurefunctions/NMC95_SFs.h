#ifndef insane_physics_NMC95_SFs_HH
#define insane_physics_NMC95_SFs_HH

#include "insane/structurefunctions/StructureFunctions.h"

namespace insane {
  namespace physics {

    /** NMC95 Structure Functions.
     * \ingroup StructureFunctions
     */
    class  NMC95_SFs : public StructureFunctions {
      protected:
        mutable double fF1;
        mutable double fF2;

      public:
        NMC95_SFs();
        virtual ~NMC95_SFs();

        //virtual double Get(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const ;
 
        virtual double F1p(double x, double Q2) const ;
        virtual double F2p(double x, double Q2) const ;
        virtual double F1n(double x, double Q2) const ;
        virtual double F2n(double x, double Q2) const ;
        virtual double F1p_TMC(double x, double Q2) const {return F1p(x,Q2);}
        virtual double F2p_TMC(double x, double Q2) const {return F2p(x,Q2);}
        virtual double F1n_TMC(double x, double Q2) const {return F1n(x,Q2);}
        virtual double F2n_TMC(double x, double Q2) const {return F2n(x,Q2);}

        virtual double R(double x, double Q2, Nuclei target, Twist t = Twist::All) const;

      ClassDef(NMC95_SFs,1)
    };

  }
}

#endif
