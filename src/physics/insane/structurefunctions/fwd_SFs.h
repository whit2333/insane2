#ifndef insane_physics_fwd_SFs_HH
#define insane_physics_fwd_SFs_HH 1

#include "insane/structurefunctions/SFsFromPDFs.h"
#include "insane/structurefunctions/SSFsFromPDFs.h"
#include "insane/pdfs/JAM_PPDFs.h"
#include "insane/pdfs/JAM_T3DFs.h"
#include "insane/pdfs/JAM_T4DFs.h"
#include "insane/pdfs/CTEQ10_UPDFs.h"
#include "insane/pdfs/Stat2015_PPDFs.h"
#include "insane/pdfs/Stat2015_UPDFs.h"
#include "insane/pdfs/AAC08_PPDFs.h"
#include "insane/pdfs/BB_PPDFs.h"

#include "insane/pdfs/BBS_PPDFs.h"
#include "insane/pdfs/GS_PPDFs.h"
#include "insane/pdfs/ABDY_PPDFs.h"
#include "insane/pdfs/DSSV_PPDFs.h"
#include "insane/pdfs/LSS2006_PPDFs.h"
#include "insane/pdfs/LSS2010_PPDFs.h"
#include "insane/pdfs/DNS2005_PPDFs.h"

#include "insane/structurefunctions/SFsFromVPACs.h"
#include "insane/structurefunctions/SSFsFromVPACs.h"
#include "insane/xsections/MAID_VPACs.h"

namespace insane {
  namespace physics {

    /** Structure function aliases. 
     *
     * These aliases mirror those in structurefunctions/include/LinkDef.h 
     *
     */

    /** @name SFAlias.
     *  Structure functions.
     *  Some alises for structure functions.
     */
    //@{
    using CTEQ10_SFs   = insane::physics::SFsFromPDFs<insane::physics::CTEQ10_UPDFs>;
    using Stat2015_SFs = insane::physics::SFsFromPDFs<insane::physics::Stat2015_UPDFs>;
    using MAID_SFs      = insane::physics::SFsFromVPACs<insane::physics::MAID_VPACs>;
    //@}

    /** @name SSFAlias.
     *  Spin structure functions.
     *  Some alises for spin structure functions.
     *  Note the structure functions making use of higher twists appended with
     *  the highest twist, i.e., "_t3".
     */
    //@{
    using JAM_SSFs =  insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs>;
    using JAM_SSFs_t2 =  insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs>;
    using JAM_SSFs_t3 =  insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs>;
    using JAM_SSFs_t4 =  insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs,insane::physics::JAM_T4DFs>;

    using Stat2015_SSFs        = insane::physics::SSFsFromPDFs<insane::physics::Stat2015_PPDFs>;
    using Stat2015_SSFs_t2     = insane::physics::SSFsFromPDFs<insane::physics::Stat2015_PPDFs>;
    using Stat2015_JAM_SSFs_t3 = insane::physics::SSFsFromPDFs<insane::physics::Stat2015_PPDFs,insane::physics::JAM_T3DFs>;

    using AAC08_SSFs         = insane::physics::SSFsFromPDFs<insane::physics::AAC08_PPDFs>;
    using AAC08_JAM_SSFs_t3  = insane::physics::SSFsFromPDFs<insane::physics::AAC08_PPDFs,insane::physics::JAM_T3DFs>;

    using BB_SSFs         = insane::physics::SSFsFromPDFs<insane::physics::BB_PPDFs>;
    using BB_JAM_SSFs_t3  = insane::physics::SSFsFromPDFs<insane::physics::BB_PPDFs,insane::physics::JAM_T3DFs>;

    using DSSV_SSFs         = insane::physics::SSFsFromPDFs<insane::physics::DSSV_PPDFs>;
    using DSSV_JAM_SSFs_t3  = insane::physics::SSFsFromPDFs<insane::physics::DSSV_PPDFs,insane::physics::JAM_T3DFs>;

    using BBS_SSFs         = insane::physics::SSFsFromPDFs<insane::physics::BBS_PPDFs>;
    using BBS_JAM_SSFs_t3  = insane::physics::SSFsFromPDFs<insane::physics::BBS_PPDFs,insane::physics::JAM_T3DFs>;

    using GS_SSFs         = insane::physics::SSFsFromPDFs<insane::physics::GS_PPDFs>;
    using GS_JAM_SSFs_t3  = insane::physics::SSFsFromPDFs<insane::physics::GS_PPDFs,insane::physics::JAM_T3DFs>;

    using LSS2010_SSFs         = insane::physics::SSFsFromPDFs<insane::physics::LSS2010_PPDFs>;
    using LSS2010_JAM_SSFs_t3  = insane::physics::SSFsFromPDFs<insane::physics::LSS2010_PPDFs,insane::physics::JAM_T3DFs>;

    using LSS2006_SSFs         = insane::physics::SSFsFromPDFs<insane::physics::LSS2006_PPDFs>;
    using LSS2006_JAM_SSFs_t3  = insane::physics::SSFsFromPDFs<insane::physics::LSS2006_PPDFs,insane::physics::JAM_T3DFs>;

    using DNS2005_SSFs         = insane::physics::SSFsFromPDFs<insane::physics::DNS2005_PPDFs>;
    using DNS2005_JAM_SSFs_t3  = insane::physics::SSFsFromPDFs<insane::physics::DNS2005_PPDFs,insane::physics::JAM_T3DFs>;

    using ABDY_SSFs         = insane::physics::SSFsFromPDFs<insane::physics::ABDY_PPDFs>;
    using ABDY_JAM_SSFs_t3  = insane::physics::SSFsFromPDFs<insane::physics::ABDY_PPDFs,insane::physics::JAM_T3DFs>;

    using MAID_SSFs     = insane::physics::SSFsFromVPACs<insane::physics::MAID_VPACs>;

    using All_SFs_t2 = std::tuple<JAM_SSFs_t2, Stat2015_SSFs_t2, AAC08_SSFs, DSSV_SSFs, GS_SSFs,
                                  BBS_SSFs, LSS2010_SSFs, LSS2006_SSFs, DNS2005_SSFs, ABDY_SSFs>;
    //@}


  }
}

#endif

