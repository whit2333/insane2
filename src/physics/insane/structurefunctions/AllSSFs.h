#ifndef insane_physics_AllSSFs_H
#define insane_physics_AllSSFs_H 1

#include "insane/structurefunctions/fwd_SFs.h"
#include <variant>

namespace insane {
  namespace physics {
    using SSFsVariant = std::variant<BBS_SSFs*, LSS2010_SSFs*>;
    //JAM_SSFs_t2*, Stat2015_SSFs_t2*, AAC08_SSFs*, DSSV_SSFs*, GS_SSFs*,
     //                             , LSS2006_SSFs*, DNS2005_SSFs*, ABDY_SSFs*>;

    std::vector<SSFsVariant> GetAllSSFs() {
      return std::vector<SSFsVariant>{new BBS_SSFs(), new LSS2010_SSFs()};
        //new JAM_SSFs_t2,  new Stat2015_SSFs_t2(), new AAC08_SSFs(),   new DSSV_SSFs(),
        //                              new GS_SSFs(),      new BBS_SSFs(),         new LSS2010_SSFs(), new LSS2006_SSFs(),
        //                              new DNS2005_SSFs(), new ABDY_SSFs()};
    }


  }
}

#endif

