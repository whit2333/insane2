#ifndef insane_physics_SFsFromPDFs_HHXX
#define insane_physics_SFsFromPDFs_HHXX
// ------------------------------------------
namespace insane {
  namespace physics {

    template <class PDF, class...T>
    double   SFsFromPDFs<PDF,T...>::Calculate(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t, OPELimit l) const
    {
      auto sftype = std::get<SF>(sf);
      auto target = std::get<Nuclei>(sf);

      //constexpr int n_HT   = sizeof...(HT);
      double        result = 0.0;

      switch(sftype) {

        case SF::F1 :
          if( l == OPELimit::Massless) {
            result = insane::physics::F1(fDFs,target, x, Q2); 
          } else {
            result = insane::physics::F1_TMC(fDFs,target, x, Q2); 
          }
          break;

        case SF::F2 :
          if( l == OPELimit::Massless) {
            result = insane::physics::F2(fDFs,target, x, Q2); 
          } else {
            result = insane::physics::F2_TMC(fDFs,target, x, Q2); 
          }
          break;

        case SF::g1    :
        case SF::g2    :
        case SF::FL    :
        case SF::R     :
        case SF::W1    :
        case SF::W2    :
        case SF::gT    :
        case SF::g2_WW :
        case SF::g1_BT :
          std::cout << "Error: " << __FILE__ << ":" << __LINE__ <<  " SF not implemented!\n";

        default:
          result = 0.0;
          break;
      }
      return result;
    }
    //______________________________________________________________________________

    template <class PDF, class...T>
    double  SFsFromPDFs<PDF,T...>::Uncertainties(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t, OPELimit l) const
    {
      return 0.0;
    }
    //______________________________________________________________________________


  }
}

#endif

