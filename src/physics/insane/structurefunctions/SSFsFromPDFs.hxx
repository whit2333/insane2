#ifndef insane_physics_SSFsFromPDFs_HHXX
#define insane_physics_SSFsFromPDFs_HHXX
#include <iostream>

// ------------------------------------------
namespace insane {
  namespace physics {

    template <class PDF, class...T>
    double   SSFsFromPDFs<PDF,T...>::Calculate(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t, OPELimit l) const
    {
      auto sftype = std::get<SF>(sf);
      auto target = std::get<Nuclei>(sf);
      double        result = 0.0;

      switch(sftype) {

        case SF::g1 :
          if( l == OPELimit::Massless ) {
            result = insane::physics::g1(fDFs,target, x, Q2); 
          }
          //else if( l == OPELimit::Massless && (t==Twist::Two) ) {
          //  result = std::get<0>(fDFs).g1(x,Q2,target); 
          //}
          ////else if( l == OPELimit::Massless && (t==Twist::Three) ) {
          ////  result = std::get<1>(fDFs).g1(x,Q2,target); 
          ////}
          //else if( l == OPELimit::MassiveTarget && (t==Twist::Two) ) {
          //  result = std::get<0>(fDFs).g1(x,Q2,target); 
          //}
          ////else if( l == OPELimit::MassiveTarget && (t==Twist::Three) ) {
          ////  result = std::get<1>(fDFs).g1(x,Q2,target); 
          else {
            result = insane::physics::g1_TMC(fDFs,target, x, Q2); 
          }
          break;

        case SF::g2 :
          if( l == OPELimit::Massless ) {
            result = insane::physics::g2(fDFs,target, x, Q2); 
          }
          //else if( l == OPELimit::Massless && (t==Twist::Two) ) {
          //  result = std::get<0>(fDFs).g2(x,Q2,target); 
          //}
          ////else if( l == OPELimit::Massless && (t==Twist::Three) ) {
          ////  result = std::get<1>(fDFs).g2(x,Q2,target); 
          ////}
          //else if( l == OPELimit::MassiveTarget && (t==Twist::Two) ) {
          //  result = std::get<0>(fDFs).g2(x,Q2,target); 
          //}
          ////else if( l == OPELimit::MassiveTarget && (t==Twist::Three) ) {
          //  result = std::get<1>(fDFs).g2(x,Q2,target); 
          else {
            result = insane::physics::g2_TMC(fDFs,target, x, Q2); 
          }
          break;

        case SF::F1    :
        case SF::F2    :
        case SF::FL    :
        case SF::R     :
        case SF::W1    :
        case SF::W2    :
        case SF::gT    :
        case SF::g2_WW :
        case SF::g1_BT :
          std::cout << "Error: " << __FILE__ << ":" << __LINE__ <<  " SF not implemented!\n";

        default:
          result = 0.0;
          break;
      }
      return result;
    }
    //______________________________________________________________________________

    template <class PDF, class...T>
    double  SSFsFromPDFs<PDF,T...>::Uncertainties(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t, OPELimit l) const
    {
      return 0.0;
    }
    //______________________________________________________________________________


  }
}

#endif

