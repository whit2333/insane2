#ifndef InSANETarget_HH
#define InSANETarget_HH 1

#include "insane/experiment/TargetMaterial.h"
#include "TList.h"
#include "TBrowser.h"
#include "TNamed.h"
#include <iostream>

namespace insane {


/** Base class for a fixed target.
 *
 * \ingroup Apparatus
 */
class Target : public TNamed {

   protected:

      TList    fMaterials;
      Int_t    fNMaterials;
      Double_t fPackingFraction;

      //BremsstrahlungRadiator  fBremRadiator; 

   public:

      TGeoVolume * fTopVolume; //!

   public:

      Target(const char * name = "aTarget", const char * title = "A Target");
      virtual ~Target();
      Target(const Target & tg);
      Target(const Target * tg);
      Target& operator=(const Target& tg); 

      virtual void DefineMaterials() {}

      virtual void InitFromFile();

      Bool_t IsFolder() const { return kTRUE; }
      void Browse(TBrowser* b) {
         b->Add(&fMaterials, "Materials");
      }

      /** Add a new material to target */
      void AddMaterial(TargetMaterial * mat) ;

      /** Get material by name. Returns 0 if it is not found. */
      TargetMaterial * GetMaterial(const char * name) const ;

      /** Get material by name. Returns 0 if it is not found. */
      TargetMaterial * GetMaterial(Int_t i) const ;

      /** Print the target and materials */
      void Print(const Option_t * opt = "") const ; // *MENU*
      void Print(std::ostream &stream) const ;

      Double_t GetAttenuationLength() const ;

      /** Return the radiation length in g/cm^2. This doesn't make much
       *  sense for a composite target so GetNumberOfRadiationLengths is
       *  a more useful quantity.   
       */
      Double_t GetRadiationLength() const ;

      /** returns the length density in units of nuclei per cm^2*/
      Double_t GetLengthDensity() const ;

      /** Return the radiation length as a fraction of the energy lost. */
      Double_t GetNumberOfRadiationLengths() const ;

      Int_t GetNMaterials() const { return fNMaterials; }

      Double_t GetPackingFraction(){ return fPackingFraction; }
      void     SetPackingFraction(Double_t pf) { fPackingFraction = pf; }

      void     DrawTarget(Option_t * opt = "ogl"); 

      ClassDef(Target,7)
};

/** Simple, single nucleus target. 
 *
 * \ingroup Apparatus
 */
class SimpleTarget : public Target {
   public:
      SimpleTarget(const char * name = "SimpleTarget", const char * title = "Simple Target");
      virtual ~SimpleTarget();
      virtual void DefineMaterials();
      ClassDef(SimpleTarget,1)
};

/** A target with windows. 
 *
 * \ingroup Apparatus
 */
class SimpleTargetWithWindows : public Target {
   public:
      SimpleTargetWithWindows(const char * name = "SimpleTarget", const char * title = "Simple Target");
      virtual ~SimpleTargetWithWindows();
      virtual void DefineMaterials();
      ClassDef(SimpleTargetWithWindows,1)
};

}

#endif
