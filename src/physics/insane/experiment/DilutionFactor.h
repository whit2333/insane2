#ifndef DilutionFactor_HH
#define DilutionFactor_HH 1
#include "TObject.h"
#include "TNamed.h"
#include "TList.h"
#include <vector>
#include "TF1.h"
#include "TGraph2D.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TBrowser.h"
#include "insane/experiment/Target.h"
#include "TParticle.h"

namespace insane {
namespace physics {
/** Dilution factor for asymmetry calculation
 *
 *  \f$ df(Q^2,W) = \frac{C_1 \sigma_1(Q^2,W) }{C_1 \sigma_1(Q^2,W) + C_14 \sigma_14(Q^2,W) +C_4 \sigma_4(Q^2,W) + ... } \f$
 *
 *  \todo need to setup cross sections in a way that it can just be passed off to
 *        BETAG4 event generator (action) and it just runs....
 *
 *  Ideally one would:
 *   - create target materials (with cross sections)
 *   - then create the target
 *   - Create the DilutionFactor and set the target
 *   - Define DilutionFactor's kinematic binning
 *   - Then throw at event generator which grabs all the relevent cross sections etc...
 *
 * \ingroup physics
 */
class DilutionFactor : public TNamed {
   protected:
      Double_t    fDilution;
      Double_t    fDilutionError;
      Double_t    fQ2;
      Double_t    fW;
      Double_t    fx;
      Double_t    fPackingFraction;
      Double_t    fPackingFractionError;

   public:
      DilutionFactor(const char * n = "", const char * t = "");
      virtual ~DilutionFactor();

      Double_t GetDilution() const { return fDilution; }
      Double_t GetDilutionError() const { return fDilutionError; }
      Double_t GetQ2() const { return fQ2; }
      Double_t GetW() const { return fW; }
      Double_t Getx() const { return fx; }
      Double_t GetPackingFraction() const {return fPackingFraction; }
      Double_t GetPackingFractionError() const { return fPackingFractionError;}

      void SetDilution            (Double_t v){  fDilution            =v ; }
      void SetDilutionErr         (Double_t v){  fDilutionError       =v ; }
      void SetQ2                  (Double_t v){  fQ2                  =v ; }
      void SetW                   (Double_t v){  fW                   =v ; }
      void Setx                   (Double_t v){  fx                   =v ; }
      void SetPackingFraction     (Double_t v){  fPackingFraction     =v ; }
      void SetPackingFractionError(Double_t v){  fPackingFractionError=v ; }

      Bool_t IsFolder() const { return kTRUE; }
      void Browse(TBrowser* /*b*/) { }

      void Print(Option_t * opt ="") const ; // *MENU* 

      ClassDef(DilutionFactor, 3)
};


/**  Fits a set of dilution factors as a function of kinematic variables.
 *   Use to get the value of dilution for fine binning.
 *
 *
 */
class DilutionFunction : public TNamed {
   public :
      DilutionFunction(const char * n = "", const char * t = "");
      ~DilutionFunction() ;

      void  InitGraph();
      void  FitGraphs();
      void  Add(DilutionFactor * df);
      void  SetNBins(Int_t n = 1) { fNBins = n; }
      Int_t GetNBins() { return fNBins; }
      void   Draw(Option_t* option = "");
      /** Necessary for Browsing */
      Bool_t IsFolder() const {
         return kTRUE;
      }
      /** Needed to make object browsable. */
      void Browse(TBrowser* b) {
         b->Add(&fDFs, "Dilution Factors");
         b->Add(&fFuncs, "Dilution Functions");
         b->Add(fGraph, "df(x,W)");
         b->Add(fDfVsx, "df vs x");
         b->Add(fDfVsW, "df vs W");
      }

      Double_t          fQ2_bin;
      Double_t          fQ2_min;
      Double_t          fQ2_max;
      Int_t             fNBins;
      Double_t          fPar[3]; //->
      TList             fDFs;
      TList             fFuncs;
      TGraph2D *        fGraph; //->  // x vs W plot
      TGraphErrors *    fDfVsx; //->
      TGraphErrors *    fDfVsW; //->

      Double_t GetXFitResult(Double_t x);
      Double_t GetWFitResult(Double_t W); // *MENU*
      Double_t GetDilution(Double_t x); // *MENU*

      TF1 *             fxFit; //!
      TF1 *             fWFit; //!

      ClassDef(DilutionFunction, 2)
};


/** Dilution factor calculated from target setup and proton/neutron unpolarized structure functions.
 *
 *  \ingroup physics
 */
class DilutionFromTarget : public DilutionFunction {

   protected:
      Target * fTarget; //->

   public :
      DilutionFromTarget(const char * n = "", const char * t = "");
      ~DilutionFromTarget();

      Target * GetTarget() { return fTarget; }
      void           SetTarget(Target * targ) { fTarget = targ; }

      /** Calculates the rate from all target materials.
       */
      Double_t GetTotalRate(const Double_t x, const Double_t Q2);

      /** Calculates the rate from all POLARIZED target materials.
       */
      Double_t GetPolarizedRate(const Double_t x, const Double_t Q2);

      Double_t GetDilution_xW(const Double_t x, const Double_t W);
      Double_t GetDilution(const Double_t x, const Double_t Q2);
      Double_t GetDilution_WQ2(const Double_t W, const Double_t Q2);

      Double_t GetDilution(     const TParticle * beam, const TParticle * scat);
      Double_t GetTotalRate(    const TParticle * beam, const TParticle * scat);
      Double_t GetPolarizedRate(const TParticle * beam, const TParticle * scat);



      /** Use with TF1 functions. */
      double GetDilutionFromx(double *x, double *p);

      /** Use with TF1 functions. */
      double GetDilutionFromW(double *x, double *p);

      ClassDef(DilutionFromTarget, 2)
};

}
}

#endif

