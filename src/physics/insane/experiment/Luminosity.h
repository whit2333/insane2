#ifndef InSANELuminosity_HH
#define InSANELuminosity_HH 1

#include <vector>
#include <array>
#include "TObject.h"
#include "insane/experiment/Target.h"

#ifndef ELECTRONCHARGE
#define ELECTRONCHARGE 1.602e-19
#endif

#ifndef NAVOGADRO
#define NAVOGADRO 6.022e23
#endif

namespace insane {

  namespace materials {

    /**  \f$ L_i = I_{beam} \rho_{i} l_{i} N_{A} 1/m_{a} \f$
     */
    double ComputeLuminosityPerN(std::array<int,2> ZA, double density, double length, double I);

  }

  namespace physics {


    /** Fixed target Luminosity.
     *
     *  Note: Units are A, seconds, g, cm, and g/cm^3
     *        The luminosity is returned as k  
     *
     *  Fixed target luminosity :
     *
     * \f$ L = I_{beam} \rho_{targ} L_{targ} N_{A} 1/m_{a}  \f$
     *
     *  \f$ L[1/(s*cm^2)] = I_{beam}[1/s] \rho_{targ}[g/cm^3] L_{targ}[cm] N_{A}[1/mol] 1/m_{a}[mol/g]  \f$
     *
     */
    class Luminosity : public TObject {

    protected:
      Target * fTarget;         //!
      mutable double fLuminosity;     // The calculated luminosity per nuclei
      double         fBeamCurrent;    // The beam current in amps


    public:
      Luminosity(Target * targ = nullptr, double current = 0.0);
      virtual ~Luminosity();

      /** Adds up the luminosities for each material
       *
       *  \f$ L = \sum_i L_i \f$
       *
       *  where
       *
       *  \f$ L_i = I_{beam} \rho_{i} l_{i} N_{A} 1/m_{a} \f$
       */
      double CalculateLuminosity() const;

      /** Adds up the luminosity per nucleon for each material
       *
       *  \f$ L/A = \sum_i L_i/A_i \f$
       *
       *  where
       *
       *  \f$ L_i = I_{beam} \rho_{i} l_{i} N_{A} 1/m_{a} \f$
       */
      double CalculateLuminosityPerNucleon() const;


      /** \f$ L_i = I_{beam} \rho_{i} l_{i} N_{A} 1/m_{a} \f$
      */
      double GetMaterialLuminosity(int i) const;

      /** Beam current divided by electron charge */
      double       GetElectronFlux() const { return(fBeamCurrent / ELECTRONCHARGE); }
      Target * GetTarget() const { return fTarget; }

      void SetTarget(Target * t) { fTarget = t; }

      virtual void Print(Option_t * opt="") const ;
      virtual void Print(std::ostream& s) const ;


      ClassDef(Luminosity, 2)
    };

  } // namespace physics
} // namespace insane
#endif
