#ifndef TargetMaterial_HH
#define TargetMaterial_HH 1

#include "TNamed.h"
#include "TList.h"
#include "insane/base/PhysicalConstants.h"
#include "insane/base/Nucleus.h"
#include "TParticle.h"

#include "TGeoManager.h"
#include "TGeoMaterial.h"
#include "TGeoMedium.h"
#include "TGeoShape.h"
#include "TGeoVolume.h"
#include "TGeoMatrix.h"
#include "insane/rad/RadiativeEffects.h"

namespace insane {

//class BremsstrahlungRadiator;

/** Target material which is used in Target, Luminosity and Dilution.
 *  Need to define nuclei,density,length, and longitudinal position.
 *
 *  Note: units are \f$ cm \f$ and \f$ g/cm^3 \f$ and packing fraction is between 0 and 1.
 *
 *  Useful densities (g/cm3): (source: Wolfram Alpha)
 *   - LH2  = 0.0720
 *   - L4He = 0.1412
 *   - LN2  = 0.8116
 *   - Cu   = 8.96
 *   - Al   = 2.7
 *   - Ni   = 8.908
 *   - SNH3 = 0.817 (wikipedia)
 *   - SNH3 = 0.696 (Wolfram Alpha)
 *
 * \ingroup materials
 */
class TargetMaterial : public TNamed {

   protected:


      Nucleus                fNucleus;
      //ElasticRadiativeTail * fElasticTail; //!
      //InclusiveBornDISXSec * fDISXSec;     //!
      //InclusiveDiffXSec    * fPi0DiffXSec;  //!
      //InclusiveDiffXSec    * fPionDiffXSec; //!


   public:

      Int_t    fMatID;
      // These should be deprecated for the TGeo classes
      Double_t fA;
      Double_t fZ;
      Double_t fN;
      Double_t fDensity;          // g/cm^3
      Double_t fLength;           // cm
      Double_t fZposition;        // cm 
      Bool_t   fIsPolarized;      //
      Double_t fPackingFraction;  // 
      Double_t fDFCoeff;          //
      Double_t fAtomicMass;       //
      Double_t fgEMC;

      TGeoMixture                * fTGeoMixture;  //!
      TGeoMedium                 * fTGeoMedium;   //!
      TGeoVolume                 * fTGeoVolume;   //!
      TGeoShape                  * fTGeoShape;    //!
      TGeoMatrix                 * fTGeoMatrix;   //!

      //BremsstrahlungRadiator  * fBremRadiator; //! 

   public:

      /** Proton from liquid H2 by default.  */
      TargetMaterial(
            const char * name = "someMaterial", 
            const char * title = "a Target  Material", 
            Int_t Z = 1, 
            Int_t A = 1);

      virtual ~TargetMaterial();
      TargetMaterial(const TargetMaterial& v); 
      TargetMaterial& operator=(const TargetMaterial& v); 

      virtual void InitGeo();

      Int_t  GetMaterialID() const { return fMatID; } 
      Int_t  GetMatID()      const { return fMatID; } 
      void   SetMaterialID(Int_t i){ fMatID = i; } 
      void   SetMatID(     Int_t i){ fMatID = i; } 

      //BremsstrahlungRadiator  * GetBremRadiator() const;// {return fBremRadiator;}
      //void SetBremRadiator(BremsstrahlungRadiator * r);// {fBremRadiator=r;}

      const Nucleus& GetNucleus() const { return fNucleus; }

      /** Set the nucleus. Currently uses A as the atomic mass. */
      void SetNucleus(Int_t Z, Int_t A, Bool_t pol = false);

      /** Returns g/cm^2 */
      Double_t GetRadiationLength() const {
         return(insane::rad::rad_length(int(fZ),int(fA)));
      }

      /** Returns cm */
      Double_t GetAttenuationLength() const {
         return(GetRadiationLength()/fDensity);
      }

      /** Returns fractional energy loss in units of radiation legnth. */
      Double_t GetNumberOfRadiationLengths() const {
         return((fPackingFraction*fLength)/GetAttenuationLength());
      }

      /** Returns \f$  pf l_{i} \rho_i N_{A} 1/m_{a}  [1/cm^2] \f$ .
       *  Used in \f$ L [1/(s*cm)]= (I_{beam}[1/s])(\rho_{targ}[g/cm^3])(L_{targ}[cm])(N_{A}[1/mol])(1/m_{a}[mol/g]) \f$
       */
      Double_t GetLengthDensity() const {
        using insane::constants::Avogadro;
        return(fPackingFraction * fLength * fDensity * Avogadro  / fAtomicMass);
      }

      /** \f$ C = \rho L pf  [g/cm^2] \f$ */
      Double_t GetWeightCoefficient() const {
         return (fDensity * fLength * fPackingFraction);
      }

      /** Print the target and materials */
      void Print(const Option_t * opt = "") const ; // *MENU*
      void Print(std::ostream &stream) const ;

      /**  Calculates a simple born event rate
       *   \f$ r(x,Q^2) = \rho z ( Z F_2^p + N F_2^N ) g_{EMC} \f$
       *   where \f$ g_{EMC} = 1 \f$ for now.
       *    Need to move this to Yield or similar classes
       */
      //Double_t GetRate(const Double_t x, const Double_t Q2);
      //Double_t GetRate(const TParticle * beam, const TParticle * scat);

      Double_t GetLength() const { return(fPackingFraction*fLength);}
      Double_t GetDensity() const{ return(fDensity);}
      Bool_t   IsPolarized() { return(fIsPolarized); }

      // Update the geometry
      void Update();

      ClassDef(TargetMaterial,5)
};
}

#endif

