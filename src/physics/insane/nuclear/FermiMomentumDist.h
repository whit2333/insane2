#ifndef InSANE_physics_FermiMomentumDist_HH
#define InSANE_physics_FermiMomentumDist_HH

#include "TObject.h"
#include "insane/nuclear/fermi3.h"
#include <map>

namespace insane {
   namespace physics {
      /** Nucleon momentum distribution in nuclei.
       *  Momentum distribution is given by n(\vec{p}) which has the normalization
       *  \int dp^3 n(\vec{p}) = 1.
       */
      class  FermiMomentumDist : public TObject {
         private:
            mutable std::map<int,double> fNorms;

         public:
            FermiMomentumDist();
            virtual ~FermiMomentumDist();

            // Nucleon momentum distribution (units: [fm^3]) as function of k [fm^1] 
            virtual double n( double k, int A ) const;

            double  Evaluate( double k, int A ) const; 

            double  GetNorm(       int A ) const;
            double  CalculateNorm( int A ) const;

            double operator() (double *x, double *p) ;

            double GetMomentum(double P, int A); 
            double GetProb(double P, int A) const; 

            ClassDef(FermiMomentumDist,2)
      };
   }
}

#endif

