#ifndef InSANE_physics_DoubleDistributionGPDs
#define InSANE_physics_DoubleDistributionGPDs

#include "insane/gpds/GeneralizedPartonDistributions.h"
#include "insane/pdfs/UnpolarizedPDFs.h"

namespace insane {
  namespace physics {

    /** Double distribution formulation of GPDs.
     *  Here we only consider the one component double distributions (1CDD).
     */
    class DoubleDistributionGPDs : public GeneralizedPartonDistributions {
    protected:
      mutable UnpolarizedPDFs fPDFs;

    public:

      DoubleDistributionGPDs(){ }
      virtual ~DoubleDistributionGPDs(){ }

      virtual double H_u(   double x, double xi, double t, double Q2) const ;
      virtual double H_uval(double x, double xi, double t, double Q2) const ;
      virtual double H_ubar(double x, double xi, double t, double Q2) const ;

      virtual double H_d(   double x, double xi, double t, double Q2) const ;
      virtual double H_dval(double x, double xi, double t, double Q2) const ;
      virtual double H_dbar(double x, double xi, double t, double Q2) const ;

      virtual double H_g(   double x, double xi, double t, double Q2) const ;
      //virtual double Htilde_g(   double x, double xi, double t, double Q2) const ;

      double H_DD(std::function<double(double*,double*)> f, double x, double xi, double t, double Q2) const;
      double H_DDintegral_DGLAP_low( std::function<double(double*,double*)> f, double x, double xi, double t, double Q2) const;
      double H_DDintegral_DGLAP_high(std::function<double(double*,double*)> f, double x, double xi, double t, double Q2) const;
      double H_DDintegral_ERBL(      std::function<double(double*,double*)> f, double x, double xi, double t, double Q2) const;

      double ProfileFunction(double N, double beta, double alpha) const;

      double H_u_integrand(double x, double xi, double t, double Q2,double bb) const;
      double H_u_DGLAP_low( double x, double xi, double t, double Q2) const ;
      double H_u_DGLAP_high(double x, double xi, double t, double Q2) const ;
      double H_u_ERBL(      double x, double xi, double t, double Q2) const ;
      //double h(int f, int i, double beta, double Q2) const;
      //double f(int f, int i, double beta, double alpha, double t, double Q2) const;
      //double H_i(int f, int i,double x, double xi, double t, double Q2) const;
      //double Htilde_i_integrand(int f, int i,double x, double xi, double t, double Q2,double bb) const;
      //double H_i_integrate_DD(int f, int i,double x, double xi, double t, double Q2) const;
      //double Htilde_i(int f, int i,double x, double xi, double t, double Q2) const;


      ClassDef(DoubleDistributionGPDs,1)
    };

  }
}
#endif

