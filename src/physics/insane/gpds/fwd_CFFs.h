#ifndef insane_physics_fwd_CFFs_HH
#define insane_physics_fwd_CFFs_HH

#include "insane/gpds/CFFsFromGPDs.h"
#include "insane/gpds/DD_GPDs.h"
#include "insane/gpds/GK_GPDs.h"

namespace insane {
  namespace physics {

    //using DD_CFFs =  insane::physics::CFFsFromGPDs<insane::physics::DD_GPDs>;
    using GK_CFFs =  insane::physics::CFFsFromGPDs<insane::physics::GK_GPDs>;

  }
}

#endif

