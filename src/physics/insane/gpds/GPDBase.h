#ifndef insane_physics_GPDBase_HH
#define insane_physics_GPDBase_HH 

#include "TNamed.h"
#include "TAttLine.h"
#include "TAttMarker.h"
#include "TAttFill.h"
#include <array>
#include <string>
//#include <string_view>
#include "insane/base/Physics.h"

namespace insane {
  namespace physics {

    //template<typename T>
    //struct GPD {
    //};

    class GPDBase : public TNamed , public virtual TAttLine, public virtual TAttFill, public virtual TAttMarker {
      public:

      protected:
        std::string           fLabel;
        mutable double        f_Q2;

        struct GPD_vars {
          double x ;
          double xi;
          double t ;
        };

        mutable std::array<double, NPartons> fValues;
        mutable std::array<double, NPartons> fUncertainties;

        bool IsComputed(const GPD_vars& x, double Q2) const;

      public :

        GPDBase();
        virtual ~GPDBase();

        void        SetLabel(const char * l){ fLabel = l;}
        const char* GetLabel() const { return fLabel.c_str(); }

        /** Resets x,Q2,and the pdf values to zero. */
        void Reset();

        /** Calculate and return distribution value.
         * Returns the current value for flavor f but checks that the 
         * distributions have already been calculated at (x,Q2). 
         * It uses IsComputed(x,Q2) to do this check.
         */
        double  Get(const Parton& f, const GPD_vars& x, double Q2) const ;

        double  Get(Parton f, const GPD_vars& x, double Q2) const ;

        /** Get current distribution value.
         * Note: this method should only be used to get the stored values 
         * after Calculate(x,Q2) or Get(f,x,Q2) has been used at the desired (x,Q2).
         */ 
        double  Get(const Parton& f) const ;

        /** Get current distribution value.
         * Note: this method should only be used to get the stored values 
         * after Calculate(x,Q2) or Get(f,x,Q2) has been used at the desired (x,Q2).
         */ 
        double  Get(Parton f) const ;

        /** Virtual method should get all values of pdfs and set
         *  values of fX and fQsquared.
         */
        virtual const std::array<double,NPartons>& Calculate    (const GPD_vars& x, double Q2) const;
        virtual const std::array<double,NPartons>& Uncertainties(const GPD_vars& x, double Q2) const;

        virtual double H(      int i, double x, double xi, double t, double Q2) const {return 0.0;}
        virtual double E(      int i, double x, double xi, double t, double Q2) const {return 0.0;}
        virtual double H_tilde(int i, double x, double xi, double t, double Q2) const {return 0.0;}
        virtual double E_tilde(int i, double x, double xi, double t, double Q2) const {return 0.0;}

        virtual double H_u(   double x, double xi, double t, double Q2) const         {return 0.0;}
        virtual double H_d(   double x, double xi, double t, double Q2) const         {return 0.0;}
        virtual double H_ubar(double x, double xi, double t, double Q2) const         {return 0.0;}
        virtual double H_dbar(double x, double xi, double t, double Q2) const         {return 0.0;}

        virtual double H_g(   double x, double xi, double t, double Q2) const         {return 0.0;}
        virtual double Htilde_g(   double x, double xi, double t, double Q2) const    {return 0.0;}

        virtual double Htilde_u(   double x, double xi, double t, double Q2) const    {return 0.0;}
        virtual double Htilde_d(   double x, double xi, double t, double Q2) const    {return 0.0;}
        virtual double Htilde_ubar(double x, double xi, double t, double Q2) const    {return 0.0;}
        virtual double Htilde_dbar(double x, double xi, double t, double Q2) const    {return 0.0;}

        ClassDef(GPDBase, 1)
    };
  }
}

#endif

