#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;


#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;

#pragma link C++ class insane::physics::GPDBase+;
#pragma link C++ class insane::physics::DD_GPDs+;

#pragma link C++ class insane::physics::GeneralizedPartonDistributions+;
#pragma link C++ class insane::physics::DoubleDistributionGPDs+;
#pragma link C++ class insane::physics::GK_GPDs+;

#pragma link C++ class insane::physics::CFFsFromGPDs<insane::physics::GK_GPDs>+;
#pragma link C++ class insane::physics::CFFsFromGPDs<insane::physics::DD_GPDs>+;


#pragma link C++ class insane::physics::ComptonFormFactors+;


#endif

