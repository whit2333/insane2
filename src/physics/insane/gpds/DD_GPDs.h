#ifndef InSANE_physics_DD_GPDs
#define InSANE_physics_DD_GPDs

#include "insane/gpds/GPDBase.h"

namespace insane {
  namespace physics {

    /** Double distribution formulation of GPDs.
     *  Here we only consider the one component double distributions (1CDD).
     */
    template<class UPDFs>
    class DD_GPDs : public GPDBase {
    public:
      mutable UPDFs fPDFs;
      //mutable CTEQ6UnpolarizedPDFs fPDFs;

    public:
      DD_GPDs(){ }
      virtual ~DD_GPDs(){ }

      virtual double H_u(   double x, double xi, double t, double Q2) const ;
      virtual double H_uval(double x, double xi, double t, double Q2) const ;
      virtual double H_ubar(double x, double xi, double t, double Q2) const ;

      virtual double H_d(   double x, double xi, double t, double Q2) const ;
      virtual double H_dval(double x, double xi, double t, double Q2) const ;
      virtual double H_dbar(double x, double xi, double t, double Q2) const ;

      virtual double H_g(   double x, double xi, double t, double Q2) const ;
      //virtual double Htilde_g(   double x, double xi, double t, double Q2) const ;

      double H_DD(std::function<double(double*,double*)> f, double x, double xi, double t, double Q2) const;
      double H_DDintegral_DGLAP_low( std::function<double(double*,double*)> f, double x, double xi, double t, double Q2) const;
      double H_DDintegral_DGLAP_high(std::function<double(double*,double*)> f, double x, double xi, double t, double Q2) const;
      double H_DDintegral_ERBL(      std::function<double(double*,double*)> f, double x, double xi, double t, double Q2) const;

      double ProfileFunction(double N, double beta, double alpha) const;

      double H_u_integrand(double x, double xi, double t, double Q2,double bb) const;
      double H_u_DGLAP_low( double x, double xi, double t, double Q2) const ;
      double H_u_DGLAP_high(double x, double xi, double t, double Q2) const ;
      double H_u_ERBL(      double x, double xi, double t, double Q2) const ;
      //double h(int f, int i, double beta, double Q2) const;
      //double f(int f, int i, double beta, double alpha, double t, double Q2) const;
      //double H_i(int f, int i,double x, double xi, double t, double Q2) const;
      //double Htilde_i_integrand(int f, int i,double x, double xi, double t, double Q2,double bb) const;
      //double H_i_integrate_DD(int f, int i,double x, double xi, double t, double Q2) const;
      //double Htilde_i(int f, int i,double x, double xi, double t, double Q2) const;


      ClassDef(DD_GPDs,1)
    };

  }
}

#include "insane/gpds/DD_GPDs.hxx"

#endif

