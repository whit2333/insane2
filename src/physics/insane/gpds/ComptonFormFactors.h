#ifndef InSANE_physics_ComptonFormFactors_HH
#define InSANE_physics_ComptonFormFactors_HH

#include "TObject.h"

namespace insane {
   namespace physics {

      class ComptonFormFactors {
         public:
            ComptonFormFactors(){ } 
            virtual ~ComptonFormFactors(){ } 

            ClassDef(ComptonFormFactors,1)
      };

   }
}

#endif
