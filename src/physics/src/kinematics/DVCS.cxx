#include "insane/kinematics/DVCS.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "TMath.h"
#include <complex>

namespace insane::kine::dvcs {

  double epsilon(double Q2, double xB, double M) {
    return insane::kine::epsilon_xQ2(xB, Q2);
    // return 2.0*xB*M/TMath::Sqrt(Q2);
  }

  double Delta2_min(double Q2, double xB, double M) {
    double eps = epsilon(Q2, xB, M);
    double num = -Q2 * (2.0 * (1.0 - xB) * (1.0 - TMath::Sqrt(1.0 + eps * eps)) + eps * eps);
    double den = 4.0 * xB * (1.0 - xB) + eps * eps;
    return num / den;
  }

  double Delta2_min2(double Q2, double xB, double M) {
    double eps = epsilon(Q2, xB, M);
    double num =
        -Q2 * (2.0 * (1.0 - xB) + eps * eps - 2.0 * TMath::Sqrt(1.0 + eps * eps) * (1.0 - xB));
    double den = 4.0 * xB * (1.0 - xB) + eps * eps;
    return num / den;
  }
  double Delta2_max(double Q2, double xB, double M) {
    double eps = epsilon(Q2, xB, M);
    double num =
        -Q2 * (2.0 * (1.0 - xB) + eps * eps + 2.0 * TMath::Sqrt(1.0 + eps * eps) * (1.0 - xB));
    double den = 4.0 * xB * (1.0 - xB) + eps * eps;
    return num / den;
  }

  double y_max(double eps) { return 2.0 * (TMath::Sqrt(1.0 + eps * eps) - 1.0) / eps * eps; }

  double Delta2_perp(double xi, double Delta2, double D2m) {
    // approximate form
    return (1.0 - xi * xi) * (Delta2 - D2m);
  }

  double xi_BKM(double xB, double Q2, double Delta2) {
    return (xB * (1.0 + Delta2 / (2.0 * Q2)) / (2.0 - xB + xB * Delta2 / Q2));
  }

  double eta_BKM(double xi, double Q2, double Delta2) {
    return (-xi / (1.0 - Delta2 / (2.0 * Q2)));
  }
} // namespace insane::kine::dvcs
