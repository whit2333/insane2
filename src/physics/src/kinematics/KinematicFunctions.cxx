#include "insane/kinematics/KinematicFunctions.h"
#include <cmath>
#include "TMath.h"

using namespace insane::units;

namespace insane::kine {

    using units::GeV;
    using units::M_p;
    using units::M_pion;

    typedef enum { kx, ky, kQ2, kW, kW2, knu, kepsilon, kE, kEprime, ktheta, kphi } Variable;

    double tau(double Qsq, double Mtarg) { return (Qsq / (4.0 * Mtarg * Mtarg)); }

    double q_abs(double Qsq, double Mtarg) {
      return std::sqrt(4.0 * Mtarg * Mtarg * tau(Qsq, Mtarg) * (1.0 + tau(Qsq, Mtarg)));
    }

    double Q2(double en, double enprime, double theta) {
      return (4.0 * en * enprime * TMath::Power(TMath::Sin(theta / 2.0), 2));
    }
    double Q2_EEprimeTheta(double en, double enprime, double theta) {
      return (4.0 * en * enprime * TMath::Power(TMath::Sin(theta / 2.0), 2));
    }
    double Q2_xW(double x, double W, double Mtarg) {
      return ((W * W - Mtarg * Mtarg) / ((1 / x) - 1));
    }


    double xBjorken(double Qsq, double nu, double Mtarg) {
      // Bjorken scaling variable, x
      return (Qsq / (2.0 * Mtarg * nu));
    }

    double xBjorken_EEprimeTheta(double e, double eprime, double theta) {
      // Bjorken scaling variable, x
      return (xBjorken(Q2(e, eprime, theta), (e - eprime)));
    }

    double xi_Nachtmann(double x, double Q2) {
      // Nachtmann scaling variable, xi
      return (2.0 * x / (1.0 + TMath::Sqrt(1.0 + 4.0 * x * x * ((M_p / GeV) * (M_p / GeV)) / Q2)));
    }

    double nu(double en, double enprime) { return (en - enprime); }

    double nu_WsqQsq(double Wsq, double Qsq, double Mtarg) {
      double num = Wsq - Mtarg * Mtarg + Qsq;
      double den = 2. * Mtarg;
      double res = num / den;
      return res;
    }

    double epsilon(double en, double enprime, double theta) {
      if (theta == 0.0)
        return (0.0);
      double nu2  = nu(en, enprime) * nu(en, enprime);
      double Q2   = Q2_EEprimeTheta(en, enprime, theta);
      double TAN  = TMath::Tan(theta / 2.0);
      double TAN2 = TAN * TAN;
      double num  = 1.;
      double den  = 1. + 2. * (1. + nu2 / Q2) * TAN2;
      double eps  = num / den;
      return eps;
    }

    double epsilon_xQ2(double xB, double Q2, double M){
      return 2.0*xB*M/TMath::Sqrt(Q2);
    }
    //______________________________________________________________________________

    double W2(double Qsq, double nu, double Mtarg) {
      return (2.0 * nu * Mtarg - Qsq + Mtarg * Mtarg);
    }

    double W_xQsq(double x, double Qsq, double Mtarg) {
      double result = 0.0;
      double t      = 0.0;
      double W      = TMath::Sqrt(Mtarg * Mtarg + Qsq / x - Qsq);
      if (W > 0) {
        result = W;
      } else {
        result = 0;
      }
      return result;
    }
    double W_xQ2(double x, double Qsq, double Mtarg) { return W_xQsq(x, Qsq, Mtarg); }

    double W_EEprimeTheta(double e, double eprime, double theta) {
      double result;
      double W = W_xQsq(xBjorken_EEprimeTheta(e, eprime, theta), Q2(e, eprime, theta));
      if (W > 0) {
        result = W;
      } else {
        result = 0;
      }
      return result;
    }

    double xBjorken_WQsq(double W, double Qsq, double Mtarg) {
      return (Qsq / (W * W + Qsq - Mtarg * Mtarg));
    }
    double xBjorken_WQ2(double W, double Q2, double Mtarg) {
      return (Q2 / (W * W + Q2 - Mtarg * Mtarg));
    }

    double BeamEnergy_xQ2y(double x, double Q2, double y, double Mtarg) {
      return (Q2 / (2. * Mtarg * x * y));
    }
    double Eprime_xQ2y(double x, double Q2, double y, double Mtarg) {
      return (-(Q2 * (-1.0 + y)) / (2. * Mtarg * x * y));
    }
    double Eprime_W2theta(double W2, double theta, double Ebeam, double Mtarg) {
      return ((Mtarg * Mtarg + 2.0 * Mtarg * Ebeam - W2) /
              (2.0 * Mtarg + 4.0 * Ebeam * TMath::Power(TMath::Sin(theta / 2.0), 2.0)));
    }
    double Theta_xQ2y(double x, double Q2, double y, double Mtarg) {
      if (Q2 == 0.0)
        return (0.0);
      double res = 2.0 * TMath::ASin((Mtarg * x * y) / TMath::Sqrt(Q2 - Q2 * y));
      if (res > TMath::Pi())
        return TMath::Pi();
      return res;
    }
    double Theta_epsilonQ2W2(double eps, double Q2, double W2, double Mtarg) {
      using namespace TMath;
      double nu = (W2 - Mtarg * Mtarg + Q2) / (2.0 * Mtarg);
      if (Q2 == 0.0)
        return 0.0;
      double arg = ((1.0 / eps) - 1.0) / (2.0 * (1.0 + (nu * nu) / Q2));
      return (2.0 * ATan(Sqrt(arg)));
    }
    double Eprime_epsilonQ2W2(double eps, double Q2, double W2, double Mtarg) {
      using namespace TMath;
      double nu    = (W2 - Mtarg * Mtarg + Q2) / (2.0 * Mtarg);
      double theta = 0.0;
      if (Q2 != 0.0)
        theta = Theta_epsilonQ2W2(eps, Q2, W2, Mtarg);
      double s2 = Power(Sin(theta / 2.0), 2.0);
      double t1 = (nu / 2.0);
      double t2 = -1.0;
      if (Q2 != 0.0)
        t2 = (-1.0 + Sqrt(1.0 + Q2 / (nu * nu * s2)));
      return t1 * t2;
    }
    double BeamEnergy_epsilonQ2W2(double eps, double Q2, double W2, double Mtarg) {
      using namespace TMath;
      double nu     = (W2 - Mtarg * Mtarg + Q2) / (2.0 * Mtarg);
      double Eprime = Eprime_epsilonQ2W2(eps, Q2, W2, Mtarg);
      return (nu + Eprime);
    }

    double K_Hand(double x, double Q2) {
      // Hand convention for the virtual photon flux factor.
      // K is the energy requred for a real photon to create the final state
      // L.N. Hand Phys.Rev. 129 (1963) 1834-1846
      double nu = Q2 / (2.0 * (M_p / GeV) * x);
      return (nu - Q2 / (2.0 * (M_p / GeV)));
    }

      double Sig_Mott(double en, double theta)
      {
         double res =  (1.0/137.0)*TMath::Cos(theta/2.0)/(2.0*en*TMath::Power(TMath::Sin(theta/2.0),2.0));
         return res*res;
      }

      double fRecoil(double en, double theta, double Mtarg)
      {
         return(1.0 + 2.0 * en * TMath::Power(TMath::Sin(theta / 2.0), 2) / (Mtarg));
      }


    //double D(double e, double eprime, double theta, double R) {
    //  double eps = epsilon(e, eprime, theta);
    //  return ((e - eps * eprime) / (e * (1.0 + eps * R)));
    //}

    //double Dprime(double e, double eprime, double theta, double R) {
    //  double y       = (e - eprime) / e;
    //  double eps     = epsilon(e, eprime, theta);
    //  double num     = (1. - eps) * (2. - y);
    //  double den     = y * (1. + eps * R);
    //  double d_prime = num / den;
    //  return d_prime;
    //}
    //double d(double e, double eprime, double theta, double R) {
    //  double eps = epsilon(e, eprime, theta);
    //  return (D(e, eprime, theta, R) * TMath::Sqrt(2.0 * eps / (1.0 + eps)));
    //}

    //double Eta(double e, double eprime, double theta) {
    //  double eps = epsilon(e, eprime, theta);
    //  return (eps * TMath::Sqrt(Q2(e, eprime, theta)) / (e - eps * eprime));
    //}

    //double Xi(double e, double eprime, double theta) {
    //  double eps = epsilon(e, eprime, theta);
    //  return (Eta(e, eprime, theta) * (1.0 + eps) / (2.0 * eps));
    //}

    //double Chi(double e, double eprime, double theta, double phi) {
    //  return ((eprime * TMath::Sin(theta) * (1.0 / TMath::Cos(phi))) /
    //          (e - eprime * TMath::Cos(theta)));
    //}
} // namespace insane::kine


  //namespace dvcs {
  //  double epsilon(double Q2, double xB, double M) {
  //    return insane::kine::epsilon_xQ2(xB, Q2,M);
  //    // return 2.0*xB*M/TMath::Sqrt(Q2);
  //  }

  //  double Delta2_min(double Q2, double xB, double M) {
  //    double eps = epsilon(Q2, xB, M);
  //    double num = -Q2 * (2.0 * (1.0 - xB) * (1.0 - TMath::Sqrt(1.0 + eps * eps)) + eps * eps);
  //    double den = 4.0 * xB * (1.0 - xB) + eps * eps;
  //    return num / den;
  //  }

  //  double Delta2_min2(double Q2, double xB, double M) {
  //    double eps = epsilon(Q2, xB, M);
  //    double num =
  //        -Q2 * (2.0 * (1.0 - xB) + eps * eps - 2.0 * TMath::Sqrt(1.0 + eps * eps) * (1.0 - xB));
  //    double den = 4.0 * xB * (1.0 - xB) + eps * eps;
  //    return num / den;
  //  }
  //  double Delta2_max(double Q2, double xB, double M) {
  //    double eps = epsilon(Q2, xB, M);
  //    double num =
  //        -Q2 * (2.0 * (1.0 - xB) + eps * eps + 2.0 * TMath::Sqrt(1.0 + eps * eps) * (1.0 - xB));
  //    double den = 4.0 * xB * (1.0 - xB) + eps * eps;
  //    return num / den;
  //  }

  //  double tmin(double Q2, double xB, double M) {
  //    const double gamma   = 2.0 * xB * M / std::sqrt(Q2);
  //    const double sqrtgam = std::sqrt(1.0 + gamma * gamma);
  //    return Q2 * (1.0 - sqrtgam + gamma * gamma / 2.0) /
  //           (xB * (1.0 - sqrtgam + gamma * gamma / (2.0 * xB)));
  //  }

  //  double y_max(double eps) { return 2.0 * (TMath::Sqrt(1.0 + eps * eps) - 1.0) / eps * eps; }

  //  double Delta2_perp(double xi, double Delta2, double D2m) {
  //    // approximate form
  //    return (1.0 - xi * xi) * (Delta2 - D2m);
  //  }

  //  double xi_BKM(double xB, double Q2, double Delta2) {
  //    return (xB * (1.0 + Delta2 / (2.0 * Q2)) / (2.0 - xB + xB * Delta2 / Q2));
  //  }

  //  double eta_BKM(double xi, double Q2, double Delta2) {
  //    return (-xi / (1.0 - Delta2 / (2.0 * Q2)));
  //  }
  //} // namespace dvcs

  //namespace Phys {

  //  double A_pair_corr(double A, double Apair, double Rpair) {
  //    using namespace TMath;
  //    double res = (-2.0 * Apair * Rpair) / (-2.0 + Rpair) + (A * (2.0 + Rpair)) / (2 - Rpair);
  //    return (res);
  //  }

  //  double delta_A_pair_corr(double A, double delta_A, double Apair, double delta_Apair,
  //                           double Rpair, double delta_Rpair) {
  //    // Systematic error propagation for pair symmetric background correction.
  //    using namespace TMath;
  //    double res = (16 * Power(A + Apair, 2) * Power(delta_Rpair, 2) +
  //                  Power(-2 + Rpair, 2) * (4 * Power(delta_Apair, 2) * Power(Rpair, 2) +
  //                                          Power(delta_A, 2) * Power(2 + Rpair, 2))) /
  //                 Power(-2 + Rpair, 4);
  //    // double res = (4*Power(A - Apair,2)*Power(delta_Rpair,2) + Power(1 +
  //    // Rpair,2)*(4*Power(delta_Apair,2)*Power(Rpair,2) + Power(delta_A + 3*delta_A*Rpair,2)))/
  //    //      Power(1 + Rpair,4);
  //    return (Sqrt(res));
  //  }

  //  double A_elastic_sub_corr(double A, double SigmaEl, double SigmaIn, double DelEl) {
  //    using namespace TMath;
  //    double res = -(DelEl / SigmaIn) + (A * (SigmaEl + SigmaIn)) / SigmaIn;
  //    return (res);
  //  }
  //  double delta_A_elastic_sub_corr(double A, double deltaA, double SigmaEl, double deltaSigmaEl,
  //                                  double SigmaIn, double deltaSigmaIn, double DelEl,
  //                                  double deltaDelEl) {
  //    // Systematic error for elastic radiative tail correction
  //    using namespace TMath;
  //    double res =
  //        (Power(deltaSigmaIn, 2) * Power(DelEl - A * SigmaEl, 2) +
  //         (Power(deltaDelEl, 2) + Power(A, 2) * Power(deltaSigmaEl, 2)) * Power(SigmaIn, 2) +
  //         Power(deltaA, 2) * Power(SigmaIn, 2) * Power(SigmaEl + SigmaIn, 2)) /
  //        Power(SigmaIn, 4);
  //    return (Sqrt(res));
  //  }

  //  // uncertainties
  //  double delta_g1(double x, double Q2, double E0, double R, double deltaR, double F1,
  //                  double deltaF1, double Apara, double deltaApara, double Aperp,
  //                  double deltaAperp) {

  //    using namespace TMath;
  //    double nu   = Q2 / (2.0 * (M_p / GeV) * x);
  //    double GAM2 = Q2 / (nu * nu);
  //    double Ep   = insane::kine::Eprime_xQ2y(x, Q2, nu / E0);
  //    double th   = insane::kine::Theta_xQ2y(x, Q2, nu / E0);
  //    double eps  = insane::kine::epsilon(E0, Ep, th);
  //    double aD   = (1.0 / E0) * (E0 - eps * Ep);

  //    double eta   = insane::kine::Eta(E0, Ep, th);
  //    double xi    = insane::kine::Xi(E0, Ep, th);
  //    double dg1_2 = (Power(deltaAperp, 2) * (1 + eps) * Power(F1, 2) *
  //                    Power(-eta + Sqrt(GAM2), 2) * Power(1 + eps * R, 2)) /
  //                       (2. * Power(aD, 2) * eps * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2)) +
  //                   (Power(deltaApara, 2) * Power(F1, 2) * Power(1 + eps * R, 2) *
  //                    Power(1 + Sqrt(GAM2) * xi, 2)) /
  //                       (Power(aD, 2) * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2)) +
  //                   (Power(deltaR, 2) * eps * (1 + eps) * Power(F1, 2) *
  //                    Power(Sqrt(2) * Aperp * (-eta + Sqrt(GAM2)) +
  //                              2 * Apara * Sqrt(eps / (1 + eps)) * (1 + Sqrt(GAM2) * xi),
  //                          2)) /
  //                       (4. * Power(aD, 2) * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2)) +
  //                   (Power(deltaF1, 2) * (1 + eps) * Power(1 + eps * R, 2) *
  //                    Power(Sqrt(2) * Aperp * (-eta + Sqrt(GAM2)) +
  //                              2 * Apara * Sqrt(eps / (1 + eps)) * (1 + Sqrt(GAM2) * xi),
  //                          2)) /
  //                       (4. * Power(aD, 2) * eps * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2));
  //    return (Sqrt(dg1_2));
  //  }

  //  double delta_g2(double x, double Q2, double E0, double R, double deltaR, double F1,
  //                  double deltaF1, double Apara, double deltaApara, double Aperp,
  //                  double deltaAperp) {

  //    using namespace TMath;
  //    double nu   = Q2 / (2.0 * (M_p / GeV) * x);
  //    double GAM2 = Q2 / (nu * nu);
  //    double Ep   = insane::kine::Eprime_xQ2y(x, Q2, nu / E0);
  //    double th   = insane::kine::Theta_xQ2y(x, Q2, nu / E0);
  //    double eps  = insane::kine::epsilon(E0, Ep, th);
  //    double aD   = (1.0 / E0) * (E0 - eps * Ep);

  //    double eta = insane::kine::Eta(E0, Ep, th);
  //    double xi  = insane::kine::Xi(E0, Ep, th);
  //    double dg2_2 =
  //        (Power(deltaAperp, 2) * (1 + eps) * Power(F1, 2) * Power(1 + eta * Sqrt(GAM2), 2) *
  //         Power(1 + eps * R, 2)) /
  //            (2. * Power(aD, 2) * eps * GAM2 * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2)) +
  //        (Power(deltaApara, 2) * Power(F1, 2) * Power(1 + eps * R, 2) *
  //         Power(-Sqrt(GAM2) + xi, 2)) /
  //            (Power(aD, 2) * GAM2 * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2)) +
  //        (Power(deltaR, 2) * eps * (1 + eps) * Power(F1, 2) *
  //         Power(Sqrt(2) * Aperp * (1 + eta * Sqrt(GAM2)) +
  //                   2 * Apara * Sqrt(eps / (1 + eps)) * (-Sqrt(GAM2) + xi),
  //               2)) /
  //            (4. * Power(aD, 2) * GAM2 * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2)) +
  //        (Power(deltaF1, 2) * (1 + eps) * Power(1 + eps * R, 2) *
  //         Power(Sqrt(2) * Aperp * (1 + eta * Sqrt(GAM2)) +
  //                   2 * Apara * Sqrt(eps / (1 + eps)) * (-Sqrt(GAM2) + xi),
  //               2)) /
  //            (4. * Power(aD, 2) * eps * GAM2 * Power(1 + GAM2, 2) * Power(1 + eta * xi, 2));
  //    return (Sqrt(dg2_2));
  //  }

