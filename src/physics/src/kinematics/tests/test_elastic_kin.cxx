#include "insane/kinematics/Core.h"
#include "insane/kinematics/Elastic.h"

using namespace insane::kine;
using namespace insane::kine::elastic;

#define CATCH_CONFIG_MAIN
//#include "catch/catch.hpp"
#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

double test_precision = 1.0e-5;

SCENARIO("Elastic ep scattering kinematics", "[kinematics]") {

  GIVEN("A fixed electron beam energy and theta and phi variables") {
    auto initial_vars = make_independent_vars<theta_v, phi_v>().add<E_beam_v>(
        [](const auto& v) constexpr { return 10.0; });

    WHEN("Using the the helper function in kinematics/Elastic.h to add all kinematic variables to "
         "the stack") {

      //auto elastic_vars = initial_vars.add<M_recoil_v, E_prime_v, Q2_v, nu_v,W_v>(add_func1);
      auto elastic_vars = initial_vars.add<ElasticPrimaryVars>(add_ElasticPrimaryVars)
//.add<ElasticVectorVars>(add_ElasticVectorVars)
      ;

      THEN("Compute for E0=10.0 GeV, E'=4.7, and theta=17 degrees") {
        auto input = std::make_tuple( theta_v{17.0 * M_PI / 180.0}, phi_v{0.0});

        auto values = elastic_vars.ComputeValues(input);

        const auto Ebeam = std::get<E_beam_v>(values);
        const auto Ep    = std::get<E_prime_v>(values);
        const auto theta = std::get<theta_v>(values) * 180.0 / M_PI;
        const auto Q2    = std::get<Q2_v>(values);
        // const auto theta_recoil = std::get<theta_recoil_v>(values)* 180.0 / M_PI;

        // could probably make this better, like: values.get<x_v>()
        std::cout << " E0  = " << std::get<E_beam_v>(values)               << "\n";
        std::cout << " E'  = " << std::get<E_prime_v>(values)              << "\n";
        std::cout << " th  = " << std::get<theta_v>(values) * 180.0 / M_PI << "\n";
        std::cout << " Q2  = " << std::get<Q2_v>(values)                   << "\n";
        //std::cout << " th_r= " << std::get<theta_recoil_v>(values) * 180.0 / M_PI << "\n";
        //std::cout << " x   = " << std::get<x_v>(values)                    << "\n";
        //std::cout << " W   = " << std::get<W_v>(values)                    << "\n";
        //std::cout << " y   = " << std::get<y_v>(values)                    << "\n";
        //std::cout << " nu  = " << std::get<nu_v>(values)                   << "\n";

        REQUIRE( std::abs( Ebeam - 10.0 ) < test_precision );
        //REQUIRE( std::abs( Ep - 4.7 ) < test_precision );
        REQUIRE( std::abs( theta - 17.0 ) < test_precision );
        REQUIRE( std::abs( Q2 - 5.96238 ) < test_precision );
      }
    }
  }
}
