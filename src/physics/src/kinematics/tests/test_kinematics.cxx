#include "insane/kinematics/Core.h"

using namespace insane::kine;

#define CATCH_CONFIG_MAIN
#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

double test_precision = 1.0e-5;

SCENARIO("Variable properties", "[Kinematics]") {
  GIVEN("A variable should have some dimension") {
    WHEN("variable is simple scaler") {
    using E_beam_v  = Var<struct E_beam_tag>;
    using Omega_v   = VarVec<struct Omega_tag,2>;
    using ThreeMom_v   = VarVec<struct ThreeMom_v_tag,3>;
      THEN("it should have dimension 1."){
        REQUIRE( sizeof(E_beam_v)/sizeof(double) == 1);
        REQUIRE( sizeof(Omega_v)/sizeof(double) == 2);
        REQUIRE( sizeof(ThreeMom_v)/sizeof(double) == 3);
      }
    }
  }
}

SCENARIO("DIS ", "[Kinematics]") {

  GIVEN("Beam energy, E', and theta as independent variables.") {
    // Note in the future meta classes might help with the type/tag declarations
    using E_beam_v  = Var<struct E_beam_tag>;
    using E_prime_v = Var<struct E_prime_tag>;
    using theta_v   = Var<struct theta_tag>;

    WHEN("Adding other kinematic variables") {
      using Q2_v = Var<struct Q2_tag>;
      using nu_v = Var<struct nu_tag>;
      using x_v  = Var<struct x_tag>;
      using y_v  = Var<struct y_tag>;
      using W_v  = Var<struct W_tag>;

      double M_p = 0.938;

      auto measured_vars = make_independent_vars<E_beam_v, E_prime_v, theta_v>();

      auto dis_kine_vars = measured_vars
                               .add<Q2_v, nu_v>(
                                   [](const auto& v) constexpr {
                                     const auto&  E0    = std::get<E_beam_v>(v);
                                     const auto&  Ep    = std::get<E_prime_v>(v);
                                     const auto&  th    = std::get<theta_v>(v);
                                     const double M_p   = 0.938;
                                     const double sinth = std::sin(th / 2.0);
                                     return 4.0 * E0 * Ep * sinth * sinth;
                                   },
                                   [](const auto& v) constexpr {
                                     const auto& E0 = std::get<E_beam_v>(v);
                                     const auto& Ep = std::get<E_prime_v>(v);
                                     return E0 - Ep;
                                   }) // note that we are chaining here.
                               .add<x_v, y_v>(
                                   [](const auto& v) constexpr {
                                     // calculate x
                                     const auto& Q2 = std::get<Q2_v>(v);
                                     const auto& nu = std::get<nu_v>(v);
                                     // std::cout << Q2 << "\n";
                                     // std::cout << nu << "\n";
                                     double M_p = 0.938;
                                     return Q2 / (2.0 * M_p * nu);
                                   },
                                   [](const auto& v) constexpr {
                                     // calculate y
                                     const auto& E0 = std::get<E_beam_v>(v);
                                     const auto& nu = std::get<nu_v>(v);
                                     return nu / E0;
                                   })
                               .add<W_v>([](const auto& v) constexpr {
                                 // calculate W
                                 const double M_p = 0.938;
                                 const auto&  Q2  = std::get<Q2_v>(v);
                                 const auto&  nu  = std::get<nu_v>(v);
                                 return std::sqrt(M_p * M_p - Q2 + 2.0 * M_p * nu);
                               });

      THEN("Compute for E0=10.6 GeV, E'=4.7, and theta=17 degrees") {
        auto input = std::make_tuple(E_beam_v{10.6}, E_prime_v{4.7}, theta_v{17.0 * M_PI / 180.0});

        auto dis_values = dis_kine_vars.ComputeValues(input);

        const auto Ebeam = std::get<E_beam_v>(dis_values);
        const auto Ep = std::get<E_prime_v>(dis_values);
        const auto theta = std::get<theta_v>(dis_values)* 180.0 / M_PI;
        const auto Q2 = std::get<Q2_v>(dis_values);
        const auto x = std::get<x_v>(dis_values);
        const auto W = std::get<W_v>(dis_values);
        const auto y = std::get<y_v>(dis_values);
        const auto nu = std::get<nu_v>(dis_values);

        // could probably make this better, like: dis_values.get<x_v>()
        //std::cout << " E0  = " << std::get<E_beam_v>(dis_values)               << "\n";
        //std::cout << " E'  = " << std::get<E_prime_v>(dis_values)              << "\n";
        //std::cout << " th  = " << std::get<theta_v>(dis_values) * 180.0 / M_PI << "\n";
        //std::cout << " Q2  = " << std::get<Q2_v>(dis_values)                   << "\n";
        //std::cout << " x   = " << std::get<x_v>(dis_values)                    << "\n";
        //std::cout << " W   = " << std::get<W_v>(dis_values)                    << "\n";
        //std::cout << " y   = " << std::get<y_v>(dis_values)                    << "\n";
        //std::cout << " nu  = " << std::get<nu_v>(dis_values)                   << "\n";

        REQUIRE( std::abs( Ebeam - 10.6 ) < test_precision );
        REQUIRE( std::abs( Ep - 4.7 ) < test_precision );
        REQUIRE( std::abs( theta - 17.0 ) < test_precision );
        REQUIRE( std::abs( Q2 - 4.35379 ) < test_precision );
        REQUIRE( std::abs( nu - 5.9 ) < test_precision );
        REQUIRE( std::abs( W - 2.7558 ) < test_precision );
        REQUIRE( std::abs( x - 0.393354 ) < test_precision );
        REQUIRE( std::abs( y - 0.556604 ) < test_precision );
      }
    }
    using Omega_v   = VarVec<struct Omega_v_tag,2>;

    WHEN("using a 2 dimensional variables (Omega)") {
      using Q2_v = Var<struct Q2_tag>;
      using nu_v = Var<struct nu_tag>;
      using x_v  = Var<struct x_tag>;
      using y_v  = Var<struct y_tag>;
      using W_v  = Var<struct W_tag>;

      double M_p = 0.938;

      auto measured_vars = make_independent_vars<E_beam_v, E_prime_v, Omega_v>();

      auto dis_kine_vars = measured_vars
                               .add<Q2_v, nu_v>(
                                   [](const auto& v) constexpr {
                                     const auto&  E0    = std::get<E_beam_v>(v);
                                     const auto&  Ep    = std::get<E_prime_v>(v);
                                     const auto&  th    = std::get<Omega_v>(v)[0];
                                     const double M_p   = 0.938;
                                     const double sinth = std::sin(th / 2.0);
                                     return 4.0 * E0 * Ep * sinth * sinth;
                                   },
                                   [](const auto& v) constexpr {
                                     const auto& E0 = std::get<E_beam_v>(v);
                                     const auto& Ep = std::get<E_prime_v>(v);
                                     return E0 - Ep;
                                   }) // note that we are chaining here.
                               .add<x_v, y_v>(
                                   [](const auto& v) constexpr {
                                     // calculate x
                                     const auto& Q2 = std::get<Q2_v>(v);
                                     const auto& nu = std::get<nu_v>(v);
                                     // std::cout << Q2 << "\n";
                                     // std::cout << nu << "\n";
                                     double M_p = 0.938;
                                     return Q2 / (2.0 * M_p * nu);
                                   },
                                   [](const auto& v) constexpr {
                                     // calculate y
                                     const auto& E0 = std::get<E_beam_v>(v);
                                     const auto& nu = std::get<nu_v>(v);
                                     return nu / E0;
                                   })
                               .add<W_v>([](const auto& v) constexpr {
                                 // calculate W
                                 const double M_p = 0.938;
                                 const auto&  Q2  = std::get<Q2_v>(v);
                                 const auto&  nu  = std::get<nu_v>(v);
                                 return std::sqrt(M_p * M_p - Q2 + 2.0 * M_p * nu);
                               });

      THEN("Compute for E0=10.6 GeV, E'=4.7, and theta=17 degrees") {
        auto input = std::make_tuple(E_beam_v{10.6}, E_prime_v{4.7}, Omega_v{{17.0 * M_PI / 180.0,M_PI}});

        auto dis_values = dis_kine_vars.ComputeValues(input);

        const auto Ebeam = std::get<E_beam_v>(dis_values);
        const auto Ep = std::get<E_prime_v>(dis_values);
        const auto theta = std::get<Omega_v>(dis_values)[0]* 180.0 / M_PI;
        const auto phi = std::get<Omega_v>(dis_values)[1]* 180.0 / M_PI;
        const auto Q2 = std::get<Q2_v>(dis_values);
        const auto x = std::get<x_v>(dis_values);
        const auto W = std::get<W_v>(dis_values);
        const auto y = std::get<y_v>(dis_values);
        const auto nu = std::get<nu_v>(dis_values);

        // could probably make this better, like: dis_values.get<x_v>()
        //std::cout << " E0  = " << std::get<E_beam_v>(dis_values)               << "\n";
        //std::cout << " E'  = " << std::get<E_prime_v>(dis_values)              << "\n";
        //std::cout << " th  = " << std::get<theta_v>(dis_values) * 180.0 / M_PI << "\n";
        //std::cout << " Q2  = " << std::get<Q2_v>(dis_values)                   << "\n";
        //std::cout << " x   = " << std::get<x_v>(dis_values)                    << "\n";
        //std::cout << " W   = " << std::get<W_v>(dis_values)                    << "\n";
        //std::cout << " y   = " << std::get<y_v>(dis_values)                    << "\n";
        //std::cout << " nu  = " << std::get<nu_v>(dis_values)                   << "\n";

        REQUIRE( std::abs( Ebeam - 10.6 ) < test_precision );
        REQUIRE( std::abs( Ep - 4.7 ) < test_precision );
        REQUIRE( std::abs( theta - 17.0 ) < test_precision );
        REQUIRE( std::abs( phi - 180.0 ) < test_precision );
        REQUIRE( std::abs( Q2 - 4.35379 ) < test_precision );
        REQUIRE( std::abs( nu - 5.9 ) < test_precision );
        REQUIRE( std::abs( W - 2.7558 ) < test_precision );
        REQUIRE( std::abs( x - 0.393354 ) < test_precision );
        REQUIRE( std::abs( y - 0.556604 ) < test_precision );
      }
    }
  }
}
