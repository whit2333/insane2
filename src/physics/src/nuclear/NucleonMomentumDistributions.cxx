#include "insane/nuclear/NucelonMomentumDistributions.h"
#include "TMath.h"
#include "insane/base/PhysicalConstants.h"
#include <iostream>

using namespace insane::physics;

NucelonMomentumDistributions::NucelonMomentumDistributions(){
}
//______________________________________________________________________________
NucelonMomentumDistributions::~NucelonMomentumDistributions(){
}
//______________________________________________________________________________
double NucelonMomentumDistributions::n(
      double p,
      int A ) const
{
   // Nucleon momentum distribution (units: [GeV^-3]) as function of p [GeV] 
   // Note: This implements the shape of the distribution.
   // The normalization is taken care of  with calls to operator() or Evaluate()
   double res = 0;
   // fermi3_ uses MeV. 
   double P = 1000.0*p;
   fermi3_(&P,&A,&res);
   res *= 1.0e9;
   //res *= 2.0*insane::units::twopi;
   return (res) ;
}
//______________________________________________________________________________
double NucelonMomentumDistributions::GetNorm( int A ) const
{
   // Calculates the normalization such that \/int dp^3 n(\vec{p}) = 1 
   auto search = fNorms.find(A);
   if( fNorms.count(A) == 0){
   //if(search == fNorms.end()) {
      fNorms[A] = CalculateNorm(A);
      //std::cout << "norm : " << fNorms[A] << "\n";
   }
   return fNorms[A];
}
//______________________________________________________________________________
double NucelonMomentumDistributions::CalculateNorm(int A) const
{
   // Calculates the normalization such that \/int dp^3 n(\vec{p}) = 1 
   // Note the spherically symmetric integral is
   // \int_0^\infty 4\pi k^2 n(k) dk = 1
   int    Np   = 200;
   double kmin = 0.0;
   double kmax = 5.0;
   double dk   = (kmax-kmin)/(double(Np));
   double res  = 0.;

   for(int i = 0; i<Np; i++) {
      double k = kmin + 0.5*dk + double(i)*dk;
      res += dk*n(k,A)*k*k;
   }
   //std::cout <<  "GetNorm(" << A << ") = " << GetNorm(A) << "\n";
   res *= 2.0*insane::units::twopi;
   return res;
}
//______________________________________________________________________________
double NucelonMomentumDistributions::Evaluate(double k, int A) const
{
   //std::cout <<  "GetNorm(" << A << ") = " << GetNorm(A) << "\n";
   double res  = k*k*n(k,A)/GetNorm(A);
   return (res) ;
}
//______________________________________________________________________________
double NucelonMomentumDistributions::operator() (double *x, double *p)
{
   // function implementation using class data members
   double P = x[0];
   int    A = p[0];
   double res = 0;
   //std::cout << " A=" << A << std::endl;
   fermi3_(&P,&A,&res);
   return(res);
}
//______________________________________________________________________________
double NucelonMomentumDistributions::GetMomentum(double P, int A) {
   double P0 = P*1000.0;
   double res  = 0.0;
   fermi3_(&P,&A,&res);
   return (res) ;
}
//______________________________________________________________________________
double NucelonMomentumDistributions::GetProb(double P, int A) const {
   // Takes arguments in GeV but fermi3_ uses MeV. 
   double res = P*P*n(P,A);
   res *= 2.0*insane::units::twopi;

   //double k = P/hbarc_gev_fm;
   //double P0 = P*1000.0;
   //double res  = 0.0;
   //fermi3_(&P,&A,&res);
   //res *= (1.0e6);
   //return res;
   //res *= 4.0*TMath::Pi();
   //return (k*k*res/hbarc_gev_fm) ;
}
//______________________________________________________________________________

