#include "insane/experiment/Target.h"
#include "TGeoBBox.h"
#include "TGeoManager.h"

namespace insane {

//_______________________________________________________________________________
Target::Target(const char * name, const char * title) : TNamed(name, title) {
   fMaterials.Clear();
   fMaterials.SetOwner(true);
   fNMaterials = 0;
   fPackingFraction = 1.0;

   auto * vacuum_mat  = new TGeoMaterial("vacuum_mat",0,0,0);
   auto   * vacuum_med  = new TGeoMedium("vacuum_med",0,vacuum_mat);
   //TGeoBBox     * box         = new TGeoBBox(100,100,100); // 2x2x2 m^3
   fTopVolume                 =  gGeoManager->MakeBox("Top",vacuum_med,100.,100.,100.);//new TGeoVolume("topVolume",box, vacuum_med);
   gGeoManager->SetTopVolume(fTopVolume);
   //fBremRadiator = new BremsstrahlungRadiator();
}
//_______________________________________________________________________________
Target::~Target(){
}
//______________________________________________________________________________
Target::Target(const Target & tg) : TNamed(tg) {
   fMaterials.AddAll( &(tg.fMaterials));
   fMaterials.SetOwner(true);
   fNMaterials  = tg.fNMaterials;
}
//______________________________________________________________________________
Target::Target(const Target * tg) : TNamed(*tg) {
   // should do a deep copy?
   fMaterials.AddAll( &(tg->fMaterials));
   fMaterials.SetOwner(true);
   fNMaterials  = tg->fNMaterials;
}
//________________________________________________________________________________
Target& Target::operator=(const Target& tg){
   if (this != &tg) {
      fMaterials.AddAll( &(tg.fMaterials));
      fMaterials.SetOwner(true);
      fNMaterials  = tg.fNMaterials;
   }
   return *this;
}
//_______________________________________________________________________________
void Target::InitFromFile(){
   //fBremRadiator = new BremsstrahlungRadiator();
   //for (int i = 0; i < fMaterials.GetEntries(); i++) {
   //   TargetMaterial* mat = GetMaterial(i);
   //   fBremRadiator->AddRadiator(mat);
   //}
}
//_______________________________________________________________________________
void Target::Print(const Option_t * opt ) const {
   std::cout << "Target : " << GetTitle() << "  (" << GetName() << ")" << std::endl;
   Double_t rl_tot=0.0;
   for (int i = 0; i < fMaterials.GetEntries(); i++) {
      TargetMaterial* mat = GetMaterial(i);
      mat->Print();
      rl_tot += mat->GetNumberOfRadiationLengths();
   }
   std::cout << " Total thickness = " << rl_tot << " (radiation lengths)" << std::endl;
   //fBremRadiator.Print();

}
//_______________________________________________________________________________

void Target::Print(std::ostream &stream) const 
{
   stream << "Target : " << GetTitle() << "  (" << GetName() << ")" << std::endl;
   Double_t rl_tot=0.0;
   for (int i = 0; i < fMaterials.GetEntries(); i++) {
      TargetMaterial* mat = GetMaterial(i);
      mat->Print(stream);
      rl_tot += mat->GetNumberOfRadiationLengths();
   }
   stream << " Total thickness = " << rl_tot << " (radiation lengths)" << std::endl;
   //fBremRadiator.Print(stream);

}
//_______________________________________________________________________________
void Target::AddMaterial(TargetMaterial * mat) {
   mat->Update();
   mat->SetMatID(fNMaterials);
   fMaterials.Add(mat);
   fNMaterials++;
   //fBremRadiator.AddRadiator(mat);
}
//______________________________________________________________________________
TargetMaterial * Target::GetMaterial(const char * name) const {
   return (TargetMaterial*)(fMaterials.FindObject(name)) ;
}
//______________________________________________________________________________
TargetMaterial * Target::GetMaterial(Int_t i) const {
   if (i >= GetNMaterials())  {
      Error("GetMaterial(Int_t i)","Material number i=%d too large. There are %d materials for this target.",i,GetNMaterials());
      return(nullptr);
   }
   return (TargetMaterial*)(fMaterials.At(i)) ;
}
//______________________________________________________________________________
Double_t Target::GetAttenuationLength() const {
   Double_t res = 0;
   for(int i = 0; i<GetNMaterials() ; i++) {
      res += GetMaterial(i)->GetAttenuationLength() ; 
   }
   return(res);
}
//______________________________________________________________________________
Double_t Target::GetRadiationLength() const {
   //Double_t l0         = 0.0
   Double_t lrho0      = 0.0; // sum L_i*rho_i
   Double_t lrhoOverX0 = 0.0; // sum L_i*rho_i/X_i = L_0*rho_0/X_0, where X_0 is the total target radiation length
   for(int i = 0; i<GetNMaterials() ; i++) {
      Double_t li    = GetMaterial(i)->GetLength();
      Double_t rhoi  = GetMaterial(i)->GetDensity();
      Double_t lrhoi = li*rhoi;
      Double_t Xi    = GetMaterial(i)->GetRadiationLength();
      lrho0         += lrhoi;
      lrhoOverX0    += lrhoi/Xi;
      //Double_t l = GetMaterial(i)->GetAttenuationLength();
      //res += (1.0 - TMath::Exp(-1.0*GetMaterial(i)->GetLength()/l)) ; 
   }
   return lrho0/lrhoOverX0;
}
//______________________________________________________________________________
Double_t Target::GetLengthDensity() const {
   Double_t tot = 0.0; 
   for(int i = 0; i<GetNMaterials() ; i++) {
      tot += GetMaterial(i)->GetLengthDensity();
   }
   return(tot);
}
//______________________________________________________________________________
Double_t Target::GetNumberOfRadiationLengths() const { //Double_t l0         = 0.0
   //Double_t lrho0      = 0.0; // sum L_i*rho_i
   Double_t lrhoOverX0 = 0.0; // sum L_i*rho_i/X_i = L_0*rho_0/X_0, where X_0 is the total target radiation length
   for(int i = 0; i<GetNMaterials() ; i++) {
      //Double_t l0   += GetMaterial(i)->GetLength();
      //Double_t lrhoi = GetMaterial(i)->GetLengthDensity();
      Double_t li    = GetMaterial(i)->GetLength();
      Double_t rhoi  = GetMaterial(i)->GetDensity();
      Double_t lrhoi = li*rhoi;
      Double_t Xi    = GetMaterial(i)->GetRadiationLength();
      //lrho0         += lrhoi;
      lrhoOverX0    += lrhoi/Xi;
      //Double_t l = GetMaterial(i)->GetAttenuationLength();
      //res += (1.0 - TMath::Exp(-1.0*GetMaterial(i)->GetLength()/l)) ; 
   }
   //Double_t res = lrho0/lrhoOverX0;
   return(lrhoOverX0);
}
//______________________________________________________________________________
void     Target::DrawTarget(Option_t * opt) {

   for(int i = 0; i<GetNMaterials() ; i++) {
      TargetMaterial * mat = GetMaterial(i);
      fTopVolume->AddNodeOverlap(mat->fTGeoVolume,1,mat->fTGeoMatrix);
   }

   gGeoManager->CloseGeometry();
   gGeoManager->SetTopVisible(0);
   fTopVolume->Draw(opt);

}
//______________________________________________________________________________



//_______________________________________________________________________________
SimpleTarget::SimpleTarget(const char * name, const char * title) : Target(name, title) {
   DefineMaterials();
}
//_______________________________________________________________________________
SimpleTarget::~SimpleTarget() {
}
//_______________________________________________________________________________
void SimpleTarget::DefineMaterials() {
   auto * matLH2  = new TargetMaterial("LH2", "LH2", 1, 1);
   matLH2->fLength                = 1.0;      //cm
   matLH2->fZposition             = 0.0;      //cm
   matLH2->fDensity               = 0.07085;  // g/cm3
   matLH2->fIsPolarized           = false;

   this->AddMaterial(matLH2);
}
//_______________________________________________________________________________



//_______________________________________________________________________________

SimpleTargetWithWindows::SimpleTargetWithWindows(const char * name, const char * title) : Target(name, title) {
   DefineMaterials();
}
//_______________________________________________________________________________
SimpleTargetWithWindows::~SimpleTargetWithWindows() {
}
//_______________________________________________________________________________
void SimpleTargetWithWindows::DefineMaterials() {
   auto * matAl1 = new TargetMaterial("window1", "Aluminum  upstream window", 13, 27);
   matAl1->fLength               = 0.05  ;  //cm
   matAl1->fDensity              = 2.7;              // g/cm3
   matAl1->fZposition            =-0.5-0.05/2.0 ;     //cm
   this->AddMaterial(matAl1);

   auto * matLH2  = new TargetMaterial("LH2", "LH2", 1, 1);
   matLH2->fLength                = 1.0;      //cm
   matLH2->fZposition             = 0.0;      //cm
   matLH2->fDensity               = 0.07085;  // g/cm3
   matLH2->fIsPolarized           = false;
   this->AddMaterial(matLH2);

   auto * matAl2 = new TargetMaterial("window2", "Aluminum  downstream window", 13, 27);
   matAl2->fLength               = 0.05  ;  //cm
   matAl2->fDensity              = 2.7;              // g/cm3
   matAl2->fZposition            = 0.5+0.05/2.0 ;     //cm
   this->AddMaterial(matAl2);

   matAl1->fTGeoVolume->SetFillColor(4);
   matAl1->fTGeoVolume->SetLineColor(4);
   matAl2->fTGeoVolume->SetFillColor(2);
   matAl2->fTGeoVolume->SetLineColor(2);

   //fBremRadiator.Print();

}
//_______________________________________________________________________________

}
