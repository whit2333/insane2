#include "insane/experiment/DilutionFactor.h"

namespace insane {
namespace physics {

DilutionFactor::DilutionFactor(const char * n , const char * t) :TNamed(n,t) {
   fDilution = 0.15;
   fDilutionError = 0.0;
   fQ2 = 2.0;
   fW = 2.0;
   fx = 0.0;
   fPackingFraction = 0.6;
}
//_____________________________________________________________________________
DilutionFactor::~DilutionFactor() {
}

//_____________________________________________________________________________
void DilutionFactor::Print(Option_t *) const {

   std::cout << "Dilution = " << fDilution << std::endl;
   std::cout << "   error = " << fDilutionError << std::endl;
   std::cout << "       W = " << fW << std::endl;
   std::cout << "       x = " << fx << std::endl;
   std::cout << "      Q2 = " << fQ2 << std::endl;
}

//_____________________________________________________________________________

//_____________________________________________________________________________

DilutionFunction::DilutionFunction(const char * n, const char * t): TNamed(n, t)
{
   fQ2_bin = 0.0;
   fQ2_min = 0.0;
   fQ2_max = 0.0;
   fGraph = nullptr;
   fNBins = 0;
   fPar[0] = 0.150;
   fPar[1] = 0.0;
   fPar[2] = 0.0;
   fDfVsx = nullptr;
   fDfVsW = nullptr;
}
//_____________________________________________________________________________

DilutionFunction::~DilutionFunction() {
   if (fGraph) delete fGraph;
}
//_____________________________________________________________________________

void DilutionFunction::InitGraph() {
   TH1::AddDirectory(kFALSE);
   if (fGraph) delete fGraph;
   fGraph = new TGraph2D(fNBins);
   fGraph->SetNameTitle("dfvsxAndW", "df(x,W)");
   fDfVsx = new TGraphErrors(fNBins);
   fDfVsx->SetNameTitle("fDfVsx", "df vs x");
   fDfVsW = new TGraphErrors(fNBins);
   fDfVsW->SetNameTitle("fDfVsW", "df vs W");
   TH1::AddDirectory(kTRUE);
}
//_____________________________________________________________________________

void DilutionFunction::Add(DilutionFactor * df)
{
   /*     TH1::AddDirectory(kFALSE);*/
   fDFs.Add(df);
   if (!fGraph) InitGraph();
   fGraph->SetPoint(fDFs.GetEntries() - 1, df->Getx(), df->GetW(), df->GetDilution());

   fDfVsx->SetPoint(fDFs.GetEntries() - 1, df->Getx(), df->GetDilution());
   fDfVsx->SetPointError(fDFs.GetEntries() - 1, 0, df->GetDilutionError());

   fDfVsW->SetPoint(fDFs.GetEntries() - 1, df->GetW(), df->GetDilution());
   fDfVsW->SetPointError(fDFs.GetEntries() - 1, 0, df->GetDilutionError());
   /*     TH1::AddDirectory(kTRUE);*/
}
//_____________________________________________________________________________

void DilutionFunction::FitGraphs()
{
   /*     TH1::AddDirectory(kFALSE);*/
   const char * xn = Form("dfVx-%s", GetName());
   fxFit = new TF1(xn, "[0]+[1]*x+[2]*x*x", 0, 1);
   if (fDfVsx) fDfVsx->Fit(xn);

   const char * Wn = Form("dfVW-%s", GetName());
   fWFit = new TF1(Wn, "[0]+[1]*x+[2]*x*x", 0, 1);
   if (fDfVsW) fDfVsW->Fit(Wn);

   fFuncs.Add(fxFit);
   fFuncs.Add(fWFit);

//      TH1::AddDirectory(kTRUE);

}
//_____________________________________________________________________________

void  DilutionFunction::Draw(Option_t* option)
{
   if (fDfVsx) {
      new TCanvas();
      fDfVsx->Draw("ape");
   }
}
//_____________________________________________________________________________
Double_t DilutionFunction::GetXFitResult(Double_t x) {
   if (!fxFit) FitGraphs();
   return(fxFit->Eval(x));
}
//_____________________________________________________________________________
Double_t DilutionFunction::GetWFitResult(Double_t W) {
   if (!fWFit) FitGraphs();
   return(fWFit->Eval(W));
}
//_____________________________________________________________________________
Double_t DilutionFunction::GetDilution(Double_t x) {
   Double_t res =  fPar[0] + fPar[1] * x + fPar[2] * x * x ;
   if (res > 1.0 || res < 0.0) Warning("GetDilution", "Dilution value is out of range: %f ", res);
   return(res);
}
//_____________________________________________________________________________


DilutionFromTarget::DilutionFromTarget(const char * n, const char * t)
   : DilutionFunction(n, t) {
}
//_____________________________________________________________________________
DilutionFromTarget::~DilutionFromTarget() {
}

//_____________________________________________________________________________
Double_t DilutionFromTarget::GetTotalRate(const Double_t x, const Double_t Q2) {
   if (!fTarget) {
      Error("GetTotalRate", "No Target Defined");
      return(0.0);
   }
   Double_t result = 0.0;
   //std::cout << "rates: " ; 
   for (int i = 0; i < fTarget->GetNMaterials(); i++) {
      auto* aMat = (TargetMaterial*) fTarget->GetMaterial(i);
      Double_t arate = 0.0;//aMat->GetRate(x, Q2) ;
      result += arate;
      //std::cout << arate << " " ;
   }
   //std::cout << std::endl;
   //std::cout << "tot rate: " << result << std::endl;
   return(result);
}
//_____________________________________________________________________________
Double_t DilutionFromTarget::GetPolarizedRate(const Double_t x, const Double_t Q2) {
   if (!fTarget) {
      Error("GetTotalRate", "No Target Defined");
      return(0.0);
   }
   Double_t result = 0.0;
   for (int i = 0; i < fTarget->GetNMaterials(); i++) {
      auto* aMat = (TargetMaterial*) fTarget->GetMaterial(i);
      if (aMat->IsPolarized()) result += 0.0;//aMat->GetRate(x, Q2);
   }
   //std::cout << "Pol rate: " << result << std::endl;
   return(result);
}
//_____________________________________________________________________________
Double_t DilutionFromTarget::GetDilution_WQ2(const Double_t W, const Double_t Q2) {
   Double_t x = insane::kine::xBjorken_WQsq(W,Q2); 
   return GetDilution(x,Q2);
}
//_____________________________________________________________________________
Double_t DilutionFromTarget::GetDilution_xW(const Double_t x, const Double_t W) {
   Double_t Q2 = insane::kine::Q2_xW(x,W); 
   return GetDilution(x,Q2);
}
//_____________________________________________________________________________
Double_t DilutionFromTarget::GetDilution(const Double_t x, const Double_t Q2) {
   Double_t res = GetPolarizedRate(x, Q2) / GetTotalRate(x, Q2);
   if( TMath::IsNaN(res) ) res = 0.000000001;
   return res;
}
//______________________________________________________________________________
Double_t DilutionFromTarget::GetDilution(const TParticle * beam, const TParticle * scat) {
   Double_t res = GetPolarizedRate(beam, scat) / GetTotalRate(beam, scat);
   if( TMath::IsNaN(res) ) res = 0.000000001;
   return res;
}
//______________________________________________________________________________
Double_t DilutionFromTarget::GetTotalRate(const TParticle * beam, const TParticle * scat) {
   if (!fTarget) {
      Error("GetTotalRate", "No Target Defined");
      return(0.0);
   }
   Double_t result = 0.0;
   for (int i = 0; i < fTarget->GetNMaterials(); i++) {
      auto * aMat  = (TargetMaterial*) fTarget->GetMaterial(i);
      Double_t               arate = 0.0;//aMat->GetRate(beam, scat) ;
      result += arate;
   }
   return(result);
}
//______________________________________________________________________________
Double_t DilutionFromTarget::GetPolarizedRate(const TParticle * beam, const TParticle * scat) {
   if (!fTarget) {
      Error("GetTotalRate", "No Target Defined");
      return(0.0);
   }
   Double_t result = 0.0;
   for (int i = 0; i < fTarget->GetNMaterials(); i++) {
      auto* aMat = (TargetMaterial*) fTarget->GetMaterial(i);
      if (aMat->IsPolarized()) result += 0.0;//aMat->GetRate(beam, scat);
   }
   return(result);
}
//_____________________________________________________________________________
double DilutionFromTarget::GetDilutionFromx(double *x, double *p) {
   double res = GetDilution(x[0], p[0]);
   if (res != res) res = 0.01;
   /*      std::cout << "x= " << x[0] << ", Q2=" << p[0] << ", res=" << res << "\n";*/
   return(res);
}
//_____________________________________________________________________________
double DilutionFromTarget::GetDilutionFromW(double *x, double *p) {
   double res = GetDilution(insane::kine::xBjorken_WQsq(x[0], p[0]), p[0]);
   if (res != res) res = 0.01;
   /*      std::cout << "W= " << x[0] << ", x= " << ::Kine::xBjorken_WQsq(x[0],p[0]) << ", Q2=" << p[0] << ", res=" << res << "\n";*/
   return(res);
   return(GetDilution(insane::kine::xBjorken_WQsq(x[0], p[0]) , p[0]));
}
//_____________________________________________________________________________

}
}
