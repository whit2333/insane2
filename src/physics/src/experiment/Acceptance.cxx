#include "insane/experiment/Acceptance.h"
#include "TRandom3.h"
#include "insane/kinematics/KinematicFunctions.h"

namespace insane {
namespace physics {

Acceptance::Acceptance(const char * n , const char * t) : TNamed(n,t) {
   fFunctions.Clear();

   fBeamEnergy = 5.9;
   fEnergyMin  = 0.5;
   fEnergyMax  = 5.0;
   fThetaMin   = 30.0*TMath::Pi()/180.0;
   fThetaMax   = 50.0*TMath::Pi()/180.0;

}
//___________________________________________________________________________


Acceptance::~Acceptance(){

}

//___________________________________________________________________________

void Acceptance::Initialize(){
    //TF1 * f = 0;
    Double_t x,Q2,W,E,Eprime,theta;

    fEnergy = (fEnergyMax+fEnergyMin)/2.0;
    fTheta  = (fThetaMax+fThetaMin)/2.0;

    fxMin =  999999.0;
    fxMax = -999999.0;
    fWMin =  999999.0;
    fWMax = -999999.0;
    fQ2Min =  999999.0;
    fQ2Max = -999999.0;
    
    //1 
    E      = fBeamEnergy;
    Eprime = fEnergyMin;
    theta  = fThetaMin;
 
    Q2     = insane::kine::Qsquared(E,Eprime,theta);
    x      = insane::kine::xBjorken_EEprimeTheta(E,Eprime,theta);
    W      = insane::kine::W_EEprimeTheta(E,Eprime,theta); 
    if( x < fxMin ) fxMin = x;
    if( x > fxMax ) fxMax = x;
    if( Q2 < fQ2Min ) fQ2Min = Q2;
    if( Q2 > fQ2Max ) fQ2Max = Q2;
    if( W < fWMin ) fWMin = W;
    if( W > fWMax ) fWMax = W;
    
    //2
    E      = fBeamEnergy;
    Eprime = fEnergyMin;
    theta  = fThetaMax;

    Q2     = insane::kine::Qsquared(E,Eprime,theta);
    x      = insane::kine::xBjorken_EEprimeTheta(E,Eprime,theta);
    W      = insane::kine::W_EEprimeTheta(E,Eprime,theta); 
    if( x < fxMin ) fxMin = x;
    if( x > fxMax ) fxMax = x;
    if( Q2 < fQ2Min ) fQ2Min = Q2;
    if( Q2 > fQ2Max ) fQ2Max = Q2;
    if( W < fWMin ) fWMin = W;
    if( W > fWMax ) fWMax = W;

    //3
    E      = fBeamEnergy;
    Eprime = fEnergyMax;
    theta  = fThetaMax;

    Q2     = insane::kine::Qsquared(E,Eprime,theta);
    x      = insane::kine::xBjorken_EEprimeTheta(E,Eprime,theta);
    W      = insane::kine::W_EEprimeTheta(E,Eprime,theta); 
    if( x < fxMin ) fxMin = x;
    if( x > fxMax ) fxMax = x;
    if( Q2 < fQ2Min ) fQ2Min = Q2;
    if( Q2 > fQ2Max ) fQ2Max = Q2;
    if( W < fWMin ) fWMin = W;
    if( W > fWMax ) fWMax = W;

    //4
    E      = fBeamEnergy;
    Eprime = fEnergyMax;
    theta  = fThetaMin;

    Q2     = insane::kine::Qsquared(E,Eprime,theta);
    x      = insane::kine::xBjorken_EEprimeTheta(E,Eprime,theta);
    W      = insane::kine::W_EEprimeTheta(E,Eprime,theta); 
    if( x < fxMin ) fxMin = x;
    if( x > fxMax ) fxMax = x;
    if( Q2 < fQ2Min ) fQ2Min = Q2;
    if( Q2 > fQ2Max ) fQ2Max = Q2;
    if( W < fWMin ) fWMin = W;
    if( W > fWMax ) fWMax = W;


}
//___________________________________________________________________________
TH2F * Acceptance::KinematicCoverage_xQ2(Int_t N) {
   TRandom3 rand;

   auto * h2 = new TH2F(Form("%s-Q2Vsx",GetName()),Form("%s - Q2 Vs x",GetTitle() ), 100,fxMin,fxMax,100,fQ2Min,fQ2Max);

   Double_t x,W,Q2,e,eprime,theta;

   for(int i = 0; i <N ;i++) {

      e = fBeamEnergy;
      eprime = rand.Uniform(fEnergyMin,fEnergyMax);
      theta = rand.Uniform(fThetaMin,fThetaMax);
      Q2     = insane::kine::Qsquared(e,eprime,theta);
      x      = insane::kine::xBjorken_EEprimeTheta(e,eprime,theta);
      W      = insane::kine::W_EEprimeTheta(e,eprime,theta); 

      h2->Fill(x,Q2); 

   }

   return(h2);
}
//___________________________________________________________________________
}}
