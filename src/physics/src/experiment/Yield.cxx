#include "insane/experiment/Yield.h"

namespace insane {
namespace physics {

//______________________________________________________________________________
Yield::Yield(Target * targ, Double_t current) 
   : Luminosity(targ,current)
{
   fXsec = nullptr;
}
//______________________________________________________________________________
Yield::~Yield()
{ }
//______________________________________________________________________________
void Yield::AddCrossSection(DiffXSec * xs)
{
   if(xs) {
      fXSections.Add(xs);
   }
}
//______________________________________________________________________________
Double_t Yield::CalculateRate(Int_t i, Double_t * x, int pdgcode)
{
   if(!fXsec) {
      Error("CalculateRate()","No cross section set");
      return 0;
   }
   if(!fTarget) {
      Error("CalculateRate()","No target set");
      return 0;
   }
   //std::cout << "xsec(" << fXsec->GetName() << ") pdgcode = " << fXsec->GetParticleType(0) << "\n"; 
   if( (pdgcode != 0) && (pdgcode != fXsec->GetParticleType(0)) ) {
         return 0.0;
   }
   TargetMaterial * mat = fTarget->GetMaterial(i);
   //mat->Print();
   fXsec->SetTargetMaterial(*mat);

   Double_t sigma    = fXsec->EvaluateXSec(fXsec->GetDependentVariables(x))*1.0e-33; // 1nb = 1e-33 cm^2
   Double_t L        = GetMaterialLuminosity(i);
   Double_t mat_rate = sigma*L;
   //std::cout << "Material(" << i << ") rate = " << mat_rate << "\n"; 
   //Double_t rate = mat_rate;
   return(mat_rate);
}
//______________________________________________________________________________
Double_t Yield::CalculateRate(Double_t * x, int pdgcode){
   if(!fXsec) {
      Error("CalculateRate()","No cross section set");
   }
   if(!fTarget) {
      Error("CalculateRate()","No target set");
   }
   Double_t rate = 0.0;


   for (int i = 0; i < fTarget->GetNMaterials(); i++) {
      rate += CalculateRate(i,x,pdgcode);
   }
   return(rate);
}
//______________________________________________________________________________
Double_t Yield::CalculateTotalRate(Int_t i, Double_t * x, int pdgcode)
{
   Double_t total_rate = 0.0;
   for(int ixs = 0; ixs<fXSections.GetEntries() ; ixs++) {
      SetCrossSection((DiffXSec*)fXSections.At(ixs));
      total_rate += CalculateRate(i,x,pdgcode);
   }
   return total_rate;
}
//______________________________________________________________________________
Double_t Yield::CalculateTotalRate(Double_t * x, int pdgcode)
{
   Double_t total_rate = 0.0;
   for(int ixs = 0; ixs<fXSections.GetEntries() ; ixs++) {
      SetCrossSection((DiffXSec*)fXSections.At(ixs));
      total_rate += CalculateRate(x,pdgcode);
   }
   return total_rate;
}
//______________________________________________________________________________

}
}
