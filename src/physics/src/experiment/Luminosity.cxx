#include "insane/experiment/Luminosity.h"


namespace insane {

  namespace materials {

    /**  \f$ L_i = I_{beam} \rho_{i} l_{i} N_{A} 1/m_{a} \f$
    */
    double ComputeLuminosityPerN(std::array<int,2> ZA, double density, double length, double I) {
      using namespace insane::constants;
      double Phi_e = I / ELECTRONCHARGE;
      double L_rho = length * density * Avogadro / ZA.at(1);
      double lum   = Phi_e * L_rho;
      double res   = lum * ZA.at(1);
      return res;
    }
  } // namespace materials

  namespace physics {

    Luminosity::Luminosity(Target * targ, double current) : TObject() {
      fTarget      = targ;
      fBeamCurrent = current;
    }
    //______________________________________________________________________________

    Luminosity::~Luminosity()
    {
    }
    //______________________________________________________________________________
    
    double Luminosity::CalculateLuminosity() const {
      fLuminosity = 0.0;
      for (int i = 0; i < fTarget->GetNMaterials(); i++) {
        fLuminosity += GetMaterialLuminosity(i);
      }
      return(fLuminosity);
    }
    //______________________________________________________________________________
    
    double Luminosity::CalculateLuminosityPerNucleon() const {
      double res = 0.0;
      for (int i = 0; i < fTarget->GetNMaterials(); i++) {
        res += ( GetMaterialLuminosity(i)*fTarget->GetMaterial(i)->fA);
      }
      return(res);
    }
    //______________________________________________________________________________
    
    double Luminosity::GetMaterialLuminosity(int i) const {
      if (!fTarget) {
        Error("GetMaterialLuminosity", "No Target Defined");
        return(0.0);
      }
      auto* aMat = (TargetMaterial*) fTarget->GetMaterial(i);
      if (!aMat) return(0.0);
      return(GetElectronFlux() * aMat->GetLengthDensity());
    }
    //______________________________________________________________________________

    void Luminosity::Print(Option_t * opt) const {
      Print(std::cout);
      //fTarget->Print();
      //std::cout << " Luminosity   = " << CalculateLuminosity() << " cm^-2 s^-1\n";
      //std::cout << " fBeamCurrent = " << fBeamCurrent << " A\n";
      //std::cout << " e charge     = " << ELECTRONCHARGE << " C\n";
      //std::cout << " e flux       = " << GetElectronFlux() << " 1/s\n";
    }
    //______________________________________________________________________________


    void Luminosity::Print(std::ostream& s) const {
      //fTarget->Print();
      s << "Luminosity for target: " << fTarget->GetTitle() << "  (" << fTarget->GetName() << ")" << std::endl;
      double rl_tot=0.0;
      for (int i = 0; i < fTarget->GetNMaterials(); i++) {
        TargetMaterial* mat = fTarget->GetMaterial(i);
        mat->Print(s);
        double lum = GetMaterialLuminosity(i);
        s << "   Luminosity = " <<  lum << " x " << mat->fA << " = " <<  lum * mat->fA << " nucleon cm^-2 s^-1 " << std::endl;
        rl_tot += mat->GetNumberOfRadiationLengths();
      }
      s << " Total thickness = " << rl_tot << " (radiation lengths)" << std::endl;
      s << " Luminosity   = " << CalculateLuminosityPerNucleon() << " nucleon cm^-2 s^-1\n";
      s << " fBeamCurrent = " << fBeamCurrent << " A\n";
      s << " e flux       = " << GetElectronFlux() << " 1/s\n";
      s << " e charge     = " << ELECTRONCHARGE << " C\n";
    }
    //______________________________________________________________________________

  }
}
