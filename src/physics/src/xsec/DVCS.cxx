#include "insane/xsec/DVCS.h"

#include "TMath.h"
#include "insane/kinematics/KinematicFunctions.h"
#include <complex>

namespace insane {
  namespace xsec::dvcs {


    double K_DVCS(double E0, double Q2, double xB, double Delta2, double M)
    {
      // http://inspirehep.net/record/537446
      double eps = epsilon(Q2,xB,M);
      double nu  =  Q2/(2.0*M*xB);
      double y   = nu/E0;
      double D2m = Delta2_min(Q2, xB, M);
      double t1  = -1.0*(Delta2/Q2)*(1.0-xB)*(1.0-y-y*y*eps*eps/4.0)*(1.0-D2m/Delta2);
      double t2  = TMath::Sqrt(1.0+eps*eps)+(4.0*xB*(1.0-xB)+eps*eps)/(4.0*(1.0-xB))*(Delta2-D2m)/Q2;
      return TMath::Sqrt(t1*t2);
    }
    //______________________________________________________________________________

    double K_DVCS2(double E0, double Q2, double xB, double Delta2, double M)
    {
      // http://inspirehep.net/record/537446
      double eps = epsilon(Q2,xB,M);
      double nu  =  Q2/(2.0*M*xB);
      double y   = nu/E0;
      double t_min = Delta2_min(Q2, xB, M);
      double t_max = Delta2_max(Q2, xB, M);
      double t1  = -1.0*(1.0-y-y*y*eps*eps/4.0);
      double t2  =  (4.0*xB*(1.0-xB)+eps*eps);
      double t3  = (Delta2 - t_min)*(Delta2-t_max);
      return TMath::Sqrt(t1*t2*t3)/(2.0*Q2);
    }
    //______________________________________________________________________________

    double K_DVCS(const DVCS_KinematicVariables& vars)
    {
      const double& Q2      = vars.Q2;
      const double& xB      = vars.xB;
      const double& y       = vars.y;
      const double& E0      = vars.E0;
      const double& M       = vars.M;
      const double& Delta2  = vars.t;
      const double& eps     = vars.eps;
      double D2m = Delta2_min(Q2, xB, M);
      double t1  = -1.0*(Delta2/Q2)*(1.0-xB)*(1.0-y-y*y*eps*eps/4.0)*(1.0-D2m/Delta2);
      double t2  = TMath::Sqrt(1.0+eps*eps)+(4.0*xB*(1.0-xB)+eps*eps)/(4.0*(1.0-xB))*(Delta2-D2m)/Q2;
      return TMath::Sqrt(t1*t2);
    }
    //______________________________________________________________________________

    double K_DVCS2(const DVCS_KinematicVariables& vars)
    {
      const double& Q2      = vars.Q2;
      const double& xB      = vars.xB;
      const double& y       = vars.y;
      const double& E0      = vars.E0;
      const double& M       = vars.M;
      const double& Delta2  = vars.t;
      const double& eps     = vars.eps;
      double t_min = Delta2_min(Q2, xB, M);
      double t_max = Delta2_max(Q2, xB, M);
      double t1  = -1.0*(1.0-y-y*y*eps*eps/4.0);
      double t2  =  (4.0*xB*(1.0-xB)+eps*eps);
      double t3  = (Delta2 - t_min)*(Delta2-t_max);
      return TMath::Sqrt(t1*t2*t3)/(2.0*Q2);
    }
    //______________________________________________________________________________

    double J_DVCS(double E0, double Q2, double xB, double Delta2, double M)
    {
      //  
      double eps = epsilon(Q2,xB,M);
      double nu  =  Q2/(2.0*M*xB);
      double y   = nu/E0;
      double t1  = (1.0-y-y*eps*eps/2.0)*(1.0+Delta2/Q2);
      double t2  = -1.0*(1.0-xB)*(2.0-y)*Delta2/Q2;
      //double t1 =  1.0 - (Delta2/Q2)*(1.0-xB*(2.0-y) + y*eps*eps/2.0) + y*eps*eps/2.0;
      return t1+t2;
    }
    //______________________________________________________________________________


    double J_DVCS(const DVCS_KinematicVariables& vars)
    {
      const double& Q2      = vars.Q2;
      const double& xB      = vars.xB;
      const double& y       = vars.y;
      const double& E0      = vars.E0;
      const double& M       = vars.M;
      const double& Delta2  = vars.t;
      const double& eps     = vars.eps;
      //double t1 =  1.0 - (Delta2/Q2)*(1.0-xB*(2.0-y) + y*eps*eps/2.0) + y*eps*eps/2.0;
      double t1  = (1.0-y-y*eps*eps/2.0)*(1.0+Delta2/Q2);
      double t2  = -1.0*(1.0-xB)*(2.0-y)*Delta2/Q2;
      return t1+t2;
    }
    //______________________________________________________________________________

    double P1(double E0, double Q2, double xB, double Delta2, double phi, double M)
    {
      double eps = epsilon(Q2,xB,M);
      double nu  =  Q2/(2.0*M*xB);
      double y   = nu/E0;
      double J = J_DVCS(E0,Q2,xB,Delta2,M);
      double K = K_DVCS(E0,Q2,xB,Delta2,M);
      return( (-1.0/(y*(1.0+eps*eps)))*(J + 2.0*K*TMath::Cos(phi)) );
      //double k_dot_Delta = (-Q2/(2.0*y*(1.0+eps*eps)))*(J + 2.0*K*TMath::Cos(phi));
      //return(1.0 + 2.0*k_dot_Delta/Q2);
    }
    //______________________________________________________________________________

    double P2(double E0, double Q2, double xB, double Delta2, double phi, double M)
    {
      double eps = epsilon(Q2,xB,M);
      double nu  =  Q2/(2.0*M*xB);
      double y   = nu/E0;
      double J = J_DVCS(E0,Q2,xB,Delta2,M);
      double K = K_DVCS(E0,Q2,xB,Delta2,M);
      return(1.0+Delta2/Q2+1.0/(y*(1.0+eps*eps))*(J+2.0*K*TMath::Cos(phi)));
      //double k_dot_Delta = (-Q2/(2.0*y*(1.0+eps*eps)))*(J + 2.0*K*TMath::Cos(phi));
      //return( (-2.0*k_dot_Delta + Delta2)/Q2 );
    }
    //______________________________________________________________________________
    
    double P1(const DVCS_KinematicVariables& vars)
    {
      const double& phi     = vars.phi;
      const double& Q2      = vars.Q2;
      const double& xB      = vars.xB;
      const double& y       = vars.y;
      const double& E0      = vars.E0;
      const double& M       = vars.M;
      const double& Delta2  = vars.t;
      const double& eps     = vars.eps;
      const double& K       = vars.K;
      const double& J       = vars.J;
      //double J = J_DVCS(E0,Q2,xB,Delta2,M);
      //double K = K_DVCS(E0,Q2,xB,Delta2,M);
      return( (-1.0/(y*(1.0+eps*eps)))*(J + 2.0*K*TMath::Cos(phi)) );
      //double k_dot_Delta = (-Q2/(2.0*y*(1.0+eps*eps)))*(J + 2.0*K*TMath::Cos(phi));
      //return(1.0 + 2.0*k_dot_Delta/Q2);
    }
    //______________________________________________________________________________

    double P2(const DVCS_KinematicVariables& vars)
    {
      const double& phi     = vars.phi;
      const double& Q2      = vars.Q2;
      const double& xB      = vars.xB;
      const double& y       = vars.y;
      const double& E0      = vars.E0;
      const double& M       = vars.M;
      const double& Delta2  = vars.t;
      const double& eps     = vars.eps;
      const double& K       = vars.K;
      const double& J       = vars.J;
      return(1.0+Delta2/Q2+1.0/(y*(1.0+eps*eps))*(J+2.0*K*TMath::Cos(phi)));
      //double k_dot_Delta = (-Q2/(2.0*y*(1.0+eps*eps)))*(J + 2.0*K*TMath::Cos(phi));
      //return( (-2.0*k_dot_Delta + Delta2)/Q2 );
    }
    //______________________________________________________________________________

    double c0_BH_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs)
    {
      const double& Q2      = vars.Q2;
      const double& xB      = vars.xB;
      const double& y       = vars.y;
      const double& E0      = vars.E0;
      const double& M       = vars.M;
      const double& Delta2  = vars.t;
      const double& eps     = vars.eps;
      const double& K       = vars.K;
      const double& F1      = ffs.F1;
      const double& F2      = ffs.F2;
      double t1 = 8.0*K*K*((2.0+3.0*eps*eps)*(Q2/Delta2)*(F1*F1-Delta2*F2*F2/(4.0*M*M))+2.0*xB*xB*(F1+F2)*(F1+F2));
      double t20 = (2.0-y)*(2.0-y);
      double t21 = (2.0+eps*eps)*(4.0*xB*xB*M*M/Delta2*(1.0+Delta2/Q2)*(1.0+Delta2/Q2)
                                  +4.0*(1.0-xB)*(1.0+xB*Delta2/Q2))*(F1*F1-Delta2*F2*F2/(4.0*M*M));
      double t22 = 4.0*xB*xB*(xB+(1.0-xB+eps*eps/2.0)*(1.0-Delta2/Q2)*(1.0-Delta2/Q2)-xB*(1.0-2.0*xB)*(Delta2/Q2)*(Delta2/Q2))*(F1+F2)*(F1+F2);
      double t2 = t20*(t21+t22);
      double t3 = 8.0*(1.0+eps*eps)*(1.0-y-eps*eps*y*y/4.0)*(2.0*eps*eps*(1.0-Delta2/(4.0*M*M))*(F1*F1-Delta2*F1*F2/(4.0*M*M)) -xB*xB*(1.0-Delta2/Q2)*(1.0-Delta2/Q2)*(F1+F2)*(F1+F2));

      return t1+t2+t3;
    }
    //______________________________________________________________________________

    double c1_BH_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs)
    {
      const double& Q2      = vars.Q2;
      const double& xB      = vars.xB;
      const double& y       = vars.y;
      const double& E0      = vars.E0;
      const double& M       = vars.M;
      const double& Delta2  = vars.t;
      const double& eps     = vars.eps;
      const double& F1      = ffs.F1;
      const double& F2      = ffs.F2;
      const double& K       = vars.K;
      double t10 = 8.0*K*(2.0-y);
      double t11 = (4.0*xB*xB*M*M/Delta2 -2.0*xB-eps*eps)*(F1*F1-Delta2*F2*F2/(4.0*M*M));
      double t12 = 2.0*xB*xB*(1.0-(1.0-2.0*xB)*(Delta2/Q2))*(F1+F2)*(F1+F2);
      return t10*(t11+t12);
    }
    //______________________________________________________________________________

    double c2_BH_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs)
    {
      const double& Q2      = vars.Q2;
      const double& xB      = vars.xB;
      const double& y       = vars.y;
      const double& E0      = vars.E0;
      const double& M       = vars.M;
      const double& Delta2  = vars.t;
      const double& eps     = vars.eps;
      const double& F1      = ffs.F1;
      const double& F2      = ffs.F2;
      const double& K       = vars.K;
      double t10 = 8.0*K*K*xB*xB*((4.0*M*M/Delta2)*(F1*F1 - Delta2*F2*F2/(4.0*M*M))+2.0*(F1+F2)*(F1+F2));
      return t10;
    }
    //______________________________________________________________________________

    //------------------------------------------
    // DVCS Fourier Harmonics
    //------------------------------------------

    double c0_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F)
    {
      using namespace TMath;
      using std::conj;
      const double& Q2      = vars.Q2;
      const double& xB      = vars.xB;
      const double& y       = vars.y;
      const double& E0      = vars.E0;
      const double& M       = vars.M;
      const double& Delta2  = vars.t;
      DVCS_CFFs FF_1{ F.F1, F.F2, F.H,             F.E,       F.Htilde,       F.Etilde};
      DVCS_CFFs FF_2{ F.F1, F.F2, conj(F.H), conj(F.E), conj(F.Htilde), conj(F.Etilde)};
      double res = std::real(2.0*(2.0-2.0*y + Power(y,2.0))*C_angular_DVCS_unp(vars,FF_1,FF_2));
      // need to check that result is always real... ?
      return res;
    }
    //______________________________________________________________________________

    double c1_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F)
    {
      using namespace TMath;
      using std::conj;
      const double& xB      = vars.xB;
      const double& y       = vars.y;
      const double& K       = vars.K;
      DVCS_CFFs FF_1{F.F1, F.F2, F.Heff, F.Eeff, F.Hefftilde, F.Eefftilde};
      DVCS_CFFs FF_2{F.F1, F.F2, conj(F.H), conj(F.E), conj(F.Htilde), conj(F.Etilde)};
      double T0 = std::real(C_angular_DVCS_unp(vars, FF_1, FF_2));
      double res = (8.0*K*(2.0-y)*T0)/(2.0-xB);
      return res;
    }
    //______________________________________________________________________________

    double s1_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F)
    {
      using namespace TMath;
      using std::conj;
      DVCS_CFFs FF_1{F.F1, F.F2, F.Heff, F.Eeff, F.Hefftilde, F.Eefftilde};
      DVCS_CFFs FF_2{F.F1, F.F2, conj(F.H), conj(F.E), conj(F.Htilde), conj(F.Etilde)};
      //(-8*K*y*Im(\[ScriptCapitalC]DVCS("T,unp",\[ScriptCapitalF]eff,\[ScriptCapitalF]star)))/(2 - xB)
      return 0.0;
    }
    //______________________________________________________________________________

    double c2_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F)
    {
      using namespace TMath;
      using std::conj;
      const double& xB      = vars.xB;
      const double& Q2      = vars.Q2;
      const double& M       = vars.M;
      const double& K       = vars.K;
      DVCS_CFFs FF_1{F.F1, F.F2, F.HT,   F.ET,   F.HTtilde,   F.ETtilde};
      DVCS_CFFs FF_2{F.F1, F.F2, conj(F.H), conj(F.E), conj(F.Htilde), conj(F.Etilde)};
      double T0  = std::real(C_angular_DVCS_unp(vars,FF_1,FF_2));
      double res = ((-4.0*Power(K,2.0)*Q2)/(Power(M,2.0)*(2.0-xB)))*T0;
      return res;
    }
    //______________________________________________________________________________

    double s2_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F)
    {
      using namespace TMath;
      using std::conj;
      //(-8*K*y*Im(\[ScriptCapitalC]DVCS("T,unp",\[ScriptCapitalF]eff,\[ScriptCapitalF]star)))/(2 - xB)
      return 0.0;
    }
    //______________________________________________________________________________

    //------------------------------------------
    // Interference Fourier Harmonics
    //------------------------------------------

    double c0_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs)
    {
      using namespace TMath;
      using std::conj;
      const double& xB      = vars.xB;
      const double& Q2      = vars.Q2;
      const double& Delta2  = vars.t;
      const double& M       = vars.M;
      const double& y       = vars.y;
      const double& K       = vars.K;
      DVCS_CFFs FF_1{ffs.F1, ffs.F2, ffs.HT,   ffs.ET,   ffs.HTtilde,  ffs.ETtilde};
      std::complex<double> T0 = (Power(K,2)*Power(2.0-y,2.0)*C_angular_I_unp(vars,FF_1))/(1.0-y);
      std::complex<double> T1 = ((2.0-xB)*(1.0-y)*Delta2/Q2)*(C_angular_I_unp(vars,FF_1) + DeltaC_angular_I_unp(vars,FF_1));
      double res = -8.0*(2.0-y)*std::real(T0+T1);
      return res;
    }
    //______________________________________________________________________________

    double c1_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs)
    {
      using namespace TMath;
      using std::conj;
      //8*K*(-2 + 2*y - Power(y,2))*Re(\[ScriptCapitalC]I("unp",\[ScriptCapitalF]))
      return 0.0;
    }
    //______________________________________________________________________________

    double s1_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs)
    {
      using namespace TMath;
      using std::conj;
      //8*K*(2 - y)*y*Im(\[ScriptCapitalC]I("unp",\[ScriptCapitalF]))
      return 0.0;
    }
    //______________________________________________________________________________

    double c2_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs)
    {
      using namespace TMath;
      //(16*Power(K,2)*(-2 + y)*Re(\[ScriptCapitalC]I("unp",\[ScriptCapitalF]eff)))/(2 - xB)
      return 0.0;
    }
    //______________________________________________________________________________

    double s2_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs)
    {
      using namespace TMath;
      //(16*Power(K,2)*y*Im(\[ScriptCapitalC]I("unp",\[ScriptCapitalF]eff)))/(2 - xB)
      return 0.0;
    }
    //______________________________________________________________________________

    double c3_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs)
    {
      //(-8*Power(K,3)*Q2*Re(\[ScriptCapitalC]I("T,unp",\[ScriptCapitalF]T,\[ScriptCapitalF]star)))/(Power(M,2)*Power(2 - xB,2))
      using namespace TMath;
      return 0.0;
    }
    //______________________________________________________________________________

    double s3_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs)
    {
      using namespace TMath;
      return 0.0;
    }
    //______________________________________________________________________________

    double c0_BH_spin0(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs)
    {
      using namespace TMath;
      using std::conj;
      const double& xB      = vars.xB;
      const double& Q2      = vars.Q2;
      const double& Delta2  = vars.t;
      const double& M       = vars.M;
      const double& y       = vars.y;
      const double& K       = vars.K;
      const double& eps     = vars.eps;
      double t1 = (2.0-y)*(2.0-y)+y*y*(1.0+eps*eps)*(1.0+eps*eps);
      double t2 = 4.0*xB*xB*M*M/Delta2 + 4.0*(1.0-xB)+(4.0*xB+eps*eps)*Delta2/Q2;
      double t3 = 32.0*xB*xB*K*K*M*M/Delta2;
      double t4 = 2.0*eps*eps*(4.0*(1.0-y)*(3.0+2.0*eps*eps)+y*y*(2.0-eps*eps*eps*eps));
      double t5 = -4.0*xB*xB*(2.0-y)*(2.0-y)*(2.0+eps*eps)*Delta2/Q2;
      return t1*t2+t3+t4+t5;
    }
    //______________________________________________________________________________
    
    double c1_BH_spin0(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs)
    {
      using namespace TMath;
      using std::conj;
      const double& xB      = vars.xB;
      const double& Delta2  = vars.t;
      const double& M       = vars.M;
      const double& y       = vars.y;
      const double& eps     = vars.eps;
      return -8.0*(2.0-y)*(2.0*xB+eps*eps-4.0*xB*xB*M*M/Delta2);
    }
    //______________________________________________________________________________
    
    double c2_BH_spin0(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs)
    {
      using namespace TMath;
      using std::conj;
      const double& xB      = vars.xB;
      const double& Delta2  = vars.t;
      const double& M       = vars.M;
      return 32.0*xB*xB*M*M/Delta2;
    }
    //______________________________________________________________________________

    std::complex<double> C_angular_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_CFFs& F, const DVCS_CFFs& Fstar)
    {
      using namespace TMath;
      const std::complex<double>& Hcff          = F.H;
      const std::complex<double>& Ecff          = F.E;
      const std::complex<double>& Hcfftilde     = F.Htilde;
      const std::complex<double>& Ecfftilde     = F.Etilde;
      const std::complex<double>& Hcffstar      = Fstar.H;
      const std::complex<double>& Ecffstar      = Fstar.E;
      const std::complex<double>& Hcfftildestar = Fstar.Htilde;
      const std::complex<double>& Ecfftildestar = Fstar.Etilde;
      const double& Q2      = vars.Q2;
      const double& xB      = vars.xB;
      const double& y       = vars.y;
      const double& E0      = vars.E0;
      const double& M       = vars.M;
      const double& Delta2  = vars.t;
      std::complex<double> T0 = 4.0*(Hcff*Hcffstar + Hcfftilde*Hcfftildestar)*(1.0-xB);
      std::complex<double> T1 = -1.0*(Ecffstar*Hcff + Ecff*Hcffstar + Ecfftildestar*Hcfftilde + Ecfftilde*Hcfftildestar)*Power(xB,2.0);
      std::complex<double> T2 = -1.0*(Ecfftilde*Ecfftildestar*Power(xB,2)*Delta2)/(4.*Power(M,2.0));
      std::complex<double> T3 = -1.0*Ecff*Ecffstar*(Power(xB,2) + (Power(2.0-xB,2.0)*Delta2)/(4.*Power(M,2.0)));
      double denom = Power(2.0 - xB,2.0);
      return( (T0+T1+T2+T3)/denom );
    }
    //______________________________________________________________________________

    std::complex<double> C_angular_I_unp(const DVCS_KinematicVariables& vars, const DVCS_CFFs& F)
    {
      using namespace TMath;
      //F1*HCFF + ((F1 + F2)*HCFFtilde*xB)/(2 - xB) - (ECFF*F2*\[CapitalDelta]2)/(4.*Power(M,2))
      return 0.0;
    }
    //______________________________________________________________________________
    //
    std::complex<double> C_angular_I_T_unp(const DVCS_KinematicVariables& vars, const DVCS_CFFs& F)
    {
      using namespace TMath;
      return 0.0;
    }
    //______________________________________________________________________________

    std::complex<double> DeltaC_angular_I_unp(const DVCS_KinematicVariables& vars,
                                              const DVCS_CFFs&               F) {
      return 0.0;
    }

  } // namespace xsec::dvcs
} // namespace insane
