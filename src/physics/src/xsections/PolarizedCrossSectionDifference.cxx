#include "insane/xsections/PolarizedCrossSectionDifference.h"
#include "insane/base/InSANEPhysics.h"

namespace insane {
  namespace physics {
    PolarizedCrossSectionDifference::PolarizedCrossSectionDifference()
    {
      fPolType         = -1; 
      fLabel           = "#frac{d^{2}#sigma}{dE'd#Omega}";
      fUnits           = "nb/GeV/sr";
      fPhysType        = 2;                 ///< DIS


      // Structure functions g1,g2
      PolarizedStructureFunctions * psf   = new DefaultPolarizedStructureFunctions();
      SetPolarizedStructureFunctions(psf);

      // Nucleon form factors 
      FormFactors * FFs = new DefaultFormFactors();
      SetFormFactors(FFs);

      fUseScalingFunction = false; 

    }
    //______________________________________________________________________________
    PolarizedCrossSectionDifference::~PolarizedCrossSectionDifference(){

    }
    //______________________________________________________________________________
    Double_t PolarizedCrossSectionDifference::EvaluateXSec(const Double_t *par) const{

      if (!VariablesInPhaseSpace(fnDim, par)){
        std::cout << "[PolarizedCrossSectionDifference::EvaluateXSec]: Something is wrong with the phase space!" << std::endl;
        std::cout << "Phase space dimension: "   << fnDim << std::endl; 
        for(int i=0;i<fnDim;i++) std::cout << "par[" << i << "]: " << par[i] << std::endl;
        return(0.0);
      }

      Double_t Ebeam  = GetBeamEnergy();
      Double_t Eprime = par[0];
      Double_t theta  = par[1];
      Double_t fullXsec=0,result=0; 

      Double_t M     = M_p/GeV; 
      Double_t alpha = fine_structure_const; 
      Double_t nu    = Ebeam-Eprime; 
      Double_t SIN   = TMath::Sin(theta); 
      Double_t COS   = TMath::Cos(theta); 
      Double_t x     = insane::Kine::xBjorken_EEprimeTheta(Ebeam,Eprime,theta);  
      Double_t Q2    = insane::Kine::Qsquared(Ebeam,Eprime,theta); 
      Double_t T1    = 4.*alpha*alpha/(M*Q2);                  /// sign convention: see Anselmino et al [Phys Rep 261, 1 (1995), Eq 2.1.24]  
      Double_t g1=0,g2=0,T2=0,SF=0; 
      ProcessPolarizedStructureFunctions(x,Q2,g1,g2); 

      switch(fPolType){
        case 1: // parallel 
          T2       = Eprime/(nu*Ebeam); 
          fullXsec = T1*T2*( (Ebeam + Eprime*COS)*g1 - (Q2/nu)*g2 );
          break;
        case 2: // perpendicular 
          T2       = Eprime*Eprime*SIN/(nu*nu*Ebeam); 
          fullXsec = T1*T2*( nu*g1 + (2.*Ebeam)*g2 );
          break;
        default:
          std::cout << "[PolarizedCrossSectionDifference::EvaluateXSec]: ";
          std::cout << "Invalid type!  Exiting..." << std::endl;
          exit(1); 
      }

      if( IncludeJacobian() ){
        result = fullXsec*TMath::Sin(theta);
      }else{
        result = fullXsec;
      }

      result *= hbarc2_gev_nb; // converts from 1/GeV^2 to nb.   

      SF = ScalingFunction(Q2); 
      if(fUseScalingFunction) result *= SF; 

      return result;

    }
    //______________________________________________________________________________
    void PolarizedCrossSectionDifference::ProcessPolarizedStructureFunctions(Double_t x,Double_t Q2,Double_t &g1,Double_t &g2) const{

      //Nucleus::NucleusType TargetType = fNucleus.GetType();
      Double_t GE_el=0,GM_el=0; 
      Double_t GE_qe=0,GM_qe=0; 
      Double_t g1_in=0,g2_in=0; 
      Nucleus nucleus = GetTargetNucleus();

      PolarizedStructureFunctions * fPolSFs   = GetPolarizedStructureFunctions();
      FormFactors * fFF = GetFormFactors();

      if(nucleus == Nucleus::Proton()) {
        g1_in = fPolSFs->g1p(x,Q2);
        g2_in = fPolSFs->g2p(x,Q2);
        GE_el = fFF->GEp(Q2);
        GM_el = fFF->GMp(Q2); 
        GE_qe = 0.;
        GM_qe = 0.; 
      } else if(nucleus == Nucleus::Neutron()){
        g1_in = fPolSFs->g1n(x,Q2);
        g2_in = fPolSFs->g2n(x,Q2);
        GE_el = fFF->GEn(Q2);
        GM_el = fFF->GMn(Q2); 
        GE_qe = 0.;
        GM_qe = 0.; 
      } else if(nucleus == Nucleus::Deuteron()){
        g1_in = fPolSFs->g1d(x,Q2);
        g2_in = fPolSFs->g2d(x,Q2);
        GE_el = fFF->GEd(Q2);
        GM_el = fFF->GMd(Q2);
        GE_qe = fFF->GEdQE(x,Q2);
        GM_qe = fFF->GMdQE(x,Q2);
      } else if(nucleus == Nucleus::He3()){
        g1_in = fPolSFs->g1He3(x,Q2);
        g2_in = fPolSFs->g2He3(x,Q2);
        GE_el = fFF->GEHe3(Q2);
        GM_el = fFF->GMHe3(Q2);
        GE_qe = fFF->GEHe3QE(x,Q2);
        GM_qe = fFF->GMHe3QE(x,Q2);
      } else {
        std::cout << "[PolarizedCrossSectionDifference::ProcessPolarizedStructureFunctions]: "; 
        std::cout << "Invalid target!  Exiting..." << std::endl;
        exit(1);
      }

      Double_t M     = M_p/GeV;
      Double_t tau   = Q2/(4.*M*M);
      // elastic 
      Double_t g1_el = 0.5*GM_el*(GE_el + tau*GM_el)/(1. + tau); 
      Double_t g2_el = 0.5*tau*GM_el*(GE_el - GM_el)/(1. + tau);
      // quasi-elastic 
      Double_t g1_qe = 0.5*GM_qe*(GE_qe + tau*GM_qe)/(1. + tau); 
      Double_t g2_qe = 0.5*tau*GM_qe*(GE_qe - GM_qe)/(1. + tau);

      switch(fPhysType){
        case 0: // elastic 
          g1 = g1_el;
          g2 = g2_el; 
          break;
        case 1: // quasi-elastic 
          g1 = g1_qe; 
          g2 = g2_qe; 
          break;
        case 2: // inelastic (default) 
          g1 = g1_in; 
          g2 = g2_in;
          break; 
        default: 
          // just to be uber-safe, we print errors if you set a type incorrectly  
          std::cout << "PolarizedCrossSectionDifference::ProcessPolarizedStructureFunctions]: ";
          std::cout << "Invalid physics type! Exiting..." << std::endl;
          exit(1);  
      }

    }
    //______________________________________________________________________________
    Double_t PolarizedCrossSectionDifference::ScalingFunction(Double_t Q2) const{

      // a scaling function to scale the model to E94010 data 
      // FIXME: Get rid of hard coding! 

      Double_t p0=0,p1=0;
      switch(fPolType){
        case 1: // parallel
          p0 = 0.00096263;
          p1 = 0.0110808;
          break;
        case 2: // perpendicular 
          p0 = 0.0163187;
          p1 = 0.0954788;
          break; 
        default:
          std::cout << "[PolarizedCrossSectionDifference::ScalingFunction]: ";
          std::cout << "Invalid type!  Exiting..." << std::endl;
          exit(1); 

      }

      Double_t SF = p0 + p1*Q2; 
      return SF;

    }
    //______________________________________________________________________________
  }}
