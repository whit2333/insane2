#include "insane/xsections/InclusiveDISXSec.h"
//#include "InelasticRadiativeTail.h"

namespace insane {
namespace physics {

InclusiveDISXSec::InclusiveDISXSec()
{
   fID        = 100100001;
   fTitle     = "InclusiveDISXSec";
   fPlotTitle = "#frac{d#sigma}{dE'd#Omega} nb/GeV-Sr";

   //fProtonXSec  = new InclusiveDiffXSec();
   fProtonXSec  = new InelasticRadiativeTail2();
   //((InelasticRadiativeTail*)fProtonXSec)->GetBornXSec()->SetTargetNucleus(Nucleus::Proton());
   fProtonXSec->SetTargetNucleus(Nucleus::Proton());
   //((InelasticRadiativeTail*)fProtonXSec)->SetTargetThickness(0.046);

   //fNeutronXSec = new InclusiveDiffXSec();
   fNeutronXSec  = new InelasticRadiativeTail2();
   //((InelasticRadiativeTail*)fNeutronXSec)->GetBornXSec()->SetTargetNucleus(Nucleus::Neutron());
   fNeutronXSec->SetTargetNucleus(Nucleus::Neutron());
   //((InelasticRadiativeTail*)fNeutronXSec)->SetTargetThickness(0.046);
}
//______________________________________________________________________________

InclusiveDISXSec::~InclusiveDISXSec()
{ }
//______________________________________________________________________________

InclusiveDISXSec*  InclusiveDISXSec::Clone(const char * newname) const 
{
   std::cout << "InclusiveDISXSec::Clone()\n";
   auto * copy = new InclusiveDISXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

void InclusiveDISXSec::SetTargetThickness(Double_t t)
{
   ((InelasticRadiativeTail2*)fProtonXSec)->SetTargetThickness(t);
   ((InelasticRadiativeTail2*)fNeutronXSec)->SetTargetThickness(t);
}
//______________________________________________________________________________

}}
