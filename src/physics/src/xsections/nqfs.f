      !> CALCULATE QUASIELASTIC, TWO-NUCLEON, RESONANCE, AND X-SCALING
      !! ELECTRON SCATTERING CROSS SECTION FOR ARBITRARY NUCLEI
      !!
      !! @ingroup QFS
      !!
      !! @author 
      !! WRITTEN BY J.W. LIGHTBODY JR. AND J.S. O'CONNELL 
      !! NATIONAL BUREAU OF STANDARDS, GAITHERSBURG, MD 20899 
      !! OCTOBER 1987
      !!
      !! @param RES returned value of cross section ()
      !! @param ZZ number of protons in nucleus
      !! @param NN number of neutrons in nucleus
      !! @param EBEAM electron beam energy (MeV)
      !! @param PCENTRAL central momentum value
      !! @param DELP momentum acceptance 
      !! @param THETA central angle 
      !! @param DELTHETA angular acceptance
      !!
      !! REVISION HISTORY:
      !! 
      !! - 09/16-02 K. Slifer, Temple U.
      !!  - Modified subroutine "RADIATE" to include external radiation using
      !!  formalism from S. Stein et al. Phys. Rev. D 12 7
      !!  In the resonance region, the new code gives identical results
      !!  to original code if external radiation is turned off.
      !!  At larger nu, the new code predicts a much larger tail than the original.
      !!
      !!  - Included option 'Qdep' to introduce a Q^2 dependent correction to the
      !!  Q.E. peak, based on a comparison with Saclay and SLAC carbon data.
      !!  The correction is only to the peak height.  A more sophistacated correction
      !!  would adjust the gaussian width as well.
      !!
      !!  - The most recent F.F. are provided as an option, but they are commented 
      !!  out at present. See functions GEP,GEN for example  
      !! 
      !!  - If the radiated cross section exhibits glitches adjust the parameters
      !!  "PREC" or "DEL" in subroutine RADIATE
      !!
      !! - 08/08/05 P. Solvignon, Temple University
      !!  Made several changes to improve Nitrogen xs model:
      !!       - change the dipole parameter AD1 in the delta region from 
      !!         700 to 685 MeV.
      !!       - multiplie delta cross section by 0.1+0.75*Ep
      !!       - multiplie dip cross section by 0.65
      !!       - multiplie SIG_1500 by 4.0/E
      !!       - multiplie SIG_1700 by Q2/100
      !!       - divide SIG_DIS by 2.7/(4.0*E*sin(THR/2.0))
      !!  Used radiation length proper to E01-012 experiment.  
      !!
      !! - 05/29/11 D. Flay, Temple University
      !!  Made the following changes to improve the Nitrogen xs model:
      !!       - multiplied the delta cross section by 0.1-2.65*Ep
      !!       - multiplied the dip cross section by 20*(1+W/E)
      !!           - provides good agreement at 4- and 5-pass
      !!
      !! - 10/12/2011 Whitney Armstrong (whit@temple.edu)
      !!   My comments have a WRA tag on them.
      !!       - Changed program to a subroutine in order to 
      !!         implement as a C function in the new C++ 
      !!         package, templeQFS. 
      !!       - Also added the flag ISCFUNC which, when set to
      !!         true skips the legacy input data entry so that
      !!         it behaves like a C function
      !!
      !! -  10/17/2011 Whitney Armstrong (whit@temple.edu)
      !!   Created files qfs_targs.f, qfs_sigs.f, qfs_func.f
      !!   to help break up this giant file
      !!
      !! -  10/19/2011 Whitney Armstrong (whit@temple.edu)
      !!    Added Doxygen documentation!
      !!
      SUBROUTINE qfs(RES,ZZ,NN,EBEAM,PCENTRAL,DELP,THETA,DELTHETA)
      IMPLICIT REAL*8 (A-H,O-Z)
      LOGICAL Qdep,INTERACTIVE,RATES,RAD,extrad,ISCFUNC
      CHARACTER TAG*4,LINE*80,nucleus*3,units*2
      COMMON/PAR/E,TH,W,Z,A,EPS,EPSD,PF,SPENCE
      COMMON/ADDONS/SCALE,Tb,Ta,extrad
      COMMON/QDEPENDENCE/XPAR0,XPAR1,Qdep
      REAL*8 current, density,zlength,domega,deltap,P0(30),P0N(30)
      REAL*8 sigmatot(1000), xnu(1000),sigave(30)
      DATA sigmatot/1000*0.0/,xnu/1000*0.0/,sigave/30*0.0/

      PM=939.D0
      DM=1232.D0
      ALPH=1./137.03604D0
      HBARC=197.32858D0
      PI=ACOS(-1.)

      ISCFUNC    =.TRUE.  ! skips legacy data input for C function.
      INTERACTIVE=.FALSE. ! Prompt user for input? Otherwise read from input.dat
      RATES      =.FALSE. ! Calculat the rates? (THIS FEATURE NOT QUITE DEBUGGED YET)
      RAD        =.TRUE.  ! turn on R.C. (internal only)
      extrad     =.TRUE.  ! turns on external R.C. 
      units      ='pb'    ! units/MeV-sr
      Qdep       =.FALSE. ! Correct Q.E. peak to agree with world data.
c      OPEN(20,FILE='input_N2.dat',STATUS='old')
c    writing a new C++ class (QFSXSecConfigurations) that writes 
c    this file. 
!        OPEN(20,FILE='qfs_input.dat',STATUS='old')
c PART 1 - Input data
c WRA :
c INPUTS: Z,A,
c        TH(theta), PF(fermi momentum),
c        EPSD(elastic to quasi-eleastic peak separation energy nomially 10MeV),
c        EPSD(delta separation energy nominally15MeV), 
c        Tb(target radiation before scattering typically ~0.01 but depends
c        very much on experiment), 
c        Ta(target radiation length after scattering 
c        typically ~0.01 but depends very much on experiment)
c        WMIN(minimum photon energy, omgea)
c        WMAX(maximum photon energy)
c        DELTAW(step size in photon/E' energy
c        PCENTRAL(central momentum of acceptance)
c        DELP('symmetric' momentum acceptance (PCENTRAL-PMIN)/PCENTRAL)
990   IF(INTERACTIVE .AND. (.NOT.ISCFUNC)) THEN
        CALL PROMPT(Z,A,E,TH,PF,EPS,EPSD,Tb,Ta)
        WRITE(6,'(A)') 'ENTER WMIN,WMAX,DELTAW: '
        READ(*,*) WMIN,WMAX,DELTAW
         TAG     = '    '
         nucleus = '   '
         GOTO 991
      ENDIF
      IF( (.NOT.ISCFUNC) )  THEN
        read(20,'(A)',ERR=999) LINE
        IF (LINE(1:1).EQ.'#') GOTO 990
        read(LINE,*) TAG,nucleus,TH,PF,EPS,EPSD
         GOTO 991
      ENDIF
c(Z,A,E,TH,PF,EPS,EPSD,Tb,Ta)
      IF( ISCFUNC ) THEN
c RES,ZZ,NN,PART,EBEAM,PCENTRAL,DELP,THETA,DELTHETA
        Z=ZZ
        N=NN
        A=Z+N
        E=EBEAM
        TH=THETA*180.0/PI ! degrees
        WMIN=E-PCENTRAL*(1.0+DELP)
        WMAX=E-PCENTRAL*(1.0-DELP)
        DELTAW=(WMAX-WMIN)/2.0 ! ONE STEP! (centered in the middle of the bin)
        PF=130.0
        EPS=10.0
        EPSD=15.0
        Tb=0.007
        Ta=0.04
        density= 12.25 ! needed only for Rates (Luminosity)
        zlength = 40.0 ! needed only for Rates (Luminosity)
        TAG     = '    '
        nucleus = '   '
!         E      = 1717.9
!         WMIN   = 45.
!         WMAX   = 975.
!         DELTAW = 5.0
!         Tb     = 7.338E-3
!         Ta     = 4.189E-2
!          density= 12.25  
!          zlength = 40.0 
        GOTO 991
      ENDIF

c PART 2 - predefined parameters
c -WRA
991   CONTINUE
      IF(.NOT.ISCFUNC)  THEN
        CALL TARGETCELL(TAG,INTERACTIVE,E,WMIN,WMAX,DELTAW,Tb,Ta,
     &                 density,zlength) ! predefined running parameters -wra
        CALL GETZA(nucleus,INTERACTIVE,Z,A) ! predefined target parameters -wra
!         CALL OPENFILES(TAG,INTERACTIVE,RATES,nucleus) !writes files (10 for rates 8 for xsection)
      ENDIF

      CALL GETSCALE(units,SCALE,UNSCALE)
      CALL GETQDEP(A,Qdep,XPAR0,XPAR1)!get pars to adjust QE peak -wra
 
!       write(8,'(2A,x,2F4.0,3A)')'#',nucleus,Z,A,'  :  ',units,'/MeV-sr'
!       write(8,'(A,2F7.1)')      '#E0 (MeV),TH   : ',E,TH
!       write(8,'(A,2F7.1)')      '#Pcentral,Wmin : ',PCENTRAL,WMIN
!       WRITE(8,'(A,3F7.1)')      '#PF,EPS,EPSD   : ',PF,EPS   ,EPSD
!       WRITE(8,'(A,L7,2F7.4)')   '#extrad,Tb,Ta  : ',extrad,Tb,Ta
!       write(8,'(A,L7,2F7.4)')  '#Q Dependence  : ',Qdep,XPAR0,XPAR
! 
!       write(*,'(2A,x,2F4.0,3A)')'#',nucleus,Z,A,':  ',units,'/MeV-sr'
!       write(*,'(A,2F7.1)')      '#E0 (MeV),TH : ',E,TH
!       write(*,'(A,2F7.1)')      '#Pcentral,Wmin : ',PCENTRAL,WMIN
!       WRITE(*,'(A,3F7.1)')      '#PF,EPS,EPSD : ',PF,EPS   ,EPSD
!       WRITE(*,'(A,L7,2F7.4)')   '#extrad,Tb,Ta: ',extrad,Tb,Ta
!       write(*,'(A,L7,2F7.4)')  '#Q Dependence: ',Qdep,XPAR0,XPAR

c PART 3 - array initialization for binning (to mimic HRS?)

c -WRA
      Pmax=E-WMIN
      Pmin=E-WMAX
      IF (RATES) THEN
        CALL GETRATES(deltaP,current,domega) ! defines parameters for Luminosity and solid angle calculatrions/integration -wra
        CALL INITIALIZE(Pmax,deltaP,xnu,sigmatot,sigave,P0,P0N) ! zeros arrays for some binning procedure -wra
      ENDIF

c PART 4 -  some checks

c -WRA
      W=WMIN ! W is omega (photon energy) -wra
      ii=0
c      WRITE(6,'(A,F10.1)') "E0: ",E
  10  W = W + DELTAW  ! E-E'
c      WRITE(6,'(A,F10.1)') "W : ",W," min/max ",WMIN,"/",WMAX
      ii=ii+1
      IF(W.GE.WMAX)GO TO 30
      IF(W.LE.1.)GO TO 30
!       write(6,*)'sigma : '

c PART 5 - Calculate the cross sections

c -WRA
      THR=TH*PI/180.
      QMS=4.*E*(E-W)*SIN(THR/2.)**2 ! Q squared 
      SIGQFZA=SIGQFS(E,TH,W,Z,A,EPS,PF)*SCALE ! Q.E. Peak
      SIGDA=SIGDEL(E,TH,W,A,EPSD,PF)*SCALE    ! Delta
      SIGXA=SIGX(E,TH,W,A)*SCALE              ! DIS?
      SIGR1A=SIGR1(E,TH,W,A,PF)*SCALE         ! Resonance 1500MeV
      SIGR2A=SIGR2(E,TH,W,A,PF)*SCALE         ! Resonance 1700MeV
      SIG2NA=SIG2N(E,TH,W,Z,A,PF)*SCALE       ! Dip region
      SIG=SIGQFZA+SIGDA+SIGXA+SIGR1A+SIGR2A+SIG2NA
c

c PART 6 -  Photon scattering

c -WRA
      xW = pm**2+2.*pm*W-QMS   ! missing mass squared
      xW = sqrt(abs(xW)) 
      QVS=QMS+W**2
      EPD=E-(DM-PM)*(DM+PM)/2./PM
      EPD=EPD/(1.+2.*E*SIN(THR/2.)**2/PM)
      EPD=EPD-EPSD
      EPQ=4.*E**2*SIN(THR/2.)**2/2./PM
      EPQ=EPQ/(1.+2.*E*SIN(THR/2.)**2/PM)+EPS
      EPQ=E-EPQ
      EKAPPA=W-QMS/2./PM
      IF(EKAPPA.GT.-PM/2.)THEN
        CMTOT=SQRT(PM**2+2.*PM*EKAPPA)
      ELSE
        CMTOT=PM
      ENDIF
      FLUX=(ALPH/2./PI**2)*((E-W)/E)*((2.*PM*W-QMS)/2./PM/QMS)
      POLARI=1./(1.+2.*QVS*TAN(THR/2.)**2/QMS)
      FLUX=FLUX/(1.-POLARI)
      IF(EKAPPA.LT.0.)PHOTSIG=0.
      PHOTSIG=SIG/FLUX
C------
c PART 7 -  Radiative corrections 

c-WRA
      IF(.NOT.RAD) THEN
!         write (8,'(2F7.1,2E10.3)') W,xW,0.00,SIG 
        GO TO 10
      ENDIF
      CALL RADIATE(E,TH,W,SIG,SIGRAD)
!       write (8,'(2F7.1,2E10.3)') W,xW,SIGRAD,SIG   ! nu,W,exp. & born XS in "units"
C      write (18,'(F7.1,3E10.3)') W,SIGRAD,SIGRAD*0.02,SIGRAD*0.05
      RES=SIGRAD
!       write(6,*)'sigma : ',SIGRAD
      IF(RATES) THEN
        sigmatot(ii)=SIGRAD*UNSCALE                ! convert back to cm^2 for rates.
        xnu(ii)=W
      ENDIF

      IF(.NOT.ISCFUNC)  THEN ! skip this because we want just one cross section
        GO TO 10 ! REPEAT for next bin in photon energy -wra
      ENDIF
  30  CONTINUE
      NBIN=ii
      close(8)


c PART 7 -  COMPUTE THE RATES 
c -WRA
c  GET RATES IS BROKEN , Don't USE IT - D. Flay 10/12/2011
      IF(.NOT.ISCFUNC)  THEN
      IF (RATES) THEN
      if (Z.EQ.2) THEN ! Helium-3
        XLum=(current/1.6E-19)*density*zlength ! Luminosity

        do i=1,NBIN
           do j=1,30
             Pup= P0(j)*(1.0+deltap)
             Plow=P0(j)*(1.0-deltap)
             escat=E-xnu(i)
             IF (escat.LE.Pup.AND.ESCAT.GE.Plow) THEN
               sigave(j)=sigave(j)+sigmatot(i)
               P0N(j)=P0N(j)+1
             ENDIF
           enddo
        enddo
        do j=1,30 
          write(6,*) "DEBUG3",Pmin,P0N(j),P0(j)
          if ( (P0(j).GT.Pmin).and.(P0N(j).gt.0) ) then
            sigave(j)=sigave(j)/P0N(j)
            deltaE=P0(j)*(2*deltaP)
            rate=Xlum*sigave(j)*domega*deltaE
            write(10,*) P0(j), rate
          endif
        enddo
      endif
      ENDIF
      ENDIF
      IF(.NOT.ISCFUNC)  THEN
        GOTO 990
      ENDIF
999   CONTINUE
      IF(.NOT.ISCFUNC)  THEN
        STOP
      ENDIF
      END


      !> Calculates the QFS cross section without applying
      !! radiative corrections
      !!
      !! @ingroup QFS
      !! 
      !! @param MYEPS 
      !! @param MYEPSD
      !! @param MYPF 
      !!
      !!  REVISION HISTORY: 
      !!  
      !!  - 10/17/2011 - Whitney Armstrong
      !!    Added qfs_born subroutine
      !!
      !!
      SUBROUTINE qfs_born(RES,ZZ,NN,MYEPS,MYEPSD,MYPF,
     &                    EBEAM,PCENTRAL,THETA,XSTYPE)
c      SUBROUTINE qfs_born(RES,ZZ,NN,MYEPS,MYEPSD,MYPF,
c     &                    EBEAM,PCENTRAL,DELP,THETA,DELTHETA,XSTYPE)
      IMPLICIT REAL*8 (A-H,O-Z)
      LOGICAL Qdep
      CHARACTER TAG*4,LINE*80,nucleus*3,units*2
      INTEGER XSTYPE     ! xs type: leave as zero if you want the full xs; see below for options 1--6    
      COMMON/PAR/E,TH,W,Z,A,EPS,EPSD,PF,SPENCE
      COMMON/ADDONS/SCALE,Tb,Ta,extrad
      COMMON/QDEPENDENCE/XPAR0,XPAR1,Qdep
      REAL*8 current, density,zlength,domega,deltap,P0(30),P0N(30)
      REAL*8 MYEPS,MYEPSD,MYPF 

      PM=939.D0
      DM=1232.D0
      ALPH=1./137.03604D0
      HBARC=197.32858D0
      PI=ACOS(-1.)

      ISCFUNC    =.TRUE.  ! skips legacy data input for C function.
      units      ='pb'    ! units/MeV-sr
c WRA :
c INPUTS: Z,A,
c        TH(theta), PF(fermi momentum),
c        EPSD(elastic to quasi-eleastic peak separation energy nomially 10MeV),
c        EPSD(delta separation energy nominally15MeV), 
c        Tb(target radiation before scattering typically ~0.01 but depends
c        very much on experiment), 
c        Ta(target radiation length after scattering 
c        typically ~0.01 but depends very much on experiment)
c        WMIN(minimum photon energy, omgea)
c        WMAX(maximum photon energy)
c        DELTAW(step size in photon/E' energy
c        PCENTRAL(central momentum of acceptance)
c        DELP('symmetric' momentum acceptance (PCENTRAL-PMIN)/PCENTRAL)
c (Z,A,E,TH,PF,EPS,EPSD,Tb,Ta)
c RES,ZZ,NN,PART,EBEAM,PCENTRAL,DELP,THETA,DELTHETA
        Z=ZZ
        N=NN
        A=Z+N
        E=EBEAM
        TH=THETA*180.0/PI ! degrees
!         WMIN=E-PCENTRAL*(1.0+DELP)
!         WMAX=E-PCENTRAL*(1.0-DELP)
!         DELTAW=(WMAX-WMIN)/2.0 ! ONE STEP!!! (centered in the middle of the bin)
!         PF=130.0
!         EPS=10.0
!         EPSD=15.0
c        Tb=0.007
c        Ta=0.04
c        density= 12.25 ! needed only for Rates (Luminosity)
c        zlength = 40.0 ! needed only for Rates (Luminosity)
c        TAG     = '    '
c        nucleus = '   '

      W=E-PCENTRAL ! W is omega (photon energy) 

      EPS  = MYEPS 
      EPSD = MYEPSD 
      PF   = MYPF 

      ! WRITE(*,*) 'Fermi momentum: ',PF 
      ! WRITE(*,*) 'Nucleon separation energy: ',EPS 
      ! WRITE(*,*) 'Delta separation energy: ',EPSD 

      CALL GETSCALE(units,SCALE,UNSCALE)
      CALL GETQDEP(A,Qdep,XPAR0,XPAR1) !get pars to adjust QE peak 

c     Calculate the cross sections
      THR=TH*PI/180.
      QMS=4.*E*(E-W)*SIN(THR/2.)**2 ! Q squared 
      SIGQFZA=SIGQFS(E,TH,W,Z,A,EPS,PF)*SCALE ! Q.E. Peak
      SIGDA=SIGDEL(E,TH,W,A,EPSD,PF)*SCALE    ! Delta
      SIGXA=SIGX(E,TH,W,A)*SCALE              ! DIS?
      SIGR1A=SIGR1(E,TH,W,A,PF)*SCALE         ! Resonance 1500MeV
      SIGR2A=SIGR2(E,TH,W,A,PF)*SCALE         ! Resonance 1700MeV
      SIG2NA=SIG2N(E,TH,W,Z,A,PF)*SCALE       ! Dip region
      SIG=SIGQFZA+SIGDA+SIGXA+SIGR1A+SIGR2A+SIG2NA
      IF (SIG .LT. 0.0 ) THEN
        RES=0.0
      ELSE
        RES=SIG
      ENDIF

      IF(XSTYPE .eq. 1) THEN 
       ! QE 
       RES = SIGQFZA 
      ELSE IF(XSTYPE .eq. 2) THEN 
       ! Delta 
       RES = SIGDA 
      ELSE IF(XSTYPE .eq. 3) THEN 
       ! DIS  
       RES = SIGXA 
      ELSE IF(XSTYPE .eq. 4) THEN
       ! 1500 MeV resonance  
       RES = SIGR1A 
      ELSE IF(XSTYPE .eq. 5) THEN 
       ! 1700 MeV resonance  
       RES = SIGR2A 
      ELSE IF(XSTYPE .eq. 6) THEN 
       ! Dip region 
       RES = SIG2NA 
      ENDIF 

      END

      !> Calculates the QFS cross section applying
      !! radiative corrections
      !!
      !! @ingroup QFS
      !!
      !!  REVISION HISTORY: 
      !!  
      !!  - 10/19/2011 - Whitney Armstrong
      !!    Added qfs_radiated subroutine
      !!
      !!
      SUBROUTINE qfs_radiated(RES,ZZ,NN,EBEAM,PCENTRAL,DELP,THETA,
     ^ DELTHETA)
      IMPLICIT REAL*8 (A-H,O-Z)
      LOGICAL Qdep
      CHARACTER TAG*4,LINE*80,nucleus*3,units*2
      COMMON/PAR/E,TH,W,Z,A,EPS,EPSD,PF,SPENCE
      COMMON/ADDONS/SCALE,Tb,Ta,extrad
      COMMON/QDEPENDENCE/XPAR0,XPAR1,Qdep
      REAL*8 current, density,zlength,domega,deltap,P0(30),P0N(30)

      PM=939.D0
      DM=1232.D0
      ALPH=1./137.03604D0
      HBARC=197.32858D0
      PI=ACOS(-1.)

!       ISCFUNC    =.TRUE.  ! skips legacy data input for C function.
      units      ='pb'    ! units/MeV-sr
c WRA :
c INPUTS: Z,A,
c        TH(theta), PF(fermi momentum),
c        EPSD(elastic to quasi-eleastic peak separation energy nomially 10MeV),
c        EPSD(delta separation energy nominally15MeV), 
c        Tb(target radiation before scattering typically ~0.01 but depends
c        very much on experiment), 
c        Ta(target radiation length after scattering 
c        typically ~0.01 but depends very much on experiment)
c        WMIN(minimum photon energy, omgea)
c        WMAX(maximum photon energy)
c        DELTAW(step size in photon/E' energy
c        PCENTRAL(central momentum of acceptance)
c        DELP('symmetric' momentum acceptance (PCENTRAL-PMIN)/PCENTRAL)
        Z=ZZ
        N=NN
        A=Z+N
        E=EBEAM
        TH=THETA*180.0/PI ! degrees
        PF=130.0
        EPS=10.0
        EPSD=15.0
        Tb=0.007
        Ta=0.04
        density= 12.25 ! needed only for Rates (Luminosity)
        zlength = 40.0 ! needed only for Rates (Luminosity)
        TAG     = '    '
        nucleus = '   '
        W=E-PCENTRAL ! W is omega (photon energy) 
      CALL GETSCALE(units,SCALE,UNSCALE)
      CALL GETQDEP(A,Qdep,XPAR0,XPAR1) !get pars to adjust QE peak 
c     Calculate the cross sections
      THR=TH*PI/180.
      QMS=4.*E*(E-W)*SIN(THR/2.)**2 ! Q squared 
      SIGQFZA=SIGQFS(E,TH,W,Z,A,EPS,PF)*SCALE ! Q.E. Peak
      SIGDA=SIGDEL(E,TH,W,A,EPSD,PF)*SCALE    ! Delta
      SIGXA=SIGX(E,TH,W,A)*SCALE              ! DIS?
      SIGR1A=SIGR1(E,TH,W,A,PF)*SCALE         ! Resonance 1500MeV
      SIGR2A=SIGR2(E,TH,W,A,PF)*SCALE         ! Resonance 1700MeV
      SIG2NA=SIG2N(E,TH,W,Z,A,PF)*SCALE       ! Dip region
      SIG=SIGQFZA+SIGDA+SIGXA+SIGR1A+SIGR2A+SIG2NA
      IF (SIG .LT. 0.0 ) THEN
        RES=0.0
      ELSE
        RES=SIG
        CALL RADIATE(E,TH,W,SIG,SIGRAD)
      ENDIF
      RES=SIGRAD

      RES=SIGRAD
      END

      !> Calculates the radiative corrections
      !!
      !! @ingroup QFS
      !!
      !! DOES NOT INCLUDE CONTRIBUTION FROM ELASTIC SCATTERING
      !!
      !! REVISION HISTORY: 
      !!
      !!  - 09/16/02 K. Slifer
      !!     Rewrote subroutine to include external bremsstrahlung using
      !!     formalism from S. Stein et al. Phys. Rev. D 12 7. Equation (A82)
      !!     Where possible the equation number is given with each expression.
      !!
      SUBROUTINE RADIATE(E,TH,W,SIGNR,SIGRAD)
      IMPLICIT REAL*8 (A-H,O-Z)
      LOGICAL extrad
      COMMON/PAR/E0,TH0,W0,Z,A,EPS,EPSD,PF,SPENCE
      COMMON/ADDONS/SCALE,Tb,Ta,extrad
      DEL   = 10
      PREC  = .0001  ! 0.001 gets rid of glitches

      if (.NOT.extrad) THEN  ! don't apply external rad. cor.
        Tb=0.0
        Ta=0.0
      endif
      xb    = 4./3.  ! A45
      XM    = 931.49 ! Mass of the nucleon
      XMT   = A*XM   ! Mass of the target
      ALPH  = 1./137.03604
      EMASS = 0.511
      PI    = ACOS(-1.)
      THR   = TH*PI/180.
      ARG   = COS(THR/2.)**2

      SPENCE= PI**2/6.-LOG(ARG)*LOG(1.-ARG)
      DO 10 NSP=1,50
 10     SPENCE = SPENCE-ARG**NSP/FLOAT(NSP)**2

      QMS   = 4.*E*(E-W)*SIN(THR/2.)**2

      D1=(2.*ALPH/PI)*(LOG(QMS/EMASS**2)-1.)      ! =2b*tr (A57)
      tr=D1/2./xb

      D2 = 13.*(LOG(QMS/EMASS**2)-1.)/12.-17./36. ! this term dominates D2
      D2 = D2 +0.5*(PI**2/6.-SPENCE)
      D2 = D2 -1./4.*( LOG( E/(E-W) ) )**2        ! Correct. to peak. appr.
      D2 = D2*(2.*ALPH/PI)                 
      D2 = D2+0.5772*xb*(Tb+Ta)                   ! Here D2= F-1
      xF = (1.+D2)                                ! (A44)

      Tpb = tr + Tb
      Tpa = tr + Ta  
   
      R   = ( XMT+E*(1-COS(THR)) )/( XMT-(E-W)*(1-COS(THR)) ) ! (A83)
      eta = LOG(1440.*Z**(-2./3.) )/LOG(183.*Z**(-1./3.) )    ! (A46)
      xi  = (PI*EMASS/2./ALPH)*(Ta+Tb)
      xi  = xi/( (Z+eta)*LOG(183.*Z**(-1./3.)) )              ! (A52)

      SIGRAD = SIGNR * xF
      SIGRAD = SIGRAD*( (R*DEL/E  )**(xb*Tpb) )
      SIGRAD = SIGRAD*( (DEL/(E-W))**(xb*Tpa) )
      SIGRAD = SIGRAD*(1. - xi/DEL/( 1.-xb*(Tpb+Tpa)) )

      TERM1=(R*DEL/E  )**(xb*Tpb)
      TERM2=(DEL/(E-W))**(xb*Tpa)
      TERM3=(1. - xi/DEL/( 1.-xb*(Tpb+Tpa)) )
      TERM4=xF
C
C-----Stein's 1st integral wrt dEs' (A82)
C
C     limits of 0 to W-DEL give almost same results
C
      X1LO   = (E-W)*( XMT/( XMT-2.*(E-W)*(SIN(THR/2.))**2) -1.0 )
      X1HI   = W-R*DEL
      ANS_Es = 0.
      IF (X1HI.GT.X1LO) THEN
        CALL ROM(X1LO,X1HI,PREC,ANS_Es,KF,1)
      ELSE
        write(6,*) "Integral dEs. SKIPPING:nu,lower,higher ",W,X1LO,X1HI
      ENDIF
      ANS_Es = ANS_Es * SCALE
C
C-----Stein's 2nd integral wrt dEp' (A82)
C
C     limits of 0 to W-DEL give almost same results
C
      X2LO   = E*( 1.0-XMT/( XMT+2.*E*(SIN(THR/2.))**2) )
      X2HI   = W-DEL 
      ANS_Ep = 0.
      IF (X2HI.GT.X2LO) THEN
         CALL ROM(X2LO,X2HI,PREC,ANS_Ep,KF,2)
      ELSE
         write(6,*) "Integral dEp. SKIPPING:nu,lower,higher ",W,X2LO,X2HI
      ENDIF
      ANS_Ep = ANS_Ep * SCALE

CCDEBUG
C      WRITE(6,'(F7.1,3F8.2)') W,(SIGRAD-SIGNR)/SIGNR*100., 
C     &                          ANS_Es/SIGNR*100.,
C     &                          ANS_Ep/SIGNR*100.
C
      SIGRAD = SIGRAD + ANS_Es + ANS_Ep
CDEBUG
C      WRITE(6,'(F7.1,6F8.2)') 
C     &         W,TERM1,TERM2,TERM3,TERM4,
C     &         ANS_Es/SIGRAD*100.0,ANS_Ep/SIGRAD*100.0

      RETURN
      END

      !> Integration of cross section for radiative corrections
      !! 
      !! @ingroup QFS
      !!
      !!  REVISION HISTORY: 
      !!
      !!
      SUBROUTINE VALY(X,F,IFUNC)
      IMPLICIT REAL*8 (A-H,O-Z)
      LOGICAL extrad
C     REAL*8 FUNCTION F(X)
      COMMON/PAR/E,TH,W,Z,A,EPS,EPSD,PF,SPENCE
      COMMON/ADDONS/SCALE,Tb,Ta,extrad

      if (.NOT.extrad) THEN  ! apply external rad. cor.
        Tb=0.0
        Ta=0.0
      endif
      ALPH  = 1./137.03604
      EMASS = 0.511
      PI    = ACOS(-1.)
      THR   = TH*PI/180.
      xb    = 4./3.
      XM    = 931.49 ! Mass of the nucleon
      XMT   = A*XM   ! Mass of the target

      eta = LOG(1440.*Z**(-2./3.) )/LOG(183.*Z**(-1./3.) )
      xi  = (PI*EMASS/2./ALPH)*(Ta+Tb)
      xi  = xi/( (Z+eta)*LOG(183.*Z**(-1./3.)) )
      R   = ( XMT+E*(1-COS(THR)) )/( XMT-(E-W)*(1-COS(THR)) )
C
C-----Stein's 2nd integral dEp'
C
      QMS2  = 4.*E*(E-X)*SIN(THR/2.)**2  ! 1/15/03
      tr2   = 1./xb*(ALPH/PI)*(LOG(QMS2/EMASS**2)-1.)
      Tpb   = tr2 + Tb
      Tpa   = tr2 + Ta

      D2    = 13.*(LOG(QMS2/EMASS**2)-1.)/12.-17./36.
      D2    = D2 - 1./4.*( LOG( E/(E-W) ) )**2 !KS. Correction to peak. approx.
      D2    = D2 + 0.5*(PI**2/6.-SPENCE)
      D2    = D2 * (2.*ALPH/PI)
      D2    = D2 + 0.5772*xb*(Tb+Ta)

      SIG2  = SIGQFS(E,TH,X,Z,A,EPS,PF)
      SIG2  = SIG2 + SIGDEL(E,TH,X,A,EPSD,PF)
      SIG2  = SIG2 + SIGX(E,TH,X,A)
      SIG2  = SIG2 + SIGR1(E,TH,X,A,PF)
      SIG2  = SIG2 + SIGR2(E,TH,X,A,PF)
      SIG2  = SIG2 + SIG2N(E,TH,X,Z,A,PF)
      F2    = ( xb*Tpa/(W-X) ) *phi((W-X)/(E-X))
      F2    = F2 + xi/(2.*(W-X)**2)
      F2    = F2 * SIG2*(1.+D2)
      F2    = F2 * ( (W-X)/(E-X) )**(xb*Tpa)
      F2    = F2 * ( (W-X)*R/(E) )**(xb*Tpb)
C
C-----Stein's 1st integral dEs'
C
      QMS1  = 4.*(E-W+X)*(E-W)*SIN(THR/2.)**2    !    1/15/03
      tr1   = 1./xb*(ALPH/PI)*(LOG(QMS1/EMASS**2)-1.)
      Tpb   = tr1 + Tb
      Tpa   = tr1 + Ta

      D2    = 13.*(LOG(QMS1/EMASS**2)-1.)/12.-17./36.
      D2    = D2 - 1./4.*( LOG( E/(E-W) ) )**2 !Corr. to peak. approx.
      D2    = D2 + 0.5*(PI**2/6.-SPENCE)
      D2    = D2 * (2.*ALPH/PI)
      D2    = D2 + 0.5772*xb*(Tb+Ta) ! 1/14/02

      SIG1  = SIGQFS(E-W+X,TH,X,Z,A,EPS,PF)
      SIG1  = SIG1 + SIGDEL(E-W+X,TH,X,A,EPSD,PF)
      SIG1  = SIG1 +   SIGX(E-W+X,TH,X,A)
      SIG1  = SIG1 +  SIGR1(E-W+X,TH,X,A,PF)
      SIG1  = SIG1 +  SIGR2(E-W+X,TH,X,A,PF)
      SIG1  = SIG1 +  SIG2N(E-W+X,TH,X,Z,A,PF)

      F1    = ( xb*Tpb/(W-X) ) *phi((W-X)/(E))   ! 
      F1    = F1 + xi/(2.*(W-X)**2)
      F1    = F1 * SIG1*(1.+D2)
      F1    = F1 * ( (W-X)/((E-W)*R) )**(xb*Tpa)
      F1    = F1 * ( (W-X)/ (E)      )**(xb*Tpb) ! 

      IF(IFUNC.EQ.2) THEN      ! dEp'
        F=F2
      ELSEIF (IFUNC.EQ.1) THEN ! dEs'
        F=F1
      ELSE
        write(6,*) "PROBLEM. "
        STOP
      ENDIF
      RETURN
      END

      !> ROMBERG METHOD OF INTEGRATION
      !!
      !! @ingroup QFS
      !!
      !!  REVISION HISTORY: 
      !!
      !!
      SUBROUTINE ROM(A,B,EPS,ANS,K,IFUNC)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION W(50,50)
      H=B-A
      K=0
      CALL VALY(A,FA,IFUNC)
      CALL VALY(B,FB,IFUNC)
      W(1,1)=(FA+FB)*H/2.
    4 K=K+1
      IF(K.GE.49)GO TO 5
      H=H/2.
      SIG=0.
      M=2**(K-1)
      DO 1 J=1,M
      J1=2*J-1
      X=A+FLOAT(J1)*H
      CALL VALY(X,F,IFUNC)
C      write(6,*) "DEBUG: ",k,IFUNC,f
    1 SIG=SIG+F
      W(K+1,1)=W(K,1)/2.+H*SIG
      DO 2 L=1,K
      IU=K+1-L
      IV=L+1
    2 W(IU,IV)=(4.**(IV-1)*W(IU+1,IV-1)-W(IU,IV-1))/(4.**(IV-1)-1.)
      E=(W(IU,IV)-W(IU,IV-1))/W(IU,IV)
      IF(ABS(E)-EPS) 3,3,4
    3 ANS=W(1,IV)
      RETURN
    5 PRINT 100
  100 FORMAT(' K OVERFLOW')
      CALL EXIT(0)
      END
      
      !> returns 
      !! 
      !! @ingroup QFS
      !!
      !!  REVISION HISTORY: 
      !!
      !!
      REAL*8 function phi(x)
      IMPLICIT REAL*8 (A-H,O-Z)
      phi=1.0-x+3./4.*x**2
      RETURN
      END

      !> Opens various output files
      !! 
      !! @deprecated Left over from Slifer et al.
      !! 
      !! @ingroup QFS
      !!
      !!  REVISION HISTORY: 
      !!
      !! - 10/19/2011 Whitney Armstrong 
      !!   Called it obsolete!
      !!
      SUBROUTINE OPENFILES(TAG,INTERACTIVE,RATES,nucleus)
      CHARACTER TAG*4,nucleus*3
      LOGICAL INTERACTIVE,RATES
 
      if (nucleus.EQ.'He3') THEN ! He-3
         OPEN(8,FILE=TAG//'_He_sig_qfs.out',STATUS='UNKNOWN')
      if (RATES) THEN
         OPEN(10,FILE=TAG//'_He_rates.out',STATUS='UNKNOWN')
      endif
      elseif (nucleus.EQ.'Nit') THEN ! Nitrogen
        OPEN(8,FILE=TAG//'_N2_sig_qfs.out',STATUS='UNKNOWN')
      elseif (nucleus.EQ.'NH3') THEN ! Ammonia
        OPEN(8,FILE=TAG//'_NH3_sig_qfs.out',STATUS='UNKNOWN')
      elseif (nucleus.EQ.'Sil') THEN ! Silicon
        OPEN(8,FILE=TAG//'_Si_sig_qfs.out',STATUS='UNKNOWN')
      elseif (nucleus.EQ.'Irn') THEN ! Iron
        OPEN(8,FILE=TAG//'_Fe_sig_qfs.out',STATUS='UNKNOWN')
      elseif (INTERACTIVE) THEN ! will prompt user for input
        OPEN(8,FILE='sig_qfs.out',STATUS='UNKNOWN')
C        OPEN(18,FILE='sig_qfs.fak',STATUS='UNKNOWN')
      else
        WRITE(6,*) 'Unknown target '
        STOP
      endif
      RETURN
      END

      !> DETERMINE DESIRED UNITS OF CROSS SECTION
      !!
      !! @ingroup QFS
      !!
      !! @param units can be 'cm','mb','ub','nb', or 'pb'
      !!
      !!  REVISION HISTORY: 
      !!
      SUBROUTINE GETSCALE(units,SCALE,UNSCALE)
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER units*2

C-----DETERMINE DESIRED UNITS OF CROSS SECTION
      if(units.EQ.'cm') then     ! cm^2/(MeV sr)
        SCALE=1.D-26
        UNSCALE=1.0
      elseif(units.EQ.'mb')then  ! mb/(MeV sr)
        SCALE=1.D+01
        UNSCALE=1.D-27
      elseif(units.EQ.'ub')then  ! ub/(MeV sr)
        SCALE=1.D+04
        UNSCALE=1.D-30
      elseif(units.EQ.'nb')then  ! nb/(MeV sr)
        SCALE=1.D+07
        UNSCALE=1.D-33
      elseif(units.EQ.'pb') then ! pb/(MeV sr)
        SCALE=1.D+10
        UNSCALE=1.D-36
      else
        WRITE(6,*) 'Unknown scale'
        STOP
      endif
C      write(8,'(A,A)') '#',units
      RETURN
      END



      !> DETERMINE nucleus charge, mass
      !!
      !! @ingroup QFS
      !!
      !!  REVISION HISTORY: 
      !!
      SUBROUTINE GETZA(nucleus,INTERACTIVE,Z,A)
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER nucleus*3 
      LOGICAL INTERACTIVE
C-----DETERMINE nucleus charge, mass
      if (nucleus.EQ.'He3') THEN ! He-3
        Z=2
        A=3
      elseif (nucleus.EQ.'Nit') THEN ! Nitrogen
        Z=7
        A=14
      elseif (nucleus.EQ.'Sil') THEN ! Silicon
        Z=14
        A=28
      elseif (nucleus.EQ.'Irn') THEN ! Iron
        Z=26
        A=56
      elseif (nucleus.EQ.'NH3') THEN ! Ammonia
        Z=10
        A=17
      elseIF (INTERACTIVE) THEN
        CONTINUE
      ELSE
        write(6,*) 'Problem'
        stop
      endif
C------
      RETURN
      END


      !> Introduce Q-dependent correction to Q.E. Peak height
      !!
      !! @ingroup QFS
      !!
      !!  REVISION HISTORY: 
      !!
      SUBROUTINE GETQDEP(A,Qdep,XPAR0,XPAR1)
C     Introduce Q-dependent correction to Q.E. Peak height
      IMPLICIT REAL*8 (A-H,O-Z)
      LOGICAL Qdep

      XPAR1 = 0.0453828 ! GeV^2  ! 12/13/02 All available CARBON(only) FIT
      XPAR0 = 0.686704  ! GeV^2
C      XPAR1 = 0.0683579 ! GeV^2  ! He3 Fit. Bates data, T.S. Ueng Thesis
C      XPAR0 = 0.757269  ! GeV^2

C      XPAR1 = 0.0476549 ! GeV^2 ! He3 Fit. E94010 unfolded xs.
C      XPAR0 = 0.801282  ! GeV^2
      RETURN
      END

      !> Prompt user for input
      !!
      !!
      !!  REVISION HISTORY: 
      !!
      SUBROUTINE PROMPT(Z,A,E,TH,PF,EPS,EPSD,Tb,Ta)
      IMPLICIT REAL*8 (A-H,O-Z)
C     Prompt user for input
c      WRITE(6,'(A)')  'Enter Z,A: '
c      READ(*,*) Z,A
c      Z = 7.0
c      A = 14.0
      WRITE(6,'(A)' ) 'Incident energy(MeV) and scatt angle(deg.): '
      READ(*,*) E,TH
c      WRITE(6,'(A)' ) 'P_fermi,Nucleon and Delta separation (MeV): '
c      READ(*,*) PF,EPS,EPSD
      PF   = 221.0
      EPS  = 25.0
      EPSD = 15.0
c      WRITE(6,'(A)') 'Target thickness Tb, Ta (in radiation lengths):'
c      READ(*,*) Tb,Ta
c      Tb = 0.0
c      Ta = 0.0      
      RETURN
      END

      !> INPUTS NEEDED FOR RATES CALCULATIONS.
      !!
      !! @ingroup QFS
      !!
      !!  REVISION HISTORY: 
      !!
      SUBROUTINE GETRATES(deltaP,current,domega)
      IMPLICIT REAL*8 (A-H,O-Z)
C-----INPUTS NEEDED FOR RATES CALCULATIONS...
C   --E94010 RUNPLAN VALUES--
      deltaP  = .04
      current = 10.0
      domega  = 3.9
c      density = 7.4
C   --MORE REALISTIC VALUES--
C      deltaP = .045           ! Spectrometer Momentum acceptance. DeltaP/P
C      current= 10.0           ! Amps
C      domega = 6.7            ! Acceptance in msr

      current= current*1.0E-6 ! Convert from microAmps to Amps
      domega = domega*1.0E-3  ! Convert from msr to sr
      RETURN
      END

      !> Called if RATES flag is TRUE
      !!
      !! @ingroup QFS
      !!
      !! Initialize some arrays used for rates calculations
      !!
      !!  REVISION HISTORY: 
      !!
      SUBROUTINE INITIALIZE(Pmax,deltaP,xnu,sigmatot,sigave,P0,P0N)
      IMPLICIT REAL*8 (A-H,O-Z)
      real*8 P0(30),P0N(30)
      real*8 sigmatot(1000), xnu(1000),sigave(30)
C----Initialize some arrays used for rates calculations
      do i =1,1000
        sigmatot(i)=0
        xnu(i)     =0
      enddo
      do n=1,30
        P0(n)    =Pmax*(1.0-deltap)**(2*n-1) ! find central momentums
        P0N(n)   =0                          ! Number of bins covered
        sigave(n)=0.0
      enddo
      RETURN
      END



CDEBUG      These are the most recent form factors available. 
C           Do not use option 'Qdep' with these FF.
CDEBUG      REAL*8 FUNCTION GEP(QMS,AP)
CDEBUG      IMPLICIT REAL*8 (A-H,O-Z)
CDEBUG      GEP=1./(1.+QMS/AP**2)**2
CDEBUG      RETURN
CDEBUG      END 
CDEBUG      
CDEBUG      REAL*8 FUNCTION GEN(QMS,AP)
CDEBUG      IMPLICIT REAL*8 (A-H,O-Z)
CDEBUGC     H.ZHU et al. PRL 87, Number 8, 081801-1 (2001)
CDEBUG
CDEBUG      PM   = 939.
CDEBUG      UN   = -1.91304184
CDEBUG      xp   = 4.36   ! +- 1.11
CDEBUG      xa   = 0.895  ! +- 0.039 
CDEBUG
CDEBUG      GEN = -UN * xa * TAU(QMS)/( 1.0+xp*TAU(QMS) )
CDEBUG      GEN = GEN * GEP(QMS,AP)
CDEBUG      RETURN
CDEBUG      END
CDEBUG
CDEBUG      REAL*8 FUNCTION GMP(QMS,AP)
CDEBUG      IMPLICIT REAL*8 (A-H,O-Z)
CDEBUGC     Gayou et al. PRL 88,number 9, 092301-1 (2002)
CDEBUG      UP   =  2.7928456
CDEBUG      GMP  =  UP * GEP(QMS,AP)
CDEBUG      GMP  =  GMP/(1.0 - 0.13*(QMS-0.04) )
CDEBUG      RETURN
CDEBUG      END
CDEBUG
CDEBUG      REAL*8 FUNCTION GMN(QMS,AP)
CDEBUG      IMPLICIT REAL*8 (A-H,O-Z)
CDEBUG      UN   = -1.91304184
CDEBUG      GMN  = UN * GEP(QMS,AP)
CDEBUG      RETURN
CDEBUG      END
CDEBUG
      REAL*8 FUNCTION TAU(QMS)
      IMPLICIT REAL*8 (A-H,O-Z)
      PM   = 939.
      TAU  = QMS/4.0/PM**2
      RETURN
      END

      REAL*8 FUNCTION GEP(QMS,AP)
      IMPLICIT REAL*8 (A-H,O-Z)
      GEP=1./(1.+QMS/AP**2)**2
      RETURN
      END

      REAL*8 FUNCTION GEN(QMS,AP)
      IMPLICIT REAL*8 (A-H,O-Z)
      PM   = 939.
      UN   = -1.91304184

      GEN = -UN
      GEN = GEN * TAU(QMS)/( 1.0+5.6*TAU(QMS) )
      GEN = GEN * GEP(QMS,AP)
      RETURN
      END

      REAL*8 FUNCTION GMP(QMS,AP)
      IMPLICIT REAL*8 (A-H,O-Z)
      UP   =  2.7928456
      GMP  =  UP * GEP(QMS,AP)
      RETURN
      END

      REAL*8 FUNCTION GMN(QMS,AP)
      IMPLICIT REAL*8 (A-H,O-Z)
      UN   = -1.91304184
      GMN  = UN * GEP(QMS,AP)
      RETURN
      END


