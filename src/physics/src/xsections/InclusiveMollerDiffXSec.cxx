#include "insane/xsections/InclusiveMollerDiffXSec.h"
#include "insane/base/PhysicalConstants.h"

namespace insane {
namespace physics {

InclusiveMollerDiffXSec::InclusiveMollerDiffXSec()
{
   fID            = 100004021;
   SetTitle("InclusiveMollerDiffXSec");
   SetPlotTitle("Moller cross-section");
   fLabel         = "#frac{d#sigma}{d#Omega}";
   fUnits         = "nb/sr";
   fnDim          = 2;
   fPIDs.clear();
   fnParticles    = 2;
   fPIDs.push_back(11);
   fPIDs.push_back(11);
}
//______________________________________________________________________________

InclusiveMollerDiffXSec::~InclusiveMollerDiffXSec()
{ }
//______________________________________________________________________________

void InclusiveMollerDiffXSec::InitializePhaseSpaceVariables()
{
   PhaseSpace * ps = GetPhaseSpace();


      // ------------------------------
      // Electron 1
      auto * varEnergy = new PhaseSpaceVariable("energy_e", "E_{e'}",0.50,GetBeamEnergy());
      varEnergy->SetDependent(true);
      varEnergy->SetParticleIndex(0);
      ps->AddVariable(varEnergy);

      auto *   varTheta = new PhaseSpaceVariable("theta_e", "#theta_{e'}",0.0,180.0*degree);
      varTheta->SetParticleIndex(0);
      ps->AddVariable(varTheta);

      auto *   varPhi = new PhaseSpaceVariable("phi_e", "#phi_{e'}",-180.0*degree, 180.0*degree );
      varPhi->SetParticleIndex(0);
      varPhi->SetUniform(true);
      ps->AddVariable(varPhi);

      // ------------------------------
      // Electron 2
      auto * varEnergy_e2 = new PhaseSpaceVariable("energy_e2", "E_{e2}",1.0e-6,12.0);
      varEnergy_e2->SetParticleIndex(1);
      varEnergy_e2->SetDependent(true);
      ps->AddVariable(varEnergy_e2);

      auto *   varTheta_e2 = new PhaseSpaceVariable("theta_e2", "#theta_{e2}",0.0*degree,180*degree);
      varTheta_e2->SetParticleIndex(1);
      varTheta_e2->SetDependent(true);
      ps->AddVariable(varTheta_e2);

      auto *   varPhi_e2 = new PhaseSpaceVariable("phi_e2", "#phi_{e2}",-1.0*TMath::Pi(), 1.0*TMath::Pi() );
      varPhi_e2->SetParticleIndex(1);
      varPhi_e2->SetDependent(true);
      ps->AddVariable(varPhi_e2);

      SetPhaseSpace(ps);
}
//______________________________________________________________________________

Double_t * InclusiveMollerDiffXSec::GetDependentVariables(const Double_t * x) const
{
   using namespace TMath;
   //std::cout << "x[0] = " << x[0] << std::endl;
   //std::cout << "x[1] = " << x[1] << std::endl;
   //std::cout << "x[2] = " << x[2] << std::endl;
   //std::cout << "x[3] = " << x[3] << std::endl;
   //std::cout << "x[4] = " << x[4] << std::endl;
   //std::cout << "x[5] = " << x[5] << std::endl;
   double  th     = x[0];
   double  phi    = x[1];
   double  E0     = fBeamEnergy;
   double  me     = insane::units::_e/GeV;
   double  costh  = Cos(th);
   //double  th_cm  = 2.0*ATan( Tan(th)*Sqrt((fBeamEnergy+me)/(2.0*me)) );

   // 
   double s         = 2.0*me*(me+fBeamEnergy);
   double gamcm     = (fBeamEnergy+me)/Sqrt(s);
   double th_cm     = 2.0*ATan( Tan(th)*gamcm );

   double  E1_prime = me+(E0-me)*Cos(th_cm/2.0)*Cos(th_cm/2.0);
   //double  E1_prime = me*(E0+me+(E0-me)*costh*costh)/(E0+me-(E0-me)*costh*costh);
 
   double  pprime   = Sqrt(E1_prime*E1_prime - me*me);//(2*costh*Sqrt(E0 - me)*me*Sqrt(E0 + me))/(E0 - costh*costh*E0 + me + costh*costh*me);
   double  eprime   = E1_prime;//Sqrt(pprime*pprime + me*me);
   TVector3 k1(0, 0, Sqrt(E0*E0 - me*me) ); // incident electron
   TVector3 p1(1, 0, 0);
   p1.SetMagThetaPhi(pprime, x[0], x[1]);
   TVector3 p2 = k1 - p1 ;

   //std::cout << "eprime = " << E1_prime << std::endl;
   fDependentVariables[0] = eprime;
   fDependentVariables[1] = x[0];
   fDependentVariables[2] = x[1];
   fDependentVariables[3] = Sqrt(p2.Mag2()+me*me);
   fDependentVariables[4] = p2.Theta();
   fDependentVariables[5] = p2.Phi();
   //std::cout << "vectors" << std::endl;
   //std::cout << "fDependentVariables[0] = " << fDependentVariables[0] << std::endl;
   //std::cout << "fDependentVariables[1] = " << fDependentVariables[1] << std::endl;
   //std::cout << "fDependentVariables[2] = " << fDependentVariables[2] << std::endl;
   //std::cout << "fDependentVariables[3] = " << fDependentVariables[3] << std::endl;
   //std::cout << "fDependentVariables[4] = " << fDependentVariables[4] << std::endl;
   //std::cout << "fDependentVariables[5] = " << fDependentVariables[5] << std::endl;
   //k1.Print();
   //k2.Print();
   //p2.Print();
   //if (p2.Phi() < 0.0) fDependentVariables[5] = p2.Phi() + 2.0 * TMath::Pi() ;
   return(fDependentVariables);
}
//________________________________________________________________________

double  InclusiveMollerDiffXSec::MollerCrossSection_CM(double E0, double th) const
{
   // Moller cross section in the center of mass 
   using namespace TMath;
   double costh = Cos(th);
   double sinth = Sin(th);
   double me    = M_e/GeV;
   //double s2    = Sin(th/2.0)*Sin(th/2.0);
   //double c2    = Cos(th/2.0)*Cos(th/2.0);
   //double A     = (1.0/137.0)*(1.0/137.0)/(4.0*E0*E0);
   //double a0    = Power( (2*E0*E0 - me*me)/(E0*E0 - me*me), 2.0);
   //double a1    = 4.0/(s2*s2);
   //double a2    = -3.0/s2;
   //double a3    = (1.0 + 4.0/(s2*s2))/a0;
   //return hbarc2_gev_nb*A*a0*(a1 + a2 + a3);
   //double b0    = 1.0/2.0;
   //double b1    = (1.0+c2*c2)/(s2*s2);
   //double b2    = 2.0/(s2*c2);
   //double b3    = (1.0+s2*s2)/(c2*c2);
   //return hbarc2_gev_nb*A*b0*(b1 + b2 + b3);
   //double h0    = (3.0+costh*costh)*(3.0+costh*costh)/(sinth*sinth*sinth*sinth); // high energy limit.
   //return hbarc2_gev_nb*A*h0;

   double gamma     = fBeamEnergy/me;
   double gammabar  = Sqrt((gamma+1.0)/2.0);
   double t0 = Power((1.0/137.0)/(2.0*me*gammabar*(gammabar*gammabar-1.0)*sinth*sinth),2.0);
   double t1 = (2.0*gammabar*gammabar-1.0)*(2.0*gammabar*gammabar-1.0)*(4.0-3.0*sinth*sinth)
               + (gammabar*gammabar-1.0)*(gammabar*gammabar-1.0)*(4.0+sinth)*sinth;
   double sig2 = hbarc2_gev_nb*t0*t1;
   return sig2;
}
//______________________________________________________________________________

double  InclusiveMollerDiffXSec::MollerCrossSection_CM_dE(double E0, double sin2th) const
{
   // Moller cross section in the center of mass 
   using namespace TMath;
   double s2    = sin2th;
   double me    = M_e/GeV;
   double m     = me;
   double Alpha = (1.0/137.0)*(1.0/137.0)/(4.0*E0*E0);
   double a0    = Power( (2*E0*E0 - me*me)/(E0*E0 - me*me), 2.0);

   double dt0 = ((-4*Power(E0,6) + 4*Power(E0,4)*Power(m,2) - 3*Power(E0,2)*Power(m,4) + 
                   Power(m,6))*Power(Alpha,2))/Power(Power(E0,3) - E0*Power(m,2),3);
   double dt3 = -Alpha*Alpha/(Power(E0,3.0));
   //std::cout << " dt0 " << dt0 <<std::endl;
   //std::cout << " dt3 " << dt3 <<std::endl;

   double a1    = 4.0/(s2*s2);
   double a2    = -3.0/s2;
   double a3    = (1.0 + 4.0/(s2*s2))/dt0;
   return hbarc2_gev_nb*dt0*(a1 + a2 + a3*dt3);
}
//______________________________________________________________________________

double InclusiveMollerDiffXSec::MollerCrossSection_LAB(double E0, double theta) const
{
   using namespace TMath;
   double me     = M_e/GeV;
   double s         = 2.0*me*(me+E0);
   double gamcm     = (E0+me)/Sqrt(s);
   double th_cm     = 2.0*ATan( Tan(theta)*gamcm );
   double Eprime_cm = Sqrt(s/2.0);
   double sig = MollerCrossSection_CM( Eprime_cm, th_cm );
   double dOmega_cm_over_lab= (Sin(th_cm)/Sin(theta))*2.0*gamcm/((1.0+gamcm*gamcm*Tan(theta)*Tan(theta))*Cos(theta)*Cos(theta));
   sig *= dOmega_cm_over_lab;
   return sig;
}
//______________________________________________________________________________

Double_t  InclusiveMollerDiffXSec::EvaluateXSec(const Double_t * x) const
{
   // Get the recoiling proton momentum
   bool part1 = VariablesInPhaseSpace(3, x);
   bool part2 = VariablesInPhaseSpace(3, &x[3]);
   if( (!part1) && (!part2) ) {
      //std::cout << " not in PS \n";
      return(0.0);
   }
   using namespace TMath;
   using namespace insane::units;

   double Eprime = x[0];
   double theta  = x[1];
   double sig1   = 0.0;
   double sig2   = 0.0;

   double sig = 0.0;//std::max(sig1, sig2);
   double Z   = fTargetNucleus.GetZ();

   if( part1 ) {
      sig1 = MollerCrossSection_LAB(fBeamEnergy,theta);
      if( Eprime > fBeamEnergy ) sig1 = 0.0;
      if( IncludeJacobian() ){
         sig1 *= Sin(theta);
      }
      sig=sig1;

      //return( sig*Z );
   }
   if( part2 ) {
      Eprime = x[0+3];
      theta  = x[1+3];
      sig2 = MollerCrossSection_LAB(fBeamEnergy,theta);
      if( Eprime > fBeamEnergy ) sig2 = 0.0;
      if( IncludeJacobian() ){
         sig2 *= Sin(theta);
      }
      sig=sig2;
      //return( sig*Z );
   }

   //double s         = 2.0*me*(me+fBeamEnergy);
   //double gamcm     = (fBeamEnergy+me)/Sqrt(s);
   //double th_cm     = 2.0*ATan( Tan(theta)*gamcm );
   //double Eprime_cm = Sqrt(s/2.0);

   //double sig = MollerCrossSection_CM( Eprime_cm, th_cm );

   //double dOmega_cm_over_lab= (Sin(th_cm)/Sin(theta))*2.0*gamcm/((1.0+gamcm*gamcm*Tan(theta)*Tan(theta))*Cos(theta)*Cos(theta));
   //sig *= dOmega_cm_over_lab;

   //if(sig<0) {
   //   return 0.0;
   //}
   
   sig = std::max(sig1, sig2);
   //double Z   = fTargetNucleus.GetZ();
   double res = sig*Z;
   //if( IncludeJacobian() ){
   //   //std::cout <<  sinth*res << " nb\n";
   //   return( sinth*res);
   //}
   return(res);
}
//______________________________________________________________________________

double InclusiveMollerDiffXSec::CosTheta2(double Ebeam, double E2)
{
   using namespace TMath;
   using namespace insane::units;
   double me     = M_e/GeV;
   double res = 2.0*me*me;
   res += 2.0*me*(Ebeam-E2);
   res += -2.0*Ebeam*E2;
   res *= -1.0/(2.0*Sqrt(Ebeam*Ebeam - me*me)*Sqrt(E2*E2 - me*me));
   return res;
}
//______________________________________________________________________________


}}
