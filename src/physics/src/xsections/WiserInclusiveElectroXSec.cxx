#include "insane/xsections/WiserInclusiveElectroXSec.h"


namespace insane {
namespace physics {

//______________________________________________________________________________
WiserInclusiveElectroXSec::WiserInclusiveElectroXSec()
{
   fID         = 100003002;
}
//______________________________________________________________________________
WiserInclusiveElectroXSec::~WiserInclusiveElectroXSec()
{ }
//______________________________________________________________________________
Double_t WiserInclusiveElectroXSec::EvaluateXSec(const Double_t * x) const {

   //if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);
   double RES            = 0.0;
   //if( (fWiserParticleCode<1) || (fWiserParticleCode>6) ) fWiserParticleCode = 1;
   auto    PART           = (int)fWiserParticleCode;
   //std::cout << "PART = " << PART << std::endl;
   //std::cout << "fWiserParticleCode = " << fWiserParticleCode << std::endl;
   //std::cout << "pdg = " << fParticle->PdgCode() << std::endl;
   //std::cout << "pdg2 = " << (-1*fParticle->PdgCode()) << std::endl;
   //std::cout << "newpart = " << GetWiserType(-1*fParticle->PdgCode()) << std::endl;

   if( GetTargetNucleus() == Nucleus::Neutron() ) {
      // if target is a neutron use isospin to get the result
      if(fParticle->PdgCode() != 111)  // make sure it is not set to pi0
         PART = GetWiserType(-1*fParticle->PdgCode());
      //std::cout << "Neutron" << std::endl;
   }
   else if( !(GetTargetNucleus() == Nucleus::Proton()) ) {
      std::cout << " NOT Proton or neutron" << std::endl;
   }
   //else std::cout << "Proton" << std::endl;
   //std::cout << "PART = " << PART << std::endl;
   double EBEAM          = GetBeamEnergy();
   double Epart          = x[0];
   double Ppart          = TMath::Sqrt(Epart*Epart - M_pion*M_pion/(GeV*GeV));
   double THETA          = x[1];
   double radlen         = fRadiationLength;

   // Solution to Tiator-wright Eqn.5 with theta_e=0
   double num = TMath::Power(Ppart,2.0) + TMath::Power(M_p/GeV,2.0) - TMath::Power(M_p/GeV - Epart, 2.0);
   double denom = 2.0*(Ppart*TMath::Cos(THETA) + M_p/GeV - Epart);

   double AMT = M_p/MeV;//is target mass?
   double AM1 = M_pion/MeV;//is produced particle mass (MeV/c2)
   double EI  = EBEAM*1000.0;//is the electron beam energy (MeV)
   double W0  = (num/denom)*1000.0;//is the theta=0 photon energy (MeV)
   double TP  = (Epart - M_pion/GeV)*1000.0;//is the kinetic energy of the produced particle (MeV)
   double TH  = THETA;//is the angle of the produced particle (radians)
   double GN  = 0.0;//the result of Ne*R/w0 of eqn.12 in Nuc.Phys.A379

   if( W0<0.0 || W0>EI ) {
      GN=0.0;
   } else {
      vtp_(&AMT,&AM1,&EI,&W0,&TP,&TH,&GN);
      GN *= 1000.0; // because GN has units 1/MeV
   }

   //std::cout << "W0 = " << W0 << std::endl;
   //std::cout << "GN = " << GN << std::endl;
   //wiser_all_sig_(&RES,&PART,&EBEAM,&PscatteredPart,&THETA);
   double tot = 0.0;
   if(PART==0){
      // pi0 = (pi+ + pi-)/2.0
      PART=1;
      wiser_fit_(&PART, &Ppart, &THETA, &EBEAM, &RES);
      tot += RES;
      PART=2;
      wiser_fit_(&PART, &Ppart, &THETA, &EBEAM, &RES);
      tot += RES;
      tot = tot/2.0;
   }else {
      wiser_fit_(&PART, &Ppart, &THETA, &EBEAM, &RES);
      tot += RES;
   }
   // RES is Ex*dsig/dp^3 with units of GeV-ub/(GeV/c)^2
   if(tot<0.0) tot = 0.0;
   tot *= (Ppart); // Takes dp^3 to dEdOmega 
   tot *= GN*1000; // conv
   if( TMath::IsNaN(tot) ) tot = 0.0;
   //std::cout << " wiser result is " << RES << " nb/GeV*str "
   //<< " for p,theta " << PscatteredPart << "," << THETA << "\n";
   if ( IncludeJacobian() ) return(tot * TMath::Sin(x[1]) ); 
   return(tot); // converts nb to mb
}
//______________________________________________________________________________





//_____________________________________________________________________________

ElectroWiserDiffXSec::ElectroWiserDiffXSec()
{
   fID = 100103002;
   fTitle = "ElectroWiser";
   fPlotTitle = "#frac{d#sigma}{dE_{#pi} d#Omega_{#pi}} nb/GeV-Sr";
   fPIDs.clear();
   fPIDs.push_back(211);//
   fProtonXSec  = new WiserInclusiveElectroXSec();
   fProtonXSec->SetTargetNucleus(Nucleus::Proton());
   fNeutronXSec = new WiserInclusiveElectroXSec();
   fNeutronXSec->SetTargetNucleus(Nucleus::Neutron());
   fProtonXSec->UsePhaseSpace(false);
   fNeutronXSec->UsePhaseSpace(false);
   //std::cout << "done" << std::endl;
}
//_____________________________________________________________________________

ElectroWiserDiffXSec::~ElectroWiserDiffXSec()
{ }
//______________________________________________________________________________

Double_t  ElectroWiserDiffXSec::EvaluateXSec(const Double_t * x) const{

   //std::cout << " composite eval " << std::endl;
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   if( !fProtonXSec )  return 0.0;
   if( !fNeutronXSec ) return 0.0;

   Double_t z    = GetZ();
   //if( z>0.0 ) z = 1.0;
   Double_t n    = GetN();
   //if( n>0.0 ) n = 1.0;
   Double_t a    = GetA();

   Double_t sigP = fProtonXSec->EvaluateXSec(x);
   Double_t sigN = fNeutronXSec->EvaluateXSec(x);
   Double_t xsec = z*sigP + n*sigN;

   // Take into account nuclear transparency sigma_A = A^alpha sigma_p
   xsec = (xsec/a)*TMath::Power(a,0.8);
   //std::cout << " Z  = " << GetZ() << std::endl;
   //std::cout << "sp  = " << sigP << std::endl;
   //std::cout << " N  = " << GetN() << std::endl;
   //std::cout << "sn  = " << sigN << std::endl;

   //std::cout << " composite eval " <<  xsec << std::endl;
   if( IncludeJacobian() ) return xsec*TMath::Sin(x[1]);
   return xsec;
}
//______________________________________________________________________________
}}
