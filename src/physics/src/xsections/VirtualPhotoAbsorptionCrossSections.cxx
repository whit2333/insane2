#include "insane/xsections/VirtualPhotoAbsorptionCrossSections.h"

namespace insane {
  namespace physics {

    VirtualPhotoAbsorptionCrossSections::VirtualPhotoAbsorptionCrossSections(const char * n, const char * t):
      TNamed(n,t)
    { }
    //______________________________________________________________________________
    
    VirtualPhotoAbsorptionCrossSections::~VirtualPhotoAbsorptionCrossSections()
    { }
    //______________________________________________________________________________

  }
}
