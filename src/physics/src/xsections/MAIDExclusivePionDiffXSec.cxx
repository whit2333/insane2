#include "insane/xsections/MAIDExclusivePionDiffXSec.h"
#include <fstream>

namespace insane {
namespace physics {
//______________________________________________________________________________
MAIDExclusivePionDiffXSec::MAIDExclusivePionDiffXSec(const char * pion,const char * nucleon)
{
   fID         = 1020;
   fPion    = pion; 
   fNucleon = nucleon; 
   frxn     = Form("%s_%s",fPion.Data(),fNucleon.Data());

   SetTitle("MAIDExclusiveDiffXSec");
   SetPlotTitle("MAID #vec{p}(#vec{e},e'#pi)p");
   fLabel      = " #frac{d#sigma}{dE'd#Omega#Omega_{#pi}} ";
   fUnits      = "nb/GeV/sr^{2}";

   fOmega_pi_cm_over_lab = 1.0;
   fOmega_pi_cm = 0.0;
   fTheta_pi_cm = 0.0;
   fPhi_pi_cm   = 0.0;
   fM_1         = M_p/GeV;
   fM_2         = M_p/GeV;
   fM_pi        = M_pion/GeV;

   fTheta_pi_q_lab = 0.0;

   fnDim = 5;
   fPIDs.clear();
   fnParticles = 3;
   fPIDs.push_back(11);
   fPIDs.push_back(2212);
   fPIDs.push_back(111);

   for(int i=0;i<5;i++){
      y_lim.push_back( std::vector<double>() );
      yi_lim.push_back( std::vector<Int_t>() );
      xd.push_back(0.0);
      x_eval[i] = 0.5;
      yi_lim[i].resize(2);
      y_lim[i].push_back(0.0); 
      y_lim[i].push_back(1.0); 
   }

   // work around
   c_5 = &(c_5_4[0]);
   //c_5.resize(5); 
   c_4.resize(4); 
   c_3.resize(3); 
   c_2.resize(2); 
   c_1.resize(1); 
   for(int i=0;i<2;i++){
      c_5[i].resize(2); 
      c_4[i].resize(2); 
      c_3[i].resize(2); 
      c_2[i].resize(2); 
      c_1.push_back(0.0);
      for(int j=0;j<2;j++){
         c_5[i][j].resize(2); 
         c_4[i][j].resize(2); 
         c_3[i][j].resize(2); 
         c_2[i].push_back(0.0);
         for(int k=0;k<2;k++){
            c_5[i][j][k].resize(2); 
            c_4[i][j][k].resize(2); 
            c_3[i][j].push_back(0.0);
            for(int l=0;l<2;l++){
               c_4[i][j][k].push_back(0.0);
               c_5[i][j][k][l].resize(2);
               for(int m=0;m<2;m++){
                  c_5[i][j][k][l].push_back(0.0);
               }
            }
         }
      }
   }

   TString grid_path = std::getenv("InSANE_PDF_GRID_DIR");
   fprefix     = grid_path + Form("/maid2007/"); // input directory name 
   fDebug      = false;                          // debug flag 
   //fPT.SetMagThetaPhi(0.0,180*degree,0.0);
   fPx         = 0.;                             // target polarization parallel to virtual photon momentum 
   fPz         = 0.;                             // target polarization perpendicular to virtual photon momentum 
   SetVirtualPhotonFluxConvention(2);            // default is the Hand convention
   // SetCrossSectionType(1);                        // cross section type 
   // SetUnits(1);                                   // default is nb, GeV  
   
   ImportData();
}

//______________________________________________________________________________
MAIDExclusivePionDiffXSec::~MAIDExclusivePionDiffXSec(){
   
}

//____________________________________________________________________
void MAIDExclusivePionDiffXSec::InitializePhaseSpaceVariables() {
   //std::cout << " o InSANEInclusiveDiffXSec::InitializePhaseSpaceVariables() \n";
   PhaseSpace * ps = GetPhaseSpace();
      //------------------------
      auto * varEnergy = new PhaseSpaceVariable();
      varEnergy = new PhaseSpaceVariable();
      varEnergy->SetNameTitle("energy_e", "E_{e'}");
      varEnergy->SetMinimum(0.001); //GeV
      varEnergy->SetMaximum(5.0); //GeV
      varEnergy->SetParticleIndex(0);
      ps->AddVariable(varEnergy);

      auto *   varTheta = new PhaseSpaceVariable();
      varTheta->SetNameTitle("theta_e", "#theta_{e'}"); // ROOT string latex
      varTheta->SetMinimum(0.0*degree ); //
      varTheta->SetMaximum(180.0*degree ); //
      varTheta->SetParticleIndex(0);
      ps->AddVariable(varTheta);

      auto *   varPhi = new PhaseSpaceVariable();
      varPhi->SetNameTitle("phi_e", "#phi_{e'}"); // ROOT string latex
      varPhi->SetMinimum(-360.0*degree ); //
      varPhi->SetMaximum( 360.0*degree ); //
      varPhi->SetParticleIndex(0);
      ps->AddVariable(varPhi);

      //------------------------
      auto * varEnergyP = new PhaseSpaceVariable();
      varEnergyP = new PhaseSpaceVariable();
      varEnergyP->SetNameTitle("energy_p", "E_{p}");
      varEnergyP->SetMinimum(0.0); //GeV
      varEnergyP->SetMaximum(5.90); //GeV
      varEnergyP->SetParticleIndex(1);
      varEnergyP->SetDependent(true);
      ps->AddVariable(varEnergyP);

      auto *   varThetaP = new PhaseSpaceVariable();
      varThetaP->SetNameTitle("theta_p", "#theta_{p}"); // ROOT string latex
      varThetaP->SetMinimum(0.0  *degree ); //
      varThetaP->SetMaximum(180.0*degree ); //
      varThetaP->SetDependent(true);
      varThetaP->SetParticleIndex(1);
      ps->AddVariable(varThetaP);

      auto *   varPhiP = new PhaseSpaceVariable();
      varPhiP->SetNameTitle("phi_p", "#phi_{p}"); // ROOT string latex
      varPhiP->SetMinimum(-360.0*degree ); //
      varPhiP->SetMaximum( 360.0*degree ); //
      varPhiP->SetDependent(true);
      varPhiP->SetParticleIndex(1);
      ps->AddVariable(varPhiP);

      //------------------------
      auto * varEnergyPi = new PhaseSpaceVariable();
      varEnergyPi = new PhaseSpaceVariable();
      varEnergyPi->SetNameTitle("energy_pi", "E_{pi}");
      varEnergyPi->SetMinimum(0.0); //GeV
      varEnergyPi->SetMaximum(5.0); //GeV
      varEnergyPi->SetParticleIndex(2);
      varEnergyPi->SetDependent(true);
      ps->AddVariable(varEnergyPi);

      auto *   varThetaPi = new PhaseSpaceVariable();
      varThetaPi->SetNameTitle("theta_pi", "#theta_{pi}"); // ROOT string latex
      varThetaPi->SetMinimum(0.0  *degree ); //
      varThetaPi->SetMaximum(180.0*degree ); //
      varThetaPi->SetParticleIndex(2);
      ps->AddVariable(varThetaPi);

      auto *   varPhiPi = new PhaseSpaceVariable();
      varPhiPi->SetNameTitle("phi_pi", "#phi_{pi}"); // ROOT string latex
      varPhiPi->SetMinimum(-360.0*degree ); //
      varPhiPi->SetMaximum( 360.0*degree ); //
      varPhiPi->SetParticleIndex(2);
      ps->AddVariable(varPhiPi);

      //------------------------
      SetPhaseSpace(ps);
      //ps->Print();
}

//______________________________________________________________________________
TVector3 MAIDExclusivePionDiffXSec::GetTargetPolInCM(Double_t eps,Double_t Q2, Double_t W2, Double_t phi) const {
  
   if(eps == 1.0) eps = 0.9999;
   Double_t E0     = insane::Kine::BeamEnergy_epsilonQ2W2(eps,Q2,W2);
   Double_t Eprime = insane::Kine::Eprime_epsilonQ2W2(eps,Q2,W2);
   Double_t theta  = insane::Kine::Theta_epsilonQ2W2(eps,Q2,W2);
   Double_t nu     = E0 - Eprime;

   if(E0==0.0)     E0     = 0.000002;
   if(Eprime==0.0) Eprime = 0.000001;
   if(theta==0.0)  theta  = 0.000001;
   //if( TMath::IsNaN(E0)){
   //   std::cout << " E0  = " << E0 << std::endl;
   //   std::cout << " Ep  = " << Eprime << std::endl;
   //   std::cout << " th  = " << theta << std::endl;
   //   std::cout << " eps = " << eps << std::endl;
   //   std::cout << " Q2  = " << Q2 << std::endl;
   //   std::cout << " W2  = " << W2 << std::endl;
   //   fk1_CM.Print();
   //   fk2_CM.Print();
   //   fq_CM.Print();
   //}
   if( phi<0.0 ) phi = phi + 2.0*pi;

   // Set vectors in the lab frame
   fk1_CM.SetXYZ(0.0,0.0,E0);
   fk2_CM.SetMagThetaPhi(Eprime,theta,phi);
   fq_CM  = fk1_CM - fk2_CM;
   fPT_CM = fTargetPol;
   Double_t pt_mag = fTargetPol.Mag();

   //std::cout << " check boost of q in lab vs cm " << std::endl;
   //TVector3 qTest = (fk1_CM-fk2_CM);
   //fq_CM.Print();
   //qTest.Print();


   //// Rotation to q along z 
   fRotateToq.SetToIdentity();
   fY_axis =  fk1_CM.Cross(fk2_CM);
   fY_axis.SetMag(1.0);
   fZ_axis = fq_CM;
   fZ_axis.SetMag(1.0);
   fX_axis = fY_axis.Cross(fZ_axis);
   fX_axis.SetMag(1.0);
   //std::cout << " X axis:"; fX_axis.Print();
   //std::cout << " Y axis:"; fY_axis.Print();
   //std::cout << " Z axis:"; fZ_axis.Print();
   fRotateToq.RotateAxes(fX_axis,fY_axis,fZ_axis);
   fRotateToq.Invert();
   //fRotateToq.Dump();

   //Double_t angle;
   //TVector3 axis;

   // ----------------------
   //// New rotation technique
   //fRotateToq.SetToIdentity();
   //fZ_axis.SetXYZ( 0.0, 0.0, 1.0);
   //fY_axis.SetXYZ( 0.0, 1.0, 0.0);
   //fX_axis     =  fZ_axis.Cross(fq_CM); // not actually the x axis
   //fYScat_axis =  fk1_CM.Cross(fk2_CM);
   // Here we take q in the frame we want it in, z along q and y normal to scattering plane. 
   // Then we do the rotations to bring it back to the lab frame (boosted to CM)
   // Rotate around q to bring x and y to "up and down"  
   // Then we rotate q
   //fRotateToq.Rotate(fY_axis.Angle(fYScat_axis),fZ_axis);
   //fRotateToq.Rotate(fZ_axis.Angle(fq_CM),fX_axis);
   //fRotateToq.Invert();
   //rot.Dump();
   //std::cout << "angle  = " << angle << std::endl;
   //std::cout << "axis   = " ; rotAxis.Print();

   //fRotateToq.AngleAxis(angle,axis); 
   //std::cout << "angle = " << angle << std::endl;
   //std::cout << "axis = " ; axis.Print();

   //std::cout << " Pt before : " ;fPT_CM.Print();
   fPT_CM.Transform(fRotateToq);
   fq_CM.Transform(fRotateToq);
   fk1_CM.Transform(fRotateToq);
   fk2_CM.Transform(fRotateToq);

   //std::cout << " check to see if q rotates to z axis " << std::endl;
   //fq_CM.Print();

   // Set the 4 vectors for boosting
   f4VecPT_CM.SetVect(fPT_CM);
   f4VecPT_CM.SetT(1.0);
   f4Veck1_CM.SetVect(fk1_CM);
   f4Veck1_CM.SetT(fk1_CM.Mag());
   f4Veck2_CM.SetVect(fk2_CM);
   f4Veck2_CM.SetT(fk2_CM.Mag());
   f4Vecq_CM.SetVect(fq_CM);
   f4Vecq_CM.SetT(nu);

   // Boost 4 vectors to the piN CM
   fBoostToCM         = fq_CM;
   fBoostToCM        *= (-1.0/(nu + (M_p/GeV)));
   f4VecPT_CM.Boost(fBoostToCM);
   f4Veck1_CM.Boost(fBoostToCM);
   f4Veck2_CM.Boost(fBoostToCM);
   f4Vecq_CM.Boost(fBoostToCM);

   // Get the 3 vectors in CM
   fk1_CM = f4Veck1_CM.Vect();
   fk2_CM = f4Veck2_CM.Vect();
   fq_CM  = f4Vecq_CM.Vect();
   fPT_CM = f4VecPT_CM.Vect();

   //std::cout << " Pt after  : " ;fPT_CM.Print();
   fPT_CM.SetMag(pt_mag);

   if( TMath::Abs(fq_CM.X()) > 1.0e-10) 
      Error("GetTargetPolInCM","X component of q not zero!");
   if( TMath::Abs(fq_CM.Y()) > 1.0e-10) 
      Error("GetTargetPolInCM","Y component of q not zero!");

   //std::cout << " x = " << TMath::Abs(fq_CM.X()) << std::endl;
   //std::cout << " check to see if q rotates to z axis " << std::endl;
   //fq_CM.Print();

   // Get boost to CM frame
   //boostToCM.Print();
   //TLorentzVector Pt_cm(Pt,Pt.Mag());
   //Pt_cm.Boost(boostToCM);
   //if( (Pt_cm.Vect().Mag() > 1.0 ) ||  TMath::IsNaN(Pt_cm.Vect().Mag()) ) {
   //  //std::cout << "Pt is NAN" << std::endl;
   //  k1.Print();
   //  k2.Print();
   //  q.Print();
   //  Pt_cm.Print();
   //}

   return(fPT_CM);
}

//______________________________________________________________________________
Double_t * MAIDExclusivePionDiffXSec::GetDependentVariables(const Double_t * x) const {
   //std::cout << "MAIDExclusivePionDiffXSec::GetDependentVariables" << std::endl;
  
   // Variables evaluated in the lab frame.
   Double_t    E0       = GetBeamEnergy();
   Double_t    Eprime   = x[0];
   Double_t    theta    = x[1];
   Double_t    phi      = x[2];
   Double_t    theta_pi = x[3];
   Double_t    phi_pi   = x[4];
   if( phi<0.0 ) phi = phi + 2.0*pi;

   Double_t    Q2       = 4.0*Eprime*E0*TMath::Power(TMath::Sin(theta/2.0),2.0);
   Double_t    W        = insane::Kine::W_EEprimeTheta(E0,Eprime,theta);
   Double_t    nu       = E0 - Eprime;
   Double_t    absq_lab = TMath::Sqrt(Q2+nu*nu);

   fOmega_th_lab        = (TMath::Power(fM_2+fM_pi,2.0) - fM_1*fM_1 + Q2)/(2.0*fM_1);
   //std::cout << " nu = " << nu << std::endl;
   //std::cout << " E0 = " << E0 << std::endl;
   //std::cout << " Ep = " << Eprime << std::endl;

   // ---------------------------------------
   // Momentum vectors
   fk1_CM.SetMagThetaPhi(E0,0.0,0.0);
   fk2_CM.SetMagThetaPhi(Eprime,theta,phi);
   fq_CM = fk1_CM - fk2_CM;
   fP1_CM.SetXYZ(0,0,0);
   fP2_CM.SetXYZ(0,0,0);
   fkpi_CM.SetMagThetaPhi(1.0,theta_pi,phi_pi); // just a unit vector in the right direction

   // Rotation Matrix from lab to scattering plane coords
   labToScat.SetToIdentity();
   scatToLab.SetToIdentity();

   fY_axis =  fk1_CM.Cross(fk2_CM);
   fY_axis.SetMag(1.0);
   fZ_axis = fq_CM;
   fZ_axis.SetMag(1.0);
   fX_axis = fY_axis.Cross(fZ_axis);
   fX_axis.SetMag(1.0);
   // Create rotation matricies
   scatToLab.RotateAxes(fX_axis,fY_axis,fZ_axis);
   //labToScat.RotateAxes(fX_axis,fY_axis,fZ_axis);
   labToScat = scatToLab.Inverse();

   // Rotate 3 vectors to scattering plane coords
   fPT_scat = fTargetPol;
   fPT_scat.Transform( labToScat);
   fq_CM.Transform(    labToScat);
   fkpi_CM.Transform(  labToScat);
   fP1_CM.Transform(   labToScat);
   fk1_CM.Transform(   labToScat);
   fk2_CM.Transform(   labToScat);

   // Get boost to CM frame
   //Double_t Ecm    = TMath::Sqrt(fM_1*fM_1-Q2+2.0*nu*fM_1) ;
   //Double_t qcm    = fq_CM.Mag()*(fM_1)/Ecm;
   betaToCM        = fq_CM;
   betaToCM       *= (-1.0/(nu + fM_1));
   betaToLAB       = -1.0*betaToCM ;

   // Set the 4 vectors for boosting
   f4VecPT_CM.SetVect(fPT_scat);
   f4VecPT_CM.SetT(0.0);

   f4Vecq_CM.SetVect(fq_CM);
   f4Vecq_CM.SetT(nu);

   f4Veckpi_CM.SetVect(fkpi_CM);
   f4Veckpi_CM.SetT(1.0);// k is just a unit vector here

   f4VecP1_CM.SetVect(fP1_CM);
   f4VecP1_CM.SetE(fM_1);

   f4Veck1_CM.SetVect(fk1_CM);
   f4Veck1_CM.SetT(fk1_CM.Mag());

   f4Veck2_CM.SetVect(fk2_CM);
   f4Veck2_CM.SetT(fk2_CM.Mag());

   // boost  vectors in scat plane coords to pi-N CM system
   f4Vecq_CM.Boost(   betaToCM);
   f4Veckpi_CM.Boost( betaToCM);
   f4VecP1_CM.Boost(  betaToCM);
   f4Veck1_CM.Boost(  betaToCM);
   f4Veck2_CM.Boost(  betaToCM);
   // note we don't bother with k1 and k2

   // Center of mass values
   Double_t qcm      = f4Vecq_CM.Vect().Mag();
   Double_t nu_cm    = f4Vecq_CM.E();
   Double_t E1_cm    = f4VecP1_CM.E();
   Double_t qvec2_cm = f4Vecq_CM.Vect().Mag2();

   // Solution for pion energy and final state
   fOmega_pi_cm      = (E1_cm*E1_cm - fM_2*fM_2 + fM_pi*fM_pi + 2.0*E1_cm*nu_cm + nu_cm*nu_cm )/(2.0*(nu_cm + E1_cm));

   fkpi_CM = f4Veckpi_CM.Vect();
   fkpi_CM.SetMag( TMath::Sqrt(fOmega_pi_cm*fOmega_pi_cm - fM_pi*fM_pi) );
   f4Veckpi_CM.SetVect(fkpi_CM);
   f4Veckpi_CM.SetE(fOmega_pi_cm);

   fTheta_pi_cm      = fkpi_CM.Theta();
   fPhi_pi_cm        = fkpi_CM.Phi();
   if(fPhi_pi_cm < 0) fPhi_pi_cm += 2.0*pi;

   fP2_CM  = -1.0*fkpi_CM;
   f4VecP2_CM.SetVect(fP2_CM);
   f4VecP2_CM.SetE(TMath::Sqrt(fP2_CM.Mag2() + fM_2*fM_2));

   // Set the CM 3 vectors
   fq_CM   = f4Vecq_CM.Vect();
   //fkpi_CM = f4Veckpi_CM.Vect();
   fP1_CM  = f4VecP1_CM.Vect();
   //fP2_CM  = f4VecP2_CM.Vect();
   fk1_CM  = f4Veck1_CM.Vect();
   fk2_CM  = f4Veck2_CM.Vect();

   // prepare new lab vectors
   //f4Veck1_LAB  = f4Veck1_CM ;
   //f4Veck2_LAB  = f4Veck2_CM ;
   f4Vecq_LAB   = f4Vecq_CM  ;
   f4VecP1_LAB  = f4VecP1_CM ;
   f4VecP2_LAB  = f4VecP2_CM ;
   f4Veck1_LAB  = f4Veck1_CM ;
   f4Veck2_LAB  = f4Veck2_CM ;
   f4Veckpi_LAB = f4Veckpi_CM;
   //std::cout << " CM RESULTS " << std::endl;
   //TLorentzVector test1 = f4Vecq_LAB;
   //test1 += f4VecP1_LAB;
   //TLorentzVector test2 = f4Veckpi_LAB;
   //test2 += f4VecP2_LAB;
   //std::cout << " initial 4 momentum = "; test1.Print();
   //std::cout << " final   4 momentum = "; test2.Print();
   //f4Vecq_LAB.Print();
   //f4Veckpi_LAB.Print();
   //f4VecP1_LAB.Print();
   //f4VecP2_LAB.Print();

   // boost back to lab frame in scatting coords 
   //f4Veck1_LAB.Boost(betaToLAB);
   //f4Veck2_LAB.Boost(betaToLAB);
   f4Vecq_LAB.Boost(betaToLAB);
   f4VecP1_LAB.Boost(betaToLAB);
   f4VecP2_LAB.Boost(betaToLAB);
   f4Veck1_LAB.Boost(betaToLAB);
   f4Veck2_LAB.Boost(betaToLAB);
   f4Veckpi_LAB.Boost(betaToLAB);

   //std::cout << fOmega_pi_lab << std::endl;
   fOmega_pi_lab = f4Veckpi_CM.E();

   // set the 3 vectors lab frame 
   //fk1_LAB  = f4Veck1_LAB.Vect();
   //fk2_LAB  = f4Veck2_LAB.Vect();
   fq_LAB   = f4Vecq_LAB.Vect();
   fP1_LAB  = f4VecP1_LAB.Vect();
   fP2_LAB  = f4VecP2_LAB.Vect();
   fkpi_LAB = f4Veckpi_LAB.Vect();
   fk1_LAB  = f4Veck1_LAB.Vect();
   fk2_LAB  = f4Veck2_LAB.Vect();
   //f4Veck1_LAB  = f4Veck1_CM ;
   //f4Veck2_LAB  = f4Veck2_CM ;

   //f4Veck1_LAB.Print();
   //f4Veck2_LAB.Print();
   
   // Jacobian to change the virtual photon xsec to lab coords (see Appendix A)
   fOmega_pi_cm_over_lab = (fkpi_LAB.Mag2()*W)/(qcm*( TMath::Abs((fM_1+nu)*(fkpi_LAB.Mag())- fOmega_pi_lab*absq_lab*TMath::Cos(fkpi_LAB.Theta())) ));



   // rotate back to lab coords
   fq_LAB.Transform(scatToLab);
   fP1_LAB.Transform(scatToLab);
   fP2_LAB.Transform(scatToLab);
   fkpi_LAB.Transform(scatToLab);
   fk1_LAB.Transform(scatToLab);
   fk2_LAB.Transform(scatToLab);
   //fk1_LAB  = fk1_CM ;
   //fk2_LAB  = fk2_CM ;

   // Set the lab 4 vectors to the rotated angle
   f4Vecq_LAB.SetVect(fq_LAB) ;
   f4VecP1_LAB.SetVect(fP1_LAB);
   f4VecP2_LAB.SetVect(fP2_LAB);
   f4Veckpi_LAB.SetVect(fkpi_LAB );
   f4Veck1_LAB.SetVect(fk1_LAB);
   f4Veck2_LAB.SetVect(fk2_LAB);

   // ---------------------------------------
   //f4Veck1_LAB.Print();
   //f4Veck2_LAB.Print();
   //f4Vecq_LAB.Print();
   //f4Veckpi_LAB.Print();
   //f4VecP1_LAB.Print();
   //f4VecP2_LAB.Print();

   //TLorentzVector test0 = f4Veck1_LAB;
   //test0 += f4VecP1_LAB;
   //TLorentzVector test1 = f4Veck2_LAB;
   //test1 += f4VecP2_LAB;
   //test1 += f4Veckpi_LAB;
   //TLorentzVector test2 = f4Vecq_LAB;
   //test2 += f4VecP1_LAB;
   //TLorentzVector test3 = f4Veckpi_LAB;
   //test3 += f4VecP2_LAB;

   //std::cout << " total-0 = "; test0.Print();
   //std::cout << " total-1 = "; test1.Print();
   //std::cout << " total-2 = "; test2.Print();
   //std::cout << " total-3 = "; test3.Print();

   // This is important as it is used by 
   fTheta_pi_q_lab        = fq_LAB.Angle(fkpi_LAB);

   fDependentVariables[0] = Eprime;
   fDependentVariables[1] = theta;
   fDependentVariables[2] = phi;

   fDependentVariables[3] = f4VecP2_LAB.E();
   fDependentVariables[4] = fP2_LAB.Theta();
   fDependentVariables[5] = fP2_LAB.Phi();

   fDependentVariables[6] = f4Veckpi_LAB.E();
   fDependentVariables[7] = fkpi_LAB.Theta();
   fDependentVariables[8] = fkpi_LAB.Phi();

   //for(int i = 0;i<9;i++){
   //   std::cout << "var[" << i << "] = " << fDependentVariables[i] <<std::endl;
   //}
   return(fDependentVariables);
}

//______________________________________________________________________________
void MAIDExclusivePionDiffXSec::SetVirtualPhotonFluxConvention(Int_t i){
   fConvention = i;
   switch(fConvention){
      case 0: 
         fConventionName = "A";  // axial? 
         break;
      case 1: 
         fConventionName = "Gilman"; 
         break;
      case 2: 
         fConventionName = "Hand";
         break;
      default:
         std::cout << "[MAIDExclusivePionDiffXSec::EvaluateVirtualPhotonFlux]: Invalid convention!  Exiting..." << std::endl;
         exit(1);
   }
}

//______________________________________________________________________________
void MAIDExclusivePionDiffXSec::SetReactionChannel(TString pion,TString nucleon){
   fPion    = pion; 
   fNucleon = nucleon; 
   frxn     = Form("%s_%s",pion.Data(),nucleon.Data());
}

//______________________________________________________________________________
Double_t MAIDExclusivePionDiffXSec::GetVirtualPhotonCrossSection(MAIDPolarizedKinematicKey* key, Double_t phi_e) const{

   fP_t_cm = GetTargetPolInCM(  key->fepsilon, key->fQ2, key->fW*key->fW/(1000.0*1000.0), phi_e);
   // For some extreme cases A_1 can be bigger than 100% ... It is just set to 100%...
   if( TMath::Abs(key->fA_1) > 1.0 ) key->fA_1 = key->fA_1/(TMath::Abs(key->fA_1) + 0.00001) ;
   fA_t.SetXYZ(   key->fA_1,  key->fA_2,   key->fA_3);
   fA_et.SetXYZ(  key->fA_e1, key->fA_e2,  key->fA_e3);
   Double_t res = 0.0;
   res = (key->fSigma_0)*( 1.0 + fHelicity*key->fA_e + (fP_t_cm*fA_t) + fHelicity*(fP_t_cm*fA_et) );
   //key->Dump();
   if( (res<0.0) ||  TMath::IsNaN(res) ){
      key->Dump();
      fP_t_cm.Print();
      fA_t.Print();
      fA_et.Print();
      std::cout << " (p_t*A_t)  = " << (fP_t_cm*fA_t)  << std::endl;
      std::cout << " (p_t*A_et) = " << (fP_t_cm*fA_et) << std::endl;
      std::cout << " fHelicity = " << fHelicity << std::endl;
      std::cout << " A_e = " << key->fA_e << std::endl;
      std::cout << " sigma_0= " << key->fSigma_0 << std::endl;
      std::cout << " sigma_v = " << res << std::endl;
      res = 0.0;
   }
   return(res);
}

//______________________________________________________________________________
void     MAIDExclusivePionDiffXSec::FillHyperCubeValues(Double_t phi_e) const {
   MAIDPolarizedKinematicKey *aKey =nullptr;
   for(int i=0;i<5;i++){
      //std::cout << "y_lim[" << i << 0 << "] = " << y_lim[i][0] << std::endl; 
      //std::cout << "y_lim[" << i << 1 << "] = " << y_lim[i][1] << std::endl; 
      xd[i] = (x_eval[i]-y_lim[i][0])/(y_lim[i][1]-y_lim[i][0]);
   }

   for(int i=0;i<2;i++){
      for(int j=0;j<2;j++){
         for(int k=0;k<2;k++){
            for(int l=0;l<2;l++){
               for(int m=0;m<2;m++){
                  // grid point
                  fKineKey->fepsilon = y_lim[0][i];   
                  fKineKey->fQ2      = y_lim[1][j]; 
                  fKineKey->fW       = y_lim[2][k]; 
                  fKineKey->fTheta   = y_lim[3][l];   
                  fKineKey->fPhi     = y_lim[4][m];   
                  //std::cout << "[" << i << j << k << l << m << "] " << std::endl;
                  //int       ikey     = fGridData.BinarySearch(fKineKey);
                  int       ikey     = GetGridIndex(yi_lim[0][i],
                                                    yi_lim[1][j],
                                                    yi_lim[2][k],
                                                    yi_lim[3][l],
                                                    yi_lim[4][m]);
                  double    sig      = 0.0; 
                  if(ikey != -1 ){
                     aKey = (MAIDPolarizedKinematicKey*)fGridData.At(ikey);
                     sig = GetVirtualPhotonCrossSection(aKey,phi_e);
                     //std::cout << " sig = " << sig << std::endl;
                  }
                  c_5[i][j][k][l][m] = sig;//double(m); // f(x[]) = m  
               }
            }
         }
      }
   }
}

//______________________________________________________________________________
Double_t MAIDExclusivePionDiffXSec::GetInterpolation() const {
   for(int i=0;i<2;i++){
      for(int j=0;j<2;j++){
         for(int k=0;k<2;k++){
            for(int l=0;l<2;l++){
               c_4[i][j][k][l] = c_5[i][j][k][l][0]*(1.0-xd[4]) + c_5[i][j][k][l][1]*xd[4] ;
               if(TMath::IsNaN(c_4[i][j][k][l])){
                  std::cout << "c_4[" << i << j << k << l << "] = " << c_4[i][j][k][l] << std::endl;
                  std::cout << "c_5[" << i << j << k << l << "0] = " << c_5[i][j][k][l][0] << std::endl;
                  std::cout << "c_5[" << i << j << k << l << "1] = " << c_5[i][j][k][l][1] << std::endl;
                  std::cout << "x_d[4]=" << xd[4] << std::endl;
               }
            }
         }
      }
   }
   for(int i=0;i<2;i++){
      for(int j=0;j<2;j++){
         for(int k=0;k<2;k++){
            c_3[i][j][k] = c_4[i][j][k][0]*(1.0-xd[3]) + c_4[i][j][k][1]*xd[3] ;
         }
      }
   }
   for(int i=0;i<2;i++){
      for(int j=0;j<2;j++){
         c_2[i][j] = c_3[i][j][0]*(1.0-xd[2]) + c_3[i][j][1]*xd[2] ;
      }
   }
   for(int i=0;i<2;i++){
      c_1[i] = c_2[i][0]*(1.0-xd[1]) + c_2[i][1]*xd[1] ;
   }
   c_0 = c_1[0]*(1.0-xd[0]) + c_1[1]*xd[0] ;
   //std::cout << "result is " << c_0 << std::endl;
   return(c_0);
}

//______________________________________________________________________________
Double_t MAIDExclusivePionDiffXSec::EvaluateXSec(const Double_t *par) const{
   if (!VariablesInPhaseSpace(GetPhaseSpace()->GetDimension(), par)) return(0.0);
   // compute needed variables in GeV and rad  
   // FIXME: what about theta and phi -- they should be for the pion.  
   //        setting to zero for now...   
   Double_t Es    = GetBeamEnergy(); 
   Double_t Ep    = par[0]; 
   Double_t th    = par[1];
   Double_t ph    = par[2];
   Double_t W     = insane::Kine::W_EEprimeTheta(Es,Ep,th); 
   Double_t Q2    = insane::Kine::Qsquared(Es,Ep,th); 
   Double_t eps   = insane::Kine::epsilon(Es,Ep,th); 
   Double_t omega_pi = par[6];
   Double_t th_pi = par[7]; 
   Double_t ph_pi = par[8]; 
   Double_t M_1   = M_p/GeV; // target nucleus
   Double_t M_2   = M_p/GeV; // recoil nucleon
   Double_t M_pi  = M_pion/GeV; // recoil pion
   Double_t omega_th = (TMath::Power(M_2+M_pi,2.0) - M_1*M_1 + Q2)/(2.0*M_1);

   if( ph_pi<0.0 ) ph_pi = ph_pi + 2.0*pi;
   if( ph<0.0 ) ph = ph + 2.0*pi;

   if(omega_th > omega_pi) return(0.0); 
   // convert W to MeV 
   Double_t CONV  = 1E+3; 
   W             *= CONV;  

   //std::cout << "W = " << W << std::endl;
   //std::cout << "Q2 = " << Q2 << std::endl;

   /// MAID model kinematic limits
   if( W  > 2000.0 ) return(0.0);
   //if( W  < 1073.2 ) return(0.0);
   if( W  < 1100.0 ) return(0.0);
   if( Q2 > 5.0    ) return(0.0);
   if( Q2 < 0.0    ) return(0.0);

   // Double_t Gamma = EvaluateFluxFactor(Es,Ep,th);  
   // interpolate from the grid (Bilinear Interpolation from Wikipedia) 
   fKineKey->fepsilon = eps;   
   fKineKey->fQ2      = Q2; 
   fKineKey->fW       = W; 
   fKineKey->fTheta   = fTheta_pi_cm/degree;   
   fKineKey->fPhi     = fPhi_pi_cm/degree;   

   //std::cout << "----------------------------------------------" << std::endl;
   //std::cout << "eps   = " << eps << std::endl;
   //std::cout << "Q2    = " << Q2 << std::endl;
   //std::cout << "W     = " << W << std::endl;
   //std::cout << "th    = " << th/degree << std::endl;
   //std::cout << "ph    = " << ph/degree << std::endl;
   //std::cout << "th_pi = " << th_pi/degree << std::endl;
   //std::cout << "ph_pi = " << ph_pi/degree << std::endl;
   //fKineKey->Dump();

   x_eval[0] = eps;
   x_eval[1] = Q2;
   x_eval[2] = W;
   x_eval[3] = fTheta_pi_cm/degree;
   x_eval[4] = fPhi_pi_cm/degree;

   // Get boundaries for each linear interpolation. 
   // Indices run from ai-->ei, where i = 1 (low), 2 (high) 
   Int_t ia1,ia2; 
   BinarySearch(&fepsilon,fKineKey->fepsilon,ia1,ia2);
   //std::cout << "ia1    = " << ia1 << std::endl;
   //std::cout << "ia2    = " << ia2 << std::endl;
   //std::cout << "y_lim.size() = " << y_lim.size() << std::endl;
   //std::cout << "y_lim[0].size() = " << y_lim[0].size() << std::endl;
   y_lim[0][0] = fepsilon.at(ia1);
   y_lim[0][1] = fepsilon.at(ia2);
   yi_lim[0][0] = ia1;
   yi_lim[0][1] = ia2;

   Int_t ib1,ib2; 
   BinarySearch(&fQ2,fKineKey->fQ2,ib1,ib2);
   y_lim[1][0] = fQ2.at(ib1);
   y_lim[1][1] = fQ2.at(ib2);
   yi_lim[1][0] = ib1;
   yi_lim[1][1] = ib2;
   //std::cout << "ib1    = " << ib1 << std::endl;
   //std::cout << "ib2    = " << ib2 << std::endl;

   Int_t ic1,ic2; 
   BinarySearch(&fW,fKineKey->fW,ic1,ic2);
   y_lim[2][0] = fW.at(ic1);
   y_lim[2][1] = fW.at(ic2);
   yi_lim[2][0] = ic1;
   yi_lim[2][1] = ic2;
   //std::cout << "ic1    = " << ic1 << std::endl;
   //std::cout << "ic2    = " << ic2 << std::endl;

   Int_t id1,id2; 
   BinarySearch(&fTheta,fKineKey->fTheta,id1,id2);
   y_lim[3][0] = fTheta.at(id1);
   y_lim[3][1] = fTheta.at(id2);
   yi_lim[3][0] = id1;
   yi_lim[3][1] = id2;
   //std::cout << "id1    = " << id1 << std::endl;
   //std::cout << "id2    = " << id2 << std::endl;

   Int_t ie1,ie2; 
   BinarySearch(&fPhi,fKineKey->fPhi,ie1,ie2);
   y_lim[4][0] = fPhi.at(ie1);
   y_lim[4][1] = fPhi.at(ie2);
   yi_lim[4][0] = ie1;
   yi_lim[4][1] = ie2;
   //std::cout << "ie1    = " << ie1 << std::endl;
   //std::cout << "ie2    = " << ie2 << std::endl;
   //int jkey     = GetGridIndex(yi_lim[0][0],
   //      yi_lim[1][0],
   //      yi_lim[2][0],
   //      yi_lim[3][0],
   //      yi_lim[4][0]);
   //MAIDPolarizedKinematicKey * aKey = (MAIDPolarizedKinematicKey*)fGridData.At(jkey);


   Double_t gamma = EvaluateFluxFactor(Es,Ep,th);
   FillHyperCubeValues(ph); // Create a hyper cube for phi_e = ph
   Double_t res = GetInterpolation(); // Get interoplation result.

   res = gamma*res*1.0e3*fOmega_pi_cm_over_lab;
   //std::cout << fOmega_pi_cm_over_lab << std::endl;

   if(res<0){
      //std::cout << " sigma_vres = " << res << std::endl;
      res = 0.0;
   }
   //if(res < 0.3) aKey->Dump();

   if(IncludeJacobian()) return res*TMath::Sin(th);
   return res;  

}

//______________________________________________________________________________
Double_t MAIDExclusivePionDiffXSec::EvaluateFluxFactor(Double_t Es,Double_t Ep,Double_t th) const{

   Double_t nu    = Es-Ep; 
   Double_t Q2    = insane::Kine::Qsquared(Es,Ep,th);
   Double_t eps   = insane::Kine::epsilon(Es,Ep,th); 
   Double_t K     = EvaluateVirtualPhotonFlux(nu,Q2);  
   Double_t alpha = fine_structure_const; 
   Double_t T1    = alpha/(2.*pi*pi); 
   Double_t T2    = Ep/Es; 
   Double_t T3    = K/Q2; 
   Double_t T4    = 1./(1.-eps); 
   Double_t gamma = T1*T2*T3*T4; 
   return gamma; 

}
//______________________________________________________________________________
Double_t MAIDExclusivePionDiffXSec::EvaluateVirtualPhotonFlux(Double_t nu,Double_t Q2) const{

   Double_t K = 0.; 
   Double_t M = M_p/GeV;

   switch(fConvention){
      case 0: // virtual photon energy 
         K = nu; 
         break;
      case 1: // Gilman: use q-vector 
         K = TMath::Sqrt(nu*nu + Q2); 
         break;
      case 2: // Hand: use equivalent energy necessary for the same reaction from a real photon 
         K = nu - Q2/(2.*M); 
         break;
      default: 
         std::cout << "[MAIDExclusivePionDiffXSec::EvaluateVirtualPhotonFlux]: Invalid convention!  Exiting..." << std::endl;
         exit(1);
   }

   return K; 

}
//______________________________________________________________________________
void MAIDExclusivePionDiffXSec::BinarySearch(std::vector<Double_t> *array,Double_t key,Int_t &lowerbound,Int_t &upperbound) const {
   Int_t comparisonCount = 1;    //count the number of comparisons (optional)
   Int_t n               = array->size();
   lowerbound            = 0;
   upperbound            = n-1;

   // To start, find the subscript of the middle position.
   Int_t position = ( lowerbound + upperbound) / 2;

   while((array->at(position) != key) && (lowerbound <= upperbound)){
      comparisonCount++;
      if (array->at(position) > key){
         // decrease position by one.
         upperbound = position - 1;
      }else{
         // Else, increase position by one.
         lowerbound = position + 1;
      }
      position = (lowerbound + upperbound) / 2;
   }

   Double_t lo=0,hi=0,mid;
   Int_t dump = lowerbound;

   if (lowerbound <= upperbound){
      // std::cout << "[BinarySearch]: The number was found in array subscript " << position << std::endl; 
      // std::cout << "                The binary search found the number after " << comparisonCount << " comparisons." << std::endl;             
      // lo  = array[lowerbound];
      // hi  = array[upperbound];
      // mid = array[position]  
      // if(lo==hi){
      lowerbound = position;
      upperbound = position+1;
      // }
   }else{
      lowerbound = upperbound;
      upperbound = dump;
   }
   // to safeguard against values that are outside the boundaries of the grid 
   if(upperbound>=n){
      upperbound = n-1;
      lowerbound = n-2;
   }
   if(upperbound==0){
      lowerbound = 0;
      upperbound = 1;
   }
   //lo   = array->at(lowerbound);
   //hi   = array->at(upperbound);
   // std::cout << "[BinarySearch]: Sorry, the number is not in this array.  The binary search made " << comparisonCount << " comparisons." << std::endl;
   // std::cout << "                Target = "         << key << std::endl;
   // std::cout << "                Bounding values: " << std::endl;
   // std::cout << "                low  = "           << lo << std::endl;
   // std::cout << "                high = "           << hi << std::endl;
}
//______________________________________________________________________________
void MAIDExclusivePionDiffXSec::ImportData(){

   fFileName = fprefix + Form("target_pol_xsecs_%s.txt",frxn.Data());

   fKineKey = new MAIDPolarizedKinematicKey();

   Double_t ieps,iQ2,iW,itheta,iphi,iSig_0,iA_e,iA_1,iA_2,iA_3,iA_e1,iA_e2,iA_e3; 

   std::ifstream infile(fFileName.Data());
   Int_t N = 0;
   if(infile.fail()){
      std::cout << "[MAIDExclusivePionDiffXSec::ImportData]: Cannot open the file: " << fFileName.Data() << std::endl;
      exit(1); 
   }else{
      std::cout << "[MAIDExclusivePionDiffXSec::ImportData]: Opening the file: " << fFileName.Data() << std::endl;
      while(!infile.eof()){
         auto * akey = new MAIDPolarizedKinematicKey(N);
         infile >> ieps >> iQ2 >> iW >> itheta >> iphi >> iSig_0 >> iA_e >> iA_1 >> iA_2 >> iA_3 >> iA_e1 >> iA_e2 >> iA_e3; 
         akey->fepsilon  = ieps; 
         akey->fQ2       = iQ2; 
         akey->fW        = iW; 
         akey->fTheta    = itheta; 
         akey->fPhi      = iphi; 
         akey->fSigma_0  = iSig_0;
         akey->fA_e      = iA_e/100.0;
         akey->fA_1      = iA_1/100.0;
         akey->fA_2      = iA_2/100.0;
         akey->fA_3      = iA_3/100.0;
         akey->fA_e1     = iA_e1/100.0;
         akey->fA_e2     = iA_e2/100.0;
         akey->fA_e3     = iA_e3/100.0;
         //if( infile.eof() ) break;
         fGridData.Add(akey);
         N++;
      }
      infile.close(); 
   }

   std::cout << "sorting... "<< std::endl;
   //fGridData.Sort();
   //fGridData.Print();
   std::cout << "done sorting. " << std::endl;

   auto * akey = (MAIDPolarizedKinematicKey*)fGridData.At(0);
   fepsilon.push_back(akey->fepsilon);  
   fW.push_back(akey->fW);  
   fQ2.push_back(akey->fQ2);  
   fTheta.push_back(akey->fTheta);  
   fPhi.push_back(akey->fPhi);  
   for(int i = 0;i<fGridData.GetEntries();i++){
      akey = (MAIDPolarizedKinematicKey*)fGridData.At(i);
      if(fPhi.back()     != akey->fPhi)     fPhi.push_back(akey->fPhi); 
      if(fTheta.back()   != akey->fTheta)   fTheta.push_back(akey->fTheta); 
      if(fW.back()       != akey->fW)       fW.push_back(akey->fW); 
      if(fQ2.back()      != akey->fQ2)      fQ2.push_back(akey->fQ2); 
      if(fepsilon.back() != akey->fepsilon) fepsilon.push_back(akey->fepsilon); 
   }

   // erase repeated entries 
   std::sort( fW.begin(),fW.end() ); 
   fW.erase( std::unique( fW.begin(),fW.end() ),fW.end() ); 
 
   std::sort( fQ2.begin(),fQ2.end() ); 
   fQ2.erase( std::unique( fQ2.begin(),fQ2.end() ),fQ2.end() );  
 
   std::sort( fepsilon.begin(),fepsilon.end() ); 
   fepsilon.erase( std::unique( fepsilon.begin(),fepsilon.end() ),fepsilon.end() );  
  
   std::sort( fTheta.begin(),fTheta.end() ); 
   fTheta.erase( std::unique( fTheta.begin(),fTheta.end() ),fTheta.end() );  
 
   std::sort( fPhi.begin(),fPhi.end() ); 
   fPhi.erase( std::unique( fPhi.begin(),fPhi.end() ),fPhi.end() );  

   // ------------------------------------------------------------------
   std::cout << "epsilon values: ";
   for(auto it = fepsilon.begin() ; it != fepsilon.end(); ++it)
      std::cout << ' ' << *it;
   std::cout << ' ' << std::endl;;

   std::cout << "theta_pi values: ";
   for(auto it = fTheta.begin() ; it != fTheta.end(); ++it)
      std::cout << ' ' << *it;
   std::cout << ' ' << std::endl;;

   std::cout << "Phi_pi values: ";
   for(auto it = fPhi.begin() ; it != fPhi.end(); ++it)
      std::cout << ' ' << *it;
   std::cout << ' ' << std::endl;;

   std::cout << "Q2 values: ";
   for(auto it = fQ2.begin() ; it != fQ2.end(); ++it)
      std::cout << ' ' << *it;
   std::cout << ' ' << std::endl;;

   std::cout << "W values: ";
   for(auto it = fW.begin() ; it != fW.end(); ++it)
      std::cout << ' ' << *it;
   std::cout << ' ' << std::endl;;

   // print the size of the grid  
   std::cout << "[MAIDExclusivePionDiffXSec::ImportData]: Grid dimensions: " 
             << fepsilon.size() << " x " << fQ2.size()    << " x " 
             << fW.size()       << " x " << fTheta.size() << " x " 
             << fPhi.size()     << std::endl;

   fNumberOfPoints = fGridData.GetEntries() ; 
}

//______________________________________________________________________________
Int_t MAIDExclusivePionDiffXSec::GetGridIndex(Int_t ieps, Int_t iQ2, Int_t iW, Int_t iTh, Int_t iPh) const {
   Int_t res  = -1;
   Int_t nPh  = iPh;
   Int_t nTh  = fPhi.size()*iTh;
   Int_t nW   = fTheta.size()*fPhi.size()*iW;
   Int_t nEps = fW.size()*fTheta.size()*fPhi.size()*ieps;
   Int_t nQ2  = fQ2.size()*fW.size()*fTheta.size()*fPhi.size()*iQ2;
   res        = nPh + nTh + nW + nQ2 + nEps; 
   //std::cout << " index = " << res << " of " << fNumberOfPoints << std::endl;
   if( res >= fNumberOfPoints ) res = -1;
   return(res);
}

//______________________________________________________________________________
void MAIDExclusivePionDiffXSec::Print(Option_t * ){
   PrintParameters(); 
   PrintData();
}
//______________________________________________________________________________
void MAIDExclusivePionDiffXSec::PrintParameters(){

   std::cout << "-------------------- MAID Parameters --------------------" << std::endl; 
   std::cout << "Reaction channel (pion,nucleon) = " << "(" << fPion << "," << fNucleon << ")" << std::endl; 
   std::cout << "Virtual photon flux convention  = " << fConvention << " (" << fConventionName << ")" << std::endl;
   std::cout << "Number of data points           = " << fNumberOfPoints     << std::endl;
   std::cout << "---------------------------------------------------------" << std::endl; 

}
//______________________________________________________________________________
void MAIDExclusivePionDiffXSec::PrintData(){

   std::cout << "-------------------- MAID Data --------------------" << std::endl; 
   if(fNumberOfPoints==0){
      std::cout << "[MAIDExclusivePionDiffXSec::PrintData]: No data!  Exiting..." << std::endl;
      exit(1); 
   }
}

//______________________________________________________________________________
void MAIDExclusivePionDiffXSec::Clear(Option_t * ){

   fQ2.clear();
   fW.clear();
   fEprime.clear();
   fTheta.clear();
   fPhi.clear();
   fepsilon.clear();
   fNu.clear(); 
   // fGamma.clear(); 
   fSigma_0.clear(); 
   fA_e.clear();
   fA_1.clear();
   fA_2.clear();
   fA_3.clear();
   fA_e1.clear();
   fA_e2.clear();
   fA_e3.clear();
   
}

//______________________________________________________________________________
}}
