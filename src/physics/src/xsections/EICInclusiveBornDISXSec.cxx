#include "insane/xsections/EICInclusiveBornDISXSec.h"
#include <cmath>

namespace insane {
  namespace physics {
    namespace eic {


      EICInclusiveBornDISXSec::EICInclusiveBornDISXSec()
      {
        fID        = 100000010;
        fTitle     = "EICInclusiveBornDISXSec";
        fPlotTitle = "#frac{d#sigma}{dE'd#Omega} nb/GeV-sr";
        fnDim          = 3; // 3 for DIS on the nucleon + 3 for nucleon fermi momentum
        fnParticles    = 1;
        fPIDs.clear();
        fPIDs.push_back(11);
        xs = new  CompositeDiffXSec2();
        xs->InitializePhaseSpaceVariables();
        xs->UsePhaseSpace(false);
      }
      //______________________________________________________________________________

      EICInclusiveBornDISXSec::~EICInclusiveBornDISXSec()
      {
        delete xs;
      }
      //______________________________________________________________________________
      EICInclusiveBornDISXSec::EICInclusiveBornDISXSec(const EICInclusiveBornDISXSec& rhs) : InclusiveDiffXSec(rhs)
      {
        (*this) = rhs; 
      }
      //______________________________________________________________________________

      EICInclusiveBornDISXSec& EICInclusiveBornDISXSec::operator=(const EICInclusiveBornDISXSec& rhs)
      {
        if (this != &rhs) {  // make sure not same object
          InclusiveDiffXSec::operator=(rhs);
        }
        return *this;

      }
      //______________________________________________________________________________

      EICInclusiveBornDISXSec*  EICInclusiveBornDISXSec::Clone(const char * newname) const
      {
        std::cout << "EICInclusiveBornDISXSec::Clone()\n";
        auto * copy = new EICInclusiveBornDISXSec();
        (*copy) = (*this);
        return copy;
      }
      //______________________________________________________________________________

      EICInclusiveBornDISXSec*  EICInclusiveBornDISXSec::Clone() const 
      {
        return( Clone("") ); 
      }
      //______________________________________________________________________________
      
      void EICInclusiveBornDISXSec::InitializePhaseSpaceVariables()
      {
        //std::cout << " o InclusiveDiffXSec::InitializePhaseSpaceVariables() \n";

        PhaseSpace * ps = GetPhaseSpace();

          auto * varEnergy = new PhaseSpaceVariable("energy_e", "E_{e'}", 0.5, fBeamEnergy);
          ps->AddVariable(varEnergy);

          auto *   varTheta = new PhaseSpaceVariable("theta_e", "#theta_{e'}", 2.0*degree, 170.0*degree );
          ps->AddVariable(varTheta);

          auto *   varPhi = new PhaseSpaceVariable("phi_e", "#phi_{e'}",-180.0*degree,180.0*degree );
          varPhi->SetUniform(true);
          ps->AddVariable(varPhi);

          SetPhaseSpace(ps);

      }
      //______________________________________________________________________________

      Double_t EICInclusiveBornDISXSec::EvaluateXSec(const Double_t *x) const
      {
        if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);


        Double_t M    = M_p/GeV;            // in GeV 
        Double_t Ep   = x[0];               // scattered electron energy in GeV 
        Double_t th   = x[1];               // electron scattering angle in radians 
        Double_t ph   = x[2];               // electron scattering angle in radians 

        double P0     = fKine.fP0.E();
        double P      = TMath::Sqrt(P0*P0-M*M);
        //std::cout << "P0  = " << P0  << std::endl;
        //std::cout << "P   = " << P   << std::endl;

        double k1_Lab  = fBeamEnergy;
        double k2_Lab  = Ep;
        double th_Lab  = th;
        //std::cout << "k1_Lab = " << k1_Lab << std::endl;
        //std::cout << "k2_Lab = " << k2_Lab << std::endl;
        //std::cout << "th_Lab = " << th_Lab/degree << std::endl;
        
        double s      = M*M + 2.0*k1_Lab*(P0+P);
        //std::cout << "s   = " << s   << std::endl;

        double k1_Rest = (s-M*M)/(2.0*M);
        double k2_Rest = k2_Lab*(P0+P*TMath::Cos(th_Lab))/M;
        double th_Rest = TMath::ASin( (k2_Lab/k2_Rest)*TMath::Sin(th_Lab) );
        //std::cout << "k1_Rest = " << k1_Rest << std::endl;
        //std::cout << "k2_Rest = " << k2_Rest << std::endl;
        //std::cout << "th_Rest = " << th_Rest/degree << std::endl;
        //
        Double_t kine[] = {k2_Rest, th_Rest, ph};
        xs->SetIncludeJacobian(false);
        xs->SetTargetNucleus(GetTargetNucleus());
        xs->SetBeamEnergy(k1_Rest);
        double xs_Rest = xs->EvaluateXSec(kine);
        //std::cout << "xs_Rest = "<< xs_Rest << std::endl;

        // Compute the jacobian (not for full incoherent process, but close enough)
        //double M     = M_p/GeV;
        double Jacobian  = (k2_Lab/k2_Rest)*(TMath::Tan(th_Rest)/TMath::Sin(th_Lab));
        Jacobian        *= ((P0/M)*TMath::Cos(th_Lab) + TMath::Sqrt(P0*P0-M*M)/M);
        //std::cout << Jacobian << std::endl;
        //std::cout << "xs_Rest = "<< xs_Rest << std::endl;

        double fullXsec = Jacobian*xs_Rest;

        if( TMath::IsNaN(fullXsec) || fullXsec < 0.0 ) return 0.0;
        if( std::isinf(  fullXsec) ) return 0.0;


        if ( IncludeJacobian() ){
          return fullXsec;
        }
        return fullXsec;
      }
      //______________________________________________________________________________
    }
  }
}


