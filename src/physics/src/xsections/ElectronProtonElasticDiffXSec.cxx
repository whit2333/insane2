#include "insane/xsections/ElectronProtonElasticDiffXSec.h"
#include "insane/base/PhysicalConstants.h"
#include "insane/xsections/RadiativeEffects.h"

namespace insane {
   namespace physics {

ElectronProtonElasticDiffXSec::ElectronProtonElasticDiffXSec()
{
   fID            = 200000040;
   SetTitle("ElectronProtonElasticDiffXSec");
   SetPlotTitle("Elastic cross-section");
   fLabel         = "#frac{d#sigma}{d#Omega}";
   fUnits         = "nb/sr";
   fnDim          = 3;
   fPIDs.clear();
   fnParticles    = 1;
   fPIDs.push_back(11);
   //fPIDs.push_back(2212);
   SetElasticOnly(true);
}
//______________________________________________________________________________

ElectronProtonElasticDiffXSec::~ElectronProtonElasticDiffXSec(){
}
//______________________________________________________________________________

void ElectronProtonElasticDiffXSec::InitializePhaseSpaceVariables()
{

   PhaseSpace * ps = GetPhaseSpace();
   // ------------------------------
   // Electron
   auto * varEnergy = new PhaseSpaceVariable(
         "energy_e", "E_{e'}", 0.001, GetBeamEnergy());
   ps->AddVariable(varEnergy);
   // Note the energy is now independent for radiative tail

   auto *   varTheta = new PhaseSpaceVariable(
         "theta_e", "#theta_{e'}", 0.01*degree, 180.0*degree);
   ps->AddVariable(varTheta);

   auto *   varPhi = new PhaseSpaceVariable(
         "phi_e", "#phi_{e'}", -180.0*degree, 180.0*degree);
   //varPhi->SetUniform(true);
   varPhi->SetUniform(true);
   ps->AddVariable(varPhi);


   // ------------------------------
   // proton
   //PhaseSpaceVariable * varP_p = new PhaseSpaceVariable(
   //      "P_p", "P_{p}",0.001,5.0);
   //varP_p->SetDependent(true);
   //varP_p->SetParticleIndex(1);
   //ps->AddVariable(varP_p);

   //PhaseSpaceVariable *   varTheta_p = new PhaseSpaceVariable( 
   //      "theta_p", "#theta_{p}", 1*degree, 180.0*degree);
   //varTheta_p->SetParticleIndex(1);
   //varTheta_p->SetDependent(true);
   ////varTheta_p->SetDependent(true);
   //ps->AddVariable(varTheta_p);

   //PhaseSpaceVariable *   varPhi_p = new PhaseSpaceVariable(
   //      "phi_p", "#phi_{p}", -180.0*degree, 180.0*degree);
   //varPhi_p->SetParticleIndex(1);
   //varPhi_p->SetDependent(true);
   //ps->AddVariable(varPhi_p);

   SetPhaseSpace(ps);
}
////______________________________________________________________________________
//
//void ElectronProtonElasticDiffXSec::DefineEvent(Double_t * vars)
//{
//   // Virtual method to define the random event from the variables provided.
//   // This is the transition point between the phase space variables and
//   // the particles. The argument should be the full list of variables returned from
//   // GetDependentVariables. 
//
//   Int_t totvars = 0;
//
//   ::Kine::SetMomFromEThetaPhi((TParticle*)(fParticles.At(0)), &vars[totvars]);
//   totvars += GetNParticleVars(0);
//
//   ::Kine::SetEFromMomThetaPhi((TParticle*)(fParticles.At(1)), &vars[totvars]);
//   totvars += GetNParticleVars(1);
//
//}
////______________________________________________________________________________
//
Double_t * ElectronProtonElasticDiffXSec::GetDependentVariables(const Double_t * x) const
{
   // The elastic radiative tail is a function of E', theta, and phi
   //
   using namespace TMath;
   using namespace insane::units;
   //double M = M_p/GeV;//fTargetNucleus.GetMass();//GetParticlePDG(1)->Mass();
   //TVector3 p2(0, 0, 0);          // scattered electron
   double P_e      = x[0];//GetEPrime(x[0]);
   double theta_e  = x[1];
   double phi      = x[2];
   //double Eprime   = P_e;

   //double Es     = M*P_alpha/((M+E_alpha)*Cos(theta_alpha) - P_alpha);
   //double theta  = GetTheta_e(Es,theta_alpha);
   //double Eprime = Es/(1.0 + 2.0*Es/M*TMath::Power(TMath::Sin(theta/2.0), 2));

   //if(Es<0.0 ){
   //   return nullptr;
   //}
   //if(std::isnan(theta) ){
   //   return nullptr;
   //}
   fDependentVariables[0] = x[0]; 
   fDependentVariables[1] = x[1]; 
   fDependentVariables[2] = x[2]; 
   //fDependentVariables[3] = P_alpha;
   //fDependentVariables[4] = theta_alpha;
   //fDependentVariables[5] = phi;
   return(fDependentVariables);
}
//______________________________________________________________________________

double ElectronProtonElasticDiffXSec::GetPalpha(double Es, double alpha) const
{
   using namespace TMath;
   using namespace insane::units;
   double M   = M_p/GeV;//fTargetNucleus.GetMass();
   double res = 4.0*Es*M*(Es+M)*Cos(alpha)/(Es*Es+4.0*Es*M+2.0*M*M - Es*Es*Cos(2.0*alpha));
   return res;
}
//______________________________________________________________________________

double ElectronProtonElasticDiffXSec::GetTheta_e(double Es, double alpha) const
{
   using namespace TMath;
   using namespace insane::units;
   double M   = M_p/GeV;//fTargetNucleus.GetMass();
   double res = 2.0*ATan((Es+M)*Sin(alpha)/(M*Cos(alpha)));
   return res;
}
//______________________________________________________________________________

double  ElectronProtonElasticDiffXSec::ElectronToAlphaJacobian(double Es, double alpha) const
{
   using namespace TMath;
   using namespace insane::units;
   double M   = M_p/GeV;//fTargetNucleus.GetMass();
   double theta         = GetTheta_e(Es,alpha);
   double dtheta_dalpha = 2.0*M*(Es+M)/(Power(M*Cos(alpha),2.0) + Power((Es+M)*Sin(alpha),2.0));
   double res           = dtheta_dalpha*Sin(theta);
   return res;
}
//______________________________________________________________________________

double ElectronProtonElasticDiffXSec::Ib(Double_t E0,Double_t E,Double_t t) const {
   // Bremsstrahlung loss 
   // Tsai, SLAC-PUB-0848 (1971), Eq B.43
   // Same as B.43 but we explicitly write out W_b(E,eps) 
   // so that X0 cancels from the formula
   Double_t b     = 4.0/3.0;//fKinematics.Getb(); 
   Double_t gamma = TMath::Gamma(1. + b*t);
   // Double_t Wb    = W_b(E0,E0-E);
   // Wb is (X0 cancels out when combined with everything in this method): 
   // Double_t b     = fKinematicsExt.Getb(); 
   // Double_t X0    = fX0Eff;
   // Double_t Wb    = (1./X0)*(b/eps)*GetPhi(eps/E0);
   Double_t v     = (E0-E)/E0;
   Double_t phi   = (1.0 - v + (3.0/4.0)*v*v); 
   Double_t T1    = (b*t/gamma)*( 1./(E0-E) ); 
   Double_t T2    = TMath::Power(v,b*t);
   Double_t T3    = phi;
   Double_t ib    = T1*T2*T3;
   return ib; 
}
//______________________________________________________________________________

double ElectronProtonElasticDiffXSec::GetEs(double Ep, double theta) const {
   // Returns the incident beam energy need to elastically scatter
   // at the given energy and angle.
   using namespace TMath;
   double M  = M_p/GeV;
   return( Ep/(1.0 - (2.0*Ep/M)*Power(Sin(theta/2.0),2)) );
}
//________________________________________________________________________
////______________________________________________________________________________
//Double_t ElectronProtonElasticDiffXSec::GetEPrime(const Double_t theta)const  {
//   // Returns the scattered electron energy using the angle. */
//   Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
//   return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M * TMath::Power(TMath::Sin(theta / 2.0), 2)));
//}
////________________________________________________________________________
//Double_t ElectronProtonElasticDiffXSec::GetTheta(double ep) const {
//   using namespace TMath;
//   double M     = fTargetNucleus.GetMass();
//   double Eb    = GetBeamEnergy();
//   double theta = 2.0*ASin(Sqrt((Eb-ep)*M/(2.0*Eb*ep)));
//   return theta;
//}
////______________________________________________________________________________
//
double ElectronProtonElasticDiffXSec::Sig_el(double Es, double Ep, double theta) const
{
   using namespace TMath;
   using namespace insane::units;
   double M      = M_p/GeV;//fTargetNucleus.GetMass();
   double Qsquared = 4.0 * Ep * Es * TMath::Power(TMath::Sin(theta / 2.0), 2);
   double tau      = Qsquared/(4.0*M*M);
   double mottXSec = insane::Kine::Sig_Mott(Es,theta);
   double recoil   = insane::Kine::fRecoil(Es,theta,M);
   double GE2     = Power(fFormFactors->GEp(Qsquared), 2);
   double GM2     = Power(fFormFactors->GMp(Qsquared), 2);
   double sig_el  = (mottXSec/recoil)*((GE2+tau*GM2)/(1.0+tau)+2.0*tau*GM2*Power(Tan(theta/2.0),2));
   return sig_el;
}
//______________________________________________________________________________

Double_t  ElectronProtonElasticDiffXSec::EvaluateXSec(const Double_t * x) const {
   // Get the recoiling proton momentum
   if(!x) return(0.0);
   if (!VariablesInPhaseSpace(3, x)) return(0.0);

   using namespace TMath;
   using namespace insane::units;

   double E0     = GetBeamEnergy();
   double M      = M_p/GeV;//fTargetNucleus.GetMass();
   double A      = fTargetNucleus.GetA();
   double T_mat  = fTargetMaterial.GetNumberOfRadiationLengths();
   //std::cout << T_mat << std::endl;

   double Ep     = x[0];
   double theta  = x[1];
   double Es     = GetEs(Ep,theta);

   if( (Es > E0) || (Es < 0.0)  ){
      return 0.0;
   }

   double Qsquared = 4.0*Ep*Es*Power(Sin(theta/2.0),2);

   //double ws = omega_s_Tsai(Es,Ep,theta,M);
   //double wp = omega_p_Tsai(Es,Ep,theta,M);

   double sig1  = Sig_el(Es,Ep,theta);
   //double sig2  = Sig_el(Es-ws,Ep,theta);

   double T_X0     = tr_Tsai(Qsquared);
   double T        = T_X0 + T_mat;
   double bT       = T*4.0/3.0;

   // I is the probability of the incident electron losing 
   // energy to be come Ep. This dramatically reduces the cross section
   // far away from the peak.
   double I   = Ib(E0,Es,T/2.0);
   //std::cout << "I(" << E0 << ", " << Es << ") = " << I << std::endl;

   // Eq'ns C.13 of Tsai 1971, SLAC-PUB-848
   //double T00 = (1.0+0.5772*bT)*Power(ws/Es,bT/2.0)*Power(wp/(Ep+wp),bT/2.0);
   //double T01 = sig1*bT*DiLog(wp/(Ep+wp))/(2.0*wp);
   //double T02 = (sig2*bT*DiLog(ws/Es)/(2.0*ws))*(M+(Es-ws)*(1.0-Cos(theta)))/(M-Ep*(1.0-Cos(theta)));

   double sig_rad = I*A*sig1*hbarc2_gev_nb;

   if( IncludeJacobian() ) sig_rad = sig_rad*TMath::Sin(theta);
   if( sig_rad <0.0 || TMath::IsNaN(sig_rad)) sig_rad = 0.0;
   return sig_rad;
}
//______________________________________________________________________________


}
}
