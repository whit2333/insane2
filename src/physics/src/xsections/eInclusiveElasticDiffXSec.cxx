#include "insane/xsections/eInclusiveElasticDiffXSec.h"
#include "insane/formfactors/DipoleFormFactors.h"
#include <cmath>
namespace insane {
namespace physics {


eInclusiveElasticDiffXSec::eInclusiveElasticDiffXSec() 
{
   fID         = 100004011;
   fnDim = 2;
}
//______________________________________________________________________________

eInclusiveElasticDiffXSec::~eInclusiveElasticDiffXSec() 
{
}
//______________________________________________________________________________

void eInclusiveElasticDiffXSec::InitializePhaseSpaceVariables()
{
  PhaseSpace * ps = GetPhaseSpace();
  auto * varEnergy = new PhaseSpaceVariable();
  varEnergy->SetNameTitle("energy_e", "E_{e'}");
  varEnergy->SetMinimum(0.01); //GeV
  varEnergy->SetMaximum(5.9); //GeV
  varEnergy->SetDependent(true);
  ps->AddVariable(varEnergy);

  auto *   varTheta = new PhaseSpaceVariable();
  varTheta->SetNameTitle("theta_e", "#theta_{e'}"); // ROOT string latex
  varTheta->SetMinimum( 0.01 *degree ); //
  varTheta->SetMaximum( 180.0*degree ); //
  ps->AddVariable(varTheta);

  auto *   varPhi = new PhaseSpaceVariable();
  varPhi->SetNameTitle("phi_e", "#phi_{e'}"); // ROOT string latex
  varPhi->SetMinimum(-180.0*degree ); //
  varPhi->SetMaximum( 180.0*degree ); //
  ps->AddVariable(varPhi);

  SetPhaseSpace(ps);
}
//______________________________________________________________________________

Double_t * eInclusiveElasticDiffXSec::GetDependentVariables(const Double_t * x) const
{
  // Uses inputs: theta and phi. 
  // Returns : Energy, theta, and phi
  Double_t   theta = x[0];
   Double_t eprime  = GetEPrime(theta);
   fDependentVariables[0] = eprime;
   fDependentVariables[1] = theta;
   fDependentVariables[2] = x[1];
   return(fDependentVariables);
}
//______________________________________________________________________________

Double_t  eInclusiveElasticDiffXSec::EvaluateXSec(const Double_t * x) const 
{
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   double Eprime   = x[0];
   double theta    = x[1];
   using namespace TMath;
   if(theta > Pi() ) return 0.0;

   double M             = fTargetNucleus.GetMass();//M_p/GeV;
   double sinthetaOver2 = Sin(theta/2.0);
   double tanthetaOver2 = Tan(theta/2.0);
   double Qsquared      = 4.0*Eprime*GetBeamEnergy()*TMath::Power(sinthetaOver2*sinthetaOver2, 2.0);
   double tau           = Qsquared/(4.0*M*M);

   double mottXSec      = insane::Kine::Sig_Mott(GetBeamEnergy(),theta);
   double recoil_factor = Eprime/GetBeamEnergy();
   double GE2           = Power(fFormFactors->GEp(Qsquared), 2.0);
   double GM2           = Power(fFormFactors->GMp(Qsquared), 2.0);

   // Rosenbluth formula
   double res = mottXSec*recoil_factor*( (GE2+tau*GM2)/(1.0+tau) 
         + 2.0*tau*GM2*tanthetaOver2*tanthetaOver2 );
   res = res * hbarc2_gev_nb;

   if( TMath::IsNaN(res) || res < 0.0 ) return 0.0;
   if( std::isinf(res) ) return 0.0;
   return( res );
}
//______________________________________________________________________________

double eInclusiveElasticDiffXSec::WDependentXSec(double *x, double *p) 
{
   Double_t W      = x[0];
   Double_t Q2     = p[0];
   Double_t phi    = p[1];
   Double_t M        = fTargetNucleus.GetMass();

   //   This should be a delta function...
   if(TMath::Abs(W-M) > 0.001) return 0.0;

   Double_t xbj    = insane::Kine::xBjorken_WQsq(W, Q2);
   Double_t E0     = GetBeamEnergy();
   Double_t y_frac = Q2/(2.0*(M_p/GeV)*xbj*E0);
   if( y_frac >1.0 ) return 0.0;
   Double_t Eprime = insane::Kine::Eprime_xQ2y(xbj,Q2,y_frac);
   Double_t theta  = insane::Kine::Theta_xQ2y(xbj,Q2,y_frac);
   if( TMath::IsNaN(theta) )  return 0.0;
   if( TMath::IsNaN(Eprime) )  return 0.0;
   //std::cout << "W: " << W << ", Q2: " << Q2 << std::endl;
   //std::cout << "theta: " << theta << ", Eprime: " << Eprime ;
   //std::cout << ", y_frac: " << y_frac << ", x: " << xbj << std::endl;
   //fFuncArgs[0] = Eprime;
   fFuncArgs[0] = theta;
   fFuncArgs[1] = phi;
   return(EvaluateXSec(GetDependentVariables(fFuncArgs)));
}
//______________________________________________________________________________

double eInclusiveElasticDiffXSec::Vs_W(double *x, double *p) 
{
   //Double_t E0 = GetBeamEnergy();
   //Double_t W  = x[0];
   //Double_t th = p[0]; 
   //fFuncArgs[0] = ::Kine::Eprime_W2theta(W*W,th,E0);
   fFuncArgs[0] = p[0];
   fFuncArgs[1] = p[1];
   return(EvaluateXSec(GetDependentVariables(fFuncArgs)));
}
//______________________________________________________________________________
}}
