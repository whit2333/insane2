#include "insane/xsections/MAIDPolarizedTargetDiffXSec.h"
#include <fstream>

namespace insane {
namespace physics {
MAIDPolarizedTargetDiffXSec::MAIDPolarizedTargetDiffXSec(
      const char * pion,
      const char * nucleon)
{
   fID      = 100104035;
   fPion    = pion; 
   fNucleon = nucleon; 
   frxn     = Form("%s_%s",fPion.Data(),fNucleon.Data());

   fnDim = 5;

   //x_eval.resize(N);
   //xd.resize(5);
   //y_lim.resize(5);
   for(int i=0;i<5;i++){
      y_lim.push_back( std::vector<double>() );
      xd.push_back(0.0);
      x_eval[i] = 0.5;
      //y_lim[i].resize(2);
      y_lim[i].push_back(0.0); 
      y_lim[i].push_back(1.0); 
   }

   // work around
   c_5 = &(c_5_4[0]);
   //c_5.resize(5); 
   c_4.resize(4); 
   c_3.resize(3); 
   c_2.resize(2); 
   c_1.resize(1); 
   for(int i=0;i<2;i++){
      c_5[i].resize(2); 
      c_4[i].resize(2); 
      c_3[i].resize(2); 
      c_2[i].resize(2); 
      c_1.push_back(0.0);
      for(int j=0;j<2;j++){
         c_5[i][j].resize(2); 
         c_4[i][j].resize(2); 
         c_3[i][j].resize(2); 
         c_2[i].push_back(0.0);
         for(int k=0;k<2;k++){
            c_5[i][j][k].resize(2); 
            c_4[i][j][k].resize(2); 
            c_3[i][j].push_back(0.0);
            for(int l=0;l<2;l++){
               c_4[i][j][k].push_back(0.0);
               c_5[i][j][k][l].resize(2);
               for(int m=0;m<2;m++){
                  c_5[i][j][k][l].push_back(0.0);
               }
            }
         }
      }
   }

   Initialize();
   ImportData();
}

//______________________________________________________________________________
MAIDPolarizedTargetDiffXSec::~MAIDPolarizedTargetDiffXSec(){
   
}

//____________________________________________________________________
//void MAIDPolarizedTargetDiffXSec::InitializePhaseSpaceVariables() {
//   //std::cout << " o InSANEInclusiveDiffXSec::InitializePhaseSpaceVariables() \n";
//   InSANEPhaseSpace * ps = GetPhaseSpace();
//   if (ps) delete ps;
//   ps = 0;
//   if (!ps) {
//      //       std::cout << " o creating new InSANEPhaseSpace\n";
//
//      ps = new InSANEPhaseSpace();
//
//      InSANEPhaseSpaceVariable * varEnergy = new InSANEPhaseSpaceVariable();
//      varEnergy = new InSANEPhaseSpaceVariable();
//      varEnergy->SetNameTitle("energy_e", "E_{e'}");
//      varEnergy->SetMinimum(0.3); //GeV
//      varEnergy->SetMaximum(5.0); //GeV
//      //fEnergy_e = varEnergy->GetCurrentValueAddress();
//      //fEnergy = varEnergy->GetCurrentValueAddress();
//      ps->AddVariable(varEnergy);
//
//      InSANEPhaseSpaceVariable *   varTheta = new InSANEPhaseSpaceVariable();
//      varTheta->SetNameTitle("theta_e", "#theta_{e'}"); // ROOT string latex
//      varTheta->SetMinimum(1.0  *degree ); //
//      varTheta->SetMaximum(179.0*degree ); //
//      //fTheta_e = varTheta->GetCurrentValueAddress();
//      //fTheta = varTheta->GetCurrentValueAddress();
//      ps->AddVariable(varTheta);
//
//      InSANEPhaseSpaceVariable *   varPhi = new InSANEPhaseSpaceVariable();
//      varPhi->SetNameTitle("phi_e", "#phi_{e'}"); // ROOT string latex
//      varPhi->SetMinimum(-60.0*degree ); //
//      varPhi->SetMaximum( 60.0*degree ); //
//      //fPhi_e = varPhi->GetCurrentValueAddress();
//      //fPhi = varPhi->GetCurrentValueAddress();
//      ps->AddVariable(varPhi);
//
//
//      InSANEPhaseSpaceVariable * varEnergyPi = new InSANEPhaseSpaceVariable();
//      varEnergyPi = new InSANEPhaseSpaceVariable();
//      varEnergyPi->SetNameTitle("energy_pi", "E_{pi}");
//      varEnergyPi->SetMinimum(0.0); //GeV
//      varEnergyPi->SetMaximum(5.0); //GeV
//      ps->AddVariable(varEnergyPi);
//
//      InSANEPhaseSpaceVariable *   varThetaPi = new InSANEPhaseSpaceVariable();
//      varTheta->SetNameTitle("theta_pi", "#theta_{pi}"); // ROOT string latex
//      varTheta->SetMinimum(0.0  *degree ); //
//      varTheta->SetMaximum(180.0*degree ); //
//      ps->AddVariable(varThetaPi);
//
//      InSANEPhaseSpaceVariable *   varPhiPi = new InSANEPhaseSpaceVariable();
//      varPhi->SetNameTitle("phi_pi", "#phi_{pi}"); // ROOT string latex
//      varPhi->SetMinimum(-180.0*degree ); //
//      varPhi->SetMaximum( 180.0*degree ); //
//      ps->AddVariable(varPhiPi);
//
//      SetPhaseSpace(ps);
//
//   } else {
//      std::cout << " Using existing phase space variables !\n";
//   }
//}

//______________________________________________________________________________
void MAIDPolarizedTargetDiffXSec::SetVirtualPhotonFluxConvention(Int_t i){
   fConvention = i;
   switch(fConvention){
      case 0: 
         fConventionName = "A";  // axial? 
         break;
      case 1: 
         fConventionName = "Gilman"; 
         break;
      case 2: 
         fConventionName = "Hand";
         break;
      default:
         std::cout << "[MAIDPolarizedTargetDiffXSec::EvaluateVirtualPhotonFlux]: Invalid convention!  Exiting..." << std::endl;
         exit(1);
   }
}

//______________________________________________________________________________
void MAIDPolarizedTargetDiffXSec::SetReactionChannel(TString pion,TString nucleon){
   fPion    = pion; 
   fNucleon = nucleon; 
   frxn     = Form("%s_%s",pion.Data(),nucleon.Data());
}

//______________________________________________________________________________
void     MAIDPolarizedTargetDiffXSec::FillHyperCubeValues() const {
   for(int i=0;i<5;i++)  xd[i] = (x_eval[i]-y_lim[i][0])/(y_lim[i][1]-y_lim[i][0]);
   for(int i=0;i<2;i++){
      for(int j=0;j<2;j++){
         for(int k=0;k<2;k++){
            for(int l=0;l<2;l++){
               for(int m=0;m<2;m++){
                  fKineKey->fepsilon = y_lim[0][i];   
                  fKineKey->fQ2      = y_lim[1][j]; 
                  fKineKey->fW       = y_lim[2][k]; 
                  fKineKey->fTheta   = y_lim[3][l];   
                  fKineKey->fPhi     = y_lim[4][m];   
                  int key_loc        = fGridData.BinarySearch(fKineKey);
                  //std::cout << " key_loc = " << key_loc << std::endl;
                  double sig0        = 0;
                  if(key_loc != -1 ) sig0 = ( (MAIDPolarizedKinematicKey*)fGridData.At(key_loc) )->fSigma_0/hbarc2_gev_ub;
                  //std::cout << " sig0 = " << sig0 << std::endl;
                  double res         = sig0; 
                  c_5[i][j][k][l][m] = res;//double(m); // f(x[]) = m  
               }
            }
         }
      }
   }
}

//______________________________________________________________________________
Double_t MAIDPolarizedTargetDiffXSec::GetInterpolation() const {
   for(int i=0;i<2;i++){
      for(int j=0;j<2;j++){
         for(int k=0;k<2;k++){
            for(int l=0;l<2;l++){
               c_4[i][j][k][l] = c_5[i][j][k][l][0]*(1.0-xd[4]) + c_5[i][j][k][l][1]*xd[4] ;
            }
         }
      }
   }
   for(int i=0;i<2;i++){
      for(int j=0;j<2;j++){
         for(int k=0;k<2;k++){
            c_3[i][j][k] = c_4[i][j][k][0]*(1.0-xd[3]) + c_4[i][j][k][1]*xd[3] ;
         }
      }
   }
   for(int i=0;i<2;i++){
      for(int j=0;j<2;j++){
         c_2[i][j] = c_3[i][j][0]*(1.0-xd[2]) + c_3[i][j][1]*xd[2] ;
      }
   }
   for(int i=0;i<1;i++){
      c_1[i] = c_2[i][0]*(1.0-xd[1]) + c_2[i][1]*xd[1] ;
   }
   c_0 = c_1[0]*(1.0-xd[0]) + c_1[1]*xd[0] ;
   //std::cout << "result is " << c_0 << std::endl;
   return(c_0);
}

//______________________________________________________________________________
void MAIDPolarizedTargetDiffXSec::Initialize(){
   TString grid_path = std::getenv("InSANE_PDF_GRID_DIR");
   fprefix     = grid_path + Form("/maid2007/"); // input directory name 
   fDebug      = false;                          // debug flag 
   fh          = 1.;                             // electron helicity (+/- 1) 
   fPx         = 0.;                             // target polarization parallel to virtual photon momentum 
   fPz         = 0.;                             // target polarization perpendicular to virtual photon momentum 
   SetVirtualPhotonFluxConvention(2);            // default is the Hand convention
   // SetCrossSectionType(1);                        // cross section type 
   // SetUnits(1);                                   // default is nb, GeV  
}

//______________________________________________________________________________
Double_t MAIDPolarizedTargetDiffXSec::EvaluateXSec(const Double_t *par) const{
   if (!VariablesInPhaseSpace(fnDim, par)) return(0.0);
   // compute needed variables in GeV and rad  
   // FIXME: what about theta and phi -- they should be for the pion.  
   //        setting to zero for now...   
   Double_t Es    = GetBeamEnergy(); 
   Double_t Ep    = par[0]; 
   Double_t th    = par[1];
   Double_t W     = insane::Kine::W_EEprimeTheta(Es,Ep,th); 
   Double_t Q2    = insane::Kine::Qsquared(Es,Ep,th); 
   Double_t eps   = insane::Kine::epsilon(Es,Ep,th); 
   Double_t th_pi = 0.1; 
   Double_t ph_pi = 0.1; 
   // convert W to MeV 
   Double_t CONV  = 1E+3; 
   W             *= CONV;  
   // Double_t Gamma = EvaluateFluxFactor(Es,Ep,th);  
   // interpolate from the grid (Bilinear Interpolation from Wikipedia) 
   fKineKey->fepsilon = eps;   
   fKineKey->fQ2      = Q2; 
   fKineKey->fW       = W; 
   fKineKey->fTheta   = th_pi;   
   fKineKey->fPhi     = ph_pi;   

   //std::cout << "eps   = " << eps << std::endl;
   //std::cout << "Q2    = " << Q2 << std::endl;
   //std::cout << "W     = " << W << std::endl;
   //std::cout << "th_pi = " << th_pi << std::endl;
   //std::cout << "ph_pi = " << ph_pi << std::endl;

   x_eval[0] = eps;
   x_eval[1] = Q2;
   x_eval[2] = W;
   x_eval[3] = th_pi;
   x_eval[4] = ph_pi;

   // Get boundaries for each linear interpolation. 
   // Indices run from ai-->ei, where i = 1 (low), 2 (high) 
   Int_t ia1,ia2; 
   BinarySearch(&fepsilon,fKineKey->fepsilon,ia1,ia2);
   //std::cout << "ia1    = " << ia1 << std::endl;
   //std::cout << "ia2    = " << ia2 << std::endl;
   //std::cout << "y_lim.size() = " << y_lim.size() << std::endl;
   //std::cout << "y_lim[0].size() = " << y_lim[0].size() << std::endl;
   y_lim[0][0] = fepsilon.at(ia1);
   y_lim[0][1] = fepsilon.at(ia2);

   Int_t ib1,ib2; 
   BinarySearch(&fQ2,fKineKey->fQ2,ib1,ib2);
   y_lim[1][0] = fQ2.at(ib1);
   y_lim[1][1] = fQ2.at(ib2);

   Int_t ic1,ic2; 
   BinarySearch(&fW,fKineKey->fW,ic1,ic2);
   y_lim[2][0] = fW.at(ic1);
   y_lim[2][1] = fW.at(ic2);

   Int_t id1,id2; 
   BinarySearch(&fTheta,fKineKey->fTheta,id1,id2);
   y_lim[3][0] = fTheta.at(id1);
   y_lim[3][1] = fTheta.at(id2);

   Int_t ie1,ie2; 
   BinarySearch(&fPhi,fKineKey->fPhi,ie1,ie2);
   y_lim[4][0] = fPhi.at(ie1);
   y_lim[4][1] = fPhi.at(ie2);

   FillHyperCubeValues();
   Double_t res = GetInterpolation();

   if(IncludeJacobian()) return res*TMath::Sin(th);
   return res;  

}

//______________________________________________________________________________
void MAIDPolarizedTargetDiffXSec::ConvertTargetPolarization(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi) const{

   /// theta_scat = e- in-plane scattering angle 
   /// phi_scat   = e- out-of-plane scattering angle 
   /// +x is out of the page
   /// +y is to the right 
   /// +z is up 

   Double_t SIN_TH = TMath::Sin(theta);
   Double_t COS_TH = TMath::Cos(theta);
   Double_t SIN_PH = TMath::Sin(phi);
   Double_t COS_PH = TMath::Cos(phi);
   /// incident electron 
   Double_t k1      = TMath::Sqrt(Ebeam*Ebeam - (M_e*M_e)/(GeV*GeV) );
   Double_t k1_x    = 0;
   Double_t k1_y    = 0;
   Double_t k1_z    = k1;
   // Double_t k1_t = Ebeam;
   /// scattered electron 
   Double_t k2      = TMath::Sqrt(Eprime*Eprime - (M_e*M_e)/(GeV*GeV));
   Double_t k2_x    = k2*SIN_TH*COS_PH;
   Double_t k2_y    = k2*SIN_TH*SIN_PH;
   Double_t k2_z    = k2*COS_TH;
   // Double_t k2_t = Eprime;
   /// 3-vectors 
   TVector3 k11(k1_x,k1_y,k1_z);
   TVector3 k22(k2_x,k2_y,k2_z);
   Double_t Mag_k11 = k11.Mag(); 
   Double_t Mag_k22 = k22.Mag();
   TVector3 k11_hat = k11*(1./Mag_k11); 
   TVector3 k22_hat = k22*(1./Mag_k22);  
   TVector3 q_hat   = k11_hat - k22_hat; 

   /// evaluate unit vectors (needed to convert target polarization vector to MAID coordinates)
   /// see J Phys G, Nucl Part Phys 18, 449 (1992), Eq 16  
   TVector3 ez      = q_hat;  
   TVector3 ey      = k11_hat.Cross(k22_hat)*(1./SIN_TH); 
   TVector3 ex      = ey.Cross(ez); 
   /// convert target polarization to MAID coordinates  
   Double_t sx      = fS.Dot(ex);  
   Double_t sy      = fS.Dot(ey);  
   Double_t sz      = fS.Dot(ez);  
   fS_pr.SetXYZ(sx,sy,sz); 

}
//______________________________________________________________________________
Double_t MAIDPolarizedTargetDiffXSec::EvaluateFluxFactor(Double_t Es,Double_t Ep,Double_t th) const{

   Double_t nu    = Es-Ep; 
   Double_t Q2    = insane::Kine::Qsquared(Es,Ep,th);
   Double_t eps   = insane::Kine::epsilon(Es,Ep,th); 
   Double_t K     = EvaluateVirtualPhotonFlux(nu,Q2);  
   Double_t alpha = fine_structure_const; 
   Double_t T1    = alpha/(2.*pi*pi); 
   Double_t T2    = Ep/Es; 
   Double_t T3    = K/Q2; 
   Double_t T4    = 1./(1.-eps); 
   Double_t gamma = T1*T2*T3*T4; 
   return gamma; 

}
//______________________________________________________________________________
Double_t MAIDPolarizedTargetDiffXSec::EvaluateVirtualPhotonFlux(Double_t nu,Double_t Q2) const{

   Double_t K = 0.; 
   Double_t M = M_p/GeV;

   switch(fConvention){
      case 0: // virtual photon energy 
         K = nu; 
         break;
      case 1: // Gilman: use q-vector 
         K = TMath::Sqrt(nu*nu + Q2); 
         break;
      case 2: // Hand: use equivalent energy necessary for the same reaction from a real photon 
         K = nu - Q2/(2.*M*M); 
         break;
      default: 
         std::cout << "[MAIDPolarizedTargetDiffXSec::EvaluateVirtualPhotonFlux]: Invalid convention!  Exiting..." << std::endl;
         exit(1);
   }

   return K; 

}
//______________________________________________________________________________
void MAIDPolarizedTargetDiffXSec::BinarySearch(std::vector<Double_t> *array,Double_t key,Int_t &lowerbound,Int_t &upperbound) const {
   Int_t comparisonCount = 1;    //count the number of comparisons (optional)
   Int_t n               = array->size();
   lowerbound            = 0;
   upperbound            = n-1;

   // To start, find the subscript of the middle position.
   Int_t position = ( lowerbound + upperbound) / 2;

   while((array->at(position) != key) && (lowerbound <= upperbound)){
      comparisonCount++;
      if (array->at(position) > key){
         // decrease position by one.
         upperbound = position - 1;
      }else{
         // Else, increase position by one.
         lowerbound = position + 1;
      }
      position = (lowerbound + upperbound) / 2;
   }

   Double_t lo=0,hi=0,mid;
   Int_t dump = lowerbound;

   if (lowerbound <= upperbound){
      // std::cout << "[BinarySearch]: The number was found in array subscript " << position << std::endl; 
      // std::cout << "                The binary search found the number after " << comparisonCount << " comparisons." << std::endl;             
      // lo  = array[lowerbound];
      // hi  = array[upperbound];
      // mid = array[position]  
      // if(lo==hi){
      lowerbound = position;
      upperbound = position;
      // }
   }else{
      lowerbound = upperbound;
      upperbound = dump;
      // to safeguard against values that are outside the boundaries of the grid 
      if(upperbound>=n){
         upperbound = n-1;
         lowerbound = n-2;
      }
      if(upperbound==0){
         lowerbound = 0;
         upperbound = 1;
      }
      lo   = array->at(lowerbound);
      hi   = array->at(upperbound);
      // std::cout << "[BinarySearch]: Sorry, the number is not in this array.  The binary search made " << comparisonCount << " comparisons." << std::endl;
      // std::cout << "                Target = "         << key << std::endl;
      // std::cout << "                Bounding values: " << std::endl;
      // std::cout << "                low  = "           << lo << std::endl;
      // std::cout << "                high = "           << hi << std::endl;
   }


}
//______________________________________________________________________________
void MAIDPolarizedTargetDiffXSec::ImportData(){

   fFileName = fprefix + Form("target_pol_xsecs_%s.txt",frxn.Data());

   fKineKey = new MAIDPolarizedKinematicKey();

   Double_t ieps,iQ2,iW,itheta,iphi,iSig_0,iA_e,iA_1,iA_2,iA_3,iA_e1,iA_e2,iA_e3; 

   std::ifstream infile(fFileName.Data());
   Int_t N = 0;
   if(infile.fail()){
      std::cout << "[MAIDPolarizedTargetDiffXSec::ImportData]: Cannot open the file: " << fFileName.Data() << std::endl;
      exit(1); 
   }else{
      std::cout << "[MAIDPolarizedTargetDiffXSec::ImportData]: Opening the file: " << fFileName.Data() << std::endl;
      while(!infile.eof()){
         auto * akey = new MAIDPolarizedKinematicKey(N);
         infile >> ieps >> iQ2 >> iW >> itheta >> iphi >> iSig_0 >> iA_e >> iA_1 >> iA_2 >> iA_3 >> iA_e1 >> iA_e2 >> iA_e3; 
         akey->fepsilon  = ieps; 
         akey->fQ2       = iQ2; 
         akey->fW        = iW; 
         akey->fTheta    = itheta; 
         akey->fPhi      = iphi; 
         akey->fSigma_0  = iSig_0;
         akey->fA_e      = iA_e;
         akey->fA_1      = iA_1;
         akey->fA_2      = iA_2;  
         akey->fA_3      = iA_3; 
         akey->fA_e1     = iA_e1; 
         akey->fA_e2     = iA_e2; 
         akey->fA_e3     = iA_e3; 
         fGridData.Add(akey);
         N++;
      }
      infile.close(); 
   }

   std::cout << "sorting... "<< std::endl;
   fGridData.Sort();
   //fGridData.Print();
   std::cout << "done sorting. " << std::endl;

   auto * akey = (MAIDPolarizedKinematicKey*)fGridData.At(0);
   fepsilon.push_back(akey->fepsilon);  
   fW.push_back(akey->fW);  
   fQ2.push_back(akey->fQ2);  
   fTheta.push_back(akey->fTheta);  
   fPhi.push_back(akey->fPhi);  
   for(int i = 0;i<fGridData.GetEntries();i++){
      akey = (MAIDPolarizedKinematicKey*)fGridData.At(i);
      if(fW.back()       != akey->fW)       fW.push_back(akey->fW); 
      if(fQ2.back()      != akey->fQ2)      fQ2.push_back(akey->fQ2); 
      if(fepsilon.back() != akey->fepsilon) fepsilon.push_back(akey->fepsilon); 
      if(fTheta.back()   != akey->fTheta)   fTheta.push_back(akey->fTheta); 
      if(fPhi.back()     != akey->fPhi)     fPhi.push_back(akey->fPhi); 
   }

   // erase repeated entries 
   std::sort( fW.begin(),fW.end() ); 
   fW.erase( std::unique( fW.begin(),fW.end() ),fW.end() ); 
 
   std::sort( fQ2.begin(),fQ2.end() ); 
   fQ2.erase( std::unique( fQ2.begin(),fQ2.end() ),fQ2.end() );  
 
   std::sort( fepsilon.begin(),fepsilon.end() ); 
   fepsilon.erase( std::unique( fepsilon.begin(),fepsilon.end() ),fepsilon.end() );  
  
   std::sort( fTheta.begin(),fTheta.end() ); 
   fTheta.erase( std::unique( fTheta.begin(),fTheta.end() ),fTheta.end() );  
 
   std::sort( fPhi.begin(),fPhi.end() ); 
   fPhi.erase( std::unique( fPhi.begin(),fPhi.end() ),fPhi.end() );  

   // print the size of the grid  
   std::cout << "[MAIDPolarizedTargetDiffXSec::ImportData]: Grid dimensions: " 
             << fepsilon.size() << " x " << fQ2.size()    << " x " 
             << fW.size()       << " x " << fTheta.size() << " x " 
             << fPhi.size()     << std::endl;

   fNumberOfPoints = fW.size(); 

}
//______________________________________________________________________________
void MAIDPolarizedTargetDiffXSec::Print(){
   PrintParameters(); 
   PrintData();
}
//______________________________________________________________________________
void MAIDPolarizedTargetDiffXSec::PrintParameters(){

   std::cout << "-------------------- MAID Parameters --------------------" << std::endl; 
   std::cout << "Reaction channel (pion,nucleon) = " << "(" << fPion << "," << fNucleon << ")" << std::endl; 
   std::cout << "Virtual photon flux convention  = " << fConvention << " (" << fConventionName << ")" << std::endl;
   std::cout << "Number of data points           = " << fNumberOfPoints     << std::endl;
   std::cout << "---------------------------------------------------------" << std::endl; 

}
//______________________________________________________________________________
void MAIDPolarizedTargetDiffXSec::PrintData(){

   std::cout << "-------------------- MAID Data --------------------" << std::endl; 
   if(fNumberOfPoints==0){
      std::cout << "[MAIDPolarizedTargetDiffXSec::PrintData]: No data!  Exiting..." << std::endl;
      exit(1); 
   }
}

//______________________________________________________________________________
void MAIDPolarizedTargetDiffXSec::Clear(){

   fQ2.clear();
   fW.clear();
   fEprime.clear();
   fTheta.clear();
   fPhi.clear();
   fepsilon.clear();
   fNu.clear(); 
   // fGamma.clear(); 
   fSigma_0.clear(); 
   fA_e.clear();
   fA_1.clear();
   fA_2.clear();
   fA_3.clear();
   fA_e1.clear();
   fA_e2.clear();
   fA_e3.clear();
   
}

//______________________________________________________________________________
}}
