#include "insane/xsections/MAIDInclusivePionDiffXSec.h"

namespace insane {
namespace physics {
MAIDInclusivePionDiffXSec::MAIDInclusivePionDiffXSec(
      const char * nucleon, 
      const char * pion)
{
   fID         = 1020;
   SetTitle("MAIDInclusivePionDiffXSec");
   fLabel      = " #frac{d#sigma}{d#omega_{#pi}d#Omega_{#pi}} ";
   fUnits      = "nb/GeV-str";
   fXSec0 = nullptr;
   fReturnZero = false; // used if nucleon and pion arguments are not correct.

   if( !strcmp("n",nucleon) ) {
      // Neutron target
      fTargetNucleus = Nucleus::Neutron();
      // possible reactions:
      if( !strcmp("piminus",pion)  ){
         //  e + neutron  >> e' + pi- + proton
         SetPlotTitle("MAID #vec{n}(#vec{e},#pi^{-})e'p");
         fXSec0 = new MAIDExclusivePionDiffXSec3("piminus","p");
      } else {
         //  e + neutron  >> e' + pi0 + neutron 
         SetPlotTitle("MAID #vec{n}(#vec{e},#pi^{0})e'n");
         fXSec0 = new MAIDExclusivePionDiffXSec3("pi0","n");
      } 
   } else {
      // Proton target
      fTargetNucleus = Nucleus::Proton();
      // possible reactions:
      if( !strcmp("piplus",pion)  ){
         //  e + proton  >> e' + pi+ + neutron
         SetPlotTitle("MAID #vec{p}(#vec{e},#pi^{+})e'n");
         fXSec0 = new MAIDExclusivePionDiffXSec3("piplus","n");
      } else {
         //  e + proton  >> e' + pi0 + proton 
         SetPlotTitle("MAID #vec{p}(#vec{e},#pi^{0})e'p");
         fXSec0 = new MAIDExclusivePionDiffXSec3("pi0","p");
      } 
   }
   // set to pi0 p if arguments don't work out right
   if(!fXSec0) {
      SetPlotTitle("MAID #vec{p}(#vec{e},#pi^{0})e'p");
      fXSec0 = new MAIDExclusivePionDiffXSec3("pi0","p");
      Warning("MAIDInclusivePionDiffXSec","Nucleon and Pion combination will return zero!");
      fReturnZero = true;
   }
   fXSec0->UsePhaseSpace(false);
   fXSec0->SetIncludeJacobian(true);
   fSig0.fXSec = fXSec0;

   // Integration options
   //fIntType = ROOT::Math::IntegrationMultiDim::kMISER;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   fIntType = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   fAbsErr                           = 1.0e0;    // desired absolute Error 
   fRelErr                           = 1.0e-6;   // desired relative Error 
   fNcalls                           = 1e3;      // number of calls (MC only)
   fRule                             = 1;        // Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    
   fnDim = 3;
}

//________________________________________________________________________________
MAIDInclusivePionDiffXSec::~MAIDInclusivePionDiffXSec(){
   if(fXSec0) delete fXSec0;
}
//______________________________________________________________________________
double MAIDInclusivePionDiffXSec::MAIDXSec_5Fold_2D_integrand::DoEval(const double* x) const {
   using namespace TMath;

   Double_t w  = fE_pi;
   //fArgs[0]            E' Calculated below  
   fArgs[1]    = x[0];// theta_e
   fArgs[2]    = x[1];// phi_e
   //fArgs[3]            theta_pi  already set
   //fArgs[4]            phi_pi    already set

   Double_t Mpi         = fXSec->fM_pi;
   Double_t M1          = fXSec->fM_1;
   Double_t M2          = fXSec->fM_2;
   Double_t E1          = fXSec->GetBeamEnergy();
   Double_t cosThetaPi  = Cos(fArgs[3]);
   Double_t sinThetaPi  = Sin(fArgs[3]);
   Double_t cosThetaE   = Cos(fArgs[1]);
   Double_t sinThetaE   = Sin(fArgs[1]);
   Double_t sin2the     = Power(Sin(fArgs[1]/2.0),2.0);
   Double_t cosDeltaPhi = Cos(fArgs[2]-fArgs[4]);
   Double_t E2          = (Power(M1,2.) - Power(M2,2.) + Power(Mpi,2.) - 2.*M1*w + 2.*E1*(M1 - w + cosThetaPi*Sqrt(Power(Mpi,2) + Power(w,2.))))/
                          (2.*(M1 + 2.*E1*sin2the - w + cosThetaE*cosThetaPi*Sqrt(Power(Mpi,2) + Power(w,2)) - cosDeltaPhi*sinThetaE*sinThetaPi*Sqrt(Power(Mpi,2) + Power(w,2))));
   Double_t dEdw        = Abs((-2.*cosThetaPi*((-1. + cosThetaE)*E1 + cosThetaE*M1)*Power(Mpi,2.) + 
                          cosThetaPi*(-2.*(-1. + cosThetaE)*E1*M1 - cosThetaE*(Power(M1,2.) - Power(M2,2.) + Power(Mpi,2.)) + 4*Power(E1,2.)*sin2the)*w + 
                          cosDeltaPhi*sinThetaE*sinThetaPi*(2.*(E1 + M1)*Power(Mpi,2.) + (2.*E1*M1 + Power(M1,2.) - Power(M2,2.) + Power(Mpi,2.))*w) + 
                          (-Power(M1,2.) - Power(M2,2.) + Power(Mpi,2) - 4.*E1*(E1 + M1)*sin2the)*Sqrt(Power(Mpi,2.) + Power(w,2.)))/
                            (Sqrt(Power(Mpi,2.) + Power(w,2.))*Power(M1 + 2.*E1*sin2the - w + 
                                                              (cosThetaE*cosThetaPi - cosDeltaPhi*sinThetaE*sinThetaPi)*Sqrt(Power(Mpi,2.) + Power(w,2.)),2.)))/2.;
   fArgs[0]             = E2;

   Double_t res         = fXSec->EvaluateXSec(fXSec->GetDependentVariables(fArgs));
   if(res<=0.0 || TMath::IsNaN(res) ){ return 0.0; }
   res *= dEdw;
   //res *= TMath::Sin(fArgs[3]);
   //std::cout << " E0          = " << E1 << std::endl;
   //std::cout << " E'          = " << fArgs[0] << std::endl;
   //std::cout << " dEdw        = " << dEdw << std::endl;
   //std::cout << " w           = " << w << std::endl;
   //std::cout << " theta_e     = " << fArgs[1]/degree << std::endl;
   //std::cout << " phi_e       = " << fArgs[2]/degree << std::endl;
   //std::cout << " theta_pi     = " << fArgs[3]/degree << std::endl;
   //std::cout << " phi_pi       = " << fArgs[4]/degree << std::endl;
   //std::cout << " res(5-fold)  = " << res << std::endl;
   if(res<=0.0 || TMath::IsNaN(res) ){ return 0.0; }
   return res;
}
//________________________________________________________________________________
Double_t   MAIDInclusivePionDiffXSec::EvaluateXSec(const Double_t *x) const {
   if(fReturnZero) return 0.0;
   //if (!VariablesInPhaseSpace(GetPhaseSpace()->GetDimension(), x)) return(0.0);

   fSig0.fE_pi    = x[0]; // E_pi
   //fSig0.fArgs[0] = x[0]; // E_e
   //fSig0.fArgs[1] = x[1]; // theta_e
   //fSig0.fArgs[2] = x[2]; // phi_e
   fSig0.fArgs[3] = x[1]; // theta_pi
   fSig0.fArgs[4] = x[2]; // phi_pi

   Double_t min[4] = {0.0        , 0.0,            0.0,         0.0};
   Double_t max[4] = {TMath::Pi(), 2.0*TMath::Pi(),TMath::Pi(), 2.0*TMath::Pi()};
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kMISER;
   ROOT::Math::IntegratorMultiDim ig2(fIntType,fAbsErr,fRelErr,fNcalls); 
   //ig2.SetRelTolerance(relErr);
  
   ig2.SetFunction(fSig0);
   Double_t res       = ig2.Integral(min,max); 

   //ig2.SetFunction(fSig1);
   //res               += ig2.Integral(min,max); 

   std::cout << " res = " << res << std::endl;
   if(!IncludeJacobian()) return res/TMath::Sin(x[1]);
   return(res);
}

//________________________________________________________________________________
Double_t * MAIDInclusivePionDiffXSec::GetDependentVariables(const Double_t * x) const {
   fDependentVariables[0] = x[0];
   fDependentVariables[1] = x[1];
   fDependentVariables[2] = x[2];
   return(fDependentVariables);
}

//________________________________________________________________________________
void       MAIDInclusivePionDiffXSec::InitializePhaseSpaceVariables(){
   if(fXSec0)fXSec0->InitializePhaseSpaceVariables();
   InclusiveDiffXSec::InitializePhaseSpaceVariables();
}

//________________________________________________________________________________
}}
