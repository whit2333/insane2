#include "insane/xsections/IncoherentDISXSec.h"
#include "insane/base/PhysicalConstants.h"
#include "TMath.h"
#include "insane/formfactors/FormFactors.h"
#include "TVector3.h"
#include "insane/nuclear/FermiMomentumDist.h"
#include "insane/xsections/CompositeDiffXSec2.h"
#include <cmath>

using namespace insane::physics;

IncoherentDISXSec::IncoherentDISXSec()
{
   fID            = 201100000;
   SetTitle("IncoherentDISXSec");
   SetPlotTitle("Incoherent DIS cross-section");
   fLabel         = "#frac{d#sigma}{dt dx dQ^{2} d#phi }";
   fUnits         = "nb/sr";
   fnDim          = 6; // 3 for DIS on the nucleon + 3 for nucleon fermi momentum
   fnParticles    = 2;
   fPIDs.clear();
   fPIDs.push_back(11);
   fPIDs.push_back(1000020030);

   fActiveXSec  = new CompositeDiffXSec2();
   //fActiveXSec  = new InclusiveDiffXSec();
   //fActiveXSec  = new InelasticRadiativeTail2();
   //((InelasticRadiativeTail*)fActiveXSec)->GetBornXSec()->SetTargetNucleus(Nucleus::Proton());
   fActiveTargetFragment = Nucleus::Proton();
   fActiveXSec->SetTargetNucleus( fActiveTargetFragment );
   fActiveXSec->UsePhaseSpace(false);
   //((InelasticRadiativeTail*)fActiveXSec)->SetTargetThickness(0.046);

}
//______________________________________________________________________________

IncoherentDISXSec::~IncoherentDISXSec()
{ }
//______________________________________________________________________________

void IncoherentDISXSec::SetRecoilNucleus(const Nucleus& spectator)
{
   Nucleus target     = GetTargetNucleus();
   fSpectatorTargetFragment = spectator;
   fActiveTargetFragment    = target - fSpectatorTargetFragment;
   fActiveXSec->SetTargetNucleus(fActiveTargetFragment);

   fPIDs[1] = fSpectatorTargetFragment.GetPdgCode();

   std::cout << "Target:" << std::endl;
   target.Print();

   std::cout << "Active:" << std::endl;
   fActiveTargetFragment.Print();

   std::cout << "Spectator:" << std::endl;
   fSpectatorTargetFragment.Print();
}
//______________________________________________________________________________

void IncoherentDISXSec::SetRecoilNucleus(const Nucleus& spectator, const Nucleus& active)
{
   Nucleus target     = spectator + active;
   DiffXSec::SetTargetNucleus(target);
   fSpectatorTargetFragment = spectator;
   fActiveTargetFragment    = active;
   fActiveXSec->SetTargetNucleus(fActiveTargetFragment);

   fPIDs[1] = fSpectatorTargetFragment.GetPdgCode();

   std::cout << "Target:" << std::endl;
   target.Print();

   std::cout << "Active:" << std::endl;
   fActiveTargetFragment.Print();

   std::cout << "Spectator:" << std::endl;
   fSpectatorTargetFragment.Print();
}
//______________________________________________________________________________

void IncoherentDISXSec::SetTargetNucleus(const Nucleus& target)
{
   DiffXSec::SetTargetNucleus( target );
   SetRecoilNucleus( fSpectatorTargetFragment );
}
//______________________________________________________________________________

void IncoherentDISXSec::InitializePhaseSpaceVariables()
{

   using namespace insane::units;
   PhaseSpace * ps = GetPhaseSpace();

   // ------------------------------
   // Electron
   auto * varEnergy = new PhaseSpaceVariable("energy_e", "E_{e'}",0.5, GetBeamEnergy());
   varEnergy->SetParticleIndex(0);
   varEnergy->SetDependent(true);
   ps->AddVariable(varEnergy);

   auto * varTheta = new PhaseSpaceVariable("theta_e", "#theta_{e'}",2.5*degree, 180.0*degree);
   varTheta->SetParticleIndex(0);
   varTheta->SetDependent(true);
   ps->AddVariable(varTheta);

   auto * varPhi = new PhaseSpaceVariable("phi_e", "#phi_{e'}",-180.0*degree, 180.0*degree);
   varPhi->SetParticleIndex(0);
   varPhi->SetDependent(true); // calculated from the phi below
   ps->AddVariable(varPhi);

   // ------------------------------
   // Recoil A-1
   auto * varP_p2 = new PhaseSpaceVariable("P_p2", "P_{p2}",0.0,2.0);
   varP_p2->SetParticleIndex(1);
   varP_p2->SetDependent(true);
   ps->AddVariable(varP_p2);

   auto * varTheta_p2 = new PhaseSpaceVariable("theta_p2", "#theta_{p2}",0.0*degree, 180.0*degree);
   varTheta_p2->SetParticleIndex(1);
   varTheta_p2->SetDependent(true);
   ps->AddVariable(varTheta_p2);

   auto * varPhi_p2 = new PhaseSpaceVariable("phi_p2", "#phi_{p2}", -1.0*TMath::Pi(), 1.0*TMath::Pi() );
   varPhi_p2->SetParticleIndex(1);
   varPhi_p2->SetDependent(true);
   //varPhi_p2->SetInverted(true);
   ps->AddVariable(varPhi_p2);

   // ------------------------------
   //
   auto * var_x = new PhaseSpaceVariable("x", "x",0.001, 1.0);
   var_x->SetParticleIndex(-1);
   var_x->SetDependent(true); // calculated from electron energy/angles
   ps->AddVariable(var_x);

   auto * var_Q2 = new PhaseSpaceVariable("Q2", "Q^{2}",0.001, 10.0);
   var_Q2->SetParticleIndex(-1);
   var_Q2->SetDependent(true); // calculated from electron energy/angles
   ps->AddVariable(var_Q2);

   auto * var_E0rest = new PhaseSpaceVariable("k1rest", "k_{1}^{rest}",0.001, 13.0);
   var_E0rest->SetParticleIndex(-1);
   var_E0rest->SetDependent(true); // calculated from electron energy/angles
   ps->AddVariable(var_E0rest);

   auto * var_y = new PhaseSpaceVariable("y", "y",0.0, 1.0);
   var_y->SetParticleIndex(-1);
   var_y->SetDependent(true); // calculated from electron energy/angles
   ps->AddVariable(var_y);

   // ------------------------------
   // electron variables in the nucleon rest frame 
   auto * varP_k2rest = new PhaseSpaceVariable("energy_k2rest", "E_{k2rest}",0.5, GetBeamEnergy()+1.0);
   varP_k2rest->SetParticleIndex(-1);
   ps->AddVariable(varP_k2rest);

   auto * varTheta_k2rest = new PhaseSpaceVariable("theta_k2rest", "#theta_{k2rest}",1.0*degree, 180.0*degree);
   varTheta_k2rest->SetParticleIndex(-1);
   ps->AddVariable(varTheta_k2rest);

   auto * varPhi_k2rest = new PhaseSpaceVariable("phi_k2rest", "#phi_{k2rest}", -1.0*TMath::Pi(), 1.0*TMath::Pi() );
   varPhi_k2rest->SetParticleIndex(-1);
   varPhi_k2rest->SetUniform(true);
   ps->AddVariable(varPhi_k2rest);

   // ------------------------------
   // Initial nucleon
   auto * varP_p1 = new PhaseSpaceVariable("P_p1", "P_{p1}",0.0,2.0);
   varP_p1->SetParticleIndex(-1);
   ps->AddVariable(varP_p1);

   auto * varTheta_p1 = new PhaseSpaceVariable("theta_p1", "#theta_{p1}",0.0*degree, 180.0*degree);
   varTheta_p1->SetParticleIndex(-1);
   varTheta_p1->SetUniform(true);
   ps->AddVariable(varTheta_p1);

   auto * varPhi_p1 = new PhaseSpaceVariable("phi_p1", "#phi_{p1}", -1.0*TMath::Pi(), 1.0*TMath::Pi() );
   varPhi_p1->SetParticleIndex(-1);
   varPhi_p1->SetUniform(true);
   ps->AddVariable(varPhi_p1);

   // ------------------------------
   //
   SetPhaseSpace(ps);

   // ------------------------------
   //
   fActiveXSec->InitializePhaseSpaceVariables();
   fActiveXSec->InitializeFinalStateParticles();
   fActiveXSec->Print();
}
//______________________________________________________________________________

void IncoherentDISXSec::DefineEvent(Double_t * vars)
{
   // Virtual method to define the random event from the variables provided.
   // This is the transition point between the phase space variables and
   // the particles. The argument should be the full list of variables returned from
   // GetDependentVariables. 

   Int_t totvars = 0;
   for(int i = 0; i < 1; i++) {
      /// \todo fix this hard coding of 3 variables per event.
      /// here we are assuming the order E,theta,phi,then others
      /// \todo figure out how to handle vertex.
      insane::Kine::SetMomFromEThetaPhi((TParticle*)(fParticles.At(i)), &vars[totvars]);
      //((TParticle*)fParticles.At(i))->SetProductionVertex(GetRandomVertex());
      totvars += GetNParticleVars(i);
   }
   for(int i = 1; i < 2; i++) {
      /// \todo fix this hard coding of 3 variables per event.
      /// here we are assuming the order E,theta,phi,then others
      /// \todo figure out how to handle vertex.
      if( i==1 ) {
         insane::Kine::SetEFromMomThetaPhi((TParticle*)(fParticles.At(i)), &vars[totvars]);
      } else {
         insane::Kine::SetMomFromEThetaPhi((TParticle*)(fParticles.At(i)), &vars[totvars]);
      }
      //((TParticle*)fParticles.At(i))->SetProductionVertex(GetRandomVertex());
      totvars += GetNParticleVars(i);
   }

}
//______________________________________________________________________________

Double_t * IncoherentDISXSec::GetDependentVariables(const Double_t * x) const
{
   using namespace TMath;

   fKine.fR_phi.SetToIdentity();
   fKine.fR_k1.SetToIdentity();
   fKine.fR_q1.SetToIdentity();

   // electron variables start in the nucleon rest frame
   double E_e_rest   = x[0];
   double th_e_rest  = x[1];
   double phi_e_rest = x[2]; // uniform phi in nucleon rest frame where ( \vec{k1} = k1 \vec{z} )

   double P_p1     = x[3];
   double theta_p1 = x[4];
   double phi_p1   = x[5];

   //std::cout << " ============================================ " << std::endl;
   //std::cout << "   E_e_rest  = " <<   E_e_rest << std::endl;
   //std::cout << "  th_e_rest  = " <<  th_e_rest << std::endl;
   //std::cout << " phi_e_rest  = " << phi_e_rest << std::endl;

   double M        = 0.938;
   double E_p1     = Sqrt(P_p1*P_p1 + M*M);

   // --------------------------------------
   // Set the 4 vectors in the lab
   fKine.fk1 = {0, 0, fBeamEnergy, fBeamEnergy };

   fKine.fp1 = {0, 0, P_p1, E_p1 };
   fKine.fp1.SetTheta(theta_p1);
   fKine.fp1.SetPhi(  phi_p1);

   // --------------------------------------
   // Calculate kinematic variables
   double s   = (fKine.fp1 + fKine.fk1)*(fKine.fp1 + fKine.fk1);

   // --------------------------------------
   // Kinematics in the nucleon rest frame 
   double E0_rest = (s-M*M)/(2.0*M);

   // --------------------------------------
   // Rotate p1 to phi = 0
   fKine.fR_phi.SetToIdentity();
   fKine.fR_phi.RotateZ(-phi_p1);

   fKine.fp1 = TLorentzRotation(fKine.fR_phi)*(fKine.fp1);
   fKine.fk1 = TLorentzRotation(fKine.fR_phi)*(fKine.fk1);

   // --------------------------------------
   // Boost to p1 rest frame
   fKine.fLambda_p1 = TLorentzRotation(-1.0*(fKine.fp1.BoostVector()));

   fKine.fp1        = (fKine.fLambda_p1)*(fKine.fp1);
   fKine.fk1        = (fKine.fLambda_p1)*(fKine.fk1);

   // --------------------------------------
   // Rotate k1 to theta = 0
   fKine.fR_k1.SetToIdentity();
   fKine.fR_k1.RotateY(fKine.fk1.Theta());

   fKine.fp1 = TLorentzRotation(fKine.fR_k1)*(fKine.fp1);
   fKine.fk1 = TLorentzRotation(fKine.fR_k1)*(fKine.fk1);

   //std::cout << "p1 (rest) :"; fKine.fp1.Print();

   // --------------------------------------
   // Caclulate the (DIS) k2 kinematics
   fKine.fk2 = {0, 0, E_e_rest, E_e_rest };
   fKine.fk2.SetTheta( th_e_rest  );
   fKine.fk2.SetPhi(   phi_e_rest );

   fKine.fq1 = fKine.fk1 - fKine.fk2;

   double Q2        = (fKine.fq1)*(fKine.fq1);
   double xbj       = Q2/(2.0*M*fKine.fq1.E());
   double E0_A      = fKine.fk1.E();
   double nu_A      = fKine.fq1.E();
   double y         = nu_A/E0_A;
   double eprime_A  = E0_A - nu_A;
   double theta_e_A = th_e_rest;
   double phi_e_A   = phi_e_rest;
   //std::cout << E0_rest - E0_A << std::endl;
   if( y > 1.0 ) {
      return nullptr;
   }
   if( std::isnan(eprime_A) ) {
      //std::cout << " Error: eprime_A is NaN " << std::endl;
      return nullptr;
   }
   //std::cout << " k2 :"; fKine.fk2.Print();

   // --------------------------------------
   //
   TLorentzRotation lambda_inv = 
      TLorentzRotation(fKine.fR_phi.Inverse())*
      (fKine.fLambda_p1.Inverse())*
      TLorentzRotation(fKine.fR_k1.Inverse());

   fKine.fk1 = (lambda_inv)*(fKine.fk1);
   fKine.fk2 = (lambda_inv)*(fKine.fk2);
   fKine.fq1 = (lambda_inv)*(fKine.fq1);
   fKine.fp1 = (lambda_inv)*(fKine.fp1);
   //fKine.fq2 = (lambda_inv)*(fKine.fq2);
   //fKine.fp2 = (lambda_inv)*(fKine.fp2);
   

   // --------------------------------------
   // Set the recoil
   fKine.fp2 = fKine.fp1;
   //TVector3 p2_recoil = fKine.fp2.Vect();
   fKine.fp2.SetVect( -1.0*(fKine.fp2.Vect()) );

   // --------------------------------------
   //
   //std::cout << "k2 (final) : ";  fKine.fk2.Print();
   //std::cout << "k2 (final) : ";  fKine.fp1.Print();

   fDependentVariables[0]  = fKine.fk2.E();
   fDependentVariables[1]  = fKine.fk2.Theta();
   fDependentVariables[2]  = fKine.fk2.Phi();

   fDependentVariables[3]  = fKine.fp2.Vect().Mag();
   fDependentVariables[4]  = fKine.fp2.Theta();
   fDependentVariables[5]  = fKine.fp2.Phi();

   fDependentVariables[6] = xbj;
   fDependentVariables[7] = Q2;
   fDependentVariables[8] = E0_rest;
   fDependentVariables[9] = y;

   fDependentVariables[10] = x[0];
   fDependentVariables[11] = x[1];
   fDependentVariables[12] = x[2];

   fDependentVariables[13] = x[3];
   fDependentVariables[14] = x[4];
   fDependentVariables[15] = x[5];

   return(fDependentVariables);
}
//______________________________________________________________________________

Double_t  IncoherentDISXSec::EvaluateXSec(const Double_t * x) const
{
   if( (!x) || (x == nullptr) ) {
      //std::cout << " bad vars \n";
      return(0.0);
   }
   if( !VariablesInPhaseSpace(6, x) ) return(0.0);

   const double* vars_e  = &(x[10]);
   double E1       = x[0];
   double theta    = x[1];
   double phi      = x[2];
   double P_p1     = x[3];
   double th_p1    = x[4];
   double phi_p1   = x[5];

   double xBjorken = x[6];
   double Q2       = x[7];
   double E0       = x[8];
   double y        = x[9];

   //double E_k2_rest     = x[10];
   //double th_k2_rest    = x[7];
   //double phi_k2_rest   = x[8];


   //std::array<double,3> e_vars = {{ x[0], x[1], x[2] }};

   fActiveXSec->SetBeamEnergy(E0);
   double xsec = fActiveXSec->EvaluateXSec(fActiveXSec->GetDependentVariables(vars_e));
   //double xsec = fActiveXSec->EvaluateXSec(x);
   //std::cout << "xsec = " << xsec  << std::endl;

   double norm    = 1.0;
   double prob_p1 = fFermiDist.GetProb(P_p1,fTargetNucleus.GetA());
   double res     = norm*prob_p1*xsec;

   if( std::isnan( res ) ) {
      return 0.0;
   }
   //Double_t Eprime = x[0];
   //Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
   //Double_t Qsquared = 4.0 * Eprime * GetBeamEnergy() * TMath::Power(TMath::Sin(theta / 2.0), 2);
   //Double_t tau = Qsquared / (4.0 * M * M);
   //Double_t mottXSec = (1. / 137.) * (1. / 137.) *
   //   TMath::Power(TMath::Cos(theta / 2.0), 2) /
   //   (4.0 * GetBeamEnergy() * GetBeamEnergy() * TMath::Power(TMath::Sin(theta / 2.0), 4))
   //   * (Eprime / GetBeamEnergy());
   //Double_t GE2 = TMath::Power(fFormFactors->GEp(Qsquared), 2);
   //Double_t GM2 = TMath::Power(fFormFactors->GMp(Qsquared), 2);

   ////std::cout << " Qsq= " << Qsquared;
   ////std::cout << " tau= " << tau;
   ////std::cout << " GE2= " << GE2;
   ////std::cout << " GM2= " << GM2  << "\n";;

   //Double_t res = mottXSec * ((GE2 + tau * GM2) / (1.0 + tau) + 2.0 * tau * GM2 * TMath::Power(TMath::Tan(theta / 2.0), 2));
   //res = res * hbarc2_gev_nb;

   if( IncludeJacobian() ) return( TMath::Sin(theta)*res);
   return(res);
}
//______________________________________________________________________________



