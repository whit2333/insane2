#include "insane/xsections/InclusiveBornDISXSec.h"
#include <cmath>


namespace insane {
  namespace physics {



    InclusiveBornDISXSec::InclusiveBornDISXSec()
    {
      fID        = 100000010;
      fTitle     = "InclusiveBornDISXSec";
      fPlotTitle = "#frac{d#sigma}{dE'd#Omega} nb/GeV-sr";
    }
    //______________________________________________________________________________

    InclusiveBornDISXSec::~InclusiveBornDISXSec()
    {
    }
    //______________________________________________________________________________
    
    InclusiveBornDISXSec::InclusiveBornDISXSec(const InclusiveBornDISXSec& rhs) : InclusiveDiffXSec(rhs)
    {
      (*this) = rhs; 
    }
    //______________________________________________________________________________

    InclusiveBornDISXSec& InclusiveBornDISXSec::operator=(const InclusiveBornDISXSec& rhs)
    {
      if (this != &rhs) {  // make sure not same object
        InclusiveDiffXSec::operator=(rhs);
      }
      return *this;

    }
    //______________________________________________________________________________

    InclusiveBornDISXSec*  InclusiveBornDISXSec::Clone(const char * newname) const
    {
      std::cout << "InclusiveBornDISXSec::Clone()\n";
      auto * copy = new InclusiveBornDISXSec();
      (*copy) = (*this);
      return copy;
    }
    //______________________________________________________________________________

    InclusiveBornDISXSec*  InclusiveBornDISXSec::Clone() const 
    {
      return( Clone("") ); 
    }
    //______________________________________________________________________________

    Double_t InclusiveBornDISXSec::EvaluateXSec(const Double_t *x) const
    {
      if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

      Double_t M    = M_p/GeV;            // in GeV 
      Double_t Ep   = x[0];               // scattered electron energy in GeV 
      Double_t th   = x[1];               // electron scattering angle in radians 
      Double_t Nu   = fBeamEnergy - Ep;
      if(Nu <0.0) return 0.0;
      Double_t SIN  = TMath::Sin(th/2.);
      Double_t SIN2 = SIN*SIN;
      Double_t COS2 = 1. - SIN2; 
      Double_t TAN2 = SIN2/COS2; 
      Double_t Q2   = 4.0*fBeamEnergy*Ep*SIN2;
      Double_t xBj  = Q2/(2.0*M*Nu);

      Double_t A    = GetA();
      Double_t Z    = GetZ();

      // Calculate structure functions 
      Double_t F1 = 0.0;
      Double_t F2 = 0.0;
      if( (A==1)&&(Z==1) ){
        // proton, N = A-Z = 0  
        //std::cout << "Test" <<std::endl;
        F1 = fStructureFunctions->F1p(xBj,Q2); 
        F2 = fStructureFunctions->F2p(xBj,Q2); 
      }else if( (A==1)&&(Z==0) ){
        // neutron, N = A-Z = 1
        F1 = fStructureFunctions->F1n(xBj,Q2); 
        F2 = fStructureFunctions->F2n(xBj,Q2); 
      }else if( (A==2)&&(Z==1) ){
        // Remember that the StructureFunctions return results PER NUCLEON 
        // So we multiply by A here
        // deuteron, N = A-Z = 1
        F1 = A*fStructureFunctions->F1d(xBj,Q2); 
        F2 = A*fStructureFunctions->F2d(xBj,Q2); 
      }else if( (A==3)&&(Z==2) ){
        // 3He, N = A-Z = 1
        F1 = A*fStructureFunctions->F1He3(xBj,Q2); 
        F2 = A*fStructureFunctions->F2He3(xBj,Q2); 
      } else {
        // These SFs are not per nucleon
        F1 = fStructureFunctions->F1Nuclear(xBj,Q2,Z,A);
        F2 = fStructureFunctions->F2Nuclear(xBj,Q2,Z,A);
        //std::cout << "F2(" << Ep << ") = " << F2 <<std::endl;
      }
      Double_t W1 = (1./M)*F1;
      Double_t W2 = (1./Nu)*F2;
      // compute the Mott cross section (units = mb): 
      //Double_t hbarc2 = 0.38939129; // (hbar*c)^2 = 0.38939129 mb*GeV^2  
      Double_t alpha  = 1./137.;
      Double_t num    = alpha*alpha*COS2; 
      Double_t den    = 4.*fBeamEnergy*fBeamEnergy*SIN2*SIN2; 
      Double_t MottXS = num/den;
      // compute the full cross section (units = nb/GeV/sr) 
      Double_t fullXsec = MottXS*(W2 + 2.0*TAN2*W1)*hbarc2_gev_nb;

      if( TMath::IsNaN(fullXsec) || fullXsec < 0.0 ) return 0.0;
      if( std::isinf(fullXsec) ) return 0.0;


      if ( IncludeJacobian() ) return fullXsec*TMath::Sin(th);
      return fullXsec;
    }
    //______________________________________________________________________________
 
    //void xs_transform_test(){
    //  //TransformedDiffXSec<InclusiveDiffXSec, 
    //}

  }
}

