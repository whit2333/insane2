#include "insane/xsections/ElectroProductionXSec.h"

namespace insane {
namespace physics {

//==============================================================================

ElectroProductionXSec::ElectroProductionXSec()
{ 
   fID         = 1180;
   fnDim = 6;
   fnParticles = 2;
   fMomentum_pi = nullptr;
   fTheta_pi = nullptr;
   fPhi_pi = nullptr;
}
//______________________________________________________________________________

ElectroProductionXSec::ElectroProductionXSec(const ElectroProductionXSec& rhs) : InclusiveDiffXSec(rhs) 
{
   (*this) = rhs;
}
//______________________________________________________________________________

ElectroProductionXSec& ElectroProductionXSec::operator=(const ElectroProductionXSec& rhs)
{
   if (this != &rhs) {  // make sure not same object
      InclusiveDiffXSec::operator=(rhs);
   }
   return *this;    // Return ref for multiple assignment
}
//______________________________________________________________________________

ElectroProductionXSec::~ElectroProductionXSec()
{ }
//______________________________________________________________________________

ElectroProductionXSec* ElectroProductionXSec::Clone(const char * newname) const 
{
   std::cout << "ElectroProductionXSec::Clone()\n";
   auto * copy = new ElectroProductionXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

ElectroProductionXSec* ElectroProductionXSec::Clone() const
{ 
   return( Clone("") );
} 
//______________________________________________________________________________

void ElectroProductionXSec::InitializePhaseSpaceVariables()
{
   // eN--> gamma* N --> e' P X 
   auto * ps = GetPhaseSpace();

   /// Electron variables
   auto * varEnergy = new PhaseSpaceVariable();
   varEnergy = new PhaseSpaceVariable();
   varEnergy->SetNameTitle("energy_e", "E_{e'}");
   varEnergy->SetMinimum(1.0); //GeV
   varEnergy->SetMaximum(1.2); //GeV
   ps->AddVariable(varEnergy);

   auto *   varTheta = new PhaseSpaceVariable();
   varTheta->SetNameTitle("theta_e", "#theta_{e'}"); // ROOT string latex
   varTheta->SetMinimum(20.0 * TMath::Pi() / 180.0); //
   varTheta->SetMaximum(50.0 * TMath::Pi() / 180.0); //
   ps->AddVariable(varTheta);

   auto *   varPhi = new PhaseSpaceVariable();
   varPhi->SetNameTitle("phi_e", "#phi_{e'}"); // ROOT string latex
   varPhi->SetMinimum(179.0 * TMath::Pi() / 180.0); //
   varPhi->SetMaximum(180.0 * TMath::Pi() / 180.0); //
   ps->AddVariable(varPhi);

   /// Production particle variables
   auto * varEnergy2 = new PhaseSpaceVariable();
   varEnergy2 = new PhaseSpaceVariable();
   varEnergy2->SetNameTitle("momentum_pi", "P_{#pi}");
   varEnergy2->SetMinimum(0.01); //GeV
   varEnergy2->SetMaximum(4.9); //GeV
   varEnergy2->SetParticleIndex(1);
   ps->AddVariable(varEnergy2);

   auto *   varTheta2 = new PhaseSpaceVariable();
   varTheta2->SetNameTitle("theta_pi", "#theta_{#pi}"); // ROOT string latex
   varTheta2->SetMinimum(20.0 * TMath::Pi() / 180.0); //
   varTheta2->SetMaximum(50.0 * TMath::Pi() / 180.0); //
   varTheta2->SetParticleIndex(1);
   ps->AddVariable(varTheta2);

   auto *   varPhi2 = new PhaseSpaceVariable();
   varPhi2->SetNameTitle("phi_pi", "#phi_{#pi}"); // ROOT string latex
   varPhi2->SetMinimum(-50.0 * TMath::Pi() / 180.0); //
   varPhi2->SetMaximum(50.0 * TMath::Pi() / 180.0); //
   varPhi2->SetParticleIndex(1);
   ps->AddVariable(varPhi2);

}
//______________________________________________________________________________

Double_t ElectroProductionXSec::GetOneOverEpsilon(Double_t energy, Double_t theta)
{
   /** \f$ 1/\epsilon = 1+ 2(1+\frac{\nu^{2}}{Q^{2}})tan^2(\theta/2) \f$ */
   return(1.0 +
         2.0 * (1.0 + GetPhotonEnergy(energy, theta) * GetPhotonEnergy(energy, theta) / GetQSquared(energy, theta))
         * TMath::Power(TMath::Tan(theta / 2.0), 2));
}
//______________________________________________________________________________

Double_t ElectroProductionXSec::GetEpsilon(Double_t energy, Double_t theta)
{
   /** \f$ \epsilon = 1/( 1+ 2(1+\frac{\nu^{2}}{Q^{2}})tan^2(\theta/2) ) \f$ */
   return(1.0 / GetOneOverEpsilon(energy, theta));
}
//______________________________________________________________________________

Double_t ElectroProductionXSec::GetHandFluxFactor(Double_t energy, Double_t theta)
{
   return(GetPhotonEnergy(energy, theta) - GetQSquared(energy, theta) / (2.0 * M_p/GeV));
}
//______________________________________________________________________________

Double_t ElectroProductionXSec::GetVirtualPhotonFlux(Double_t energy, Double_t theta)
{
   Double_t coef = (1.0 / 137.0) * (1.0 / (2.0 * TMath::Pi() * TMath::Pi()));
   Double_t oneover1minusepsilon = 1.0 / (1.0 - GetEpsilon(energy, theta));
   Double_t eovereprime = energy / GetBeamEnergy();
   return(coef * GetHandFluxFactor(energy, theta) / GetQSquared(energy, theta) * eovereprime * oneover1minusepsilon);
}
//______________________________________________________________________________
}}
