#include "RADCORKinematics.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
RADCORVariables::RADCORVariables()
{
   fIsElastic  = false;
   fIsInternal = true; 
   fIsExternal = true; 
   fUnits      = 0;     // 0 => GeV, 1 => MeVo
   fCONV       = 1E+3;  // multiplying by fCONV converts GeV to MeV   
   fM = M_p/GeV; 
   fm = M_e/GeV; 
   fM_pion = M_pion/GeV;  
   Clear();
}
//______________________________________________________________________________
RADCORVariables::~RADCORVariables(){


}
//______________________________________________________________________________
void RADCORVariables::Update(){

   /// general constants 
   Double_t alpha = fine_structure_const;
   // for Q2, etc 
   Double_t SIN   = sin(ftheta/2.0);
   Double_t SIN2  = SIN*SIN;
   // for 4-vectors
   Double_t SIN_TH = TMath::Sin(ftheta);
   Double_t COS_TH = TMath::Cos(ftheta);
   Double_t SIN_PH = TMath::Sin(fphi);
   Double_t COS_PH = TMath::Cos(fphi);
   // powers of Z 
   Double_t Z23   = TMath::Power(fZ,-2.0/3.0);
   Double_t Z13   = TMath::Power(fZ,-1.0/3.0);

   Double_t MASS=0.; 
   //if(fUnits==1){
   //   // convert to MeV.  Default is GeV (fUnits = 0)
   //   // energies are NOT converted, because they are input 
   //   // by the user (including M_A)  
   //   fM      *= fCONV; 
   //   fm      *= fCONV; 
   //   fM_pion *= fCONV; 
   //} 

   if(fIsElastic){
      MASS = fM_A; 
   }else{
      MASS = fM; 
   }

   /// scalar variables 
   // radiation lengths  
   if(!fIsExternal){
      fTb = 0.; 
      fTa = 0.; 
   }
   fT   = fTb + fTa;

   // kinematics, etc.
   // From Rev. Mod. Phys. 41, 205 (1969)
   // Double_t f23 = 1440.0; 
   // Double_t f13 = 183.0; 
   // From Rev. Mod. Phys. 46, 815 (1974) [UPDATED from 1969]
   Double_t f23 = 1194.; 
   Double_t f13 = 184.15; 

   fEta = TMath::Log(f23*Z23)/TMath::Log(f13*Z13);        
   fb   = (4./3.)*(1. + (1./9.)*( (fZ+1.)/(fZ+fEta) )*( 1./TMath::Log(f13*Z13) ) ); 
   fXi  = ( pi*fm/(2.*alpha) )*( fT/( (fZ+fEta)*TMath::Log(f13*Z13) ) );

   // other kinematical quantities  
   fR   = (fM_A + 2.0*fEs*SIN2)/(fM_A - 2.0*fEp*SIN2);
   fQ2  = insane::Kine::Qsquared(fEs,fEp,ftheta); 
   fx   = fQ2/(2.*MASS*(fEs-fEp));
   fy   = (fEs-fEp)/fEs;
   fW2  = MASS*MASS + 2.*MASS*(fEs-fEp) - fQ2; 
   fW   = TMath::Sqrt(fW2); 

   /// 4-vectors 
   /// theta_scat = e- in-plane scattering angle in RADIANS 
   /// phi_scat   = e- out-of-plane scattering angle in RADIANS
   /// +x is out of the page
   /// +y is to the right 
   /// +z is up 

   // incident electron: s 
   Double_t s   = TMath::Sqrt(fEs*fEs - fm*fm);
   Double_t s_x = 0;
   Double_t s_y = 0;
   Double_t s_z = s;
   Double_t s_t = fEs;
   // scattered electron: p  
   Double_t p   = TMath::Sqrt(fEp*fEp - fm*fm);
   Double_t p_x = p*SIN_TH*COS_PH;
   Double_t p_y = p*SIN_TH*SIN_PH;
   Double_t p_z = p*COS_TH;
   Double_t p_t = fEp;
   // for the (FIXED) target: t  
   Double_t t_x = 0;
   Double_t t_y = 0;
   Double_t t_z = 0;
   // Double_t t_t = fM_A;
   Double_t t_t = MASS;                  // inelastic scattering from a nucleon!
   // note: time component goes last 
   fs     = TLorentzVector(s_x,s_y,s_z,s_t);
   fp     = TLorentzVector(p_x,p_y,p_z,p_t);
   ft     = TLorentzVector(t_x,t_y,t_z,t_t);
   fu     = fs + ft - fp;
   // zero component and 3-vector terms 
   fu_0   = fEs - fEp + MASS;
   fpVect = TMath::Sqrt(fp.E()*fp.E() - fp*fp);
   fsVect = TMath::Sqrt(fs.E()*fs.E() - fs*fs);
   ftVect = TMath::Sqrt(ft.E()*ft.E() - ft*ft);
   fuVect = TMath::Sqrt(fu_0*fu_0 - fu*fu);

}
//______________________________________________________________________________
void RADCORVariables::Clear(){
   fEs  = 0;
   fEp  = 0; 
   fx   = 0;
   fy   = 0;
   fQ2  = 0;
   fW   = 0; 
   fW2  = 0;
   fb   = 0;
   fEta = 0;
   fXi  = 0;
   fR   = 0;
   fTb  = 0;
   fTa  = 0;
   fT   = 0;

}
//______________________________________________________________________________
void RADCORVariables::Print(){

   std::cout << "-------------------   RADCOR Variables    -------------------" << std::endl;
   std::cout << "Es:            " << fEs                                        << std::endl;
   std::cout << "Ep:            " << fEp                                        << std::endl;
   std::cout << "theta:         " << ftheta/degree                              << std::endl;
   std::cout << "phi:           " << fphi/degree                                << std::endl;
   std::cout << "x:             " << fx                                         << std::endl;
   std::cout << "y:             " << fy                                         << std::endl;
   std::cout << "Q2:            " << fQ2                                        << std::endl;
   std::cout << "W:             " << fW                                         << std::endl;
   std::cout << "W2:            " << fW2                                        << std::endl;
   std::cout << "b:             " << fb                                         << std::endl;
   std::cout << "eta:           " << fEta                                       << std::endl;
   std::cout << "xi:            " << fXi                                        << std::endl;
   std::cout << "R:             " << fR                                         << std::endl;
   std::cout << "-------------------  RADCOR Rad. Lengths  -------------------" << std::endl;
   std::cout << "t_b:           " << fTb                                        << std::endl;
   std::cout << "t_a:           " << fTa                                        << std::endl;
   std::cout << "T:             " << fT                                         << std::endl;
   std::cout << "------------------- RADCOR Mass Variables -------------------" << std::endl;
   std::cout << "electron mass: " << fm   << " GeV"                             << std::endl;
   std::cout << "proton mass:   " << fM   << " GeV"                             << std::endl;
   std::cout << "target mass:   " << fM_A << " GeV"                             << std::endl;
   std::cout << "------------------- RADCOR Target Variables -----------------" << std::endl;
   std::cout << "Z:             " << fZ                                         << std::endl;
   std::cout << "A:             " << fA                                         << std::endl;

}
//______________________________________________________________________________

//______________________________________________________________________________
RADCORKinematics::RADCORKinematics(){

   SetM(M_p/GeV); 
   Setm(M_e/GeV); 
   SetM_A(M_p/GeV); 
   SetM_pion(M_pion/GeV); 

   // fVariables.Setk1(&fk1);
   // fVariables.Setk2(&fk2);
   // fVariables.SetP(&fP);
   // fVariables.SetXi(&fXi);
   // fVariables.SetEta(&fEta);

   fIsModified = true;

}
//______________________________________________________________________________
RADCORKinematics::~RADCORKinematics(){

}
//______________________________________________________________________________
bool RADCORKinematics::Valid() const {
   if( fVariables.fEp < 0.0 ) return false;
   if( fVariables.fEs < 0.0 ) return false;
   if( fVariables.fx  < 0.0 ) return false;

   return true;
}
//______________________________________________________________________________

}}
