#include "insane/xsections/MAIDPhotoAbsorptionCrossSections.h"
#include "insane/pdfs/FortranWrappers.h"
#include "insane/kinematics/KinematicFunctions.h"

namespace insane {
  namespace physics {

    MAIDPhotoAbsorptionCrossSections::MAIDPhotoAbsorptionCrossSections(
      const char * n, const char * t ) : 
      PhotoAbsorptionCrossSections(n,t)
    {
      sigT    = 0.0;
      sigL    = 0.0;
      sigLT   = 0.0;
      sigLTp  = 0.0;
      sigTTp  = 0.0;
      sigL0   = 0.0;
      sigLT0  = 0.0;
      sigLT0p = 0.0;
    }
    
    MAIDPhotoAbsorptionCrossSections::~MAIDPhotoAbsorptionCrossSections()
    { }
    
    void MAIDPhotoAbsorptionCrossSections::CalculateProton(Double_t x, Double_t Q2)
    {
      fSig_T    = 0.0;
      fSig_L    = 0.0;
      fSig_LT   = 0.0;
      fSig_LTp  = 0.0;
      fSig_TT   = 0.0;
      fSig_TTp  = 0.0;
      fSig_L0   = 0.0;
      fSig_LT0  = 0.0;
      fSig_LT0p = 0.0;

      double W = insane::kine::W_xQsq(x,Q2)*1000.0; // W in MeV
      int    iso = 1; // gamma + p -> pi0 + p
      //std::cout << "W  = " << W  << "\n";
      //std::cout << "Q2 = " << Q2 << "\n";
      //std::cout << "x  = " << x  << "\n";

      if( Q2 >= 0.0 && Q2 < 5.0)  {
        if( W > 1073.2 && W <= 1800.0 ) {
          iso = 1; // gamma + p -> pi+ + n
          maid07tot_(&iso,&W,&Q2,&sigT,&sigL,&sigLT,&sigLTp,&sigTTp,&sigL0,&sigLT0,&sigLT0p);
          fSig_T    += (sigT   /1000.0);
          fSig_L    += (sigL   /1000.0);
          fSig_LT   += (sigLT  /1000.0);
          fSig_LTp  += (sigLTp /1000.0);
          fSig_TT   += (sigTT  /1000.0);
          fSig_TTp  += (sigTTp /1000.0);
          fSig_L0   += (sigL0  /1000.0);
          fSig_LT0  += (sigLT0 /1000.0);
          fSig_LT0p += (sigLT0p/1000.0);
        }

        // pi+ threshold is slightly larger than pi0
        if( W > 1079.1 && W <= 1800.0 ) {
          iso = 3; // gamma + p -> pi+ + n
          maid07tot_(&iso,&W,&Q2,&sigT,&sigL,&sigLT,&sigLTp,&sigTTp,&sigL0,&sigLT0,&sigLT0p);
          fSig_T    += (sigT   /1000.0);
          fSig_L    += (sigL   /1000.0);
          fSig_LT   += (sigLT  /1000.0);
          fSig_LTp  += (sigLTp /1000.0);
          fSig_TT   += (sigTT  /1000.0);
          fSig_TTp  += (sigTTp /1000.0);
          fSig_L0   += (sigL0  /1000.0);
          fSig_LT0  += (sigLT0 /1000.0);
          fSig_LT0p += (sigLT0p/1000.0);
        }
      }

      fSigs[0] = fSig_T    ;
      fSigs[1] = fSig_L    ;
      fSigs[2] = fSig_LT   ;
      fSigs[3] = fSig_LTp  ;
      fSigs[4] = fSig_TT   ;
      fSigs[5] = fSig_TTp  ;
      fSigs[6] = fSig_L0   ;
      fSigs[7] = fSig_LT0  ;
      fSigs[8] = fSig_LT0p ;

    }
    
    void MAIDPhotoAbsorptionCrossSections::CalculateNeutron(Double_t x, Double_t Q2)
    {

      fSig_T    = 0.0;
      fSig_L    = 0.0;
      fSig_LT   = 0.0;
      fSig_LTp  = 0.0;
      fSig_TT   = 0.0;
      fSig_TTp  = 0.0;
      fSig_L0   = 0.0;
      fSig_LT0  = 0.0;
      fSig_LT0p = 0.0;

      double W = insane::kine::W_xQsq(x,Q2)*1000.0; // W in MeV
      int    iso = 1; // gamma + p -> pi0 + p
      //std::cout << "W  = " << W  << "\n";
      //std::cout << "Q2 = " << Q2 << "\n";
      //std::cout << "x  = " << x  << "\n";

      if( Q2 >= 0.0 && Q2 < 5.0)  {
        if( W > 1073.2 && W <= 1800.0 ) {
          iso = 2; // gamma + n -> pi0 + n
          maid07tot_(&iso,&W,&Q2,&sigT,&sigL,&sigLT,&sigLTp,&sigTTp,&sigL0,&sigLT0,&sigLT0p);
          fSig_T    += sigT   ;
          fSig_L    += sigL   ;
          fSig_LT   += sigLT  ;
          fSig_LTp  += sigLTp ;
          fSig_TT   += sigTT  ;
          fSig_TTp  += sigTTp ;
          fSig_L0   += sigL0  ;
          fSig_LT0  += sigLT0 ;
          fSig_LT0p += sigLT0p;
        }

        // pi+ threshold is slightly larger than pi0
        if( W > 1079.1 && W <= 1800.0 ) {
          iso = 4; // gamma + n -> pi- + p
          maid07tot_(&iso,&W,&Q2,&sigT,&sigL,&sigLT,&sigLTp,&sigTTp,&sigL0,&sigLT0,&sigLT0p);
          fSig_T    += sigT   ;
          fSig_L    += sigL   ;
          fSig_LT   += sigLT  ;
          fSig_LTp  += sigLTp ;
          fSig_TT   += sigTT  ;
          fSig_TTp  += sigTTp ;
          fSig_L0   += sigL0  ;
          fSig_LT0  += sigLT0 ;
          fSig_LT0p += sigLT0p;
        }
      }

      fSigs[0] = fSig_T    ;
      fSigs[1] = fSig_L    ;
      fSigs[2] = fSig_LT   ;
      fSigs[3] = fSig_LTp  ;
      fSigs[4] = fSig_TT   ;
      fSigs[5] = fSig_TTp  ;
      fSigs[6] = fSig_L0   ;
      fSigs[7] = fSig_LT0  ;
      fSigs[8] = fSig_LT0p ;

    }
    //______________________________________________________________________________

  }
}

