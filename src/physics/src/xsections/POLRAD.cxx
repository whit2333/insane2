#include "insane/xsections/POLRAD.h" 
#include <typeinfo>
#include <fstream>

namespace insane {
namespace physics {

//_____________________________________________________________________________
POLRAD::POLRAD(){
   Feta_2plus     = 0.0;
   Fxi_2plus      = 0.0;
   Feta_2minus    = 0.0;
   Fxi_2minus     = 0.0;
   Feta_d         = 0.0;
   Fxi_d          = 0.0;
   Fetaeta_2plus  = 0.0;
   Fxieta_2plus   = 0.0;
   Fetaeta_2minus = 0.0;
   Fxieta_2minus  = 0.0;
   Fetaeta_d      = 0.0;
   Fxieta_d       = 0.0;
   Feta_1plus     = 0.0;
   Fetaeta_1plus  = 0.0;
   Feta           = 0.0;
   Fetaeta        = 0.0;
   seta           = 0.0;
   sxi            = 0.0;
   reta           = 0.0;
   rxi            = 0.0;
   F_IR           = 0.0;
   Fxi_IR         = 0.0;
   Feta_IR        = 0.0;
   Fxieta_IR      = 0.0;
   Fetaeta_IR     = 0.0;
   F              = 0.0;
   F_d            = 0.0;
   F_1plus        = 0.0;
   F_2plus        = 0.0;
   F_2minus       = 0.0;
   F_i            = 0.0;
   F_ii           = 0.0;
   // note index 0 is not used in order to be similar to paper
   Int_t kis[] = {0,3,3,4,5,5,5,3,4};
   Int_t lis[] = {0,1,1,1,2,3,3,1,2};
   for(Int_t i=0;i<9;i++){
      ki[i]=kis[i];
      li[i]=lis[i];
   }
   // Set up sizes. (HEIGHT x WIDTH)
   Int_t ni = 9;
   Int_t nj = 6;
   Int_t nk = 4;
   fT.resize(ni);
   fT2.resize(ni);
   for (Int_t i = 0; i < ni ; ++i) {
      fT[i].resize(nj);
      fT2[i].resize(nj);
      for (Int_t j = 0; j < nj; ++j){
         fT[i][j].resize(nk);
         fT2[i][j].resize(nk);
         for(Int_t k = 0; k<nk; ++k) fT[i][j][k] = 0.0;
         for(Int_t k = 0; k<nk; ++k) fT2[i][j][k] = 0.0;
      }
   }

   fTau           = 0.0;
   ffSFIndex      = 0;
   fThIIndex      = 0;
   fThJIndex      = 0;

   // various switches 
   fIsMultiPhoton         = true;
   fReCalculateTs         = true;
   //fIsQEFullCalc          = false;
   fIsExtTail             = false;
   //fIsUltraRel            = false;

   fERT_IntegrationMethod = kAnglePeaking;
   fQRT_IntegrationMethod = kAnglePeaking;
   fIRT_IntegrationMethod = kAnglePeaking;  
   
   fUnpolSFs   = nullptr;
   fUnpolQESFs = nullptr;
   fPolSFs     = nullptr;
   fFFs        = nullptr;
   fFFTs       = nullptr;

   fErr           = 1E-10; 
   fDepth         = 100; 
   fDebug         = 0;

   fUnpolSFs   = nullptr;
   fUnpolQESFs = nullptr;
   fPolSFs     = nullptr;
   fFFs        = nullptr;
   fFFTs       = nullptr;

   fspin2        = 1;
   fDeltaE       = 0.010; // 10 MeV
   fNucZ         = 1.0;
   fZ            = 1.0;
   fA            = 1.0;
   fTargMom      = 0.0;
   fPF           = 0.164; // Fermi momentum
   fP_L          = 1.0;
   fP_N          = 1.0;
   fQ_N          = 0.0;
   fP_n          = 0.0;
   fP_p      = 0.0;
   fHelicity = 1;
   ft_b      = 0.;
   ft_a      = 0.;

   for(Int_t i=0;i<9;i++){
      fFin[i] = 0.0;
      fFel[i] = 0.0;
      fFqe[i] = 0.0;
   }
   fSFType = 0;
   for(Int_t i=0;i<9;i++){
      ffSFin[i] = 0.0;
      ffSFel[i] = 0.0;
      ffSFqe[i] = 0.0;
   }
   fFc  = 0.0;
   fFm  = 0.0;
   fFq  = 0.0;
   fGe  = 0.0;
   fGm  = 0.0;
   fe_p = 0.0;
   fe_n = 0.0;
   fm_p = 0.0;
   fm_n = 0.0;

   fF1   = 0.0;
   fF2   = 0.0;
   fF1qe = 0.0;
   fF2qe = 0.0;
   fg1   = 0.0;
   fg2   = 0.0;
   fb1   = 0.0;
   fb2   = 0.0;
   fb3   = 0.0;
   fb4   = 0.0;

   fArbValue1 = 0.0;
   fArbValue2 = 0.0;
   fArbValue3 = 0.0;
   fArbValue4 = 0.0;
   fArbValue5 = 0.0;
   fArbValue6 = 0.0;
   fArbValue7 = 0.0;

   SetHelicity(1);

   fTargetNucleus = Nucleus::Proton();
   SetTargetNucleus(Nucleus::Proton()); 
   fPOLRADTarget = fTargetNucleus.GetType(); 


   fKinematics.Setm( M_e/GeV );
   fKinematics.SetM(M_p/GeV);
   fKinematics.SetM_pion(0.131);
   fKinematics.SetM_A(M_p/GeV);
   // initialize 4-vectors 
   TLorentzVector p(0.0,0.0,0.0,M_p/GeV);
   TLorentzVector k1(0.0,0.0,5.9,5.9);
   TLorentzVector eta(0.0,0.0,1.0,0.0);
   TLorentzVector k2(1.0,0.0,1.0,TMath::Sqrt(2.0));
   TLorentzVector xi = (1.0/(M_e/GeV))*k1;
   fKinematics.SetP( p );
   fKinematics.SetEta( eta );
   fKinematics.Setk1( k1  );
   fKinematics.Setk2( k2 );
   fKinematics.SetXi( xi );

   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kLEGENDRE; //kVEGAS,kADAPTIVE,kADAPTIVESINGULAR
   fIRT_tau_integrator   = nullptr;
   fIRT_tau_AbsErr       = 1e-10;
   fIRT_tau_RelErr       = 0.000001;
   fIRT_tau_nCalls       = 10000;
   fIRT_tau_Rule         = 2;
   fIRT_tau_Type         = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR;
   fIRT_tau_func.fPOLRAD = this;

   fIRT_R_integrator   = nullptr;
   fIRT_R_AbsErr       = 1e10;
   fIRT_R_RelErr       = 1e-5;
   fIRT_R_nCalls       = 1000;
   fIRT_R_Rule         = 1;
   fIRT_R_nLegPoints   = 12;
   fIRT_R_Type         = ROOT::Math::IntegrationOneDim::kLEGENDRE; //kADAPTIVE
   //fIRT_R_Type         = ROOT::Math::IntegrationOneDim::kADAPTIVE;
   fIRT_R_func.fPOLRAD = this;

}
//_____________________________________________________________________________
POLRAD::~POLRAD(){
   if(fIRT_R_integrator)   delete fIRT_R_integrator;
   if(fIRT_tau_integrator) delete fIRT_tau_integrator;
}
//_____________________________________________________________________________
void POLRAD::Initialize(){
}
//____________________________________________________________________________
void POLRAD::SetTarget(Int_t Z, Int_t A) {
   Error("SetTarget"," This currently does nothin!");
}
//____________________________________________________________________________
void POLRAD::SetTargetNucleus(const Nucleus& n){

   //fPOLRADTarget = t;
   fTargetNucleus = n; 
   //fTargetNucleus.Print();
   fA   = Double_t( fTargetNucleus.GetA() );
   fZ   = Double_t( fTargetNucleus.GetZ() );
   fspin2  = int(2.0*fTargetNucleus.GetSpin());
   //std::cout << " s2 = " << fspin2 << std::endl;
   fP_p = fTargetNucleus.GetEffectiveProtonPolarization(); 
   fP_n = fTargetNucleus.GetEffectiveNeutronPolarization(); 
   fKinematics.SetM_A( fTargetNucleus.GetMass() ); 

   //TString Name = fTargetNucleus.GetName(); 
   //std::cout << "[POLRAD::SetTargetType]: Now using " << Name << " target." << std::endl;

   //Print();
}
//____________________________________________________________________________
void POLRAD::SetKinematics(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){

   /// theta_scat = e- in-plane scattering angle 
   /// phi_scat   = e- out-of-plane scattering angle 
   /// +x is out of the page
   /// +y is to the right 
   /// +z is up 

   Double_t SIN_TH = TMath::Sin(theta);
   Double_t COS_TH = TMath::Cos(theta);
   Double_t SIN_PH = TMath::Sin(phi);
   Double_t COS_PH = TMath::Cos(phi);
   /// incident electron 
   Double_t k1   = TMath::Sqrt(Ebeam*Ebeam - (M_e*M_e)/(GeV*GeV) ); 
   Double_t k1_x = 0; 
   Double_t k1_y = 0; 
   Double_t k1_z = k1;
   Double_t k1_t = Ebeam;  
   /// scattered electron 
   Double_t k2   = TMath::Sqrt(Eprime*Eprime - (M_e*M_e)/(GeV*GeV)); 
   Double_t k2_x = k2*SIN_TH*COS_PH; 
   Double_t k2_y = k2*SIN_TH*SIN_PH; 
   Double_t k2_z = k2*COS_TH;
   Double_t k2_t = Eprime; 
   /// for the target
   Double_t p_x = 0; 
   Double_t p_y = 0; 
   Double_t p_z = 0;
   Double_t p_t = M_p/GeV;

   /// Lorentz vectors 
   /// t-component goes last
   TLorentzVector k11(k1_x,k1_y,k1_z,k1_t);
   TLorentzVector k22(k2_x,k2_y,k2_z,k2_t);
   TLorentzVector p(p_x,p_y,p_z,p_t);
   fKinematics.Setk1(k11);
   fKinematics.Setk2(k22);
   fKinematics.SetP(p);

   /// Kinematics for external tail [Phys Rev D 12, 1884 (1975)]
   /// Es    = incident electron energy (GeV) 
   /// Ep    = scattered electron energy (GeV) 
   /// theta = electron scattering angle (rad) 
   fKinematics.SetEs(Ebeam); 
   fKinematics.SetEp(Eprime);
   fKinematics.SetTheta(theta);   /// \todo Use proper scattering angle, incorporating theta, phi... 

   Double_t m             = (M_e/GeV);
   Double_t M             = fKinematics.GetM(); 
   Double_t k1_abs        = k11.Vect().Mag();
   Double_t S             = 2.*k11*p; 
   Double_t k10_over_k1_m = (k11.T())/(m*(k1_abs));
   Double_t a             = 2.*m*M/S;
   /// J Phys G: Nucl Part Phys 18, 1737 (1992) 
   /// Eq 5     
   Double_t xi_x = k11.X()*k10_over_k1_m;
   Double_t xi_y = k11.Y()*k10_over_k1_m;
   Double_t xi_z = k11.Z()*k10_over_k1_m;
   Double_t xi_t = k1_abs/m;
   /// Eq 6: 'explicitly covariant form' 
   //Double_t xi_x_2 = ( (1./m)*k11.X() - (a/M)*p.X() )/TMath::Sqrt(1.-a*a);
   //Double_t xi_y_2 = ( (1./m)*k11.Y() - (a/M)*p.Y() )/TMath::Sqrt(1.-a*a);
   //Double_t xi_z_2 = ( (1./m)*k11.Z() - (a/M)*p.Z() )/TMath::Sqrt(1.-a*a);
   //Double_t xi_t_2 = ( (1./m)*k11.T() - (a/M)*p.T() )/TMath::Sqrt(1.-a*a);

   // std::cout << "xi_x = " << xi_x << "\t" << "xi_x_2 = " << xi_x_2 << std::endl;
   // std::cout << "xi_y = " << xi_y << "\t" << "xi_y_2 = " << xi_y_2 << std::endl;
   // std::cout << "xi_z = " << xi_z << "\t" << "xi_z_2 = " << xi_z_2 << std::endl;
   // std::cout << "xi_t = " << xi_t << "\t" << "xi_t_2 = " << xi_t_2 << std::endl;

   TLorentzVector xi(xi_x,xi_y,xi_z,xi_t);   

   /// Now that k1 is set we must reset xi
   fKinematics.SetXi( xi );

   // fKinematics.GetXi().Print();
   // fKinematics.fM    = fM;
   // fKinematics.fm    = fm;
   // fKinematics.fP    = fP;
   // fKinematics.fk1   = fk1;
   // fKinematics.fk2   = fk2;
   // fKinematics.fXi   = fXi;
   // fKinematics.fEta  = fEta;

}
//____________________________________________________________________________
void POLRAD::SetKinematicVectors(TLorentzVector ik1,TLorentzVector ik2,TLorentzVector ip){

   fKinematics.Setk1(ik1);
   fKinematics.Setk2(ik2 );
   fKinematics.SetP( ip);
   /// Now that k1 is set we must reset xi
   /// J Phys G: Nucl Part Phys 18, 1737 (1992) 
   Double_t m             = (M_e/GeV);
   Double_t M             = fKinematics.GetM(); 
   Double_t k1_abs        = ik1.Vect().Mag();
   //Double_t k10_over_k1_m = (ik1.T())/(m*(k1_abs));
   Double_t S             = 2.*ik1*ip; 
   Double_t a             = 2.*m*M/S;
   /// Eq 5     
   // TLorentzVector xi( k11.X()*k10_over_k1_m, k11.Y()*k10_over_k1_m, k11.Z()*k10_over_k1_m, k1_abs/m);
   /// Eq 6: 'explicitly covariant form' 
   Double_t xi_x = ( (1./m)*ik1.X() - (a/M)*ip.X() )/TMath::Sqrt(1.-a*a);
   Double_t xi_y = ( (1./m)*ik1.Y() - (a/M)*ip.Y() )/TMath::Sqrt(1.-a*a);
   Double_t xi_z = ( (1./m)*ik1.Z() - (a/M)*ip.Z() )/TMath::Sqrt(1.-a*a);
   Double_t xi_t = ( (1./m)*ik1.T() - (a/M)*ip.T() )/TMath::Sqrt(1.-a*a);
   // std::cout << "xi_x = " << xi_x << std::endl;
   // std::cout << "xi_y = " << xi_y << std::endl;
   // std::cout << "xi_z = " << xi_z << std::endl;
   // std::cout << "xi_t = " << xi_t << std::endl;
   TLorentzVector xi(xi_x,xi_y,xi_z,xi_t);   

   fKinematics.SetXi( xi );
}
//____________________________________________________________________________
void POLRAD::SetPolarizations(Double_t *P){

   fP_L = P[0];        // initial lepton polarization 
   fP_N = P[1];        // target polarization 
   fQ_N = P[2];        // target quadrupole moment 
   /// Target polarization 4-vector
   /// +x is out of the page
   /// +y is to the right 
   /// +z is up 
   Double_t th = P[3]; /// target-theta (in-plane) angle
   Double_t ph = P[4]; /// target-phi (out-of-plane) angle  

   Double_t SIN_TH = TMath::Sin(th); 
   Double_t COS_TH = TMath::Cos(th); 
   Double_t SIN_PH = TMath::Sin(ph); 
   Double_t COS_PH = TMath::Cos(ph); 

   Double_t eta_x = SIN_TH*COS_PH; 
   Double_t eta_y = SIN_TH*SIN_PH; 
   Double_t eta_z = COS_TH; 
   Double_t eta_t = 0.;      /// zero time component
   fKinematics.SetEta( TLorentzVector(eta_x,eta_y,eta_z,eta_t) );

}
//_____________________________________________________________________________
//Double_t POLRAD::aik(Int_t i,Int_t k) {
//	Double_t etaq = fKinematics.GetEta()*fKinematics.Getq();//-fQ2*(fa_eta-fb_eta) + fS_x*fc_eta;
//	Double_t Q2   = fKinematics.GetQ2();
//	if( k==1 && (i==1||i==2||i==3||i==7) ) return(1.0);
//
//	if( (k==1) && (i==4||i==8) ) return(etaq);
//	if( (k==2) && (i==4||i==8) ) return(-1.0);
//
//	if( (k==1) && (i==5||i==6) ) return(Q2-3.0*etaq*etaq);
//	if( (k==2) && (i==5||i==6) ) return(6.0*etaq);
//	if( (k==3) && (i==5||i==6) ) return(-3.0);
//	else {
//		std::cout << "[POLRAD::aik(int i,int k)]: " << std::endl;
//                std::cout << Form("Calculating for (i,k) = (%d,%d)! See PolRad2.0 Manual eqn(B.3)",i,k) << std::endl;
//		return(0);
//	}
//
//}
//_____________________________________________________________________________
Double_t POLRAD::aik(Int_t i,Int_t k) {
   //Double_t etaq = fKinematics.GetEta()*fKinematics.Getq();//-fQ2*(fa_eta-fb_eta) + fS_x*fc_eta;
   Double_t Q2    = fKinematics.GetQ2();
   Double_t a_eta = fKinematics.Geta_eta();
   Double_t b_eta = fKinematics.Getb_eta();
   Double_t c_eta = fKinematics.Getc_eta();
   Double_t S_x   = fKinematics.GetS_x();
   Double_t etaq  = -Q2*(a_eta-b_eta) + S_x*c_eta;
   Double_t M     = fKinematics.GetM();

   //if( k==1 && (i==1||i==2||i==3||i==7) ) return(1.0);
   //if( (k==1) && (i==4||i==8) ) return(etaq);
   //if( (k==2) && (i==4||i==8) ) return(-1.0);
   //if( (k==1) && (i==5||i==6) ) return(Q2-3.0*etaq*etaq);
   //if( (k==2) && (i==5||i==6) ) return(6.0*etaq);
   //if( (k==3) && (i==5||i==6) ) return(-3.0);

   if( k==1 && (i==1||i==2||i==3||i==7) ) return(1.0);
   if( (k==1) && (i==4||i==8) ) return(etaq/M);
   if( (k==2) && (i==4||i==8) ) return(-1.0/M);
   if( (k==1) && (i==5||i==6) ) return( (Q2-3.0*etaq*etaq)/(M*M));
   if( (k==2) && (i==5||i==6) ) return(6.0*etaq/(M*M));
   if( (k==3) && (i==5||i==6) ) return(-3.0/(M*M) );
   else {
      std::cout << "[POLRAD::aik(int i,int k)]: " << std::endl;
      std::cout << Form("Calculating for (i,k) = (%d,%d)! See PolRad2.0 Manual eqn(B.3)",i,k) << std::endl;
      return(0);
   }

}
//_____________________________________________________________________________
Double_t POLRAD::THETAij(Int_t i,Int_t j, Double_t tau) {
   if(fReCalculateTs) Tijk(tau);
   Double_t res = 0.0;
   for(int k = TMath::Max(1,j+li[i]-ki[i]); k <= TMath::Min(j,li[i]); k++ ){
      if(k==1){
         res += aik(i,k)*fT[i][j][k];
         //std::cout << i << "," << j << "," << k << " = " << fT[i][j][k] << "\n";
      }
      else if(k==2) { 
         res += aik(i,k)*fT[i][j-1][k];
         //std::cout << i << "," << j << "," << k << " = " << fT[i][j-1][k] << "\n";
      }
      else if(k==3){
         res += aik(i,k)*fT[i][j-2][k];
         //std::cout << i << "," << j << "," << k << " = " << fT[i][j-2][k] << "\n";
      }
   }
   return(res);
}
//_____________________________________________________________________________
//Double_t POLRAD::THETAij(Int_t i,Int_t j, Double_t tau) {
//	if(fReCalculateTs) Tijk(tau);
//	Double_t res = 0.0;
//	for(int k = TMath::Max(1,j+li[i]-ki[i]); k <= TMath::Min(j,li[i]); k++ ){
//		res += aik(i,k)*fT[i][j][k];
//	}
//	return(res);
//}
//_____________________________________________________________________________
Double_t POLRAD::qik(Int_t i, Int_t k, Double_t tau) {
   Double_t M = fKinematics.GetM();
   Double_t arg = ((k==2)*( (i==5) + (i==6) ))*(tau/M);
   if( arg != 0.0) std::cout << "q" << i << k << " = " << arg <<std::endl;
   return arg; 
}
//_____________________________________________________________________________
Double_t POLRAD::B1(Double_t tau) {
   Double_t Lambda_Q = fKinematics.GetLambda_Q();
   Double_t Q2       = fKinematics.GetQ2();
   Double_t S_p      = fKinematics.GetS_p();
   Double_t S_x      = fKinematics.GetS_x();
   Double_t arg      = -0.5*(Lambda_Q*tau + S_p*(S_x*tau + 2.0*Q2) );
   return arg; 
}
//_____________________________________________________________________________
Double_t POLRAD::B2(Double_t tau) {
   Double_t Lambda_Q = fKinematics.GetLambda_Q();
   Double_t Q2       = fKinematics.GetQ2();
   Double_t S_p      = fKinematics.GetS_p();
   Double_t S_x      = fKinematics.GetS_x();
   Double_t arg      = -0.5*(Lambda_Q*tau - S_p*(S_x*tau + 2.0*Q2) );  
   return arg; 
}
//_____________________________________________________________________________
Double_t POLRAD::C1(Double_t tau) {
   Double_t M   = fKinematics.GetM();
   Double_t m   = fKinematics.Getm();
   Double_t Q2  = fKinematics.GetQ2();
   Double_t S_x = fKinematics.GetS_x();
   Double_t S   = fKinematics.GetS();
   Double_t arg = TMath::Power(S*tau+Q2,2.0) + 4.0*m*m*(Q2 + tau*S_x - tau*tau*M*M);
   return arg;
}
//_____________________________________________________________________________
Double_t POLRAD::C2(Double_t tau) { 
   Double_t M   = fKinematics.GetM();
   Double_t m   = fKinematics.Getm();
   Double_t Q2  = fKinematics.GetQ2();
   Double_t S_x = fKinematics.GetS_x();
   Double_t X   = fKinematics.GetX();
   Double_t arg = TMath::Power(X*tau-Q2,2.0) + 4.0*m*m*(Q2 + tau*S_x - tau*tau*M*M);
   return arg;
}
//_____________________________________________________________________________
Double_t POLRAD::Fd(Double_t tau) {
   Double_t Q2  = fKinematics.GetQ2();
   Double_t S_x = fKinematics.GetS_x();
   Double_t S_p = fKinematics.GetS_p();
   Double_t num = S_p*(tau*S_x + 2.*Q2); 
   Double_t c1  = C1(tau);
   Double_t c2  = C2(tau);
   Double_t sC1 = TMath::Sqrt(c1); 
   Double_t sC2 = TMath::Sqrt(c2);
   Double_t den = sC1*sC2*(sC1 + sC2); 
   Double_t arg = num/den;
   // if(TMath::Abs(tau) < 0.1) return 0.0;
   // arg = (1.0/sC2 - 1.0/sC1)/tau;
   return arg; 
}
//_____________________________________________________________________________
Double_t POLRAD::aik_A(Int_t i,Int_t k) {
   //Double_t etaq = fKinematics.GetEta()*fKinematics.Getq();//-fQ2*(fa_eta-fb_eta) + fS_x*fc_eta;
   Double_t Q2    = fKinematics.GetQ2();
   Double_t a_eta = fKinematics.Geta_eta_A();
   Double_t b_eta = fKinematics.Getb_eta_A();
   Double_t c_eta = fKinematics.Getc_eta_A();
   Double_t S_x   = fKinematics.GetS_xA();
   Double_t etaq  = -Q2*(a_eta-b_eta) + S_x*c_eta;
   Double_t M     = fKinematics.GetM_A();
   if( k==1 && (i==1||i==2||i==3||i==7) ) return(1.0);

   if( (k==1) && (i==4||i==8) ) return(etaq/M);
   if( (k==2) && (i==4||i==8) ) return(-1.0/M);

   if( (k==1) && (i==5||i==6) ) return( (Q2-3.0*etaq*etaq)/(M*M));
   if( (k==2) && (i==5||i==6) ) return(6.0*etaq/(M*M));
   if( (k==3) && (i==5||i==6) ) return(-3.0/(M*M) );
   else {
      std::cout << "[POLRAD::aik(int i,int k)]: " << std::endl;
      std::cout << Form("Calculating for (i,k) = (%d,%d)! See PolRad2.0 Manual eqn(B.3)",i,k) << std::endl;
      return(0);
   }

}
//_____________________________________________________________________________
Double_t POLRAD::THETAij_A(Int_t i,Int_t j, Double_t tau) {
   if(fReCalculateTs) Tijk_A(tau);
   Double_t res = 0.0;
   for(int k = TMath::Max(1,j+li[i]-ki[i]); k <= TMath::Min(j,li[i]); k++ ){
      if(k==1){
         res += aik_A(i,k)*fT[i][j][k];
         //std::cout << i << "," << j << "," << k << " = " << fT[i][j][k] << "\n";
      }
      else if(k==2) { 
         res += aik_A(i,k)*fT[i][j-1][k];
         //std::cout << i << "," << j << "," << k << " = " << fT[i][j-1][k] << "\n";
      }
      else if(k==3){
         res += aik_A(i,k)*fT[i][j-2][k];
         //std::cout << i << "," << j << "," << k << " = " << fT[i][j-2][k] << "\n";
      }
   }
   return(res);
}
//_____________________________________________________________________________
Double_t POLRAD::qik_A(Int_t i, Int_t k, Double_t tau) {
   Double_t M = fKinematics.GetM_A();
   Double_t arg = ((k==2)*( (i==5) + (i==6) ))*(tau/M);
   if( arg != 0.0) std::cout << "q" << i << k << " = " << arg <<std::endl;
   return arg; 
}
//_____________________________________________________________________________
Double_t POLRAD::B1_A(Double_t tau) {
   Double_t Lambda_Q = fKinematics.GetLambda_QA();
   Double_t Q2       = fKinematics.GetQ2();
   Double_t S_p      = fKinematics.GetS_pA();
   Double_t S_x      = fKinematics.GetS_xA();
   Double_t arg      = -0.5*(Lambda_Q*tau + S_p*(S_x*tau + 2.0*Q2) );
   return arg; 
}
//_____________________________________________________________________________
Double_t POLRAD::B2_A(Double_t tau) {
   Double_t Lambda_Q = fKinematics.GetLambda_QA();
   Double_t Q2       = fKinematics.GetQ2();
   Double_t S_p      = fKinematics.GetS_pA();
   Double_t S_x      = fKinematics.GetS_xA();
   Double_t arg      = -0.5*(Lambda_Q*tau - S_p*(S_x*tau + 2.0*Q2) );  
   return arg; 
}
//_____________________________________________________________________________
Double_t POLRAD::C1_A(Double_t tau) {
   Double_t M   = fKinematics.GetM_A();
   Double_t m   = fKinematics.Getm();
   Double_t Q2  = fKinematics.GetQ2();
   Double_t S_x = fKinematics.GetS_xA();
   Double_t S   = fKinematics.GetS_A();
   Double_t arg = TMath::Power(S*tau+Q2,2.0) + 4.0*m*m*(Q2 + tau*S_x - tau*tau*M*M);
   return arg;
}
//_____________________________________________________________________________
Double_t POLRAD::C2_A(Double_t tau) { 
   Double_t M   = fKinematics.GetM_A();
   Double_t m   = fKinematics.Getm();
   Double_t Q2  = fKinematics.GetQ2();
   Double_t S_x = fKinematics.GetS_xA();
   Double_t X   = fKinematics.GetX_A();
   Double_t arg = TMath::Power(X*tau-Q2,2.0) + 4.0*m*m*(Q2 + tau*S_x - tau*tau*M*M);
   return arg;
}
//_____________________________________________________________________________
Double_t POLRAD::Fd_A(Double_t tau) {
   // if(tau==0) return(0.0);
   Double_t Q2  = fKinematics.GetQ2();
   Double_t S_x = fKinematics.GetS_xA();
   Double_t S_p = fKinematics.GetS_pA();
   Double_t num = S_p*(tau*S_x + 2.*Q2); 
   Double_t c1  = C1_A(tau);
   Double_t c2  = C2_A(tau);
   Double_t sC1 = TMath::Sqrt(c1); 
   Double_t sC2 = TMath::Sqrt(c2);
   Double_t den = sC1*sC2*(sC1 + sC2); 
   Double_t arg = num/den;
   return arg; 
}
//_____________________________________________________________________________
Double_t POLRAD::Tijk(Double_t tau) {
   Int_t ni = 9;
   Int_t nj = 6;
   Int_t nk = 4;
   for (Int_t i = 0; i < ni ; ++i) {
      for (Int_t j = 0; j < nj; ++j){
         for(Int_t k = 0; k<nk; ++k) fT[i][j][k] = 0.0;
      }
   }

   // basic terms
   // Double_t mB1 = C1(tau);  
   // Double_t mB2 = C2(tau);  
   // Double_t mC1 = C1(tau);  
   // Double_t mC2 = C2(tau);

   Double_t Lambda_Q = fKinematics.GetLambda_Q();
   Double_t Q2       = fKinematics.GetQ2();
   Double_t Q2_m     = fKinematics.GetQ2_m();
   Double_t m        = fKinematics.Getm();
   Double_t M        = fKinematics.GetM();
   Double_t S        = fKinematics.GetS();
   Double_t S_x      = fKinematics.GetS_x();
   Double_t S_p      = fKinematics.GetS_p();
   Double_t X        = fKinematics.GetX();
   //Double_t tau_min  = fKinematics.GetTau_min();

   Double_t a_eta    = fKinematics.Geta_eta();
   Double_t b_eta    = fKinematics.Getb_eta();
   Double_t c_eta    = fKinematics.Getc_eta();
   Double_t a_xi     = fKinematics.Geta_xi();
   Double_t b_xi     = fKinematics.Getb_xi();
   Double_t c_xi     = fKinematics.Getc_xi();

   seta     = a_eta + b_eta;
   reta     = tau*(a_eta - b_eta) + 2.0*c_eta;
   sxi      = a_xi + b_xi;
   rxi      = tau*(a_xi - b_xi) + 2.0*c_xi;
   F_d      = Fd(tau);
   F        = 1.0/TMath::Sqrt(Lambda_Q);
   F_1plus  = 1.0/TMath::Sqrt(C2(tau)) + 1.0/TMath::Sqrt(C1(tau));
   F_2plus  = B2(tau)*TMath::Power(C2(tau),-1.5) - B1(tau)*TMath::Power(C1(tau),-1.5) ;
   F_2minus = B2(tau)*TMath::Power(C2(tau),-1.5) + B1(tau)*TMath::Power(C1(tau),-1.5) ;
   //         std::cout << "========================================================" << std::endl;
   //  	std::cout << "tau = " << tau << std::endl;
   //  	std::cout << "C1_A(tau) = " << C1_A(tau) << std::endl;
   //  	std::cout << "C2_A(tau) = " << C2_A(tau) << std::endl;
   //  	std::cout << "B1_A(tau) = " << B1_A(tau) << std::endl;
   //  	std::cout << "B2_A(tau) = " << B2_A(tau) << std::endl;
   //  	std::cout << "TMath::Power(C1_A(tau),-1.5) = " << TMath::Power(C1_A(tau),-1.5) << std::endl;
   //  	std::cout << "TMath::Power(C2_A(tau),-1.5) = " << TMath::Power(C2_A(tau),-1.5) << std::endl;
   F_IR     = m*m*F_2plus - Q2_m*F_d;
   F_i      = -1.0*TMath::Power(Lambda_Q,-1.5)*B1(tau);
   F_ii     = (3.0*B1(tau)*B1(tau) - Lambda_Q*C1(tau))/(2.0*TMath::Power(Lambda_Q,2.5));

   // all other terms 
   Feta_2plus  = ((2.0*F_1plus + tau*F_2minus)*seta + F_2plus*reta)/2.0;
   Fxi_2plus   = ((2.0*F_1plus + tau*F_2minus)*sxi + F_2plus*rxi)/2.0;
   Feta_2minus = ((2.0*F_d + F_2plus)*tau*seta + F_2minus*reta)/2.0;
   Fxi_2minus  = ((2.0*F_d + F_2plus)*tau*sxi + F_2minus*rxi)/2.0;
   Feta_d      = (F_1plus*seta + F_d*reta)/2.0;
   Fxi_d       = (F_1plus*sxi + F_d*rxi)/2.0;

   Fetaeta_2plus = ((2.0*F_1plus + tau*F_2minus)*( reta*seta + seta*reta ) +
         F_2plus*( reta*reta + tau*tau*seta*seta) +
         4.0*( 2.0*F + F_d*tau*tau )*seta*seta)/4.0;


   Fetaeta_2minus = ((2.0*F_d + F_2plus)*(reta*seta + seta*reta)+
         F_2minus*( reta*reta + tau*tau*seta*seta) + 
         4.0*tau*F_1plus*seta*seta)/4.0;
   /// This term sucks!
   Fxieta_2plus = ((2.0*F_1plus + tau*F_2minus)*( reta*sxi + seta*rxi ) +
         F_2plus*reta*rxi + tau*tau*seta*sxi*F_2plus + 
         4.0*(2.0*F + F_d*tau*tau)*seta*sxi )/4.0;

   /*
      std::cout << " Fxieta_2plus = " << Fxieta_2plus  << "\n";
      std::cout << "     (F_2plus) = " << ( F_2plus) << "\n";
      std::cout << "     (2.0*F_d ) = " << (2.0*F_d) << "\n";
      std::cout << "     (reta*sxi + seta*rxi) = " << (reta*sxi + seta*rxi)  << "\n";
      std::cout << "     F_2minus  = " << F_2minus  << "\n";
      std::cout << "     (reta*rxi + tau*tau*seta*sxi)  = " << (reta*rxi + tau*tau*seta*sxi)   << "\n";
      std::cout << "     4.0*tau*F_1plus*seta*sxi)/4.0 = " << 4.0*tau*F_1plus*seta*sxi << "\n";
      */



   /// So does this one!!!
   Fxieta_2minus  = ( 2.0*F_d*( reta*sxi + seta*rxi )*tau + 
         F_2plus*(reta*sxi + seta*rxi)*tau +
         F_2minus*reta*rxi + F_2minus*tau*tau*seta*sxi + 
         4.0*tau*F_1plus*seta*sxi)/4.0;

   Fetaeta_d      = (F_1plus*(reta*seta + seta*reta) + 
         F_d*( reta*reta + tau*tau*seta*seta) + 
         4.0*F*seta*seta)/4.0;
   Fxieta_d       =  (F_1plus*(reta*sxi + seta*rxi) + 
         F_d*(reta*rxi + tau*tau*seta*sxi) + 
         4.0*F*seta*sxi)/4.0;

   Feta_1plus     = ((4.0*F+tau*tau*F_d)*seta+F_1plus*reta)/2.0;
   Fetaeta_1plus  = (2.0*(4.0*F + tau*tau*F_d)*reta*seta +
         F_1plus*(reta*reta + tau*tau*seta*seta) +
         4.0*(2.0*F_i-tau*F)*seta*seta)/4.0;

   Feta           = (F*(reta-tau*seta)+2.0*F_i*seta)/2.0;
   Fetaeta        = (F*(reta-tau*seta)*(reta-tau*seta)+
         4.0*F_i*(reta-tau*seta)+4.0*F_ii*seta*seta)/4.0;

   Fxi_IR      = m*m*Fxi_2plus - Q2_m*Fxi_d;
   Feta_IR     = m*m*Feta_2plus - Q2_m*Feta_d;
   Fxieta_IR   = m*m*Fxieta_2plus - Q2_m*Fxieta_d;
   Fetaeta_IR  = m*m*Fetaeta_2plus - Q2_m*Fetaeta_d;

   Double_t etaq     = -Q2*(a_eta-b_eta) + S_x *c_eta;
   Double_t etaKAPPA = (Q2 + 4.0*m*m)*(a_eta+b_eta)+S_p*c_eta;
   Double_t etak1    = (etaKAPPA+etaq)/2.0;
   Double_t etak2    = (etaKAPPA-etaq)/2.0;
   Double_t k2xi     = Q2_m*a_xi+2.0 *m*m*b_xi+ X*c_xi;
   Double_t xip      = S*a_xi + X *b_xi + 2.0*M*M*c_xi;
   Double_t xieta    = 2.0*(2.0*m*m*(a_eta*a_xi + b_eta*b_xi) +
         2.0*M*M*c_eta*c_xi + Q2_m*(a_xi*b_eta+b_xi*a_eta) + 
         S*(a_xi*c_eta+c_xi*a_eta) + 
         X*(b_xi*c_eta+c_xi*b_eta));
   /*
      Tijk(1,F,F_2plus,F_2minus,F_d,F_1plus,Fxi_2minus,Fxi_2plus,Fxi_d,Feta_2minus,Feta_2plus,Feta_d,Feta,tau);
   // following B.5
   for(int i=5;i<=6;i++){
   for(int j=1;j<=ki[i];j++){
   fT[i][j][1] = fT[i-4][j][1];
   }
   }

   Tijk(2,Feta,Feta_2plus,Feta_2minus,Feta_d,Feta_1plus,Fxieta_2minus,Fxieta_2plus,Fxieta_d,Fetaeta_2minus,Fetaeta_2plus,Fetaeta_d,Fetaeta,tau);
   // following B.5
   for(int i=5;i<=6;i++){
   for(int j=1;j<=ki[i];j++){
   fT[i][j][2] = fT[i-4][j][2];
   }
   }
   Tijk(3,Fetaeta,Fetaeta_2plus,Fetaeta_2minus,Fetaeta_d,Fetaeta_1plus,0,0,0,0,0,0,0,tau);
   // following B.5
   for(int i=5;i<=6;i++){
   for(int j=1;j<=ki[i];j++){
   fT[i][j][3] = fT[i-4][j][3];
   }
   }
   */

   // k=1
   fT[1][1][1] =  4.0*(Q2 - 2.0*m*m)*F_IR;
   fT[1][2][1] =  4.0*tau*F_IR;
   fT[1][3][1] = -4.0*F - 2.0*tau*tau*F_d;
   fT[2][1][1] =  2.0*(S*X - M*M*Q2)*F_IR/(M*M);
   fT[2][2][1] = (2.0*m*m*S_p*F_2minus + S_p*S_x*F_1plus + 2.0*(S_x - 2.0*M*M*tau)*F_IR - tau*S_p*S_p*F_d)/(2.0*M*M);
   fT[2][3][1] = (4.0*M*M*F + (4.0*m*m + 2.0*M*M*tau*tau - S_x*tau)*F_d - S_p*F_1plus)/(2.0*M*M);

   fT[3][1][1] = (-8.0*fP_L*m/M)*(etaq*k2xi - Q2*xieta)*F_IR;
   fT[3][2][1] = (2.0*fP_L*m/M)*(
         etaKAPPA*(4.0*m*m*Fxi_d - 4.0*m*m*Fxi_2plus + 
            2.0*Fxi_IR  - Q2*Fxi_2minus + Q2_m*Fxi_2plus ) +
         k2xi*(-8.0*m*m*Feta_d + 4.0*m*m*Feta_2plus + 2.0*etaq*tau*F_d) - 
         4.0*etaq*Fxi_IR + 4.0*xieta *tau*F_IR);
   fT[3][3][1] = (2.0*fP_L*m/M)*(etaKAPPA*tau*(Fxi_2plus - Fxi_2minus - 2.0*Fxi_d) + 
         2.0*k2xi*tau*Feta_d + 4.0*m*m*Fxieta_d + 
         6.0*Fxieta_IR/**/+Q2*Fxieta_2minus - Q2_m*Fxieta_2plus/**/);
   fT[3][4][1] = (-2.0*fP_L*m*tau/M)*(2.0*Fxieta_d/**/+ Fxieta_2plus - Fxieta_2minus/**/);

   fT[4][1][1] = (4.0*m*fP_L/(M*M))*(S_x*k2xi - 2.0*xip *Q2)*F_IR;
   fT[4][2][1] = (m*fP_L/(M*M))*(4.0*m*m*(2.0*k2xi*F_d-k2xi*F_2plus -S_p*Fxi_d + S_p*Fxi_2plus)
         - 2.0*k2xi*tau*S_x*F_d-8.0*xip*tau*F_IR + 
         2.0*(2.0*S_x-S_p)*Fxi_IR+S_p*(Q2*Fxi_2minus-Q2_m*Fxi_2plus));
   fT[4][3][1] = (-m*fP_L/(M*M))*(2.0*Fxi_d*(2.0*m*m-tau*S_p)+2.0*k2xi*tau*F_d + 6.0*Fxi_IR+(Q2-tau*S_p)*Fxi_2minus - (Q2_m-tau*S_p)*Fxi_2plus);
   fT[4][4][1] = (m*fP_L*tau/(M*M))*(2.0*Fxi_d-Fxi_2minus+Fxi_2plus);
   fT[7][1][1] = -2.0*(Q2+4.0*m*m+12.0*etak1*etak2)*F_IR;
   fT[7][2][1] = -2.0*(3.0*etaKAPPA*(2.0*m*m*Feta_2minus - etaKAPPA*tau*F_d+etaq*F_1plus)+6.0*etaq*Feta_IR+tau*F_IR);
   fT[7][3][1] = 2.0*F+tau*tau*F_d+6.0*(etaKAPPA*Feta_1plus+etaq*tau*Feta_d - 4.0*m*m*Fetaeta_d);
   fT[8][1][1] = (-6.0/M)*(S*etak2+X*etak1)*F_IR;
   fT[8][2][1] = (-3.0/M)*(etaKAPPA*(m*m*F_2minus-tau*S_p*F_d)+
         etaq*F_IR+m*m*S_p*Feta_2minus+(S*etak1+X*etak2)*F_1plus+S_x*Feta_IR);
   fT[8][3][1] = (-3.0/(2.0*M))*(8.0*m*m*Feta_d-etaKAPPA*F_1plus-etaq*tau*F_d-S_x*tau*Feta_d-S_p*Feta_1plus);

   // following B.5
   for(int i=5;i<=6;i++){
      for(int j=1;j<=ki[i];j++){
         fT[i][j][1] = fT[i-4][j][1];
      }
   }

   /// Following substitution B.6
   /// Appending an eta , Feta_all->Feta_all
   /// There seems to be no Feta_IR defined so I left it out of the substitution. 
   // k = 2 
   Int_t kk = 1;
   fT[1][1/*+1*/][kk+1] =  4.0*(Q2 - 2.0*m*m)*Feta_IR 
      + qik(1,kk+1,tau)*fT[1][1][kk];
   fT[1][2/*+1*/][kk+1] =  4.0*tau*Feta_IR 
      + qik(1,kk+1,tau)*fT[1][2][kk];
   fT[1][3/*+1*/][kk+1] = -4.0*Feta - 2.0*tau*tau*Feta_d 
      + qik(1,kk+1,tau)*fT[1][3][kk];

   fT[2][1/*+1*/][kk+1] =  2.0*(S*X - M*M*Q2)*Feta_IR/(M*M) 
      + qik(2,kk+1,tau)*fT[2][1][kk];
   fT[2][2/*+1*/][kk+1] = (2.0*m*m*S_p*Feta_2minus + S_p*S_x*Feta_1plus 
         + 2.0*(S_x - 2.0*M*M*tau)*Feta_IR - tau*S_p*S_p*Feta_d)/(2.0*M*M)
      + qik(2,kk+1,tau)*fT[2][2][kk];
   fT[2][3/*+1*/][kk+1] = (4.0*M*M*Feta + (4.0*m*m + 2.0*M*M*tau*tau - S_x*tau)*Feta_d - S_p*Feta_1plus)/(2.0*M*M) 
      + qik(2,kk+1,tau)*fT[2][3][kk];

   fT[3][1/*+1*/][kk+1] = (-8.0*fP_L*m/M)*(etaq*k2xi-Q2*xieta)*Feta_IR
      + qik(3,kk+1,tau)*fT[3][1][kk];
   fT[3][2/*+1*/][kk+1] =  (2.0*fP_L*m/M)*(etaKAPPA*(
            4.0*m*m*Fxieta_d - 4.0*m*m*Fxieta_2plus + 
            2.0*Fxieta_IR    - 
            Q2*Fxieta_2minus + Q2_m*Fxieta_2plus)
         + k2xi*(-8.0*m*m*Fetaeta_d + 
            4.0*m*m*Fetaeta_2plus + 
            2.0*etaq*tau*Feta_d) -
         4.0*etaq*Fxieta_IR + 
         4.0*xieta *tau*Feta_IR)
      + qik(3,kk+1,tau)*fT[3][2][kk];
   fT[3][3/*+1*/][kk+1] = (2.0*fP_L*m/M)*(etaKAPPA*tau*(Fetaeta_2plus- Fetaeta_2minus - 2.0*Fetaeta_d) 
         + 2.0*k2xi*tau*Fetaeta_d + 4.0*m*m*Fxieta_d+
         6.0*Fxieta_IR+Q2*Fxieta_2minus -Q2_m*Fxieta_2plus)
      + qik(3,kk+1,tau)*fT[3][3][kk];
   fT[3][4/*+1*/][kk+1] = (-2.0*fP_L*m*tau/M)*(2.0*Fxieta_d+Fxieta_2plus-Fxieta_2minus)
      + qik(3,kk+1,tau)*fT[3][4][kk];

   //std::cout << "m = " << m << std::endl;
   //std::cout << "P_L = " << fP_L << std::endl;
   //std::cout << "xip = " << xip << std::endl;
   //std::cout << "Feta_IR = " << Feta_IR << std::endl;
   //std::cout << "k2xi = " << k2xi << std::endl;

   fT[4][1/*+1*/][kk+1] = (4.0*m*fP_L/(M*M))*(S_x*k2xi - 2.0*xip *Q2)*Feta_IR
      + qik(4,kk+1,tau)*fT[4][1][kk];
   fT[4][2/*+1*/][kk+1] = (m*fP_L/(M*M))*(4.0*m*m*(2.0*k2xi*Feta_d-k2xi*Feta_2plus-S_p*Fxieta_d+S_p*Fxieta_2plus)
         - 2.0*k2xi*tau*S_x*Feta_d-8.0*xip*tau*Feta_IR+2.0*(2.0*S_x-S_p)*Fxieta_IR+S_p*(Q2*Fxieta_2minus-Q2_m*Fxieta_2plus))
      + qik(4,kk+1,tau)*fT[4][2][kk];
   fT[4][3/*+1*/][kk+1] = (-m*fP_L/(M*M))*(2.0*Fxieta_d*(2.0*m*m-tau*S_p)+2.0*k2xi*tau*Feta_d+6.0*Fxieta_IR+(Q2-tau*S_p)*Fxieta_2minus - (Q2_m-tau*S_p)*Fxieta_2plus)
      + qik(4,kk+1,tau)*fT[4][3][kk];
   fT[4][4/*+1*/][kk+1] = (m*fP_L*tau/(M*M))*(2.0*Fxieta_d-Fxieta_2minus+Fxieta_2plus)
      + qik(4,kk+1,tau)*fT[4][4][kk];

   fT[7][1/*+1*/][kk+1] = -2.0*(Q2+4.0*m*m+12.0*etak1*etak2)*Feta_IR
      + qik(7,kk+1,tau)*fT[7][1][kk];
   fT[7][2/*+1*/][kk+1] = -2.0*(3.0*etaKAPPA*(2.0*m*m*Fetaeta_2minus - etaKAPPA*tau*Feta_d+etaq*Feta_1plus)+6.0*etaq*Fetaeta_IR+tau*Feta_IR)
      + qik(7,kk+1,tau)*fT[7][2][kk];
   fT[7][3/*+1*/][kk+1] = 2.0*Feta + tau*tau*Feta_d + 
      6.0*(etaKAPPA*Fetaeta_1plus+etaq*tau*Fetaeta_d - 4.0*m*m* /*Fetaeta_d*/0.0 )
      + qik(7,kk+1,tau)*fT[7][3][kk];
   fT[8][1/*+1*/][kk+1] = (-6.0/M)*(S*etak2+X*etak1)*Feta_IR
      + qik(8,kk+1,tau)*fT[8][1][kk];
   fT[8][2/*+1*/][kk+1] = (-3.0/M)*(etaKAPPA*(m*m*Feta_2minus-tau*S_p*Feta_d)+ etaq*Feta_IR+m*m*S_p*Fetaeta_2minus+(S*etak1+X*etak2)*Feta_1plus+S_x*Fetaeta_IR)
      + qik(8,kk+1,tau)*fT[8][2][kk];
   fT[8][3/*+1*/][kk+1] = (-3.0/(2.0*M))*(8.0*m*m*Fetaeta_d-etaKAPPA*Feta_1plus-etaq*tau*Feta_d-S_x*tau*Fetaeta_d-S_p*Fetaeta_1plus)
      + qik(8,kk+1,tau)*fT[8][3][kk];


   // following B.5
   for(int i=5;i<=6;i++){
      for(int j=1;j<=ki[i];j++){
         fT[i][j][2] = fT[i-4][j][2];
      }
   }

   /// Following substitution B.6 for k=3 you have to now append two etas 
   /// (which is not clear by their definition)
   /// Note Terms with two upper indices get set to 0
   /// Appending ANOTHER eta to the k=2 terms (above) yeilds:
   kk = 2;
   fT[1][1/*+1*/][kk+1] =  4.0*(Q2 - 2.0*m*m)*Fetaeta_IR 
      + qik(1,kk+1,tau)*fT[1][1][kk];
   fT[1][2/*+1*/][kk+1] =  4.0*tau*Fetaeta_IR 
      + qik(1,kk+1,tau)*fT[1][2][kk];
   fT[1][3/*+1*/][kk+1] = -4.0*Fetaeta - 2.0*tau*tau*Fetaeta_d 
      + qik(1,kk+1,tau)*fT[1][3][kk];

   fT[2][1/*+1*/][kk+1] =  2.0*(S*X - M*M*Q2)*Fetaeta_IR/(M*M) 
      + qik(2,kk+1,tau)*fT[2][1][kk];
   fT[2][2/*+1*/][kk+1] = (2.0*m*m*S_p*Fetaeta_2minus + S_p*S_x*Fetaeta_1plus 
         + 2.0*(S_x - 2.0*M*M*tau)*Fetaeta_IR - tau*S_p*S_p*Fetaeta_d)/(2.0*M*M)
      + qik(2,kk+1,tau)*fT[2][2][kk];
   fT[2][3/*+1*/][kk+1] = (4.0*M*M*Fetaeta + (4.0*m*m + 2.0*M*M*tau*tau - S_x*tau)*Fetaeta_d - S_p*Fetaeta_1plus)/(2.0*M*M) 
      + qik(2,kk+1,tau)*fT[2][3][kk];

   // previously commented out...  
   //   		fT[3][1+1][kk+1] = (-8.0*fP_L*m/M)*(etaq*k2xi-Q2*xieta)*Fetaeta_IR
   //   			              + qik(3,kk+1,tau)*fT[3][1][kk];
   //   		fT[3][2+1][kk+1] =  (2.0*fP_L*m/M)*(etaKAPPA*(
   //   		                     4.0*m*m*0.0/*Fxieta_d*/ - 
   //  		                     4.0*m*m*0.0/*Fxieta_2plus*/ + 
   //   		                     2.0*0.0/*Fxieta_IR*/    - 
   //   		                     Q2*0.0/*Fxieta_2minus*/ + 
   //   		                     Q2_m*0.0/*Fxieta_2plus*/)
   //   	                     + k2xi*(-8.0*m*m*0.0/*Fetaeta_d*/ + 
   //   	                             4.0*m*m*0.0/*Fetaeta_2plus*/ + 
   //   	                             2.0*etaq*tau*Fetaeta_d) -
   //   	                     4.0*etaq*0.0/*Fxieta_IR*/ + 
   //   	                     4.0*xieta *tau*Fetaeta_IR)
   //   			     + qik(3,kk+1,tau)*fT[3][2][kk];
   //  
   //   		fT[3][3+1][kk+1] = (2.0*fP_L*m/M)*(etaKAPPA*tau*(0.0/*Fetaeta_2plus- Fetaeta_2minus - 2.0*Fetaeta_d*/) 
   //   				      + 2.0*k2xi*tau*0.0/*Fetaeta_d*/ + 4.0*m*m*Fxieta_d+
   //   				      6.0*Fxieta_IR+Q2*Fxieta_2minus -Q2_m*Fxieta_2plus)
   //   			             + qik(3,kk+1,tau)*fT[3][3][kk];
   //   		fT[3][4+1][kk+1] = (-2.0*fP_L*m*tau/M)*(2.0*Fxieta_d+Fxieta_2plus-Fxieta_2minus)
   //   			              + qik(3,kk+1,tau)*fT[3][4][kk];
   //  
   //  
   //   		fT[4][1+1][kk+1] = (4.0*m*fP_L/(M*M))*(S_x*k2xi - 2.0*xip *Q2)*Fetaeta_IR
   //   			+ qik(4,kk+1,tau)*fT[4][1][kk];
   //   		fT[4][2+1][kk+1] = (m*fP_L/(M*M))*(4.0*m*m*(2.0*k2xi*Fetaeta_d - 
   //   		                                            k2xi*Fetaeta_2plus - 
   //   		                                            S_p*0.0/*Fxieta_d*/ + 
   //   		                                            S_p*0.0/*Fxieta_2plus*/) -
   //                                                      2.0*k2xi*tau*S_x*Fetaeta_d -
   //                                                      8.0*xip*tau*Fetaeta_IR + 
   //                                                      2.0*(2.0*S_x-S_p)*0.0/*Fxieta_IR*/+
   //                                                      S_p*(Q2*0.0/*Fxieta_2minus*/-Q2_m*0.0/*Fxieta_2plus*/))
   //   			+ qik(4,kk+1,tau)*fT[4][2][kk];
   //   		fT[4][3+1][kk+1] = (-m*fP_L/(M*M))*(2.0*0.0/*Fxieta_d*/*(2.0*m*m-tau*S_p) + 
   //                                                       2.0*k2xi*tau*Fetaeta_d + 
   //                                                       6.0*/*Fxieta_IR*/0.0 + 
   //                                                       (Q2-tau*S_p)*/*Fxieta_2minus*/0.0 - 
   //                                                       (Q2_m-tau*S_p)*/*Fxieta_2plus*/0.0)
   //   			+ qik(4,kk+1,tau)*fT[4][3][kk];
   //   		fT[4][4+1][kk+1] = 0.0/* (m*fP_L*tau/(M*M))*(2.0*Fxieta_d-Fxieta_2minus+Fxieta_2plus)*/
   //   			+ qik(4,kk+1,tau)*fT[4][4][kk];

   // the above was previously commented out...  

   fT[7][1/*+1*/][kk+1] = -2.0*(Q2+4.0*m*m+12.0*etak1*etak2)*Fetaeta_IR
      + qik(7,kk+1,tau)*fT[7][1][kk];
   fT[7][2/*+1*/][kk+1] = -2.0*(3.0*etaKAPPA*(2.0*m*m* /*Fetaeta_2minus*/0.0 -
            etaKAPPA*tau*Fetaeta_d + 
            etaq*Fetaeta_1plus) + 
         6.0*etaq* /*Fetaeta_IR*/0.0 + tau*Fetaeta_IR)
      + qik(7,kk+1,tau)*fT[7][2][kk];
   fT[7][3/*+1*/][kk+1] = 2.0*Fetaeta + tau*tau*Fetaeta_d + 
      6.0*(etaKAPPA* /*Fetaeta_1plus*/0.0+
            etaq*tau* /*Fetaeta_d*/0.0 - 
            4.0*m*m*Fetaeta_d)
      + qik(7,kk+1,tau)*fT[7][3][kk];
   fT[8][1/*+1*/][kk+1] = (-6.0/M)*(S*etak2+X*etak1)*Fetaeta_IR
      + qik(8,kk+1,tau)*fT[8][1][kk];
   fT[8][2/*+1*/][kk+1] = (-3.0/M)*(etaKAPPA*(m*m*Fetaeta_2minus-tau*S_p*Fetaeta_d)+ etaq*Fetaeta_IR+m*m*S_p* /*Fetaeta_2minus*/0.0
         +(S*etak1+X*etak2)*Fetaeta_1plus+S_x* /*Fetaeta_IR*/0.0)
      + qik(8,kk+1,tau)*fT[8][2][kk];
   fT[8][3/*+1*/][kk+1] = (-3.0/(2.0*M))*(8.0*m*m* /*Fetaeta_d*/0.0-etaKAPPA*Fetaeta_1plus-etaq*tau*Fetaeta_d-S_x*tau* /*Fetaeta_d*/0.0-S_p* /*Fetaeta_1plus*/0.0)
      + qik(8,kk+1,tau)*fT[8][3][kk];


   // following B.5
   for(int i=5;i<=6;i++){
      for(int j=1;j<=ki[i];j++){
         fT[i][j][3] = fT[i-4][j][3];
      }
   }


   fReCalculateTs = false;
   //Print();
   //fKinematics.Print(); 
   return(0);
}
//_____________________________________________________________________________
Double_t POLRAD::Tijk_A(Double_t tau) {
   Int_t ni = 9;
   Int_t nj = 6;
   Int_t nk = 4;
   for (Int_t i = 0; i < ni ; ++i) {
      for (Int_t j = 0; j < nj; ++j){
         for(Int_t k = 0; k<nk; ++k) fT[i][j][k] = 0.0;
      }
   }

   // basic terms
   // Double_t mB1 = C1(tau);  
   // Double_t mB2 = C2(tau);  
   // Double_t mC1 = C1(tau);  
   // Double_t mC2 = C2(tau);

   Double_t Lambda_Q = fKinematics.GetLambda_QA();
   Double_t Q2       = fKinematics.GetQ2();
   Double_t Q2_m     = fKinematics.GetQ2_m();
   Double_t m        = fKinematics.Getm();
   Double_t M        = fKinematics.GetM_A();
   Double_t S        = fKinematics.GetS_A();
   Double_t S_x      = fKinematics.GetS_xA();
   Double_t S_p      = fKinematics.GetS_pA();
   Double_t X        = fKinematics.GetX_A();
   Double_t tau_min  = fKinematics.GetTau_A_min();

   Double_t a_eta    = fKinematics.Geta_eta_A();
   Double_t b_eta    = fKinematics.Getb_eta_A();
   Double_t c_eta    = fKinematics.Getc_eta_A();
   Double_t a_xi     = fKinematics.Geta_xi_A();
   Double_t b_xi     = fKinematics.Getb_xi_A();
   Double_t c_xi     = fKinematics.Getc_xi_A();

   seta     = a_eta + b_eta;
   reta     = tau*(a_eta - b_eta) + 2.0*c_eta;
   sxi      = a_xi + b_xi;
   rxi      = tau*(a_xi - b_xi) + 2.0*c_xi;
   F_d      = Fd_A(tau);
   F        = 1.0/TMath::Sqrt(Lambda_Q);
   F_1plus  = 1.0/TMath::Sqrt(C2_A(tau)) + 1.0/TMath::Sqrt(C1_A(tau));
   F_2plus  = B2_A(tau)*TMath::Power(C2_A(tau),-1.5) - B1_A(tau)*TMath::Power(C1_A(tau),-1.5) ;
   F_2minus = B2_A(tau)*TMath::Power(C2_A(tau),-1.5) + B1_A(tau)*TMath::Power(C1_A(tau),-1.5) ;
   //         std::cout << "========================================================" << std::endl;
   //  	std::cout << "tau = " << tau << std::endl;
   //  	std::cout << "C1_A(tau) = " << C1_A(tau) << std::endl;
   //  	std::cout << "C2_A(tau) = " << C2_A(tau) << std::endl;
   //  	std::cout << "B1_A(tau) = " << B1_A(tau) << std::endl;
   //  	std::cout << "B2_A(tau) = " << B2_A(tau) << std::endl;
   //  	std::cout << "TMath::Power(C1_A(tau),-1.5) = " << TMath::Power(C1_A(tau),-1.5) << std::endl;
   //  	std::cout << "TMath::Power(C2_A(tau),-1.5) = " << TMath::Power(C2_A(tau),-1.5) << std::endl;
   F_IR     = m*m*F_2plus - Q2_m*F_d;
   F_i      = -1.0*TMath::Power(Lambda_Q,-1.5)*B1_A(tau);
   F_ii     = (3.0*B1_A(tau)*B1_A(tau) - Lambda_Q*C1_A(tau))/(2.0*TMath::Power(Lambda_Q,2.5));

   // all other terms 
   Feta_2plus  = ((2.0*F_1plus + tau*F_2minus)*seta + F_2plus*reta)/2.0;
   Fxi_2plus   = ((2.0*F_1plus + tau*F_2minus)*sxi + F_2plus*rxi)/2.0;
   Feta_2minus = ((2.0*F_d + F_2plus)*tau*seta + F_2minus*reta)/2.0;
   Fxi_2minus  = ((2.0*F_d + F_2plus)*tau*sxi + F_2minus*rxi)/2.0;
   Feta_d      = (F_1plus*seta + F_d*reta)/2.0;
   Fxi_d       = (F_1plus*sxi + F_d*rxi)/2.0;

   Fetaeta_2plus = ((2.0*F_1plus + tau*F_2minus)*( reta*seta + seta*reta ) +
         F_2plus*( reta*reta + tau*tau*seta*seta) +
         4.0*( 2.0*F + F_d*tau*tau )*seta*seta)/4.0;


   Fetaeta_2minus = ((2.0*F_d + F_2plus)*(reta*seta + seta*reta)+
         F_2minus*( reta*reta + tau*tau*seta*seta) + 
         4.0*tau*F_1plus*seta*seta)/4.0;
   /// This term sucks!
   Fxieta_2plus = ((2.0*F_1plus + tau*F_2minus)*( reta*sxi + seta*rxi ) +
         F_2plus*reta*rxi + tau*tau*seta*sxi*F_2plus + 
         4.0*(2.0*F + F_d*tau*tau)*seta*sxi )/4.0;

   /*
      std::cout << " Fxieta_2plus = " << Fxieta_2plus  << "\n";
      std::cout << "     (F_2plus) = " << ( F_2plus) << "\n";
      std::cout << "     (2.0*F_d ) = " << (2.0*F_d) << "\n";
      std::cout << "     (reta*sxi + seta*rxi) = " << (reta*sxi + seta*rxi)  << "\n";
      std::cout << "     F_2minus  = " << F_2minus  << "\n";
      std::cout << "     (reta*rxi + tau*tau*seta*sxi)  = " << (reta*rxi + tau*tau*seta*sxi)   << "\n";
      std::cout << "     4.0*tau*F_1plus*seta*sxi)/4.0 = " << 4.0*tau*F_1plus*seta*sxi << "\n";
      */



   /// So does this one!!!
   Fxieta_2minus  = ( 2.0*F_d*( reta*sxi + seta*rxi )*tau + 
         F_2plus*(reta*sxi + seta*rxi)*tau +
         F_2minus*reta*rxi + F_2minus*tau*tau*seta*sxi + 
         4.0*tau*F_1plus*seta*sxi)/4.0;

   Fetaeta_d      = (F_1plus*(reta*seta + seta*reta) + 
         F_d*( reta*reta + tau*tau*seta*seta) + 
         4.0*F*seta*seta)/4.0;
   Fxieta_d       =  (F_1plus*(reta*sxi + seta*rxi) + 
         F_d*(reta*rxi + tau*tau*seta*sxi) + 
         4.0*F*seta*sxi)/4.0;

   Feta_1plus     = ((4.0*F+tau*tau*F_d)*seta+F_1plus*reta)/2.0;
   Fetaeta_1plus  = (2.0*(4.0*F + tau*tau*F_d)*reta*seta +
         F_1plus*(reta*reta + tau*tau*seta*seta) +
         4.0*(2.0*F_i-tau*F)*seta*seta)/4.0;

   Feta           = (F*(reta-tau*seta)+2.0*F_i*seta)/2.0;
   Fetaeta        = (F*(reta-tau*seta)*(reta-tau*seta)+
         4.0*F_i*(reta-tau*seta)+4.0*F_ii*seta*seta)/4.0;

   Fxi_IR      = m*m*Fxi_2plus - Q2_m*Fxi_d;
   Feta_IR     = m*m*Feta_2plus - Q2_m*Feta_d;
   Fxieta_IR   = m*m*Fxieta_2plus - Q2_m*Fxieta_d;
   Fetaeta_IR  = m*m*Fetaeta_2plus - Q2_m*Fetaeta_d;

   Double_t etaq     = -Q2*(a_eta-b_eta) + S_x *c_eta;
   Double_t etaKAPPA = (Q2 + 4.0*m*m)*(a_eta+b_eta)+S_p*c_eta;
   Double_t etak1    = (etaKAPPA+etaq)/2.0;
   Double_t etak2    = (etaKAPPA-etaq)/2.0;
   Double_t k2xi     = Q2_m*a_xi+2.0 *m*m*b_xi+ X*c_xi;
   Double_t xip      = S*a_xi + X *b_xi + 2.0*M*M*c_xi;
   Double_t xieta    = 2.0*(2.0*m*m*(a_eta*a_xi + b_eta*b_xi) +
         2.0*M*M*c_eta*c_xi + Q2_m*(a_xi*b_eta+b_xi*a_eta) + 
         S*(a_xi*c_eta+c_xi*a_eta) + 
         X*(b_xi*c_eta+c_xi*b_eta));
   /*
      Tijk(1,F,F_2plus,F_2minus,F_d,F_1plus,Fxi_2minus,Fxi_2plus,Fxi_d,Feta_2minus,Feta_2plus,Feta_d,Feta,tau);
   // following B.5
   for(int i=5;i<=6;i++){
   for(int j=1;j<=ki[i];j++){
   fT[i][j][1] = fT[i-4][j][1];
   }
   }

   Tijk(2,Feta,Feta_2plus,Feta_2minus,Feta_d,Feta_1plus,Fxieta_2minus,Fxieta_2plus,Fxieta_d,Fetaeta_2minus,Fetaeta_2plus,Fetaeta_d,Fetaeta,tau);
   // following B.5
   for(int i=5;i<=6;i++){
   for(int j=1;j<=ki[i];j++){
   fT[i][j][2] = fT[i-4][j][2];
   }
   }
   Tijk(3,Fetaeta,Fetaeta_2plus,Fetaeta_2minus,Fetaeta_d,Fetaeta_1plus,0,0,0,0,0,0,0,tau);
   // following B.5
   for(int i=5;i<=6;i++){
   for(int j=1;j<=ki[i];j++){
   fT[i][j][3] = fT[i-4][j][3];
   }
   }
   */

   // k=1
   fT[1][1][1] =  4.0*(Q2 - 2.0*m*m)*F_IR;
   fT[1][2][1] =  4.0*tau*F_IR;
   fT[1][3][1] = -4.0*F - 2.0*tau*tau*F_d;
   fT[2][1][1] =  2.0*(S*X - M*M*Q2)*F_IR/(M*M);
   fT[2][2][1] = (2.0*m*m*S_p*F_2minus + S_p*S_x*F_1plus + 2.0*(S_x - 2.0*M*M*tau)*F_IR - tau*S_p*S_p*F_d)/(2.0*M*M);
   fT[2][3][1] = (4.0*M*M*F + (4.0*m*m + 2.0*M*M*tau*tau - S_x*tau)*F_d - S_p*F_1plus)/(2.0*M*M);

   fT[3][1][1] = (-8.0*fP_L*m/M)*(etaq*k2xi - Q2*xieta)*F_IR;
   fT[3][2][1] = (2.0*fP_L*m/M)*(
         etaKAPPA*(4.0*m*m*Fxi_d - 4.0*m*m*Fxi_2plus + 
            2.0*Fxi_IR  - Q2*Fxi_2minus + Q2_m*Fxi_2plus ) +
         k2xi*(-8.0*m*m*Feta_d + 4.0*m*m*Feta_2plus + 2.0*etaq*tau*F_d) - 
         4.0*etaq*Fxi_IR + 4.0*xieta *tau*F_IR);
   fT[3][3][1] = (2.0*fP_L*m/M)*(etaKAPPA*tau*(Fxi_2plus - Fxi_2minus - 2.0*Fxi_d) + 
         2.0*k2xi*tau*Feta_d + 4.0*m*m*Fxieta_d + 
         6.0*Fxieta_IR/**/+Q2*Fxieta_2minus - Q2_m*Fxieta_2plus/**/);
   fT[3][4][1] = (-2.0*fP_L*m*tau/M)*(2.0*Fxieta_d/**/+ Fxieta_2plus - Fxieta_2minus/**/);

   fT[4][1][1] = (4.0*m*fP_L/(M*M))*(S_x*k2xi - 2.0*xip *Q2)*F_IR;
   fT[4][2][1] = (m*fP_L/(M*M))*(4.0*m*m*(2.0*k2xi*F_d-k2xi*F_2plus -S_p*Fxi_d + S_p*Fxi_2plus)
         - 2.0*k2xi*tau*S_x*F_d-8.0*xip*tau*F_IR + 
         2.0*(2.0*S_x-S_p)*Fxi_IR+S_p*(Q2*Fxi_2minus-Q2_m*Fxi_2plus));
   fT[4][3][1] = (-m*fP_L/(M*M))*(2.0*Fxi_d*(2.0*m*m-tau*S_p)+2.0*k2xi*tau*F_d + 6.0*Fxi_IR+(Q2-tau*S_p)*Fxi_2minus - (Q2_m-tau*S_p)*Fxi_2plus);
   fT[4][4][1] = (m*fP_L*tau/(M*M))*(2.0*Fxi_d-Fxi_2minus+Fxi_2plus);
   fT[7][1][1] = -2.0*(Q2+4.0*m*m+12.0*etak1*etak2)*F_IR;
   fT[7][2][1] = -2.0*(3.0*etaKAPPA*(2.0*m*m*Feta_2minus - etaKAPPA*tau*F_d+etaq*F_1plus)+6.0*etaq*Feta_IR+tau*F_IR);
   fT[7][3][1] = 2.0*F+tau*tau*F_d+6.0*(etaKAPPA*Feta_1plus+etaq*tau*Feta_d - 4.0*m*m*Fetaeta_d);
   fT[8][1][1] = (-6.0/M)*(S*etak2+X*etak1)*F_IR;
   fT[8][2][1] = (-3.0/M)*(etaKAPPA*(m*m*F_2minus-tau*S_p*F_d)+
         etaq*F_IR+m*m*S_p*Feta_2minus+(S*etak1+X*etak2)*F_1plus+S_x*Feta_IR);
   fT[8][3][1] = (-3.0/(2.0*M))*(8.0*m*m*Feta_d-etaKAPPA*F_1plus-etaq*tau*F_d-S_x*tau*Feta_d-S_p*Feta_1plus);

   // following B.5
   for(int i=5;i<=6;i++){
      for(int j=1;j<=ki[i];j++){
         fT[i][j][1] = fT[i-4][j][1];
      }
   }

   /// Following substitution B.6
   /// Appending an eta , Feta_all->Feta_all
   /// There seems to be no Feta_IR defined so I left it out of the substitution. 
   // k = 2 
   for(int kk=1;kk<=1;kk++){
      fT[1][1/*+1*/][kk+1] =  4.0*(Q2 - 2.0*m*m)*Feta_IR 
         + qik(1,kk+1,tau)*fT[1][1][kk];
      fT[1][2/*+1*/][kk+1] =  4.0*tau*Feta_IR 
         + qik(1,kk+1,tau)*fT[1][2][kk];
      fT[1][3/*+1*/][kk+1] = -4.0*Feta - 2.0*tau*tau*Feta_d 
         + qik(1,kk+1,tau)*fT[1][3][kk];

      fT[2][1/*+1*/][kk+1] =  2.0*(S*X - M*M*Q2)*Feta_IR/(M*M) 
         + qik(2,kk+1,tau)*fT[2][1][kk];
      fT[2][2/*+1*/][kk+1] = (2.0*m*m*S_p*Feta_2minus + S_p*S_x*Feta_1plus 
            + 2.0*(S_x - 2.0*M*M*tau)*Feta_IR - tau*S_p*S_p*Feta_d)/(2.0*M*M)
         + qik(2,kk+1,tau)*fT[2][2][kk];
      fT[2][3/*+1*/][kk+1] = (4.0*M*M*Feta + (4.0*m*m + 2.0*M*M*tau*tau - S_x*tau)*Feta_d - S_p*Feta_1plus)/(2.0*M*M) 
         + qik(2,kk+1,tau)*fT[2][3][kk];

      // previously commented out...  

      fT[3][1/*+1*/][kk+1] = (-8.0*fP_L*m/M)*(etaq*k2xi-Q2*xieta)*Feta_IR
         + qik(3,kk+1,tau)*fT[3][1][kk];
      fT[3][2/*+1*/][kk+1] =  (2.0*fP_L*m/M)*(etaKAPPA*(
               4.0*m*m*Fxieta_d - 4.0*m*m*Fxieta_2plus + 
               2.0*Fxieta_IR    - 
               Q2*Fxieta_2minus + Q2_m*Fxieta_2plus)
            + k2xi*(-8.0*m*m*Fetaeta_d + 
               4.0*m*m*Fetaeta_2plus + 
               2.0*etaq*tau*Feta_d) -
            4.0*etaq*Fxieta_IR + 
            4.0*xieta *tau*Feta_IR)
         + qik(3,kk+1,tau)*fT[3][2][kk];
      fT[3][3/*+1*/][kk+1] = (2.0*fP_L*m/M)*(etaKAPPA*tau*(Fetaeta_2plus- Fetaeta_2minus - 2.0*Fetaeta_d) 
            + 2.0*k2xi*tau*Fetaeta_d + 4.0*m*m*Fxieta_d+
            6.0*Fxieta_IR+Q2*Fxieta_2minus -Q2_m*Fxieta_2plus)
         + qik(3,kk+1,tau)*fT[3][3][kk];
      fT[3][4/*+1*/][kk+1] = (-2.0*fP_L*m*tau/M)*(2.0*Fxieta_d+Fxieta_2plus-Fxieta_2minus)
         + qik(3,kk+1,tau)*fT[3][4][kk];

      fT[4][1/*+1*/][kk+1] = (4.0*m*fP_L/(M*M))*(S_x*k2xi - 2.0*xip *Q2)*Feta_IR
         + qik(4,kk+1,tau)*fT[4][1][kk];
      fT[4][2/*+1*/][kk+1] = (m*fP_L/(M*M))*(4.0*m*m*(2.0*k2xi*Feta_d-k2xi*Feta_2plus-S_p*Fxieta_d+S_p*Fxieta_2plus)
            - 2.0*k2xi*tau*S_x*Feta_d-8.0*xip*tau*Feta_IR+2.0*(2.0*S_x-S_p)*Fxieta_IR+S_p*(Q2*Fxieta_2minus-Q2_m*Fxieta_2plus))
         + qik(4,kk+1,tau)*fT[4][2][kk];
      fT[4][3/*+1*/][kk+1] = (-m*fP_L/(M*M))*(2.0*Fxieta_d*(2.0*m*m-tau*S_p)+2.0*k2xi*tau*Feta_d+6.0*Fxieta_IR+(Q2-tau*S_p)*Fxieta_2minus - (Q2_m-tau*S_p)*Fxieta_2plus)
         + qik(4,kk+1,tau)*fT[4][3][kk];
      fT[4][4/*+1*/][kk+1] = (m*fP_L*tau/(M*M))*(2.0*Fxieta_d-Fxieta_2minus+Fxieta_2plus)
         + qik(4,kk+1,tau)*fT[4][4][kk];

      // the above was previously commented out...  

      // setting i = 7,8 terms to zero for now, since that's done in fortran... 
      fT[7][1/*+1*/][kk+1] = -2.0*(Q2+4.0*m*m+12.0*etak1*etak2)*Feta_IR
         + qik(7,kk+1,tau)*fT[7][1][kk];
      fT[7][2/*+1*/][kk+1] = -2.0*(3.0*etaKAPPA*(2.0*m*m*Fetaeta_2minus - etaKAPPA*tau*Feta_d+etaq*Feta_1plus)+6.0*etaq*Fetaeta_IR+tau*Feta_IR)
         + qik(7,kk+1,tau)*fT[7][2][kk];
      fT[7][3/*+1*/][kk+1] = 2.0*Feta + tau*tau*Feta_d + 
         6.0*(etaKAPPA*Fetaeta_1plus+etaq*tau*Fetaeta_d - 4.0*m*m* /*Fetaeta_d*/0.0 )
         + qik(7,kk+1,tau)*fT[7][3][kk];
      fT[8][1/*+1*/][kk+1] = (-6.0/M)*(S*etak2+X*etak1)*Feta_IR
         + qik(8,kk+1,tau)*fT[8][1][kk];
      fT[8][2/*+1*/][kk+1] = (-3.0/M)*(etaKAPPA*(m*m*Feta_2minus-tau*S_p*Feta_d)+ etaq*Feta_IR+m*m*S_p*Fetaeta_2minus+(S*etak1+X*etak2)*Feta_1plus+S_x*Fetaeta_IR)
         + qik(8,kk+1,tau)*fT[8][2][kk];
      fT[8][3/*+1*/][kk+1] = (-3.0/(2.0*M))*(8.0*m*m*Fetaeta_d-etaKAPPA*Feta_1plus-etaq*tau*Feta_d-S_x*tau*Fetaeta_d-S_p*Fetaeta_1plus)
         + qik(8,kk+1,tau)*fT[8][3][kk];

   }


   // 	// following B.5
   for(int i=5;i<=6;i++){
      for(int j=1;j<=ki[i];j++){
         fT[i][j][2] = fT[i-4][j][2];
      }
   }

   /// Following substitution B.6 for k=3 you have to now append two etas 
   /// (which is not clear by their definition)
   /// Note Terms with two upper indices get set to 0
   /// Appending ANOTHER eta to the k=2 terms (above) yeilds:

   for(int kk=2;kk<=2;kk++){ /* k = kk+1*/
      fT[1][1/*+1*/][kk+1] =  4.0*(Q2 - 2.0*m*m)*Fetaeta_IR 
         + qik(1,kk+1,tau)*fT[1][1][kk];
      fT[1][2/*+1*/][kk+1] =  4.0*tau*Fetaeta_IR 
         + qik(1,kk+1,tau)*fT[1][2][kk];
      fT[1][3/*+1*/][kk+1] = -4.0*Fetaeta - 2.0*tau*tau*Fetaeta_d 
         + qik(1,kk+1,tau)*fT[1][3][kk];

      fT[2][1/*+1*/][kk+1] =  2.0*(S*X - M*M*Q2)*Fetaeta_IR/(M*M) 
         + qik(2,kk+1,tau)*fT[2][1][kk];
      fT[2][2/*+1*/][kk+1] = (2.0*m*m*S_p*Fetaeta_2minus + S_p*S_x*Fetaeta_1plus 
            + 2.0*(S_x - 2.0*M*M*tau)*Fetaeta_IR - tau*S_p*S_p*Fetaeta_d)/(2.0*M*M)
         + qik(2,kk+1,tau)*fT[2][2][kk];
      fT[2][3/*+1*/][kk+1] = (4.0*M*M*Fetaeta + (4.0*m*m + 2.0*M*M*tau*tau - S_x*tau)*Fetaeta_d - S_p*Fetaeta_1plus)/(2.0*M*M) 
         + qik(2,kk+1,tau)*fT[2][3][kk];

      // previously commented out...  
      //   		fT[3][1+1][kk+1] = (-8.0*fP_L*m/M)*(etaq*k2xi-Q2*xieta)*Fetaeta_IR
      //   			              + qik(3,kk+1,tau)*fT[3][1][kk];
      //   		fT[3][2+1][kk+1] =  (2.0*fP_L*m/M)*(etaKAPPA*(
      //   		                     4.0*m*m*0.0/*Fxieta_d*/ - 
      //  		                     4.0*m*m*0.0/*Fxieta_2plus*/ + 
      //   		                     2.0*0.0/*Fxieta_IR*/    - 
      //   		                     Q2*0.0/*Fxieta_2minus*/ + 
      //   		                     Q2_m*0.0/*Fxieta_2plus*/)
      //   	                     + k2xi*(-8.0*m*m*0.0/*Fetaeta_d*/ + 
      //   	                             4.0*m*m*0.0/*Fetaeta_2plus*/ + 
      //   	                             2.0*etaq*tau*Fetaeta_d) -
      //   	                     4.0*etaq*0.0/*Fxieta_IR*/ + 
      //   	                     4.0*xieta *tau*Fetaeta_IR)
      //   			     + qik(3,kk+1,tau)*fT[3][2][kk];
      //  
      //   		fT[3][3+1][kk+1] = (2.0*fP_L*m/M)*(etaKAPPA*tau*(0.0/*Fetaeta_2plus- Fetaeta_2minus - 2.0*Fetaeta_d*/) 
      //   				      + 2.0*k2xi*tau*0.0/*Fetaeta_d*/ + 4.0*m*m*Fxieta_d+
      //   				      6.0*Fxieta_IR+Q2*Fxieta_2minus -Q2_m*Fxieta_2plus)
      //   			             + qik(3,kk+1,tau)*fT[3][3][kk];
      //   		fT[3][4+1][kk+1] = (-2.0*fP_L*m*tau/M)*(2.0*Fxieta_d+Fxieta_2plus-Fxieta_2minus)
      //   			              + qik(3,kk+1,tau)*fT[3][4][kk];
      //  
      //  
      //   		fT[4][1+1][kk+1] = (4.0*m*fP_L/(M*M))*(S_x*k2xi - 2.0*xip *Q2)*Fetaeta_IR
      //   			+ qik(4,kk+1,tau)*fT[4][1][kk];
      //   		fT[4][2+1][kk+1] = (m*fP_L/(M*M))*(4.0*m*m*(2.0*k2xi*Fetaeta_d - 
      //   		                                            k2xi*Fetaeta_2plus - 
      //   		                                            S_p*0.0/*Fxieta_d*/ + 
      //   		                                            S_p*0.0/*Fxieta_2plus*/) -
      //                                                      2.0*k2xi*tau*S_x*Fetaeta_d -
      //                                                      8.0*xip*tau*Fetaeta_IR + 
      //                                                      2.0*(2.0*S_x-S_p)*0.0/*Fxieta_IR*/+
      //                                                      S_p*(Q2*0.0/*Fxieta_2minus*/-Q2_m*0.0/*Fxieta_2plus*/))
      //   			+ qik(4,kk+1,tau)*fT[4][2][kk];
      //   		fT[4][3+1][kk+1] = (-m*fP_L/(M*M))*(2.0*0.0/*Fxieta_d*/*(2.0*m*m-tau*S_p) + 
      //                                                       2.0*k2xi*tau*Fetaeta_d + 
      //                                                       6.0*/*Fxieta_IR*/0.0 + 
      //                                                       (Q2-tau*S_p)*/*Fxieta_2minus*/0.0 - 
      //                                                       (Q2_m-tau*S_p)*/*Fxieta_2plus*/0.0)
      //   			+ qik(4,kk+1,tau)*fT[4][3][kk];
      //   		fT[4][4+1][kk+1] = 0.0/* (m*fP_L*tau/(M*M))*(2.0*Fxieta_d-Fxieta_2minus+Fxieta_2plus)*/
      //   			+ qik(4,kk+1,tau)*fT[4][4][kk];

      // the above was previously commented out...  

      fT[7][1+1][kk+1] = -2.0*(Q2+4.0*m*m+12.0*etak1*etak2)*Fetaeta_IR
         + qik(7,kk+1,tau)*fT[7][1][kk];
      fT[7][2+1][kk+1] = -2.0*(3.0*etaKAPPA*(2.0*m*m* /*Fetaeta_2minus*/0.0 -
               etaKAPPA*tau*Fetaeta_d + 
               etaq*Fetaeta_1plus) + 
            6.0*etaq* /*Fetaeta_IR*/0.0 + tau*Fetaeta_IR)
         + qik(7,kk+1,tau)*fT[7][2][kk];
      fT[7][3+1][kk+1] = 2.0*Fetaeta + tau*tau*Fetaeta_d + 
         6.0*(etaKAPPA* /*Fetaeta_1plus*/0.0+
               etaq*tau* /*Fetaeta_d*/0.0 - 
               4.0*m*m*Fetaeta_d)
         + qik(7,kk+1,tau)*fT[7][3][kk];
      fT[8][1+1][kk+1] = (-6.0/M)*(S*etak2+X*etak1)*Fetaeta_IR
         + qik(8,kk+1,tau)*fT[8][1][kk];
      fT[8][2+1][kk+1] = (-3.0/M)*(etaKAPPA*(m*m*Fetaeta_2minus-tau*S_p*Fetaeta_d)+ etaq*Fetaeta_IR+m*m*S_p* /*Fetaeta_2minus*/0.0
            +(S*etak1+X*etak2)*Fetaeta_1plus+S_x* /*Fetaeta_IR*/0.0)
         + qik(8,kk+1,tau)*fT[8][2][kk];
      fT[8][3+1][kk+1] = (-3.0/(2.0*M))*(8.0*m*m* /*Fetaeta_d*/0.0-etaKAPPA*Fetaeta_1plus-etaq*tau*Fetaeta_d-S_x*tau* /*Fetaeta_d*/0.0-S_p* /*Fetaeta_1plus*/0.0)
         + qik(8,kk+1,tau)*fT[8][3][kk];

   }

   // following B.5
   for(int i=5;i<=6;i++){
      for(int j=1;j<=ki[i];j++){
         fT[i][j][3] = fT[i-4][j][3];
      }
   }


   fReCalculateTs = false;
   //Print();
   //fKinematics.Print(); 
   return(0);
}
//_____________________________________________________________________________
void POLRAD::ProcessFancyElasticStructureFunctions(Double_t R,Double_t tau,Double_t *el)  {

   // Strictly speaking, R = Rel, tau = tau_A
   Double_t Q2    = fKinematics.GetQ2();
   Double_t x     = fKinematics.Getx_A();
   Double_t MT    = fKinematics.GetM_A();
   // evaluate structure functions and form factors at x_pr and t  
   Double_t x_pr  = x*(Q2 + R*tau)/(Q2 - R*x);
   Double_t t     = Q2 + R*tau;

   //if(fDebug>2){
   //   std::cout << "R  = " << R  << "\t" << "tau = " << tau  << std::endl;
   //   std::cout << "Q2 = " << Q2 << "\t" << "Q2' = " << t    << std::endl;
   //   std::cout << "x  = " << x  << "\t" << "x'  = " << x_pr << std::endl;
   //   std::cout << "t  = " << t  << "\t" << "MT  = " << MT << std::endl;
   //}

   ProcessStructureFunctions(x_pr,t);
   ProcessFormFactors(t);

   // needed variables: 
   Double_t Eta_A = t/(4.*MT*MT);  

   fArbValue5 = Eta_A; 
   fArbValue6 = t; 

   // elastic terms 
   // we start at index 1 to agree with the paper
   for(Int_t i=0;i<9;i++) el[i] = 0.;

   // spin 1 targets 
   if(fspin2==2) {
      el[0] = 0; 
      el[1] = (Eta_A/6.)*fFm*fFm*( 4.*(1.+Eta_A) + Eta_A*fQ_N);  
      el[2] = (fFc*fFc + (2./3.)*Eta_A*fFm*fFm + (8./9.)*Eta_A*Eta_A*fFq*fFq) 
         + (fQ_N/6.)*( Eta_A*fFm*fFm + ( (4.*Eta_A*Eta_A)/(1.+Eta_A) )*( (Eta_A/3.)*fFq + fFc - fFm)*fFq);
      el[3] = (-fP_N/2.)*(1.+Eta_A)*fFm*( (Eta_A/3.)*fFq + fFc );
      el[4] = (fP_N/4.)*fFm*( 0.5*fFm - fFc - (Eta_A/3.)*fFq ); 
      el[5] = (fQ_N/24.)*fFm*fFm; 
      el[6] = (fQ_N/24.)*( fFm*fFm + ( 4./(1+Eta_A) )*( (Eta_A/3.)*fFq + fFc + Eta_A*fFm )*fFq ); 
      el[7] = (fQ_N/6.)*Eta_A*(1.+Eta_A)*fFm*fFm; 
      el[8] = (-fQ_N/6.)*Eta_A*fFm*(fFm + 2.*fFq);
   }
   // for spin-1/2 targets
   if(fspin2==1) {
      //std::cout << " spin 1/2 " << std::endl;
      //std::cout << fP_N << std::endl;
      el[0] = 0; 
      el[1] = fZ*fZ*Eta_A*fGm*fGm;
      el[2] = fZ*fZ*(fGe*fGe + Eta_A*fGm*fGm)/(1.+Eta_A);
      el[3] = (fP_N/2.)*fZ*fZ*fGm*fGe; 
      el[4] = (fP_N/4.)*fZ*fZ*fGm*(fGe-fGm)/(1.+Eta_A);
   }

   //if(fDebug > 2){
   //   std::cout << "Fancy elastic form factors : " << std::endl;
   //   for(int i=0;i<9;i++){
   //      std::cout << "el[" << i << "]: " << Form("%.3E",el[i]) << std::endl;
   //   }
   //}
}
//_____________________________________________________________________________
void POLRAD::ProcessFancyQuasiElasticStructureFunctions(Double_t R,Double_t tau,Double_t *qe)  {

   // evaluate structure functions and form factors 
   Double_t M    = fKinematics.GetM();
   Double_t x    = fKinematics.Getx();
   Double_t S_x  = fKinematics.GetS_x(); 
   Double_t Q2   = fKinematics.GetQ2();
   Double_t x_pr = x*(Q2 + R*tau)/(Q2 - R*x); 
   Double_t t    = Q2 + R*tau; 
   // ProcessStructureFunctions(x_pr,t);  // is this needed? 
   ProcessFormFactors(t);   

   Double_t Eps = 2.*M*M/(S_x - R); 
   //fArbValue7   = Eps; 

   // needed variables: 
   Double_t eta = t/(4.*M*M);
   // See ffquas subroutine in POLRAD for the following
   // Assuming the validity of y-scaling, S_M = S_E = S_EM = F(nu_q) 
   // paper reference: arXiv:hep-ph/0006079, pg. 4   
   Double_t S_M  = 0;                 // Magnetic smearing term 
   Double_t S_E  = 0;                 // Electric smearing term 
   Double_t S_EM = 0;                 // Electromagnetic smearing term 
   Double_t Q,Q2_fm,q,Fq; 
   Double_t chbar = 1.0;//hbarc_gev_fm;     // c*hbar in GeV*fm /// NOT SURE IF THIS IS NEEDED -whit

   if( fTargetNucleus == Nucleus::Proton() ) {
      S_E   = 0.; 
      S_M   = 0.; 
      S_EM  = 0.; 
   } else if( fTargetNucleus == Nucleus::Neutron() ) {
      S_E   = 0.; 
      S_M   = 0.; 
      S_EM  = 0.; 
   //} else if( fTargetNucleus == Nucleus::Triton() ) {
   //   S_E   = 1.; 
   //   S_M   = 1.; 
   //   S_EM  = 1.; 
   } else if( fTargetNucleus == Nucleus::Deuteron() ) {
      // From Phys. Rev. D 12, 1884 (1975)  
      Q2_fm = t/TMath::Power(chbar,2.0);                     // Q2 in fm^-2 
      q     = TMath::Sqrt(Q2_fm);
      Fq    = (1.58/q)*( TMath::ATan(q/0.930) - 2.*TMath::ATan(q/3.19) + TMath::ATan(q/5.45) );
      S_E   = TMath::Sqrt(1. - Fq*Fq);
      S_M   = S_E; 
      S_EM  = S_E;
   } else if( fTargetNucleus == Nucleus::He3() ) {
      // paper reference: Adv. in Phys 15, 57 (1966) ??  
      Q = TMath::Sqrt(TMath::Abs(t))/fPF; 
      if( TMath::Abs(t) < TMath::Power(2.*fPF,2.) ){
         S_M  = 0.75*Q - (1./16.)*TMath::Power(Q,3.);    
         S_E  = S_M;                                           
         S_EM = S_E;                                        
      }else{
         S_M  = 1.0;
         S_E  = 1.0;
         S_EM = S_E;                                        
      }
   } else if( fTargetNucleus == Nucleus::He4() ) {
      S_E   = 1.0; 
      S_M   = 1.0; 
      S_EM  = 1.0; 
   } else if( fTargetNucleus == Nucleus::Fe56() ) {
      // whatever...
      S_E   = 1.0; 
      S_M   = 1.0; 
      S_EM  = 1.0; 
   } else {
      S_E   = 1.0; 
      S_M   = 1.0; 
      S_EM  = 1.0; 
   }

   // std::cout << "t = " << t << std::endl;
   // std::cout << "Q = " << Q << std::endl;
   // std::cout << "eta = " << eta << std::endl;
   // std::cout << "fm_n = " << fm_n << std::endl;
   // std::cout << "fm_p = " << fm_p << std::endl;
   // std::cout << "fe_n = " << fe_n << std::endl;
   // std::cout << "fe_p = " << fe_p << std::endl;
   // std::cout << "fP_p = " << fP_p << std::endl;
   // std::cout << "fP_n = " << fP_n << std::endl;
   // std::cout << "S_M = " << S_M << std::endl;
   // std::cout << "S_E = " << S_E << std::endl;
   // std::cout << "S_EM = " << S_EM << std::endl;
   // quasi-elastic terms
   // we start at index 1 to agree with the paper

   for(int i=0;i<9;i++) qe[i] = 0;

   if( fQRT_IntegrationMethod == kFull ) {

      ProcessStructureFunctions(x_pr,t);  // is this needed? 
      /// Full calculation: same functional form as inelastic terms  
      /// \todo Need to implement polarized SFs for quasi-elastic case (full calculation)    
      qe[0] = 0; 
      qe[1] = fF1qe + (fQ_N/6.)*fb1; 
      qe[2] = Eps*(fF2qe + (fQ_N/6.)*fb2qe);
      qe[3] = fP_N*Eps*(fg1qe + fg2qe);
      qe[4] = fP_N*Eps*Eps*fg2qe; 
      qe[5] = (fQ_N/6.)*Eps*Eps*fb1qe; 
      qe[6] = (fQ_N/6.)*Eps*Eps*Eps*( (fb2qe/3) + fb3qe + fb4qe);
      qe[7] = (fQ_N/6.)*Eps*( (fb2qe/3.) - fb3qe); 
      qe[8] = (fQ_N/6.)*Eps*Eps*( (fb2qe/3.) - fb4qe);
      //qe[0] = 0; 
      //qe[1] = fF1qe;       // + (fQ_N/6.)*fb1; 
      //qe[2] = Eps*(fF2qe); // + (fQ_N/6.)*fb2);
      //qe[3] = 0.;          // fP_N*Eps*(fg1 + fg2);
      //qe[4] = 0.;          // fP_N*Eps*Eps*fg2; 
      //qe[5] = 0.;          // (fQ_N/6.)*Eps*Eps*fb1; 
      //qe[6] = 0.;          // (fQ_N/6.)*Eps*Eps*Eps*( (fb2/3.0) + fb3 + fb4);
      //qe[7] = 0.;          // (fQ_N/6.)*Eps*( (fb2/3.) - fb3); 
      //qe[8] = 0.;          // (fQ_N/6.)*Eps*Eps*( (fb2/3.) - fb4);

   } else { 

      qe[1] = eta*( (fA-fZ)*fm_n*fm_n + fZ*fm_p*fm_p)*S_M;
      qe[2] = ( eta*( (fA-fZ)*fm_n*fm_n + fZ*fm_p*fm_p)*S_M + ( (fA-fZ)*fe_n*fe_n + fZ*fe_p*fe_p)*S_E )/(1.+eta); 
      qe[3] = (fP_N/2.)*( (fA-fZ)*fP_n*fe_n*fm_n + fZ*fP_p*fe_p*fm_p)*S_EM;
      qe[4] = (fP_N/(4.*(1.+eta)))*( ( (fA-fZ)*fP_n*fe_n*fm_n + fZ*fP_p*fe_p*fm_p)*S_EM - ( (fA-fZ)*fP_n*fm_n*fm_n + fZ*fP_p*fm_p*fm_p)*S_M );

   }

   // for some reason, this is what the fortran uses... 
   // qe[3] = 0; 
   // qe[4] = fP_N*(-2.*M*M/(1.+eta))*fm_n*fm_n*(fA-fZ)*S_M;
   //std::cout << "qe[1] = " << qe[1] << std::endl;
   //std::cout << "qe[2] = " << qe[2] << std::endl;
   //std::cout << "qe[3] = " << qe[3] << std::endl;
   //std::cout << "qe[4] = " << qe[4] << std::endl;

}
//_____________________________________________________________________________
void POLRAD::ProcessFancyInelasticStructureFunctions(Double_t R,Double_t tau,Double_t *in,Double_t x, Double_t Q2)  {

   // evaluate structure functions and form factors 
   Double_t x_pr  = x*(Q2 + R*tau)/(Q2 - R*x); 
   Double_t t     = Q2 + R*tau; 
   Double_t S_x   = Q2/x;
   Double_t M     = fKinematics.GetM();
   if(fDebug>3){
      std::cout << "ProcessFancyInelasticStructureFunctions" << std::endl;
      std::cout << "R  = " << R  << "\t" << "tau = " << tau  << std::endl;
      std::cout << "Q2 = " << Q2 << "\t" << "Q2' = " << t    << std::endl;
      std::cout << "x  = " << x  << "\t" << "x'  = " << x_pr << std::endl;
   } 
   // x_pr takes the place of a x integration variable 
   // t takes the place of a Q2 integration variable
   ProcessStructureFunctions(x_pr,t);   
   //ProcessFormFactors(t);   

   // needed variables: 
   Double_t Eps = 2.*M*M/(S_x-R); 
   // inelastic terms 
   // we start at index 1 to agree with the paper
   in[0] = 0; 
   in[1] = fF1 + (fQ_N/6.)*fb1; 
   in[2] = Eps*(fF2 + (fQ_N/6.)*fb2);
   in[3] = fP_N*Eps*(fg1 + fg2);
   in[4] = fP_N*Eps*Eps*fg2; 
   in[5] = (fQ_N/6.)*Eps*Eps*fb1; 
   in[6] = (fQ_N/6.)*Eps*Eps*Eps*( (fb2/3.0) + fb3 + fb4);
   in[7] = (fQ_N/6.)*Eps*( (fb2/3.) - fb3); 
   in[8] = (fQ_N/6.)*Eps*Eps*( (fb2/3.) - fb4);

}
//_____________________________________________________________________________
void POLRAD::ProcessFancyInelasticStructureFunctions(Double_t R,Double_t tau,Double_t *in)  {

   // evaluate structure functions and form factors 
   Double_t M   = fKinematics.GetM();
   Double_t x   = fKinematics.Getx();
   Double_t Q2  = fKinematics.GetQ2();
   Double_t S_x = fKinematics.GetS_x();

   Double_t x_pr = x*(Q2 + R*tau)/(Q2 - R*x); 
   Double_t t    = Q2 + R*tau;
   //std::cout << "x = " << x_pr << "\n"; 
   //std::cout << "t = " << t << "\n"; 
   // x_pr takes the place of a x integration variable 
   // t takes the place of a Q2 integration variable
   ProcessStructureFunctions(x_pr,t);   
   //ProcessFormFactors(t);   

   // needed variables: 
   Double_t Eps = 2.*M*M/(S_x-R); 
   fArbValue7 = Eps; 

   // inelastic terms 
   // we start at index 1 to agree with the paper
   in[0] = 0; 
   in[1] = fF1 + (fQ_N/6.)*fb1; 
   in[2] = Eps*(fF2 + (fQ_N/6.)*fb2);
   in[3] = fP_N*Eps*(fg1 + fg2);
   in[4] = fP_N*Eps*Eps*fg2; 
   in[5] = (fQ_N/6.)*Eps*Eps*fb1; 
   in[6] = (fQ_N/6.)*Eps*Eps*Eps*( (fb2/3) + fb3 + fb4);
   in[7] = (fQ_N/6.)*Eps*( (fb2/3.) - fb3); 
   in[8] = (fQ_N/6.)*Eps*Eps*( (fb2/3.) - fb4);

}
//____________________________________________________________________________________
void POLRAD::ProcessStructureFunctions(Double_t x, Double_t Q2)  {

   if( fTargetNucleus == Nucleus::Proton() ) {
         fF1   = fUnpolSFs->F1p(x,Q2);
         fF2   = fUnpolSFs->F2p(x,Q2);
         fF1qe = 0.;  
         fF2qe = 0.;  
         fg1   = fPolSFs->g1p(x,Q2);
         fg2   = fPolSFs->g2p(x,Q2);
         fg1qe = 0.0;//fPolSFs->g1d(x,Q2);
         fg2qe = 0.0;//fPolSFs->g2d(x,Q2);
         fb1   = 0.;
         fb2   = 0.;
         fb3   = 0.;
         fb4   = 0.;
         fb1qe = 0.;
         fb2qe = 0.;
         fb3qe = 0.;
         fb4qe = 0.;
   } else if( fTargetNucleus == Nucleus::Neutron() ) {
         fF1   = fUnpolSFs->F1n(x,Q2);
         fF2   = fUnpolSFs->F2n(x,Q2);
         fF1qe = 0.;  
         fF2qe = 0.;  
         fg1   = fPolSFs->g1n(x,Q2);
         fg2   = fPolSFs->g2n(x,Q2);
         fg1qe = 0.0;//fPolSFs->g1d(x,Q2);
         fg2qe = 0.0;//fPolSFs->g2d(x,Q2);
         fb1   = 0.;
         fb2   = 0.;
         fb3   = 0.;
         fb4   = 0.;
         fb1qe = 0.;
         fb2qe = 0.;
         fb3qe = 0.;
         fb4qe = 0.;
   } else if( fTargetNucleus == Nucleus::Deuteron() ) {
         fF1   = fUnpolSFs->F1d(x,Q2);
         fF2   = fUnpolSFs->F2d(x,Q2);
         fF1qe = fUnpolQESFs->F1d(x,Q2);
         fF2qe = fUnpolQESFs->F2d(x,Q2);
         fg1   = fPolSFs->g1d(x,Q2);
         fg2   = fPolSFs->g2d(x,Q2);
         fg1qe = 0.0;//fPolSFs->g1d(x,Q2);
         fg2qe = 0.0;//fPolSFs->g2d(x,Q2);
         fb1   = 0.;     // FIXME: b_i != 0 for deuterons!    
         fb2   = 0.;     // FIXME: b_i != 0 for deuterons!
         fb3   = 0.;     // FIXME: b_i != 0 for deuterons!
         fb4   = 0.;     // FIXME: b_i != 0 for deuterons!
         fb1qe = 0.;
         fb2qe = 0.;
         fb3qe = 0.;
         fb4qe = 0.;
   } else if( fTargetNucleus == Nucleus::He3() ) {
         fF1   = fUnpolSFs->F1He3(x,Q2);
         fF2   = fUnpolSFs->F2He3(x,Q2);
         fF1qe = fUnpolQESFs->F1He3(x,Q2);
         fF2qe = fUnpolQESFs->F2He3(x,Q2);
         fg1   = fPolSFs->g1He3(x,Q2);
         fg2   = fPolSFs->g2He3(x,Q2);
         fg1qe = 0.0;//fPolSFs->g1d(x,Q2);
         fg2qe = 0.0;//fPolSFs->g2d(x,Q2);
         fb1   = 0.;
         fb2   = 0.;
         fb3   = 0.;
         fb4   = 0.;
         fb1qe = 0.;
         fb2qe = 0.;
         fb3qe = 0.;
         fb4qe = 0.;
   } else {
         std::cout << "[POLRAD::ProcessStructureFunctions]: ";
         std::cout << "Invalid target type! Using proton..." << std::endl;
         fF1   = fUnpolSFs->F1p(x,Q2);
         fF2   = fUnpolSFs->F2p(x,Q2);
         fF1qe = 0.;  
         fF2qe = 0.;  
         fg1   = fPolSFs->g1p(x,Q2);
         fg2   = fPolSFs->g2p(x,Q2);
         fg1qe = 0.0;//fPolSFs->g1d(x,Q2);
         fg2qe = 0.0;//fPolSFs->g2d(x,Q2);
         fb1   = 0.;
         fb2   = 0.;
         fb3   = 0.;
         fb4   = 0.;
         fb1qe = 0.;
         fb2qe = 0.;
         fb3qe = 0.;
         fb4qe = 0.;
   }

}
//_____________________________________________________________________________
void POLRAD::ProcessFormFactors(Double_t Q2) {

   // // WARNING: we apply sqrt(F) to agree with ROSETAIL.  Why do they do that?? 
   // Double_t Ftilde = FTilde(Q2); 
   // Double_t sqrtF  = TMath::Sqrt(Ftilde);

   // proton 
   fe_p = fFFs->GEp(Q2);
   fm_p = fFFs->GMp(Q2);
   // neutron
   fe_n = fFFs->GEn(Q2);
   fm_n = fFFs->GMn(Q2);
   // deuteron 
   fFc  = fFFs->GEd(Q2); 
   fFm  = fFFs->GMd(Q2); 
   fFq  = 0;                  // FIXME: quadrupole form factor  
   // target 
   if( fTargetNucleus == Nucleus::Proton() ) {
         fGe = fFFs->GEp(Q2);
         fGm = fFFs->GMp(Q2);
   } else if( fTargetNucleus == Nucleus::Neutron() ) {
         fGe = fFFs->GEn(Q2);
         fGm = fFFs->GMn(Q2);
   } else if( fTargetNucleus == Nucleus::Deuteron() ) {
         fGe = fFFs->GEd(Q2);
         fGm = fFFs->GMd(Q2);
   } else if( fTargetNucleus == Nucleus::He3() ) {
         // Note we use fFFTs here not fFFs
         fGe = fFFTs->GEHe3(Q2);
         fGm = fFFTs->GMHe3(Q2);		
   } else {
         fGe = fFFs->GENuclear(Q2,fTargetNucleus.GetZ(),fTargetNucleus.GetA());
         fGm = fFFs->GMNuclear(Q2,fTargetNucleus.GetZ(),fTargetNucleus.GetA());
   }

   //if(fDebug>4){
   //   std::cout << "Q2 = " << Q2  << std::endl;
   //   std::cout << "GE = " << fGe << std::endl;
   //   std::cout << "GM = " << fGm << std::endl;
   //}

}
//_____________________________________________________________________________
Double_t POLRAD::Spence(Double_t x) {

   Double_t ans = (-1.)*TMath::DiLog(-x); 

   if(fDebug > 5){
      std::cout << "SPENCE" << std::endl;
      std::cout << "Phi(" << x << ") = " << ans << std::endl;
   }

   return ans;

}
//_____________________________________________________________________________
Double_t POLRAD::S_phi(){

   /// S_phi from the fortran code
   /// See deltas subroutine, sfpr term   
   Double_t S       = fKinematics.GetS();
   Double_t X       = fKinematics.GetX();
   Double_t S_prime = fKinematics.GetS_prime(); 
   Double_t X_prime = fKinematics.GetX_prime(); 
   Double_t Q2      = fKinematics.GetQ2(); 
   Double_t W2      = fKinematics.GetW2(); 
   Double_t l_m     = fKinematics.Getl_m(); 
   Double_t m       = fKinematics.Getm(); 
   Double_t M       = fKinematics.GetM(); 
   Double_t T1      = 0.5*l_m*l_m - l_m*TMath::Log( (S_prime*X_prime)/(m*m*W2) );  // delta_inf 
   Double_t T2      = -0.5*TMath::Power(TMath::Log(S_prime/X_prime),2.);           // sum of two spence functions (see SLAC-PUB-0848) 
   Double_t T3      = Spence( (S*X - Q2*M*M)/(S_prime*X_prime) ) - pi2/3.;         // Schwinger term    
   Double_t SPhi    = T1 + T2 + T3; 
   return SPhi;  

}
//_____________________________________________________________________________
Double_t POLRAD::S_phi(Double_t s,Double_t lambda,Double_t a,Double_t b) {

   /// S_phi from the J Phys G Nucl Part Phys 20, 513 (1994) 
   /// Eq 24

   Double_t delta[5] = {0,1,1,-1,-1};
   Double_t a_j=0,gamma_k_j=0,tau_j=0;  
   Double_t arg1=0,arg2=0;
   Double_t Sphi_u=0,Sphi_1=0,Sphi=0; 

   if(fDebug > 5 ) std::cout << "S_phi" << std::endl;

   // gamma_i 
   Double_t gamma[3] = { 0.,(-1.0/(b-lambda))*TMath::Power(TMath::Sqrt(b)-TMath::Sqrt(lambda),2.0), 
      (1.0/(b-lambda))*TMath::Power(TMath::Sqrt(b)+TMath::Sqrt(lambda),2.0)}; 
   Double_t gamma_u  = (TMath::Sqrt(b+lambda) - TMath::Sqrt(lambda) )/TMath::Sqrt(lambda); 
   Double_t T1       = (s/(2.*TMath::Sqrt(lambda))); 
   Double_t D        = (s+a)*(lambda*a - s*b) + (1./4.)*TMath::Power(lambda+b,2.0); 

   if(fDebug > 5){
      std::cout << "s = "       << s       << std::endl;
      std::cout << "lambda = "  << lambda  << std::endl;
      std::cout << "a = "       << a       << std::endl;
      std::cout << "b = "       << b       << std::endl;
      std::cout << "gamma_u = " << gamma_u << std::endl;
      std::cout << "T1 = "      << T1      << std::endl;
      std::cout << "D = "       << D       << std::endl;
      for(int i=0;i<3;i++) std::cout << "gamma[" << i << "] = " << gamma[i] << std::endl; 
   }

   // Eq. 24 of J Phys G 20, 513 (1994) 
   // Sphi evaluated at gamma = gamma_u 
   for(int i=1;i<=2;i++){
      for(int j=1;j<=4;j++){
         for(int k=1;k<=2;k++){
            a_j       = s - delta[j]*TMath::Sqrt(lambda);
            tau_j     = -a*TMath::Sqrt(lambda) + 0.5*delta[j]*(b-lambda) + TMath::Power(-1.,j)*TMath::Sqrt(D); 
            if(k==1) gamma_k_j = (-1./tau_j)*(a_j*TMath::Sqrt(b) + TMath::Sqrt(b*a_j*a_j + tau_j*tau_j) );  
            if(k==2) gamma_k_j = (-1./tau_j)*(a_j*TMath::Sqrt(b) - TMath::Sqrt(b*a_j*a_j + tau_j*tau_j) );  
            arg1      = (gamma[i] - gamma_u)/( gamma[i] - gamma_k_j ); 
            arg2      = (gamma_u + TMath::Power(-1.,i) )/( gamma_k_j + TMath::Power(-1.,i) ); 
            Sphi_u   += TMath::Power(-1.,i)*delta[j]*( Spence(arg1) + Spence(arg2) );
         }
      }
   }
   // Sphi evaluated at gamma = gamma[1]  
   for(int i=1;i<=2;i++){
      for(int j=1;j<=4;j++){
         for(int k=1;k<=2;k++){
            a_j       = s - delta[j]*TMath::Sqrt(lambda);
            tau_j     = -a*TMath::Sqrt(lambda) + 0.5*delta[j]*(b-lambda) + TMath::Power(-1.,j)*TMath::Sqrt(D); 
            if(k==1) gamma_k_j = (-1./tau_j)*(a_j*TMath::Sqrt(b) + TMath::Sqrt(b*a_j*a_j + tau_j*tau_j) );  
            if(k==2) gamma_k_j = (-1./tau_j)*(a_j*TMath::Sqrt(b) - TMath::Sqrt(b*a_j*a_j + tau_j*tau_j) );  
            arg1      = (gamma[i] - gamma[1])/( gamma[i] - gamma_k_j ); 
            arg2      = (gamma[1] + TMath::Power(-1.,i) )/( gamma_k_j + TMath::Power(-1.,i) ); 
            Sphi_1   += TMath::Power(-1.,i)*delta[j]*( Spence(arg1) + Spence(arg2) );
         }
      }
   }       

   if(fDebug > 5 ) std::cout << "Sphi_u = " << Sphi_u << "\t" << "Sphi_1 = " << Sphi_1 << std::endl;

   Sphi = T1*(Sphi_u - Sphi_1);
   return Sphi;

}
//______________________________________________________________________________
Double_t POLRAD::Delta_sp(){

   Double_t A     = 1. - fKinematics.Getz_p();
   Double_t B     = 1. - fKinematics.Getz_s();
   Double_t delta = 2.*TMath::Log(A*B) + 3.;
   return delta;

}
//____________________________________________________________________________________
Double_t POLRAD::sigma_b(){

   /// Elastic radiative tail [external] 
   /// Angle peaking approximation 
   /// Phys Rev D 12 1884 (1975), Eq A49  
   Double_t Es      = fKinematics.GetEs(); 
   Double_t Ep      = fKinematics.GetEp(); 
   Double_t th      = fKinematics.GetTheta();
   Double_t SIN     = TMath::Sin(th/2.); 
   Double_t SIN2    = SIN*SIN;
   Double_t MT      = fKinematics.GetM_A(); 
   Double_t m       = fKinematics.Getm();
   Double_t alpha   = fine_structure_const;  
   // omega_s 
   Double_t num_s   = Ep;
   Double_t den_s   = 1. - 2.*(Ep/MT)*SIN2;
   Double_t omega_s = Es - (num_s/den_s);
   // omega_p 
   Double_t num_p   = Es;
   Double_t den_p   = 1. + 2.*(Es/MT)*SIN2;
   Double_t omega_p = (num_p/den_p) - Ep;
   // v terms 
   Double_t v_s     = omega_s/Es;
   Double_t v_p     = omega_p/(Ep + omega_p);
   Double_t phi_s   = 1. - v_s + (3./4.)*v_s*v_s; // phi(v_s);
   Double_t phi_p   = 1. - v_p + (3./4.)*v_p*v_p; // phi(v_p);
   // Z terms 
   Double_t Z       = fZ;  
   Double_t Z13     = TMath::Power(Z,-1./3); 
   Double_t Z23     = TMath::Power(Z,-2./3); 
   // eta (Eq A46 in Stein et al.) 
   Double_t eta     = TMath::Log(1440.*Z23)/TMath::Log(183.*Z13); 
   // b (Eq A45 in Stein et al.)  
   Double_t b_num   = (1./9.)*(Z+1.)/(Z+eta); 
   Double_t b_den   = TMath::Log(183.*Z13);
   Double_t b       = (4./3.)*(1. + b_num/b_den); 
   // xi 
   Double_t xi_num  = pi*m*(ft_a + ft_b);
   Double_t xi_den  = (2.*alpha)*( (Z+eta)*TMath::Log(183.*Z13) ); 
   Double_t xi      = xi_num/xi_den;  
   // put it together  
   Double_t num     = MT + 2.*(Es - omega_s)*SIN2;
   Double_t den     = MT - 2.*Ep*SIN2;
   Double_t T1      = num/den;
   Double_t T2      = sigma_el_tilde(Es-omega_s,th);
   Double_t T3      = b*ft_b*phi_s/omega_s + xi/(2.*omega_s*omega_s);
   Double_t T4      = sigma_el_tilde(Es,th);
   Double_t T5      = b*ft_a*phi_p/omega_p + xi/(2.*omega_p*omega_p);
   Double_t sig     = T1*T2*T3 + T4*T5;

   // Need to apply a jacobian to convert to dsig/dxdy:
   Double_t J       = Ep/( 2.0*pi*MT*(Es-Ep) );
   sig *= 1./J; 

   // std::cout << "=========== sigma_b ===========" << std::endl;
   // std::cout << "eta   = " << eta   << std::endl;
   // std::cout << "b     = " << b     << std::endl;
   // std::cout << "xi    = " << xi    << std::endl;
   // std::cout << "t_b   = " << ft_b  << std::endl;
   // std::cout << "t_a   = " << ft_a  << std::endl;
   // std::cout << "phi_s = " << phi_s << std::endl;
   // std::cout << "phi_p = " << phi_p << std::endl;
   // std::cout << "T1    = " << T1    << std::endl;
   // std::cout << "T2    = " << T2    << std::endl;
   // std::cout << "T3    = " << T3    << std::endl;
   // std::cout << "T4    = " << T4    << std::endl;
   // std::cout << "T5    = " << T5    << std::endl;

   return sig;
}
//____________________________________________________________________________________
Double_t POLRAD::sigma_el_tilde(Double_t Es,Double_t theta){

   /// Elastic cross section 
   /// Phys Rev D 12 1884 (1975), Eq A55  
   Double_t alpha   = fine_structure_const;  
   Double_t MT      = fKinematics.GetM_A(); 
   Double_t COS     = TMath::Cos(theta/2.);
   Double_t SIN     = TMath::Sin(theta/2.);
   Double_t COS2    = COS*COS;
   Double_t SIN2    = SIN*SIN;
   Double_t TAN2    = SIN2/COS2;
   Double_t Ep      = Es/(1.+2.*Es*SIN2/MT);
   Double_t Q2      = insane::Kine::Qsquared(Es,Ep,theta);
   Double_t f_tilde = FTilde(Q2); 
   // form factors 
   ProcessFormFactors(Q2);
   Double_t tau     = Q2/(4.*MT*MT);
   Double_t GE      = fGe;  
   Double_t GM      = fGm; 
   Double_t W1      = tau*GM*GM;                           
   Double_t W2      = (GE*GE + tau*GM*GM)/(1.+tau);  

   // Mott XS
   Double_t num    = alpha*alpha*COS*COS;
   Double_t den    = 4.*Es*Es*SIN2*SIN2;
   Double_t MottXS = num/den;
   // put it together  
   Double_t T1     = MottXS*(Ep/Es);
   Double_t T2     = W2 + 2.*TAN2*W1;
   Double_t sig    = f_tilde*T1*T2;

   // std::cout << "=========== sigma_el ===========" << std::endl;
   // std::cout << "Es = " << Es << std::endl;
   // std::cout << "Ep = " << Ep << std::endl;
   // std::cout << "Q2 = " << Q2 << std::endl;
   // std::cout << "W1 = " << W1 << std::endl;
   // std::cout << "W2 = " << W2 << std::endl;
   // std::cout << "T1 = " << T1 << std::endl;
   // std::cout << "T2 = " << T2 << std::endl;

   return sig;

}
//____________________________________________________________________________________
Double_t POLRAD::FTilde(Double_t Q2){

   Double_t alpha = fine_structure_const; 
   Double_t m     = fKinematics.Getm();
   Double_t Es    = fKinematics.GetEs();  
   Double_t Ep    = fKinematics.GetEp();  
   Double_t th    = fKinematics.GetTheta();
   Double_t COS   = TMath::Cos(th/2.); 
   Double_t COS2  = COS*COS;  
   Double_t T     = ft_a + ft_b;
   Double_t Phi   = Spence(COS2);
   // Z terms 
   Double_t Z     = fZ; 
   Double_t Z13   = TMath::Power(Z,-1./3); 
   Double_t Z23   = TMath::Power(Z,-2./3); 
   // eta (Eq A46 in Stein et al.) 
   Double_t eta   = TMath::Log(1440.*Z23)/TMath::Log(183.*Z13); 
   // b (Eq A45 in Stein et al.)  
   Double_t b_num = (1./9.)*(Z+1.)/(Z+eta); 
   Double_t b_den = TMath::Log(183.*Z13);
   Double_t b     = (4./3.)*(1. + b_num/b_den); 
   // put it together 
   Double_t F     = 1. + 0.5772*b*T 
      + (2.*alpha/pi)*( -(14./9.) + (13/12.)*TMath::Log( Q2/(m*m) ) )
      - (alpha/(2.*pi))*TMath::Power(TMath::Log(Es/Ep),2.) 
      + (alpha/pi)*( (pi*pi/6. - Phi) );

   // std::cout << "FTilde(Q2 = " << Q2 <<" = " << F << std::endl; 

   return F; 

}
//_____________________________________________________________________________
void POLRAD::Print(Option_t * ) const  {
   std::cout << "------------------- POLRAD Variables -------------------" << std::endl;
   /*std::cout << "x:              " << fx                                              << std::endl;
     std::cout << "y:              " << fy                                              << std::endl;
     std::cout << "Q2:             " << fQ2            << " GeV^2"                      << std::endl;
     std::cout << "S:              " << fS             << " GeV^2"                      << std::endl;
     std::cout << "X:              " << fX             << " GeV^2"                      << std::endl;
     std::cout << "S_x:            " << fS_x           << " GeV^2"                      << std::endl;
     std::cout << "S_p:            " << fS_p           << " GeV^2"                      << std::endl;
     std::cout << "Q2_m:           " << fQ2_m          << " GeV^2"                      << std::endl; 
     std::cout << "W2:             " << fW2            << " GeV^2"                      << std::endl;
     std::cout << "Lambda_s:       " << fLambda_s                                       << std::endl;
     std::cout << "Lambda_Q:       " << fLambda_Q                                       << std::endl;
     std::cout << "tau:            " << fTau                                            << std::endl;
     std::cout << "tau_min:        " << fTau_min                                        << std::endl;
     std::cout << "tau_max:        " << fTau_max                                        << std::endl;
     */
   std::cout << "F_2plus:        " << F_2plus                                         << std::endl;
   std::cout << "Feta_2plus:     " << Feta_2plus                                      << std::endl;                    
   std::cout << "Fxi_2plus:      " << Fxi_2plus                                       << std::endl;                       
   std::cout << "Fxieta_2plus:   " << Fxieta_2plus                                    << std::endl;                                  
   std::cout << "Fetaeta_2plus:  " << Fetaeta_2plus                                   << std::endl;                              
   std::cout << "F_2minus:       " << F_2minus                                        << std::endl;                   
   std::cout << "Feta_2minus:    " << Feta_2minus                                     << std::endl;                                  
   std::cout << "Fxi_2minus:     " << Fxi_2minus                                      << std::endl;                        
   std::cout << "Fxieta_2minus:  " << Fxieta_2minus                                   << std::endl;                                      
   std::cout << "Fetaeta_2minus: " << Fetaeta_2minus                                  << std::endl;                             
   std::cout << "F_d :           " << F_d                                             << std::endl;
   std::cout << "Feta_d:         " << Feta_d                                          << std::endl;                               
   std::cout << "Fxi_d:          " << Fxi_d                                           << std::endl;                          
   std::cout << "Fetaeta_d:      " << Fetaeta_d                                       << std::endl;                                      
   std::cout << "Fxieta_d:       " << Fxieta_d                                        << std::endl;                                     
   std::cout << "F_1plus:        " << F_1plus                                         << std::endl;
   std::cout << "Feta_1plus:     " << Feta_1plus                                      << std::endl;                                 
   std::cout << "Fetaeta_1plus:  " << Fetaeta_1plus                                   << std::endl;                                       
   std::cout << "F:              " << F                                               << std::endl;
   std::cout << "Feta:           " << Feta                                            << std::endl;                 
   std::cout << "Fetaeta:        " << Fetaeta                                         << std::endl;                                
   std::cout << "F_IR:           " << F_IR                                            << std::endl;
   std::cout << "Feta_IR:        " << Feta_IR                                         << std::endl;        
   std::cout << "Fxi_IR:         " << Fxi_IR                                          << std::endl;        
   std::cout << "Fxieta_IR:      " << Fxieta_IR                                       << std::endl;        
   std::cout << "Fetaeta_IR:     " << Fetaeta_IR                                      << std::endl;      
   std::cout << "F_i:            " << F_i                                             << std::endl;                
   std::cout << "F_ii:           " << F_ii                                            << std::endl;         
   // std::cout << "fa_L:           " << fa_L                                            << std::endl;
   // std::cout << "fb_L:           " << fb_L                                            << std::endl;
   // std::cout << "fc_L:           " << fc_L                                            << std::endl;
   // std::cout << "fa_T:           " << fa_T                                            << std::endl;
   // std::cout << "fb_T:           " << fb_T                                            << std::endl;
   // std::cout << "fc_T:           " << fc_T                                            << std::endl;
   // std::cout << "sxi:            " << sxi                                             << std::endl;                         
   // std::cout << "rxi:            " << rxi                                             << std::endl;                       
   std::cout << "sxi:            " << sxi                                             << std::endl;
   std::cout << "rxi:            " << rxi                                             << std::endl;
   std::cout << "seta:           " << seta                                            << std::endl;
   std::cout << "reta:           " << reta                                            << std::endl;
   std::cout << "-------------------   POLRAD Physics Variables  -------------------" << std::endl;
   // std::cout << "electron mass:                     " << fm        << " GeV"          << std::endl; 
   // std::cout << "pion mass:                         " << fM_pion   << " GeV"          << std::endl; 
   // std::cout << "target mass:                       " << fM        << " GeV"          << std::endl;
   std::cout << "2*target spin:                       " << fspin2                       << std::endl;
   // std::cout << "target atomic mass:                " << fA        << " g/mol"        << std::endl;
   // std::cout << "target atomic number:              " << fZ                           << std::endl; 
   std::cout << "target polarization:               " << fP_N                         << std::endl;
   std::cout << "target quadrupole moment:          " << fQ_N                         << std::endl;
   std::cout << "lepton polarization:               " << fP_L                         << std::endl;
   std::cout << "degree of polarization of proton:  " << fP_p                         << std::endl;
   std::cout << "degree of polarization of neutron: " << fP_n                         << std::endl;
   // std::cout << "-------------------   POLRAD SFs and FFs  -------------------" << std::endl;
   // PrintSF();
   //         fXi.Print();
   //         fEta.Print();
}

//______________________________________________________________________________
Double_t POLRAD::sig_dis_born(Double_t S,Double_t X,Double_t Q2)  {

   // Eq. 3: DIS Born cross section 
   //fq = fk1 - fk2;
   Double_t m         = fKinematics.Getm();
   Double_t M         = fKinematics.GetM();
   Double_t Q2_m      = fKinematics.GetQ2_m();

   if( fKinematics.GetW2() < M*M ) return(0.0);

   Double_t a_eta    = fKinematics.Geta_eta();
   Double_t b_eta    = fKinematics.Getb_eta();
   Double_t c_eta    = fKinematics.Getc_eta();
   Double_t a_xi     = fKinematics.Geta_xi();
   Double_t b_xi     = fKinematics.Getb_xi();
   Double_t c_xi     = fKinematics.Getc_xi();

   TLorentzVector k1  = fKinematics.Getk1();
   TLorentzVector k2  = fKinematics.Getk2();
   TLorentzVector P   = fKinematics.GetP();
   TLorentzVector Xi  = fKinematics.GetXi();
   TLorentzVector Eta = fKinematics.GetEta();
   TLorentzVector q   = fKinematics.Getq();

   Double_t S_x          = fKinematics.GetS_x(); 
   Double_t S_p          = fKinematics.GetS_p();
   Double_t Lambda_s     = fKinematics.GetLambda_s(); 
   Double_t y            = fKinematics.Gety(); 
   Double_t x            = fKinematics.Getx(); 

   // Using the four vectors defined
   Double_t eta_dot_q       = Eta*q;
   Double_t eta_dot_Kappa   = Eta*(k1 + k2);  /// Kappa = k1 + k2
   Double_t eta_dot_k1      = Eta*k1;
   Double_t eta_dot_k2      = Eta*k2;
   Double_t k2_dot_xi       = Xi*k2;
   Double_t xi_dot_P        = Xi*P;
   Double_t xi_dot_eta      = Xi*Eta;

   Double_t sig_0 = 0; 
   Double_t alpha = fine_structure_const;  
   Double_t A     = (4.*pi*alpha*alpha/Lambda_s)*(S*S_x/(Q2*Q2));
   Double_t N[9],T[9];

   Double_t R   = 0;         // We want to evaluate SF and FF at (x,Q2), choose R   = 0 
   Double_t tau = 0;         // We want to evaluate SF and FF at (x,Q2), choose tau = 0
   // ProcessFancyInelasticStructureFunctions(R,tau,ffSFin,x,Q2);
   ProcessFancyInelasticStructureFunctions(R,tau,ffSFin);

   // coefficients are N*T
   N[0] = 0; 
   T[0] = 0; 

   N[1] = 1.0; 
   T[1] = (Q2 - 2.*m*m)*ffSFin[1]; 

   N[2] = 1./(2.*M*M); 
   T[2] = (S*X - M*M*Q2)*ffSFin[2];

   N[3] = 2.*m*M*fP_L/(M*M);  
   T[3] = ( Q2*(xi_dot_eta) - (eta_dot_q)*(k2_dot_xi) )*ffSFin[3]; 

   N[4] = m*M*fP_L/(M*M*M*M); 
   T[4] = (S_x*(k2_dot_xi) - 2.*Q2*(xi_dot_P) )*(eta_dot_q)*ffSFin[4]; 

   N[5] = 1./(M*M); 
   T[5] = (Q2 - 2.*m*m)*( Q2 - 3.*(eta_dot_q)*(eta_dot_q) )*ffSFin[5];

   N[6] = 1./(2.*M*M*M*M); 
   T[6] = (S*X - M*M*Q2)*( Q2 - 3.*(eta_dot_q)*(eta_dot_q) )*ffSFin[6]; 

   N[7] = -0.5; 
   T[7] = ( Q2 + 4.*m*m + 12.*(eta_dot_k1)*(eta_dot_k2) )*ffSFin[7]; 

   N[8] = -3./(2.*M*M); 
   T[8] = ( X*(eta_dot_k1) + S*(eta_dot_k2) )*(eta_dot_q)*ffSFin[8];  

   for(int i=1;i<9;i++){
      sig_0 += A*N[i]*T[i]; 
   }

   if(fDebug > 3) {
      // std::cout << " Q2(local) = " << Q2 << ", fQ2 = " << fQ2 << "\n";
      // std::cout << " x(local) = " << Q2 << ", fx = " << fx << "\n";
      // std::endl;
      for(int i=1;i<9;i++){
         std::cout << "i: " << i << ", sf: " << Form("%.3E",ffSFin[i]);
         std::cout << ", N[i]:     " << Form("%.3E",N[i]);
         std::cout << ", T[i]:     " << Form("%.3E",T[i]);
         std::cout << ", sig_0_i:  " << Form("%.3E",A*N[i]*T[i]) << std::endl;
      }
   }

   // Eqn 9 assuming b1=b2=0, 
   //         Double_t sig_long_ultra = ((4.0*pi*alpha*alpha*S)/(Q2*Q2))*
   //                                   (fUnpolSFs->F1p(x,Q2)*x*y*y + fUnpolSFs->F2p(x,Q2)*(1.0-y)
   //                                    - fP_N*fP_L*x*y*(2.0-y)*fPolSFs->g1p(x,Q2) );

   // std::cout << " ----- \n";
   // std::cout << "sig_long_ultra = " << sig_long_ultra << "\n";
   // std::cout << " ----- \n";
   //return sig_long_ultra;
   return sig_0;

}
//______________________________________________________________________________
Double_t POLRAD::sig_el_born(Double_t S,Double_t Q2)  {

   // Eq. 3: Elastic Born cross section 
   // Same as born for inelastic but uses the fancy structure functions of the appendix.
   //
   // Assumes that the kinematics for Elastic scattering have already been solved. 
   // and set using SetKinematics to the appropriate Ebeam,Eprime,theta 
   // which conserves energy and momentum.
    
   //fq = fk1 - fk2;
   Double_t m         = fKinematics.Getm();
   Double_t M         = fKinematics.GetM();
   Double_t Q2_m      = fKinematics.GetQ2_m();

   Double_t X        = fKinematics.GetX();
   Double_t a_eta    = fKinematics.Geta_eta();
   Double_t b_eta    = fKinematics.Getb_eta();
   Double_t c_eta    = fKinematics.Getc_eta();
   Double_t a_xi     = fKinematics.Geta_xi();
   Double_t b_xi     = fKinematics.Getb_xi();
   Double_t c_xi     = fKinematics.Getc_xi();

   TLorentzVector k1  = fKinematics.Getk1();
   TLorentzVector k2  = fKinematics.Getk2();
   TLorentzVector P   = fKinematics.GetP();
   TLorentzVector Xi  = fKinematics.GetXi();
   TLorentzVector Eta = fKinematics.GetEta();
   TLorentzVector q   = fKinematics.Getq();

   Double_t S_x          = fKinematics.GetS_x(); 
   Double_t S_p          = fKinematics.GetS_p();
   Double_t Lambda_s     = fKinematics.GetLambda_s(); 
   Double_t y            = fKinematics.Gety(); 
   Double_t x            = fKinematics.Getx(); 

   // Using the four vectors defined
   Double_t eta_dot_q       = Eta*q;
   Double_t eta_dot_Kappa   = Eta*(k1 + k2);  /// Kappa = k1 + k2
   Double_t eta_dot_k1      = Eta*k1;
   Double_t eta_dot_k2      = Eta*k2;
   Double_t k2_dot_xi       = Xi*k2;
   Double_t xi_dot_P        = Xi*P;
   Double_t xi_dot_eta      = Xi*Eta;

   Double_t sig_0 = 0; 
   Double_t alpha = fine_structure_const;  
   Double_t A     = (4.*pi*alpha*alpha/Lambda_s)*(S*S_x/(Q2*Q2));
   Double_t N[9],T[9];

   Double_t R   = 0;         // We want to evaluate SF and FF at (x,Q2), choose R   = 0 
   Double_t tau = 0;         // We want to evaluate SF and FF at (x,Q2), choose tau = 0
   ProcessFancyElasticStructureFunctions(R,tau,ffSFin);

   // coefficients are N*T
   N[0] = 0; 
   T[0] = 0; 

   N[1] = 1.0; 
   T[1] = (Q2 - 2.*m*m)*ffSFin[1]; 

   N[2] = 1./(2.*M*M); 
   T[2] = (S*X - M*M*Q2)*ffSFin[2];

   N[3] = 2.*m*M*fP_L/(M*M);  
   T[3] = ( Q2*(xi_dot_eta) - (eta_dot_q)*(k2_dot_xi) )*ffSFin[3]; 

   N[4] = m*M*fP_L/(M*M*M*M); 
   T[4] = (S_x*(k2_dot_xi) - 2.*Q2*(xi_dot_P) )*(eta_dot_q)*ffSFin[4]; 

   N[5] = 1./(M*M); 
   T[5] = (Q2 - 2.*m*m)*( Q2 - 3.*(eta_dot_q)*(eta_dot_q) )*ffSFin[5];

   N[6] = 1./(2.*M*M*M*M); 
   T[6] = (S*X - M*M*Q2)*( Q2 - 3.*(eta_dot_q)*(eta_dot_q) )*ffSFin[6]; 

   N[7] = -0.5; 
   T[7] = ( Q2 + 4.*m*m + 12.*(eta_dot_k1)*(eta_dot_k2) )*ffSFin[7]; 

   N[8] = -3./(2.*M*M); 
   T[8] = ( X*(eta_dot_k1) + S*(eta_dot_k2) )*(eta_dot_q)*ffSFin[8];  

   for(int i=1;i<9;i++){
      sig_0 += A*N[i]*T[i]; 
   }

   if(fDebug > 3) {
      // std::cout << " Q2(local) = " << Q2 << ", fQ2 = " << fQ2 << "\n";
      // std::cout << " x(local) = " << Q2 << ", fx = " << fx << "\n";
      // std::endl;
      for(int i=1;i<9;i++){
         std::cout << "i: " << i << ", sf: " << Form("%.3E",ffSFin[i]);
         std::cout << ", N[i]:     " << Form("%.3E",N[i]);
         std::cout << ", T[i]:     " << Form("%.3E",T[i]);
         std::cout << ", sig_0_i:  " << Form("%.3E",A*N[i]*T[i]) << std::endl;
      }
   }

   // Eqn 9 assuming b1=b2=0, 
   //         Double_t sig_long_ultra = ((4.0*pi*alpha*alpha*S)/(Q2*Q2))*
   //                                   (fUnpolSFs->F1p(x,Q2)*x*y*y + fUnpolSFs->F2p(x,Q2)*(1.0-y)
   //                                    - fP_N*fP_L*x*y*(2.0-y)*fPolSFs->g1p(x,Q2) );

   // std::cout << " ----- \n";
   // std::cout << "sig_long_ultra = " << sig_long_ultra << "\n";
   // std::cout << " ----- \n";
   //return sig_long_ultra;
   return sig_0;

}
//______________________________________________________________________________
Double_t POLRAD::IRT() {

   // Sec. 2.1.2, Eq. 12: Inelastic radiative tail (IRT)  
   // FIXME: Check multiple photon contribution! 

   if( fKinematics.GetW2() < fKinematics.GetM()*fKinematics.GetM() ) return 0.0;
   Double_t S           = fKinematics.GetS(); 
   Double_t X           = fKinematics.GetX(); 
   Double_t Q2          = fKinematics.GetQ2(); 
   Double_t alpha       = fine_structure_const;  
   Double_t sig_born    = sig_dis_born(S,X,Q2); 
   Double_t sig_F       = 0.0;

   if( fIRT_IntegrationMethod == kAnglePeaking ) {
      sig_F = sig_F_in_angle_peaking(); 
   } else {
      sig_F = sig_F_in_2(); 
   }
   //Double_t sig_F       = sig_F_in_2D(); 
   //Double_t sig_F     = sig_F_in_delta(); 

   Double_t delta_r_ir  = Delta_R_IR(); 
   Double_t delta_vert  = Delta_vert();
   Double_t delta_vac_l = Delta_vac_l(); 
   Double_t delta_vac_h = Delta_vac_h(); 
   Double_t Delta_v     = delta_r_ir + delta_vert + delta_vac_l + delta_vac_h; 
   Double_t Delta_infty = Delta_inf();

   //std::cout << "================== IRT ==================" << std::endl;
   //std::cout << "Ep           = " << fKinematics.GetEp() << std::endl;;
   //std::cout << "x            = " << fKinematics.Getx() << std::endl;;
   //std::cout << "Delta_R_IR   = " << delta_r_ir  << std::endl; 
   //std::cout << "Delta_vert   = " << delta_vert  << std::endl; 
   //std::cout << "Delta_vac_l  = " << delta_vac_l << std::endl; 
   //std::cout << "Delta_vac_h  = " << delta_vac_h << std::endl; 
   //std::cout << "Delta_v      = " << Delta_v     << std::endl; 
   //std::cout << "Delta_inf    = " << Delta_infty << std::endl; 
   //std::cout << "sig_dis_born = " << sig_born    << std::endl; 
   //std::cout << "sig_F        = " << sig_F       << std::endl; 
   Double_t T1 = 0.; 
   if(fIsMultiPhoton){ 
      // Double_t T1          = TMath::Exp( (alpha/pi)*Delta_infty )*(alpha/pi)*( Delta_v - Delta_infty )*sig_born;
      T1         = TMath::Exp( (alpha/pi)*Delta_infty )*( 1. + (alpha/pi)*(Delta_v - Delta_infty) )*sig_born;
   } else{
      T1         = (1. + (alpha/pi)*Delta_v)*sig_born; 
   }
   Double_t T2  = sig_F; 
   Double_t arg = T1 + T2; 
   //std::cout << arg - sig_born << std::endl;
   return arg;

}
//______________________________________________________________________________
Double_t POLRAD::sig_F_in_delta() {
   // Eq. 13: infrared free part of the IRT 
   Double_t Tau_min  = fKinematics.GetTau_min(); 
   Double_t Tau_max  = fKinematics.GetTau_max(); 
   Double_t min      = Tau_min; 
   Double_t max      = Tau_max; 

   IRT_TauFuncWrap_delta funcWrap;
   funcWrap.fPOLRAD = this;

   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE;//,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kLEGENDRE; //kVEGAS,kADAPTIVE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR;
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kNONADAPTIVE;
   double absErr                           = 1.0e100; /// desired absolute Error 
   double relErr                           = 1.0e-3; /// desired relative Error 
   unsigned int ncalls                     = 1.0e5;  /// number of calls (MC only)
   int rule                                = 1;        /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    
   int nLegendrePoints                     = 20;

   ROOT::Math::VirtualIntegratorOneDim *  ig = nullptr;
   ROOT::Math::IntegratorOneDim  ig2(type,absErr,relErr,ncalls);
   ig2.SetFunction( funcWrap );
   ig2.SetRelTolerance( relErr );
   if( type == ROOT::Math::IntegrationOneDim::kLEGENDRE) { 
      ig = ig2.GetIntegrator();
      ((ROOT::Math::GaussLegendreIntegrator*)ig)->SetNumberPoints(nLegendrePoints); 
   }
   Double_t res  = ig2.Integral(min,max);
   std::cout << "int: " << res << " +/- " << ig2.Error() <<  std::endl;

   Double_t y         = fKinematics.Gety();
   Double_t alpha     = fine_structure_const; 
   Double_t N         = (-1.0)*alpha*alpha*alpha*y; 
   Double_t sig       = N*res; 
   //if(fDebug > 2){
   //   std::cout << "N     = " << N         << std::endl;
   //   std::cout << "int   = " << res  << std::endl;
   //std::cout << "sig_F = " << sig       << std::endl;
   //}
   return sig;

}
//______________________________________________________________________________
Double_t POLRAD::sig_F_in_Tau_Integrand_delta(Double_t &tau) {

   if(fDebug > 3) std::cout << "==================== tau-int ====================" << std::endl;

   fTau = tau;// Set tau to be used when   integrating over R
   /// \todo perhaps this should be a parameter or something in the wrapper function 
   ///       inorder to avoid have this data member?

   fReCalculateTs = true;
   Double_t W2       = fKinematics.GetW2();
   Double_t M        = fKinematics.GetM();
   Double_t M_pion   = fKinematics.GetM_pion();
   Double_t T1,T2,T3,T4;
   Double_t arg=0,sum2=0; 
   Double_t R_max = (W2 - TMath::Power(M+M_pion,2.))/(1.+tau);  
   Double_t min = 0;
   Double_t max = R_max; 

   // Test to integrate just over the delta. 
   Double_t delta_width = 0.10;
   Double_t R_1 = (W2 - TMath::Power(M_P33/GeV - delta_width/2.0,2.))/(1.+tau);  
   Double_t R_2 = (W2 - TMath::Power(M_P33/GeV + delta_width/2.0,2.))/(1.+tau);  
   min = R_2;
   max = R_1;

   ROOT::Math::VirtualIntegratorOneDim *  ig2 = nullptr;

   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE;//,kLEGENDRE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kLEGENDRE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kNONADAPTIVE;
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR;
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kGAUSS;
   double absErr                           = 1.0e100;   /// desired absolute Error 
   double relErr                           = 1.0e-3;   /// desired relative Error 
   unsigned int ncalls                     = 5.0e4;    /// number of calls (MC only)
   int rule                                = 1;        /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    
   int nLegendrePoints                     = 40;

   IRT_RFuncWrap_3 funcWrap1;
   funcWrap1.fPOLRAD = this;

   ROOT::Math::IntegratorOneDim  ig2_2(type,absErr,relErr,ncalls);//,rule);
   ig2_2.SetFunction( funcWrap1 );
   ig2_2.SetRelTolerance( relErr );

   if( type == ROOT::Math::IntegrationOneDim::kLEGENDRE) { 
      ig2 = ig2_2.GetIntegrator();
      ((ROOT::Math::GaussLegendreIntegrator*)ig2)->SetNumberPoints(nLegendrePoints); 
   }

   arg = ig2_2.Integral(min,max);
   //std::cout << "int: " << arg << " +/- " << ig2_2.Error() <<  std::endl;
   if(fDebug > 3){
      std::cout << "tau-int: " << arg << std::endl;
      if(arg > 1E+4){
         std::cout << "WARNING: tau-int: " << arg << "\t" << "tau = " << tau << std::endl;
      }
   }

   return arg;
}
//______________________________________________________________________________
Double_t POLRAD::sig_F_in_2() {
   
   // Main integral for IRT 
   // Integrates over tau

   Double_t min      = fKinematics.GetTau_min();
   Double_t max      = fKinematics.GetTau_max();
   Double_t Q2       = fKinematics.GetQ2();
   Double_t X        = fKinematics.GetX();
   Double_t tau_peak = Q2/X;
   Double_t tau_1    = 0.0;
   Double_t tau_2    = 0.85*tau_peak;
   if(tau_2 < tau_1 ) tau_2 = tau_1;
   Double_t tau_3    = 1.15*tau_peak;
   if(tau_3 < tau_2 ) tau_3 = tau_2;

   // Wrapper for function sig_F_in_Tau_Integrand_2
   //IRT_TauFuncWrap_2 funcWrap;
   //funcWrap.fPOLRAD = this;

   if(!fIRT_tau_integrator) {
      std::cout << " - Creating IRT integrator with : " << std::endl;
      std::cout << "         type  = "  << fIRT_tau_Type << std::endl;
      std::cout << "     rel.err.  = "  << fIRT_tau_RelErr << std::endl;
      std::cout << "     abs.err.  = "  << fIRT_tau_AbsErr << std::endl;
      std::cout << "         rule  = "  << fIRT_tau_Rule << std::endl;
      std::cout << "         calls = "  << fIRT_tau_nCalls << std::endl;
      fIRT_tau_integrator = new ROOT::Math::IntegratorOneDim(fIRT_tau_Type,
                                                             fIRT_tau_AbsErr,
                                                             fIRT_tau_RelErr,
                                                             fIRT_tau_nCalls,
                                                             fIRT_tau_Rule);
      fIRT_tau_integrator->SetFunction(fIRT_tau_func);
   }

   //unsigned int nLegendrePoints              = 6;
   //ROOT::Math::VirtualIntegratorOneDim *  ig = 0;
   //if( fIRT_tau_Type == ROOT::Math::IntegrationOneDim::kLEGENDRE) { 
   //   ig = fIRT_tau_integrator->GetIntegrator();
   //   ((ROOT::Math::GaussLegendreIntegrator*)ig)->SetNumberPoints(nLegendrePoints); 
   //}

   Double_t res  = fIRT_tau_integrator->Integral(min,max);
   //std::cout << " sig_F_in2 res = " << res << std::endl;
   //res  += ig2.Integral(tau_1,tau_2);
   //std::cout << " sig_F_in2 res = " << res << std::endl;
   //res  += ig2.Integral(tau_2,tau_3);
   //std::cout << " sig_F_in2 res = " << res << std::endl;
   //res  += ig2.Integral(tau_3,max);
   //std::cout << " sig_F_in2 res = " << res << std::endl;
   Double_t y         = fKinematics.Gety();
   Double_t alpha     = fine_structure_const; 
   Double_t N         = (-1.0)*alpha*alpha*alpha*y; 
   Double_t sig       = N*res; 
   //if(fDebug > 2){
   //   std::cout << "N     = " << N         << std::endl;
   //   std::cout << "int   = " << res  << std::endl;
   //   std::cout << "sig_F = " << sig       << std::endl;
   //}
   return sig;

}
//______________________________________________________________________________
Double_t POLRAD::sig_F_in_Tau_Integrand_2(Double_t &tau) {

   // Performs R integral for a given value of tau 

   fTau = tau; // Set tau to be used when   integrating over R
   /// \todo perhaps this should be a parameter or something in the wrapper function 
   ///       inorder to avoid have this data member?

   fReCalculateTs    = true; // Needed for the new value of tau
   Double_t W2       = fKinematics.GetW2();
   Double_t M        = fKinematics.GetM();
   Double_t M_pion   = fKinematics.GetM_pion();
   Double_t arg=0; 
   //Double_t R_max = (W2 - TMath::Power(1.512,2.0))/(1.0+tau);  
   Double_t R_max = (W2 - TMath::Power(M+M_pion,2.))/(1.+tau);  
   Double_t min = 1.0e-30;
   Double_t max = R_max; 

   //if(fDebug>3){
   //   std::cout << "For the R integral: " << std::endl;
   //   std::cout << "tau = " << tau << std::endl;
   //   std::cout << "Rmin = " << min << std::endl;
   //   std::cout << "Rmax = " << max << std::endl;
   //}

   if(!fIRT_R_integrator) {
      fIRT_R_integrator = new ROOT::Math::IntegratorOneDim(fIRT_R_Type,
                                                           fIRT_R_AbsErr,
                                                           fIRT_R_RelErr,
                                                           fIRT_R_nCalls,
                                                           fIRT_R_Rule);
      fIRT_R_integrator->SetFunction(fIRT_R_func);
   }

   ROOT::Math::VirtualIntegratorOneDim *  ig2 = nullptr;

   if( fIRT_R_Type == ROOT::Math::IntegrationOneDim::kLEGENDRE) { 
      ig2 = fIRT_R_integrator->GetIntegrator();
      ((ROOT::Math::GaussLegendreIntegrator*)ig2)->SetNumberPoints(fIRT_R_nLegPoints); 
   }

   Double_t res = fIRT_R_integrator->Integral(min,max);
   //std::cout << "res = "<<  res << std::endl;

   return res;
}
//______________________________________________________________________________
Double_t POLRAD::sig_F_in_angle_peaking() {
   
   // Main integral for IRT 
   // Integrates over tau

   Double_t min      = fKinematics.GetTau_min();
   Double_t max      = fKinematics.GetTau_max();
   Double_t Q2       = fKinematics.GetQ2();
   Double_t X        = fKinematics.GetX();

   // Angle peaking approximation peaks
   Double_t nu1   = fKinematics.GetEs();
   Double_t nu2   = fKinematics.GetEp();
   Double_t theta = fKinematics.GetTheta();
   Double_t Mass  = fKinematics.GetM();
   Double_t m     = fKinematics.Getm();
   Double_t d_th_1= TMath::Sqrt(m/nu1)/2.0;
   Double_t d_th_2= TMath::Sqrt(m/nu2)/2.0;

   Double_t tau_s = nu2*(TMath::Cos(theta)-1.0);
   Double_t tau_p = nu1*(1.0-TMath::Cos(theta));
   Double_t tau_s_width = TMath::Abs((nu2*(TMath::Cos(theta-d_th_1)-1.0)) - ( nu2*(TMath::Cos(theta+d_th_1)-1.0) ));
   Double_t tau_p_width = TMath::Abs((nu1*(1.0-TMath::Cos(theta-d_th_2))) - ( nu1*(1.0-TMath::Cos(theta+d_th_2)) ));

   //std::cout << "tau_s : " << tau_s << " +- " << tau_s_width << std::endl;
   //std::cout << "tau_p : " << tau_p << " +- " << tau_p_width << std::endl;

   Double_t int1  = sig_F_in_Tau_Integrand_2(tau_s)*tau_s_width;
   Double_t int2  = sig_F_in_Tau_Integrand_2(tau_p)*tau_p_width;

   Double_t res   = int1 + int2 ;
   Double_t y         = fKinematics.Gety();
   Double_t alpha     = fine_structure_const; 
   Double_t N         = (-1.0)*alpha*alpha*alpha*y; 
   Double_t sig       = N*res; 
   //if(fDebug > 2){
   //   std::cout << "N     = " << N         << std::endl;
   //   std::cout << "int   = " << res  << std::endl;
   //   std::cout << "sig_F = " << sig       << std::endl;
   //}
   //std::cout << "sig_F = " << sig       << std::endl;
   return sig;

}
//______________________________________________________________________________
Double_t POLRAD::sig_F_in_R_Integrand_3(Double_t &R) {

   // The main IRT integrand as a function of r and tau
   // using data member fTau
   Double_t sf0[9];
   Double_t sft[9];
   ProcessFancyInelasticStructureFunctions(R,fTau,sft);  
   ProcessFancyInelasticStructureFunctions(0.0,0.0,sf0);  
   Double_t Q2 = fKinematics.GetQ2(); 

   Double_t tau = fTau;
   Double_t T1  =0;
   Double_t res = 0;

   for(Int_t i=1;i<=8;i++){
      for(Int_t j=1;j<=ki[i];j++){
         res += THETAij(i,j,tau)*sft[i]*TMath::Power(R,double(j)-2.0)/TMath::Power(Q2 + R*tau,2.0); 
         if(j==1) res += THETAij(i,j,tau)*(-1.0*sf0[i])/(Q2*Q2*R) ; 
      }
   }

   return res; 
}
//______________________________________________________________________________
Double_t POLRAD::sig_F_in_2D() {

   Double_t W2       = fKinematics.GetW2();
   Double_t M        = fKinematics.GetM();
   Double_t M_pion   = fKinematics.GetM_pion();
   //Double_t R_max = (W2 - TMath::Power(M+M_pion,2.))/(1.+tau);  
   Double_t r_min = 1.0e-32;
   Double_t r_max    = (W2 - TMath::Power(M+M_pion,2.0));
   Double_t tau_min  = fKinematics.GetTau_min(); 
   Double_t tau_max  = fKinematics.GetTau_max(); 

   Double_t min[2] = {r_min,tau_min};
   Double_t max[2] = {r_max,tau_max};

   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kADAPTIVE;//,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kPLAIN;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kMISER;
   double absErr                           = 1.0e100;   /// desired absolute Error 
   double relErr                           = 1.0e-2;   /// desired relative Error 
   unsigned int ncalls                     = 1.0e2;    /// number of calls (MC only)
   int rule                                = 1;        /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    

   IRT_2D_integrand funcWrap;
   funcWrap.fPOLRAD = this;

   ROOT::Math::IntegratorMultiDim ig2(type);//,absErr,relErr,ncalls); 
   ig2.SetFunction(funcWrap);
   //ig2.SetRelTolerance(relErr);

   Double_t y         = fKinematics.Gety();
   Double_t alpha     = fine_structure_const; 
   Double_t N         = (-1.0)*alpha*alpha*alpha*y; 
   Double_t res       = N*ig2.Integral(min,max); 

   return res;
}
//______________________________________________________________________________
Double_t POLRAD::sig_F_in_2D_Integrand(Double_t r, Double_t tau) {

   fTau = tau;
   fReCalculateTs = true;

   Double_t R = r/(1.0 + tau);

   Double_t sf0[9];
   Double_t sft[9];
   ProcessFancyInelasticStructureFunctions(R,fTau,sft);  
   ProcessFancyInelasticStructureFunctions(0.0,0.0,sf0);  

   Double_t Q2 = fKinematics.GetQ2(); 

   Double_t T1  =0;
   Double_t res = 0;

   for(Int_t i=1;i<=8;i++){
      for(Int_t j=1;j<=ki[i];j++){
         res += THETAij(i,j,tau)*sft[i]*TMath::Power(R,double(j)-2.0)/TMath::Power(Q2 + R*tau,2.0); 
         if(j==1) res += (-1.0*sf0[i])/(Q2*Q2) ; 
      }
   }

   //if(fDebug > 4) {
   //   std::cout << "R = " << R << std::endl;
   //   std::cout << "tau = " << fTau << std::endl;
   //   std::cout << "Q2 = " << Q2 << std::endl;
   //   std::cout << "(1+R*tau/Q2) = " << 1 + R*fTau/Q2 << std::endl;
   //std::cout << "R-int-1: " << arg << " , i= " << ffSFIndex << std::endl;
   //	std::cout << "F(" << ffSFIndex << "," << R << "," << fTau << ") = " << F_RT << "\t" 
   //   		  << "F(" << ffSFIndex << ",0,0) = " << F_00 << std::endl; 
   //}
   res = res/(1.0+tau);
   //if(res<0.0) std::cout << "2d integrand = " << res << std::endl; 
   return res; 
}
//______________________________________________________________________________
Double_t POLRAD::Delta_vert() {

   // Eq. 15 in POLRAD has ultrarelativistic approximations...
   // This is Eq. 20 (no ultrarelativistic approximation) of J Phys. G 20, 513 (1994) 

   Double_t m        = fKinematics.Getm();
   Double_t Q2       = fKinematics.GetQ2();
   Double_t lambda   = m; // fDeltaE; // photon mass => detector resolution
   Double_t Q2_m     = fKinematics.GetQ2_m();
   Double_t Lambda_m = fKinematics.GetLambda_m();
   Double_t L_m      = fKinematics.GetL_m();  
   Double_t J0       = 2.*(Q2_m*L_m -1.);
   Double_t p_num    = 2.*TMath::Sqrt(Lambda_m); 
   Double_t p_den    = Q2 + TMath::Sqrt(Lambda_m); 
   Double_t p_arg    = p_num/p_den; 
   Double_t Phi      = Spence(p_arg);
   Double_t T1       = J0*TMath::Log(lambda/m);
   Double_t T2       = ( (3./2.)*Q2 + 4.*m*m )*L_m - 2.;
   Double_t T3       = (-1.)*( Q2_m/TMath::Sqrt(Lambda_m) )*(0.5*Lambda_m*L_m*L_m + 2.*Phi - pi2/4.);  
   Double_t delta    = T1 + T2 + T3;  
   return delta; 

}
//______________________________________________________________________________
Double_t POLRAD::Delta_R_IR() {

   // Ultrarelativistic approximation (m=0)
   // Following paper: T.V. Kuchto and N.M. Shumeiko Nucl.Phys.B219 p412-436 1983
   // Equation 15
   Double_t m        = fKinematics.Getm(); 
   Double_t lambda   = m; // photon mass (not detector resolution fDeltaE?) 
   Double_t M        = fKinematics.GetM();
   Double_t M_pion   = fKinematics.GetM_pion();
   Double_t W        = fKinematics.GetW();
   Double_t W2       = fKinematics.GetW2();
   // Double_t Q2       = fKinematics.GetQ2();
   Double_t Q2_m     = fKinematics.GetQ2_m();
   Double_t Lambda_m = fKinematics.GetLambda_m();
   Double_t L_m      = fKinematics.GetL_m(); 
   Double_t J0       = 2.*(Q2_m*L_m -1.);

   // First term 
   Double_t a_num    = W*W - TMath::Power(M + M_pion,2.0);
   Double_t a_den    = lambda*W; 
   Double_t a_arg    = a_num/a_den; 
   Double_t a2=0;
   if(a_arg > 0 ){
      a2        = TMath::Log(a_arg);
   }else {
      std::cout << "[POLRAD::Delta_R_IR]: Error! " << std::endl;
      std::cout << "W2 = " << W*W << "\t" << "(Mp + Mpi)^2 = " << TMath::Power(M+M_pion,2.) << std::endl;
      a2        = 0;
      return(0);
      //exit(1);
   }
   Double_t T1       = J0*a2;
   // Second term
   Double_t S_prime  = fKinematics.GetS_prime(); 
   Double_t X_prime  = fKinematics.GetX_prime(); 
   Double_t L_Sprime = fKinematics.GetL_Sprime(); 
   Double_t L_Xprime = fKinematics.GetL_Xprime(); 
   Double_t b1       = S_prime*L_Sprime; 
   Double_t b2       = X_prime*L_Xprime; 
   Double_t T2       = 0.5*(b1 + b2);  
   // Third term    
   Double_t a_prime  = fKinematics.Geta_prime(); 
   Double_t b_prime  = fKinematics.Getb_prime(); 
   Double_t T3       = S_phi();
   Double_t T3prime  = S_phi(Q2_m,Lambda_m,a_prime,b_prime);

   // Put it together
   Double_t delta    = T1 + T2 + T3; 

   //if(fDebug >=0) {
   //	std::cout << "================ Delta_R_IR ================" << std::endl; 
   //	std::cout << "Lambda_m = " << Lambda_m << std::endl;
   //	std::cout << "L_m      = " << L_m      << std::endl;
   //	std::cout << "J0       = " << J0       << std::endl;
   //	std::cout << "a1       = " << a1       << std::endl;
   //	std::cout << "a_num    = " << a_num    << std::endl;
   //	std::cout << "a_den    = " << a_den    << std::endl;
   //	std::cout << "a_arg    = " << a_arg    << std::endl;
   //	std::cout << "a2       = " << a2       << std::endl;
   //	std::cout << "T1       = " << T1       << std::endl;
   //	std::cout << "b1       = " << b1       << std::endl;
   //	std::cout << "b2       = " << b2       << std::endl;
   //	std::cout << "T2       = " << T2       << std::endl;
   //	std::cout << "T3       = " << T3       << std::endl;
   //	std::cout << "T3'      = " << T3prime  << std::endl;
   //        std::cout << "ans      = " << delta    << std::endl;
   //}

   return delta; 

}
//______________________________________________________________________________
Double_t POLRAD::Delta_vac_l() {

   // Eq. 21 from J Phys G 20, 513 (1994) 
   Double_t delta=0; 
   Double_t T1=0,T2=0,T3=0; 
   Double_t Lambda_m=0,num=0,den=0,arg=0,L_m=0; 
   Double_t m_e   = M_e/GeV;     // electron mass (GeV)
   Double_t m_mu  = M_muon/GeV;  // muon mass (GeV) 
   Double_t m_tau = M_tau/GeV;   // tau mass (GeV) 
   Double_t m[3]  = {m_e,m_mu,m_tau};
   Double_t Q2    = fKinematics.GetQ2();  
   // 0 = electron, 1 = muon, 2 = tau 
   for(int i=0;i<3;i++){
      Lambda_m = Q2*Q2 + 4.*m[i]*m[i]*Q2; 
      num      = TMath::Sqrt(Lambda_m) + Q2;
      den      = TMath::Sqrt(Lambda_m) - Q2;   
      arg      = num/den; 
      L_m      = TMath::Log(arg)/TMath::Sqrt(Lambda_m);
      T1       = (2./3.)*(Q2 + 2.*m[i]*m[i])*L_m;
      T2       = -10./9.; 
      T3       = (8.*m[i]*m[i]/(3.*Q2))*(1. - 2.*m[i]*m[i]*L_m); 
      delta   += T1 + T2 + T3; 
   }

   return delta;


}
//______________________________________________________________________________
Double_t POLRAD::Delta_vac_h() {

   Double_t A=0,B=0,C=0;
   Double_t alpha = fine_structure_const; 
   Double_t S     = fKinematics.GetS(); 
   Double_t absS  = TMath::Abs(S); 
   Double_t rS    = TMath::Sqrt(absS);
   Double_t Q2    = fKinematics.GetQ2(); 

   // Fit from Phys Lett B 356, 358 (1995)  
   // if(rS>0.0 && rS< 2.0){
   // 	A = 0;
   // 	B = 0.00228770; 
   // 	C = 4.08041425;
   // }else if(rS>2.0 && rS<4.0){
   // 	A = 0;
   // 	B = 0.00251507; 
   // 	C = 3.09624477;
   // }else if(rS>4.0 && rS<10.0){
   // 	A = 0.0; 
   // 	B = 0.00279328; 
   // 	C = 2.07463133; 
   // }else if(rS>10.0 && sqrtS<91.2){
   // 	A = 0.00122270;
   // 	B = 0.00296694;
   // 	C = 1.0; 
   // }else if(rS>91.2 && rS<1E+5){
   // 	A = 0.00164178;
   // 	B = 0.00292051;
   // 	C = 1.0; 
   // }else{
   // 	A = 0;
   // 	B = 0;
   // 	C = 0;
   // }

   // NATO ASI 233, 201 (1989)
   // if(rS>0.0 && rS<=0.30){
   // 	A = 0.0;
   // 	B = 0.00835; 
   // 	C = 1.0;
   // }else if(rS >0.3 && rS <=3.0){
   // 	A = 0.0;
   // 	B = 0.00238;
   // 	C = 3.927; 
   // }else if(rS>3.0 && rS <=100.0){
   // 	A = 0.00165; 
   // 	B = 0.00299;
   // 	C = 1.0; 
   // }else if(rS>100.0){
   // 	A = 0.00221; 
   // 	B = 0.00293;
   // 	C = 1.0; 
   // }else{
   // 	A = 0;
   // 	B = 0;
   // 	C = 0;
   // }

   // Double_t delta = A + B*TMath::Log(1. + C*S); 

   // from the fortran code... 
   if(Q2 < 1 ){
      A = -1.345E-9;
      B = -2.302E-3;
      C =  4.091;
   }else if(Q2 < 64.0){
      A = -1.512E-3;
      B = -2.822E-3;
      C =  1.218;
   }else{
      A = -1.1344E-3;
      B = -3.0680E-3;
      C =  9.9992E-1;
   }

   Double_t delta = (-1.)*( A + B*TMath::Log(1. + C*Q2) )*(2.*pi/alpha); 

   return delta; 

}
//______________________________________________________________________________
Double_t POLRAD::Delta_inf() {

   // Eq. 16.  Used in the ultrarelativistic approx. 
   // and the multiple photon corrections 
   Double_t l_m     = fKinematics.Getl_m();
   Double_t W2      = fKinematics.GetW2();   
   Double_t M       = fKinematics.GetM();   
   Double_t M_pion  = fKinematics.GetM_pion();   
   Double_t S_prime = fKinematics.GetS_prime(); 
   Double_t X_prime = fKinematics.GetX_prime(); 
   Double_t T1      = l_m - 1.;
   Double_t arg     = W2 - TMath::Power(M + M_pion,2.0);
   Double_t A       = TMath::Power(arg,2.0);
   Double_t B       = S_prime*X_prime; 
   Double_t T2      = TMath::Log(A/B); 
   Double_t delta   = T1*T2; 
   return delta; 

}
//______________________________________________________________________________
Double_t POLRAD::ERT() {
   //fKinematics.Print();
   // Eq. 18, Elastic radiative tail (exact) 
   // Note that this is PER NUCLEON!
   Double_t Q2         = fKinematics.GetQ2();
   Double_t x          = fKinematics.Getx_A();
   Double_t y          = fKinematics.Gety_A();
   Double_t S_xA       = fKinematics.GetS_xA();
   Double_t Lambda_QA  = fKinematics.GetLambda_QA();
   Double_t MT         = fKinematics.GetM_A();
   Double_t alpha      = fine_structure_const; 
   Double_t mp         = MultiPhoton_el(x,y,Q2);     // multiple-soft photon radiation (POLRAD manual) 
   // Double_t mp         = MultiPhoton(Q2);         // multiple-soft photon radiation (Stein et al)  seems to give the same result as POLRAD 
   Double_t N          = (-1.0)*alpha*alpha*alpha*y/(fA*fA);
   Double_t Tau_A_min  = (S_xA - TMath::Sqrt(Lambda_QA))/(2.0*MT*MT);
   Double_t Tau_A_max  = (S_xA + TMath::Sqrt(Lambda_QA))/(2.0*MT*MT);
   Double_t min        = Tau_A_min;  
   Double_t max        = Tau_A_max; 
   Double_t ext_tail   = sigma_b(); 

   //if(fDebug>2){
   //	std::cout << "================== ERT Tau Integral ==================" << std::endl;
   //	std::cout << "min = " << min << std::endl;
   //	std::cout << "max = " << max << std::endl;
   //}

   ERTFuncWrap fERTFuncWrap;
   fERTFuncWrap.fPOLRAD = this;

   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kLEGENDRE; //kNONADAPTIVE; //kLEGENDRE,kVEGAS,kADAPTIVE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kNONADAPTIVE; //kLEGENDRE,kVEGAS,kADAPTIVE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR;
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE;
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kGAUSS;

   double absErr                           = 1.0e100;  /// desired absolute Error 
   double relErr                           = 1.0e-3;  /// desired relative Error 
   unsigned int ncalls                     = 1.0e3;   /// number of calls (MC only)
   unsigned int npoints                    = 5000;
   unsigned int rule                       = 1;       /// GK rule

   ROOT::Math::IntegratorOneDim  ig2(type,absErr,relErr,ncalls,rule);
   //ROOT::Math::GaussLegendreIntegrator ig2;
   //ROOT::Math::GSLIntegrator ig2(type);
   //ig2.SetNumberPoints(npoints); // only for Gauss-Legendre

   ig2.SetFunction( fERTFuncWrap );
   ig2.SetRelTolerance( relErr );
   //int nsteps = int(max-min)*1000;
   Double_t sig_el = ig2.Integral(min,max);// +  N*ig2.Integral(0.5,max);
   //std::cout << " sig_el = " << sig_el << std::endl;
   sig_el *= N;
   //Double_t sig_el = N*AdaptiveSimpson(&POLRAD::ERT_Tau_Integrand,min,max,/*fErr*/1.0e-1,1); 
   //Double_t sig_el = N*SimpleIntegration(&POLRAD::ERT_Tau_Integrand,min,0,fErr,nsteps); 

   //std::cout << " difference = " << sig_el - sig_el << "\n";
   //std::cout << "sig_el = " << sig_el << "\n";

   Double_t arg    = sig_el;

   Double_t Es = fKinematics.GetEs();
   Double_t Ep = fKinematics.GetEp();
   Double_t th = fKinematics.GetTheta(); 
   // std::cout << "Es = " << Es << "\t" << "Ep = " << Ep << "\t" << "th = " << th/degree 
   //           << "\t" << "int.: " << arg*hbarc2_gev_nb << "\t" << "ext: " << ext_tail*hbarc2_gev_nb << std::endl; 

   if(fIsExtTail){
      arg += ext_tail;
   }

   if(fIsMultiPhoton){
      arg = mp*sig_el; 
   }

   // if(fDebug>0){
   // 	std::cout << "================== ERT Tau Integral ==================" << std::endl;
   // 	std::cout << "min = " << min << std::endl;
   // 	std::cout << "max = " << max << std::endl;
   // 	std::cout << "N   = " << N   << std::endl;
   // 	std::cout << "mp  = " << mp  << std::endl;
   // 	std::cout << "int = " << val << std::endl;
   // 	std::cout << "ERT = " << arg << std::endl;
   // }
   
   return arg;

}
//______________________________________________________________________________
Double_t POLRAD::ERT_Tau_Integrand(Double_t &tau_A) {

   fReCalculateTs = true;
   Double_t J=0,num=0,den=0,arg=0,T1=0,T2=0,T3=0;

   Double_t Q2   = fKinematics.GetQ2();
   Double_t MT   = fKinematics.GetM_A();
   Double_t M    = fKinematics.GetM(); 
   // Double_t A    = MT/M; 
   Double_t S_xA = fKinematics.GetS_xA();
   Double_t Rel  = (S_xA - Q2 )/(1. + tau_A); 
   // Double_t Rel  = (S_xA - Q2/A )/(1. + tau_A);  // because this is what the fortran does...  

   //fArbValue4    = Rel; 

   //         if(fDebug>0){
   // 		std::cout << "================== ERT Tau Integrand ==================" << std::endl;
   // 		std::cout << "S_xA  = " << S_xA << std::endl;
   // 		std::cout << "tau_A = " << tau_A << std::endl;
   // 		std::cout << "Rel   = " << Rel   << std::endl;
   // 		std::cout << "tau_A*Rel = " << tau_A*Rel << std::endl;
   // 		std::cout << "Q2   = " << Q2   << std::endl;
   //         }

   // Calculate fancy structure functions 
   ProcessFancyElasticStructureFunctions(Rel,tau_A,ffSFel); 

   arg = 0.0;
   for(Int_t i=1;i<=8;i++){
      for(Int_t j=1;j<=ki[i];j++){
         T1   = THETAij_A(i,j,tau_A);
         //T1   = THETAij(i,j,tau_A);
         J    = Double_t(j); 
         num  = 2.*MT*MT*TMath::Power(Rel,J-2.);
         den  = (1. + tau_A)*TMath::Power(Q2 + Rel*tau_A,2.); 
         T2   = num/den; 
         T3   = ffSFel[i]; 
         //if(i==1) arg += T1*T2*T3; 
         //if(i==2) arg += T1*T2*T3; 
         //else arg += T1*T2*T3; 
         //arg += T1*T2*T3; 
         //std::cout << " i= " << i << ", j = " << j << std::endl;
         //std::cout << "num  = " << num << std::endl;
         //std::cout << "den  = " << den << std::endl;
         //std::cout << "T1   = " << T1 << std::endl;
         //std::cout << "T2   = " << T2 << std::endl;
         //std::cout << "T3   = " << T3 << std::endl;
         //std::cout << "arg  = " << arg << std::endl;
      }
   }
   return arg;

}
//______________________________________________________________________________
Double_t POLRAD::QRT() {

   // Eq. 20 
   // Quasi-elastic radiative tail
   if( fQRT_IntegrationMethod == kAnglePeaking )
      return QRT_angle_peaking();

   if( fQRT_IntegrationMethod == kFull )
      return QRT_Full();


   Double_t x        = fKinematics.Getx(); 
   Double_t y        = fKinematics.Gety(); 
   Double_t Q2       = fKinematics.GetQ2(); 
   Double_t W2       = fKinematics.GetW2(); 
   Double_t M        = fKinematics.GetM();
   Double_t S_x      = fKinematics.GetS_x();
   Double_t Lambda_Q = fKinematics.GetLambda_Q(); 
   Double_t Tau_min  = (S_x - TMath::Sqrt(Lambda_Q))/(2.0*M*M);
   Double_t Tau_max  = (S_x + TMath::Sqrt(Lambda_Q))/(2.0*M*M);
   Double_t min      = Tau_min; 
   Double_t max      = Tau_max; 

   Double_t alpha    = fine_structure_const; 
   Double_t mp       = MultiPhoton_qe(x,y,Q2);  // multiple-soft photon radiation (POLRAD manual) 
   // Double_t mp       = MultiPhoton(Q2);        // multiple-soft photon radiation (Stein et al)  
   Double_t N        = (-1.)*alpha*alpha*alpha*y/(fA); 

   QRTTauFuncWrap funcWrap;
   funcWrap.fPOLRAD = this;
   ROOT::Math::VirtualIntegratorOneDim *  ig = nullptr;

   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kLEGENDRE; //kVEGAS,kADAPTIVE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR;
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE;
   double absErr                            = 1.0e-5;   /// desired absolute Error 
   double relErr                            = 1.0e-15;   /// desired relative Error 
   unsigned int ncalls                      = 1.0e6;  /// number of calls (MC only)
   int nLegendrePoints                      = 5;

   ROOT::Math::IntegratorOneDim  ig2(type,absErr,relErr,ncalls);
   ig2.SetRelTolerance( relErr );
   ig2.SetFunction( funcWrap );
   if( type == ROOT::Math::IntegrationOneDim::kLEGENDRE) { 
      ig = ig2.GetIntegrator();
      ((ROOT::Math::GaussLegendreIntegrator*)ig)->SetNumberPoints(nLegendrePoints); 
   }

   Double_t Integrand                       = ig2.Integral(min,max);
   // Double_t Integrand = AdaptiveSimpson(&POLRAD::QRT_Tau_Integrand,min,max,fErr,fDepth); 
   Double_t arg = N*Integrand; 

   if(fIsMultiPhoton){
      arg *= mp; 
   }

   //if(fDebug > 2){
   //   std::cout << "============================ QRT ============================" << std::endl;
   //   std::cout << "tau-min      = " << min << "\t" << "tau-max = " << max       << std::endl;
   //   std::cout << "x            = " << x           << std::endl;
   //   std::cout << "y            = " << y           << std::endl;
   //   std::cout << "Q2           = " << Q2          << std::endl;
   //   std::cout << "W2           = " << W2          << std::endl;
   //   std::cout << "N            = " << N           << std::endl;
   //   std::cout << "int          = " << Integrand   << std::endl;
   //   std::cout << "Multi-photon = " << mp          << std::endl;
   //   std::cout << "QRT          = " << arg         << std::endl;
   //}

   return arg;

}
//______________________________________________________________________________
Double_t POLRAD::QRT_Full() {

   // Eq. 19 
   // Quasi-elastic radiative tail
   // Exact  

   // Instead just do peaking approximation which takes the 2d integral to a 1d integral.
   // It evaluates much faster than the 2d integral. 
   //return QRT_angle_peaking();

   Double_t x        = fKinematics.Getx(); 
   Double_t y        = fKinematics.Gety(); 
   Double_t Q2       = fKinematics.GetQ2(); 
   Double_t W2       = fKinematics.GetW2(); 
   Double_t M        = fKinematics.GetM();
   Double_t S_x      = fKinematics.GetS_x();
   Double_t Lambda_Q = fKinematics.GetLambda_Q(); 
   Double_t Tau_min  = fKinematics.GetTau_min(); 
   Double_t Tau_max  = fKinematics.GetTau_max(); 
   Double_t min      = Tau_min; 
   Double_t max      = Tau_max; 

   Double_t alpha    = fine_structure_const; 
   Double_t mp       = MultiPhoton_qe(x,y,Q2);  // multiple-soft photon radiation  
   Double_t N        = (-1.)*alpha*alpha*alpha*y/(fA); 

   QRT_TauFuncWrap_QE_Full funcWrap;
   funcWrap.fPOLRAD = this;
   ROOT::Math::VirtualIntegratorOneDim *  ig = nullptr;

   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kLEGENDRE; //kVEGAS,kADAPTIVE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR;
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE;
   double absErr                            = 1.0e10;   /// desired absolute Error 
   double relErr                            = 1.0e-14;  /// desired relative Error 
   unsigned int ncalls                      = 1.0e6;   /// number of calls (MC only)
   int nLegendrePoints                      = 10;

   ROOT::Math::IntegratorOneDim  ig2(type,absErr,relErr,ncalls);
   ig2.SetRelTolerance( relErr );
   ig2.SetFunction( funcWrap );
   if( type == ROOT::Math::IntegrationOneDim::kLEGENDRE) { 
      ig = ig2.GetIntegrator();
      ((ROOT::Math::GaussLegendreIntegrator*)ig)->SetNumberPoints(nLegendrePoints); 
   }

   Double_t Integrand                       = ig2.Integral(min,max);
   // Double_t Integrand = AdaptiveSimpson(&POLRAD::QRT_Tau_Integrand_Full,min,max,fErr,fDepth); 
   Double_t arg = N*Integrand; 

   if(fIsMultiPhoton){
      arg *= mp; 
   }

   //if(fDebug > 2){
   //   std::cout << "============================ QRT ============================" << std::endl;
   //   std::cout << "tau-min      = " << min << "\t" << "tau-max = " << max       << std::endl;
   //   std::cout << "x            = " << x           << std::endl;
   //   std::cout << "y            = " << y           << std::endl;
   //   std::cout << "Q2           = " << Q2          << std::endl;
   //   std::cout << "W2           = " << W2          << std::endl;
   //   std::cout << "N            = " << N           << std::endl;
   //   std::cout << "int          = " << Integrand   << std::endl;
   //   std::cout << "Multi-photon = " << mp          << std::endl;
   //   std::cout << "QRT          = " << arg         << std::endl;
   //}

   std::cout << "QRT integrand: " << Integrand << std::endl; 

   return arg;

}
//______________________________________________________________________________
Double_t POLRAD::QRT_angle_peaking() {
   
   // Main integral for IRT 
   // Integrates over tau

   Double_t min      = fKinematics.GetTau_min();
   Double_t max      = fKinematics.GetTau_max();
   Double_t Q2       = fKinematics.GetQ2();
   Double_t X        = fKinematics.GetX();

   // Angle peaking approximation peaks
   Double_t nu1   = fKinematics.GetEs();
   Double_t nu2   = fKinematics.GetEp();
   Double_t theta = fKinematics.GetTheta();
   Double_t Mass  = fKinematics.GetM();
   Double_t m     = fKinematics.Getm();
   Double_t d_th_1= TMath::Sqrt(m/nu1)/2.0;
   Double_t d_th_2= TMath::Sqrt(m/nu2)/2.0;

   Double_t tau_s = nu2*(TMath::Cos(theta)-1.0);
   Double_t tau_p = nu1*(1.0-TMath::Cos(theta));
   Double_t tau_s_width = TMath::Abs((nu2*(TMath::Cos(theta-d_th_1)-1.0)) - ( nu2*(TMath::Cos(theta+d_th_1)-1.0) ));
   Double_t tau_p_width = TMath::Abs((nu1*(1.0-TMath::Cos(theta-d_th_2))) - ( nu1*(1.0-TMath::Cos(theta+d_th_2)) ));

   //std::cout << "tau_s : " << tau_s << " +- " << tau_s_width << std::endl;
   //std::cout << "tau_p : " << tau_p << " +- " << tau_p_width << std::endl;

   Double_t int1  = QRT_Tau_Integrand_Full(tau_s)*tau_s_width;
   Double_t int2  = QRT_Tau_Integrand_Full(tau_p)*tau_p_width;

   //std::cout << "int 1 " << int1 << std::endl;
   //std::cout << "int 2 " << int2 << std::endl;

   Double_t res   = int1 + int2 ;
   Double_t y         = fKinematics.Gety();
   Double_t alpha     = fine_structure_const; 
   Double_t N         = (-1.0)*alpha*alpha*alpha*y; 
   Double_t sig       = N*res; 

   //std::cout << "sig = " << sig << std::endl;
   return sig;
}
//______________________________________________________________________________
Double_t POLRAD::QRT_Tau_Integrand_Full(Double_t &tau){

   //if(fDebug > 3) std::cout << "==================== tau-int ====================" << std::endl;

   fTau = tau; // Set tau to be used when   integrating over R
   /// \todo perhaps this should be a parameter or something in the wrapper function 
   ///       inorder to avoid have this data member?

   fReCalculateTs    = true;
   Double_t W2       = fKinematics.GetW2();
   Double_t M        = fKinematics.GetM();
   Double_t M_pion   = M_pion/GeV;
   Double_t arg=0; 
   Double_t DeltaM   = 0.60;     // Something smaller than the proton mass.  
   Double_t R_min    = 0.0;//(W2 - TMath::Power(M+M_pion,2.))/(1.+tau);  
   //Double_t R_min    = (W2 - TMath::Power(M+2.0*M_pion,2.))/(1.+tau);  
   Double_t R_max    = (W2 - TMath::Power(M-DeltaM,2.0))/(1.0+tau);   
   // R_max determines W_max, DeltaM determines how close you get to the actual W2 in the intergral.
   Double_t min = R_min;
   Double_t max = R_max; 

   //if(fDebug>3){
   //   std::cout << "For the R integral: " << std::endl;
   //   std::cout << "tau = " << tau << std::endl;
   //   std::cout << "Rmin = " << min << std::endl;
   //   std::cout << "Rmax = " << max << std::endl;
   //}

   ROOT::Math::VirtualIntegratorOneDim *  ig2 = nullptr;

   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE;//,kLEGENDRE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kLEGENDRE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kNONADAPTIVE;
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR;
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kGAUSS;
   double absErr                           = 1.0e10;   /// desired absolute Error 
   double relErr                           = 1.0e-10;  /// desired relative Error 
   unsigned int ncalls                     = 1.0e6;   /// number of calls (MC only)
   int rule                                = 2;       /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    
   int nLegendrePoints                     = 30;

   QRT_RFuncWrap_QE_Full funcWrap1;
   funcWrap1.fPOLRAD = this;

   ROOT::Math::IntegratorOneDim  ig2_2(type,absErr,relErr,ncalls,rule);
   ig2_2.SetFunction( funcWrap1 );
   ig2_2.SetRelTolerance( relErr );

   if( type == ROOT::Math::IntegrationOneDim::kLEGENDRE) { 
      ig2 = ig2_2.GetIntegrator();
      ((ROOT::Math::GaussLegendreIntegrator*)ig2)->SetNumberPoints(nLegendrePoints); 
   }

   //Double_t simp = AdaptiveSimpson(&POLRAD::sig_F_in_R_Integrand_3,R_1,max,1.0e-6,5); 
   //arg = simp;
   Double_t res = ig2_2.Integral(min,max);

   //std::cout << res << std::endl;

   return res;

}
//______________________________________________________________________________
Double_t POLRAD::QRT_R_Integrand_Full(Double_t &R){

   // using data member fTau
   Double_t tau = fTau;
   Double_t sft[9];
   ProcessFancyQuasiElasticStructureFunctions(R,tau,sft);  
   Double_t Q2 = fKinematics.GetQ2(); 

   Double_t T1  = 0;
   Double_t res = 0;

   for(Int_t i=1;i<=8;i++){
      for(Int_t j=1;j<=ki[i];j++){
         res += THETAij(i,j,tau)*sft[i]*TMath::Power(R,double(j)-2.0)/TMath::Power(Q2 + R*tau,2.0); 
      }
   }

   //if(fDebug > 4) {
   //   std::cout << "R = " << R << std::endl;
   //   std::cout << "tau = " << fTau << std::endl;
   //   std::cout << "Q2 = " << Q2 << std::endl;
   //   std::cout << "(1+R*tau/Q2) = " << 1 + R*fTau/Q2 << std::endl;
   //std::cout << "R-int-1: " << arg << " , i= " << ffSFIndex << std::endl;
   //	std::cout << "F(" << ffSFIndex << "," << R << "," << fTau << ") = " << F_RT << "\t" 
   //   		  << "F(" << ffSFIndex << ",0,0) = " << F_00 << std::endl; 
   //}
   return res; 

}
// //______________________________________________________________________________
// Double_t POLRAD::QRT_Tau_Integrand(Double_t &tau) {
// 
//         Double_t T1=0,T2=0,arg=0;
//         fTau = tau;                                         // set fTau variable for R integrand
//         Double_t M_A     = fKinematics.GetM_A(); 
//         Double_t M       = fKinematics.GetM(); 
//         Double_t M_pion  = fKinematics.GetM_pion(); 
//         Double_t W2      = fKinematics.GetW2();  
//         Double_t R_q_min = (W2 - TMath::Power(M_A,2.0) )/(1. + tau);         // elastic peak   
//         Double_t R_q_max = (W2 - TMath::Power(M+M_pion,2.0) )/(1. + tau);    // pion production threshold   
//         Double_t min     = R_q_min; 
//         Double_t max     = R_q_max; 
// 
//         if(fDebug > 2){
// 		std::cout << "QRT Tau integrand" << std::endl;
//                 std::cout << "tau = " << tau << std::endl;
//                 std::cout << "W2 = "  << W2  << std::endl; 
// 		std::cout << "min = " << min << "\t" << "max = " << max << std::endl;
//         }
//        
// 	QRTRFuncWrap funcWrap;
// 	funcWrap.fPOLRAD = this;
// 
// 	ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kLEGENDRE; //kVEGAS,kADAPTIVE,kADAPTIVESINGULAR
// 	double absErr                            = 0.01;   /// desired absolute Error 
// 	double relErr                            = 0.01;   /// desired relative Error 
// 	unsigned int ncalls                      = 1.0e5;  /// number of calls (MC only)
// 
// 	ROOT::Math::IntegratorOneDim  ig2(type,absErr,relErr,ncalls);
// 	ig2.SetFunction( funcWrap );
// 
// 	for(Int_t i=1;i<=8;i++){
// 		for(Int_t j=1;j<=ki[i];j++){
// 			ffSFIndex = i; 
//                         fThJIndex = j; 
// 			T1        = THETAij(i,j,tau);
//                         T2        = ig2.Integral(min,max);  
// 			// T2        = AdaptiveSimpson(&POLRAD::QRT_R_Integrand,min,max,fErr,fDepth);
// 			arg      += T1*T2;
// 			if(fDebug > 2){
// 				std::cout << "i = " << i << "\t" << "j = " << j << "\t" << "R-int = " << T2 << std::endl;
// 			}
// 
// 		}
// 	}
// 
// 	return arg;
// 
// }
//______________________________________________________________________________
Double_t POLRAD::QRT_Tau_Integrand(Double_t &tau) {

   // Following Eq.20 - Ultra relativistic approx...

   fReCalculateTs = true;
   Double_t M       = fKinematics.GetM(); 
   Double_t Q2      = fKinematics.GetQ2(); 
   Double_t S_x     = fKinematics.GetS_x(); 
   Double_t R_q     = (S_x - Q2)/(1. + tau);           // follow Eq 17 in POLRAD manual, but without A subscript    
   //Double_t R_min   =  
   //Double_t R_max   = 

   fTau = tau;  // Set tau to be used when   integrating over R

   if(fDebug > 2){
      std::cout << "QRT Tau integrand" << std::endl;
      std::cout << "R_q = " << R_q << "\t" << "tau = " << tau << std::endl;
   }

   // Double_t sf[9];
   ProcessFancyQuasiElasticStructureFunctions(R_q,tau,ffSFqe); 

   //QRTRFuncWrap funcWrap;
   //funcWrap.fPOLRAD = this;
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR;
   //double absErr                            = 0.01;   /// desired absolute Error 
   //double relErr                            = 0.01;   /// desired relative Error 
   //unsigned int ncalls                      = 1.0e5;  /// number of calls (MC only)
   //ROOT::Math::IntegratorOneDim  ig2(type,absErr,relErr,ncalls);
   //ig2.SetFunction( funcWrap );
   //Double_t Integrand                       = ig2.Integral(R_min,R_max);

   Double_t res = 0.0;
   Double_t arg = 0.0; 
   for(Int_t i=1;i<=4;i++){
      for(Int_t j=1;j<=ki[i];j++){
         res += ffSFqe[i]*THETAij(i,j,tau)*2.0*M*M*TMath::Power(R_q,double(j)-2.0)/((1.0+tau)*TMath::Power(Q2 + R_q*tau,2.0));
         //num       = 2.*M*M*TMath::Power(R_q,double(j)-2.0);
         //den       = (1.+tau)*TMath::Power(Q2 + R_q*tau,2.0); 
         //T2        = num/den; 
         //T3        = sf[i];  
         //arg      += T1*T2*T3;
         //if(fDebug>3){
         //   std::cout << "---------------------------" << std::endl; 
         //   std::cout << "i   = " << i   << std::endl;
         //   std::cout << "j   = " << j   << std::endl;
         //   std::cout << "R_q = " << R_q << std::endl; 
         //   std::cout << "T1  = " << T1  << std::endl;
         //   std::cout << "num = " << num << std::endl;
         //   std::cout << "den = " << den << std::endl;
         //   std::cout << "T2  = " << T2  << std::endl;
         //   std::cout << "SF[" << i << "] = " << T3 << std::endl;
         //}
      }
   }

   arg = res;
   return arg;

}
//______________________________________________________________________________
Double_t POLRAD::QRT_R_Integrand(Double_t &R) {
   // First term
   Double_t Q2    = fKinematics.GetQ2();  
   auto J     = Double_t(fThJIndex); // fJ = fancy structure function index
   Double_t num   = TMath::Power(R,J-2.); 
   Double_t den   = TMath::Power(Q2 + R*fTau,2.0); 
   Double_t T1    = num/den; 
   // Second term 
   ProcessFancyQuasiElasticStructureFunctions(R,fTau,ffSFqe); 
   Double_t T2    = ffSFqe[ffSFIndex]; 
   Double_t arg   = T1*T2;
   return arg;

}
//______________________________________________________________________________
Double_t POLRAD::MultiPhoton(Double_t Q2){

   /// Multiple photon radiation 
   /// Phys Rev D 12, 1884 (1975) 
   /// Eq A58
   // general terms  
   Double_t Es      = fKinematics.GetEs();
   Double_t Ep      = fKinematics.GetEp();
   Double_t th      = fKinematics.GetTheta();
   Double_t SIN     = TMath::Sin(th/2.);
   Double_t SIN2    = SIN*SIN;
   Double_t MT      = fKinematics.GetM_A();
   Double_t m       = fKinematics.Getm();
   Double_t alpha   = fine_structure_const;
   // omega_s (Eq A50)  
   Double_t num_s   = Ep;
   Double_t den_s   = 1. - 2.*(Ep/MT)*SIN2;
   Double_t omega_s = Es - (num_s/den_s);
   // omega_p (Eq A51) 
   Double_t num_p   = Es;
   Double_t den_p   = 1. + 2.*(Es/MT)*SIN2;
   Double_t omega_p = (num_p/den_p) - Ep;
   // v terms 
   Double_t v_s     = omega_s/Es;
   Double_t v_p     = omega_p/(Ep + omega_p);
   // Z terms 
   Double_t Z       = fZ;
   Double_t Z13     = TMath::Power(Z,-1./3);
   Double_t Z23     = TMath::Power(Z,-2./3);
   // eta (Eq A46) 
   Double_t eta     = TMath::Log(1440.*Z23)/TMath::Log(183.*Z13);
   // b   (Eq A45)  
   Double_t b_num   = (1./9.)*(Z+1.)/(Z+eta);
   Double_t b_den   = TMath::Log(183.*Z13);
   Double_t b       = (4./3.)*(1. + b_num/b_den);
   // t_r (Eq A57) 
   Double_t t_r     = (alpha/pi)*(1./b)*( TMath::Log( Q2/(m*m) ) - 1.);
   // First term 
   Double_t T1      = TMath::Power( v_s, b*(ft_b + t_r) ); 
   // Second term 
   Double_t T2      = TMath::Power( v_p, b*(ft_a + t_r) ); 
   // Put it together 
   Double_t arg     = T1*T2; 
   return arg;

}
//______________________________________________________________________________
Double_t POLRAD::MultiPhoton_el(Double_t x,Double_t y,Double_t Q2) {

   Double_t m     = fKinematics.Getm();
   Double_t num   = y*y*TMath::Power(1. - (x/fA),2.0);
   Double_t den   = 1 - x*y/fA;
   Double_t arg   = num/den;
   Double_t alpha = fine_structure_const;
   Double_t l_m   = TMath::Log( Q2/(m*m) );
   Double_t tr    = (alpha/pi)*(l_m - 1.);
   Double_t mp_el = TMath::Power(arg,tr);
   return mp_el;

}
//______________________________________________________________________________
Double_t POLRAD::MultiPhoton_qe(Double_t x,Double_t y,Double_t Q2) {

   Double_t m     = fKinematics.Getm(); 
   Double_t num   = y*y*TMath::Power(1. - x,2.0);
   Double_t den   = 1. - x*y;
   Double_t arg   = num/den;
   Double_t alpha = fine_structure_const;
   Double_t l_m   = TMath::Log (Q2/(m*m) );
   Double_t tr    = (alpha/pi)*(l_m - 1.);
   Double_t mp_qe = TMath::Power(arg,tr);
   return mp_qe;

}
//____________________________________________________________________________________
Double_t POLRAD::AdaptiveSimpson(Double_t (POLRAD::*f)(Double_t &) ,Double_t A,Double_t B,
      Double_t epsilon,Int_t Depth) {
   // Adaptive Simpson's Rule
   Double_t C = (A + B)/2.0;
   Double_t H = B - A;
   Double_t fa = (this->*f)(A);
   Double_t fb = (this->*f)(B);
   Double_t fc = (this->*f)(C);
   Double_t S = (H/6.0)*(fa + 4.0*fc + fb);
   Double_t arg = AdaptiveSimpsonAux(f,A,B,epsilon,S,fa,fb,fc,Depth);
   return arg;
}
//____________________________________________________________________________________
Double_t POLRAD::AdaptiveSimpsonAux(Double_t (POLRAD::*f)(Double_t &) ,Double_t A,Double_t B,
      Double_t epsilon,Double_t S,Double_t fa,Double_t fb,Double_t fc,Int_t bottom) {
   // Recursive auxiliary function for AdaptiveSimpson() function
   Double_t C      = (A + B)/2.0;
   Double_t H      = B - A;
   Double_t D      = (A + C)/2.0;
   Double_t E      = (C + B)/2.0;
   Double_t fd     = (this->*f)(D);
   Double_t fe     = (this->*f)(E);
   Double_t Sleft  = (H/12.0)*(fa + 4.0*fd + fc);
   Double_t Sright = (H/12.0)*(fc + 4.0*fe + fb);
   Double_t S2 = Sleft + Sright;
   if (bottom <= 0 || fabs(S2 - S) <= 15.0*epsilon){
      return S2 + (S2 - S)/15;
   }
   Double_t arg = AdaptiveSimpsonAux(f,A,C,epsilon/2.0,Sleft, fa,fc,fd,bottom-1) +
      AdaptiveSimpsonAux(f,C,B,epsilon/2.0,Sright,fc,fb,fe,bottom-1);
   return arg;
}
}}
