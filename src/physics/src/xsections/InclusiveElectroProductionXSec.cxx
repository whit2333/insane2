#include "insane/xsections/InclusiveElectroProductionXSec.h"

#include "insane/pdfs/FortranWrappers.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "insane/xsections/WiserXSection.h"
#include "insane/xsections/WiserInclusivePhotoXSec.h"


namespace insane {
namespace physics {

InclusiveElectroProductionXSec::InclusiveElectroProductionXSec()
{ 
   fID         = 100003001;
   fBaseID     = fID;
   fPIDs.clear();
   fPIDs.push_back(111);   // pi0

   fSig_0     = new PhotonDiffXSec();
   fSig_plus  = new PhotonDiffXSec();
   fSig_minus = new PhotonDiffXSec();

   fSig_0->SetProductionParticleType(111);
   fSig_plus->SetProductionParticleType(211);
   fSig_minus->SetProductionParticleType(-211);
}
//______________________________________________________________________________

InclusiveElectroProductionXSec::~InclusiveElectroProductionXSec()
{
   delete fSig_0     ;
   delete fSig_plus  ;
   delete fSig_minus ;
}
//______________________________________________________________________________

InclusiveElectroProductionXSec::InclusiveElectroProductionXSec(const InclusiveElectroProductionXSec& rhs) : 
   PhotonDiffXSec(rhs)
{
   (*this) = rhs;
}
//______________________________________________________________________________

InclusiveElectroProductionXSec& InclusiveElectroProductionXSec::operator=(const InclusiveElectroProductionXSec& rhs) 
{
   if (this != &rhs) {  // make sure not same object
      PhotonDiffXSec::operator=(rhs);
      fSig_0     = rhs.fSig_0->Clone();
      fSig_plus  = rhs.fSig_plus->Clone();
      fSig_minus = rhs.fSig_minus->Clone();
   }
   return *this;    // Return ref for multiple assignment
}
//______________________________________________________________________________

InclusiveElectroProductionXSec*  InclusiveElectroProductionXSec::Clone(const char * newname) const 
{
   std::cout << "InclusiveElectroProductionXSec::Clone()\n";
   auto * copy = new InclusiveElectroProductionXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

InclusiveElectroProductionXSec*  InclusiveElectroProductionXSec::Clone() const
{ 
   return( Clone("") );
} 
//______________________________________________________________________________

void  InclusiveElectroProductionXSec::SetParameters( int i, const std::vector<double>& pars )
{
   if(i == 0 ) {      fParameters0 = pars; fSig_0->fParameters0 = pars;     fSig_0->SetParameters(pars);     fSig_0->SetProductionParticleType(fSig_0->GetParticleType());}  
   else if(i == 1 ) { fParameters1 = pars; fSig_plus->fParameters1 = pars;  fSig_plus->SetParameters(pars);  fSig_plus->SetProductionParticleType(fSig_plus->GetParticleType());}  
   else if(i == 2 ) { fParameters2 = pars; fSig_minus->fParameters2 = pars; fSig_minus->SetParameters(pars); fSig_minus->SetProductionParticleType(fSig_minus->GetParticleType());}  
   else  { 
      fParameters0 = pars; fSig_0->fParameters0 = pars;     fSig_0->SetParameters(pars); fSig_0->SetProductionParticleType(fSig_0->GetParticleType());
      fParameters1 = pars; fSig_plus->fParameters1 = pars;  fSig_plus->SetParameters(pars); fSig_plus->SetProductionParticleType(fSig_plus->GetParticleType());
      fParameters2 = pars; fSig_minus->fParameters2 = pars; fSig_minus->SetParameters(pars); fSig_minus->SetProductionParticleType(fSig_minus->GetParticleType()); 
   }  
   // this is needed so that fParameters is set to the correct vector above.
   SetProductionParticleType(GetParticleType());
   //std::cout << "p0 = " << pars[0] << std::endl;
}
//______________________________________________________________________________

const std::vector<double>& InclusiveElectroProductionXSec::GetParameters() const 
{
   return fParameters;
}
//______________________________________________________________________________

void  InclusiveElectroProductionXSec::SetPhaseSpace(PhaseSpace * ps)
{
   fSig_0->SetPhaseSpace(ps);
   fSig_plus->SetPhaseSpace(ps);
   fSig_minus->SetPhaseSpace(ps);
   InclusiveDiffXSec::SetPhaseSpace(ps);
}
//______________________________________________________________________________

void InclusiveElectroProductionXSec::InitializeFinalStateParticles()
{
   if(fSig_0)  fSig_0->InitializeFinalStateParticles();
   if(fSig_plus) fSig_plus->InitializeFinalStateParticles();
   if(fSig_minus) fSig_minus->InitializeFinalStateParticles();
   InclusiveDiffXSec::InitializeFinalStateParticles();
}
//_____________________________________________________________________________

void InclusiveElectroProductionXSec::InitializePhaseSpaceVariables() 
{
   fSig_0->InitializePhaseSpaceVariables()    ;
   PhaseSpace * ps = fSig_0->GetPhaseSpace();
   fSig_plus->SetPhaseSpace(ps);
   fSig_minus->SetPhaseSpace(ps);
   InclusiveDiffXSec::SetPhaseSpace(ps);
}
//______________________________________________________________________________

Double_t InclusiveElectroProductionXSec::EvaluateXSec(const Double_t * x) const 
{

   double RES            = 0.0;
   int    PART           = fParticle->PdgCode();
   if( GetTargetNucleus() == Nucleus::Neutron() ) {
      // if target is a neutron use isospin to get the result
      if(fParticle->PdgCode() != 111) {
         // make sure it is not set to pi0
         PART = -1*fParticle->PdgCode();
      }
   } else if( !(GetTargetNucleus() == Nucleus::Proton()) ) {
      std::cout << " NOT Proton or neutron" << std::endl;
   }

   double EBEAM          = GetBeamEnergy();
   double Epart          = x[0];
   double Ppart          = TMath::Sqrt(Epart*Epart - M_pion*M_pion/(GeV*GeV));
   double THETA          = x[1];

   // Solution to Tiator-wright Eqn.5 with theta_e=0
   double num = TMath::Power(Ppart,2.0) + TMath::Power(M_p/GeV,2.0) - TMath::Power(M_p/GeV - Epart, 2.0);
   double denom = 2.0*(Ppart*TMath::Cos(THETA) + M_p/GeV - Epart);

   double AMT = M_p/MeV;//is target mass?
   double AM1 = M_pion/MeV;//is produced particle mass (MeV/c2)
   double EI  = EBEAM*1000.0;//is the electron beam energy (MeV)
   double W0  = (num/denom)*1000.0;//is the theta=0 photon energy (MeV)
   double TP  = (Epart - M_pion/GeV)*1000.0;//is the kinetic energy of the produced particle (MeV)
   double TH  = THETA;//is the angle of the produced particle (radians)
   double GN  = 0.0;//the result of Ne*R/w0 of eqn.12 in Nuc.Phys.A379

   if( W0<0.0 || W0>EI ) {
      GN=0.0;
   } else {
      vtp_(&AMT,&AM1,&EI,&W0,&TP,&TH,&GN);
      GN *= 1000.0; // because GN has units 1/MeV
   }

   //std::cout << "W0 = " << W0 << std::endl;
   //std::cout << "GN = " << GN << std::endl;
   //wiser_all_sig_(&RES,&PART,&EBEAM,&PscatteredPart,&THETA);
   double tot = 0.0;
   if(PART==111){
      // pi0 = (pi+ + pi-)/2.0
      fSig_plus->SetBeamEnergy(EBEAM);
      RES = fSig_plus->EvaluateXSec(x);
      if(RES<0.0) RES = 0.0;
      tot  += RES;

      fSig_minus->SetBeamEnergy(EBEAM);
      RES = fSig_minus->EvaluateXSec(x);

      if(RES<0.0) RES = 0.0;
      tot  += RES;
      tot = tot/2.0;

   } else if( PART==211 ){

      fSig_plus->SetBeamEnergy(EBEAM);
      RES = fSig_plus->EvaluateXSec(x);

      if(RES<0.0) RES = 0.0;
      tot  += RES;

   } else if( PART==-211 ){

      fSig_minus->SetBeamEnergy(EBEAM);
      RES = fSig_minus->EvaluateXSec(x);

      if(RES<0.0) RES = 0.0;
      tot  += RES;

   }

   // RES is Ex*dsig/dp^3 with units of GeV-ub/(GeV/c)^2
   if(tot<0.0) tot = 0.0;
   //tot *= (Ppart); // Takes dp^3 to dEdOmega 
   tot *= GN*1000; // conv
   if( TMath::IsNaN(tot) ) tot = 0.0;
   //std::cout << " wiser result is " << RES << " nb/GeV*str "
   //<< " for p,theta " << PscatteredPart << "," << THETA << "\n";
   if ( IncludeJacobian() ) return(tot * TMath::Sin(x[1]) ); 
   return(tot); // converts nb to mb
}
//______________________________________________________________________________




//InclusiveElectroProductionXSec::InclusiveElectroProductionXSec()
//{
//   fID     = 100103001;
//   fBaseID = fID;
//   fTitle  = "InclusiveElectroProductionXSec";
//   fPlotTitle = "#frac{d#sigma}{dE_{#pi} d#Omega_{#pi}} nb/GeV-Sr";
//   fPIDs.clear();
//   fPIDs.push_back(111);//
//   fProtonXSec  = new InclusiveElectroProductionXSec();
//   fProtonXSec->SetTargetNucleus(Nucleus::Proton());
//   fNeutronXSec = new InclusiveElectroProductionXSec();
//   fNeutronXSec->SetTargetNucleus(Nucleus::Neutron());
//   fProtonXSec->UsePhaseSpace(false);
//   fNeutronXSec->UsePhaseSpace(false);
//   fAlphaCT  = 0.85;
//}
////_____________________________________________________________________________
//
//InclusiveElectroProductionXSec::~InclusiveElectroProductionXSec()
//{
//}
////______________________________________________________________________________
//
//InclusiveElectroProductionXSec::InclusiveElectroProductionXSec(const InclusiveElectroProductionXSec& rhs) : 
//   CompositeDiffXSec(rhs)
//{
//   (*this) = rhs;
//}
////______________________________________________________________________________
//
//InclusiveElectroProductionXSec& InclusiveElectroProductionXSec::operator=(const InclusiveElectroProductionXSec& rhs) 
//{
//   if (this != &rhs) {  // make sure not same object
//      CompositeDiffXSec::operator=(rhs);
//      fProtonXSec  = rhs.fProtonXSec->Clone();
//      fNeutronXSec = rhs.fNeutronXSec->Clone();
//   }
//   return *this;    // Return ref for multiple assignment
//}
////______________________________________________________________________________
//
//InclusiveElectroProductionXSec*  InclusiveElectroProductionXSec::Clone(const char * newname) const 
//{
//   std::cout << "InclusiveElectroProductionXSec::Clone()\n";
//   auto * copy = new InclusiveElectroProductionXSec();
//   (*copy) = (*this);
//   return copy;
//}
////______________________________________________________________________________
//
//InclusiveElectroProductionXSec*  InclusiveElectroProductionXSec::Clone() const
//{ 
//   return( Clone("") );
//} 
////______________________________________________________________________________
//
//Double_t  InclusiveElectroProductionXSec::EvaluateXSec(const Double_t * x) const
//{
//   //std::cout << " composite eval " << std::endl;
//   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);
//
//   if( !fProtonXSec )  return 0.0;
//   if( !fNeutronXSec ) return 0.0;
//
//   Double_t z    = GetZ();
//   //if( z>0.0 ) z = 1.0;
//   Double_t n    = GetN();
//   //if( n>0.0 ) n = 1.0;
//   Double_t a    = GetA();
//
//   Double_t sigP = fProtonXSec->EvaluateXSec(x);
//   Double_t sigN = fNeutronXSec->EvaluateXSec(x);
//   //std::cout << " sig_p = " << sigP << std::endl;
//   //std::cout << " sig_n = " << sigN << std::endl;
//   Double_t xsec = z*sigP + n*sigN;
//
//   // Take into account nuclear transparency sigma_A = A^alpha sigma_p
//   //if( a > 2.0 ) xsec = (xsec/a)*TMath::Power(a, 0.9);//fAlphaCT);
//
//   if( IncludeJacobian() ) return xsec*TMath::Sin(x[1]);
//   return xsec;
//}
////______________________________________________________________________________

}}
