#include "insane/xsections/pInclusiveElasticDiffXSec.h"
#include "insane/formfactors/DipoleFormFactors.h"


namespace insane {
namespace physics {
pInclusiveElasticDiffXSec::pInclusiveElasticDiffXSec()
{
   fID         = 100004012;
   fnDim = 3;
   fnParticles = 1;
   fPIDs.clear();
   fPIDs.push_back(2212);//proton
   //fDipoleFFs  = new DipoleFormFactors();
}
//______________________________________________________________________________

pInclusiveElasticDiffXSec::~pInclusiveElasticDiffXSec()
{ }
//______________________________________________________________________________

pInclusiveElasticDiffXSec * pInclusiveElasticDiffXSec::Clone(const char * newname) const
{
   std::cout << "pInclusiveElasticDiffXSec::Clone()\n";
   auto * copy = new pInclusiveElasticDiffXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

Double_t  pInclusiveElasticDiffXSec::EvaluateXSec(const Double_t * x) const 
{
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

//     std::cout << " Beam = " << GetBeamEnergy() << "\n";
//     std::cout << " Electron:\n";
//     std::cout << " *fEnergy_e = " <<*fEnergy_e << "\n";
//     std::cout << " *fTheta_e = " <<*fTheta_e/0.0175 << "\n";
//     std::cout << " *fPhi_e = " <<*fPhi_e/0.0175 << "\n";
//     std::cout << " Proton:\n";
//     std::cout << " *fEnergy_p = " <<*fEnergy_p << "\n";
//     std::cout << " *fTheta_p = " <<*fTheta_p/0.0175 << "\n";
//     std::cout << " *fPhi_p = " <<*fPhi_p/0.0175 << "\n";
//     std::cout << " \n";

   Double_t Eprime = x[0];
   Double_t theta = x[1];
   Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
   Double_t Qsquared = 4.0 * Eprime * GetBeamEnergy() * TMath::Power(TMath::Sin(theta / 2.0), 2);
   Double_t tau = Qsquared / (4.0 * M * M);
   Double_t hbarc2 = 0.38939129; /*(hbar*c)^2 = 0.38939129 GeV^2 mbarn */
   Double_t mottXSec = hbarc2 * (1. / 137.) * (1. / 137.) *
                       TMath::Power(TMath::Cos(theta / 2.0), 2) /
                       (4.0 * GetBeamEnergy() * GetBeamEnergy() * TMath::Power(TMath::Sin(theta / 2.0), 4))
                       * (Eprime / GetBeamEnergy());
   Double_t GE2 = TMath::Power(fFormFactors->GEp(Qsquared), 2);
   Double_t GM2 = TMath::Power(fFormFactors->GMp(Qsquared), 2);

//    std::cout << " Qsq= " << Qsquared;
//    std::cout << " tau= " << tau;
//    std::cout << " GE2= " << GE2;
//    std::cout << " GM2= " << GM2  << "\n";;

   Double_t res = mottXSec * ((GE2 + tau * GM2) / (1.0 + tau) + 2.0 * tau * GM2 * TMath::Power(TMath::Tan(theta / 2.0), 2));
   return(res);
}

//________________________________________________________________________
void pInclusiveElasticDiffXSec::InitializePhaseSpaceVariables()
{
   auto * ps = GetPhaseSpace();

   auto * varEnergy = new PhaseSpaceVariable();
   varEnergy = new PhaseSpaceVariable();
   varEnergy->SetNameTitle("momentum", "P_{p}");
   varEnergy->SetMinimum(0.8); //GeV
   varEnergy->SetMaximum(5.9); //GeV
   /*      fEnergy_e = varEnergy->GetCurrentValueAddress();*/
   //fEnergy = varEnergy->GetCurrentValueAddress();
   ps->AddVariable(varEnergy);

   auto *   varTheta = new PhaseSpaceVariable();
   varTheta->SetNameTitle("theta", "#theta_{p}"); // ROOT string latex
   varTheta->SetMinimum(30.0 * TMath::Pi() / 180.0); //
   varTheta->SetMaximum(50.0 * TMath::Pi() / 180.0); //
   /*      fTheta_e = varTheta->GetCurrentValueAddress();*/
   //fTheta = varTheta->GetCurrentValueAddress();
   ps->AddVariable(varTheta);

   auto *   varPhi = new PhaseSpaceVariable();
   varPhi->SetNameTitle("phi", "#phi_{p}"); // ROOT string latex
   varPhi->SetMinimum(-40.0 * TMath::Pi() / 180.0); //
   varPhi->SetMaximum(40.0 * TMath::Pi() / 180.0); //
   /*      fPhi_e = varPhi->GetCurrentValueAddress();*/
   //fPhi = varPhi->GetCurrentValueAddress();
   ps->AddVariable(varPhi);
}

//________________________________________________________________________
}}
