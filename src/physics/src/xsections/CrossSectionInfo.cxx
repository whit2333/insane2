#include "insane/xsections/CrossSectionInfo.h"

insane::physics::CrossSectionInfo* insane::physics::CrossSectionInfo::fgCrossSectionInfo = nullptr;
//______________________________________________________________________________

insane::physics::CrossSectionInfo::CrossSectionInfo( )
{
   fNameMap = {
      { 100000000, "InSANEInclusiveDiffXSec                " },
      { 100000001, "InSANEFlatInclusiveDiffXSec            " },
      { 100000002, "InSANEInclusiveMottXSec                " },
      { 100000010, "InSANEInclusiveBornDISXSec             " },
      { 100000011, "CTEQ6eInclusiveDiffXSec                " },
      { 100000012, "F1F209eInclusiveDiffXSec               " },
      { 100100000, "InSANECompositeDiffXSec                " },
      { 100100001, "InSANEInclusiveDISXSec                 " },
      { 100001001, "InSANEPhotonDiffXSec                   " },
      { 100001002, "InSANEInclusiveWiserXSec               " },
      { 100002001, "InSANEInclusivePhotoProductionXSec     " },
      { 100102001, "InclusivePhotoProductionXSec           " },
      { 100003001, "InSANEInclusiveElectroProductionXSec   " },
      { 100103001, "InclusiveElectroProductionXSec         " },
      { 100002002, "WiserInclusivePhotoXSec                " },
      { 100002003, "WiserInclusivePhotoXSec2               " },
      { 100102002, "PhotoWiserDiffXSec                     " },
      { 100102003, "PhotoWiserDiffXSec2                    " },
      { 100003002, "WiserInclusiveElectroXSec              " },
      { 100103002, "ElectroWiserDiffXSec                   " },
      { 100001012, "OARPionDiffXSec                        " },
      { 100002012, "OARInclusivePhotoXSec                  " },
      { 100102012, "PhotoOARPionDiffXSec                   " },
      { 100003012, "OARPionElectroDiffXSec                 " },
      { 100103012, "ElectroOARPionDiffXSec                 " },
      { 100004001, "InSANEInclusiveEPCVXSec                " },
      { 100004002, "InSANEInclusiveEPCVXSec2               " },
      { 100004011, "InSANEeInclusiveElasticDiffXSec        " },
      { 100004012, "InSANEpInclusiveElasticDiffXSec        " },
      { 100004021, "InSANEInclusiveMollerDiffXSec          " },
      { 100104031, "MAIDInclusiveDiffXSec                  " },
      { 100104032, "MAIDInclusivePionDiffXSec              " },
      { 100104033, "MAIDInclusiveElectronDiffXSec          " },
      { 100104034, "MAIDNucleusInclusiveDiffXSec           " },
      { 100104035, "MAIDPolarizedTargetDiffXSec            " },
      { 100010001, "InSANEPOLRADBornDiffXSec               " },
      { 100010002, "InSANEPOLRADRadiatedDiffXSec           " },
      { 100010002, "InSANEPOLRADElasticTailDiffXSec        " },
      { 100010003, "InSANEPOLRADInelasticTailDiffXSec      " },
      { 100010004, "InSANEPOLRADInternalPolarizedDiffXSec  " },
      { 100010005, "InSANEPOLRADQuasiElasticTailDiffXSec   " },
      { 100010011, "InSANERadiativeTail                    " },
      { 100010012, "InSANEInelasticRadiativeTail           " },
      { 100010013, "InSANEInelasticRadiativeTail2          " },
      { 100010014, "InSANEElasticRadiativeTail             " },
      { 100010021, "InSANERADCORInternalUnpolarizedDiffXSec" },
      { 100010022, "InSANERADCORRadiatedDiffXSec           " },
      { 100010023, "InSANERADCORRadiatedUnpolarizedDiffXSec" },
      { 100010031, "InSANEPolarizedCrossSectionDifference  " },
      {  10000000, "InclusiveHadronProductionXSec<T>"        }, 
      {  20000000, "InSANERadiator<T>"                       }
   };

}
//______________________________________________________________________________

insane::physics::CrossSectionInfo::~CrossSectionInfo()
{ }
//______________________________________________________________________________

insane::physics::CrossSectionInfo* insane::physics::CrossSectionInfo::GetInstance()
{ 
   if ( fgCrossSectionInfo == nullptr )
   {
      fgCrossSectionInfo = new CrossSectionInfo ( ) ;
   }
   return fgCrossSectionInfo;
}
//______________________________________________________________________________



