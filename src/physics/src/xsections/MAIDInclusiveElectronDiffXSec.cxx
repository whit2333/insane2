#include "insane/xsections/MAIDInclusiveElectronDiffXSec.h"

namespace insane {
namespace physics {
MAIDInclusiveElectronDiffXSec::MAIDInclusiveElectronDiffXSec(
      const char * nucleon)
{
   fID         = 100104033;
   fXSec0 = new MAIDExclusivePionDiffXSec("pi0",nucleon);
   if( !strcmp("p",nucleon)  )
      fXSec1 = new MAIDExclusivePionDiffXSec("piplus","n");
   else 
      fXSec1 = new MAIDExclusivePionDiffXSec("piminus","p");
   fSig0.fXSec = fXSec0;
   fSig1.fXSec = fXSec1;

   // Integration options
   fIntType = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   fAbsErr                           = 1.0e-4;   /// desired absolute Error 
   fRelErr                           = 1.0e-4;   /// desired relative Error 
   fNcalls                           = 1e3;    /// number of calls (MC only)
   fRule                             = 1;        /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    
}

//________________________________________________________________________________
MAIDInclusiveElectronDiffXSec::~MAIDInclusiveElectronDiffXSec(){
   delete fXSec0;
   delete fXSec1;
}
//________________________________________________________________________________
Double_t   MAIDInclusiveElectronDiffXSec::EvaluateXSec(const Double_t *x) const {
   if (!VariablesInPhaseSpace(GetPhaseSpace()->GetDimension(), x)) return(0.0);
   fSig0.fArgs[0] = x[0];
   fSig0.fArgs[1] = x[1];
   fSig0.fArgs[2] = x[2];
   Double_t min[2] = {0.0        , 0.0};
   Double_t max[2] = {TMath::Pi(), 2.0*TMath::Pi()};
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kMISER;
   ROOT::Math::IntegratorMultiDim ig2(fIntType,fAbsErr,fRelErr,fNcalls); 
   //ig2.SetRelTolerance(relErr);
  
   ig2.SetFunction(fSig0);
   Double_t res       = ig2.Integral(min,max); 

   ig2.SetFunction(fSig1);
   res               += ig2.Integral(min,max); 

   //std::cout << " res = " << res << std::endl;
   if(IncludeJacobian()) return res*TMath::Sin(x[1]);
   return(res);
}

//________________________________________________________________________________
Double_t * MAIDInclusiveElectronDiffXSec::GetDependentVariables(const Double_t * x) const {
   fDependentVariables[0] = x[0];
   fDependentVariables[1] = x[1];
   fDependentVariables[2] = x[2];
   return(fDependentVariables);
}

//________________________________________________________________________________
void       MAIDInclusiveElectronDiffXSec::InitializePhaseSpaceVariables(){
   fXSec0->InitializePhaseSpaceVariables();
   fXSec1->InitializePhaseSpaceVariables();
   InclusiveDiffXSec::InitializePhaseSpaceVariables();
}

//________________________________________________________________________________
}}
