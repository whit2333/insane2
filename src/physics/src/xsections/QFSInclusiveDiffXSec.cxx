#include "insane/xsections/QFSInclusiveDiffXSec.h"

namespace insane {
namespace physics {
//___________________________________________________________________

QFSXSecConfiguration::QFSXSecConfiguration()
{
   fTAG    = "'4_SY'"; /// \todo fix this stupid hard coded configurations
   fTarget = "'He3'";
   fTH     = 40.0;  // degrees

   fPF     = 130.0; // MeV/c
   fEPS    = 10.0;  // MeV      fermi momentum
   fEPSD   = 15.0;  // MeV

   fInputFileDocumentation = "# \n";
   fInputFileDocumentation += "# This file is read when \"INTERACTIVE\" flag is set to False.";
   fInputFileDocumentation += "# Lines starting with \"#\" are ignored.\n";
   fInputFileDocumentation += "# 1)TAG    ! Select cell and energy from E94010. \n";
   fInputFileDocumentation += "# 2)Target ! 'He3' : z= 2,A= 3 \n";
   fInputFileDocumentation += "#          ! 'Nit' : z= 7,A=14\n";
   fInputFileDocumentation += "# 3)TH     ! Scattering angle in degrees\n";
   fInputFileDocumentation += "# 4)PF     ! FERMI MOMENTUM [MEV/C]           (Affects width of QE)\n";
   fInputFileDocumentation += "# 5)EPS    ! NUCLEON SEPARATION ENERGY [MEV]. (Shifts QE central value)\n";
   fInputFileDocumentation += "# 6)EPSD   ! DELTA SEPARATION ENERGY [MEV].   (Shifts Delta central value)\n";
   fInputFileDocumentation += "# \n";
}
//___________________________________________________________________
QFSXSecConfiguration::~QFSXSecConfiguration() {}
//___________________________________________________________________
void QFSXSecConfiguration::Print(const Option_t * opt){

   // std::cout <<  "TAG:    " << fTAG    << std::endl; 
   // std::cout <<  "Target: " << fTarget << std::endl;  
   // std::cout <<  "TH:     " << fTH     << std::endl;  
   std::cout <<  "PF:     " << fPF     << std::endl;  
   std::cout <<  "EPS:    " << fEPS    << std::endl;  
   std::cout <<  "EPSD:   " << fEPSD   << std::endl;  


}
//___________________________________________________________________
Int_t QFSXSecConfiguration::WriteQFSInputFile() {
   std::ofstream qfsFile;
   qfsFile << fInputFileDocumentation.Data();
   qfsFile << fTAG.Data() << " ";
   qfsFile << fTarget.Data() << " ";
   qfsFile << fTH << " ";
   qfsFile << fPF << " ";
   qfsFile << fEPS << " ";
   qfsFile << fEPSD << " ";
   qfsFile << "\n";
   qfsFile.open("qfs_input.dat", std::ios::trunc);
   return(0);
}
//___________________________________________________________________



//___________________________________________________________________
QFSInclusiveDiffXSec::QFSInclusiveDiffXSec() {
   fIsConfigured = true;
   fConfiguration = new QFSXSecConfiguration();
   fXSTYPE = 0; 
}
//___________________________________________________________________
QFSInclusiveDiffXSec::~QFSInclusiveDiffXSec() {
   delete fConfiguration;
}
//___________________________________________________________________
Double_t QFSInclusiveDiffXSec::EvaluateXSec(const Double_t * x) const {
   if (!fIsConfigured) {
      Error("EvaluateXSec","QFS unconfigured.");
   }
   // if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);
   double RES      = 0;
   double PF       = fConfiguration->GetFermiMomentum(); 
   double EPS      = fConfiguration->GetNucleonSeparationEnergy(); 
   double EPSD     = fConfiguration->GetDeltaSeparationEnergy(); 
   double Z        = GetZ();
   double N        = GetN();
   double EBEAM    = fBeamEnergy * 1000.0;
   double PCENTRAL = x[0] * 1000.0;
   //double DELP     = 0.1;
   double THETA    = x[1];
   //double DELTHETA = 1.0*degree;
   auto XSTYPE      = (int)fXSTYPE; 
   //qfs_born_(&RES,&Z,&N,&EPS,&EPSD,&PF,&EBEAM,&PCENTRAL,&DELP,&THETA,&DELTHETA,&XSTYPE);
   qfs_born_(&RES,&Z,&N,&EPS,&EPSD,&PF,&EBEAM,&PCENTRAL,&THETA,&XSTYPE);
   /*      std::cout << " qfs results is " << RES << " pb. \n";*/
   return(RES);
}
//___________________________________________________________________
void QFSInclusiveDiffXSec::Configure() {
   if (!fIsConfigured) {
      if (!(fConfiguration->WriteQFSInputFile())) fIsConfigured = true;
      else std::cout << " x - Failed to write QFS Configuration \n";
      std::cout << " o QFSInclusiveDiffXSec configured. \n";
   } else {
      std::cout << " o QFSInclusiveDiffXSec already configured. \n";
   }
}
//___________________________________________________________________
}}
