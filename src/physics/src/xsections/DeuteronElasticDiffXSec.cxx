#include "insane/xsections/DeuteronElasticDiffXSec.h"
#include "insane/base/PhysicalConstants.h"

namespace insane {
   namespace physics {

DeuteronElasticDiffXSec::DeuteronElasticDiffXSec()
{
   fID            = 200010020;
   SetTitle("DeuteronElasticDiffXSec");
   SetPlotTitle("Elastic cross-section");
   fLabel         = "#frac{d#sigma}{d#Omega}";
   fUnits         = "nb/sr";
   fnDim          = 2;
   fPIDs.clear();
   fnParticles    = 2;
   fPIDs.push_back(11);
   fPIDs.push_back(1000010020);
}
//______________________________________________________________________________

DeuteronElasticDiffXSec::~DeuteronElasticDiffXSec(){
}
//______________________________________________________________________________

void DeuteronElasticDiffXSec::InitializePhaseSpaceVariables()
{

   PhaseSpace * ps = GetPhaseSpace();
   //if(ps) delete ps;
   //ps = nullptr;
   //std::cout << " Creating NEW PhaseSpace for DeuteronElasticDiffXSec\n";
   //ps = new PhaseSpace();

   // ------------------------------
   // Electron
   auto * varEnergy = new PhaseSpaceVariable(
         "energy_e", "E_{e'}", 0.50, GetBeamEnergy());
   varEnergy->SetDependent(true);
   ps->AddVariable(varEnergy);

   auto *   varTheta = new PhaseSpaceVariable(
         "theta_e", "#theta_{e'}", 1.0*degree, 60.0*degree);
   ps->AddVariable(varTheta);

   auto *   varPhi = new PhaseSpaceVariable(
         "phi_e", "#phi_{e'}", -180.0*degree, 180.0*degree);
   varPhi->SetUniform(true);
   ps->AddVariable(varPhi);


   // ------------------------------
   // D
   auto * varP_D = new PhaseSpaceVariable(
         "P_D", "P_{D}",0.9,5.0);
   varP_D->SetParticleIndex(1);
   varP_D->SetDependent(true);
   ps->AddVariable(varP_D);

   auto *   varTheta_D = new PhaseSpaceVariable( 
         "theta_D", "#theta_{D}", 30*degree, 180.0*degree);
   varTheta_D->SetParticleIndex(1);
   varTheta_D->SetDependent(true);
   ps->AddVariable(varTheta_D);

   auto *   varPhi_D = new PhaseSpaceVariable(
         "phi_D", "#phi_{D}", -180.0*degree, 180.0*degree);
   varPhi_D->SetParticleIndex(1);
   varPhi_D->SetDependent(true);
   ps->AddVariable(varPhi_D);

   SetPhaseSpace(ps);
}
//______________________________________________________________________________

Double_t * DeuteronElasticDiffXSec::GetDependentVariables(const Double_t * x) const
{
   //std::cout << "x[0] = " << x[0] << std::endl;
   //std::cout << "x[1] = " << x[1] << std::endl;
   //std::cout << "x[2] = " << x[2] << std::endl;
   //std::cout << "x[3] = " << x[3] << std::endl;
   //std::cout << "x[4] = " << x[4] << std::endl;
   //std::cout << "x[5] = " << x[5] << std::endl;
   double mass = fTargetNucleus.GetMass();
   TVector3 k1(0, 0, fBeamEnergy); // incident electron
   TVector3 k2(0, 0, 0);          // scattered electron
   double eprime =  GetEPrime(x[0]);
   k2.SetMagThetaPhi(eprime, x[0], x[1]);
   TVector3 p2 = k1 - k2; // recoil proton
   fDependentVariables[0] = eprime;
   fDependentVariables[1] = x[0];
   fDependentVariables[2] = x[1];
   fDependentVariables[3] = TMath::Sqrt(p2.Mag2()+mass*mass);
   fDependentVariables[4] = p2.Theta();
   fDependentVariables[5] = p2.Phi();
   //std::cout << "vectors" << std::endl;
   //std::cout << "fDependentVariables[0] = " << fDependentVariables[0] << std::endl;
   //std::cout << "fDependentVariables[1] = " << fDependentVariables[1] << std::endl;
   //std::cout << "fDependentVariables[2] = " << fDependentVariables[2] << std::endl;
   //std::cout << "fDependentVariables[3] = " << fDependentVariables[3] << std::endl;
   //std::cout << "fDependentVariables[4] = " << fDependentVariables[4] << std::endl;
   //std::cout << "fDependentVariables[5] = " << fDependentVariables[5] << std::endl;
   //k1.Print();
   //k2.Print();
   //p2.Print();
   if (p2.Phi() < 0.0) fDependentVariables[5] = p2.Phi() + 2.0 * TMath::Pi() ;
   return(fDependentVariables);
}
//______________________________________________________________________________

Double_t DeuteronElasticDiffXSec::GetEPrime(const Double_t theta)const  {
   // Returns the scattered electron energy using the angle. */
   Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
   return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M * TMath::Power(TMath::Sin(theta / 2.0), 2)));
}
//________________________________________________________________________

Double_t  DeuteronElasticDiffXSec::EvaluateXSec(const Double_t * x) const {
   // Get the recoiling proton momentum
   if (!VariablesInPhaseSpace(6, x)) return(0.0);

   double M = fTargetNucleus.GetMass();

   //if(x[3] < M ) return 0.0;

   Double_t Eprime   = x[0];
   Double_t theta    = x[1] ;
   Double_t Qsquared = 4.0 * Eprime * GetBeamEnergy() * TMath::Power(TMath::Sin(theta / 2.0), 2);
   //Double_t tau = Qsquared / (4.0 * M * M);
   Double_t mottXSec = (1.0/137.0)*(1.0/137.0)*TMath::Power(TMath::Cos(theta/2.0),2)/
      (4.0*GetBeamEnergy()*GetBeamEnergy()*TMath::Power(TMath::Sin(theta/2.0),4))*(Eprime/GetBeamEnergy());

   //Double_t GC2 = TMath::Power(fFormFactors->FCd(Qsquared), 2);
   //Double_t GM2 = TMath::Power(fFormFactors->FMd(Qsquared), 2);
   //Double_t GQ2 = TMath::Power(fFormFactors->FQd(Qsquared), 2);

   Double_t A  = fFormFactors->Ad(Qsquared);
   Double_t B  = fFormFactors->Bd(Qsquared);
   Double_t FF = A + B*TMath::Power(TMath::Tan(theta/2.0),2.0);
   //std::cout << " Qsq= " << Qsquared;
   //std::cout << " tau= " << tau;
   //std::cout << " GE2= " << GE2;
   //std::cout << " GM2= " << GM2  << "\n";;

   Double_t res = mottXSec*FF;// ((GE2 + tau * GM2) / (1.0 + tau) + 2.0 * tau * GM2 * TMath::Power(TMath::Tan(theta / 2.0), 2));
   res = res * hbarc2_gev_nb;

   if( IncludeJacobian() ) return( TMath::Sin(theta)*res);
   return(res);
}
//______________________________________________________________________________


}
}
