#include "insane/xsections/InelasticRadiativeTail.h"

namespace insane {
namespace physics {

InelasticRadiativeTail::InelasticRadiativeTail()
{
   fID = 100010012;
   SetTitle("InelasticRadiativeTail");//,"POLRAD Born cross-section");
   SetPlotTitle("Inelastic Radiative Tail cross-section");

   fLabel = "#frac{d#sigma}{dEd#Omega}";
   fUnits = "nb/GeV/sr";
   fAddRegion4 = false;
   fInternalOnly = false;

   fRadLen[0] = 0.05; 
   fRadLen[1] = 0.05; 

   //fDiffXSec0 = new POLRADInelasticTailDiffXSec();
   fDiffXSec0 = new  POLRADBornDiffXSec();
   fDiffXSec0->SetTargetNucleus(Nucleus::Proton());
   fDiffXSec0->InitializePhaseSpaceVariables();
   fDiffXSec0->InitializeFinalStateParticles();

   GetRADCOR()->SetUnpolarizedCrossSection(fDiffXSec0); 
   GetRADCOR()->SetRadiationLengths(fRadLen);

}
//________________________________________________________________________________
InelasticRadiativeTail::~InelasticRadiativeTail(){


}
//________________________________________________________________________________

//________________________________________________________________________________
FullInelasticRadiativeTail::FullInelasticRadiativeTail(){
   
   SetTitle("FullInelasticRadiativeTail");//,"POLRAD Born cross-section");
   SetPlotTitle("Full Inelastic Radiative Tail cross-section");

   fLabel = "#frac{d#sigma}{dEd#Omega}";
   fUnits = "nb/GeV/sr";

   fRadLen[0] = 0.025; 
   fRadLen[1] = 0.025; 

   fDiffXSec = new InelasticRadiativeTail();
   fDiffXSec->SetTargetNucleus(Nucleus::Proton());
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();

   fDiffXSec0 = new  POLRADBornDiffXSec();
   fDiffXSec0->SetTargetNucleus(Nucleus::Proton());
   fDiffXSec0->InitializePhaseSpaceVariables();
   fDiffXSec0->InitializeFinalStateParticles();

   fRADCOR->SetUnpolarizedCrossSection(fDiffXSec0); 

   fRADCOR->SetRadiationLengths(fRadLen);


}
//________________________________________________________________________________
FullInelasticRadiativeTail::~FullInelasticRadiativeTail(){


}
//________________________________________________________________________________



}}
