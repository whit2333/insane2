C ----------------------------------------------------------------------
C                           total_07.f
C                 5 total cross sections
C    *****        Program Source: 2007  (18.05.07)         *****
C ----------------------------------------------------------------------
c
      SUBROUTINE RESPINC(WCM,Q2,QPI,EGEQ,EGCM,
     & A1,A2,A3,A4,A5,A6,H1,H2,H3,H4,H5,H6,
     & ST,SL,SLT,SLTP,STTP,SL0,SLT0,SLTP0)
      IMPLICIT NONE
      REAL*8 WCM,Q2,QPI,EGEQ,EGCM,FACT,SQ2,FLong
      REAL*8 RT00,RL00,RLT0x,RLT0y
      REAL*8 RLTP0x,RLTP0y,RTTP0z
      REAL*8 ST,SL,SLT,SLTP,STTP,SL0,SLT0,SLTP0
      COMPLEX*16 A1,A2,A3,A4,A5,A6,H1,H2,H3,H4,H5,H6
      COMPLEX*16 CA1,CA2,CA3,CA4,CA5,CA6
c
c  ********  5 total cross sections (in mcb) *************
c   RT     =  RT00
c   RL     =  RL00
c   RLT    = (RLT0x  + RLT0y)/2
c   RLTP   = (RLTP0x - RLTP0y)/2
c   RTTP   =  RTTP0z
c

      FACT=10000.*QPI/EGEQ
      FLong=SQRT(Q2)/EGCM
      SQ2=DSQRT(2.D0)
c
      CA1=DCONJG(A1)
      CA2=DCONJG(A2)
      CA3=DCONJG(A3)
      CA4=DCONJG(A4)
      CA5=DCONJG(A5)
      CA6=DCONJG(A6)

c  *****  evaluated with spin functions A1,...,A6


      RT00 = DREAL(CA1*A1+CA2*A2+CA3*A3+CA4*A4)/2.D0
      ST = FACT*RT00

      RL00 = DREAL( CA5*A5+CA6*A6)
      SL0 = FACT*RL00
      SL = FACT*FLong**2*RL00

      RLT0x= DIMAG(-CA5*A2+CA5*A3-CA6*A1-CA6*A4)/SQ2
      RLT0y= DIMAG(-CA5*A2-CA5*A3+CA6*A1-CA6*A4)/SQ2
      SLT0 = FACT*(RLT0x + RLT0y)/2.D0
      SLT = FACT*FLong*(RLT0x + RLT0y)/2.D0

      RLTP0x= DREAL( CA5*A2-CA5*A3+CA6*A1+CA6*A4)/SQ2
      RLTP0y= DREAL(-CA5*A2-CA5*A3+CA6*A1-CA6*A4)/SQ2
      SLTP0   = FACT*(RLTP0x - RLTP0y)/2.D0
      SLTP   = FACT*FLong*(RLTP0x - RLTP0y)/2.D0
c
      RTTP0z= DREAL(CA1*A1-CA2*A2+CA3*A3-CA4*A4)/2.D0
      STTP = FACT*RTTP0z
c
      RETURN
      END
