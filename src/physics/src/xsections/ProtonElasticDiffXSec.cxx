#include "insane/xsections/ProtonElasticDiffXSec.h"
#include "insane/base/PhysicalConstants.h"
#include "insane/xsections/RadiativeEffects.h"

namespace insane {
   namespace physics {

ProtonElasticDiffXSec::ProtonElasticDiffXSec()
{
   fID            = 200000040;
   SetTitle("ProtonElasticDiffXSec");
   SetPlotTitle("Elastic cross-section");
   fLabel         = "#frac{d#sigma}{d#Omega}";
   fUnits         = "nb/sr";
   fnDim          = 2;
   fPIDs.clear();
   fnParticles    = 2;
   fPIDs.push_back(11);
   fPIDs.push_back(2212);
}
//______________________________________________________________________________

ProtonElasticDiffXSec::~ProtonElasticDiffXSec(){
}
//______________________________________________________________________________

void ProtonElasticDiffXSec::InitializePhaseSpaceVariables()
{

   PhaseSpace * ps = GetPhaseSpace();

   // ------------------------------
   // Electron
   auto * varEnergy = new PhaseSpaceVariable(
         "energy_e", "E_{e'}", 0.00050, GetBeamEnergy());
   varEnergy->SetDependent(true);
   ps->AddVariable(varEnergy);

   auto *   varTheta = new PhaseSpaceVariable(
         "theta_e", "#theta_{e'}", 0.00010*degree, 60.0*degree);
   varTheta->SetDependent(true);
   ps->AddVariable(varTheta);

   auto *   varPhi = new PhaseSpaceVariable(
         "phi_e", "#phi_{e'}", -180.0*degree, 180.0*degree);
   varPhi->SetDependent(true);
   ps->AddVariable(varPhi);


   // ------------------------------
   // p
   auto * varP_p = new PhaseSpaceVariable(
         "P_p", "P_{p}",0.0,5.0);
   varP_p->SetParticleIndex(1);
   varP_p->SetDependent(true);
   ps->AddVariable(varP_p);

   auto *   varTheta_p = new PhaseSpaceVariable( 
         "theta_p", "#theta_{p}", 30*degree, 180.0*degree);
   varTheta_p->SetParticleIndex(1);
   //varTheta_p->SetDependent(true);
   ps->AddVariable(varTheta_p);

   auto *   varPhi_p = new PhaseSpaceVariable(
         "phi_p", "#phi_{p}", -180.0*degree, 180.0*degree);
   varPhi_p->SetParticleIndex(1);
   varPhi_p->SetUniform(true);
   ps->AddVariable(varPhi_p);

   SetPhaseSpace(ps);
}
//______________________________________________________________________________

void ProtonElasticDiffXSec::DefineEvent(Double_t * vars)
{
   // Virtual method to define the random event from the variables provided.
   // This is the transition point between the phase space variables and
   // the particles. The argument should be the full list of variables returned from
   // GetDependentVariables. 

   Int_t totvars = 0;

   insane::Kine::SetMomFromEThetaPhi((TParticle*)(fParticles.At(0)), &vars[totvars]);
   totvars += GetNParticleVars(0);

   insane::Kine::SetEFromMomThetaPhi((TParticle*)(fParticles.At(1)), &vars[totvars]);
   totvars += GetNParticleVars(1);

}
//______________________________________________________________________________

Double_t * ProtonElasticDiffXSec::GetDependentVariables(const Double_t * x) const
{
   using namespace TMath;
   using namespace insane::units;

   //double M            = fTargetNucleus.GetMass();
   double M            = M_p/GeV;
   double E0           = fBeamEnergy;
   double theta_alpha  = x[0];
   double phi          = x[1];
   double P_alpha      = GetPalpha(E0,theta_alpha);
   double E_alpha      = Sqrt(P_alpha*P_alpha+M*M);

   double Es     = E0;//M*P_alpha/((M+E_alpha)*Cos(theta_alpha) - P_alpha);
   double theta  = pi-GetTheta_e(Es,theta_alpha);
   double Eprime = Es/(1.0 + 2.0*Es/M*TMath::Power(TMath::Sin(theta/2.0), 2));
   if( (Eprime > E0) || (Eprime <0) ) {
      return nullptr;
   }

   //std::cout << "Pa = " <<  P_alpha << std::endl;
   //std::cout << theta/degree << std::endl;
   if(std::isnan(theta) ){
      return nullptr;
   }

   fDependentVariables[0] = Eprime;
   fDependentVariables[1] = theta;
   fDependentVariables[2] = phi+pi;
   fDependentVariables[3] = P_alpha;
   fDependentVariables[4] = theta_alpha;
   fDependentVariables[5] = phi;
   return(fDependentVariables);
}
//______________________________________________________________________________

Double_t ProtonElasticDiffXSec::GetEPrime(const Double_t theta)const  {
   // Returns the scattered electron energy using the angle. */
   //Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
   double M      = M_p/GeV;
   return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M * TMath::Power(TMath::Sin(theta / 2.0), 2)));
}
//________________________________________________________________________

Double_t ProtonElasticDiffXSec::GetTheta(double ep) const {
   using namespace TMath;
   //double M     = fTargetNucleus.GetMass();
   double M      = M_p/GeV;
   double Eb    = GetBeamEnergy();
   double theta = 2.0*ASin(Sqrt((Eb-ep)*M/(2.0*Eb*ep)));
   return theta;
}
//______________________________________________________________________________
 
double ProtonElasticDiffXSec::GetTheta_alpha(double Es, double P_alpha) const
{
   using namespace TMath;
   //double M   = fTargetNucleus.GetMass();
   double M      = M_p/GeV;
   double Pa  = P_alpha;
   double cotangent  = ((Es + M)*(M - Sqrt(Power(M,2) + Power(Pa,2))))/
      Sqrt(M*(-2*Power(Es,2)*M - 4*Es*Power(M,2) - 2*Power(M,3) - 2*Es*Power(Pa,2) - M*Power(Pa,2) + 
               2*Es*(Es + M)*Sqrt(Power(M,2) + Power(Pa,2)) + 2*M*(Es + M)*Sqrt(Power(M,2) + Power(Pa,2))));
   double res = ATan(Abs(1.0/cotangent));
   return res;
}
//______________________________________________________________________________

double ProtonElasticDiffXSec::GetPalpha(double Es, double alpha) const
{
   using namespace TMath;
   //double M   = fTargetNucleus.GetMass();
   double M      = M_p/GeV;
   double res = 4.0*Es*M*(Es+M)*Cos(alpha)/(Es*Es+4.0*Es*M+2.0*M*M - Es*Es*Cos(2.0*alpha));
   return res;
}
//______________________________________________________________________________

double ProtonElasticDiffXSec::GetTheta_e(double Es, double alpha) const
{
   using namespace TMath;
   //double M   = fTargetNucleus.GetMass();
   double M      = M_p/GeV;
   double res = 2.0*ATan((Es+M)*Sin(alpha)/(M*Cos(alpha)));
   return res;
}
//______________________________________________________________________________

double  ProtonElasticDiffXSec::ElectronToAlphaJacobian(double Es, double alpha) const
{
   using namespace TMath;
   double M      = M_p/GeV;
   //double M             = fTargetNucleus.GetMass();
   double theta         = GetTheta_e(Es,alpha);
   double dtheta_dalpha = 2.0*M*(Es+M)/(Power(M*Cos(alpha),2.0) + Power((Es+M)*Sin(alpha),2.0));
   double res           = dtheta_dalpha*Sin(theta);
   return res;
}
//______________________________________________________________________________

double ProtonElasticDiffXSec::Ib(Double_t E0,Double_t E,Double_t t) const {
   // Bremsstrahlung loss 
   // Tsai, SLAC-PUB-0848 (1971), Eq B.43
   // Same as B.43 but we explicitly write out W_b(E,eps) 
   // so that X0 cancels from the formula
   Double_t b     = 4.0/3.0;//fKinematics.Getb(); 
   Double_t gamma = TMath::Gamma(1. + b*t);
   // Double_t Wb    = W_b(E0,E0-E);
   // Wb is (X0 cancels out when combined with everything in this method): 
   // Double_t b     = fKinematicsExt.Getb(); 
   // Double_t X0    = fX0Eff;
   // Double_t Wb    = (1./X0)*(b/eps)*GetPhi(eps/E0);
   Double_t v     = (E0-E)/E0;
   Double_t phi   = (1.0 - v + (3.0/4.0)*v*v); 
   Double_t T1    = (b*t/gamma)*( 1./(E0-E) ); 
   Double_t T2    = TMath::Power(v,b*t);
   Double_t T3    = phi;
   Double_t ib    = T1*T2*T3;
   return ib; 
}
//______________________________________________________________________________

Double_t  ProtonElasticDiffXSec::EvaluateXSec(const Double_t * x) const {
   using namespace TMath;
   using namespace insane::units;
   // Get the recoiling proton momentum
   if(!x) return(0.0);
   if (!VariablesInPhaseSpace(6, x)) return(0.0);

   double M      = M_p/GeV;
   double E0     = GetBeamEnergy();
   double Es     = E0;
   double T_mat  = fTargetMaterial.GetNumberOfRadiationLengths();
   double Z      = fTargetNucleus.GetZ();

   double Eprime      = x[0];
   double Ep          = x[0];
   double theta       = x[1] ;
   double theta_alpha = x[4] ;

   double Qsquared = 4.0*Ep*Es*Power(Sin(theta/2.0),2);
   double tau      = Qsquared/(4.0*M*M);

   double T_X0     = tr_Tsai(Qsquared);
   double T        = T_X0 + T_mat;
   double bT       = T*4.0/3.0;

   // I is the probability of the incident electron losing 
   // energy to be come Ep. This dramatically reduces the cross section
   // far away from the peak.
   double I   = Ib(E0,Es,T/2.0);

   //if(x[3] < M ) return 0.0;

   //Double_t tau = Qsquared / (4.0 * M * M);
   double mottXSec = insane::Kine::Sig_Mott(Es,theta);
   double recoil   = insane::Kine::fRecoil(Es,theta,M);

   double GE2     = Power(fFormFactors->GEp(Qsquared), 2);
   double GM2     = Power(fFormFactors->GMp(Qsquared), 2);
   double sig_el  = (mottXSec/recoil)*((GE2+tau*GM2)/(1.0+tau)+2.0*tau*GM2*Power(Tan(theta/2.0),2));

   double jac = ElectronToAlphaJacobian(Es,theta_alpha);

   double F_RC  = insane::physics::F_Tsai(fBeamEnergy,Eprime,theta);  
   double RC    = insane::physics::RCToPeak_Tsai(fBeamEnergy,Eprime,theta);  
   //std::cout << " F_RC= " << F_RC;
   //std::cout << " RC  = " << RC    << "\n";;
   double res   = Z*RC*F_RC*sig_el*hbarc2_gev_nb*jac;
   //double res   = sig_el*hbarc2_gev_nb*jac;

   if( IncludeJacobian() ) return( TMath::Sin(theta_alpha)*res);
   return(res);
}
//______________________________________________________________________________

//______________________________________________________________________________


}
}
