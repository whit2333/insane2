#include "insane/xsections/MollerDiffXSec.h"
#include "insane/base/PhysicalConstants.h"

namespace insane {
  namespace physics {

    MollerDiffXSec::MollerDiffXSec()
    {
      fID            = 200001001;
      SetTitle("MollerDiffXSec");
      SetPlotTitle("Moller cross-section");
      fLabel         = "#frac{d#sigma}{d#Omega}";
      fUnits         = "nb/sr";
      fnDim          = 2;
      fPIDs.clear();
      fnParticles    = 2;
      fPIDs.push_back(11);
      fPIDs.push_back(11);
    }
    //______________________________________________________________________________

    MollerDiffXSec::~MollerDiffXSec()
    { }
    //______________________________________________________________________________

    void MollerDiffXSec::InitializePhaseSpaceVariables()
    {
      PhaseSpace* ps = GetPhaseSpace();

      // ------------------------------
      // Electron
      auto * varEnergy = new PhaseSpaceVariable("energy_e", "E_{e'}",0.50,GetBeamEnergy());
      varEnergy->SetDependent(true);
      ps->AddVariable(varEnergy);

      auto *   varTheta = new PhaseSpaceVariable("theta_e", "#theta_{e'}",0.0,180.0*degree);
      ps->AddVariable(varTheta);

      auto *   varPhi = new PhaseSpaceVariable("phi_e", "#phi_{e'}",-180.0*degree, 180.0*degree );
      varPhi->SetUniform(true);
      ps->AddVariable(varPhi);


      // ------------------------------
      // electron 2
      auto * varEnergy_e2 = new PhaseSpaceVariable("energy_e2", "E_{e2}",1.0e-6,12.0);
      varEnergy_e2->SetParticleIndex(1);
      varEnergy_e2->SetDependent(true);
      ps->AddVariable(varEnergy_e2);

      auto *   varTheta_e2 = new PhaseSpaceVariable("theta_e2", "#theta_{e2}",0.0*degree,180*degree);
      varTheta_e2->SetParticleIndex(1);
      varTheta_e2->SetDependent(true);
      ps->AddVariable(varTheta_e2);

      auto *   varPhi_e2 = new PhaseSpaceVariable("phi_e2", "#phi_{e2}",-1.0*TMath::Pi(), 1.0*TMath::Pi() );
      varPhi_e2->SetParticleIndex(1);
      varPhi_e2->SetDependent(true);
      ps->AddVariable(varPhi_e2);

      SetPhaseSpace(ps);
  }
  //______________________________________________________________________________

  Double_t * MollerDiffXSec::GetDependentVariables(const Double_t * x) const
  {
    // Solve for energy of scattered electron at given angle theta (and phi). 
    // Then, solve for the second electron.
    using namespace TMath;
    //std::cout << "x[0] = " << x[0] << std::endl;
    //std::cout << "x[1] = " << x[1] << std::endl;
    //std::cout << "x[2] = " << x[2] << std::endl;
    //std::cout << "x[3] = " << x[3] << std::endl;
    //std::cout << "x[4] = " << x[4] << std::endl;
    //std::cout << "x[5] = " << x[5] << std::endl;
    double  th     = x[0];
    double  phi    = x[1];
    double  E0     = fBeamEnergy;
    double  me     = insane::units::_e/GeV;
    double  costh  = Cos(th);
    // 
    double s         = 2.0*me*(me+fBeamEnergy);
    double gamcm     = (fBeamEnergy+me)/Sqrt(s);
    double th_cm     = 2.0*ATan( Tan(th)*gamcm );

    double  E1_prime = me+(E0-me)*Cos(th_cm/2.0)*Cos(th_cm/2.0);
    //double  E1_prime = me*(E0+me+(E0-me)*costh*costh)/(E0+me-(E0-me)*costh*costh);

    double  pprime   = Sqrt(E1_prime*E1_prime - me*me);//(2*costh*Sqrt(E0 - me)*me*Sqrt(E0 + me))/(E0 - costh*costh*E0 + me + costh*costh*me);
    double  eprime   = E1_prime;//Sqrt(pprime*pprime + me*me);
    TVector3 k1(0, 0, Sqrt(E0*E0 - me*me) ); // incident electron
    TVector3 p1(1, 0, 0);
    p1.SetMagThetaPhi(pprime, x[0], x[1]);
    TVector3 p2 = k1 - p1 ;

    fDependentVariables[0] = eprime;
    fDependentVariables[1] = x[0];
    fDependentVariables[2] = x[1];
    fDependentVariables[3] = Sqrt(p2.Mag2()+me*me);
    fDependentVariables[4] = p2.Theta();
    fDependentVariables[5] = p2.Phi();
    //std::cout << "vectors" << std::endl;
    //std::cout << "fDependentVariables[0] = " << fDependentVariables[0] << std::endl;
    //std::cout << "fDependentVariables[1] = " << fDependentVariables[1] << std::endl;
    //std::cout << "fDependentVariables[2] = " << fDependentVariables[2] << std::endl;
    //std::cout << "fDependentVariables[3] = " << fDependentVariables[3] << std::endl;
    //std::cout << "fDependentVariables[4] = " << fDependentVariables[4] << std::endl;
    //std::cout << "fDependentVariables[5] = " << fDependentVariables[5] << std::endl;
    //k1.Print();
    //k2.Print();
    //p2.Print();
    //if (p2.Phi() < 0.0) fDependentVariables[5] = p2.Phi() + 2.0 * TMath::Pi() ;
    return(fDependentVariables);
  }
  //________________________________________________________________________

  double  MollerDiffXSec::MollerCrossSection_CM(double E0, double th) const
  {
    // Moller cross section in the center of mass 
    using namespace TMath;
    double costh = Cos(th);
    double sinth = Sin(th);
    double me    = M_e/GeV;
    //double s2    = Sin(th/2.0)*Sin(th/2.0);
    //double c2    = Cos(th/2.0)*Cos(th/2.0);
    //double A     = (1.0/137.0)*(1.0/137.0)/(4.0*E0*E0);
    //double a0    = Power( (2*E0*E0 - me*me)/(E0*E0 - me*me), 2.0);
    //double a1    = 4.0/(s2*s2);
    //double a2    = -3.0/s2;
    //double a3    = (1.0 + 4.0/(s2*s2))/a0;
    //return hbarc2_gev_nb*A*a0*(a1 + a2 + a3);
    //double b0    = 1.0/2.0;
    //double b1    = (1.0+c2*c2)/(s2*s2);
    //double b2    = 2.0/(s2*c2);
    //double b3    = (1.0+s2*s2)/(c2*c2);
    //return hbarc2_gev_nb*A*b0*(b1 + b2 + b3);
    //double h0    = (3.0+costh*costh)*(3.0+costh*costh)/(sinth*sinth*sinth*sinth); // high energy limit.
    //return hbarc2_gev_nb*A*h0;

    double gamma     = fBeamEnergy/me;
    double gammabar  = Sqrt((gamma+1.0)/2.0);
    double t0 = Power((1.0/137.0)/(2.0*me*gammabar*(gammabar*gammabar-1.0)*sinth*sinth),2.0);
    double t1 = (2.0*gammabar*gammabar-1.0)*(2.0*gammabar*gammabar-1.0)*(4.0-3.0*sinth*sinth)
      + (gammabar*gammabar-1.0)*(gammabar*gammabar-1.0)*(4.0+sinth)*sinth;
    double sig2 = hbarc2_gev_nb*t0*t1;
    return sig2;
  }
  //______________________________________________________________________________

  Double_t  MollerDiffXSec::EvaluateXSec(const Double_t * x) const
  {
    // Get the recoiling proton momentum
    if (!VariablesInPhaseSpace(6, x)) {
      //std::cout << " not in PS \n";
      return(0.0);
    }
    using namespace TMath;
    using namespace insane::units;

    double E0     = fBeamEnergy;
    double Eprime = x[0];
    double sinth  = Sin(x[1]) ;
    double theta  = x[1] ;
    double me     = M_e/GeV;

    double s         = 2.0*me*(me+fBeamEnergy);
    double gamcm     = (fBeamEnergy+me)/Sqrt(s);
    //double th_cm     = 2.0*ATan( Tan(theta)*Sqrt((fBeamEnergy+me)/(2.0*me)) );
    double th_cm     = 2.0*ATan( Tan(theta)*gamcm );
    double Eprime_cm = Sqrt(s/2.0);
    //std::cout << "theta_lab = " << theta/degree << ", cm = " << th_cm/degree << std::endl;

    //double sinthcm = Sin(th_cm);
    //double t0 = Power((1.0/137.0)/(2.0*me*gammabar*(gammabar*gammabar-1.0)*sinthcm*sinthcm),2.0);
    //double a0 = (2.0*gammabar*gammabar-1.0)*(2.0*gammabar*gammabar-1.0)*(4.0-3.0*sinthcm*sinthcm)
    //            + (gammabar*gammabar-1.0)*(gammabar*gammabar-1.0)*(4.0+sinthcm)*sinthcm;
    //double sig2 = hbarc2_gev_nb*t0*a0;//MollerCrossSection_CM( Eprime_cm, th_cm );
    double sig = MollerCrossSection_CM( Eprime_cm, th_cm );
    //std::cout << sig - sig2 << std::endl;

    //double dOmega_cm_over_lab = 8.0*(fBeamEnergy+me)*Cos(theta)/Power(2.0*me+(fBeamEnergy-me)*sinth*sinth,2.0);

    double eOmega_cm_over_lab2= (Sin(th_cm)/Sin(theta))*2.0*gamcm/((1.0+gamcm*gamcm*Tan(theta)*Tan(theta))*Cos(theta)*Cos(theta));
    sig *= dOmega_cm_over_lab2;


    if(sig<0) {
      //std::cout << sig << " < 0 !!! \n";
      return 0.0;
    }
    double Z   = fTargetNucleus.GetZ();

    double res = sig*Z;
    if( IncludeJacobian() ) return( sinth*res);
    return(res);
  }
  //______________________________________________________________________________



}
}
