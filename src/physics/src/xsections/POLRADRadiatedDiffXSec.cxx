#include "insane/xsections/POLRADRadiatedDiffXSec.h"
namespace insane {
namespace physics {

POLRADRadiatedDiffXSec::POLRADRadiatedDiffXSec()
{
   SetTitle("POLRADRadiatedXSec");//,"POLRAD Radiated cross-section");
   SetPlotTitle("POLRAD Radiated XSec");
}
//______________________________________________________________________________

POLRADRadiatedDiffXSec::~POLRADRadiatedDiffXSec()
{ }
//______________________________________________________________________________

POLRADRadiatedDiffXSec*  POLRADRadiatedDiffXSec::Clone(
      const char * newname) const
{
   std::cout << "POLRADRadiatedDiffXSec::Clone()\n";
   auto * copy = new POLRADRadiatedDiffXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

Double_t POLRADRadiatedDiffXSec::EvaluateXSec(const Double_t *x) const
{
   if (!VariablesInPhaseSpace(fnDim, x)){
      std::cout << "[POLRADRadiatedDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
      return(0.0);
   }

   Double_t Eprime  = x[0];
   Double_t theta   = x[1];
   Double_t Ebeam   = GetBeamEnergy();
   Double_t nu      = Ebeam-Eprime;
   Double_t Mtarg   = 0.938;  /// \todo fix hard coding...
   //Double_t S       = 2.0*Ebeam*Mtarg;
   //Double_t X       = 2.0*Eprime*Mtarg;
   //Double_t Q2      = 4.0*Eprime*Ebeam*TMath::Sin(theta/2.0)*TMath::Sin(theta/2.0);

   // 1: Set the kinematics
   fPOLRADCalc->SetKinematics(Ebeam,Eprime,theta);

   // 2: Get the born cross section
   //Double_t bornXSec = fPOLRADCalc->sig_dis_born(S,X,Q2); 

   // 3: Get elastic radiative tail
   Double_t ert    = fPOLRADCalc->ERT();

   // 4: Get the quasielastic ratiative tail
   Double_t qrt = 0.0;
   if(fPOLRADCalc->fPOLRADTarget >= 2){
      // for deuterons, 3He, etc...
      qrt      = fPOLRADCalc->QRT();
   }

   // 5: Get the inelastic radiative tail
   Double_t irt = fPOLRADCalc->IRT();

   // 6: Put them all together
   Double_t sig_rad  = irt + (ert + qrt)/(fPOLRADCalc->fA);     

   // converts from dsigma/dxdy to dsigma/dEdOmega
   sig_rad = sig_rad * (Eprime/(2.0*TMath::Pi()*Mtarg*nu));
   if(IncludeJacobian()){
      sig_rad *= TMath::Sin(theta);
   }
   return sig_rad*hbarc2_gev_nb;
} 
//______________________________________________________________________________

}}
