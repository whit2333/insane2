#include "insane/xsections/POLRADInelasticTailDiffXSec.h"
namespace insane {
namespace physics {

POLRADInelasticTailDiffXSec::POLRADInelasticTailDiffXSec()
{
   fID         = 100010003;
   SetTitle("POLRADInelasticTailDiffXSec");
   SetPlotTitle("POLRAD Inelastic tail XSec");
   fPOLRADCalc->SetMultiPhoton(true); 
}
//______________________________________________________________________________

POLRADInelasticTailDiffXSec::~POLRADInelasticTailDiffXSec()
{ }
//______________________________________________________________________________

Double_t POLRADInelasticTailDiffXSec::EvaluateXSec(const Double_t *x) const 
{
   if (!VariablesInPhaseSpace(fnDim, x)){
      std::cout << "[POLRADInelasticTailDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
      return(0.0);
   }
   Double_t Eprime  = x[0];
   Double_t theta   = x[1];
   Double_t Ebeam   = GetBeamEnergy();
   Double_t nu      = Ebeam-Eprime;
   Double_t Mtarg   = M_p/GeV;// 0.938
   //Double_t S       = 2.0*Ebeam*Mtarg;
   //Double_t X       = 2.0*Eprime*Mtarg;
   //Double_t Q2      = 4.0*Eprime*Ebeam*TMath::Sin(theta/2.0)*TMath::Sin(theta/2.0);

   fPOLRADCalc->SetKinematics(Ebeam,Eprime,theta);

   if( fPOLRADCalc->GetKinematics()->GetW2() < Mtarg*Mtarg) return(0.0);
   Double_t sig_rad  = fPOLRADCalc->IRT();
   sig_rad           = sig_rad*(Eprime/(2.0*TMath::Pi()*Mtarg*nu))*hbarc2_gev_nb;
   if( IncludeJacobian() ) sig_rad = sig_rad*TMath::Sin(theta);
   //std::cout << "sig_irt = " << sig_rad << std::endl;
   return sig_rad;
} 
//______________________________________________________________________________
}}
