#include "insane/xsections/ProtonElasticRadiativeTailDiffXSec.h"
#include "insane/base/PhysicalConstants.h"

namespace insane {
   namespace physics {

ProtonElasticRadiativeTailDiffXSec::ProtonElasticRadiativeTailDiffXSec()
{
   fID            = 200000040;
   SetTitle("ProtonElasticRadiativeTailDiffXSec");
   SetPlotTitle("Elastic cross-section");
   fLabel         = "#frac{d#sigma}{d#Omega}";
   fUnits         = "nb/sr";
   fnDim          = 3;
   fPIDs.clear();
   fnParticles    = 2;
   fPIDs.push_back(11);
   fPIDs.push_back(1000020040);
   SetElasticOnly(true);
}
//______________________________________________________________________________

ProtonElasticRadiativeTailDiffXSec::~ProtonElasticRadiativeTailDiffXSec(){
}
//______________________________________________________________________________

void ProtonElasticRadiativeTailDiffXSec::InitializePhaseSpaceVariables()
{

   PhaseSpace * ps = GetPhaseSpace();

   // ------------------------------
   // Electron
   auto * varEnergy = new PhaseSpaceVariable(
         "energy_e", "E_{e'}", 0.001, GetBeamEnergy());
   varEnergy->SetDependent(true); 
   ps->AddVariable(varEnergy);
   // Note the energy is now independent for radiative tail

   auto *   varTheta = new PhaseSpaceVariable(
         "theta_e", "#theta_{e'}", 0.01*degree, 180.0*degree);
   varTheta->SetDependent(true);
   ps->AddVariable(varTheta);

   auto *   varPhi = new PhaseSpaceVariable(
         "phi_e", "#phi_{e'}", -180.0*degree, 180.0*degree);
   varPhi->SetDependent(true);
   //varPhi->SetUniform(true);
   ps->AddVariable(varPhi);


   // ------------------------------
   // He4
   auto * varP_p = new PhaseSpaceVariable(
         "P_He4", "P_{He4}",0.001,5.0);
   varP_p->SetParticleIndex(1);
   ps->AddVariable(varP_p);

   auto *   varTheta_p = new PhaseSpaceVariable( 
         "theta_He4", "#theta_{He4}", 1*degree, 180.0*degree);
   varTheta_p->SetParticleIndex(1);
   //varTheta_p->SetDependent(true);
   ps->AddVariable(varTheta_p);

   auto *   varPhi_p = new PhaseSpaceVariable(
         "phi_He4", "#phi_{He4}", -180.0*degree, 180.0*degree);
   varPhi_p->SetParticleIndex(1);
   varPhi_p->SetUniform(true);
   ps->AddVariable(varPhi_p);

   SetPhaseSpace(ps);
}
////______________________________________________________________________________
//
//void ProtonElasticRadiativeTailDiffXSec::DefineEvent(Double_t * vars)
//{
//   // Virtual method to define the random event from the variables provided.
//   // This is the transition point between the phase space variables and
//   // the particles. The argument should be the full list of variables returned from
//   // GetDependentVariables. 
//
//   Int_t totvars = 0;
//
//   ::Kine::SetMomFromEThetaPhi((TParticle*)(fParticles.At(0)), &vars[totvars]);
//   totvars += GetNParticleVars(0);
//
//   ::Kine::SetEFromMomThetaPhi((TParticle*)(fParticles.At(1)), &vars[totvars]);
//   totvars += GetNParticleVars(1);
//
//}
////______________________________________________________________________________
//
Double_t * ProtonElasticRadiativeTailDiffXSec::GetDependentVariables(const Double_t * x) const
{
   // The elastic radiative tail is a function of E', theta, and phi
   //
   using namespace TMath;
   double M = fTargetNucleus.GetMass();//GetParticlePDG(1)->Mass();
   //TVector3 p2(0, 0, 0);          // scattered electron
   double P_alpha      = x[0];//GetEPrime(x[0]);
   double theta_alpha  = x[1];
   double phi          = x[2];
   double E_alpha      = Sqrt(P_alpha*P_alpha+M*M);

   double Es     = M*P_alpha/((M+E_alpha)*Cos(theta_alpha) - P_alpha);
   double theta  = GetTheta_e(Es,theta_alpha);
   double Eprime = Es/(1.0 + 2.0*Es/M*TMath::Power(TMath::Sin(theta/2.0), 2));


   if(Es<0.0 ){
      return nullptr;
   }
   if(std::isnan(theta) ){
      return nullptr;
   }
   fDependentVariables[0] = Eprime;
   fDependentVariables[1] = theta;
   fDependentVariables[2] = phi+pi;
   fDependentVariables[3] = P_alpha;
   fDependentVariables[4] = theta_alpha;
   fDependentVariables[5] = phi;
   return(fDependentVariables);
}
//______________________________________________________________________________

double ProtonElasticRadiativeTailDiffXSec::GetPalpha(double Es, double alpha) const
{
   using namespace TMath;
   double M   = fTargetNucleus.GetMass();
   double res = 4.0*Es*M*(Es+M)*Cos(alpha)/(Es*Es+4.0*Es*M+2.0*M*M - Es*Es*Cos(2.0*alpha));
   return res;
}
//______________________________________________________________________________

double ProtonElasticRadiativeTailDiffXSec::GetTheta_e(double Es, double alpha) const
{
   using namespace TMath;
   double M   = fTargetNucleus.GetMass();
   double res = 2.0*ATan((Es+M)*Sin(alpha)/(M*Cos(alpha)));
   return res;
}
//______________________________________________________________________________

double  ProtonElasticRadiativeTailDiffXSec::ElectronToAlphaJacobian(double Es, double alpha) const
{
   using namespace TMath;
   double M             = fTargetNucleus.GetMass();
   double theta         = GetTheta_e(Es,alpha);
   double dtheta_dalpha = 2.0*M*(Es+M)/(Power(M*Cos(alpha),2.0) + Power((Es+M)*Sin(alpha),2.0));
   double res           = dtheta_dalpha*Sin(theta);
   return res;
}

double ProtonElasticRadiativeTailDiffXSec::Ib(Double_t E0,Double_t E,Double_t t) const {
   // Bremsstrahlung loss 
   // Tsai, SLAC-PUB-0848 (1971), Eq B.43
   // Same as B.43 but we explicitly write out W_b(E,eps) 
   // so that X0 cancels from the formula
   Double_t b     = 4.0/3.0;//fKinematics.Getb(); 
   Double_t gamma = TMath::Gamma(1. + b*t);
   // Double_t Wb    = W_b(E0,E0-E);
   // Wb is (X0 cancels out when combined with everything in this method): 
   // Double_t b     = fKinematicsExt.Getb(); 
   // Double_t X0    = fX0Eff;
   // Double_t Wb    = (1./X0)*(b/eps)*GetPhi(eps/E0);
   Double_t v     = (E0-E)/E0;
   Double_t phi   = (1.0 - v + (3.0/4.0)*v*v); 
   Double_t T1    = (b*t/gamma)*( 1./(E0-E) ); 
   Double_t T2    = TMath::Power(v,b*t);
   Double_t T3    = phi;
   Double_t ib    = T1*T2*T3;
   return ib; 
}
////______________________________________________________________________________
//Double_t ProtonElasticRadiativeTailDiffXSec::GetEPrime(const Double_t theta)const  {
//   // Returns the scattered electron energy using the angle. */
//   Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
//   return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M * TMath::Power(TMath::Sin(theta / 2.0), 2)));
//}
////________________________________________________________________________
//Double_t ProtonElasticRadiativeTailDiffXSec::GetTheta(double ep) const {
//   using namespace TMath;
//   double M     = fTargetNucleus.GetMass();
//   double Eb    = GetBeamEnergy();
//   double theta = 2.0*ASin(Sqrt((Eb-ep)*M/(2.0*Eb*ep)));
//   return theta;
//}
////______________________________________________________________________________
//
Double_t  ProtonElasticRadiativeTailDiffXSec::EvaluateXSec(const Double_t * x) const {
   // Get the recoiling proton momentum
   if(!x) return(0.0);
   if (!VariablesInPhaseSpace(3, &x[3])) return(0.0);

   using namespace TMath;

   Double_t Ebeam   = GetBeamEnergy();
   Double_t M       = GetTargetNucleus().GetMass();  

   double P_alpha      = x[3];
   double theta_alpha  = x[4];
   double E_alpha      = Sqrt(P_alpha*P_alpha+M*M);

   double Es     = M*P_alpha/((M+E_alpha)*Cos(theta_alpha) - P_alpha);
   double theta  = GetTheta_e(Es,theta_alpha);
   double Eprime = Es/(1.0 + 2.0*Es/M*TMath::Power(TMath::Sin(theta/2.0), 2));

   if(Es<0.0) return 0.0;

   double delta_function = Abs(1.0/(2.0*(M-E_alpha+P_alpha*Cos(theta_alpha))));
   double I   = Ib(Ebeam,Es,0.05);
   double jac = ElectronToAlphaJacobian(Es,theta_alpha);

   double Qsquared = 4.0 * Eprime * Es * TMath::Power(TMath::Sin(theta / 2.0), 2);
   double mottXSec = insane::Kine::Sig_Mott(Es,theta);
   double recoil   = insane::Kine::fRecoil(Es,theta,M);
   double GE2      = TMath::Power(fFormFactors->FCHe4(Qsquared), 2);
   double sig_rad = (mottXSec/recoil)*GE2*delta_function*I*jac;

   if( IncludeJacobian() ) sig_rad = sig_rad*TMath::Sin(theta_alpha);
   if( sig_rad <0.0 || TMath::IsNaN(sig_rad)) sig_rad = 0.0;
   return sig_rad;

   // ((GE2 + tau * GM2) / (1.0 + tau) + 2.0 * tau * GM2 * TMath::Power(TMath::Tan(theta / 2.0), 2));
   //res = res * hbarc2_gev_nb;

   //if( IncludeJacobian() ) return( TMath::Sin(theta)*res);
   //return(res);
}
//______________________________________________________________________________


}
}
