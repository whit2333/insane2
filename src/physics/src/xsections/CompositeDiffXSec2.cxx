#include "insane/xsections/CompositeDiffXSec2.h"
#include "insane/xsections/InclusiveDiffXSec.h"

using namespace insane::physics;

CompositeDiffXSec2::CompositeDiffXSec2()
{
   fID          = 100100000;
   fProtonXSec  = nullptr;
   fNeutronXSec = nullptr;
   fTitle = "CompositeDiffXSec2";
   fPlotTitle = "#frac{d#sigma}{dE d#Omega} nb/GeV-Sr";

   fProtonXSec  = new InclusiveDiffXSec();
   fProtonXSec->SetTargetNucleus(Nucleus::Proton());

   fNeutronXSec = new InclusiveDiffXSec();
   fNeutronXSec->SetTargetNucleus(Nucleus::Neutron());

   fNeutronXSec->UsePhaseSpace(false);
   fProtonXSec->UsePhaseSpace(false);
}
//______________________________________________________________________________
CompositeDiffXSec2::~CompositeDiffXSec2()
{
}
//_____________________________________________________________________________

CompositeDiffXSec2::CompositeDiffXSec2(const CompositeDiffXSec2& rhs) : 
   DiffXSec(rhs)
{
   (*this) = rhs;
}
//______________________________________________________________________________

CompositeDiffXSec2& CompositeDiffXSec2::operator=(const CompositeDiffXSec2& rhs) 
{
   if (this != &rhs) {  // make sure not same object
      DiffXSec::operator=(rhs);
      fProtonXSec     = rhs.fProtonXSec->Clone();
      fNeutronXSec     = rhs.fNeutronXSec->Clone();
   }
   return *this;    // Return ref for multiple assignment
}
//______________________________________________________________________________

CompositeDiffXSec2*  CompositeDiffXSec2::Clone(const char * newname) const 
{
   std::cout << "CompositeDiffXSec2::Clone()\n";
   auto * copy = new CompositeDiffXSec2();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

CompositeDiffXSec2*  CompositeDiffXSec2::Clone() const
{ 
   return( Clone("") );
} 
//______________________________________________________________________________
Double_t  CompositeDiffXSec2::EvaluateXSec(const Double_t * x) const{

   //std::cout << " composite eval " << std::endl;
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   if( !fProtonXSec )  return 0.0;
   if( !fNeutronXSec ) return 0.0;
   Double_t z    = GetZ();
   //if( z>0.0 ) z = 1.0; // hack for wiser... overridden in wiser codes
   Double_t n    = GetN();
   //if( n>0.0 ) n = 1.0;
   Double_t a =  z+n;

   Double_t xbj = insane::Kine::xBjorken_EEprimeTheta(GetBeamEnergy(),x[0],x[1]);
   Double_t emc = EMC_Effect(xbj,a);

   Double_t sigP = fProtonXSec->EvaluateXSec(x);
   Double_t sigN = fNeutronXSec->EvaluateXSec(x);
   Double_t xsec = (sigP + sigN)*(a/2.0);
   //std::cout << " Z  = " << GetZ() << std::endl;
   //std::cout << "sp  = " << sigP << std::endl;
   //std::cout << " N  = " << GetN() << std::endl;
   //std::cout << "sn  = " << sigN << std::endl;

   //std::cout << " composite eval " <<  xsec << std::endl;
   //if ( IncludeJacobian() ) return xsec*TMath::Sin(th);
   if(TMath::IsNaN(xsec)) xsec = 0.0;
   if(xsec < 0.0) xsec = 0.0; 
   return xsec;

}
//______________________________________________________________________________
void       CompositeDiffXSec2::SetTargetMaterial(const TargetMaterial& mat){
   if( fProtonXSec ) {
      fProtonXSec->SetTargetMaterial(mat);
      fProtonXSec->SetTargetNucleus(Nucleus::Proton());
   }
   if( fNeutronXSec ) {
      fNeutronXSec->SetTargetMaterial(mat);
      fNeutronXSec->SetTargetNucleus(Nucleus::Neutron());
   }
   fTargetMaterial = mat; fTargetNucleus = fTargetMaterial.GetNucleus();
}
//______________________________________________________________________________
void CompositeDiffXSec2::SetTargetNucleus(const Nucleus & targ){
   DiffXSec::SetTargetNucleus(targ);
   //if(fProtonXSec)   fProtonXSec->SetTargetNucleus(targ);
   //if(fNeutronXSec) fNeutronXSec->SetTargetNucleus(targ);
}
//______________________________________________________________________________
void       CompositeDiffXSec2::SetTargetMaterialIndex(Int_t i){
   if(fProtonXSec)   fProtonXSec->SetTargetMaterialIndex(i);
   if(fNeutronXSec) fNeutronXSec->SetTargetMaterialIndex(i);
   fMaterialIndex = i ;
}
//______________________________________________________________________________
void  CompositeDiffXSec2::SetUnits(const char * t){
   fUnits = t;
   if(fProtonXSec)   fProtonXSec->SetUnits(t);
   if(fNeutronXSec) fNeutronXSec->SetUnits(t);
}
//______________________________________________________________________________
void  CompositeDiffXSec2::SetBeamEnergy(Double_t en){
   DiffXSec::SetBeamEnergy(en);
   if(fProtonXSec) fProtonXSec->SetBeamEnergy(en);
   if(fNeutronXSec) fNeutronXSec->SetBeamEnergy(en);
}
//______________________________________________________________________________
void  CompositeDiffXSec2::SetPhaseSpace(PhaseSpace * ps){
   if(fProtonXSec)  fProtonXSec->SetPhaseSpace(ps);
   if(fProtonXSec)  DiffXSec::SetPhaseSpace(fProtonXSec->GetPhaseSpace());
   if(fNeutronXSec) fNeutronXSec->SetPhaseSpace(ps);
}
//______________________________________________________________________________
void   CompositeDiffXSec2::InitializePhaseSpaceVariables(){
   if(fProtonXSec)  fProtonXSec->InitializePhaseSpaceVariables();
   if(fNeutronXSec) fNeutronXSec->InitializePhaseSpaceVariables();
   if(fProtonXSec)  DiffXSec::SetPhaseSpace(fProtonXSec->GetPhaseSpace());
   if(fNeutronXSec)  fNeutronXSec->SetPhaseSpace(fProtonXSec->GetPhaseSpace());
   //DiffXSec::InitializePhaseSpaceVariables();
}
//______________________________________________________________________________
void CompositeDiffXSec2::InitializeFinalStateParticles(){
   if(fProtonXSec)  fProtonXSec->InitializeFinalStateParticles();
   if(fNeutronXSec) fNeutronXSec->InitializeFinalStateParticles();
   DiffXSec::InitializeFinalStateParticles();
}
//_____________________________________________________________________________
void CompositeDiffXSec2::SetParticleType(Int_t pdgcode, Int_t part) {
   DiffXSec::SetParticleType(pdgcode, part);
   if(fProtonXSec)  fProtonXSec->SetParticleType(pdgcode,part);
   if(fNeutronXSec) fNeutronXSec->SetParticleType(pdgcode,part);
}
//______________________________________________________________________________


