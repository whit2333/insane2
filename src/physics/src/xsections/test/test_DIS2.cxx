#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <typeinfo>

#include "PhaseSpaceVariables.h"
#include "NFoldDifferential.h"
#include "Jacobians.h"
#include "DiffCrossSection.h"
#include "PhysicalConstants.h"
#include "PSSampler.h"

#include "F1F209_SFs.h"
#include "DIS.h"

#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/Functor.h"
#include "Math/GaussIntegrator.h"

#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"

SCENARIO( "Phase Space Sampler", "[PSSampler]" ) {

  using namespace insane::physics;
  using namespace insane::helpers;
  double test_prec = 1.0e-7;


  // -------------------------------------------------------
  // Phase space variables

  PSV<Invariant> x_psv(  {0.01, 0.99}, Invariant::x,  "x" );
  PSV<Invariant> Q2_psv( {0.5,  10.0}, Invariant::Q2, "Q2");
  IPSV phi_psv({0.0, insane::units::twopi }, "phi" );

  GIVEN( "Valid fixed DIS kinematics" ) {

    // Prepare the initial state
    double P1 = 12.0;
    double m1 = 0.000511;
    double E1 = std::sqrt(P1*P1+m1*m1);

    double P2 = 0.0;
    double m2 = 0.938;
    double E2 = std::sqrt(P2*P2+m2*m2);

    InitialState init_state(RefFrame<FrameOfReference::Lab>{},P1, P2, m2);

    insane::physics::F1F209_SFs sfs;

    // Final state kinematics
    double x_0   = 0.4;
    double Q2_0  = 5.0;
    double phi_0 = 0.1;
    double y_0   = Q2_0/(init_state.s()-(m2*m2))/x_0;

    auto DIS_diff        = make_diff(x_psv, Q2_psv, phi_psv);
    auto DIS_phase_space = make_phase_space(DIS_diff);
    using xs = insane::physics::xs::DIS_XS<std::tuple<insane::physics::Stat2015_SFs>>;
    auto DIS_XS = xs::make_DIS_cross_section();
    DIS_XS.Print( );

    std::cout << " ==============\n";
    auto DIS_integrated_XS = make_integrated_cross_section(init_state, DIS_XS);
    //DIS_integrated_XS.Print();

    std::cout << " ==============\n";

    WHEN( "integrating to get total cross section" ) {

      ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS,1.0e-8); 
      ig2.SetFunction(DIS_integrated_XS);

      const auto& x_v0   = std::get<0>(DIS_integrated_XS.ConstPhaseSpace().ConstDifferential().fIndVars);
      const auto& Q2_v1  = std::get<1>(DIS_integrated_XS.ConstPhaseSpace().ConstDifferential().fIndVars);
      const auto& phi_v2 = std::get<2>(DIS_integrated_XS.ConstPhaseSpace().ConstDifferential().fIndVars);

      std::vector<double> a_min = {x_v0.Min(), Q2_v1.Min(), phi_v2.Min()};
      std::vector<double> b_max = {x_v0.Max(), Q2_v1.Max(), phi_v2.Max()};

      double ig_val = ig2.Integral(a_min.data(), b_max.data() );
      //status += std::fabs(val-RESULT) > ERRORLIMIT;

      auto DIS_sampler = make_ps_sampler(DIS_integrated_XS);
      auto total_XS = DIS_sampler.Init();
      std::cout << "ROOT::Math::IntegratorMultiDim (VEGAS) integral : " << ig_val <<  "  +- " << ig2.Error() << std::endl;
      std::cout << "      :insane::physics::PSSampler FOAM integral : " << total_XS << "\n";

      THEN( "VEGAS and FOAM integrated cross sections should be within 5\%") {
        REQUIRE(  std::abs(ig_val-total_XS)/ig_val  < 0.05 );
        for(int i=0; i<10;i++) {
          //std::cout << "Event " << i << "\n";
          auto vars = DIS_sampler.Generate();
          for(auto v: vars) {
            REQUIRE( DIS_phase_space.IndVarsInPhaseSpace(vars) == true );
            //std::cout << v << "\n";
          }
        }
      }
    }
  }

  //WHEN( "the independent and dependent variables are in the PS " ) {
  //}

}

