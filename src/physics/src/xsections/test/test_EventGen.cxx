#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <typeinfo>

#include "PhaseSpaceVariables.h"
#include "NFoldDifferential.h"
#include "Jacobians.h"
#include "DiffCrossSection.h"
#include "PhysicalConstants.h"
#include "PSSampler.h"

#include "F1F209_SFs.h"
#include "AMTFormFactors.h"

#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/Functor.h"
#include "Math/GaussIntegrator.h"

#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"

SCENARIO( "DIS Event Generator", "[EvGen]" ) {

  using namespace insane::physics;
  using namespace insane::helpers;
  double test_prec = 1.0e-7;

  // -------------------------------------------------------
  //  as a function of x and nu
  auto nu_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double yv = vars[1];
    double Q2 = (s-m2)*xv*yv;
    return Q2/(2.0*TMath::Sqrt(m2)*xv);
  };

  // -------------------------------------------------------
  // Phase space variables

  GIVEN( "Valid fixed DIS kinematics" ) {
    // Prepare the initial state
    double P1 = 12.0;
    double m1 = 0.000511;
    double E1 = std::sqrt(P1*P1+m1*m1);

    double P2 = 0.0;
    double m2 = 0.938;
    double E2 = std::sqrt(P2*P2+m2*m2);

    InitialState init_state(P1, P2, m2);
    init_state.Print();

    // Final state kinematics
    double x_0   = 0.4;
    double Q2_0  = 3.0;
    double phi_0 = 0.1;
    double y_0   = Q2_0/(init_state.s()-(m2*m2))/x_0;

    PSV<Invariant> x_psv(  {0.2, 0.5}, Invariant::x,  "x" );
    PSV<Invariant> Q2_psv( {0.5, 4.0}, Invariant::Q2, "Q2");
    IPSV phi_psv({0.0, insane::units::pi }, "phi" );

    //auto DIS_diff        = ;
    //auto DIS_phase_space = make_phase_space( DIS_diff ); // no dependent variables for the moment
    auto DIS_XS          = make_diff_cross_section(
      make_diff(x_psv, Q2_psv, phi_psv),
      [=](const InitialState& is, const std::array<double,3>& vars) {
        static insane::physics::F1F209_SFs sfs;
        double x_bj = vars.at(0);
        double Q2   = vars.at(1);
        double phi  = vars.at(2);
        double nu = nu_func(is,vars);
        double M    = is.p2().M();
        //double M  = 0.938;
        double F1 = sfs.F1p(x_bj,Q2);
        double F2 = sfs.F2p(x_bj,Q2);
        double W1 = (1./M) *F1;
        double W2 = (1./nu)*F2;
        //// compute the Mott cross section (units = mb): 
        ////Double_t hbarc2 = 0.38939129; // (hbar*c)^2 = 0.38939129 mb*GeV^2  
        double alpha  = 1./137.;
        double s    = is.s();
        double E0   = (s-M*M)/(2.0*M);
        double y    = Q2/(x_bj*(s-M*M));
        double Ep   = (Q2/(2.0*M*x_bj))*(1.0-y)/y;
        double th   = 2.0*std::asin(M*x_bj*y/std::sqrt(Q2*(1.0-y)));
        double COS2 = std::cos(th)*std::cos(th);
        double SIN2 = std::sin(th)*std::sin(th);
        double TAN2 = SIN2/COS2; 
        double num    = alpha*alpha*COS2; 
        double den    = 4.*E0*E0*SIN2; 
        //return 1.0/vars.at(1);
        double MottXS = num/den;
        //// compute the full cross section (units = nb/GeV/sr) 
        double fullXsec = MottXS*(W2 + 2.0*TAN2*W1)*insane::units::hbarc2_gev_nb;
        return fullXsec;
      });


    // --------------------------

    auto E_prime = [](const InitialState& is, const std::array<double,3>& x){
      double x_bj = x.at(0);
      double Q2   = x.at(1);
      double phi  = x.at(2);
      double s    = is.s();
      double M    = is.p2().M();
      double E0   = (s-M*M)/(2.0*M);
      double y    = Q2/(x_bj*(s-M*M));
      double Ep   = (Q2/(2.0*M*x_bj))*(1.0-y)/y;
      double th   = insane::units::pi - 2.0*std::asin(M*x_bj*y/std::sqrt(Q2*(1.0-y)));
      ROOT::Math::Polar3D<double> pvec(Ep,th,phi);
      return ROOT::Math::XYZTVector(pvec.x(), pvec.y(), pvec.z(), Ep);
    };

    // --------------------------
    // Define Final State
    auto DIS_ePrime_FS = make_final_state_particle( E_prime, 11 );
    auto DIS_FS        = make_final_state( make_diff(x_psv, Q2_psv, phi_psv), std::make_tuple(DIS_ePrime_FS));

    // This FS grabs the PS from the cross section
    auto DIS_FS2       = make_final_state(DIS_XS, std::make_tuple(DIS_ePrime_FS));

    // ----------------------------
    // PS Sampler
    auto DIS_integrated_XS = make_integrated_cross_section(init_state, DIS_XS);
    auto DIS_sampler = make_ps_sampler(DIS_integrated_XS);

    //auto total_XS = DIS_sampler.Init();
    // --------------------------
    // Event Generator
    auto DIS_evgen     = make_event_generator(DIS_sampler, DIS_FS);
    auto total_XS      = DIS_evgen.Init();

    WHEN( "Event generator is DIS" ) {
      THEN( "Generate 10 events with FS particles") {

        double Ebeam   = init_state.p1().E();
        auto  p_target = init_state.p2();

        for(int i=0; i<10;i++) {
          //std::cout << "Event " << i << "\n";
          auto parts = DIS_evgen.GenerateEvent();
          double x_ev, Q2_ev;

          for(auto p : parts) {
            auto q = init_state.p1()-p;
            x_ev = -1.0*(q.Dot(q))/(2.0*(p_target.Dot(q)));
            Q2_ev = -1.0*(q.Dot(q));
            //std::cout << " x  = " << x_ev << "\n";
            //std::cout << " Q2 = " << Q2_ev << "\n";
            //std::cout << p << std::endl;
          }
          for(auto v : DIS_evgen.fVars) {
            std::cout << v << std::endl;
          }
          REQUIRE( std::abs( x_ev  - DIS_evgen.fVars.at(0) ) < 1.0e-4 );
          REQUIRE( std::abs( Q2_ev - DIS_evgen.fVars.at(1) ) < 1.0e-4 );
        }
      }
    }
  }
}


SCENARIO( "Elastic ep Event Generator", "[EvGen]" ) {

  using namespace insane::physics;
  using namespace insane::helpers;
  double test_prec = 1.0e-7;

  // Q2 as a function of x and y
  auto Q2_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double yv = vars[1];
    return (s-m2)*xv*yv; };

  // x as a function of x (trivial)
  auto x_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double xv = vars[0];
    return xv; };

  // x as a function of x (trivial)
  auto simple_func = [=](const InitialState& is, const std::array<double,1>& vars){
    double xv = vars[0];
    return xv; };

  // y as a function  of x and Q2
  auto y_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double Q2v = vars[1];
    return Q2v/((s-m2)*xv); };

  //  as a function of x and nu
  auto nu_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double yv = vars[1];
    double Q2 = (s-m2)*xv*yv;
    return Q2/(2.0*TMath::Sqrt(m2)*xv);
  };

  // -------------------------------------------------------
  // Phase space variables

  PSV<Invariant> Q2_psv({0.5, 2.0}, Invariant::Q2, "Q2");
  IPSV phi_psv({0.0, insane::units::pi},  "phi");

  // Prepare the initial state
  double P1 = 2.0;
  double m1 = 0.000511;
  double E1 = std::sqrt(P1*P1+m1*m1);

  double P2 = 0.0;
  double m2 = 0.938;
  double E2 = std::sqrt(P2*P2+m2*m2);

  InitialState init_state(P1, P2, m2);

  // Final state kinematics
  double Q2_0 = 0.2;

  auto diff0 = make_diff(Q2_psv, phi_psv);
  auto ps0   = make_phase_space( diff0 );

  //auto E_prime = [](const InitialState& is, const std::array<double,2>& x){
  //  double Q2   = x.at(0);
  //  double phi  = x.at(1);
  //  double s    = is.s();
  //  double M    = is.p2().M();
  //  double E0   = is.p1().E();
  //  double Ep   = E0 - Q2/(2.0*M);
  //  //double th   = 2.0*std::asin(std::sqrt(Q2/(4.0*Ep*E0)));
  //  double th   = insane::units::pi-2.0*std::asin(std::sqrt(Q2/(4.0*Ep*E0)));
  //  ROOT::Math::Polar3D<double> pvec(Ep,th,phi);
  //  return ROOT::Math::XYZTVector(pvec.x(), pvec.y(), pvec.z(), Ep);
  //};
  //auto P_prime = [](const InitialState& is, const std::array<double,2>& x){
  //  double Q2   = x.at(0);
  //  double phi  = x.at(1);
  //  double s    = is.s();
  //  double M    = is.p2().M();
  //  double E0   = is.p1().E();
  //  double Ep   = E0 - Q2/(2.0*M);
  //  double th   = insane::units::pi-2.0*std::asin(std::sqrt(Q2/(4.0*Ep*E0)));
  //  ROOT::Math::Polar3D<double> pvec(Ep,th,phi);
  //  auto E_prime = ROOT::Math::XYZTVector(pvec.x(), pvec.y(), pvec.z(), Ep);
  //  return is.p1()+is.p2()-E_prime;
  //};
  auto E_and_P_prime = [](const InitialState& is, const std::array<double,2>& x){
    double Q2   = x.at(0);
    double phi  = x.at(1);
    double s    = is.s();
    double M    = is.p2().M();
    double E0   = (s-M*M)/(2.0*M);
    double y    = Q2/((s-M*M));
    double Ep   = (Q2/(2.0*M))*(1.0-y)/y;
    double th   = 2.0*std::asin(M*y/std::sqrt(Q2*(1.0-y)));
    ROOT::Math::Polar3D<double> pvec(Ep,th,phi);
    auto E_prime = ROOT::Math::XYZTVector(pvec.x(), pvec.y(), pvec.z(), Ep);
    auto P_prime = is.p1()+is.p2()-E_prime;
    return std::array<ROOT::Math::XYZTVector,2>({E_prime,P_prime});
  };
  auto fsps01 = make_final_state_particles({11,2212},{0.000511,0.938}, E_and_P_prime );


  auto elastic_DiffXS_func = [](const InitialState& is, const std::array<double,2>& x){
    static AMTFormFactors ffs;
    double M   = is.p2().M();
    double Q2  = x.at(0);
    double tau = Q2/(4.0*M*M);
    double E0  = is.p1().E();
    double Ep  = E0 - Q2/(2.0*M);
    if( Ep > E0 ) return 0.0;
    double theta = 2.0*std::asin(std::sqrt(Q2/(4.0*Ep*E0)));
    double mottXSec      = insane::Kine::Sig_Mott(E0,theta);
    double recoil_factor = Ep/E0;
    double GE2           = std::pow(ffs.GEp(Q2), 2.0);
    double GM2           = std::pow(ffs.GMp(Q2), 2.0);
    double tanthetaOver2 = std::tan(theta/2.0);
    // Rosenbluth formula
    double res = mottXSec*recoil_factor*( (GE2+tau*GM2)/(1.0+tau) 
                                         + 2.0*tau*GM2*tanthetaOver2*tanthetaOver2 );
    res = res * hbarc2_gev_nb;
    return(res);
  };

  auto elastic_xs = make_diff_cross_section( ps0, elastic_DiffXS_func  );

  //auto fsp0 = make_final_state_particle(E_prime,11 );
  //auto fsp1 = make_final_state_particle(P_prime,2212,0.938 );
  //std::cout << fsp0(init_state, {Q2_0,0.1}) << std::endl;

  auto fs0   = make_final_state(ps0, fsps01);
  auto parts = fs0.CalculateFinalState(init_state, {Q2_0,0.1});

  //for(auto p : parts) {
  //  std::cout << p << std::endl;
  //  std::cout << p.M() << std::endl;
  //}

  // ----------------------------
  // PS Sampler
  auto integrated_elastic_xs = make_integrated_cross_section(init_state, elastic_xs);
  auto elastic_sampler       = make_ps_sampler(integrated_elastic_xs);

  // --------------------------
  // Event Generator
  auto elastic_evgen = make_event_generator(elastic_sampler, fs0);
  auto total_XS      = elastic_evgen.Init();

  // --------------------------
  //
  double Ebeam    = init_state.p1().E();
  auto   p_target = init_state.p2();

  //TH1F* h1 = new TH1F("hQ2"," ;Q^{2}", 100,0,2.5);

  // --------------------------
  //
  for(int i=0; i<10000;i++) {

    //std::cout << "Event " << i << "\n";
    auto parts = elastic_evgen.GenerateEvent();

    auto   q      = init_state.p1()-parts.at(0);
    double Q2_ev  = -1.0*(q.Dot(q));

    std::cout << " p1 = " << init_state.p1() << "\n";
    std::cout << " Q2 = " << Q2_ev << "\n";
    //h1->Fill(Q2_ev);

    //for(auto p : parts) {
    //  //std::cout << " x  = " << x_ev << "\n";
    //  std::cout << p << std::endl;
    //}
    //for(auto v : elastic_evgen.fVars) {
    //  std::cout << v << std::endl;
    //}
  }

  //h1->Draw();
}
