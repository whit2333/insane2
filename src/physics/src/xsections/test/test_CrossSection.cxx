#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <typeinfo>

#include "PhaseSpaceVariables.h"
#include "NFoldDifferential.h"
#include "Jacobians.h"
#include "DiffCrossSection.h"
#include "PhysicalConstants.h"
#include "CrossSection.h"

#include "F1F209_SFs.h"
#include "DIS.h"

#include "fwd_SFs.h"
#include "fwd_FFs.h"


#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/Functor.h"
#include "Math/GaussIntegrator.h"

#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"

SCENARIO( "Cross Section", "[CrossSection]" ) {

  using namespace insane::physics;
  using namespace insane::helpers;
  double test_prec = 1.0e-7;

  REQUIRE(std::is_same<
          std::tuple<Stat2015_SFs>, 
          Filter<is_StructureFunction, std::tuple<Stat2015_SFs,Stat2015_SSFs>>::type
          >::value );

  REQUIRE( impl::FilteredFunctions<Stat2015_SFs, Stat2015_SSFs>::N_SF == 1 );
  REQUIRE( impl::FilteredFunctions<Stat2015_SFs, CTEQ10_SFs>::N_SF == 2 );
  REQUIRE( impl::FilteredFunctions<Stat2015_SFs, Stat2015_SSFs, CTEQ10_SFs, GS_SSFs>::N_SSF == 2 );
  REQUIRE( impl::FilteredFunctions<Stat2015_SFs, Stat2015_SSFs, CTEQ10_SFs, GS_SSFs>::N_SF == 2 );
  REQUIRE( impl::FilteredFunctions<Stat2015_SFs, Stat2015_SSFs, CTEQ10_SFs, GS_SSFs, AMT_FFs_t>::N_FF == 1 );
  REQUIRE( impl::FilteredFunctions<Stat2015_SFs, Stat2015_SSFs, CTEQ10_SFs, GS_SSFs, AMT_FFs_t, Kelly_FFs_t>::N_FF == 2 );
  REQUIRE( impl::FilteredFunctions<AMT_FFs_t, Kelly_FFs_t>::N_FF == 2 );
  REQUIRE( impl::FilteredFunctions<std::tuple<AMT_FFs_t, Kelly_FFs_t>>::N_FF == 2 );



  REQUIRE( impl::has_FF<Stat2015_SFs, Stat2015_SSFs, CTEQ10_SFs, GS_SSFs, AMT_FFs_t, Kelly_FFs_t>::value );
  REQUIRE( impl::has_SF<Stat2015_SFs, Stat2015_SSFs, CTEQ10_SFs, GS_SSFs, AMT_FFs_t, Kelly_FFs_t>::value );
  REQUIRE( impl::has_SSF<Stat2015_SFs, Stat2015_SSFs, CTEQ10_SFs, GS_SSFs, AMT_FFs_t, Kelly_FFs_t>::value );
  REQUIRE( impl::has_FF<Stat2015_SFs, Stat2015_SSFs>::value == false);

  using has_the_SFs = impl::has_SF<Stat2015_SFs, Stat2015_SSFs, CTEQ10_SFs, GS_SSFs, AMT_FFs_t, Kelly_FFs_t> ;
  using has_the_FFs = impl::has_FF<Stat2015_SFs, Stat2015_SSFs, CTEQ10_SFs, GS_SSFs, AMT_FFs_t, Kelly_FFs_t> ;
  REQUIRE( has_the_SFs::value );
  REQUIRE( has_the_FFs::value );

  REQUIRE(impl::has_all_functions<
          std::tuple<Stat2015_SFs, Stat2015_SSFs, CTEQ10_SFs, GS_SSFs, AMT_FFs_t, Kelly_FFs_t>,
          impl::has_SF,
          impl::has_FF
          >::value );

  REQUIRE( impl::has_all_functions< std::tuple< Stat2015_SSFs, AMT_FFs_t, Kelly_FFs_t>, impl::has_SF, impl::has_FF>::value  == false);


  REQUIRE( CrossSection_impl <1,std::tuple<Stat2015_SFs, Stat2015_SSFs, CTEQ10_SFs, GS_SSFs, AMT_FFs_t, Kelly_FFs_t>,
                    impl::has_SF, impl::has_FF >::N_SF ==2 );

  using list_of_functions = std::tuple<Stat2015_SFs, Stat2015_SSFs, CTEQ10_SFs, GS_SSFs, AMT_FFs_t, Kelly_FFs_t>;
  CrossSection_impl <1,list_of_functions, needs_FFs>  elastic_xs; //  SFs

  REQUIRE( CrossSection<1,std::tuple<Stat2015_SFs, Stat2015_SSFs, CTEQ10_SFs, GS_SSFs, AMT_FFs_t, Kelly_FFs_t>,
                    impl::has_SF, impl::has_FF >::N_SF ==2 );

}

