#include "insane/xsections/QEIntegral.h"
#include "insane/formfactors/GalsterFormFactors.h"

using namespace insane::physics;


//______________________________________________________________________________
QEIntegral::QEIntegral() : fIsDebug(false),fEs(1.0),fEp(0.5),fth(0.0),fZ(1.0),
   fA(1.0),fMT(1.8),fKF(1.0),fEShift(1.0) {

   // default form factors 
   // InSANEDipoleFormFactors *dipFF = new InSANEDipoleFormFactors(); 
   auto *galFF      = new GalsterFormFactors(); // also has GEp and GMp for completeness (dipole form)  
   SetFormFactors(galFF); 
   SetParameters(4); 
}
//______________________________________________________________________________
QEIntegral::~QEIntegral(){

}
//______________________________________________________________________________
Double_t QEIntegral::EvaluateIntegral(Double_t Es,Double_t Ep,Double_t th){

   fEs = Es; 
   fEp = Ep; 
   fth = th; 

   Double_t min = -1.;
   Double_t max =  1.;

   QEFuncWrap funcWrap;
   funcWrap.QEXS = this;    

   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   double absErr                           = 1e10;   /// desired absolute Error 
   double relErr                           = 1e-08;   /// desired relative Error 
   unsigned int ncalls                     = 1.0e5;   /// number of calls (MC only)

   ROOT::Math::IntegratorOneDim  ig(type,absErr,relErr,ncalls);
   ig.SetFunction( funcWrap );

   Double_t integral = ig.Integral(min,max);
   return integral;

}
//______________________________________________________________________________
Double_t QEIntegral::Integrand(Double_t COS_cm){

   // kinematics 
   Double_t Es     = fEs; 
   Double_t Ep     = fEp; 
   Double_t th     = fth;  
   Double_t nu     = fEs-fEp;
   Double_t x      = insane::Kine::xBjorken_EEprimeTheta(Es,Ep,th);  
   Double_t Q2     = insane::Kine::Qsquared(Es,Ep,th); 
   Double_t TAN    = TMath::Tan(th/2.); 
   Double_t TAN2   = TAN*TAN; 
   // nucleus and  mass terms 
   Double_t MT     = fMT;
   Double_t MN     = M_n/GeV; 
   Double_t MP     = M_p/GeV; 
   // calculations  
   Double_t tau    = Q2/(4.*MP*MP); 
   Double_t MottXS = insane::Kine::Sig_Mott(Es,th); 
   Double_t S_t    = MT*MT + 2.*MT*nu - Q2;
   Double_t qVect  = TMath::Sqrt(nu*nu + Q2);  
   Double_t W_t    = TMath::Sqrt(S_t);  
   Double_t E_cm   = (S_t + MN*MN - MP*MP)/(2.*W_t); 
   Double_t P_cm   = TMath::Sqrt(E_cm*E_cm - MN*MN);
   Double_t P_sp   = ( (MT-nu)*P_cm*COS_cm - E_cm*qVect )/TMath::Sqrt(S_t); // from the Atwood & West paper  
   Double_t E_sp   = TMath::Sqrt(P_sp*P_sp + MP*MP);  // FIXME: or MN? 
   Double_t P_0    = MT - E_sp; 
   Double_t P_x    = TMath::Sqrt( 0.5*P_cm*P_cm*(1.-COS_cm*COS_cm) ); 
   Double_t P_z    = TMath::Sqrt( P_sp*P_sp - 2.*P_x*P_x); 
   if(TMath::IsNaN(P_z)) return 0.0;
   // script F (Phys Rev D, 12 1884, Eq A76) 
   Double_t s1     = TMath::Power(P_0 - (nu/qVect)*P_z,2.); 
   Double_t s2     = ( 1. - TMath::Power(nu/qVect,2.) )*P_x*P_x; 
   Double_t sF     = (s1 + s2)/(MP*MP);
   // F_p = the probability that nucleons have the momentum P_sp inside the nucleus 
   // F_p = |psi(P_sp)|^2, where psi is the Fourier transform of the nonrelativistic spatial wave function.
   // FIXME: What is it?? 
   Double_t F_y    = ScalingFunction(x,Q2); 
   if(TMath::IsNaN(F_y)) return 0.0;
   //Double_t F_p    = F_y*F_y;   
   Double_t F_p    = F_y*fFermiMomentum.GetProb(TMath::Abs(P_sp),fA);   
   // form factors 
   //Double_t ff[2]  = {0.,0.}; 
   Double_t GE2  = fFF->GENuclear2(Q2,fZ,fA); 
   Double_t GM2  = fFF->GMNuclear2(Q2,fZ,fA);  

   //Double_t     = ff[0]; 
   //Double_t     = ff[1];  
   // compute the integrand 
   Double_t N      = MottXS*MP*( P_cm/(2.*W_t) );
   Double_t W1     = tau*GM2; 
   Double_t W2     = (GE2 + tau*GM2)/(1.+tau); 
   Double_t T1     = W2*(sF + TMath::Power(P_x/MP,2.)*2.*TAN2)*F_p*E_sp; 
   Double_t T2     = W1*2.*TAN2*F_p*E_sp; 
   Double_t res    = N*(T1 + T2); 
   if(TMath::IsNaN(res)) return 0.0;

   //if(fIsDebug){
   //   std::cout << "------------------ Integrand ------------------" << std::endl;
   //   std::cout << "cos(th_cm) = " << COS_cm    << std::endl; 
   //   std::cout << "fA         = " << fA        << std::endl;
   //   std::cout << "Es         = " << Es        << std::endl;
   //   std::cout << "Ep         = " << Ep        << std::endl;
   //   std::cout << "th         = " << th/degree << std::endl;
   //   std::cout << "nu         = " << nu        << std::endl;
   //   std::cout << "Q2         = " << Q2        << std::endl;
   //   std::cout << "qVect      = " << qVect     << std::endl;
   //   std::cout << "MT         = " << MT        << std::endl;
   //   std::cout << "tau        = " << tau       << std::endl;
   //   std::cout << "MottXS     = " << MottXS    << std::endl;
   //   std::cout << "S_t        = " << S_t       << std::endl;
   //   std::cout << "W_t        = " << W_t       << std::endl;
   //   std::cout << "E_cm       = " << E_cm      << std::endl;
   //   std::cout << "E_sp       = " << E_sp      << std::endl;
   //   std::cout << "P_cm       = " << P_cm      << std::endl;
   //   std::cout << "P_sp       = " << P_sp      << std::endl;
   //   std::cout << "P_0        = " << P_0       << std::endl;
   //   std::cout << "P_x        = " << P_x       << std::endl;
   //   std::cout << "P_z        = " << P_z       << std::endl;
   //   std::cout << "s1         = " << s1        << std::endl;
   //   std::cout << "s2         = " << s2        << std::endl;
   //   std::cout << "sF         = " << sF        << std::endl;
   //   std::cout << "F_y(psi')  = " << F_y       << std::endl;  
   //   std::cout << "F_p        = " << F_p       << std::endl;
   //   std::cout << "GE         = " << GE2       << std::endl;
   //   std::cout << "GM         = " << GM2       << std::endl;
   //   std::cout << "N          = " << N         << std::endl;
   //   std::cout << "T1         = " << T1        << std::endl;
   //   std::cout << "T2         = " << T2        << std::endl;
   //   std::cout << "res        = " << res       << std::endl;
   //}

   return res;  

}
//______________________________________________________________________________
Double_t QEIntegral::ScalingFunction(Double_t x,Double_t Qsq){
   // see Bosted and Mamyan, arXiv: 1203.2262v2, Eq 11
   // First three terms 
   Double_t psi_pr = Psi(x,Qsq,fEShift);
   Double_t T1     = 1.5576/fKF;
   Double_t T2     = 1. + TMath::Power(1.7720*(psi_pr+ 0.3014),2.);
   Double_t T3     = 1. + TMath::Exp(-2.4291*psi_pr);
   // put it together  
   Double_t sf     = T1/T2/T3;
   return sf;
}
//______________________________________________________________________________
Double_t QEIntegral::Psi(Double_t x,Double_t Qsq,Double_t EShift){
   // For psi', use Es != 0 
   Double_t M      = M_p/GeV;
   Double_t nu     = Qsq/(2.*M*x);
   Double_t qVect  = TMath::Sqrt(nu*nu + Qsq);
   Double_t kappa  = qVect/(2.*M);
   Double_t lambda = (nu-EShift)/(2.*M);
   Double_t tau    = kappa*kappa - lambda*lambda;
   Double_t eta_F  = fKF/M;
   Double_t xi_F   = TMath::Sqrt(1. + eta_F*eta_F) - 1.;
   // First term  
   Double_t T1     = 1./TMath::Sqrt(xi_F);
   // Second term 
   Double_t num    = lambda - tau;
   Double_t den    = TMath::Sqrt( (1.+lambda)*tau + kappa*TMath::Sqrt( tau*(1.+tau) ) );
   Double_t T2     = num/den;
   // Put it together  
   Double_t psi    = T1*T2;
   return psi;
}
//______________________________________________________________________________
void QEIntegral::SetParameters(Int_t A){
   if(A==2){
      fKF     = 0.085;
      fEShift = 0.000522;
   }else if(A==3){
      fKF     = 0.115;
      fEShift = 0.001;
   }else if( (A>3)&&(A<8) ){
      fKF     = 0.190;
      fEShift = 0.017;
   }else if( (A>7)&&(A<17) ){
      fKF     = 0.228;
      fEShift = 0.0165;
   }else if( (A>16)&&(A<26) ){
      fKF     = 0.230;
      fEShift = 0.023;
   }else if( (A>25)&&(A<39) ){
      fKF     = 0.236;
      fEShift = 0.018;
   }else if( (A>38)&&(A<56) ){
      fKF     = 0.241;
      fEShift = 0.028;
   }else if( (A>55)&&(A<61) ){
      fKF     = 0.241;
      fEShift = 0.023;
   }else if(A>60){
      fKF     = 0.245;
      fEShift = 0.018;
   }
}
//______________________________________________________________________________

