#include "insane/xsections/MAIDExclusivePionDiffXSec3.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
MAIDExclusivePionDiffXSec3::MAIDExclusivePionDiffXSec3(const char * pion,const char * nucleon){

   fPion    = pion; 
   fNucleon = nucleon; 
   frxn     = Form("%s_%s",fPion.Data(),fNucleon.Data());

   SetTitle("MAIDExclusivePionDiffXSec3");
   SetPlotTitle("MAID #vec{p}(#vec{e},e'#pi)p");
   fLabel      = " #frac{d#sigma}{dE'd#Omega#Omega_{#pi}} ";
   fUnits      = "nb/GeV/sr^{2}";

   fOmega_pi_cm_over_lab = 1.0;
   fOmega_pi_cm = 0.0;
   fTheta_pi_cm = 0.0;
   fPhi_pi_cm   = 0.0;
   fM_1         = M_p/GeV;
   fM_2         = M_p/GeV;
   fM_pi        = M_pion/GeV;

   fTheta_pi_q_lab = 0.0;

   fnDim       = 5;
   fPIDs.clear();
   fnParticles = 3;
   fPIDs.push_back(11);     // e
   fPIDs.push_back(2212);   // N 
   fPIDs.push_back(111);    // pi

   fDebug      = false;                          // debug flag 
   SetVirtualPhotonFluxConvention(2);
}

//______________________________________________________________________________
MAIDExclusivePionDiffXSec3::~MAIDExclusivePionDiffXSec3(){
   
}
//____________________________________________________________________
void MAIDExclusivePionDiffXSec3::InitializePhaseSpaceVariables() {
   //std::cout << " o InclusiveDiffXSec::InitializePhaseSpaceVariables() \n";
   PhaseSpace * ps = GetPhaseSpace();

      //------------------------
      auto * varEnergy = new PhaseSpaceVariable();
      varEnergy = new PhaseSpaceVariable();
      varEnergy->SetNameTitle("energy_e", "E_{e'}");
      varEnergy->SetMinimum(0.001); //GeV
      varEnergy->SetMaximum(5.0); //GeV
      varEnergy->SetParticleIndex(0);
      ps->AddVariable(varEnergy);

      auto *   varTheta = new PhaseSpaceVariable();
      varTheta->SetNameTitle("theta_e", "#theta_{e'}"); // ROOT string latex
      varTheta->SetMinimum(0.0*degree ); //
      varTheta->SetMaximum(180.0*degree ); //
      varTheta->SetParticleIndex(0);
      ps->AddVariable(varTheta);

      auto *   varPhi = new PhaseSpaceVariable();
      varPhi->SetNameTitle("phi_e", "#phi_{e'}"); // ROOT string latex
      varPhi->SetMinimum(-360.0*degree ); //
      varPhi->SetMaximum( 360.0*degree ); //
      varPhi->SetParticleIndex(0);
      ps->AddVariable(varPhi);

      //------------------------
      auto * varEnergyP = new PhaseSpaceVariable();
      varEnergyP = new PhaseSpaceVariable();
      varEnergyP->SetNameTitle("energy_p", "E_{p}");
      varEnergyP->SetMinimum(0.0); //GeV
      varEnergyP->SetMaximum(5.90); //GeV
      varEnergyP->SetParticleIndex(1);
      varEnergyP->SetDependent(true);
      ps->AddVariable(varEnergyP);

      auto *   varThetaP = new PhaseSpaceVariable();
      varThetaP->SetNameTitle("theta_p", "#theta_{p}"); // ROOT string latex
      varThetaP->SetMinimum(0.0  *degree ); //
      varThetaP->SetMaximum(180.0*degree ); //
      varThetaP->SetDependent(true);
      varThetaP->SetParticleIndex(1);
      ps->AddVariable(varThetaP);

      auto *   varPhiP = new PhaseSpaceVariable();
      varPhiP->SetNameTitle("phi_p", "#phi_{p}"); // ROOT string latex
      varPhiP->SetMinimum(-360.0*degree ); //
      varPhiP->SetMaximum( 360.0*degree ); //
      varPhiP->SetDependent(true);
      varPhiP->SetParticleIndex(1);
      ps->AddVariable(varPhiP);

      //------------------------
      auto * varEnergyPi = new PhaseSpaceVariable();
      varEnergyPi = new PhaseSpaceVariable();
      varEnergyPi->SetNameTitle("energy_pi", "E_{pi}");
      varEnergyPi->SetMinimum(0.0); //GeV
      varEnergyPi->SetMaximum(5.9); //GeV
      varEnergyPi->SetParticleIndex(2);
      varEnergyPi->SetDependent(true);
      ps->AddVariable(varEnergyPi);

      auto *   varThetaPi = new PhaseSpaceVariable();
      varThetaPi->SetNameTitle("theta_pi", "#theta_{pi}"); // ROOT string latex
      varThetaPi->SetMinimum(0.0  *degree ); //
      varThetaPi->SetMaximum(180.0*degree ); //
      varThetaPi->SetParticleIndex(2);
      ps->AddVariable(varThetaPi);

      auto *   varPhiPi = new PhaseSpaceVariable();
      varPhiPi->SetNameTitle("phi_pi", "#phi_{pi}"); // ROOT string latex
      varPhiPi->SetMinimum(-360.0*degree ); //
      varPhiPi->SetMaximum( 360.0*degree ); //
      varPhiPi->SetParticleIndex(2);
      ps->AddVariable(varPhiPi);

      //------------------------
      SetPhaseSpace(ps);
      //ps->Print();
}
//______________________________________________________________________________
Double_t * MAIDExclusivePionDiffXSec3::GetDependentVariables(const Double_t * x) const {

   // The proceedure for solving the kinematics is as follows:
   //   1. Calculate vectors in LAB frame with pion momentum set as unit vector.
   //   2. Get rotation matrix to scattering plane where q is along the z axis
   //   3. Rotate all the vecotors to scattering plane coords and set all 4vectors
   //   4. Boost to pi-N CM frame
   //   5. Solve completely kinematics in CM frame 
   //   6. Boost back to scattering plane frame.
   //   7. Rotate back to LAB coordinates.
  
   // Variables evaluated in the lab frame.
   Double_t    E0       = GetBeamEnergy();
   Double_t    Eprime   = x[0];
   Double_t    theta    = x[1];
   Double_t    phi      = x[2];
   Double_t    theta_pi = x[3];
   Double_t    phi_pi   = x[4];

   if( phi<0.0 ) phi = phi + 2.0*pi;

   Double_t    Q2       = 4.0*Eprime*E0*TMath::Power(TMath::Sin(theta/2.0),2.0);
   Double_t    W        = insane::Kine::W_EEprimeTheta(E0,Eprime,theta);
   Double_t    nu       = E0 - Eprime;
   Double_t    absq_lab = TMath::Sqrt(Q2+nu*nu);
   Double_t    me       = M_e/GeV;
   Double_t    k1       = TMath::Sqrt(E0*E0-me*me);
   Double_t    k2       = TMath::Sqrt(Eprime*Eprime-me*me);

   // Threshold (lab) energy for production of m_pi
   fOmega_th_lab        = (TMath::Power(fM_2+fM_pi,2.0) - fM_1*fM_1 + Q2)/(2.0*fM_1);

   //std::cout << " Q2      = " << Q2 << std::endl;
   //std::cout << " W       = " << W << std::endl;
   //std::cout << " nu      = " << nu << std::endl;
   //std::cout << " E0      = " << E0 << std::endl;
   //std::cout << " E'      = " << Eprime << std::endl;
   //std::cout << "theta_pi = " << theta_pi/degree << std::endl;
   //std::cout << "phi_pi   = " << phi_pi/degree << std::endl;

   // ---------------------------------------
   // Momentum vectors
   fk1_CM.SetMagThetaPhi(k1,0.0,0.0);
   fk2_CM.SetMagThetaPhi(k2,theta,phi);
   fq_CM = fk1_CM - fk2_CM;
   fP1_CM.SetXYZ(0.0,0,0);
   fP2_CM.SetXYZ(1.0,0,0);
   fkpi_CM.SetMagThetaPhi(1.0,theta_pi,phi_pi); // just a unit vector in the right direction
   //std::cout << "theta_q  = " << fq_CM.Theta()/degree << std::endl;

   // Rotation Matrix from lab to scattering plane coords
   labToScat.SetToIdentity();
   scatToLab.SetToIdentity();

   // Get rotation so that q is along z axis
   fY_axis =  fk1_CM.Cross(fk2_CM);
   fY_axis.SetMag(1.0);
   fZ_axis = fq_CM;
   fZ_axis.SetMag(1.0);
   fX_axis = fY_axis.Cross(fZ_axis);
   fX_axis.SetMag(1.0);
   scatToLab.RotateAxes(fX_axis,fY_axis,fZ_axis);
   labToScat = scatToLab.Inverse();

   // Rotate 3 vectors to scattering plane coords
   fPT_scat = fTargetPol;
   fPT_scat.Transform( labToScat);
   fq_CM.Transform(    labToScat);
   fkpi_CM.Transform(  labToScat);
   fP1_CM.Transform(   labToScat);
   fP2_CM.Transform(   labToScat);
   fk1_CM.Transform(   labToScat);
   fk2_CM.Transform(   labToScat);

   Double_t    theta_pi_SCAT = fkpi_CM.Theta();
   Double_t    phi_pi_SCAT   = fkpi_CM.Phi();

   //std::cout << "theta_pi_SCAT = " << theta_pi_SCAT/degree << std::endl;
   //std::cout << "phi_pi_SCAT   = " << phi_pi_SCAT/degree << std::endl;

   // --------------------------------------------------------
   //std::cout << "Scattering plane vectors : " << std::endl;
   //std::cout << "k1  : "; fk1_CM.Print();
   //std::cout << "k2  : "; fk2_CM.Print();
   //std::cout << "P1  : "; fP1_CM.Print();
   //std::cout << "P2  : "; fP2_CM.Print();
   //std::cout << "q   : "; fq_CM.Print();
   //std::cout << "kpi : "; fkpi_CM.Print();

   // First get the easy CM values
   // Note: the center of mass is the pion-nucleon system which is the same as the 
   //       q+p1 , not the k1+p1
   Double_t    k_cm    = absq_lab*fM_1/W;
   Double_t    beta_cm = absq_lab/(nu+fM_1);
   //std::cout << "check : absq_lab = " << absq_lab << ", fq_CM.Z() = " << fq_CM.Z() << std::endl;

   // Get boost to CM frame from Scat coords (q along z)
   betaToCM.SetXYZ(0.0,0.0,-1.0*beta_cm) ;
   betaToLAB.SetXYZ(0.0,0.0,beta_cm) ;

   // Set the 4 vectors for boosting
   f4VecPT_CM.SetVect(  fPT_scat);
   f4VecPT_CM.SetT(     1.0);
   f4Vecq_CM.SetVect(   fq_CM);
   f4Vecq_CM.SetT(      nu);
   f4Veckpi_CM.SetVect( fkpi_CM);
   f4Veckpi_CM.SetT(    1.0);// k is just a unit vector here
   f4VecP1_CM.SetVect(  fP1_CM);
   f4VecP1_CM.SetE(     fM_1);
   f4VecP2_CM.SetVect(  fP2_CM);
   f4VecP2_CM.SetE(     fM_2);
   f4Veck1_CM.SetVect(  fk1_CM);
   f4Veck1_CM.SetT(     fk1_CM.Mag());
   f4Veck2_CM.SetVect(  fk2_CM);
   f4Veck2_CM.SetT(     fk2_CM.Mag());

   //TLorentzVector test1(1.0,0.0,0.0,1.0);
   //TLorentzVector boost_piN = (f4Vecq_CM+f4VecP1_CM);
   ////std::cout << " boost    : "; boost_piN.BoostVector().Print();
   //std::cout << " betaToCM : "; betaToCM.Print();
   //std::cout << " betaToLAB: "; betaToLAB.Print();
   //std::cout << " test1    : "; test1.Print();
   //std::cout << " test1^2  : " << test1*test1 << std::endl;;
   //test1.Boost(boost_piN.BoostVector());
   //std::cout << "  boosted : "; test1.Print();
   //std::cout << "  test1^2 : " << test1*test1 << std::endl;


   // ------------------------------------------------------
   // Boost vectors in scat plane coords to pi-N CM system
   f4VecPT_CM.Boost(  betaToCM);
   f4Vecq_CM.Boost(   betaToCM);
   f4VecP1_CM.Boost(  betaToCM);
   f4VecP2_CM.Boost(  betaToCM);
   f4Veck1_CM.Boost(  betaToCM);
   f4Veck2_CM.Boost(  betaToCM);
   f4Veckpi_CM.Boost( betaToCM);


   // Center of mass values
   Double_t qcm      = f4Vecq_CM.Vect().Mag();
   Double_t nu_cm    = f4Vecq_CM.E();
   Double_t E1_cm    = f4VecP1_CM.E();
   Double_t qvec2_cm = f4Vecq_CM.Vect().Mag2();
   Double_t W_n_pi   = TMath::Sqrt((f4Vecq_CM + f4VecP1_CM)*(f4Vecq_CM + f4VecP1_CM));  // invariant mass of pi-N system

   // Solution for pion energy and final state
   fOmega_pi_cm      = ( W_n_pi*W_n_pi + fM_pi*fM_pi - fM_2*fM_2)/(2.0*W_n_pi);
   Double_t E2_cm    = ( W_n_pi*W_n_pi - fM_pi*fM_pi + fM_2*fM_2)/(2.0*W_n_pi);
   Double_t k_pi_cm  = TMath::Sqrt( fOmega_pi_cm*fOmega_pi_cm - fM_pi*fM_pi );

   // Get the angles in the CM. These are used to evaluate the virtual photon cross section
   fTheta_pi_cm      = TMath::ACos( (beta_cm - TMath::Cos(theta_pi_SCAT))/(beta_cm*TMath::Cos(theta_pi_SCAT)-1.0) );
   fPhi_pi_cm        = phi_pi_SCAT;
   fkpi_CM.SetTheta(fTheta_pi_cm);
   fkpi_CM.SetPhi(  fPhi_pi_cm);
   fkpi_CM.SetMag(k_pi_cm);

   // set the CM four vectors
   f4Veckpi_CM.SetVect(fkpi_CM);
   f4Veckpi_CM.SetE(fOmega_pi_cm);

   // recoil nucleon momentum vector
   fP2_CM  = -1.0*fkpi_CM;
   f4VecP2_CM.SetVect(fP2_CM);
   f4VecP2_CM.SetE(E2_cm);

   Double_t W_n_pi_2 = f4VecP2_CM.E() + f4Veckpi_CM.E();

   // the rest
   fq_CM   = f4Vecq_CM.Vect();
   fP1_CM  = f4VecP1_CM.Vect();
   fk1_CM  = f4Veck1_CM.Vect();
   fk2_CM  = f4Veck2_CM.Vect();
   fP_t_cm = f4VecPT_CM.Vect();
   //fP_t_cm.SetMag(1.0);


   // ---------------------------------------
   //std::cout << "check  : qcm  = " << qcm << ", k_cm = " << k_cm << std::endl;
   //std::cout << "W_n_pi check (0?): " << W_n_pi - W_n_pi_2 << std::endl;
   //std::cout << " fTheta_pi_cm = "  << fTheta_pi_cm/degree <<std::endl;
   //std::cout << " fPhi_pi_cm   = "  << fPhi_pi_cm/degree <<std::endl;
   //std::cout << " k_pi_cm      = "  << k_pi_cm <<std::endl;
   //std::cout << " fOmega_pi_cm = "  << fOmega_pi_cm <<std::endl;

   // ---------------------------------------
   //std::cout << "pi-N CM vectors : " << std::endl;
   //std::cout << "k1    : "; fk1_CM.Print();
   //std::cout << "k2    : "; fk2_CM.Print();
   //std::cout << "P1    : "; fP1_CM.Print();
   //std::cout << "P2    : "; fP2_CM.Print();
   //std::cout << "q     : "; fq_CM.Print();
   //std::cout << "kpi   : "; fkpi_CM.Print();
   //std::cout << "check : q_cm+P1_CM: " ; (fq_CM+fP1_CM).Print() ;

   // prepare new lab vectors
   f4Vecq_LAB   = f4Vecq_CM  ;
   f4VecP1_LAB  = f4VecP1_CM ;
   f4VecP2_LAB  = f4VecP2_CM ;
   f4Veck1_LAB  = f4Veck1_CM ;
   f4Veck2_LAB  = f4Veck2_CM ;
   f4Veckpi_LAB = f4Veckpi_CM;

   // boost back to lab frame in scatting coords 
   f4Vecq_LAB.Boost(  betaToLAB);
   f4VecP1_LAB.Boost( betaToLAB);
   f4VecP2_LAB.Boost( betaToLAB);
   f4Veck1_LAB.Boost( betaToLAB);
   f4Veck2_LAB.Boost( betaToLAB);
   f4Veckpi_LAB.Boost(betaToLAB);

   // set the 3 vectors lab frame 
   fq_LAB   = f4Vecq_LAB.Vect();
   fP1_LAB  = f4VecP1_LAB.Vect();
   fP2_LAB  = f4VecP2_LAB.Vect();
   fk1_LAB  = f4Veck1_LAB.Vect();
   fk2_LAB  = f4Veck2_LAB.Vect();
   fkpi_LAB = f4Veckpi_LAB.Vect();

   //std::cout << "Scattering plane vectors again " << std::endl;
   //std::cout << "k1  : "; fk1_LAB.Print();
   //std::cout << "k2  : "; fk2_LAB.Print();
   //std::cout << "P1  : "; fP1_LAB.Print();
   //std::cout << "P2  : "; fP2_LAB.Print();
   //std::cout << "q   : "; fq_LAB.Print();
   //std::cout << "kpi : "; fkpi_LAB.Print();

   //std::cout << "check: theta_pi_SCAT = " << theta_pi_SCAT/degree << ", " << fkpi_LAB.Theta()/degree << std::endl;
   //std::cout << "check: phi_pi_SCAT   = " << phi_pi_SCAT/degree << ", "   << fkpi_LAB.Phi()/degree << std::endl;
   ////fkpi_LAB.SetTheta(theta_pi_SCAT);

   // rotate back to lab coords
   fq_LAB.Transform(  scatToLab);
   fP1_LAB.Transform( scatToLab);
   fP2_LAB.Transform( scatToLab);
   fk1_LAB.Transform( scatToLab);
   fk2_LAB.Transform( scatToLab);
   fkpi_LAB.Transform(scatToLab);

   // Set the lab 4 vectors to the rotated angle
   f4Vecq_LAB.SetVect(  fq_LAB) ;
   f4VecP1_LAB.SetVect( fP1_LAB);
   f4VecP2_LAB.SetVect( fP2_LAB);
   f4Veck1_LAB.SetVect( fk1_LAB);
   f4Veck2_LAB.SetVect( fk2_LAB);
   f4Veckpi_LAB.SetVect(fkpi_LAB);

   // Now we are back in the lab
   fOmega_pi_lab       = f4Veckpi_LAB.E();
   Double_t kperp_scat = k_pi_cm*TMath::Sin(fTheta_pi_cm); 
   Double_t k0_lab     = kperp_scat/TMath::Sin(theta_pi_SCAT);

   //std::cout << "check: fOmega_pi_lab = " << fOmega_pi_lab << std::endl;
   //std::cout << "    k0_lab^2 + mpi^2 = " << TMath::Sqrt( k0_lab*k0_lab + fM_pi*fM_pi ) << std::endl;

   //std::cout << "check :  W_LAB = " << TMath::Sqrt((f4VecP1_LAB+f4Vecq_LAB)*(  f4VecP1_LAB+f4Vecq_LAB)) << std::endl;
   //std::cout << "check :  W_LAB = " << TMath::Sqrt((f4VecP2_LAB+f4Veckpi_LAB)*(f4VecP2_LAB+f4Veckpi_LAB)) << std::endl;

   //std::cout << "LAB vectors : " << std::endl;
   //std::cout << "k1  : "; fk1_LAB.Print();
   //std::cout << "k2  : "; fk2_LAB.Print();
   //std::cout << "P1  : "; fP1_LAB.Print();
   //std::cout << "P2  : "; fP2_LAB.Print();
   //std::cout << "q   : "; fq_LAB.Print();
   //std::cout << "kpi : "; fkpi_LAB.Print();

   //for some reason the angles are off below. 
   //std::cout << "check : theta_pi = " << theta_pi/degree << ", f4Veckpi_LAB.Theta() = " << f4Veckpi_LAB.Theta()/degree <<std::endl;
   //std::cout << "check : phi_pi = " << phi_pi/degree << ", f4Veckpi_LAB.Phi() = " << f4Veckpi_LAB.Phi()/degree <<std::endl;

   //std::cout << "check :  W_CM = " << TMath::Sqrt((f4VecP1_CM+f4Vecq_CM)*(  f4VecP1_CM+f4Vecq_CM)) << std::endl;
   //std::cout << "check :  W_CM = " << TMath::Sqrt((f4VecP2_CM+f4Veckpi_CM)*(f4VecP2_CM+f4Veckpi_CM)) << std::endl;

   //std::cout << "check : k1+P1-k2-P2-kpi " ; (fk1_LAB+fP1_LAB-fk2_LAB-fP2_LAB-fkpi_LAB).Print();

   // ---------------------------------------
   //f4Veck1_LAB.Print();
   //f4Veck2_LAB.Print();
   //f4Vecq_LAB.Print();
   //f4Veckpi_LAB.Print();
   //f4VecP1_LAB.Print();
   //f4VecP2_LAB.Print();
   // Jacobian to change the virtual photon xsec to lab coords (see Appendix A)
   fOmega_pi_cm_over_lab = (fkpi_LAB.Mag2()*W)/(qcm*( TMath::Abs((fM_1+nu)*(fkpi_LAB.Mag())- fOmega_pi_lab*absq_lab*TMath::Cos(theta_pi_SCAT)) ));

   // This is important as it is used by 
   fTheta_pi_q_lab        = fq_LAB.Angle(fkpi_LAB);

   fDependentVariables[0] = Eprime;
   fDependentVariables[1] = theta;
   fDependentVariables[2] = phi;

   fDependentVariables[3] = f4VecP2_LAB.E();
   fDependentVariables[4] = fP2_LAB.Theta();
   fDependentVariables[5] = fP2_LAB.Phi();

   fDependentVariables[6] = f4Veckpi_LAB.E();
   fDependentVariables[7] = fkpi_LAB.Theta();
   fDependentVariables[8] = fkpi_LAB.Phi();

   //for(int i = 0;i<9;i++){
   //   std::cout << "var[" << i << "] = " << fDependentVariables[i] <<std::endl;
   //}
   return(fDependentVariables);
}
//______________________________________________________________________________
void MAIDExclusivePionDiffXSec3::SetVirtualPhotonFluxConvention(const char * conv){
   if(  !strcmp("axial",conv) || !strcmp("Axial",conv) ) {
      SetVirtualPhotonFluxConvention(0);
   } else if(!strcmp("gilman",conv) || !strcmp("Gilman",conv) ) {
      SetVirtualPhotonFluxConvention(1);
   } else if(!strcmp("hand",conv) || !strcmp("Hand",conv) ) {
      SetVirtualPhotonFluxConvention(2);
   } else {
      Warning("SetVirtualPhotonFluxConvention(name)","Invalid convention name %s. Possible options are Axial, Gilman, or Hand",conv);
   }
}
//______________________________________________________________________________

void MAIDExclusivePionDiffXSec3::SetVirtualPhotonFluxConvention(Int_t i){
   // Virtual photon flux convention
   // 1 - Axial
   // 2 - Gilman
   // 3 - Hand   (default)
   fConvention = i;
   switch(fConvention){
      case 0: 
         fConventionName = "A";  // axial? 
         break;
      case 1: 
         fConventionName = "Gilman"; 
         break;
      case 2: 
         fConventionName = "Hand";
         break;
      default:
         std::cout << "[MAIDExclusivePionDiffXSec3::EvaluateVirtualPhotonFlux]: Invalid convention!  Exiting..." << std::endl;
         exit(1);
   }
}
//______________________________________________________________________________
void MAIDExclusivePionDiffXSec3::SetReactionChannel(TString pion,TString nucleon){
   fPion    = pion; 
   fNucleon = nucleon; 
   frxn     = Form("%s_%s",pion.Data(),nucleon.Data());
}
void MAIDExclusivePionDiffXSec3::SetTargetNucleus(const Nucleus & targ){
   Warning("SetTargetNucleus","Target nucleus is set by constructor argument and will remain unchanged.");
}
//______________________________________________________________________________
Double_t MAIDExclusivePionDiffXSec3::GetVirtualPhotonCrossSection(MAIDPolarizedKinematicKey* key, Double_t phi_e) const{

   // For some extreme cases A_1 can be bigger than 100% ... It is just set to 100%...
   if( TMath::Abs(key->fA_1) > 1.0 ) key->fA_1 = key->fA_1/(TMath::Abs(key->fA_1) + 0.00001) ;
   fA_t.SetXYZ(   key->fA_1,  key->fA_2,   key->fA_3);
   fA_et.SetXYZ(  key->fA_e1, key->fA_e2,  key->fA_e3);
   Double_t res = 0.0;
   res = (key->fSigma_0)*( 1.0 + fHelicity*key->fA_e + (fP_t_cm*fA_t) + fHelicity*(fP_t_cm*fA_et) );
   //key->Dump();
   if( (res<0.0) ||  TMath::IsNaN(res) ){
      key->Dump();
      fP_t_cm.Print();
      fA_t.Print();
      fA_et.Print();
      std::cout << " (p_t*A_t)  = " << (fP_t_cm*fA_t)  << std::endl;
      std::cout << " (p_t*A_et) = " << (fP_t_cm*fA_et) << std::endl;
      std::cout << " fHelicity = " << fHelicity << std::endl;
      std::cout << " A_e = " << key->fA_e << std::endl;
      std::cout << " sigma_0= " << key->fSigma_0 << std::endl;
      std::cout << " sigma_v = " << res << std::endl;
      res = 0.0;
   }
   return(res);
}
//______________________________________________________________________________
Double_t MAIDExclusivePionDiffXSec3::EvaluateXSec(const Double_t *par) const{
   //if (!VariablesInPhaseSpace(GetPhaseSpace()->GetDimension(), par)) return(0.0);

   // GetDependentVariables should have been called and return the arguments par.
   Double_t Es       = GetBeamEnergy();
   Double_t Ep       = par[0];
   Double_t th       = par[1];
   Double_t ph       = par[2];
   Double_t omega_pi = par[6];
   Double_t th_pi    = par[7];
   Double_t ph_pi    = par[8];

   Double_t W     = insane::Kine::W_EEprimeTheta(Es,Ep,th); 
   Double_t Q2    = insane::Kine::Qsquared(Es,Ep,th); 
   Double_t eps   = insane::Kine::epsilon(Es,Ep,th); 
   Double_t M_1   = M_p/GeV; // target nucleus
   Double_t M_2   = M_p/GeV; // recoil nucleon
   Double_t M_pi  = M_pion/GeV; // recoil pion
   Double_t omega_th = (TMath::Power(M_2+M_pi,2.0) - M_1*M_1 + Q2)/(2.0*M_1);

   //std::cout << "W = " << W << std::endl;
   //std::cout << "Q2 = " << Q2 << std::endl;

   //if( ph_pi<0.0 ) ph_pi = ph_pi + 2.0*pi;
   //if( ph<0.0 ) ph = ph + 2.0*pi;

   //std::cout << " omega_th = " << omega_th << std::endl;
   //std::cout << " omega_pi = " << omega_pi << std::endl;
   if(omega_th > omega_pi) return(0.0); 

   // convert W to MeV 
   Double_t CONV  = 1E+3; 
   W             *= CONV;  


   /// MAID model kinematic limits
   if( W  > 2000.0 ) return(0.0);
   //if( W  < 1073.2 ) return(0.0);
   if( W  < 1100.0 ) return(0.0);
   if( Q2 > 5.0    ) return(0.0);
   if( Q2 < 0.0    ) return(0.0);

   // Double_t Gamma = EvaluateFluxFactor(Es,Ep,th);  
   // interpolate from the grid (Bilinear Interpolation from Wikipedia) 
   fKineKey.fepsilon = eps;   
   fKineKey.fQ2      = Q2; 
   fKineKey.fW       = W; 
   fKineKey.fTheta   = fTheta_pi_cm/degree;   
   fKineKey.fPhi     = fPhi_pi_cm/degree;   

   //std::cout << "----------------------------------------------" << std::endl;
   //std::cout << "eps   = " << eps << std::endl;
   //std::cout << "Q2    = " << Q2 << std::endl;
   //std::cout << "W     = " << W << std::endl;
   //std::cout << "th    = " << th/degree << std::endl;
   //std::cout << "ph    = " << ph/degree << std::endl;
   //std::cout << "th_pi = " << th_pi/degree << std::endl;
   //std::cout << "ph_pi = " << ph_pi/degree << std::endl;
   //std::cout << "fTheta_pi_cm = " << fTheta_pi_cm/degree << std::endl;
   //std::cout << "fPhi_pi_cm = " << fPhi_pi_cm/degree << std::endl;
   double theta_pi_deg = fTheta_pi_cm/degree;
   double phi_pi_deg = fPhi_pi_cm/degree;

   maid07tp_( &eps, &Q2, &W, &theta_pi_deg, &phi_pi_deg, x_eval);
   
   fKineKey.fSigma_0 = x_eval[0];
   fKineKey.fA_e     = x_eval[1];
   fKineKey.fA_1     = x_eval[2];
   fKineKey.fA_2     = x_eval[3];
   fKineKey.fA_3     = x_eval[4];
   fKineKey.fA_e1    = x_eval[5];
   fKineKey.fA_e2    = x_eval[6];
   fKineKey.fA_e3    = x_eval[7];

   if( TMath::Abs(fKineKey.fA_1) > 1.0 ) fKineKey.fA_1 = fKineKey.fA_1/(TMath::Abs(fKineKey.fA_1) + 0.00001) ;
   fA_t.SetXYZ(   fKineKey.fA_1,  fKineKey.fA_2,   fKineKey.fA_3);
   fA_et.SetXYZ(  fKineKey.fA_e1, fKineKey.fA_e2,  fKineKey.fA_e3);

   Double_t res = 0.0;
   res = (fKineKey.fSigma_0)*( 1.0 + fHelicity*fKineKey.fA_e + (fP_t_cm*fA_t) + fHelicity*(fP_t_cm*fA_et) );

   Double_t gamma = EvaluateFluxFactor(Es,Ep,th);

   res = gamma*res*1.0e3*fOmega_pi_cm_over_lab;
   //std::cout << fOmega_pi_cm_over_lab << std::endl;
   //fKineKey.Dump();

   if(res<=0.0 || TMath::IsNaN(res) ){
      //std::cout << " sigma_vres = " << res << std::endl;
      res = 0.0;
   }
   //std::cout << "res(maid07) = " << res <<std::endl;
   //if(res < 0.3) aKey->Dump();

   if(IncludeJacobian()) return res*TMath::Sin(th_pi)*TMath::Sin(th);
   return res;  

}

//______________________________________________________________________________
Double_t MAIDExclusivePionDiffXSec3::EvaluateFluxFactor(Double_t Es,Double_t Ep,Double_t th) const{

   Double_t nu    = Es-Ep; 
   Double_t Q2    = insane::Kine::Qsquared(Es,Ep,th);
   Double_t eps   = insane::Kine::epsilon(Es,Ep,th); 
   Double_t K     = EvaluateVirtualPhotonFlux(nu,Q2);  
   Double_t alpha = fine_structure_const; 
   Double_t T1    = alpha/(2.*pi*pi); 
   Double_t T2    = Ep/Es; 
   Double_t T3    = K/Q2; 
   Double_t T4    = 1./(1.-eps); 
   Double_t gamma = T1*T2*T3*T4; 
   return gamma; 

}
//______________________________________________________________________________
Double_t MAIDExclusivePionDiffXSec3::EvaluateVirtualPhotonFlux(Double_t nu,Double_t Q2) const{

   Double_t K = 0.; 
   Double_t M = M_p/GeV;

   switch(fConvention){
      case 0: // virtual photon energy 
         K = nu; 
         break;
      case 1: // Gilman: use q-vector 
         K = TMath::Sqrt(nu*nu + Q2); 
         break;
      case 2: // Hand: use equivalent energy necessary for the same reaction from a real photon 
         K = nu - Q2/(2.*M); 
         break;
      default: 
         std::cout << "[MAIDExclusivePionDiffXSec3::EvaluateVirtualPhotonFlux]: Invalid convention! Using Hand" << std::endl;
         K = nu - Q2/(2.*M); 
   }

   return K; 

}
//______________________________________________________________________________
void MAIDExclusivePionDiffXSec3::PrintParameters(){

   std::cout << "-------------------- MAID Parameters --------------------" << std::endl; 
   std::cout << "Reaction channel (pion,nucleon) = " << "(" << fPion << "," << fNucleon << ")" << std::endl; 
   std::cout << "Virtual photon flux convention  = " << fConvention << " (" << fConventionName << ")" << std::endl;
   std::cout << "---------------------------------------------------------" << std::endl; 

}
//______________________________________________________________________________
void MAIDExclusivePionDiffXSec3::PrintData(){

   std::cout << "-------------------- MAID Data --------------------" << std::endl; 
}

//______________________________________________________________________________
void MAIDExclusivePionDiffXSec3::Clear(Option_t * ){

}

//______________________________________________________________________________
}}
