C ----------------------------------------------------------------------
C                           total_07.f
C                 5 total cross sections
C    *****        Program Source: 2007  (18.05.07)         *****
C ----------------------------------------------------------------------
Cj     This program was for testing the subroutine MAID07TOT 
Cj      PROGRAM INCLUSIV
Cj      IMPLICIT REAL*8 (A-H,O-Z)
Cj      INTEGER ISOCHAN
Cjc      DOUBLE PRECISION SIGT,SIGL,SIGLT,SIGLTP,SIGTTP,SIGL0,SIGLT0,SIGLTP0
Cj        SIGT=0
Cj        SIGL=0
Cj        SIGLT=0
Cj        SIGLTP=0
Cj        SIGTTP=0
Cj        SIGL0=0
Cj        SIGLT0=0
Cj        SIGLTP0=0
Cj        ISOCHAN=2
Cj        WARG=1300.D0
Cj        Q2ARG=1.10
Cj      CALL MAID07TOT(ISOCHAN,WARG,Q2ARG,
Cj     &               SIGT,SIGL,SIGLT,SIGLTP,SIGTTP,
Cj     &               SIGL0,SIGLT0,SIGLTP0)
Cj      END

      SUBROUTINE MAID07TOT(ISOCHAN,WARG,Q2ARG,
     &                     SIGT,SIGL,SIGLT,SIGLTP,SIGTTP,
     &                     SIGL0,SIGLT0,SIGLTP0)
      IMPLICIT REAL*8 (A-H,O-Z)
c      DOUBLE PRECISION SIGT,SIGL,SIGLT,SIGLTP,SIGTTP,SIGL0,SIGLT0,SIGLTP0
      CHARACTER VAR*11, UNIT*10, TEXT*64
      CHARACTER*10 MODEL(16)
      CHARACTER*20 SOLUTION
      INTEGER RESP(6)
      INTEGER VERBOSE
      COMPLEX*16 F1,F2,F3,F4,F5,F6,ACH,AIS,APN3
      COMPLEX*16 A1,A2,A3,A4,A5,A6,H1,H2,H3,H4,H5,H6
      DIMENSION ARG(400),OBS(400,36),ECM(400),ELAB(400)
      DIMENSION QPICM(400)
      DIMENSION ACH(10,8,4),AIS(10,8,3),APN3(10,8,3)
      COMMON/HELIC/ SIG1, SIG3, STL
      COMMON/QQMAX/ Q2MAX
c ***************************************************************+
      common/newres/ S31, P13, P31, D15, F35, F37
      COMMON /SOLUTION/ XX(100),ISOL
c *****************************************************************
      DATA MODEL/'Born ','Rho ','Omega ','P33(1232)', 'P11(1440)',
     * 'D13(1520) ','S11(1535) ','S31(1620)','S11(1650) ','F15(1680)'
     *,'D33(1700) ','P13(1720) ','P31(1910)','D15(1675) ','F35(1905)'
     *,'F37(1950) '/
      VERBOSE=0
      
c Set the results to zero in case the kinematics are bad
        SIGT=0
        SIGL=0
        SIGLT=0
        SIGLTP=0
        SIGTTP=0
        SIGL0=0
        SIGLT0=0
        SIGLTP0=0
c
c      OPEN(5,File='total_07.inp',form='formatted',status='old')
c      OPEN(6,File='test_total_07.out',form='formatted',status='unknown')
c
        HQC=197.3285D0
        PI=3.1415926536D0
        AMP=938.2723D0/HQC
        AMN=939.5653D0/HQC
        AMPI0=134.9642D0/HQC
        AMPIP=139.5685D0/HQC
        LMAX=5
        IMULT=0
c ********************************************************************
        IF (VERBOSE .GT. 0) THEN
        WRITE (6,99)
 99     FORMAT(19X,'M A I D  2007    (18.05.2007)',
     *  /,16X,'D. Drechsel, S.S. Kamalov, L. Tiator',
     *  /,12X,'Institut fuer Kernphysik, Universitaet Mainz',
     *  /,10X,'*************************************************',/)
        ENDIF
c *********************************************************************
c       read (5,2010) Solution
 2010  format(A20)
C       PRINT *, 'channel  1 - pi0 p; 2 - pi0 n; 3 - pi+ n; 4 - pi- p'
c       read (5,*) ISOL
c       read (5,*) ISO
C ***********************************
c Hard code the values read above:
       Solution='maid07_final'
       ISOL=1                    ! method: 1-MAID
       ISO=ISOCHAN                     ! charge channel
C ******************************
C *********************************************************************
        GO TO (31,32,33,34) ISO

  31     continue
         IF (VERBOSE .GT. 0) WRITE(6,102)
  102    FORMAT(/,13X,"*** gamma  + proton  >>  pi0 + proton  ***",/)
         AMI=AMP
         AMF=AMP
         AM=AMP
         API=AMPI0
         GO TO 35
  32     continue
         IF (VERBOSE .GT. 0) WRITE(6,103)
  103    FORMAT(/,13X,"*** gamma + neutron  >>  pi0 + neutron  ***",/)
         AMI=AMN
         AMF=AMN
         AM=AMN
         API=AMPI0
         GO TO 35
  33     continue
         IF (VERBOSE .GT. 0) WRITE(6,104)
  104    FORMAT(/,13X,"***  gamma + proton  >>  pi+ + neutron  ***",/)
         AMI=AMP
         AMF=AMN
         AM=(AMI+AMF)/2.
         API=AMPIP
         GO TO 35
  34     continue
         IF (VERBOSE .GT. 0) WRITE(6,105)
  105    FORMAT(/,13X,"***  gamma + neutron  >>  pi- + proton  ***",/)
         AMI=AMN
         AMF=AMP
         AM=(AMI+AMF)/2.
         API=AMPIP

  35    WTHR=(AMF+API)*HQC
        WTHR2=WTHR**2
c ***********************************************************************
       VEC=1
c ***********************************************************
c       read (5,*) BORN,RHO,OMEGA, P33, P11
c       read (5,*) D13, S11F, S31, S11S,  F15, D33
c       read (5,*) P13, P31, D15, F35, F37
c       READ (5,*) XE,XS,XMIX
c       READ (5,*) X3P33,X1P33,XSP33,X1S31,XSS31,X3D33,X1D33,XSD33
c       READ (5,*) X1P31,XSP31,X3F35,X1F35,XSF35,X3F37,X1F37,XSF37
c the default values of those read above are all 1
c so we just set them here again:
       BORN=1
       RHO=1
       OMEGA=1
       P33=1
       P11=1
       D13=1
       S11F=1
       S31=1
       S11S=1
       F15=1
       D33=1
       P13=1
       P31=1
       D15=1
       F35=1
       F37=1
       XE=1.D0
       XS=1.D0
       XMIX=1.D0
       X3P33=1.D0
       X1P33=1.D0
       XSP33=1.D0
       X1S31=1.D0
       XSS31=1.D0
       X3D33=1.D0
       X1D33=1.D0
       XSD33=1.D0
       X1P31=1.D0
       XSP31=1.D0
       X3F35=1.D0
       X1F35=1.D0
       XSF35=1.D0
       X3F37=1.D0
       X1F37=1.D0
       XSF37=1.D0
c ***********************************************************
c Adding these initializations to avoid gfortran problems
c -Whit
       X1P11p = 0.D0
       XSP11p = 0.D0
       X1S11p = 0.D0
       XSS11p = 0.D0
        X1S2p = 0.D0
        XSS2p = 0.D0
       X3D13p = 0.D0
       X1D13p = 0.D0
       XSD13p = 0.D0
       X3F15p = 0.D0
       X1F15p = 0.D0
       XSF15p = 0.D0
       X3P13p = 0.D0
       X1P13p = 0.D0
       XSP13p = 0.D0
       X3D15p = 0.D0
       X1D15p = 0.D0
       XSD15p = 0.D0

       X1P11n = 0.D0
       XSP11n = 0.D0
       X1S11n = 0.D0
       XSS11n = 0.D0
        X1S2n = 0.D0
        XSS2n = 0.D0
       X3D13n = 0.D0
       X1D13n = 0.D0
       XSD13n = 0.D0
       X3F15n = 0.D0
       X1F15n = 0.D0
       XSF15n = 0.D0
       X3P13n = 0.D0
       X1P13n = 0.D0
       XSP13n = 0.D0
       X3D15n = 0.D0
       X1D15n = 0.D0
       XSD15n = 0.D0
c ***************************************************
       IF (ISO.EQ.1.OR.ISO.EQ.3) THEN
c       READ (5,*) X1P11p,XSP11p,X1S11p,XSS11p,X1S2p,XSS2p
c       READ (5,*) X3D13p,X1D13p,XSD13p,X3F15p,X1F15p,XSF15p
c       READ (5,*) X3P13p,X1P13p,XSP13p,X3D15p,X1D15p,XSD15p
c Again the values read in above are all 1. by default so we just hard code them here
       X1P11p = 1.0
       XSP11p = 1.0
       X1S11p = 1.0
       XSS11p = 1.0
        X1S2p = 1.0
        XSS2p = 1.0
       X3D13p = 1.0
       X1D13p = 1.0
       XSD13p = 1.0
       X3F15p = 1.0
       X1F15p = 1.0
       XSF15p = 1.0
       X3P13p = 1.0
       X1P13p = 1.0
       XSP13p = 1.0
       X3D15p = 1.0
       X1D15p = 1.0
       XSD15p = 1.0

      STD=BORN*RHO*OMEGA*P33*P11*D13*D33*S11F*S11S*F15*S31*
     &  P13*P31*D15*F35*F37*XE*XS*XMIX*
     &  X3P33*X1P33*XSP33*X1S31*XSS31*X3D33*X1D33*XSD33*
     &  X1P31*XSP31*X3F35*X1F35*XSF35*X3F37*X1F37*XSF37*
     &  X1P11p*XSP11p*X1S11p*XSS11p*X1S2p*XSS2p*
     &  X3D13p*X1D13p*XSD13p*X3F15p*X1F15p*XSF15p*
     &  X3P13p*X1P13p*XSP13p*X3D15p*X1D15p*XSD15p

       ELSE IF (ISO.EQ.2.OR.ISO.EQ.4) THEN
c       READ (5,*) X1P11n,XSP11n,X1S11n,XSS11n,X1S2n,XSS2n
c       READ (5,*) X3D13n,X1D13n,XSD13n,X3F15n,X1F15n,XSF15n
c       READ (5,*) X3P13n,X1P13n,XSP13n,X3D15n,X1D15n,XSD15n
c Again the values read in above are all 1. by default so we just hard code them here
       X1P11n = 1.0
       XSP11n = 1.0
       X1S11n = 1.0
       XSS11n = 1.0
        X1S2n = 1.0
        XSS2n = 1.0
       X3D13n = 1.0
       X1D13n = 1.0
       XSD13n = 1.0
       X3F15n = 1.0
       X1F15n = 1.0
       XSF15n = 1.0
       X3P13n = 1.0
       X1P13n = 1.0
       XSP13n = 1.0
       X3D15n = 1.0
       X1D15n = 1.0
       XSD15n = 1.0

      STD=BORN*RHO*OMEGA*P33*P11*D13*D33*S11F*S11S*F15*S31*
     &  P13*P31*D15*F35*F37*XE*XS*XMIX*
     &  X3P33*X1P33*XSP33*X1S31*XSS31*X3D33*X1D33*XSD33*
     &  X1P31*XSP31*X3F35*X1F35*XSF35*X3F37*X1F37*XSF37*
     &  X1P11n*XSP11n*X1S11n*XSS11n*X1S2n*XSS2n*
     &  X3D13n*X1D13n*XSD13n*X3F15n*X1F15n*XSF15n*
     &  X3P13n*X1P13n*XSP13n*X3D15n*X1D15n*XSD15n

       END IF
C ******************************************************************
       CALL SOLUTIONS(SOLUTION,TEXT,XE,XS,XMIX,
     &  X3P33,X1P33,XSP33,X1S31,XSS31,X3D33,X1D33,XSD33,
     &  X1P31,XSP31,X3F35,X1F35,XSF35,X3F37,X1F37,XSF37,
     &  X1P11p,XSP11p,X1S11p,XSS11p,X1S2p,XSS2p,
     &  X3D13p,X1D13p,XSD13p,X3F15p,X1F15p,XSF15p,
     &  X3P13p,X1P13p,XSP13p,X3D15p,X1D15p,XSD15p,
     &  X1P11n,XSP11n,X1S11n,XSS11n,X1S2n,XSS2n,
     &  X3D13n,X1D13n,XSD13n,X3F15n,X1F15n,XSF15n,
     &  X3P13n,X1P13n,XSP13n,X3D15n,X1D15n,XSD15n)
      IUNI=IDINT(P33+P11+D13+D33+S11F+S11S+F15+S31+
     + P13+P31+D15+F35+F37  )
c ***************************************************
c      READ (5,*) IOUTPUT
      IOUTPUT=0
        IF (IOUTPUT .EQ. 0) THEN
          IF (VERBOSE .GT. 0) THEN
            IF (STD .EQ. 1) WRITE (6,401)
            IF (STD .NE. 1) WRITE (6,402)
          ENDIF
 401  FORMAT(' all parameters are on default values ')
 402  FORMAT(' some parameters are on non-default values,',
     &       ' for details turn full output on ')
        ELSE
           WRITE (6,106) (MODEL(I),I=1,5)
106    FORMAT(/,5X,'************************************************',
     * '**************',
     * /,5X,'Model parameters: 1 - with; 0 - without',/,
     * /,5X,5(A10,1X))
       WRITE (6,107) BORN,RHO,OMEGA,P33, P11
107    FORMAT(5X,5(F5.0,6X))
       WRITE (6,108) (MODEL(I),I=6,11)
108    FORMAT(5X,6(A10,1X))
       WRITE (6,109) D13,S11F,S31,S11S,F15,D33
109    FORMAT(5X,6(F5.0,6X))
       WRITE (6,108) (MODEL(I),I=12,16)
       WRITE (6,109) P13,P31,D15,F35,F37
      IF  (IUNI.EQ.0) WRITE (6,199)
199   FORMAT(/,5x,'!!!!!! WARNING: All resonances are turned off.',
     & ' In this case',/,5X,'!!!!!! Born and vector mesons',
     & ' contributions are non-unitarized',/)
c ***************************************************
       WRITE (6,1065) XE,XS, XMIX
 1065  FORMAT(5X,'************************************************',
     & '**************',
     & /,5x,'Parameters for piNN coupling mixing (X_mix) and',
     &  1x,'Chiral loop',/,5x, 'correction (X_ch):',1x,
     &  'relative to standard values',/,
     &  /,5x,'XE_ch =',F8.3,4x,'XL_ch =',F8.3, 4x,'X_mix =',F8.3)
       DXMIX=DABS(XMIX)
        IF (DXMIX.LE.1.E-06) write (6,165)
        IF (DXMIX.GE.1000.) write (6,166)
        IF (DXMIX.LT.1000..AND.DXMIX.GT.1.E-06) write (6,167)
165   format(43X,'PS -coupling')
166   format(43X,'PV -coupling')
167   format(43X,'mixed -coupling')

      ENDIF
c ***********************************************************
        WMEVMAX=2000.0
        IF (ISOL .GT. 1) WMEVMAX=1790.0
        Q2GEVMAX=5.0

c ***********************************************************
C       print *,'choose initial (Min) values for Q2 and W '
c       read (5,*)  Q2GEV1, WMEV1
C       print *,'choose independent variable: 1- Q2 ; 2- W ; 3- E'
c        read (5,*) Q2GEV1, XMEV1
c        read (5,*) IVAR
        IVAR=2
c        XMEV1=1232.D0
c        IF (IVAR .EQ. 2) WMEV1=XMEV1
c        IF (IVAR .EQ. 3) EMEV1=XMEV1
        WMEV1=WARG
        Q2GEV1=Q2ARG

        IF (IOUTPUT .EQ. 0) GO TO 169
        write (6,168)
168   FORMAT(5X,'************************************************',
     & '*************************')
        CALL HEL_OUT(ISO,Q2GEV1)
        write (6,168)
169   continue
c ***************************************************************


c  numt=17,35,.. odd number for Simpson integration
c  e.g. for numt=17 less than 1% error at W=1700 MeV

        NUMT=17
        HT=180.D0/(NUMT+1)
        NUMW=1
      NUMQ=1

      GO TO 12
c      IF(IVAR.EQ.2) GO TO 12
c      IF(IVAR.EQ.3) GO TO 133

C      VAR=' Q^2  '
C      UNIT='(GeV/c)^2'
C      print *, 'enter Inc MAX for  Q^2'
c      read (5,*)  HQ, Q2MAX
c
C      IF (Q2MAX.LT.Q2GEV1.OR.HQ.LT.0.D0) GO TO 2000
C      IF (HQ.EQ.0.D0) GO TO 1101
C      NUMQ=(Q2MAX-Q2GEV1+HQ)/HQ
1101  continue
c      IF(VERBOSE .GT. 0) write (6,1201) WMEV1
1201  FORMAT(/,15X,'W =',1X,F8.3,' (MeV)', /,15x,'******************')
c      IF (WMEV1.LT.WTHR.OR.WMEV1.GT.WMEVMAX) GO TO 2200
c      IF (Q2GEV1.LT.0.D0.OR.Q2GEV1.GT.Q2GEVMAX) GO TO 2200
      GO TO 1111

 12    VAR=' W   '
      Q2MAX=Q2GEV1
      UNIT=' (MeV)'
C      print *, 'enter Inc MAX for  W'
c      read (5,*) HW, WMAX
      HW=1
      WMAX=2000.D0
      IF (WMAX.LT.WMEV1.OR.HW.LT.0.D0) GO TO 2000
      IF (HW.EQ.0.D0) GO TO 1102
      NUMEW=(WMAX-WMEV1+HW)/HW
1102  continue
      IF (VERBOSE .GT. 0) write (6,1301) Q2GEV1
1301  FORMAT(/,15X,'Q^2 =',f8.3,' (GeV/c)^2 ',
     * /,15x,'***********************')
c
      IF (WMEV1.LT.WTHR.OR.WMEV1.GT.WMEVMAX) GO TO 2200
      IF (Q2GEV1.LT.0.D0.OR.Q2GEV1.GT.Q2GEVMAX) GO TO 2200
      GO TO 1111

133    CONTINUE
      VAR=' W   '
      Q2MAX=Q2GEV1
      UNIT=' (MeV)'
      read (5,*) HE, EMAX
      IF (EMAX.LT.EMEV1.OR.HE.LT.0.D0) GO TO 2000
      IF (HE.EQ.0.D0) GO TO 1103
      NUMEW=(EMAX-EMEV1+HE)/HE
1103  continue
      IF (VERBOSE .GT. 0) write (6,1301) Q2GEV1

C *******   LOOPS FOR THE VARIABLES *******

1111  NUM=0
      CUNIT=1000.*AMPIP
c      CUNIT=1.

c      DO 1000 IQ=1,NUMQ
c      IQ=1
c      Q2GEV=Q2GEV1+HQ*(IQ-1)

      Q2GEV=Q2GEV1
      Q2=Q2GEV*(1000./HQC)**2

c      DO 1 IW=1,NUMEW
c      IW=1
c        IF (IVAR .EQ. 3) THEN
c          EMEV=EMEV1+HE*(IW-1)
c          EFM=EMEV/HQC
c          WFM=SQRT(AMI**2+2*EFM*AMI)
c          WMEV=WFM*HQC
c        ELSE
c          WMEV=WMEV1+HW*(IW-1)
c          WFM=WMEV/HQC
c        ENDIF

       WMEV=WMEV1
       WFM=WMEV/HQC

c	DO 1 IW=1,NUMW
c      WMEV=WMEV1+HW*(IW-1)
c      WFM=WMEV/HQC
        IF (WMEV.GT.WMEVMAX) GO TO 18
        IF (Q2GEV.GT.Q2GEVMAX) GO TO 18
        SIGT=0
        SIGL=0
        SIGLT=0
        SIGLTP=0
        SIGTTP=0
        SIGL0=0
        SIGLT0=0
        SIGLTP0=0
        WW=2
        PP=2
        NUM=NUM+1
        DO 10 IT=1,NUMT
        TH=HT*IT
        X=COS(TH*PI/180.D0)
        STH=SIN(TH*PI/180.D0)
        WW=WW+PP
        PP=-PP
C ******************************************************************
        CALL MAID(ISO,WFM,Q2,X,QPIAV,EGVCM,EGVLAB,
     & F1,F2,F3,F4,F5,F6,A1,A2,A3,A4,A5,A6,H1,H2,H3,H4,H5,H6,
     & IMULT,LMAX,ACH,AIS,APN3,
     & BORN,VEC,OMEGA,RHO,P33,P11,D13,S11F,S11S,F15,D33)

C      EGEQ=(WFM**2-AMP**2)/2./WFM
        EGEQ=(WFM**2-AMI**2)/2./WFM
        EGCM=(WFM**2-Q2-AMI**2)/2./WFM
        EGLAB=(WFM**2+Q2-AMI**2)/2./AMI
        EPI=(WFM**2+API**2-AMF**2)/2./WFM
        QPI=SQRT(EPI**2-API**2)

        CALL RESPINC(WCM,Q2,QPI,EGEQ,EGVCM,
     & A1,A2,A3,A4,A5,A6,H1,H2,H3,H4,H5,H6,
     & ST,SL,SLT,SLTP,STTP,SL0,SLT0,SLTP0)
C ******************************************************************
        IF (IVAR.EQ.1)  ARG(NUM)=Q2GEV
        IF (IVAR.EQ.2)  ARG(NUM)=WMEV
        IF (IVAR.EQ.3)  ARG(NUM)=WMEV
        ECM(NUM)=EGCM*HQC
        QPICM(NUM)=QPI*HQC
        ELAB(NUM)=EGLAB*HQC
        SIGT  =SIGT  +WW*ST  *STH
        SIGL  =SIGL  +WW*SL  *STH
        SIGLT =SIGLT +WW*SLT *STH
        SIGLTP=SIGLTP+WW*SLTP*STH
        SIGTTP=SIGTTP+WW*STTP*STH
        SIGL0  =SIGL0  +WW*SL0  *STH
        SIGLT0 =SIGLT0 +WW*SLT0 *STH
        SIGLTP0=SIGLTP0+WW*SLTP0*STH

 10   CONTINUE

        FACT=2*PI*HT*PI/180.D0/3.D0
        OBS(NUM,1)=FACT*SIGT
        OBS(NUM,2)=FACT*SIGL
        OBS(NUM,3)=FACT*SIGLT
        OBS(NUM,4)=FACT*SIGLTP
        OBS(NUM,5)=FACT*SIGTTP
        OBS(NUM,6)=FACT*(SIGT-SIGTTP)
        OBS(NUM,7)=FACT*(SIGT+SIGTTP)
        OBS(NUM,8)=FACT*SIGL0
        OBS(NUM,9)=FACT*SIGLT0
        OBS(NUM,10)=FACT*SIGLTP0

        IF (NUM.GE.400) GO TO 18

  1   CONTINUE

 1000 CONTINUE

 18   CONTINUE

C ************** OUTPUT FOR THE RESPONSE FUNCTIONS  ****************

1003  CONTINUE

C *********** 5 total cross sections
      IF (VERBOSE .GT. 0) WRITE (6,1236) VAR
1236  FORMAT(/,' *** 5 total cross sections: ',
     & "sigT, sigL, sigLT, sigLT', sigTT' ***",/,
     & 7x,"sigT=1/2(sig_1/2 + sig_3/2), sigTT'=1/2(sig_3/2 - sig_1/2)",
     & /,7x,'sigL0=sigL*(wcm/Q)**2, sigLT0=sigLT*(wcm/Q),',
     & " sigLT0'=sigLT'*(wcm/Q)",/,
     & /,2X,A6,3X,'w(lab)',4X,
     & 'sigT',5X,'sigL',5X,'sigLT',3X,"sigLT'",3X,
     & "sigTT'",3X,'sig1/2',4x,'sig3/2',3x,
     & 'sigL0',4X,'sigLT0',2X,"sigLT0'")


      IF (VERBOSE .GT. 0) WRITE (6,1237) UNIT
1237  FORMAT(A9,'  (MeV)',1x,10(4x,'(mcb)'))

      DO      I=1,NUM
      IF (VERBOSE .GT. 0) THEN 
      IF  (IVAR.EQ.1)
     & WRITE (6,1238) ARG(I),ELAB(I),(OBS(I,J),J=1,10)
1238  FORMAT(1X,F6.3,1X,F8.1,1x,10F9.2)
      IF (IVAR.GT.1)
     & WRITE (6,1239) ARG(I),ELAB(I),(OBS(I,J),J=1,10)
1239  FORMAT(1X,F6.1,1X,F8.1,1X,10F9.2)
      ENDIF
      ENDDO

c **************************************************

      GO TO 3000
c *************** Error messages **********************

2000  WRITE (6,2001)
2001  FORMAT(/,1X,
     * '******  W R O N G   K I N E M A T I C S ! ******')
      GO TO 3000
2200  WRITE (6,2003) Q2GEVMAX,WTHR,WMEVMAX
2003  FORMAT(/,1X,'****** Q2 or W  out of limit (0-',F3.1,
     *         ' or ',F6.1,'-',F5.0,') ******')
3000  CONTINUE

      END
c
