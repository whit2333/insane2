C ----------------------------------------------------------------------
C                           target_07.f
C                     Target Polarization
C    *****        Program Source: 2007  (18.05.07)         *****
C ----------------------------------------------------------------------
      PROGRAM TARGET
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER VAR*11, UNIT*10, TEXT*64
      CHARACTER*10 MODEL(16)
      CHARACTER*20 SOLUTION
      COMPLEX*16 F1,F2,F3,F4,F5,F6,ACH,AIS,APN3,AMPL
      COMPLEX*16 A1,A2,A3,A4,A5,A6,H1,H2,H3,H4,H5,H6
      DIMENSION ARG(400),AMPL(400,10),ECM(400),ELAB(400)
      DIMENSION QPICM(400),THGAMC(400)
      DIMENSION ACH(10,8,4),AIS(10,8,3),APN3(10,8,3)
      REAL*8 RT(7),RL(4),RLT(8),RTT(5),RLTP(8),RTTP(4)
	common /frame/ wfm,q2,ami,thg,cst,snt,csf,snf,pi
c ***************************************************************+
      common/newres/ S31, P13, P31, D15, F35, F37
      COMMON/QQMAX/ Q2MAX
      COMMON /SOLUTION/ XX(100),ISOL
c *****************************************************************
      DATA MODEL/'Born ','Rho ','Omega ','P33(1232)', 'P11(1440)',
     * 'D13(1520) ','S11(1535) ','S31(1620)','S11(1650) ','F15(1680)'
     *,'D33(1700) ','P13(1720) ','P31(1910)','D15(1675) ','F35(1905)'
     *,'F37(1950) '/
c
      OPEN(5,File='target_07.inp',form='formatted',status='old')
      OPEN(6,File='target_07.out',form='formatted',status='unknown')
c
	HQC=197.3285D0
        PI=3.1415926536D0
	AMP=938.2723D0/HQC
        AMN=939.5653D0/HQC
        AMPI0=134.9642D0/HQC
	AMPIP=139.5685D0/HQC
        LMAX=5
        IMULT=0
c ********************************************************************
        WRITE (6,99)
 99     FORMAT(19X,'M A I D  2007    (18.05.2007)',
     *  /,16X,'D. Drechsel, S.S. Kamalov, L. Tiator',
     *  /,12X,'Institut fuer Kernphysik, Universitaet Mainz',
     *  /,10X,'*************************************************',/)
c *********************************************************************
       read (5,2010) Solution
 2010  format(A20)
c       PRINT *, 'channel  1 - pi0 p; 2 - pi0 n; 3 - pi+ n; 4 - pi- p'
       read (5,*) ISOL
       read (5,*) ISO
C ******************************
        GO TO (31,32,33,34) ISO

  31     WRITE(6,102)
  102    FORMAT(/,13X,"***  e + proton  >>  e' + pi0 + proton  ***",/)
         AMI=AMP
         AMF=AMP
         AM=AMP
         API=AMPI0
         GO TO 35
  32     WRITE(6,103)
  103    FORMAT(/,13X,"***  e + neutron  >> e' + pi0 + neutron  ***",/)
         AMI=AMN
         AMF=AMN
         AM=AMN
         API=AMPI0
         GO TO 35
  33     WRITE(6,104)
  104    FORMAT(/,13X,"***  e + proton  >> e' + pi+ + neutron  ***",/)
         AMI=AMP
         AMF=AMN
         AM=(AMI+AMF)/2.
         API=AMPIP
         GO TO 35
  34     WRITE(6,105)
  105    FORMAT(/,13X,"***  e + neutron  >> e' + pi- + proton  ***",/)
         AMI=AMN
         AMF=AMP
         AM=(AMI+AMF)/2.
         API=AMPIP

  35    WTHR=(AMF+API)*HQC
        WTHR2=WTHR**2
c ***********************************************************************
       VEC=1
c ***********************************************************
       read (5,*) BORN,RHO,OMEGA, P33, P11
       read (5,*) D13, S11F, S31, S11S,  F15, D33
       read (5,*) P13, P31, D15, F35, F37
       READ (5,*) XE,XS,XMIX
       READ (5,*) X3P33,X1P33,XSP33,X1S31,XSS31,X3D33,X1D33,XSD33
       READ (5,*) X1P31,XSP31,X3F35,X1F35,XSF35,X3F37,X1F37,XSF37
c ***********************************************************
c Adding these initializations to avoid gfortran problems
c -Whit
       X1P11p = 0.D0
       XSP11p = 0.D0
       X1S11p = 0.D0
       XSS11p = 0.D0
        X1S2p = 0.D0
        XSS2p = 0.D0
       X3D13p = 0.D0
       X1D13p = 0.D0
       XSD13p = 0.D0
       X3F15p = 0.D0
       X1F15p = 0.D0
       XSF15p = 0.D0
       X3P13p = 0.D0
       X1P13p = 0.D0
       XSP13p = 0.D0
       X3D15p = 0.D0
       X1D15p = 0.D0
       XSD15p = 0.D0

       X1P11n = 0.D0
       XSP11n = 0.D0
       X1S11n = 0.D0
       XSS11n = 0.D0
        X1S2n = 0.D0
        XSS2n = 0.D0
       X3D13n = 0.D0
       X1D13n = 0.D0
       XSD13n = 0.D0
       X3F15n = 0.D0
       X1F15n = 0.D0
       XSF15n = 0.D0
       X3P13n = 0.D0
       X1P13n = 0.D0
       XSP13n = 0.D0
       X3D15n = 0.D0
       X1D15n = 0.D0
       XSD15n = 0.D0
c ***************************************************
       IF (ISO.EQ.1.OR.ISO.EQ.3) THEN
       READ (5,*) X1P11p,XSP11p,X1S11p,XSS11p,X1S2p,XSS2p
       READ (5,*) X3D13p,X1D13p,XSD13p,X3F15p,X1F15p,XSF15p
       READ (5,*) X3P13p,X1P13p,XSP13p,X3D15p,X1D15p,XSD15p
      STD=BORN*RHO*OMEGA*P33*P11*D13*D33*S11F*S11S*F15*S31*
     &  P13*P31*D15*F35*F37*XE*XS*XMIX*
     &  X3P33*X1P33*XSP33*X1S31*XSS31*X3D33*X1D33*XSD33*
     &  X1P31*XSP31*X3F35*X1F35*XSF35*X3F37*X1F37*XSF37*
     &  X1P11p*XSP11p*X1S11p*XSS11p*X1S2p*XSS2p*
     &  X3D13p*X1D13p*XSD13p*X3F15p*X1F15p*XSF15p*
     &  X3P13p*X1P13p*XSP13p*X3D15p*X1D15p*XSD15p

       ELSE IF (ISO.EQ.2.OR.ISO.EQ.4) THEN
       READ (5,*) X1P11n,XSP11n,X1S11n,XSS11n,X1S2n,XSS2n
       READ (5,*) X3D13n,X1D13n,XSD13n,X3F15n,X1F15n,XSF15n
       READ (5,*) X3P13n,X1P13n,XSP13n,X3D15n,X1D15n,XSD15n
      STD=BORN*RHO*OMEGA*P33*P11*D13*D33*S11F*S11S*F15*S31*
     &  P13*P31*D15*F35*F37*XE*XS*XMIX*
     &  X3P33*X1P33*XSP33*X1S31*XSS31*X3D33*X1D33*XSD33*
     &  X1P31*XSP31*X3F35*X1F35*XSF35*X3F37*X1F37*XSF37*
     &  X1P11n*XSP11n*X1S11n*XSS11n*X1S2n*XSS2n*
     &  X3D13n*X1D13n*XSD13n*X3F15n*X1F15n*XSF15n*
     &  X3P13n*X1P13n*XSP13n*X3D15n*X1D15n*XSD15n

       END IF
C ******************************************************************
       CALL SOLUTIONS(SOLUTION,TEXT,XE,XS,XMIX,
     &  X3P33,X1P33,XSP33,X1S31,XSS31,X3D33,X1D33,XSD33,
     &  X1P31,XSP31,X3F35,X1F35,XSF35,X3F37,X1F37,XSF37,
     &  X1P11p,XSP11p,X1S11p,XSS11p,X1S2p,XSS2p,
     &  X3D13p,X1D13p,XSD13p,X3F15p,X1F15p,XSF15p,
     &  X3P13p,X1P13p,XSP13p,X3D15p,X1D15p,XSD15p,
     &  X1P11n,XSP11n,X1S11n,XSS11n,X1S2n,XSS2n,
     &  X3D13n,X1D13n,XSD13n,X3F15n,X1F15n,XSF15n,
     &  X3P13n,X1P13n,XSP13n,X3D15n,X1D15n,XSD15n)
      IUNI=IDINT(P33+P11+D13+D33+S11F+S11S+F15+S31+
     + P13+P31+D15+F35+F37  )
c ***************************************************
	READ (5,*) IOUTPUT
	IF (IOUTPUT .EQ. 0) THEN
	 IF (STD .EQ. 1) WRITE (6,401)
	 IF (STD .NE. 1) WRITE (6,402)
 401  FORMAT(' all parameters are on default values ')
 402  FORMAT(' some parameters are on non-default values,',
     &       ' for details turn full output on ')
	ELSE
       WRITE (6,106) (MODEL(I),I=1,5)
106    FORMAT(/,5X,'************************************************',
     * '**************',
     * /,5X,'Model parameters: 1 - with; 0 - without',/,
     * /,5X,5(A10,1X))
       WRITE (6,107) BORN,RHO,OMEGA,P33, P11
107    FORMAT(5X,5(F5.0,6X))
       WRITE (6,108) (MODEL(I),I=6,11)
108    FORMAT(5X,6(A10,1X))
       WRITE (6,109) D13,S11F,S31,S11S,F15,D33
109    FORMAT(5X,6(F5.0,6X))
       WRITE (6,108) (MODEL(I),I=12,16)
       WRITE (6,109) P13,P31,D15,F35,F37
      IF  (IUNI.EQ.0) WRITE (6,199)
199   FORMAT(/,5x,'!!!!!! WARNING: All resonances are turned off.',
     & ' In this case',/,5X,'!!!!!! Born and vector mesons',
     & ' contributions are non-unitarized',/)
c ***************************************************
       WRITE (6,1065) XE,XS, XMIX
 1065  FORMAT(5X,'************************************************',
     & '**************',
     & /,5x,'Parameters for piNN coupling mixing (X_mix) and',
     &  1x,'Chiral loop',/,5x, 'correction (X_ch):',1x,
     &  'relative to standard values',/,
     &  /,5x,'XE_ch =',F8.3,4x,'XL_ch =',F8.3, 4x,'X_mix =',F8.3)
        DXMIX=DABS(XMIX)
	IF (DXMIX.LE.1.E-06) write (6,165)
	IF (DXMIX.GE.1000.) write (6,166)
	IF (DXMIX.LT.1000..AND.DXMIX.GT.1.E-06) write (6,167)
165   format(43X,'PS -coupling')
166   format(43X,'PV -coupling')
167   format(43X,'mixed -coupling')

      ENDIF
c ***********************************************************
	WMEVMAX=2000.0
c     	IF (ISOL .GT. 1) WMEVMAX=1790.0
	Q2GEVMAX=5.0

c ***********************************************************
c
       read (5,*)  IFRAME
       read (5,*)  EPS
       if (iframe .eq. 5) then
       write(6,771) iframe
  771  format(' *** iframe =',i2,': invalid choice! ***')
       goto 3000
       endif

c ****************************************************************

C       print *,'choose initial (Min) values for Q2, W and Theta '
       read (5,*)  Q2GEV1, WMEV1, TH1, PH1

C       print *,'choose independent variable: 1- Q2 ; 2- W; 3- Theta '
       read (5,*) IVAR

	IF (IOUTPUT .EQ. 0) GO TO 169
      write (6,168)
168   FORMAT(5X,'************************************************',
     & '*************************')
       CALL HEL_OUT(ISO,Q2GEV1)
      write (6,168)
169   continue
c ***************************************************************

       WRITE (6,665)
 665   FORMAT(//,' *** Virtual Photon Cross Section and Asymmetries ',
     * 'with Target Polarization ***',
     * /,12X,'**** in units of microbarn/sr and percentage ****',/)
       write (6,1303) EPS
 1303  FORMAT(5x,
     &       'Transverse virtual photon polarization: epsilon =',F6.3)
c ***************************************************************
      IF (EPS.LT.0.OR.EPS.GT.1) GO TO 2400

      NUMW=1
      NUMT=1
      NUMQ=1
      NUMP=1

       IF(IVAR.EQ.2) GO TO 12
       IF(IVAR.EQ.3) GO TO 13
       IF(IVAR.EQ.4) GO TO 14

       VAR=' Q^2  '
      UNIT='(GeV/c)^2'
C      print *, 'enter Inc MAX for  Q^2'
      read (5,*)  HQ, Q2MAX
      IF (Q2MAX.LT.Q2GEV1.OR.HQ.LT.0.D0) GO TO 2000
      IF (HQ.EQ.0.D0) GO TO 1101
      NUMQ=(Q2MAX-Q2GEV1+HQ+1.D-6)/HQ
1101  write (6,1201) WMEV1, TH1, PH1
1201  FORMAT(/,5X,'W =',1X,F8.3,' (MeV);',3X,'Theta =',f8.3,' (deg)',
     * 3x,'Phi =',1x,f8.1,' (deg)',
     */,5x,70('*'))
      IF (TH1.LT.0.D0.OR.TH1.GT.180.D0) GO TO 2000
      IF (WMEV1.LT.WTHR.OR.WMEV1.GT.WMEVMAX) GO TO 2200
      IF (Q2GEV1.LT.0.D0.OR.Q2GEV1.GT.Q2GEVMAX) GO TO 2200
      GO TO 1111

 12    VAR=' W   '
       Q2MAX=Q2GEV1
      UNIT=' (MeV)'
C      print *, 'enter Inc MAX for  W'
      read (5,*) HW, WMAX
      IF (WMAX.LT.WMEV1.OR.HW.LT.0.D0) GO TO 2000
      IF (HW.EQ.0.D0) GO TO 1102
      NUMW=(WMAX-WMEV1+HW)/HW
1102   write (6,1301) Q2GEV1, TH1, Ph1
1301  FORMAT(/,5X,'Q^2 =',f8.3,' (GeV/c)^2;',3X,'Theta =',f8.3,' (deg)',
     * 3x,'Phi =',1x,f8.1,' (deg)',
     */,5x,70('*'))
      IF (TH1.LT.0.D0.OR.TH1.GT.180.D0) GO TO 2000
      IF (WMEV1.LT.WTHR.OR.WMEV1.GT.WMEVMAX) GO TO 2200
      IF (Q2GEV1.LT.0.D0.OR.Q2GEV1.GT.Q2GEVMAX) GO TO 2200

      GO TO 1111

 13    VAR='Theta '
       Q2MAX=Q2GEV1
      UNIT='  (deg)'
C      print *, 'enter Inc MAX for  Theta'
      read (5,*) HT, TMAX
      IF (TMAX.LT.TH1.OR.HT.LT.0.D0) GO TO  2000
      IF (HT.EQ.0.D0) GO TO 1103
      NUMT=(TMAX-TH1+HT)/HT
1103  write (6,1401) Q2GEV1, WMEV1, Ph1
1401  FORMAT(/,5X,'Q^2 =',f8.3,' (GeV/c)^2;',3X,'W =',1x,f8.3,' (MeV)',
     * 3x,'Phi =',1x,f8.1,' (deg)',
     */,5x,70('*'))
      IF (TH1.LT.0.D0.OR.TH1.GT.180.D0) GO TO 2000
      IF (WMEV1.LT.WTHR.OR.WMEV1.GT.WMEVMAX) GO TO 2200
      IF (Q2GEV1.LT.0.D0.OR.Q2GEV1.GT.Q2GEVMAX) GO TO 2200

      GO TO 1111

 14   VAR='Phi  '
       Q2MAX=Q2GEV1
      UNIT='  (deg)'
C      print *, 'enter Inc MAX for  Phi'
      read (5,*) HP, PMAX
      IF (PMAX.LT.PH1.OR.HP.LT.0.D0) GO TO  2000
      IF (HP.EQ.0.D0) GO TO 1104
      NUMP=(PMAX-PH1+HP)/HP
 1104 write (6,1402) Q2GEV1, WMEV1, TH1
 1402 FORMAT(/,5X,'Q^2 =',f8.3,' (GeV/c)^2;',3X,'W =',1x,f8.3,' (MeV)',
     * 3x,'Theta =',1x,f8.1,' (deg)',
     */,5x,70('*'))
      IF (TH1.LT.0.D0.OR.TH1.GT.180.D0) GO TO 2000
      IF (WMEV1.LT.WTHR.OR.WMEV1.GT.WMEVMAX) GO TO 2200
      IF (Q2GEV1.LT.0.D0.OR.Q2GEV1.GT.Q2GEVMAX) GO TO 2200
C *******   LOOPS FOR THE VARIABLES *******

1111  NUM=0

      DO 1000 IQ=1,NUMQ
      Q2GEV=Q2GEV1+HQ*(IQ-1)
      Q2=Q2GEV*(1000./HQC)**2

      DO 1 IW=1,NUMW
      WMEV=WMEV1+HW*(IW-1)
      WFM=WMEV/HQC

      CALL THGAMMA(WFM,AMI,Q2,EPS,THG)
	 thgcmin=thg
	 thgcmax=thg

	IF (IFRAME .EQ. 5 .AND. IVAR .GE. 3) write (6,1305) THG
 1305  FORMAT(5x,
     &       'Virtual photon angle in the lab frame:  thgamma =',F6.1,
     &       ' (deg)')

      DO 10 IT=1,NUMT
      TH=TH1+HT*(IT-1)
      X=COS(TH*PI/180.D0)

      DO 100 IP=1,NUMP
      PHPI=PH1+HP*(IP-1)

       IF (TH.GT.180.D0) GO TO 18
       IF (WMEV.GT.WMEVMAX) GO TO 18
       IF (Q2GEV.GT.Q2GEVMAX) GO TO 18

C *******************************************

      IF (PHPI.GT.360.D0) GO TO 18
      CSF=DCOS(PHPI*PI/180.D0)
      SNF=DSIN(PHPI*PI/180.D0)
      CS2F=DCOS(2.*PHPI*PI/180.D0)
      SN2F=DSIN(2.*PHPI*PI/180.D0)
      CST=X
      SNT=DSQRT(1.D0-X**2)

      NUM=NUM+1

      CALL MAID(ISO,WFM,Q2,X,QPIAV,EGVCM,EGVLAB,
     & F1,F2,F3,F4,F5,F6,A1,A2,A3,A4,A5,A6,H1,H2,H3,H4,H5,H6,
     & IMULT,LMAX,ACH,AIS,APN3,
     & BORN,VEC,OMEGA,RHO,P33,P11,D13,S11F,S11S,F15,D33)

C       EGEQ=(WFM**2-AM**2)/2./WFM
      EGEQ=(WFM**2-AMI**2)/2./WFM
      EGCM=(WFM**2-Q2-AMI**2)/2./WFM
      EGLAB=(WFM**2+Q2-AMI**2)/2./AMI
      EPI=(WFM**2+API**2-AMF**2)/2./WFM
      QPI=SQRT(EPI**2-API**2)

       CALL RESPONSE(WCM,Q2,QPI,EGEQ,EGVCM,
     & A1,A2,A3,A4,A5,A6,H1,H2,H3,H4,H5,H6,RT,RL,RLT,RTT,RLTP,RTTP)

       CALL OBSERVT(WCM,Q2,QPI,EGEQ,EGVCM,ST,STY,SL,SLY,STL,
     & STLX,STLY,STLZ,STT,STTX,STTY,STTZ,STLP,STLPX,STLPY,STLPZ,
     & STTPX,STTPZ,H1,H2,H3,H4,H5,H6,RT,RL,RLT,RTT,RLTP,RTTP)

c       SVIR=ST + EPS*SL + DSQRT(2.*EPS*(1.+EPS))*CSF*STL +
c     + EPS*CS2F*STT + HEL*DSQRT(2.*EPS*(1.-EPS))*SNF*STLP

C       S5FOLD=GMEV*SVIR
c       PolX=DSQRT(2.*EPS*(1.+EPS))*SNF*STLX +
c     +   EPS*SN2F*STTX + HEL*DSQRT(2.*EPS*(1.-EPS))*CSF*STLPX +
c     +   HEL*DSQRT(1.-EPS**2)*STTPX
c       PolY=STY + EPS*SLY + DSQRT(2.*EPS*(1.+EPS))*CSF*STLY +
c     +   EPS*CS2F*STTY + HEL*DSQRT(2.*EPS*(1.-EPS))*SNF*STLPY
c       PolZ=DSQRT(2.*EPS*(1.+EPS))*SNF*STLZ +
c     +   EPS*SN2F*STTZ + HEL*DSQRT(2.*EPS*(1.-EPS))*CSF*STLPZ +
c     +   HEL*DSQRT(1.-EPS**2)*STTPZ
c
c       PolX=PolX/SVIR*100
c       PolY=PolY/SVIR*100
c       PolZ=PolZ/SVIR*100
c
c	 call frames(iframe,polx,poly,polz,thgc)
c
C  ****** decomposition in sig_0, sig_e, sig_t, sig_et  ******
C  ***** sig=sig0 + Pe*sig_e + Pt*sig_t + Pe*Pt*sig_et  ******
C  *****      always plus signs are used !!!            ******
C
	 sig0=ST + EPS*SL + DSQRT(2.*EPS*(1.+EPS))*CSF*STL + EPS*CS2F*STT
	 sige=DSQRT(2.*EPS*(1.-EPS))*SNF*STLP
	 sigx=DSQRT(2.*EPS*(1.+EPS))*SNF*STLX + EPS*SN2F*STTX
	 sigy=STY+EPS*SLY+DSQRT(2.*EPS*(1.+EPS))*CSF*STLY+EPS*CS2F*STTY
	 sigz=DSQRT(2.*EPS*(1.+EPS))*SNF*STLZ+EPS*SN2F*STTZ
	 sigex=DSQRT(2.*EPS*(1.-EPS))*CSF*STLPX+DSQRT(1.-EPS**2)*STTPX
	 sigey=DSQRT(2.*EPS*(1.-EPS))*SNF*STLPY
	 sigez=DSQRT(2.*EPS*(1.-EPS))*CSF*STLPZ+DSQRT(1.-EPS**2)*STTPZ

	 call frames(iframe,sigx,sigy,sigz,thgc)
	 call frames(iframe,sigex,sigey,sigez,thgc)
C ********************
       IF (IVAR.EQ.1)  ARG(NUM)=Q2GEV
       IF (IVAR.EQ.2)  ARG(NUM)=WMEV
       IF (IVAR.EQ.3)  ARG(NUM)=TH
       IF (IVAR.EQ.4)  ARG(NUM)=PHPI
       ECM(NUM)=EGCM*HQC
       QPICM(NUM)=QPI*HQC
       ELAB(NUM)=EGLAB*HQC
	 THGAMC(NUM)=THGC
       if (thgc .gt. thgcmax) thgcmax=thgc
	 if (thgc .lt. thgcmin) thgcmin=thgc
c       AMPL(NUM,1)= SVIR
c       AMPL(NUM,2)= Polx
c       AMPL(NUM,3)= Poly
c       AMPL(NUM,4)= Polz

       AMPL(NUM,1)= sig0
       AMPL(NUM,2)= 100*sige/sig0
       AMPL(NUM,3)= 100*sigx/sig0
       AMPL(NUM,4)= 100*sigy/sig0
       AMPL(NUM,5)= 100*sigz/sig0
       AMPL(NUM,6)= 100*sigex/sig0
       AMPL(NUM,7)= 100*sigey/sig0
       AMPL(NUM,8)= 100*sigez/sig0

      IF (NUM.GE.400) GO TO 18
 100  CONTINUE
 10   CONTINUE
  1   CONTINUE
 1000 CONTINUE
 18   CONTINUE

C ******* OUTPUT FOR THE DIFF. CR. SEC. AND TARGET POLARIZATION **********

1003  CONTINUE
      IF (IFRAME.EQ.1)WRITE (6,1251) VAR
      IF (IFRAME.EQ.2)WRITE (6,1252) VAR
      IF (IFRAME.EQ.3)WRITE (6,1253) VAR
      IF (IFRAME.EQ.4)WRITE (6,1254) VAR
      IF (IFRAME.EQ.5)WRITE (6,1255) VAR
 1251 FORMAT(/,4X,A6,4x,'SIG0',7X,'Ae',6x,'Ax',6X,'Ay',6X,'Az',
     &         5x,'Aex',5x,'Aey',5x,'Aez')
 1252 FORMAT(/,4X,A6,4x,'SIG0',7X,'Ae',5x,"Ax'",5X,"Ay'",5X,"Az'",
     &         4x,"Aex'",4x,"Aey'",4x,"Aez'")
 1253 FORMAT(/,4X,A6,4x,'SIG0',7X,'Ae',6x,'At',6X,'An',6X,'Al',
     &         5x,'Aet',5x,'Aen',5x,'Ael')
 1254 FORMAT(/,4X,A6,4x,'SIG0',7X,'Ae',6x,'A1',6X,'A2',6X,'A3',
     &         5x,'Ae1',5x,'Ae2',5x,'Ae3')
 1255 FORMAT(/,4X,A6,4x,'SIG0',7X,'Ae',6x,'Aa',6X,'Ab',6X,'Ac',
     &         5x,'Aea',5x,'Aeb',5x,'Aec')
c 1252 FORMAT(/,4X,A6,2x,'THG_CM',5X,'SIG0',8X,"PX'",7X,"PY'",7X,"PZ'")
c 1253 FORMAT(/,4X,A6,2x,'THG_CM',5X,'SIG0',8X,'Pt',8X,'Pn',8X,'Pl')
c 1254 FORMAT(/,4X,A6,2x,'THG_CM',5X,'SIG0',8X,'P1',8X,'P2',8X,'P3')
c 1255 FORMAT(/,4X,A6,2x,'THG_CM',5X,'SIG0',8X,'Pa',8X,'Pb',8X,'Pc')
       WRITE (6,1237) UNIT
1237  FORMAT(1X,A9,2x,'(mcb/sr)',7(4x,'(%)',1x))
      DO 1004 I=1,NUM
      IF  (IVAR.EQ.1)
     *  WRITE (6,1238) ARG(I),(DREAL(AMPL(I,J)),J=1,8)
1238  FORMAT(2X,F6.3,3x,F8.3,1x,7(1x,F7.2))
      IF  (IVAR.EQ.2)
     *  WRITE (6,1239) ARG(I),(DREAL(AMPL(I,J)),J=1,8)
1239  FORMAT(2X,F6.1,3x,F8.3,1x,7(1x,F7.2))
      IF  (IVAR.EQ.3)
     *  WRITE (6,1240) ARG(I),(DREAL(AMPL(I,J)),J=1,8)
1240  FORMAT(2X,F6.1,3x,F8.3,1x,7(1x,F7.2))
      IF  (IVAR.EQ.4)
     *  WRITE (6,1241) ARG(I),(DREAL(AMPL(I,J)),J=1,8)
1241  FORMAT(2X,F6.1,3x,F8.3,1x,7(1x,F7.2))
1004  CONTINUE
	IF (IFRAME.NE.4) WRITE (6,1261)
1261  FORMAT(/,2x,'*** Note: For parallel or antiparallel kinematics ',
     &   '(theta=180 or 0)',/,12x,
     &   'only systems {P1,P2,P3} or {Pa,Pb,Pc} should be used! ***')
	IF (IFRAME .EQ. 5) WRITE (6,1262) THGCMIN-THG,THGCMAX-THG
1262  FORMAT(/,2x,'Wigner rotation (thgamma_cm - thgamma): min=',
     &       F7.2,' (deg), max=',F6.2,' (deg)')

      GO TO 3000
c *************** Error messages **********************

2000  WRITE (6,2001)
2001  FORMAT(/,1X,
     * '******  W R O N G   K I N E M A T I C ! ******')
      GO TO 3000
2200  WRITE (6,2003) Q2GEVMAX,WTHR,WMEVMAX
2003  FORMAT(/,1X,'****** Q2 or W  out of limit (0-',F3.1,
     *         ' or ',F6.1,'-',F5.0,') ******')
      GO TO 3000
2300  WRITE (6,2004)
2004  FORMAT(/,1X,
     * '****** electron helicity h is wrong ! ******')
      GO TO 3000
2400  WRITE (6,2005)
2005  FORMAT(/,1X,
     * '****** virtual photon polarization eps is wrong ! ******')
3000  CONTINUE
      END
c

