#include "insane/xsections/epElasticDiffXSec.h"
#include "insane/base/PhysicalConstants.h"

namespace insane {
namespace physics {

epElasticDiffXSec::epElasticDiffXSec()
{
   fID            = 200000010;
   SetTitle("epElasticDiffXSec");
   SetPlotTitle("Elastic cross-section");
   fLabel         = "#frac{d#sigma}{d#Omega}";
   fUnits         = "nb/sr";
   fnDim          = 2;
   fPIDs.clear();
   fnParticles    = 2;
   fPIDs.push_back(11);
   fPIDs.push_back(2212);
}
//______________________________________________________________________________

epElasticDiffXSec::~epElasticDiffXSec(){
}
//______________________________________________________________________________

Double_t * epElasticDiffXSec::GetDependentVariables(const Double_t * x) 
   const
{
   //std::cout << "x[0] = " << x[0] << std::endl;
   //std::cout << "x[1] = " << x[1] << std::endl;
   //std::cout << "x[2] = " << x[2] << std::endl;
   //std::cout << "x[3] = " << x[3] << std::endl;
   //std::cout << "x[4] = " << x[4] << std::endl;
   //std::cout << "x[5] = " << x[5] << std::endl;
   TVector3 k1(0, 0, fBeamEnergy); // incident electron
   TVector3 k2(0, 0, 0);          // scattered electron
   double eprime =  GetEPrime(x[0]);
   k2.SetMagThetaPhi(eprime, x[0], x[1]);
   TVector3 p2 = k1 - k2; // recoil proton
   fDependentVariables[0] = eprime;
   fDependentVariables[1] = x[0];
   fDependentVariables[2] = x[1];
   fDependentVariables[3] = TMath::Sqrt(p2.Mag2()+0.938*0.938);
   fDependentVariables[4] = p2.Theta();
   fDependentVariables[5] = p2.Phi();
   //std::cout << "vectors" << std::endl;
   //std::cout << "fDependentVariables[0] = " << fDependentVariables[0] << std::endl;
   //std::cout << "fDependentVariables[1] = " << fDependentVariables[1] << std::endl;
   //std::cout << "fDependentVariables[2] = " << fDependentVariables[2] << std::endl;
   //std::cout << "fDependentVariables[3] = " << fDependentVariables[3] << std::endl;
   //std::cout << "fDependentVariables[4] = " << fDependentVariables[4] << std::endl;
   //std::cout << "fDependentVariables[5] = " << fDependentVariables[5] << std::endl;
   //k1.Print();
   //k2.Print();
   //p2.Print();
   if (p2.Phi() < 0.0) fDependentVariables[5] = p2.Phi() + 2.0 * TMath::Pi() ;
   return(fDependentVariables);
}
//______________________________________________________________________________
Double_t epElasticDiffXSec::GetEPrime(const Double_t theta)const  {
   // Returns the scattered electron energy using the angle. */
   Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
   return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M * TMath::Power(TMath::Sin(theta / 2.0), 2)));
}
//________________________________________________________________________
Double_t  epElasticDiffXSec::EvaluateXSec(const Double_t * x) const {
   if( GetZ() != 1 ) return 0.0;
   if( GetA() != 1 ) return 0.0;
   // Get the recoiling proton momentum
   if (!VariablesInPhaseSpace(6, x)) return(0.0);

   using namespace TMath;

   double Eprime   = x[0];
   double theta    = x[1] ;
   double M        = fTargetNucleus.GetMass();//M_p/GeV;

   double sinthetaOver2 = Sin(theta/2.0);
   double tanthetaOver2 = Tan(theta/2.0);
   double Qsquared      = 4.0*Eprime*GetBeamEnergy()*TMath::Power(sinthetaOver2*sinthetaOver2, 2.0);
   double tau           = Qsquared/(4.0*M*M);

   double mottXSec      = insane::Kine::Sig_Mott(GetBeamEnergy(),theta);
   double recoil_factor = Eprime/GetBeamEnergy();
   double GE2           = Power(fFormFactors->GEp(Qsquared), 2.0);
   double GM2           = Power(fFormFactors->GMp(Qsquared), 2.0);

   // Rosenbluth formula
   double res = mottXSec*recoil_factor*( (GE2+tau*GM2)/(1.0+tau) 
                 + 2.0*tau*GM2*tanthetaOver2*tanthetaOver2 );
   res = res * hbarc2_gev_nb;

   if( IncludeJacobian() ) return( TMath::Sin(theta)*res);
   return(res);
}
//______________________________________________________________________________


}}
