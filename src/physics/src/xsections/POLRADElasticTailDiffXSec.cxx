#include "insane/xsections/POLRADElasticTailDiffXSec.h"
namespace insane {
namespace physics {

POLRADElasticTailDiffXSec::POLRADElasticTailDiffXSec()
{
   fID         = 100010002;
   SetTitle("POLRADElasticTailDiffXSec");//,"POLRAD ERT cross-section");
   SetPlotTitle("POLRAD Elastic tail XSec");
   //fPOLRADCalc->DoQEFullCalc(false); 
   fPOLRADCalc->SetMultiPhoton(false); 
   fPOLRADCalc->fErr   = 1E-2;   // integration error tolerance 
   fPOLRADCalc->fDepth = 5;     // number of iterations for integration 
}
//______________________________________________________________________________

POLRADElasticTailDiffXSec::~POLRADElasticTailDiffXSec()
{ }
//______________________________________________________________________________

Double_t POLRADElasticTailDiffXSec::EvaluateXSec(const Double_t *x) const {

   if (!VariablesInPhaseSpace(fnDim, x)){
      //std::cout << "[POLRADElasticTailDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
      return(0.0);
   }
   Double_t Eprime  = x[0];
   Double_t theta   = x[1];
   Double_t Ebeam   = GetBeamEnergy();
   Double_t nu      = Ebeam-Eprime;
   // Double_t Mtarg   = (M_p/GeV)*fPOLRADCalc->fA;  
   Double_t Mtarg   = fPOLRADCalc->GetTargetMass();  
   fPOLRADCalc->SetKinematics(Ebeam,Eprime,theta);
   Double_t sig_rad  = fPOLRADCalc->ERT();
   // converts from dsigma/dxdy to dsigma/dEdOmega
   sig_rad = sig_rad * (Eprime/(2.0*pi*Mtarg*nu));
   if( TMath::IsNaN(sig_rad) ) sig_rad = 0.0;
   sig_rad = fPOLRADCalc->fA*sig_rad*hbarc2_gev_nb;
   //std::cout << "sig_rad = " << sig_rad << std::endl;
   if( IncludeJacobian() ) sig_rad = sig_rad*TMath::Sin(theta);
   return sig_rad;
} 
//______________________________________________________________________________
}}
