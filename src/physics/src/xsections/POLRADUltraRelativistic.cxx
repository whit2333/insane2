#include "insane/xsections/POLRADUltraRelativistic.h"
namespace insane {
namespace physics {

   
//_____________________________________________________________________________
POLRADUltraRelativistic::POLRADUltraRelativistic(){
   fErr = 1.0e-8;
   fDepth = 10;
}
//_____________________________________________________________________________
POLRADUltraRelativistic::~POLRADUltraRelativistic(){
}
//______________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_in(){
   Double_t res = (fine_structure_const/pi)*Delta_1_in()*sig_dis_born(fKinematics.GetS(),fKinematics.GetX(),fKinematics.GetQ2());
   if( TMath::IsNaN(res) ) res = 0.0;
   std::cout << "term0   : " << res << std::endl;

   Double_t res2 = sig_V_r();
   std::cout << "sig_V_r   : " << res2 << std::endl;
   if( TMath::IsNaN(res2) ) res2 = 0.0;
   res += res2;

   res2 = sig_s_in();
   std::cout << "sig_s_in   : " << res2 << std::endl;
   if( TMath::IsNaN(res2) ) res2 = 0.0;
   res += res2;

   res2 = sig_p_in();
   std::cout << "sig_p_in   : " << res2 << std::endl;
   if( TMath::IsNaN(res2) ) res2 = 0.0;
   res += res2;

   //res2 = sig_r_in_long();
   res2 = sig_r_in_long_2D();
   std::cout << "sig_r_in_long   : " << res2 << std::endl;
   if( TMath::IsNaN(res2) ) res2 = 0.0;
   res += res2;

   return(res);
}
//______________________________________________________________________________
Double_t POLRADUltraRelativistic::Delta_1_in(){
   // Sec. 2.1.3, Eq. 25 
   // Ultrarelativisitic approx. 
   Double_t T1    = (fKinematics.Getl_m() - 1.0)*Delta_sp();
   Double_t T2    = -1.0*TMath::Power(TMath::Log(1.0-fKinematics.Gety()),2.0);
   Double_t T3    = -1.;
   Double_t delta = 0.5*(T1 + T2 + T3);
   //std::cout << "T1 : " << T1 << std::endl;
   //std::cout << "T2 : " << T2 << std::endl;
   //std::cout << "T3 : " << T3 << std::endl;

   return delta;
}
//______________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_V_r() {
   // Sec. 2.1.3, Eq. 27
   // Correction due to vacuum polarization effects by leptons and hadrons
   // Ultrarelativisitic approx. (?) 
   Double_t sig_v_r = (fine_structure_const/pi)*( Delta_vac_l() + Delta_vac_h() )*sig_dis_born(fKinematics.GetS(),fKinematics.GetX(),fKinematics.GetQ2());
   return sig_v_r;
}
//______________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_s_in() {
   Double_t A        = fine_structure_const/(2.*pi);
   Double_t upper    = 0.9999999; // singularity at 1
   Double_t sig_s_in = A*AdaptiveSimpson(&POLRADUltraRelativistic::sig_s_in_Integrand,fKinematics.Getz_s(),upper,fErr,fDepth);
   return sig_s_in;

}
//______________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_s_in_Integrand(Double_t z) {
   Double_t T1  = ( (1.0+z*z)*fKinematics.Getl_m() - 2.0*z)*sig_s(z);
   Double_t T2  = -2.*(fKinematics.Getl_m() - 1.)*sig_dis_born(fKinematics.GetS(),fKinematics.GetX(),fKinematics.GetQ2());
   //std::cout << "T1 : " << T1 << std::endl;
   //std::cout << "T2 : " << T2 << std::endl;
   //std::cout << "z  : " <<  z << std::endl;
   Double_t arg = (T1 + T2)/(1.-z);
   return arg;
}
//______________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_s(Double_t z) {

   Double_t N   = fKinematics.Gety()/(z - 1. + fKinematics.Gety());
   Double_t sig = N*sig_dis_born(z*fKinematics.GetS(),fKinematics.GetX(),z*fKinematics.GetQ2());
   return sig;

}
//______________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_p_in() {

   Double_t alpha    = fine_structure_const;
   Double_t A        = alpha/(2.*pi);
   Double_t upper    = 0.9999999; // singularity at 1
   Double_t sig_p_in = A*AdaptiveSimpson(&POLRADUltraRelativistic::sig_p_in_Integrand,fKinematics.Getz_p(),upper,fErr,fDepth);
   return sig_p_in;

}
//______________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_p_in_Integrand(Double_t z) {

   Double_t T1  = ( (1.+z*z)*fKinematics.Getl_m() - 2.*z)*sig_p(z);
   Double_t T2  = -2.*(fKinematics.Getl_m() - 1.)*sig_dis_born(fKinematics.GetS(),fKinematics.GetX(),fKinematics.GetQ2());
   Double_t arg = (T1 + T2)/(1.-z);
   return arg;
}
//______________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_p(Double_t z) {

   Double_t N   = fKinematics.Gety()/( z*(z - 1. + fKinematics.Gety()) );
   Double_t sig = N*sig_dis_born(fKinematics.GetS(),fKinematics.GetX()/z,fKinematics.GetQ2()/z);
   return sig;

}
//______________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_r_in_tran() {

   // Eq. 31 
   // Ultrarelativistic approx. 
   // Transversely polarized target 

   Double_t alpha = fine_structure_const;
   Double_t N     = 2.*alpha*alpha*alpha*fKinematics.Gety();
   Double_t sig   = N*AdaptiveSimpson(&POLRADUltraRelativistic::sig_r_in_tran_Integrand,fKinematics.Getx(),1.,fErr,fDepth);
   return sig;

}
//______________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_r_in_tran_Integrand(Double_t xi) {

   Double_t arg = T_u_L(xi) + fP_L*fP_N*( T_pPerp_L(xi) + T_pPerp_x(xi) ) + (fQ_N/6.)*T_q_L(xi);
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_u_p() {
   // Eq. 38 
   // Elastic radiative tail 
   // unpolarized, proton target 
   // Ultrarelativistic approx. 
   Double_t eta_min = fKinematics.Getx()*fKinematics.Getx()/(4.0*(1.0-fKinematics.Getx()));
   Double_t INFTY = 9999999;         // FIXME: How to define infinity??
   Double_t alpha = fine_structure_const;
   Double_t oneMinusy = 1.0 - fKinematics.Gety();
   Double_t Y_plus = (1.0 + TMath::Power(oneMinusy,2.0))/oneMinusy;
   Double_t A     = alpha*alpha*alpha*Y_plus/fKinematics.GetS();
   Double_t arg   = A*AdaptiveSimpson(&POLRADUltraRelativistic::sig_u_p_Integrand,eta_min,INFTY,fErr,fDepth);
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_u_p_Integrand(Double_t eta_A) {

   // Eq. 38 
   // Elastic radiative tail 
   // unpolarized proton target 
   // Ultrarelativistic approx. 
   //ProcessUltraRelCoeffs(eta_A);
   Double_t GE    = fFFTs->GEp(fKinematics.GetQ2());
   Double_t GM    = fFFTs->GMp(fKinematics.GetQ2());
   Double_t F1    = (GE + eta_A*GM)/(1. + eta_A);
   Double_t F2    = (GE - GM)/(1.+ eta_A);
   Double_t fX_1         = fKinematics.Getx()*fKinematics.Getx() + 4.*fKinematics.Getx()*eta_A - 4.*eta_A;
   Double_t fXTilde      = fX_1/(2.*eta_A*fKinematics.Getx()*fKinematics.Getx());
   Double_t arg   = (1./eta_A)*( fXTilde*(F1*F1 + eta_A*F2*F2) - TMath::Power(F1+F2,2.0) );
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_p_p_long() {

   // Eq. 38 
   // Elastic radiative tail 
   // longitudinally polarized proton target 
   // Ultrarelativistic approx. 

   Double_t eta_min = fKinematics.Getx()*fKinematics.Getx()/(4.0*(1.0-fKinematics.Getx()));
   Double_t INFTY = 0;         // FIXME: How to define infinity??
   Double_t alpha = fine_structure_const;
   Double_t oneMinusy = 1.0 - fKinematics.Gety();
   Double_t Y_minus = (1.0 - TMath::Power(oneMinusy,2.0))/oneMinusy;
   Double_t A     = alpha*alpha*alpha*Y_minus/fKinematics.GetS();
   Double_t arg   = A*AdaptiveSimpson(&POLRADUltraRelativistic::sig_p_p_long_Integrand,eta_min,INFTY,fErr,fDepth);
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_p_p_long_Integrand(Double_t eta_A) {

   // Eq. 38 
   // Elastic radiative tail 
   // longitudinally polarized proton target 
   // Ultrarelativistic approx.
   //ProcessUltraRelCoeffs(eta_A);
   Double_t GE    = fFFTs->GEp(fKinematics.GetQ2());
   Double_t GM    = fFFTs->GMp(fKinematics.GetQ2());
   Double_t F1    = (GE + eta_A*GM)/(1. + eta_A);
   Double_t F2    = (GE - GM)/(1.+ eta_A);
   Double_t fX_1         = fKinematics.Getx()*fKinematics.Getx() + 4.*fKinematics.Getx()*eta_A - 4.*eta_A;
   Double_t fXTilde      = fX_1/(2.*eta_A*fKinematics.Getx()*fKinematics.Getx());
   Double_t arg   = (1./eta_A)*(F1 + F2)*(fKinematics.Getx()*fXTilde*F1 - (F1 + F2) );
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_p_p_tran() {

   // Eq. 41 
   // Elastic radiative tail 
   // transversely polarized proton target 
   // Ultrarelativistic approx. 

   Double_t eta_min = fKinematics.Getx()*fKinematics.Getx()/(4.0*(1.0-fKinematics.Getx()));
   Double_t INFTY = 9999;         // FIXME: How to define infinity??
   Double_t alpha = fine_structure_const;
   Double_t num   = alpha*alpha*alpha*fKinematics.Getx()*fKinematics.Gety()*fKinematics.Gety();
   Double_t den   = fKinematics.GetS()*TMath::Power(1.-fKinematics.Gety(),3./2.);
   Double_t A     = (num/den)*(fKinematics.GetM()/TMath::Sqrt(fKinematics.GetQ2()));
   Double_t arg   = A*AdaptiveSimpson(&POLRADUltraRelativistic::sig_p_p_tran_Integrand,eta_min,INFTY,fErr,fDepth);
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_p_p_tran_Integrand(Double_t eta_A) {

   // Eq. 41 
   // Elastic radiative tail 
   // transversely polarized proton target 
   // Ultrarelativistic approx. 
   //ProcessUltraRelCoeffs(eta_A);
   Double_t GE      = fFFTs->GEp(fKinematics.GetQ2());
   Double_t GM      = fFFTs->GMp(fKinematics.GetQ2());
   Double_t F1      = (GE + eta_A*GM)/(1. + eta_A);
   Double_t F2      = (GE - GM)/(1.+ eta_A);
   Double_t sG1p    = TMath::Power(F1+F2,2.0);
   Double_t sG2p    = F2*(F1 + F2);
   Double_t y1Tilde = (1./fKinematics.Gety()) - 1.;
   Double_t fX_1         = fKinematics.Getx()*fKinematics.Getx() + 4.*fKinematics.Getx()*eta_A - 4.*eta_A;
   Double_t fXTilde      = fX_1/(2.*eta_A*fKinematics.Getx()*fKinematics.Getx());
   Double_t arg     = (1./eta_A)*( sG1p*(y1Tilde*( 2. + fKinematics.Getx()/(fKinematics.Gety()*eta_A) ) - 
                                  (2.-fKinematics.Gety())*fKinematics.Getx()*fXTilde/fKinematics.Gety() )
         + sG2p*( (fKinematics.Getx() + 2.*eta_A - 2.*fKinematics.Getx()*y1Tilde*y1Tilde + 6.*eta_A*y1Tilde/fKinematics.Gety())*fXTilde
            - (4.*y1Tilde/(fKinematics.Getx()*fKinematics.Gety()))*(1.+eta_A) ) );
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_u_d() {

   // Eq. 38 
   // Elastic radiative tail 
   // unpolarized deuteron target 
   // Ultrarelativistic approx. 

   Double_t eta_min = fKinematics.Getx()*fKinematics.Getx()/(4.0*(1.0-fKinematics.Getx()));
   Double_t INFTY = 9999;         // FIXME: How to define infinity??
   Double_t alpha = fine_structure_const;
   Double_t oneMinusy = 1.0 - fKinematics.Gety();
   Double_t Y_plus = (1.0 + TMath::Power(oneMinusy,2.0))/oneMinusy;
   Double_t A     = alpha*alpha*alpha*Y_plus/fKinematics.GetS();
   Double_t arg   = A*AdaptiveSimpson(&POLRADUltraRelativistic::sig_u_d_Integrand,eta_min,INFTY,fErr,fDepth);
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_u_d_Integrand(Double_t eta_A) {

   // Eq. 38 
   // Elastic radiative tail 
   // unpolarized proton target 
   // Ultrarelativistic approx. 
   //ProcessUltraRelCoeffs(eta_A);
   Double_t Fc  = fFFs->GEd(fKinematics.GetQ2());
   Double_t Fm  = fFFs->GMd(fKinematics.GetQ2());
   Double_t Fq  = 0;               // FIXME: quadrupole form factor needs to be evaluated! 
   Double_t y1Tilde = (1./fKinematics.Gety()) - 1.;
   Double_t fX_1         = fKinematics.Getx()*fKinematics.Getx() + 4.*fKinematics.Getx()*eta_A - 4.*eta_A;
   Double_t fXTilde      = fX_1/(2.*eta_A*fKinematics.Getx()*fKinematics.Getx());
   Double_t arg = (1./eta_A)*( (Fc*Fc + (8./9.)*Fq*Fq*eta_A)*fXTilde - (2./3.)*(1. + eta_A)*Fm*Fm );
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_p_d_long() {

   // Eq. 38 
   // Elastic radiative tail 
   // longitudinally polarized deuteron target 
   // Ultrarelativistic approx. 

   Double_t eta_min = fKinematics.Getx()*fKinematics.Getx()/(4.0*(1.0-fKinematics.Getx()));
   Double_t INFTY = 9999;         // FIXME: How to define infinity??
   Double_t alpha = fine_structure_const;
   Double_t oneMinusy = 1.0 - fKinematics.Gety();
   Double_t Y_minus = (1.0 - TMath::Power(oneMinusy,2.0))/oneMinusy;
   Double_t A     = alpha*alpha*alpha*Y_minus/fKinematics.GetS();
   Double_t arg   = A*AdaptiveSimpson(&POLRADUltraRelativistic::sig_p_d_long_Integrand,eta_min,INFTY,fErr,fDepth);
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_p_d_long_Integrand(Double_t eta_A) {

   // Eq. 38 
   // Elastic radiative tail 
   // longitudinally polarized deuteron target 
   // Ultrarelativistic approx. 
   // FIXME: are Fc and Fm evaluated properly? 
   //ProcessUltraRelCoeffs(eta_A);
   Double_t Fc  = fFFs->GEd(fKinematics.GetQ2());
   Double_t Fm  = fFFs->GMd(fKinematics.GetQ2());
   Double_t Fq  = 0;               // FIXME: quadrupole form factor needs to be evaluated! 
   Double_t y1Tilde = (1./fKinematics.Gety()) - 1.;
   Double_t fX_1         = fKinematics.Getx()*fKinematics.Getx() + 4.*fKinematics.Getx()*eta_A - 4.*eta_A;
   Double_t fXTilde      = fX_1/(2.*eta_A*fKinematics.Getx()*fKinematics.Getx());
   Double_t arg = (1./eta_A)*( 0.5*(1.+eta_A)*Fm - (Fc + (1./3.)*Fq*eta_A + 0.5*Fm*eta_A)*fKinematics.Getx()*fXTilde );
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_p_d_tran() {

   // Eq. 41 
   // Elastic radiative tail 
   // transversely polarized proton target 
   // Ultrarelativistic approx. 

   Double_t eta_min = fKinematics.Getx()*fKinematics.Getx()/(4.0*(1.0-fKinematics.Getx()));
   Double_t INFTY = 9999;         // FIXME: How to define infinity??
   Double_t alpha = fine_structure_const;
   Double_t num   = alpha*alpha*alpha*fKinematics.Getx()*fKinematics.Gety()*fKinematics.Gety();
   Double_t den   = fKinematics.GetS()*TMath::Power(1.-fKinematics.Gety(),3./2.);
   Double_t A     = (num/den)*(fKinematics.GetM()/TMath::Sqrt(fKinematics.GetQ2()));
   Double_t arg   = A*AdaptiveSimpson(&POLRADUltraRelativistic::sig_p_d_tran_Integrand,eta_min,INFTY,fErr,fDepth);
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_p_d_tran_Integrand(Double_t eta_A) {

   // Eq. 41 
   // Elastic radiative tail 
   // transversely polarized proton target 
   // Ultrarelativistic approx. 
   //ProcessUltraRelCoeffs(eta_A);
   Double_t Fc      = fFFs->GEd(fKinematics.GetQ2());
   Double_t Fm      = fFFs->GMd(fKinematics.GetQ2());
   Double_t Fq      = 0;                   // FIXME: quadrupole form factor needs to be evaluated! 
   Double_t sG1d    = -0.5*(1.+eta_A)*Fm*Fm;
   Double_t sG2d    = Fm*(Fc + (1./3.)*eta_A*Fq - 0.5*Fm);
   Double_t y1Tilde = (1./fKinematics.Gety()) - 1.;
   Double_t fX_1         = fKinematics.Getx()*fKinematics.Getx() + 4.*fKinematics.Getx()*eta_A - 4.*eta_A;
   Double_t fXTilde      = fX_1/(2.*eta_A*fKinematics.Getx()*fKinematics.Getx());
   Double_t arg     = (1./eta_A)*( sG1d*(y1Tilde*( 2. + fKinematics.Getx()/(fKinematics.Gety()*eta_A) ) - (2.-fKinematics.Gety())*fKinematics.Getx()*fXTilde/fKinematics.Gety() )
         + sG2d*( (fKinematics.Getx() + 2.*eta_A - 2.*fKinematics.Getx()*y1Tilde*y1Tilde + 6.*eta_A*y1Tilde/fKinematics.Gety())*fXTilde
            - (4.*y1Tilde/(fKinematics.Getx()*fKinematics.Gety()))*(1.+eta_A) ) );
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_q_d_long() {

   // Eq. 38 
   // Elastic radiative tail 
   // quadrupolarized deuteron target 
   // Ultrarelativistic approx. 

   Double_t eta_min = fKinematics.Getx()*fKinematics.Getx()/(4.0*(1.0-fKinematics.Getx()));
   Double_t INFTY = 9999;         // FIXME: How to define infinity??
   Double_t alpha = fine_structure_const;
   Double_t A     = alpha*alpha*alpha*fY_plus/fKinematics.GetS();
   Double_t arg   = A*AdaptiveSimpson(&POLRADUltraRelativistic::sig_q_d_long_Integrand,eta_min,INFTY,fErr,fDepth);
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_q_d_long_Integrand(Double_t eta_A) {

   // Eq. 38 
   // Elastic radiative tail 
   // quadrupolarized deuteron target 
   // Ultrarelativistic approx. 
   //ProcessUltraRelCoeffs(eta_A);
   Double_t Fc  = fFFs->GEd(fKinematics.GetQ2());
   Double_t Fm  = fFFs->GMd(fKinematics.GetQ2());
   Double_t Fq  = 0;               // FIXME: quadrupole form factor needs to be evaluated! 
   Double_t fX_1         = fKinematics.Getx()*fKinematics.Getx() + 4.*fKinematics.Getx()*eta_A - 4.*eta_A;
   Double_t fXTilde      = fX_1/(2.*eta_A*fKinematics.Getx()*fKinematics.Getx());
   Double_t arg = (1./eta_A)*( (1. + eta_A + (0.75*fKinematics.Getx()*fKinematics.Getx() - eta_A)*fXTilde)*Fm*Fm
         - (fXTilde*fX_1/(1.+eta_A))*Fq*(3.*Fc + 3.*eta_A*Fm + eta_A*Fq)
         - 2.*eta_A*fXTilde*Fq*(4.*Fc - 3.*fKinematics.Getx()*Fm + (4./3.)*eta_A*Fq) );
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_q_d_tran() {

   // Eq. 43 
   // Elastic radiative tail 
   // quadrupolarized deuteron target 
   // Ultrarelativistic approx. 
   return -0.5*sig_q_d_long();

}
//_____________________________________________________________________________
void POLRADUltraRelativistic::ProcessUltraRelCoeffs(Double_t eta_A) {

   fX_1       = fKinematics.Getx()*fKinematics.Getx() + 4.*fKinematics.Getx()*eta_A - 4.*eta_A; 
   fXTilde    = fX_1/(2.*eta_A*fKinematics.Getx()*fKinematics.Getx()); 
   fY_plus    = (1. + TMath::Power(1-fKinematics.Gety(),2.0))/(1.-fKinematics.Gety());  
   fY_minus   = (1. - TMath::Power(1-fKinematics.Gety(),2.0))/(1.-fKinematics.Gety());
   fEta_min   = fKinematics.Getx()*fKinematics.Getx()/( 4.*(1.-fKinematics.Getx()) );  
}
//______________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_r_in_long() {

   // Eq. 31 
   // Ultrarelativistic approx. 
   // Longitudinally polarized target 

   Double_t alpha = fine_structure_const;
   Double_t N     = 2.*alpha*alpha*alpha*fKinematics.Gety();
   Double_t upper = 0.99999999; 
   Double_t sig   = N*AdaptiveSimpson(&POLRADUltraRelativistic::sig_r_in_long_Integrand_1D,fKinematics.Getx(),upper,fErr,fDepth);
   return sig;

}
//______________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_r_in_long_2D() {

   // Eq. 31 
   // Ultrarelativistic approx. 
   // Longitudinally polarized target 

   Double_t alpha = fine_structure_const;
   Double_t N     = 2.*alpha*alpha*alpha*fKinematics.Gety();
   Double_t upper = 0.99999999; 
   Double_t min[2] = {fKinematics.Getx(),0.0};
   Double_t max[2] = {upper,1.0};

   //std::cout << "ft_1 : " << ft_1 << std::endl;
   //std::cout << "ft_2 : " << ft_2 << std::endl;
   // 1D integration part
   Double_t sig   = AdaptiveSimpson(&POLRADUltraRelativistic::sig_r_in_long_Integrand_2D_1,fKinematics.Getx(),upper,fErr,fDepth);
   std::cout << "sig1 : " << sig << std::endl;

   // 2D integration part
   ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kMISER;
   double absErr                           = 1.0e-4;   /// desired absolute Error 
   double relErr                           = 1.0e-4;   /// desired relative Error 
   unsigned int ncalls                     = 1.0e3;    /// number of calls (MC only)
   int rule                                = 1;        /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    

   SIG_r_2D_integrand funcWrap;
   funcWrap.fPOLRAD = this;

   ROOT::Math::IntegratorMultiDim ig2(type,absErr,relErr,ncalls); 
   ig2.SetFunction(funcWrap);
   Double_t sig2 = ig2.Integral(min,max);
   std::cout << "sig2 : " << sig2 << std::endl;

   return(N*(sig + sig2) );

}
//______________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_r_in_long_Integrand(Double_t xi) {
   /// \deprecated
   Double_t arg = T_u_L(xi) + fP_L*fP_N*( T_pPara_L(xi) + T_pPara_x(xi) ) + (fQ_N/3.)*T_q_L(xi);
   return arg;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_r_in_long_Integrand_1D(Double_t xi){

   // 1D integrand part of Eq 31.
   ProcessTFuncKin(xi); 
   Double_t res = T_u_L_a(xi) + fP_L*fP_N*( T_pPara_L_a(xi) + T_pPara_x_a(xi) ) + (fQ_N/3.)*T_q_L_a(xi);
   fxi_int = xi;
   //fxi = xi;
   Double_t t_integral  = AdaptiveSimpson(&POLRADUltraRelativistic::sig_r_in_long_Integrand_1D_2,ft_1,ft_2,fErr,fDepth);
   //std::cout << " res       : " << res << std::endl;
   //std::cout << "t_integral : " << t_integral << std::endl;
   if( TMath::IsNaN( res) ) res = 0.0;
   if( TMath::IsNaN( t_integral) ) t_integral = 0.0;

   return( (res + t_integral)/(xi*xi) );

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_r_in_long_Integrand_1D_2( Double_t t){
   Double_t xi = fxi_int;
   // 2D integrand part of Eq 31.
   ProcessTFuncKin(xi,t); 
   Double_t res =   T_u_L_b(xi,t)     + fP_L*fP_N*( T_pPara_L_b(xi,t) 
                  /*+ T_pPara_x_b(xi,t)*/ ) + (fQ_N/3.)*T_q_L_b(xi,t);
   return(res);
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_r_in_long_Integrand_2D(Double_t xi, Double_t t){
   // 2D integrand part of Eq 31.
   ProcessTFuncKin(xi,t); 
   if(t<ft_1) return(0.0);
   if(t>ft_2) return(0.0);
   Double_t res =   T_u_L_b(xi,t)     + fP_L*fP_N*( T_pPara_L_b(xi,t) 
                  /*+ T_pPara_x_b(xi,t)*/ ) + (fQ_N/3.)*T_q_L_b(xi,t);
   return(res);
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sig_r_in_long_Integrand_2D_1(Double_t xi){

   // 1D integrand part of Eq 31.
   ProcessTFuncKin(xi); 
   Double_t res = T_u_L_a(xi) + fP_L*fP_N*( T_pPara_L_a(xi) + T_pPara_x_a(xi) ) + (fQ_N/3.)*T_q_L_a(xi);
   //std::cout << " res       : " << res << std::endl;
   //std::cout << "t_integral : " << t_integral << std::endl;
   if( TMath::IsNaN( res) ) res = 0.0;

   return( (res)/(xi*xi) );

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_u_L(Double_t xi) {
   /// \deprecated
   fT_u_L     = sL(1,0,xi) + ( 1./(xi*fQ2_xi*fQ2_xi) )*sL(2,1,xi);  
   return fT_u_L; 
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_u_L_a(Double_t xi) {
   return sL_1_a(0,xi) + ( 1./(xi*fQ2_xi*fQ2_xi) )*sL_2_a(1,xi);  
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_u_L_b(Double_t xi,Double_t t) {
   return sL_1_b(0,xi,t) + ( 1./(xi*fQ2_xi*fQ2_xi) )*sL_2_b(1,xi,t);  
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_pPara_L(Double_t xi) {
   /// \deprecated
   fT_pPara_L = (-1.0/fQ2_xi)*sL(3,2,xi); 
   return fT_pPara_L; 
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_pPara_L_a(Double_t xi) {
   return (-1.0/fQ2_xi)*sL_3_a(2,xi); 
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_pPara_L_b(Double_t xi,Double_t t) {
   return (-1.0/fQ2_xi)*sL_3_b(2,xi,t); 
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_pPara_x(Double_t xi) {
   /// \deprecated
   Double_t g1=0; 
   if(fPOLRADTarget==0) g1 = fPolSFs->g1n(xi,ft_x);  
   if(fPOLRADTarget==1) g1 = fPolSFs->g1p(xi,ft_x);  
   if(fPOLRADTarget==2) g1 = fPolSFs->g1d(xi,ft_x);  
   if(fPOLRADTarget==3) g1 = fPolSFs->g1He3(xi,ft_x);  
   fT_pPara_x = 2.*fu*(fKinematics.GetS() + fu_x)*g1/(fu_x*fKinematics.GetX()*fQ2_xi);       
   return fT_pPara_x; 
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_pPara_x_a(Double_t xi) {
   Double_t g1 = fSFs_x[2]; 
   return 2.*fu*(fKinematics.GetS() + fu_x)*g1/(fu_x*fKinematics.GetX()*fQ2_xi);       
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_q_L(Double_t xi) {
   /// \deprecated
   fT_q_L = sL(1,4,xi) + ( 1./(xi*fQ2_xi*fQ2_xi) )*sL(2,5,xi);
   return fT_q_L;

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_q_L_a(Double_t xi) {
   return sL_1_a(4,xi) + ( 1./(xi*fQ2_xi*fQ2_xi) )*sL_2_a(5,xi);
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_q_L_b(Double_t xi,Double_t t) {
   return sL_1_b(4,xi,t) + ( 1./(xi*fQ2_xi*fQ2_xi) )*sL_2_b(5,xi,t);
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_pPerp_L(Double_t xi) {
   /// \deprecated
   fT_pPerp_L = (xi*fKinematics.GetM()/TMath::Sqrt(fKinematics.GetS()*fKinematics.GetX()*fKinematics.GetQ2()))*( sL(4,2,xi) + (2./fQ2_xi)*sL(5,3,xi) ); 
   return fT_pPerp_L; 
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_pPerp_L_a(Double_t xi) {
   return (xi*fKinematics.GetM()/TMath::Sqrt(fKinematics.GetS()*fKinematics.GetX()*fKinematics.GetQ2()))*( sL_4_a(2,xi) + (2./fQ2_xi)*sL_5_a(3,xi) ); 
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_pPerp_L_b(Double_t xi,Double_t t) {
   return (xi*fKinematics.GetM()/TMath::Sqrt(fKinematics.GetS()*fKinematics.GetX()*fKinematics.GetQ2()))*( sL_4_b(2,xi,t) + (2./fQ2_xi)*sL_5_b(3,xi,t) ); 
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_pPerp_x(Double_t xi) {
   /// \deprecated
   Double_t g1=0,g2=0; 
   if(fPOLRADTarget==0){
      g1 = fPolSFs->g1n(xi,ft_x);  
      g2 = fPolSFs->g2n(xi,ft_x); 
   }else if(fPOLRADTarget==1){
      g1 = fPolSFs->g1p(xi,ft_x);  
      g2 = fPolSFs->g2p(xi,ft_x);  
   }else if(fPOLRADTarget==2){
      g1 = fPolSFs->g1d(xi,ft_x);  
      g2 = fPolSFs->g2d(xi,ft_x);
   }else if(fPOLRADTarget==3){
      g1 = fPolSFs->g1He3(xi,ft_x);  
      g2 = fPolSFs->g2He3(xi,ft_x);
   }
   fT_pPerp_x = ( 4.*xi*fKinematics.GetM()*fu/(fKinematics.GetX()*fQ2_xi*TMath::Sqrt(fKinematics.GetS()*fKinematics.GetX()*fKinematics.GetQ2())) )*(fQ2_xi*g1 + 2.*fKinematics.GetS()*g2 );   
   return fT_pPerp_x; 
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::T_pPerp_x_a(Double_t xi) {
   Double_t g1 = fSFs_x[2];
   Double_t g2 = fSFs_x[3]; 
   return ( 4.*xi*fKinematics.GetM()*fu/(fKinematics.GetX()*fQ2_xi*TMath::Sqrt(fKinematics.GetS()*fKinematics.GetX()*fKinematics.GetQ2())) )*(fQ2_xi*g1 + 2.*fKinematics.GetS()*g2 );   
}
//_____________________________________________________________________________
void POLRADUltraRelativistic::ProcessTFuncKin(Double_t xi) {

   // Here, we calculate the T function kinematics (Eq. 33)  
   // xi = x  (integration variable) 
   fQ2_xi = fKinematics.GetQ2()/xi;
   fu     = fKinematics.GetS_x() - fQ2_xi; 
   fu_x   = fKinematics.GetS() - fQ2_xi; 
   fu_s   = fKinematics.GetX() + fQ2_xi; 
   fTT    = fKinematics.GetS()*fKinematics.GetS() + fKinematics.GetX()*fKinematics.GetX(); 
   ft_s   = fKinematics.GetQ2()*fKinematics.GetS()/fu_s; 
   ft_x   = fKinematics.GetQ2()*fKinematics.GetX()/fu_x;         
   Double_t den  = 2.*( (fu/xi) + fKinematics.GetM()*fKinematics.GetM());
   ft_1 = ( fu*(fKinematics.GetS_x() - TMath::Sqrt(fKinematics.GetLambda_Q()) ) + 2.*fKinematics.GetM()*fKinematics.GetM()*fKinematics.GetQ2())/den;
   ft_2 = ( fu*(fKinematics.GetS_x() + TMath::Sqrt(fKinematics.GetLambda_Q()) ) + 2.*fKinematics.GetM()*fKinematics.GetM()*fKinematics.GetQ2())/den;

   // Structure functions evaluated at (xi,t_s)
   POLRAD::ProcessStructureFunctions(xi,ft_s);
   fSFs_s[0] = fF1;
   fSFs_s[1] = fF2;
   fSFs_s[2] = fg1;
   fSFs_s[3] = fg2;
   fSFs_s[4] = fb1;
   fSFs_s[5] = fb2;
   fSFs_s[6] = fb3;
   fSFs_s[7] = fb4;

   // Structure functions evaluated at (xi,t_x)
   POLRAD::ProcessStructureFunctions(xi,ft_x);
   fSFs_x[0] = fF1;
   fSFs_x[1] = fF2;
   fSFs_x[2] = fg1;
   fSFs_x[3] = fg2;
   fSFs_x[4] = fb1;
   fSFs_x[5] = fb2;
   fSFs_x[6] = fb3;
   fSFs_x[7] = fb4;

}
//_____________________________________________________________________________
void POLRADUltraRelativistic::ProcessTFuncKin(Double_t xi,Double_t t) {
   ProcessTFuncKin(xi);
   // Structure functions evaluated at (xi,t_x)
   POLRAD::ProcessStructureFunctions(xi,t);
   fSFs_t[0] = fF1;
   fSFs_t[1] = fF2;
   fSFs_t[2] = fg1;
   fSFs_t[3] = fg2;
   fSFs_t[4] = fb1;
   fSFs_t[5] = fb2;
   fSFs_t[6] = fb3;
   fSFs_t[7] = fb4;
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sL_1_a(Int_t i, Double_t xi){
   // Sript_L_1 (Eq.34) without t integral term
   Double_t ans = 
      /*2.*L_Y(SFType,xi)*/ - L_s_k_a(i,xi) + L_s_l_a(i,xi) + L_x_k_a(i,xi) 
            - L_x_l_a(i,xi) /*+ 2.*L_t(i,xi,t)*/;  

   return(ans);
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sL_1_b(Int_t i, Double_t xi, Double_t t){
   // Sript_L_1 (Eq.34) with t integral term
   Double_t ans = 
      2.*L_Y(i,xi,t) - L_s_k_b(i,xi,t) + L_s_l_b(i,xi,t) + L_x_k_b(i,xi,t) 
            - L_x_l_b(i,xi,t) + 2.*L_t(i,xi,t);  

   return(ans);
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sL_2_a(Int_t i, Double_t xi){
   // Sript_L_2 (Eq.34) without t integral term
   Double_t ans = /*(fTT - fQ2_xi*fKinematics.GetS_x())*L_Y(i,xi,t)*/
                  - (fQ2_xi*fKinematics.GetX() + fTT)*L_s_k_a(i,xi) 
                  + (fTT - fQ2_xi*fKinematics.GetS())*L_x_k_a(i,xi);  

   return(ans);
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sL_2_b(Int_t i, Double_t xi, Double_t t){

   // Sript_L_2 (Eq.34) with t integral term
   
   //ProcessTFuncKin(xi); 

   // integration limits (for L functions below) 
   // Should go somewhere else!
   //Double_t den  = 2.*( (fu/xi) + fKinematics.GetM()*fKinematics.GetM());
   //ft_1 = ( fu*(fKinematics.GetS_x() - TMath::Sqrt(fKinematics.GetLambda_Q()) ) + 2.*fKinematics.GetM()*fKinematics.GetM()*fKinematics.GetQ2())/den;
   //ft_2 = ( fu*(fKinematics.GetS_x() + TMath::Sqrt(fKinematics.GetLambda_Q()) ) + 2.*fKinematics.GetM()*fKinematics.GetM()*fKinematics.GetQ2())/den;

   Double_t ans = (fTT - fQ2_xi*fKinematics.GetS_x())*L_Y(i,xi,t)
                  - (fQ2_xi*fKinematics.GetX() + fTT)*L_s_k_b(i,xi,t) 
                  + (fTT - fQ2_xi*fKinematics.GetS())*L_x_k_b(i,xi,t);  


   return(ans);
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sL_3_a(Int_t i, Double_t xi){
   return  /*2.*fKinematics.GetS_p()*L_Y(i,xi)*/ - (fu_s + fKinematics.GetX())*L_s_k_a(i,xi) - fQ2_xi*L_s_l_a(i,xi) 
            + (fKinematics.GetS() + fu_x)*L_x_k_a(i,xi) - fQ2_xi*L_x_l_a(i,xi);  
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sL_3_b(Int_t i, Double_t xi, Double_t t){
   return  2.*fKinematics.GetS_p()*L_Y(i,xi,t) - (fu_s + fKinematics.GetX())*L_s_k_b(i,xi,t) - fQ2_xi*L_s_l_b(i,xi,t) 
            + (fKinematics.GetS() + fu_x)*L_x_k_b(i,xi,t) - fQ2_xi*L_x_l_b(i,xi,t);  
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sL_4_a(Int_t i, Double_t xi){
   return 0;
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sL_4_b(Int_t i, Double_t xi, Double_t t){
   return 0;
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sL_5_a(Int_t i, Double_t xi){
   return 0;
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sL_5_b(Int_t i, Double_t xi, Double_t t){
   return 0;
}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::sL(Int_t Index,Int_t SFType,Double_t xi) {

   //ProcessTFuncKin(xi); 

   // integration limits (for L functions below) 
   Double_t den  = 2.*( (fu/xi) + fKinematics.GetM()*fKinematics.GetM());
   ft_1 = ( fu*(fKinematics.GetS_x() - TMath::Sqrt(fKinematics.GetLambda_Q()) ) + 2.*fKinematics.GetM()*fKinematics.GetM()*fKinematics.GetQ2())/den;
   ft_2 = ( fu*(fKinematics.GetS_x() + TMath::Sqrt(fKinematics.GetLambda_Q()) ) + 2.*fKinematics.GetM()*fKinematics.GetM()*fKinematics.GetQ2())/den;

   Double_t ans=0; 

//   switch(Index){
//      case 1: 
//         ans = 2.*L_Y(SFType,xi) - L_s_k(SFType,xi) + L_s_l(SFType,xi) + L_x_k(SFType,xi) 
//            - L_x_l(SFType,xi) + 2.*L_t(SFType,xi);  
//         break;
//      case 2: 
//         ans = (fTT - fQ2_xi*fKinematics.GetS_x())*L_Y(SFType,xi) - (fQ2_xi*fKinematics.GetX() + fTT)*L_s_k(SFType,xi) 
//            + (fTT - fQ2_xi*fKinematics.GetS())*L_x_k(SFType,xi);  
//         break;
//      case 3: 
//         ans = 2.*fKinematics.GetS_p()*L_Y(SFType,xi) - (fu_s + fKinematics.GetX())*L_s_k(SFType,xi) - fQ2_xi*L_s_l(SFType,xi) 
//            + (fKinematics.GetS() + fu_x)*L_x_k(SFType,xi) - fQ2_xi*L_x_l(SFType,xi);  
//         break;
//      case 4: 
//         ans = -4.*fKinematics.GetX()*L_Y(SFType,xi) + (2.*fKinematics.GetX() - fKinematics.GetS())*L_s_k(SFType,xi) - fu_s*LTilde_s_k(SFType,xi) 
//            + (2.*fQ2_xi - fu_s)*L_s_l(SFType,xi) - (fKinematics.GetX() + 2.*fu_x)*L_x_k(SFType,xi) 
//            - fu_x*L_x_l(SFType,xi) - fu_x*LTilde_x_k(SFType,xi) + 2.*fKinematics.GetS_x()*L_t(SFType,xi);  
//         break;
//      case 5: 
//         ans = (fKinematics.GetS_x()*fQ2_xi - fKinematics.GetS_p()*fKinematics.GetS_p())*L_Y(SFType,xi) + (fKinematics.GetS()*fKinematics.GetS_p() + 2.*fKinematics.GetX()*fu_s)*L_s_k(SFType,xi) 
//            + fu_s*fKinematics.GetX()*LTilde_s_k(SFType,xi) + (2.*fKinematics.GetS()*fu_x + fKinematics.GetS_p()*fKinematics.GetX())*L_x_k(SFType,xi) - fu_x*fKinematics.GetS()*LTilde_x_k(SFType,xi);  
//         break;
//      default: 
//         std::cout << "[POLRADUltraRelativistic]: Invalid sL function!  Exiting..." << std::endl;
//         exit(1); 
//   }

   return ans; 

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::L_t(Int_t i,Double_t xi, Double_t t) {
   Double_t F   = fSFs_t[i];//GetStructureFunction(i,fxi,t); 
   Double_t Lt = (1./fKinematics.GetS_x())*(F/t);
   return Lt; 
}
//_____________________________________________________________________________
//Double_t POLRADUltraRelativistic::L_t_Integrand(Double_t t) {
//
//   Double_t F   = GetStructureFunction(fxi,t); 
//   Double_t ans = F/t; 
//   return ans; 
//
//}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::L_s_l_a(Int_t i,Double_t xi) {

   Double_t F   = fSFs_s[i];//GetStructureFunction(fxi,ft_s); 
   Double_t Lsl = (1./fu_s)*F*TMath::Log(fu_s*fu_s/(fu*fQ2_xi));
     // + (1./fu_s)*AdaptiveSimpson(&POLRADUltraRelativistic::L_s_l_Integrand,ft_1,ft_2,fErr,fDepth);
   return Lsl; 

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::L_s_l_b(Int_t i,Double_t xi, Double_t t) {
   Double_t Ft  = fSFs_t[i];//GetStructureFunction(fxi,t);                 
   Double_t Fts = fSFs_s[i];//GetStructureFunction(fxi,ft_s);              
   Double_t num = Ft - Fts; 
   Double_t den = TMath::Abs(t-ft_s);
   Double_t ans = num/den; 
   Double_t Lsl =  (1./fu_s)*ans;
   return Lsl; 

}
//_____________________________________________________________________________
//Double_t POLRADUltraRelativistic::L_s_l_Integrand(Double_t t) {
//
//   Double_t Ft  = GetStructureFunction(fxi,t);                 
//   Double_t Fts = GetStructureFunction(fxi,ft_s);              
//   Double_t num = Ft - Fts; 
//   Double_t den = TMath::Abs(t-ft_s);
//   Double_t ans = num/den; 
//   return ans; 
//
//}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::L_x_l_a(Int_t i,Double_t xi){

   Double_t F   = fSFs_x[i];//GetStructureFunction(fxi,ft_x);
   Double_t Lxl = (1./fu_x)*F*TMath::Log(fu_x*fu_x/(fu*fQ2_xi));
      //+ (1./fu_x)*AdaptiveSimpson(&POLRADUltraRelativistic::L_x_l_Integrand,ft_1,ft_2,fErr,fDepth); 
   return Lxl; 

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::L_x_l_b(Int_t i,Double_t xi, Double_t t){

   //fSFType      = i;
   //fxi          = xi;
   Double_t Ft  = fSFs_t[i];//GetStructureFunction(fxi,t);                 
   Double_t Ftx = fSFs_x[i];//GetStructureFunction(fxi,ft_x);              
   Double_t num = Ft - Ftx; 
   Double_t den = TMath::Abs(t-ft_x);
   Double_t ans = num/den; 
   Double_t Lxl = (1./fu_x)*ans;
   return Lxl; 

}
//_____________________________________________________________________________
//Double_t POLRADUltraRelativistic::L_x_l_Integrand(Double_t t) {
//
//   Double_t Ft  = GetStructureFunction(fxi,t);                 
//   Double_t Ftx = GetStructureFunction(fxi,ft_x);              
//   Double_t num = Ft - Ftx; 
//   Double_t den = TMath::Abs(t-ft_x);
//   Double_t ans = num/den; 
//   return ans; 
//
//}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::L_s_k_a(Int_t i,Double_t xi) {

   //fSFType      = i; 
   //fxi          = xi;        
   Double_t F   = fSFs_s[i];//GetStructureFunction(fxi,ft_s);                                             
   Double_t Lsk = (1./fKinematics.GetS())*F*TMath::Log(fu_s*fu_s/(fu*fQ2_xi));
     // + (1./fKinematics.GetS())*AdaptiveSimpson(&POLRADUltraRelativistic::L_s_k_Integrand,ft_1,ft_2,fErr,fDepth); 
   return Lsk; 

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::L_s_k_b(Int_t i,Double_t xi, Double_t t) {
   Double_t Ft  = fSFs_t[i];//GetStructureFunction(fxi,t);     
   Double_t Fts = fSFs_s[i];//GetStructureFunction(fxi,ft_s);   
   Double_t num = ft_s*Ft - t*Fts; 
   Double_t den = t*ft_s*TMath::Abs(t-ft_s);
   Double_t ans = num/den; 
   Double_t Lsk = (1./fKinematics.GetS())*ans;
   return Lsk; 

}
//_____________________________________________________________________________
//Double_t POLRADUltraRelativistic::L_s_k_Integrand(Double_t t) {
//
//   Double_t Ft  = GetStructureFunction(fxi,t);     
//   Double_t Fts = GetStructureFunction(fxi,ft_s);   
//   Double_t num = ft_s*Ft - t*Fts; 
//   Double_t den = t*ft_s*TMath::Abs(t-ft_s);
//   Double_t ans = num/den; 
//   return ans; 
//
//}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::L_x_k_a(Int_t i,Double_t xi) {

   //fSFType      = i; 
   //fxi          = xi;        
   Double_t F   = fSFs_x[i];//GetStructureFunction(fxi,ft_x);         
   Double_t Lxk = (1./fKinematics.GetX())*F*TMath::Log(fu_x*fu_x/(fu*fQ2_xi));
      //+ (1./fKinematics.GetX())*AdaptiveSimpson(&POLRADUltraRelativistic::L_x_k_Integrand,ft_1,ft_2,fErr,fDepth); 
   return Lxk; 

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::L_x_k_b(Int_t i,Double_t xi, Double_t t) {

   //fSFType      = i; 
   //fxi          = xi;        
   Double_t Ft  = fSFs_t[i];//GetStructureFunction(fxi,t);     
   Double_t Ftx = fSFs_x[i];//GetStructureFunction(fxi,ft_x);   
   Double_t num = ft_x*Ft - t*Ftx; 
   Double_t den = t*ft_x*TMath::Abs(t-ft_x);
   Double_t ans = num/den; 
   Double_t Lxk =  (1./fKinematics.GetX())*ans;
   return Lxk; 

}
//_____________________________________________________________________________
//Double_t POLRADUltraRelativistic::L_x_k_Integrand(Double_t t) {
//
//   Double_t Ft  = GetStructureFunction(fxi,t);     
//   Double_t Ftx = GetStructureFunction(fxi,ft_x);   
//   Double_t num = ft_x*Ft - t*Ftx; 
//   Double_t den = t*ft_x*TMath::Abs(t-ft_x);
//   Double_t ans = num/den; 
//   return ans; 
//
//}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::LTilde_s_k(Int_t i,Double_t xi, Double_t t) {

   //fSFType            = i; 
   //fxi                = xi;      
   Double_t sign = -1.0;
   if( t <= ft_s ) sign = 1.0;
   Double_t F   = fSFs_t[i];//GetStructureFunction(fxi,t);  
   Double_t ans = F/(t*t); 
   Double_t LTilde_sk = (sign/fu_s)*ans;                
   return LTilde_sk; 

}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::LTilde_x_k(Int_t i,Double_t xi, Double_t t) {

   Double_t sign = -1.0;
   if( t <= ft_x ) sign = 1.0;
   Double_t F   = fSFs_t[i];//GetStructureFunction(fxi,t);  
   Double_t ans = F/(t*t); 
   Double_t LTilde_xk = (sign/fu_x)*ans;                
   return LTilde_xk; 

}
//_____________________________________________________________________________
//Double_t POLRADUltraRelativistic::LTilde_Integrand(Double_t t) {
//
//   Double_t F   = GetStructureFunction(fxi,t);  
//   Double_t ans = F/(t*t); 
//   return ans;        
//
//}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::L_Y(Int_t i,Double_t xi, Double_t t) {

   //fSFType     = SFType; 
   //fxi         = xi;      
   Double_t Ft  = fSFs_t[i];//GetStructureFunction(fxi,t);    
   Double_t Fts = fSFs_s[i];//GetStructureFunction(fxi,ft_s);  
   Double_t Ftx = fSFs_x[i];//GetStructureFunction(fxi,ft_x);  
   Double_t num = fu_x*(t-ft_x)*(Ft-Fts) + fu_s*(t-ft_s)*(Ft-Ftx); 
   Double_t den = ( fu_s*TMath::Abs(t-ft_s) + fu_x*TMath::Abs(t-ft_x) )*TMath::Abs(t-ft_s)*TMath::Abs(t-ft_x);
   Double_t ans = num/den; 
   Double_t LY = ans;
   return LY; 

}
//_____________________________________________________________________________
//Double_t POLRADUltraRelativistic::L_Y_Integrand(Double_t t) {
//
//   Double_t Ft  = GetStructureFunction(fxi,t);    
//   Double_t Fts = GetStructureFunction(fxi,ft_s);  
//   Double_t Ftx = GetStructureFunction(fxi,ft_x);  
//   Double_t num = fu_x*(t-ft_x)*(Ft-Fts) + fu_s*(t-ft_s)*(Ft-Ftx); 
//   Double_t den = ( fu_s*TMath::Abs(t-ft_s) + fu_x*TMath::Abs(t-ft_x) )*TMath::Abs(t-ft_s)*TMath::Abs(t-ft_x);
//   Double_t ans = num/den; 
//   return ans; 
//
//}
//_____________________________________________________________________________
//Double_t POLRADUltraRelativistic::GetStructureFunction(Double_t x,Double_t Q2) {
//
//   Double_t SF=0; 
//
//   if(fSFType<0 || fSFType>7){
//      std::cout << "[POLRAD]: Invalid SF code!  Exiting..." << std::endl;
//      exit(1); 
//   }
//
//   if(fPOLRADTarget<0 || fPOLRADTarget>3){
//      std::cout << "[POLRAD]: Invalid target code!  Exiting..." << std::endl;
//      exit(1); 
//   }
//
//   switch(fSFType){
//      case 0: // F1 
//         if(fPOLRADTarget==0) SF = fUnpolSFs->F1p(x,Q2); 
//         if(fPOLRADTarget==1) SF = fUnpolSFs->F1n(x,Q2); 
//         if(fPOLRADTarget==2) SF = fUnpolSFs->F1d(x,Q2); 
//         if(fPOLRADTarget==3) SF = fUnpolSFs->F1He3(x,Q2); 
//         break;
//      case 1: // F2 
//         if(fPOLRADTarget==0) SF = fUnpolSFs->F2p(x,Q2); 
//         if(fPOLRADTarget==1) SF = fUnpolSFs->F2n(x,Q2); 
//         if(fPOLRADTarget==2) SF = fUnpolSFs->F2d(x,Q2); 
//         if(fPOLRADTarget==3) SF = fUnpolSFs->F2He3(x,Q2); 
//         break;
//      case 2: // g1 
//         if(fPOLRADTarget==0) SF = fPolSFs->g1p(x,Q2); 
//         if(fPOLRADTarget==1) SF = fPolSFs->g1n(x,Q2); 
//         if(fPOLRADTarget==2) SF = fPolSFs->g1d(x,Q2); 
//         if(fPOLRADTarget==3) SF = fPolSFs->g1He3(x,Q2); 
//         break;
//      case 3: // g2 
//         if(fPOLRADTarget==0) SF = fPolSFs->g2p(x,Q2); 
//         if(fPOLRADTarget==1) SF = fPolSFs->g2n(x,Q2); 
//         if(fPOLRADTarget==2) SF = fPolSFs->g2d(x,Q2); 
//         if(fPOLRADTarget==3) SF = fPolSFs->g2He3(x,Q2); 
//         break;
//      case 4: // b1 
//         SF = 0; 
//         break;
//      case 5: // b2
//         SF = 0; 
//         break;
//      case 6: // b3
//         SF = 0;
//         break;
//      case 7: // b4
//         SF = 0;
//         break;
//   }
//
//   return SF;
//
//}
//_____________________________________________________________________________
Double_t POLRADUltraRelativistic::AdaptiveSimpson(Double_t (POLRADUltraRelativistic::*f)(Double_t ) ,Double_t A,Double_t B,
      Double_t epsilon,Int_t Depth) {
   // Adaptive Simpson's Rule
   Double_t C = (A + B)/2.0;
   Double_t H = B - A;
   Double_t fa = (this->*f)(A);
   Double_t fb = (this->*f)(B);
   Double_t fc = (this->*f)(C);
   Double_t S = (H/6.0)*(fa + 4.0*fc + fb);
   Double_t arg = AdaptiveSimpsonAux(f,A,B,epsilon,S,fa,fb,fc,Depth);
   return arg;
}
//____________________________________________________________________________________
Double_t POLRADUltraRelativistic::AdaptiveSimpsonAux(Double_t (POLRADUltraRelativistic::*f)(Double_t ) ,Double_t A,Double_t B,
      Double_t epsilon,Double_t S,Double_t fa,Double_t fb,Double_t fc,Int_t bottom) {
   // Recursive auxiliary function for AdaptiveSimpson() function
   Double_t C      = (A + B)/2.0;
   Double_t H      = B - A;
   Double_t D      = (A + C)/2.0;
   Double_t E      = (C + B)/2.0;
   Double_t fd     = (this->*f)(D);
   Double_t fe     = (this->*f)(E);
   Double_t Sleft  = (H/12.0)*(fa + 4.0*fd + fc);
   Double_t Sright = (H/12.0)*(fc + 4.0*fe + fb);
   Double_t S2 = Sleft + Sright;
   if (bottom <= 0 || fabs(S2 - S) <= 15.0*epsilon){
      return S2 + (S2 - S)/15;
   }
   Double_t arg = AdaptiveSimpsonAux(f,A,C,epsilon/2.0,Sleft, fa,fc,fd,bottom-1) +
      AdaptiveSimpsonAux(f,C,B,epsilon/2.0,Sright,fc,fb,fe,bottom-1);
   return arg;
}
//_____________________________________________________________________________

}}
