#include "insane/xsections/RADCOR2.h"

namespace insane {
namespace physics {

//______________________________________________________________________________
RADCOR2::RADCOR2(){
   fXS          = nullptr;
   fX0          = 0;
   fDelta       = 0.01; // size of strip in strip approx. 
   fDeltaM      = 0.134;   // \Delta M in integration threshold calc. 
   fIntegThresh = 0;    // assume IRT  
   fEquivRad    = true;
}
//______________________________________________________________________________
RADCOR2::~RADCOR2(){
   //delete fWatch;
}
//______________________________________________________________________________
Double_t RADCOR2::GetPhi(Double_t v) const {
   // Shape of the Bremsstrahlung spectrum for small v (complete screening)
   return(1.0 - v + (3.0/4.0)*v*v); 
}
//______________________________________________________________________________
Double_t RADCOR2::GetEsMin(Double_t Ep, Double_t theta, Double_t M_thresh) const {
   // MoTsai A.18
   Double_t M      = fKinematics.GetM();
   //Double_t M_A    = fKinematics.GetM_A();
   Double_t costh  = TMath::Cos(theta); 
   Double_t num    = M_thresh*M_thresh + 2.0*M*M_thresh + 2.0*M*Ep;
   Double_t den    = 2.*M - 2.*Ep*(1.0-costh); 
   return num/den; 
}
//______________________________________________________________________________
Double_t RADCOR2::GetEpMax(Double_t Es, Double_t theta, Double_t M_thresh) const {
   // MoTsai A.19
   Double_t M      = fKinematics.GetM();
   //Double_t M_A    = fKinematics.GetM_A();
   Double_t costh  = TMath::Cos(theta); 
   Double_t num    = 2.*M*Es - 2.*M*M_thresh - M_thresh*M_thresh;
   Double_t den    = 2.*M + 2.*Es*(1.0-costh); 
   return num/den; 
}
//______________________________________________________________________________
Double_t RADCOR2::GetEsMinALT(Double_t Ep, Double_t theta,Double_t MT,Double_t M_thresh) const {
   // MoTsai A.18
   Double_t costh  = TMath::Cos(theta); 
   Double_t num    = M_thresh*M_thresh + 2.0*MT*M_thresh + 2.0*MT*Ep;
   Double_t den    = 2.*MT - 2.*Ep*(1.0-costh); 
   return num/den; 
}
//______________________________________________________________________________
Double_t RADCOR2::GetEpMaxALT(Double_t Es, Double_t theta,Double_t MT,Double_t M_thresh) const {
   // MoTsai A.19
   Double_t costh  = TMath::Cos(theta); 
   Double_t num    = 2.*MT*Es - 2.*MT*M_thresh - M_thresh*M_thresh;
   Double_t den    = 2.*MT + 2.*Es*(1.0-costh); 
   return num/den; 
}
//______________________________________________________________________________
Double_t RADCOR2::GetTr(Double_t Q2) const {
   // Equivalent Radiator thickness, tr. MoTsai(1969) in last paragraph of sec II.A
   if(!fEquivRad) return 0.0;
   Double_t b     = fKinematics.Getb(); 
   Double_t m     = fKinematics.Getm(); 
   Double_t m2    = m*m; 
   Double_t alpha = fine_structure_const; 
   Double_t T1    = (1.0/b)*(alpha/pi); 
   Double_t T2    = TMath::Log(Q2/m2) - 1.0; 
   return T1*T2;
}
//______________________________________________________________________________
Double_t RADCOR2::Delta(Double_t E, Double_t t) const {
   // Most probable energy loss due to ionization of an 
   // electron passing through material of thickness t.
   // For Es, use t_b; for Ep, use t_a
   // Tsai, SLAC-PUB-0848 (1971), Eq 1.8 
   if(t<=0.0) return 0.0;
   Double_t Z     = fKinematics.GetZ();
   Double_t A     = fKinematics.GetA();
   Double_t m     = fKinematics.Getm();
   Double_t X0    = fX0;  // radiation length
   Double_t a     = 0.154E-3*(Z/A);               /// B19 [in GeV]
   Double_t arg   = 3E+9*a*t*X0*E*E/(m*m*Z*Z); 
   Double_t delta = a*t*X0*( TMath::Log(arg) - 0.5772); 
   return delta;
}
//______________________________________________________________________________
Double_t RADCOR2::Ib(Double_t E0,Double_t E,Double_t t) const {
   // Bremsstrahlung loss 
   // Tsai, SLAC-PUB-0848 (1971), Eq B.43
   // Same as B.43 but we explicitly write out W_b(E,eps) 
   // so that X0 cancels from the formula
   Double_t b     = fKinematics.Getb(); 
   Double_t gamma = TMath::Gamma(1. + b*t);
   // Double_t Wb    = W_b(E0,E0-E);
   // Wb is (X0 cancels out when combined with everything in this method): 
   // Double_t b     = fKinematicsExt.Getb(); 
   // Double_t X0    = fX0Eff;
   // Double_t Wb    = (1./X0)*(b/eps)*GetPhi(eps/E0);
   Double_t v     = (E0-E)/E0;
   Double_t phi   = GetPhi(v);  
   Double_t T1    = (b*t/gamma)*( 1./(E0-E) ); 
   Double_t T2    = TMath::Power(v,b*t);
   Double_t T3    = phi;
   Double_t ib    = T1*T2*T3;
   return ib; 
}
//______________________________________________________________________________
void  RADCOR2::SetRadiationLengths(double a, double b)
{
   Double_t Tb = a;        // thickness before scattering in number of X0
   Double_t Ta = b;        // thickness after scattering in number of X0
   fKinematics.Sett_b(Tb);     
   fKinematics.Sett_a(Ta);
}
//______________________________________________________________________________
void RADCOR2::SetRadiationLengths(const Double_t *X){
   Double_t Tb = X[0];        // thickness before scattering in number of X0
   Double_t Ta = X[1];        // thickness after scattering in number of X0
   fKinematics.Sett_b(Tb);     
   fKinematics.Sett_a(Ta);
}
//______________________________________________________________________________
Double_t RADCOR2::EquivRadContinuumStragglingStripApprox(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){
   // Tsai 1971, Eqn C23
   if(!fXS) {
      Error("ContinuumStragglingStripApprox","Cross section is NULL!");
      return 0.0;
   }
   fEquivRad = true;
   Double_t MT     = fXS->GetA()*M_p/GeV; 
   fKinematics.SetZ(fXS->GetZ());
   fKinematics.SetA(fXS->GetA());
   fKinematics.SetM_A(MT); 
   fX0 = insane::Math::rad_length(fXS->GetZ(),fXS->GetA());
   //std::cout << " X0 = " << fX0 << std::endl;
   SetKinematics(Ebeam,Eprime,theta,phi);

   Double_t Es     = fKinematics.GetEs();
   Double_t Ep     = fKinematics.GetEp();
   Double_t Q2     = insane::Kine::Qsquared(Ebeam,Eprime,theta); 
   Double_t T      = fKinematics.GetT() + 2.0*GetTr(fKinematics.GetQ2());
   Double_t t_b    = fKinematics.Gett_b(); 
   Double_t t_a    = fKinematics.Gett_a();
   Double_t t_r    = GetTr( fKinematics.GetQ2() ); 
   Double_t b      = fKinematics.Getb();
   Double_t gamma  = 1.0 + 0.5772*b*T;//TMath::Gamma(1.0 +b*T);
   Double_t M      = fKinematics.GetM();
   Double_t M_A    = fKinematics.GetM_A();
   Double_t M_thr  = fDeltaM; 
   Double_t xi     = fKinematics.Getxi();

   Double_t min_Ep = Ep + fDelta;
   Double_t max_Ep = GetEpMax(Es,theta);
   if(fIntegThresh==0) max_Ep = GetEpMax(Es,theta);
   if(fIntegThresh==1) max_Ep = GetEpMaxALT(Es,theta,M,M_thr);
   if(fIntegThresh==2) max_Ep = GetEpMaxALT(Es,theta,M_A,M_thr);


   // Double_t R      = (M + Es*(1.0 - TMath::Cos(theta)))/(M - max_Ep*(1.0 - TMath::Cos(theta))); 
   Double_t R      = (M_A + Es*(1.0 - TMath::Cos(theta)))/(M_A - max_Ep*(1.0 - TMath::Cos(theta)));  /// note that we use M_A, not M here! 
   // Double_t T0     = TMath::Power(R*fDelta*fDelta/(Es*Ep),b*T/2.0);
   /// use the following to be consistent with Eq 4.1 (and essentially Eq A82 in Stein) 
   Double_t T0     = TMath::Power( R*fDelta/(Es),b*(t_b + t_r) );
   Double_t T1     = TMath::Power( fDelta/(Ep)  ,b*(t_a + t_r) );
   Double_t T2     = 1.; 
   Double_t T3     = (xi/fDelta)/( 1. - b*(t_a + t_b + 2.*t_r) ); 
   /// end 

   Double_t min_Es = GetEsMin(Ep,theta);
   if(fIntegThresh==0) min_Es = GetEsMin(Ep,theta);
   if(fIntegThresh==1) min_Es = GetEsMinALT(Ep,theta,M,M_thr);
   if(fIntegThresh==2) min_Es = GetEsMinALT(Ep,theta,M_A,M_thr);
   Double_t max_Es = Es - fDelta*R;

   if( min_Es > max_Es ||  min_Ep > max_Ep ){ 
      //fKinematics.Print();
      //std::cout << min_Ep << " < Ep' < " << max_Ep << std::endl;
      //std::cout << min_Es << " < Es' < " << max_Es << std::endl;
      return(0.0);
   }

   //Double_t delta_s  = -0.5*fKinematics.Getb()*fKinematics.GetT()*TMath::Log(Es/fDelta);
   //Double_t delta_p  = -0.5*fKinematics.Getb()*fKinematics.GetT()*TMath::Log(Ep/fDelta);
   
   StripApproxIntegrand1Wrap f1;
   f1.fRADCOR = this;
   f1.Es     = Es;
   f1.Ep     = Ep;
   f1.T      = T;
   f1.theta  = theta;

   StripApproxIntegrand2Wrap f2;
   f2.fRADCOR = this;
   f2.Es     = Es;
   f2.Ep     = Ep;
   f2.T      = T;
   f2.theta  = theta;

   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE;//kGAUSS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   double absErr                           = 1.0e100;   /// desired absolute Error 
   double relErr                           = 1.0e-3;   /// desired relative Error 
   unsigned int ncalls                     = 1.0e3;  /// number of calls (MC only)
   unsigned int rule                       = 1;
   ROOT::Math::IntegratorOneDim  ig(type,absErr,relErr,ncalls,rule);

   if( !(fKinematics.Valid()) ) {
      Warning("EquivRadContinuumStragglingStripApprox","Invalid kinematics");
      return 0.0;
   }

   //std::cout << "func1 " << min_Es << " " << max_Es <<std::endl;
   ig.SetFunction( f1 );
   Double_t res1 = ig.Integral(min_Es,max_Es);

   //std::cout << "func2 " << min_Ep << " " << max_Ep <<std::endl;
   ig.SetFunction( f2 );
   Double_t res2 = ig.Integral(min_Ep,max_Ep);

   Double_t par[] = {Ep,theta,0.0};
   fXS->SetBeamEnergy(Es);
   Double_t sig0  = fXS->EvaluateBaseXSec(par);
   // Double_t sig_t = T0*sig0;
   // sig_t           += res1;
   // sig_t           += res2;
   // sig_t           *= gamma;
   /// use the following to be consistent with Eq 4.1 (and essentially Eq A82 in Stein) 
   // Double_t f_tilde = insane::Kine::F_tilde_internal(Es,Ep,theta) + 0.5772*b*(t_b + t_a);
   Double_t f_tilde = GetFTildeExactInternal(Q2,t_b,t_a);
   Double_t sig_t   = T0*T1*(T2 - T3)*f_tilde*sig0 + res1 + res2;
   /// end 
   return sig_t; 
}
//______________________________________________________________________________
Double_t RADCOR2::InternalOnly_EquivRadContinuumStragglingStripApprox(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){
   // Internal radiative corrections using the equivalent radiator with strip approximation.
   // Tsai 1971, Eqn C23
   if(!fXS) {
      Error("ContinuumStragglingStripApprox","Cross section is NULL!");
      return 0.0;
   }
   fEquivRad = true;
   // target lengths 
   Double_t t_b_temp    = fKinematics.Gett_b(); 
   Double_t t_a_temp    = fKinematics.Gett_a();

   Double_t MT     = fXS->GetA()*M_p/GeV; 
   fKinematics.SetZ(fXS->GetZ());
   fKinematics.SetA(fXS->GetA());
   fKinematics.SetM_A(MT); 
   fKinematics.Sett_b(0.0);
   fKinematics.Sett_a(0.0);
   fX0 = insane::Math::rad_length(fXS->GetZ(),fXS->GetA());
   //std::cout << " X0 = " << fX0 << std::endl;
   SetKinematics(Ebeam,Eprime,theta,phi);

   Double_t Es     = fKinematics.GetEs();
   Double_t Ep     = fKinematics.GetEp();
   Double_t Q2     = insane::Kine::Qsquared(Ebeam,Eprime,theta); 
   Double_t T      = 2.0*GetTr(fKinematics.GetQ2());
   Double_t t_b    = fKinematics.Gett_b(); 
   Double_t t_a    = fKinematics.Gett_a();
   Double_t t_r    = GetTr( fKinematics.GetQ2() ); 
   Double_t b      = fKinematics.Getb();
   Double_t gamma  = 1.0 + 0.5772*b*T;//TMath::Gamma(1.0 +b*T);
   Double_t M      = fKinematics.GetM();
   Double_t M_A    = fKinematics.GetM_A();
   Double_t M_thr  = fDeltaM; 
   Double_t xi     = fKinematics.Getxi();

   Double_t min_Ep = Ep + fDelta;
   Double_t max_Ep = GetEpMax(Es,theta);
   if(fIntegThresh==0) max_Ep = GetEpMax(Es,theta);
   if(fIntegThresh==1) max_Ep = GetEpMaxALT(Es,theta,M,M_thr);
   if(fIntegThresh==2) max_Ep = GetEpMaxALT(Es,theta,M_A,M_thr);


   // Double_t R      = (M + Es*(1.0 - TMath::Cos(theta)))/(M - max_Ep*(1.0 - TMath::Cos(theta))); 
   Double_t R      = (M_A + Es*(1.0 - TMath::Cos(theta)))/(M_A - max_Ep*(1.0 - TMath::Cos(theta)));  /// note that we use M_A, not M here! 
   // Double_t T0     = TMath::Power(R*fDelta*fDelta/(Es*Ep),b*T/2.0);
   /// use the following to be consistent with Eq 4.1 (and essentially Eq A82 in Stein) 
   Double_t T0     = TMath::Power( R*fDelta/(Es),b*(t_b + t_r) );
   Double_t T1     = TMath::Power( fDelta/(Ep)  ,b*(t_a + t_r) );
   Double_t T2     = 1.; 
   Double_t T3     = (xi/fDelta)/( 1. - b*(t_a + t_b + 2.*t_r) ); 
   /// end 

   Double_t min_Es = GetEsMin(Ep,theta);
   if(fIntegThresh==0) min_Es = GetEsMin(Ep,theta);
   if(fIntegThresh==1) min_Es = GetEsMinALT(Ep,theta,M,M_thr);
   if(fIntegThresh==2) min_Es = GetEsMinALT(Ep,theta,M_A,M_thr);
   Double_t max_Es = Es - fDelta*R;

   if( min_Es > max_Es ||  min_Ep > max_Ep ){ 
      //fKinematics.Print();
      //std::cout << min_Ep << " < Ep' < " << max_Ep << std::endl;
      //std::cout << min_Es << " < Es' < " << max_Es << std::endl;
      return(0.0);
   }

   //Double_t delta_s  = -0.5*fKinematics.Getb()*fKinematics.GetT()*TMath::Log(Es/fDelta);
   //Double_t delta_p  = -0.5*fKinematics.Getb()*fKinematics.GetT()*TMath::Log(Ep/fDelta);
   
   StripApproxIntegrand1Wrap f1;
   f1.fRADCOR = this;
   f1.Es     = Es;
   f1.Ep     = Ep;
   f1.T      = T;
   f1.theta  = theta;

   StripApproxIntegrand2Wrap f2;
   f2.fRADCOR = this;
   f2.Es     = Es;
   f2.Ep     = Ep;
   f2.T      = T;
   f2.theta  = theta;

   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   double absErr                           = 1.0e-1;   /// desired absolute Error 
   double relErr                           = 1.0e-2;   /// desired relative Error 
   unsigned int ncalls                     = 1.0e2;  /// number of calls (MC only)
   unsigned int rule                       = 1;
   ROOT::Math::IntegratorOneDim  ig(type,absErr,relErr,ncalls,rule);

   if( !(fKinematics.Valid()) ) {
      Warning("EquivRadContinuumStragglingStripApprox","Invalid kinematics");
      return 0.0;
   }

   ig.SetFunction( f1 );
   Double_t res1 = ig.Integral(min_Es,max_Es);

   ig.SetFunction( f2 );
   Double_t res2 = ig.Integral(min_Ep,max_Ep);

   Double_t par[] = {Ep,theta,0.0};
   fXS->SetBeamEnergy(Es);
   Double_t sig0  = fXS->EvaluateBaseXSec(par);
   // Double_t sig_t = T0*sig0;
   // sig_t           += res1;
   // sig_t           += res2;
   // sig_t           *= gamma;
   /// use the following to be consistent with Eq 4.1 (and essentially Eq A82 in Stein) 
   // Double_t f_tilde = insane::Kine::F_tilde_internal(Es,Ep,theta) + 0.5772*b*(t_b + t_a);
   Double_t f_tilde = GetFTildeExactInternal(Q2,t_b,t_a);
   Double_t sig_t   = T0*T1*(T2 - T3)*f_tilde*sig0 + res1 + res2;

   // restore the lengths
   fKinematics.Sett_b(t_b_temp);
   fKinematics.Sett_a(t_a_temp);
   /// end 
   return sig_t; 
}
//______________________________________________________________________________
Double_t RADCOR2::ExternalOnly_EquivRadContinuumStragglingStripApprox(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){
   // Tsai 1971, Eqn C23
   if(!fXS) {
      Error("ContinuumStragglingStripApprox","Cross section is NULL!");
      return 0.0;
   }
   fEquivRad = false;
   Double_t MT     = fXS->GetA()*M_p/GeV; 
   fKinematics.SetZ(fXS->GetZ());
   fKinematics.SetA(fXS->GetA());
   fKinematics.SetM_A(MT); 
   fX0 = insane::Math::rad_length(fXS->GetZ(),fXS->GetA());
   //std::cout << " MT = " << MT << std::endl;
   //std::cout << " X0 = " << fX0 << std::endl;
   SetKinematics(Ebeam,Eprime,theta,phi);

   Double_t Es     = fKinematics.GetEs();
   Double_t Ep     = fKinematics.GetEp();
   Double_t Q2     = insane::Kine::Qsquared(Ebeam,Eprime,theta); 
   Double_t T      = fKinematics.GetT() ;//+ 2.0*GetTr(fKinematics.GetQ2());
   Double_t t_b    = fKinematics.Gett_b(); 
   Double_t t_a    = fKinematics.Gett_a();
   Double_t t_r    = 0.0;//GetTr( fKinematics.GetQ2() ); 
   Double_t b      = fKinematics.Getb();
   Double_t gamma  = 1.0 + 0.5772*b*T;//TMath::Gamma(1.0 +b*T);
   Double_t M      = fKinematics.GetM();
   Double_t M_A    = fKinematics.GetM_A();
   Double_t M_thr  = fDeltaM; 
   Double_t xi     = fKinematics.Getxi();

   Double_t min_Ep = Ep + fDelta;
   Double_t max_Ep = GetEpMax(Es,theta);
   if(fIntegThresh==0) max_Ep = GetEpMax(Es,theta);
   if(fIntegThresh==1) max_Ep = GetEpMaxALT(Es,theta,M,M_thr);
   if(fIntegThresh==2) {
      max_Ep = GetEpMaxALT(Es,theta,M_A,M_thr);
      //std::cout << " DERP\n";
   }


   // Double_t R      = (M + Es*(1.0 - TMath::Cos(theta)))/(M - max_Ep*(1.0 - TMath::Cos(theta))); 
   Double_t R      = (M_A + Es*(1.0 - TMath::Cos(theta)))/(M_A - max_Ep*(1.0 - TMath::Cos(theta)));  /// note that we use M_A, not M here! 
   // Double_t T0     = TMath::Power(R*fDelta*fDelta/(Es*Ep),b*T/2.0);
   /// use the following to be consistent with Eq 4.1 (and essentially Eq A82 in Stein) 
   Double_t T0     = TMath::Power( R*fDelta/(Es),b*(t_b + t_r) );
   Double_t T1     = TMath::Power( fDelta/(Ep)  ,b*(t_a + t_r) );
   Double_t T2     = 1.; 
   Double_t T3     = (xi/fDelta)/( 1. - b*(t_a + t_b + 2.*t_r) ); 
   /// end 

   Double_t min_Es = GetEsMin(Ep,theta);
   if(fIntegThresh==0) min_Es = GetEsMin(Ep,theta);
   if(fIntegThresh==1) min_Es = GetEsMinALT(Ep,theta,M,M_thr);
   if(fIntegThresh==2) min_Es = GetEsMinALT(Ep,theta,M_A,M_thr);
   Double_t max_Es = Es - fDelta*R;

   if( min_Es > max_Es ||  min_Ep > max_Ep ){ 
      fKinematics.Print();
      std::cout << min_Ep << " < Ep' < " << max_Ep << std::endl;
      std::cout << min_Es << " < Es' < " << max_Es << std::endl;
      return(0.0);
   }

   //Double_t delta_s  = -0.5*fKinematics.Getb()*fKinematics.GetT()*TMath::Log(Es/fDelta);
   //Double_t delta_p  = -0.5*fKinematics.Getb()*fKinematics.GetT()*TMath::Log(Ep/fDelta);
   
   StripApproxIntegrand1Wrap f1;
   f1.fRADCOR = this;
   f1.Es     = Es;
   f1.Ep     = Ep;
   f1.T      = T;
   f1.theta  = theta;

   StripApproxIntegrand2Wrap f2;
   f2.fRADCOR = this;
   f2.Es     = Es;
   f2.Ep     = Ep;
   f2.T      = T;
   f2.theta  = theta;

   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   double absErr                           = 1.0e10;   /// desired absolute Error 
   double relErr                           = 1.0e-8;   /// desired relative Error 
   unsigned int ncalls                     = 1.0e5;  /// number of calls (MC only)
   unsigned int rule                       = 1;
   ROOT::Math::IntegratorOneDim  ig(type,absErr,relErr,ncalls,rule);

   if( !(fKinematics.Valid()) ) {
      Warning("EquivRadContinuumStragglingStripApprox","Invalid kinematics");
      return 0.0;
   }

   //std::cout << "func1 " << min_Es << " " << max_Es <<std::endl;
   ig.SetFunction( f1 );
   Double_t res1 = ig.Integral(min_Es,max_Es);

   //std::cout << "func2 " << min_Ep << " " << max_Ep <<std::endl;
   ig.SetFunction( f2 );
   Double_t res2 = ig.Integral(min_Ep,max_Ep);

   Double_t par[] = {Ep,theta,0.0};
   fXS->SetBeamEnergy(Es);
   Double_t sig0  = fXS->EvaluateBaseXSec(par);
   // Double_t sig_t = T0*sig0;
   // sig_t           += res1;
   // sig_t           += res2;
   // sig_t           *= gamma;
   /// use the following to be consistent with Eq 4.1 (and essentially Eq A82 in Stein) 
   // Double_t f_tilde = insane::Kine::F_tilde_internal(Es,Ep,theta) + 0.5772*b*(t_b + t_a);
   Double_t f_tilde = GetFTildeExactInternal(Q2,t_b,t_a);
   Double_t sig_t   = T0*T1*(T2 - T3)*f_tilde*sig0 + res1 + res2;
   /// end 
   return sig_t; 
}
//______________________________________________________________________________
Double_t RADCOR2::StripApproxIntegrand1_withD(Double_t Es,  Double_t Ep, Double_t theta, Double_t T, Double_t EsPrime){

   // Mo and Tsai 1971 C.23 
   // This is the Es' integrand
   Double_t b   = fKinematics.Getb();
   Double_t M   = fKinematics.GetM();
   Double_t M_A = fKinematics.GetM_A();
   //Double_t R = (M + Es*(1.0 - TMath::Cos(theta)))/(M - Ep*(1.0 - TMath::Cos(theta))); 
   Double_t max_Ep = GetEpMax(Es,theta);
   // Double_t R      = (M + Es*(1.0 - TMath::Cos(theta)))/(M - max_Ep*(1.0 - TMath::Cos(theta))); 
   Double_t R     = (M_A + Es*(1.0 - TMath::Cos(theta)))/(M_A - max_Ep*(1.0 - TMath::Cos(theta)));  /// note that we use M_A, not M here! 
   Double_t T1  = TMath::Power( (Es-EsPrime)*(Es-EsPrime)/(Ep*R*Es), b*T/2.0 )*b*T/(2.0*(Es-EsPrime));
   Double_t par[] = {Ep,theta,0.0};
   fXS->SetBeamEnergy(EsPrime);
   Double_t D = insane::Kine::BeamDepol(Es,EsPrime,1.0);
   return T1*fXS->EvaluateBaseXSec(par)*GetPhi((Es-EsPrime)/Es)*(1.0-D);
}
//______________________________________________________________________________
Double_t RADCOR2::StripApproxIntegrand1(Double_t Es,  Double_t Ep, Double_t theta, Double_t T, Double_t EsPrime){

   // Mo and Tsai 1971 C.23 
   // This is the Es' integrand
   Double_t t_b    = fKinematics.Gett_b(); 
   Double_t t_a    = fKinematics.Gett_a();
   Double_t Q2     = insane::Kine::Qsquared(EsPrime,Ep,theta); 
   Double_t t_r    = GetTr(Q2);
   Double_t b      = fKinematics.Getb();
   Double_t M      = fKinematics.GetM();
   Double_t M_A    = fKinematics.GetM_A();
   Double_t M_thr  = fDeltaM; 
   Double_t xi     = fKinematics.Getxi();
   Double_t v      = (Es-EsPrime)/Es;  
   Double_t phi    = GetPhi(v); 
   Double_t max_Ep = GetEpMax(Es,theta);
   if(fIntegThresh==0) GetEpMax(Es,theta); 
   if(fIntegThresh==1) GetEpMaxALT(Es,theta,M,M_thr); 
   if(fIntegThresh==2) GetEpMaxALT(Es,theta,M_A,M_thr); 
   // Double_t R  = (M + Es*(1.0 - TMath::Cos(theta)))/(M - max_Ep*(1.0 - TMath::Cos(theta))); 
   Double_t R      = (M_A + Es*(1.0 - TMath::Cos(theta)))/(M_A - max_Ep*(1.0 - TMath::Cos(theta)));  /// note that we use M_A, not M here! 
   //Double_t R = (M + Es*(1.0 - TMath::Cos(theta)))/(M - Ep*(1.0 - TMath::Cos(theta))); 
   /// Eq C23 
   // Double_t T1 = TMath::Power( (Es-EsPrime)*(Es-EsPrime)/(Ep*R*Es), b*T/2.0);
   // Double_t T2 = b*T/(2.0*(Es-EsPrime));
   // Double_t par[] = {Ep,theta,0.0};
   // fXS->SetBeamEnergy(EsPrime);
   // Double_t f_tilde = insane::Kine::F_tilde_internal(EsPrime,Ep,theta);
   // return T1*T2*f_tilde*fXS->EvaluateBaseXSec(par)*GetPhi((Es-EsPrime)/Es);
   /// including xi term, using the form of Eq 4.1 [T' = t_{a,b} + t_r, see Eq A82 in Stein (PRD 12, 1884 (1975)]  
   Double_t T1      = TMath::Power( (Es-EsPrime)/(Ep*R),b*(t_a + t_r) );
   Double_t T2      = TMath::Power( (Es-EsPrime)/(Es)  ,b*(t_b + t_r) );
   Double_t T3      = ( b*(t_b + t_r)/(Es-EsPrime) )*phi;
   Double_t T4      = xi/( 2.*TMath::Power(Es-EsPrime,2.) );
   Double_t par[]   = {Ep,theta,0.0};
   fXS->SetBeamEnergy(EsPrime);
   Double_t sig     = fXS->EvaluateBaseXSec(par); 
   /// WARNING: There's a subtlety in f_tilde: I think Es, Ep should be constants (i.e., not Es', Ep'), and the Q2 value should be 
   ///          changing... 
   // Double_t f_tilde = insane::Kine::F_tilde_internal(EsPrime,Ep,theta) + 0.5772*b*(t_b + t_a);
   Double_t f_tilde = GetFTildeExactInternal(Q2,t_b,t_a);
   Double_t res     = f_tilde*sig*T1*T2*(T3 + T4); 
   return res;

}
//______________________________________________________________________________
Double_t RADCOR2::StripApproxIntegrand2(Double_t Es,  Double_t Ep, Double_t theta, Double_t T, Double_t EpPrime){

   // Mo and Tsai 1971 C.23 
   // This is the Ep' integrand
   Double_t Q2     = insane::Kine::Qsquared(Es,EpPrime,theta); 
   Double_t t_b    = fKinematics.Gett_b(); 
   Double_t t_a    = fKinematics.Gett_a();
   Double_t t_r    = GetTr(Q2); 
   Double_t b      = fKinematics.Getb();
   Double_t M      = fKinematics.GetM();
   Double_t M_A    = fKinematics.GetM_A();
   Double_t xi     = fKinematics.Getxi();
   Double_t v      = (EpPrime-Ep)/EpPrime;  
   Double_t phi    = GetPhi(v); 
   Double_t M_thr  = fDeltaM; 
   Double_t max_Ep = GetEpMax(Es,theta);
   if(fIntegThresh==0) GetEpMax(Es,theta); 
   if(fIntegThresh==1) GetEpMaxALT(Es,theta,M,M_thr); 
   if(fIntegThresh==2) GetEpMaxALT(Es,theta,M_A,M_thr); 
   // Double_t R       = (M + Es*(1.0 - TMath::Cos(theta)))/(M - max_Ep*(1.0 - TMath::Cos(theta))); 
   Double_t R      = (M_A + Es*(1.0 - TMath::Cos(theta)))/(M_A - max_Ep*(1.0 - TMath::Cos(theta)));  /// note that we use M_A, not M here! 
   /// Eq C23 
   // Double_t T1      = TMath::Power( (EpPrime-Ep)*(EpPrime-Ep)*R/(EpPrime*Es), b*T/2.0 );
   // Double_t T2      = b*T/(2.0*(EpPrime-Ep));
   // Double_t par[]   = {EpPrime,theta,0.0};
   // Double_t f_tilde = insane::Kine::F_tilde_internal(Es,EpPrime,theta);
   // fXS->SetBeamEnergy(Es);
   // return T1*T2*f_tilde*fXS->EvaluateBaseXSec(par)*GetPhi((EpPrime-Ep)/EpPrime);
   /// including xi term, using the form of Eq 4.1 [T' = t_{a,b} + t_r, see Eq A82 in Stein (PRD 12, 1884 (1975)]  
   Double_t T1      = TMath::Power( (EpPrime-Ep)/(EpPrime), b*(t_a + t_r) );
   Double_t T2      = TMath::Power( (EpPrime-Ep)*R/(Es)   , b*(t_b + t_r) );
   Double_t T3      = ( b*(t_a + t_r)/(EpPrime-Ep) )*phi;
   Double_t T4      = xi/( 2.*TMath::Power(EpPrime - Ep,2.) ); 
   Double_t par[]   = {EpPrime,theta,0.0};
   /// WARNING: There's a subtlety in f_tilde: I think Es, Ep should be constants (i.e., not Es', Ep'), and the Q2 value should be 
   ///          changing... 
   // Double_t f_tilde = insane::Kine::F_tilde_internal(Es,EpPrime,theta) + 0.5772*b*(t_b + t_a);
   Double_t f_tilde = GetFTildeExactInternal(Q2,t_b,t_a);
   fXS->SetBeamEnergy(Es);
   Double_t sig     = fXS->EvaluateBaseXSec(par); 
   Double_t res     = f_tilde*sig*T1*T2*(T3 + T4); 
   return res;  
}
//______________________________________________________________________________
Double_t RADCOR2::ExternalOnly_ElasticPeak(Double_t Es,Double_t Ep,Double_t theta, Double_t phi){
   // Equation A.15 of Mo and Tsai 1969.
   if(!fXS) {
      Error("ExternalOnly_ElasticPeak","Cross section is NULL!");
      return 0.0;
   }
   fKinematics.SetZ(fXS->GetZ());
   fKinematics.SetA(fXS->GetA());
   fX0 = insane::Math::rad_length(fXS->GetZ(),fXS->GetA());
   SetKinematics(Es,Ep,theta); 
   Double_t T       = fKinematics.GetT();
   Double_t b       = fKinematics.Getb();
   Double_t M       = fKinematics.GetM(); 
   Double_t eta     = 1.0+Es*(1.0-TMath::Cos(theta))/M;
   Double_t DeltaE  = 0.05;//Es-Ep; // not sure about this one
   Double_t Epmax   = Es/eta;
   Double_t par[3]  = {theta,phi,0.0};
   fXS->SetBeamEnergy(Es); 
   Double_t sig1    = fXS->EvaluateBaseXSec(fXS->GetDependentVariables(par)); 
   Double_t T1      = DeltaE/Epmax;
   Double_t T2      = DeltaE*eta*eta/Es;
   Double_t res     = TMath::Power( T1*T2, b*T/2.0)*sig1;
   return(res);
}
//______________________________________________________________________________

Double_t RADCOR2::ExternalOnly_ElasticTail(Double_t Es,Double_t Ep,Double_t theta, Double_t phi){
   // Equation A.16 of Mo and Tsai 1969.
   // Todo: Replace this with Tsai(1971) Eqn C.13
   if(!fXS) {
      Error("ExternalOnly_ElasticTail","Cross section is NULL!");
      return 0.0;
   }
   ; 
   fKinematics.SetZ(fXS->GetZ());
   fKinematics.SetA(fXS->GetA());
   fKinematics.SetM_A(fXS->GetA()*M_p/GeV);

   fX0 = insane::Math::rad_length(fXS->GetZ(),fXS->GetA());
   SetKinematics(Es,Ep,theta); 
   Double_t T       = fKinematics.GetT();
   Double_t M       = fKinematics.GetM_A(); 
   //std::cout << "M = " << M << std::endl;
   Double_t eta1    = 1.0/(1.0-Ep*(1.0-TMath::Cos(theta))/M);
   Double_t eta2    = 1.0+Es*(1.0-TMath::Cos(theta))/M;

   Double_t par[3]  = {theta,phi,0.0};
   fXS->SetBeamEnergy(Ep*eta1); 
   Double_t sig1    = fXS->EvaluateBaseXSec(fXS->GetDependentVariables(par)); 
   Double_t T1      = Ib(Es,Ep*eta1,T/2.0)*eta1*eta1*sig1;

   fXS->SetBeamEnergy(Es); 
   Double_t sig2    = fXS->EvaluateBaseXSec(fXS->GetDependentVariables(par)); 
   Double_t T2      = Ib(Es/eta2,Ep,T/2.0)*sig2;

   return(T1+T2);
}
//______________________________________________________________________________

Double_t RADCOR2::ExternalOnly_ElasticTail_sPeak(Double_t Es,Double_t Ep,Double_t theta, Double_t phi){
   // Equation A.16 of Mo and Tsai 1969.
   // Todo: Replace this with Tsai(1971) Eqn C.13
   // Here we only use the s-peak because we will not detect the electron. Just
   // the target hadron.
   if(!fXS) {
      Error("ExternalOnly_ElasticTail","Cross section is NULL!");
      return 0.0;
   }
   ; 
   fKinematics.SetZ(fXS->GetZ());
   fKinematics.SetA(fXS->GetA());
   fKinematics.SetM_A(fXS->GetA()*M_p/GeV);

   fX0 = insane::Math::rad_length(fXS->GetZ(),fXS->GetA());
   SetKinematics(Es,Ep,theta); 
   Double_t T       = fKinematics.GetT();
   Double_t M       = fKinematics.GetM_A(); 
   //std::cout << "M = " << M << std::endl;
   Double_t eta1    = 1.0/(1.0-Ep*(1.0-TMath::Cos(theta))/M);
   Double_t eta2    = 1.0+Es*(1.0-TMath::Cos(theta))/M;

   Double_t par[3]  = {theta,phi,0.0};
   fXS->SetBeamEnergy(Ep*eta1); 
   Double_t sig1    = fXS->EvaluateBaseXSec(fXS->GetDependentVariables(par)); 
   Double_t T1      = Ib(Es,Ep*eta1,T/2.0)*eta1*eta1*sig1;

   fXS->SetBeamEnergy(Es); 
   Double_t sig2    = fXS->EvaluateBaseXSec(fXS->GetDependentVariables(par)); 
   Double_t T2      = Ib(Es/eta2,Ep,T/2.0)*sig2;

   return(T1+T2);
}
//______________________________________________________________________________

Double_t RADCOR2::EquivRad2DEnergyIntegral(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){
   // Approximation of MoTsai(1969) A.12 where the t integral is just set to evalute 
   // each Ib at t1=T/2+tr and t2=T/2+tr.
   if(!fXS) {
      Error("EquivRad2DEnergyIntegral","Cross section is NULL!");
      return 0.0;
   }
   fKinematics.SetZ(fXS->GetZ());
   fKinematics.SetA(fXS->GetA());
   fX0 = insane::Math::rad_length(fXS->GetZ(),fXS->GetA());
   SetKinematics(Ebeam,Eprime,theta,phi);
   Double_t Es     = fKinematics.GetEs();
   Double_t Ep     = fKinematics.GetEp();
   Double_t M      = fKinematics.GetM();
   Double_t tr     = GetTr(fKinematics.GetQ2());
   //std::cout << " tr = " << tr << std::endl;
   // First variable is Es
   /// limits defined using Ep + delta -> EpMax, EsMin -> Es - Delta  
   // Double_t min[2] = {GetEsMin(Ep,theta), Ep + fDelta       };
   // Double_t max[2] = {Es - fDelta       , GetEpMax(Es,theta)};
   /// full integration range 
   Double_t min[2] = {GetEsMin(Ep,theta), Ep                };
   Double_t max[2] = {Es                , GetEpMax(Es,theta)};
   if( min[0] > max[0] ||  min[1] > max[1] ){ 
      return(0.0);
   }

   ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kADAPTIVE;//,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kMISER;
   double absErr                           = 1.0e-4;   /// desired absolute Error 
   double relErr                           = 1.0e-4;   /// desired relative Error 
   unsigned int ncalls                     = 10000; /// number of calls (MC only)
   int rule                                = 1;        /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    
   ROOT::Math::IntegratorMultiDim ig(type,absErr,relErr,ncalls); 

   External2DEnergyIntegral_IntegrandWrap f1;
   f1.fRADCOR = this;
   f1.Es    = Es;
   f1.Ep    = Ep;
   f1.theta = theta;
   f1.phi   = phi;
   f1.t1    = fKinematics.Gett_b()+tr;
   f1.t2    = fKinematics.Gett_a()+tr;
   ig.SetFunction(f1);
   
   Double_t res1 = ig.Integral(min,max);
   return res1; 
}
//______________________________________________________________________________
Double_t RADCOR2::EquivRad2DEnergyIntegral_RegionIV(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){
   // Strip approximation Region IV integral. 
   // Approximation of MoTsai(1969) A.12 where the t integral is just set to evalute 
   // each Ib at t1=T/2+tr and t2=T/2+tr.
   if(!fXS) {
      Error("EquivRad2DEnergyIntegral_RegionIV","Cross section is NULL!");
      return 0.0;
   }
   fKinematics.SetZ(fXS->GetZ());
   fKinematics.SetA(fXS->GetA());
   fX0 = insane::Math::rad_length(fXS->GetZ(),fXS->GetA());
   SetKinematics(Ebeam,Eprime,theta,phi);
   Double_t Es     = fKinematics.GetEs();
   Double_t Ep     = fKinematics.GetEp();
   Double_t M      = fKinematics.GetM();
   Double_t M_A    = fKinematics.GetM();
   Double_t M_thr  = fDeltaM; 
   Double_t tr     = GetTr(fKinematics.GetQ2());
   //std::cout << " tr = " << tr << std::endl;
   // First variable is Es
   Double_t min_Es = GetEsMin(Ep,theta); 
   if(fIntegThresh==0) min_Es = GetEsMin(Ep,theta); 
   if(fIntegThresh==1) min_Es = GetEsMinALT(Ep,theta,M,M_thr); 
   if(fIntegThresh==2) min_Es = GetEsMinALT(Ep,theta,M_A,M_thr); 
   Double_t max_Ep = GetEpMax(Es,theta); 
   if(fIntegThresh==0) max_Ep = GetEpMax(Es,theta); 
   if(fIntegThresh==1) max_Ep = GetEpMaxALT(Es,theta,M,M_thr); 
   if(fIntegThresh==2) max_Ep = GetEpMaxALT(Es,theta,M_A,M_thr); 
   Double_t min[2] = {min_Es     ,Ep + fDelta};
   Double_t max[2] = {Es - fDelta,max_Ep     };
   if( min[0] > max[0] ||  min[1] > max[1] ){ 
      return(0.0);
   }

   ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kADAPTIVE;//,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kMISER;
   double absErr                           = 1.0e-8;   /// desired absolute Error 
   double relErr                           = 1.0e-8;   /// desired relative Error 
   unsigned int ncalls                     = 10000;    /// number of calls (MC only)
   int rule                                = 1;        /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    
   ROOT::Math::IntegratorMultiDim ig(type,absErr,relErr,ncalls); 

   External2DEnergyIntegral_IntegrandWrap f1;
   f1.fRADCOR = this;
   f1.Es    = Es;
   f1.Ep    = Ep;
   f1.theta = theta;
   f1.phi   = phi;
   // f1.t1    = fKinematics.Gett_b()+tr;
   // f1.t2    = fKinematics.Gett_a()+tr;
   // I think t_r should be redefined based upon what Es' and Ep' are when integrating... 
   f1.t1    = fKinematics.Gett_b();
   f1.t2    = fKinematics.Gett_a();

   ig.SetFunction(f1);
  
   // std::cout << "t_b = " << fKinematics.Gett_b() << "\t" << "t_a = " << fKinematics.Gett_a() << std::endl;
 
   Double_t res1 = ig.Integral(min,max);
   return res1; 
}
//______________________________________________________________________________
Double_t RADCOR2::InternalOnly_EquivRad2DEnergyIntegral(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){
   // Approximation of MoTsai(1969) A.12 where the t integral is just set to evalute 
   // each Ib at t=T/2, which in this case t=tr.
   if(!fXS) {
      Error("InternalOnly_EquivRad2DEnergyIntegral","Cross section is NULL!");
      return 0.0;
   }
   fKinematics.SetZ(fXS->GetZ());
   fKinematics.SetA(fXS->GetA());
   fX0 = insane::Math::rad_length(fXS->GetZ(),fXS->GetA());
   SetKinematics(Ebeam,Eprime,theta,phi);
   Double_t Es     = fKinematics.GetEs();
   Double_t Ep     = fKinematics.GetEp();
   Double_t M      = fKinematics.GetM();
   Double_t tr     = GetTr(fKinematics.GetQ2());
   //std::cout << " tr = " << tr << std::endl;
   // First variable is Es
   Double_t min[2] = {GetEsMin(Ep,theta), Ep + fDelta};
   Double_t max[2] = {Es - fDelta,  GetEpMax(Es,theta)};
   if( min[0] > max[0] ||  min[1] > max[1] ){ 
      return(0.0);
   }

   ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kADAPTIVE;//,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kMISER;
   double absErr                           = 1.0e-4;   /// desired absolute Error 
   double relErr                           = 1.0e-4;   /// desired relative Error 
   unsigned int ncalls                     = 1000; /// number of calls (MC only)
   int rule                                = 1;        /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    
   ROOT::Math::IntegratorMultiDim ig(type,absErr,relErr,ncalls); 

   External2DEnergyIntegral_IntegrandWrap f1;
   f1.fRADCOR = this;
   f1.Es    = Es;
   f1.Ep    = Ep;
   f1.theta = theta;
   f1.phi   = phi;
   f1.t1    = tr;
   f1.t2    = tr;
   ig.SetFunction(f1);
   
   Double_t res1 = ig.Integral(min,max);
   return res1; 
}
//______________________________________________________________________________
Double_t RADCOR2::ExternalOnly_2DEnergyIntegral(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){
   // Approximation of MoTsai(1969) A.12 where the t integral is just set to evalute 
   // each Ib at t=T/2.
   if(!fXS) {
      Error("ExternalOnly_2DEnergyIntegral","Cross section is NULL!");
      return 0.0;
   }
   fKinematics.SetZ(fXS->GetZ());
   fKinematics.SetA(fXS->GetA());
   fX0 = insane::Math::rad_length(fXS->GetZ(),fXS->GetA());
   SetKinematics(Ebeam,Eprime,theta,phi);
   Double_t Es     = fKinematics.GetEs();
   Double_t Ep     = fKinematics.GetEp();
   Double_t M      = fKinematics.GetM();
   // First variable is Es
   Double_t min[2] = {GetEsMin(Ep,theta), Ep /*+ fDelta*/};
   Double_t max[2] = {Es /*- fDelta*/,  GetEpMax(Es,theta)};
   if( min[0] > max[0] ||  min[1] > max[1] ){ 
      return(0.0);
   }

   ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kADAPTIVE;//,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kMISER;
   double absErr                           = 1.0e-3;   /// desired absolute Error 
   double relErr                           = 1.0e-4;   /// desired relative Error 
   unsigned int ncalls                     = 1000; /// number of calls (MC only)
   int rule                                = 1;        /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    
   ROOT::Math::IntegratorMultiDim ig(type,absErr,relErr,ncalls); 

   External2DEnergyIntegral_IntegrandWrap f1;
   f1.fRADCOR = this;
   f1.Es    = Es;
   f1.Ep    = Ep;
   f1.theta = theta;
   f1.phi   = phi;
   f1.t1    = fKinematics.Gett_b();
   f1.t2    = fKinematics.Gett_a();
   ig.SetFunction(f1);
   
   Double_t res1 = ig.Integral(min,max);
   return res1; 
}
//______________________________________________________________________________
Double_t RADCOR2::External2DEnergyIntegral_Integrand(Double_t Es,Double_t Ep, Double_t theta, Double_t phi, Double_t t1, Double_t t2, Double_t EsPrime, Double_t EpPrime){
   // The integrand over the two energies in Mo and Tsai (1969)  A.12
   // I have seen this used with t1 = Tb and T2 = Ta as an approximation of the
   // t integral.
   Double_t M      = fKinematics.GetM();
   Double_t M_A    = fKinematics.GetM_A();
   Double_t b      = fKinematics.Getb(); 
   Double_t M_thr  = fDeltaM; 
   Double_t max_Ep = GetEpMax(EsPrime,theta);
   Double_t Q2     = insane::Kine::Qsquared(EsPrime,EpPrime,theta);
   Double_t t_r    = GetTr(Q2);
   Double_t t_b    = t1 + t_r;    
   Double_t t_a    = t2 + t_r;    
   if(fIntegThresh==0) max_Ep = GetEpMax(EsPrime,theta);
   if(fIntegThresh==1) max_Ep = GetEpMaxALT(EsPrime,theta,M,M_thr);
   if(fIntegThresh==2) max_Ep = GetEpMaxALT(EsPrime,theta,M_A,M_thr);

   // std::cout << M_A << "\t" << M_thr << "\t" << max_Ep << "\t" << EpPrime << std::endl; 
   if( max_Ep < EpPrime) return(0.0);
   // This is the Es' and Ep' integrand
   //std::cout << "Es = " << Es << std::endl;
   //std::cout << "Ep = " << Ep << std::endl;
   //std::cout << "Es'= " << EsPrime << std::endl;
   //std::cout << "Ep'= " << EpPrime << std::endl;
   //std::cout << "t1 = " << t1 << std::endl;
   // Double_t I1    = Ib(Es,EsPrime,t_b); 
   // Double_t I2    = Ib(EpPrime,Ep,t_a); 
   // redefine t_r for each Es' and Ep', so we use t_b = t1 + t_r, t_a = t2 + t_r  
   Double_t f_tilde = insane::Kine::F_tilde_internal(EsPrime,EpPrime,theta) + 0.5772*b*(t1 + t2);  
   Double_t I1      = Ib(Es,EsPrime,t_b); 
   Double_t I2      = Ib(EpPrime,Ep,t_a); 
   Double_t par[]   = {EpPrime,theta,phi};
   // FIXME: Shouldn't this cross section be the INTERNALLY-RADIATED cross section? 
   fXS->SetBeamEnergy(EsPrime);
   Double_t sig   = f_tilde*fXS->EvaluateBaseXSec(par);  // in the equiv. rad. approx., we need to multiply this xs by f_tilde 
   //std::cout << " I1 = " << I1 << std::endl;
   //std::cout << " I2 = " << I2 << std::endl;
   Double_t res = I1*sig*I2;
   //std::cout << " res = " << res << std::endl;
   return res;
}
//______________________________________________________________________________
Double_t RADCOR2::EquivRad3DIntegral(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){
   // Mo and Tsai (1969) A.12
   // 3D integration of strip approximation Region IV and t. 
   if(!fXS) {
      Error("EquivRad3DIntegral","Cross section is NULL!");
      return 0.0;
   }
   fKinematics.SetZ(fXS->GetZ());
   fKinematics.SetA(fXS->GetA());
   fX0 = insane::Math::rad_length(fXS->GetZ(),fXS->GetA());
   SetKinematics(Ebeam,Eprime,theta,phi);
   Double_t Es     = fKinematics.GetEs();
   Double_t Ep     = fKinematics.GetEp();
   Double_t M      = fKinematics.GetM();
   Double_t T      = fKinematics.GetT() + 2.0*GetTr(fKinematics.GetQ2());
   // variable order: Es',Ep',t
   Double_t min[3] = {GetEsMin(Ep,theta), Ep , 0.0};
   Double_t max[3] = {Es ,  GetEpMax(Es,theta), T};
   if( min[0] > max[0] ||  min[1] > max[1] ){ 
      return(0.0);
   }

   ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kADAPTIVE;//,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kMISER;
   double absErr                           = 1.0e-4;   /// desired absolute Error 
   double relErr                           = 1.0e-4;   /// desired relative Error 
   unsigned int ncalls                     = 100000; /// number of calls (MC only)
   int rule                                = 1;        /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    
   ROOT::Math::IntegratorMultiDim ig(type,absErr,relErr,ncalls); 

   External3DIntegral_IntegrandWrap f1;
   f1.fRADCOR = this;
   f1.Es    = Es;
   f1.Ep    = Ep;
   f1.theta = theta;
   f1.phi   = phi;
   f1.T     = T;
   ig.SetFunction(f1);
   Double_t res1 = ig.Integral(min,max);
   return res1/T; 
}
//______________________________________________________________________________
Double_t RADCOR2::EquivRad3DIntegral_RegionIV(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){
   // Mo and Tsai (1969) A.12
   // 3D integration of strip approximation Region IV and t. 
   if(!fXS) {
      Error("EquivRad3DIntegral_RegionIV","Cross section is NULL!");
      return 0.0;
   }
   fKinematics.SetZ(fXS->GetZ());
   fKinematics.SetA(fXS->GetA());
   fX0 = insane::Math::rad_length(fXS->GetZ(),fXS->GetA());
   SetKinematics(Ebeam,Eprime,theta,phi);
   Double_t Es     = fKinematics.GetEs();
   Double_t Ep     = fKinematics.GetEp();
   Double_t M      = fKinematics.GetM();
   Double_t T      = fKinematics.GetT() + 2.0*GetTr(fKinematics.GetQ2());
   // variable order: Es',Ep',t
   Double_t min[3] = {GetEsMin(Ep,theta), Ep + fDelta, 0.0};
   Double_t max[3] = {Es - fDelta,  GetEpMax(Es,theta), T};
   if( min[0] > max[0] ||  min[1] > max[1] ){ 
      return(0.0);
   }

   ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kADAPTIVE;//,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kMISER;
   double absErr                           = 1.0e-5;   /// desired absolute Error 
   double relErr                           = 1.0e-4;   /// desired relative Error 
   unsigned int ncalls                     = 1000000;  /// number of calls (MC only)
   int rule                                = 1;        /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    
   ROOT::Math::IntegratorMultiDim ig(type,absErr,relErr,ncalls); 

   External3DIntegral_IntegrandWrap f1;
   f1.fRADCOR = this;
   f1.Es    = Es;
   f1.Ep    = Ep;
   f1.theta = theta;
   f1.phi   = phi;
   f1.T     = T;
   ig.SetFunction(f1);
   Double_t res1 = ig.Integral(min,max);
   return res1/T; 
}
//______________________________________________________________________________
Double_t RADCOR2::External3DIntegral_Integrand(Double_t Es,  Double_t Ep, Double_t theta, Double_t phi, Double_t t1, Double_t t2, Double_t EsPrime, Double_t EpPrime){
   // The integrand over the target thickness and two energies in Mo and Tsai (1969)  A.12
   // Here t1 = t and t2 = T-t
   if( GetEpMax(EsPrime,theta) < EpPrime) return(0.0);
   // This is the Es' and Ep' integrand
   //std::cout << "Es = " << Es << std::endl;
   //std::cout << "Ep = " << Ep << std::endl;
   //std::cout << "Es'= " << EsPrime << std::endl;
   //std::cout << "Ep'= " << EpPrime << std::endl;
   //std::cout << "t1 = " << t1 << std::endl;
   Double_t I1 = Ib(Es,EsPrime,t1); 
   Double_t I2 = Ib(EpPrime,Ep,t2); 
   Double_t par[] = {EpPrime,theta,phi};
   fXS->SetBeamEnergy(EsPrime);
   //std::cout << " I1 = " << I1 << std::endl;
   //std::cout << " I2 = " << I2 << std::endl;
   Double_t res = I1*fXS->EvaluateBaseXSec(par)*I2;
   //std::cout << " res = " << res << std::endl;
   return res;
}
//______________________________________________________________________________
void RADCOR2::SetKinematics(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){
   fKinematics.SetEs(Ebeam); 
   fKinematics.SetEp(Eprime); 
   fKinematics.SetTheta(theta); 
   fKinematics.SetPhi(phi); 
   //CalculateCFACT();
}
//______________________________________________________________________________
Double_t RADCOR2::GetFTildeExactInternal(Double_t q2,Double_t Tb,Double_t Ta){

   /// Phys.Rev.D 12,1884 (A44)
   /// WARNING: q2, NOT Q2!
   Double_t thr   = fKinematics.GetTheta();
   Double_t Es    = fKinematics.GetEs();
   Double_t Ep    = fKinematics.GetEp();
   Double_t m     = fKinematics.Getm();
   Double_t b     = fKinematics.Getb();
   Double_t alpha = fine_structure_const; 
   Double_t SIN   = TMath::Sin(thr/2.); 
   Double_t SIN2  = SIN*SIN; 
   Double_t COS2  = 1. - SIN2; 

   Double_t T0    = 1. + 0.5772*b*(Ta + Tb);
   Double_t T1    = 2*(alpha/pi)*( (-14/9.) + (13./12.)*TMath::Log(q2/(m*m)) );
   Double_t T2    = -0.5*(alpha/pi)*TMath::Log(Es/Ep)*TMath::Log(Es/Ep);
   Double_t T3    = (alpha/pi)*( pi*pi/6.- TMath::DiLog(COS2) );
   Double_t F     = T0 + T1 + T2 + T3;

   // if(fDebug>5){ 
   //    std::cout << "-------- GetFTildeExactInternal -------" << std::endl;
   //    std::cout << "T0 = " << T0 << std::endl;
   //    std::cout << "T1 = " << T1 << std::endl;
   //    std::cout << "T2 = " << T2 << std::endl;
   //    std::cout << "T3 = " << T3 << std::endl;
   // }
   return F;
}
////______________________________________________________________________________
//Double_t RADCOR2::GetSpence(Double_t x){
//
//   // Double_t phi = -TMath::DiLog(-x);
//   // Checking against ROSETAIL and RADCOR (fortran), this is the
//   // form that gives consistent results: 
//   Double_t phi = TMath::DiLog(x);
//   return phi; 
//
//}
////________________________________________________________
//Double_t RADCOR2::GetSmearFunc(Double_t Q2){
//
//   Double_t Q,pf,pf2,f;
//   Double_t CONV = 1E+3;
//
//   pf  = 164.0/CONV;     // fermi momentum in GeV 
//   pf2 = pf*pf;
//   Q   = TMath::Sqrt(Q2)/pf;
//
//   if(Q2 < 4.*pf2){
//      f = 0.75*Q - (1./16.)*TMath::Power(Q,3.);
//   }else{
//      f = 1.;
//   }
//
//   return f;
//
//}
//______________________________________________________________________________
}}
