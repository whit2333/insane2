#include "insane/xsections/POLRADQuasiElasticTailDiffXSec.h"
namespace insane {
namespace physics {


POLRADQuasiElasticTailDiffXSec::POLRADQuasiElasticTailDiffXSec()
{
   fID         = 100010005;
   SetTitle("POLRADQuasiElasticTailDiffXSec");//,"POLRAD QRT cross-section");
   SetPlotTitle("POLRAD QuasiElastic tail XSec");
}
//______________________________________________________________________________

POLRADQuasiElasticTailDiffXSec::~POLRADQuasiElasticTailDiffXSec()
{ }
//______________________________________________________________________________

Double_t POLRADQuasiElasticTailDiffXSec::EvaluateXSec(const Double_t *x) const 
{
   if (!VariablesInPhaseSpace(fnDim, x)){
      std::cout << "[POLRADQuasiElasticTailDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
      return(0.0);
   }

   Double_t Eprime  = x[0];
   Double_t theta   = x[1];
   Double_t Ebeam   = GetBeamEnergy();
   Double_t nu      = Ebeam-Eprime;
   Double_t Mtarg   = (M_p/GeV)*fPOLRADCalc->fA;  

   fPOLRADCalc->SetKinematics(Ebeam,Eprime,theta);

   Double_t qrt = 0.0;

   //qrt = fPOLRADCalc->QRT_Full();
   qrt = fPOLRADCalc->QRT();

   Double_t sig_rad  = qrt;     

   // converts from dsigma/dxdy to dsigma/dEdOmega
   sig_rad = sig_rad * (Eprime/(2.0*TMath::Pi()*Mtarg*nu));
   if(IncludeJacobian()){
      sig_rad *= TMath::Sin(theta);
   }
   // convert from natural units (hbar*c)^2 = 0.00038939129 GeV^2 barn 
   // divide to return nanobarns
   //std::cout << "qrt = " << sig_rad << "\n";
   if( TMath::IsNaN(sig_rad) ) return 0.0;

   return fPOLRADCalc->fA*sig_rad*hbarc2_gev_nb;
} 
//______________________________________________________________________________

}}
