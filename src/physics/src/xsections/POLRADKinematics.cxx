#include "insane/xsections/POLRADKinematics.h"
#include "insane/base/PhysicalConstants.h"

namespace insane {
namespace physics {
//_____________________________________________________________________________
POLRADVariables::POLRADVariables(){
   fk1  = nullptr;
   fk2  = nullptr;
   fP   = nullptr;
   fXi  = nullptr;
   fEta = nullptr;
   Clear();
}
//_____________________________________________________________________________

POLRADVariables::~POLRADVariables(){
}
//_____________________________________________________________________________

void POLRADVariables::Update() {

   TLorentzVector &k1  = (*fk1);
   TLorentzVector &k2  = (*fk2);
   TLorentzVector &P   = (*fP);
   TLorentzVector &Xi  = (*fXi);
   TLorentzVector &Eta = (*fEta);
   fm         = M_e/GeV;
   fM         = M_p/GeV; 
   fP_A       = (fM_A/fM)*P;
   // kinematic variables 
   fq         = k1-k2; 
   fS         = 2.0*(k1*P);
   fS_A       = 2.0*(k1*fP_A);
   fX         = 2.0*(k2*P);
   fX_A       = 2.0*(k2*fP_A);
   fQ2        = -(fq*fq);
   fy         = 1.0 - (fX/fS);
   fy_A       = 1.0 - (fX_A/fS_A);
   fx         = fQ2/(fy*fS);
   fx_A       = fQ2/(fy_A*fS_A);
   fS_x       = fS-fX;
   fS_xA      = fS_A-fX_A;
   fQ2_m      = fQ2 + 2.0*fm*fm;
   fS_p       = fS+fX;
   fS_pA      = fS_A+fX_A;
   fLambda_s  = fS*fS-4.0*fm*fm*fM*fM;
   fLambda_sA = fS_A*fS_A-4.0*fm*fm*fM_A*fM_A;
   fLambda_m  = fQ2*fQ2 + 4.*fm*fm*fQ2; 
   fL_m       = (1./TMath::Sqrt(fLambda_m) )*TMath::Log( (TMath::Sqrt(fLambda_m)+fQ2) / (TMath::Sqrt(fLambda_m)-fQ2) );
   fW2        = fS_x - fQ2 + fM*fM;
   fW         = TMath::Sqrt(fW2); 
   fLambda_Q  = fS_x*fS_x   + 4.0*fM*fM*fQ2;
   fLambda_QA = fS_xA*fS_xA + 4.0*fM_A*fM_A*fQ2;
   fl_m       = TMath::Log( fQ2/(fm*fm) );      
   fz_s       = (1.-fy)/(1.-fx*fy); 
   fz_p       = 1. - fy + fx*fy; 

   fS_prime       = fX + fQ2; 
   fX_prime       = fS - fQ2; 
   fLambda_Sprime = fS_prime*fS_prime - 4.*fW2*fm*fm; 
   fLambda_Xprime = fX_prime*fX_prime - 4.*fW2*fm*fm; 
   fL_Sprime      = (1./TMath::Sqrt(fLambda_Sprime))*TMath::Log( (fS_prime + TMath::Sqrt(fLambda_Sprime) )/(fS_prime - TMath::Sqrt(fLambda_Sprime)) ); 
   fL_Xprime      = (1./TMath::Sqrt(fLambda_Xprime))*TMath::Log( (fX_prime + TMath::Sqrt(fLambda_Xprime) )/(fX_prime - TMath::Sqrt(fLambda_Xprime)) ); 
   fa_prime       = (0.5/fW2)*(fS*fX - fM*fM*fQ2 - fW2*(fQ2 + 4.*fm*fm) ); 
   fb_prime       = (1./fW2)*(fQ2*(fS*fX - fM*fM*fQ2) - fm*fm*fLambda_Q);

   fTau_A_min = (fS_xA - TMath::Sqrt(fLambda_QA))/(2.0*fM_A*fM_A);
   fTau_A_max = (fS_xA + TMath::Sqrt(fLambda_QA))/(2.0*fM_A*fM_A);
   fTau_min   = (fS_x  - TMath::Sqrt(fLambda_Q))/(2.0*fM*fM);
   fTau_max   = (fS_x  + TMath::Sqrt(fLambda_Q))/(2.0*fM*fM);

   // for radiative corrections 
   fEs        = k1.T();           // time component has the beam e- energy 
   fEp        = k2.T();           // time component has the scattered e- energy 

   GetHPCoefficients();
   fa_eta = fHyperPlaneCoeffs[0];
   fb_eta = fHyperPlaneCoeffs[1];
   fc_eta = fHyperPlaneCoeffs[2];
   fa_xi = fHyperPlaneCoeffs[3];
   fb_xi = fHyperPlaneCoeffs[4];
   fc_xi = fHyperPlaneCoeffs[5];

   GetHPCoefficients_A();
   fa_eta_A = fHyperPlaneCoeffs[0];
   fb_eta_A = fHyperPlaneCoeffs[1];
   fc_eta_A = fHyperPlaneCoeffs[2];
   fa_xi_A = fHyperPlaneCoeffs[3];
   fb_xi_A = fHyperPlaneCoeffs[4];
   fc_xi_A = fHyperPlaneCoeffs[5];

   // Using POLRAD 2.0 definitions
   // xi coefficients 
   Double_t a_xi_1   = fS/(fm*TMath::Sqrt(fLambda_s))/2.0;
   Double_t b_xi_1   = 0.0/2.0;
   Double_t c_xi_1   = -2.*fm/TMath::Sqrt(fLambda_s)/2.0;
   Double_t a_xi_A_1 = fS_A/(fm*TMath::Sqrt(fLambda_sA))/2.0;
   Double_t b_xi_A_1 = 0.0/2.0;
   Double_t c_xi_A_1 = -2.*fm/TMath::Sqrt(fLambda_sA)/2.0;
   // eta coefficients (longitudinal polarization!)  
   Double_t a_eta_1   = 2*fM/TMath::Sqrt(fLambda_s)/2.0;
   Double_t b_eta_1   = 0;
   Double_t c_eta_1   = -fS/fM/TMath::Sqrt(fLambda_s)/2.0;
   Double_t a_eta_A_1 = 2*fM_A/TMath::Sqrt(fLambda_sA)/2.0;
   Double_t b_eta_A_1 = 0;
   Double_t c_eta_A_1 = -fS_A/fM_A/TMath::Sqrt(fLambda_sA)/2.0;

   TLorentzVector eta_L  = 2.0*((a_eta_1*k1) + (b_eta_1*k2) + (c_eta_1*P) ) ;
   TLorentzVector eta_LA = 2.0*((a_eta_A_1*k1) + (b_eta_A_1*k2) + (c_eta_A_1*P));

   // eta transverse components
   Double_t eta_2_denom = 2.0*TMath::Sqrt(fLambda_s*(fS*fX*fQ2 - fm*fm*fS_x*fS_x - fM*fM*fQ2*fQ2 - 4.0*fm*fm*fM*fM*fQ2));
   Double_t a_eta_2   = (-1.0*fS*fX+2.0*fM*fM*fQ2_m)/eta_2_denom;
   Double_t b_eta_2   = fLambda_s/eta_2_denom;
   Double_t c_eta_2   = -1.0*(fS*fQ2 + 2.0*fm*fm*fS_x)/eta_2_denom;

   Double_t eta_A_2_denom = 2.0*TMath::Sqrt(fLambda_sA*(fS_A*fX_A*fQ2 - fm*fm*fS_xA*fS_xA - fM_A*fM_A*fQ2*fQ2 - 4.0*fm*fm*fM_A*fM_A*fQ2));
   Double_t a_eta_A_2   = (-1.0*fS_A*fX_A+2.0*fM_A*fM_A*fQ2_m)/eta_2_denom;
   Double_t b_eta_A_2   = fLambda_sA/eta_2_denom;
   Double_t c_eta_A_2   = -1.0*(fS_A*fQ2 + 2.0*fm*fm*fS_xA)/eta_2_denom;

   TLorentzVector eta_T  = 2.0*((a_eta_2*k1) + (b_eta_2*k2) + (c_eta_2*P) );
   TLorentzVector eta_TA = 2.0*((a_eta_A_2*k1) + (b_eta_A_2*k2) + (c_eta_A_2*P));

   Double_t k_L = -Eta*eta_L;
   Double_t k_T = -Eta*eta_T;
   Double_t k_LA = -Eta*eta_LA;
   Double_t k_TA = -Eta*eta_TA;


   // eta 
   fa_eta   = a_eta_1*k_L + a_eta_2*k_T; 
   fb_eta   = b_eta_1*k_L + a_eta_2*k_T; 
   fc_eta   = c_eta_1*k_L + c_eta_2*k_T; 
   fa_eta_A = a_eta_A_1*k_LA + a_eta_A_2*k_TA; 
   fb_eta_A = b_eta_A_1*k_LA + a_eta_A_2*k_TA; 
   fc_eta_A = c_eta_A_1*k_LA + c_eta_A_2*k_TA; 
   
   //Eta.Print();
   //Xi.Print();
   //std::cout << "a_mathematica - a_POLRAD = " << fa_xi << " - " << a_xi_1 << " = " << fa_xi - a_xi_1 << "\n";
   //std::cout << "b_mathematica - b_POLRAD = " << fb_xi << " - " << b_xi_1 << " = " << fb_xi - b_xi_1 << "\n";
   //std::cout << "c_mathematica - c_POLRAD = " << fc_xi << " - " << c_xi_1 << " = " << fc_xi - c_xi_1 << "\n";
   //std::cout << "a_mathematica - a_POLRAD = " << fa_eta << " - " << a_eta_1 << " = " << fa_eta - a_eta_1 << "\n";
   //std::cout << "b_mathematica - b_POLRAD = " << fb_eta << " - " << b_eta_1 << " = " << fb_eta - b_eta_1 << "\n";
   //std::cout << "c_mathematica - c_POLRAD = " << fc_eta << " - " << c_eta_1 << " = " << fc_eta - c_eta_1 << "\n";

   // Set the coefficients 
   // xi 
   fa_xi    = a_xi_1;
   fb_xi    = b_xi_1;
   fc_xi    = c_xi_1;
   fa_xi_A  = a_xi_A_1; 
   fb_xi_A  = b_xi_A_1; 
   fc_xi_A  = c_xi_A_1; 
   // eta 
   //fa_eta   = a_eta_1; 
   //fb_eta   = b_eta_1; 
   //fc_eta   = c_eta_1;
   //fa_eta_A = a_eta_A_1; 
   //fb_eta_A = b_eta_A_1; 
   //fc_eta_A = c_eta_A_1;

   //Print();

}
//_____________________________________________________________________________

//_____________________________________________________________________________
void POLRADVariables::Print() {

   std::cout << "------------------- POLRAD Kinematics (Nucleon) Variables -------------------" << std::endl;
   std::cout << "x:              " << fx                                                        << std::endl;
   std::cout << "y:              " << fy                                                        << std::endl;
   std::cout << "Q2:             " << fQ2            << " GeV^2"                                << std::endl;
   std::cout << "S:              " << fS             << " GeV^2"                                << std::endl;
   std::cout << "X:              " << fX             << " GeV^2"                                << std::endl;
   std::cout << "S_x:            " << fS_x           << " GeV^2"                                << std::endl;
   std::cout << "S_p:            " << fS_p           << " GeV^2"                                << std::endl;
   std::cout << "Q2_m:           " << fQ2_m          << " GeV^2"                                << std::endl; 
   std::cout << "W2:             " << fW2            << " GeV^2"                                << std::endl;
   std::cout << "Lambda_s:       " << fLambda_s                                                 << std::endl;
   std::cout << "Lambda_Q:       " << fLambda_Q                                                 << std::endl;
   std::cout << "tau_min:        " << fTau_min                                                  << std::endl;
   std::cout << "tau_max:        " << fTau_max                                                  << std::endl;
   std::cout << "fa_eta:         " << fa_eta                                                    << std::endl;
   std::cout << "fb_eta:         " << fb_eta                                                    << std::endl;
   std::cout << "fc_eta:         " << fc_eta                                                    << std::endl;
   std::cout << "fa_xi:          " << fa_xi                                                     << std::endl;
   std::cout << "fb_xi:          " << fb_xi                                                     << std::endl;
   std::cout << "fc_xi:          " << fc_xi                                                     << std::endl;
   std::cout << "------------------- POLRAD Kinematics (Nucleus) Variables -------------------" << std::endl;
   std::cout << "S_A:            " << fS_A           << " GeV^2"                                << std::endl;
   std::cout << "X_A:            " << fX_A           << " GeV^2"                                << std::endl;
   std::cout << "S_xA:           " << fS_x           << " GeV^2"                                << std::endl;
   std::cout << "S_pA:           " << fS_pA          << " GeV^2"                                << std::endl;
   std::cout << "Lambda_sA:      " << fLambda_sA                                                << std::endl;
   std::cout << "Lambda_QA:      " << fLambda_QA                                                << std::endl;
   std::cout << "tau_min_A:      " << fTau_min                                                  << std::endl;
   std::cout << "tau_max_A:      " << fTau_max                                                  << std::endl;
   std::cout << "fa_eta_A:       " << fa_eta_A                                                  << std::endl;
   std::cout << "fb_eta_A:       " << fb_eta_A                                                  << std::endl;
   std::cout << "fc_eta_A:       " << fc_eta_A                                                  << std::endl;
   std::cout << "fa_xi_A:        " << fa_xi_A                                                   << std::endl;
   std::cout << "fb_xi_A:        " << fb_xi_A                                                   << std::endl;
   std::cout << "fc_xi_A:        " << fc_xi_A                                                   << std::endl;
   std::cout << "tau_min_A:      " << fTau_A_min                                                << std::endl;
   std::cout << "tau_max_A:      " << fTau_A_max                                                << std::endl;
   std::cout << "-------------------       POLRAD External RC Variables    -------------------" << std::endl;
   std::cout << "Es:             " << fEs                                                       << std::endl;
   std::cout << "Ep:             " << fEp                                                       << std::endl;
   std::cout << "theta:          " << fTheta/degree                                             << std::endl;
   std::cout << "-------------------          POLRAD Mass Variables        -------------------" << std::endl;
   std::cout << "electron mass:        " << fm        << " GeV"                                 << std::endl; 
   std::cout << "proton mass:          " << fM        << " GeV"                                 << std::endl;
   std::cout << "target mass:          " << fM_A      << " GeV"                                 << std::endl; 

   // std::cout << "-------------------   POLRAD SFs and FFs  -------------------" << std::endl;
   // PrintSF();
   // std::cout << "-------------------   POLRAD 4-Vectors    -------------------" << std::endl; 
   // fXi->Print();
   // fEta->Print();
   // fk1->Print();
   // fk2->Print();
   // fP->Print();
}
//_____________________________________________________________________________
void POLRADVariables::Clear(){
   /// Nucleon variables 
   fx             = 0;
   fy             = 0;
   fW             = 0;
   fQ2            = 0;
   fQ2_m          = 0;
   fS             = 0;
   fS_x           = 0;
   fS_p           = 0;
   fX             = 0;
   fm             = 0;
   fM             = 0;
   fLambda_Q      = 0;
   fLambda_s      = 0;
   fLambda_m      = 0;
   fL_m           = 0;
   fl_m           = 0;
   fz_s           = 0;
   fz_p           = 0;
   fS_prime       = 0;
   fX_prime       = 0;
   fLambda_Sprime = 0;
   fLambda_Xprime = 0;
   fL_Sprime      = 0;
   fL_Xprime      = 0;
   fa_prime       = 0;
   fb_prime       = 0;
   fa_eta         = 0;
   fb_eta         = 0;
   fc_eta         = 0;
   fa_xi          = 0;
   fb_xi          = 0;
   fc_xi          = 0;
   fTau_min       = 0;
   fTau_max       = 0;
   /// Nucleus variables 
   fx_A           = 0;
   fS_A           = 0;
   fS_xA          = 0;
   fS_pA          = 0;
   fX_A           = 0;
   fM_A           = 0;
   fLambda_QA     = 0;
   fLambda_sA     = 0;
   fLambda_QA     = 0;
   fa_eta_A       = 0;
   fb_eta_A       = 0;
   fc_eta_A       = 0;
   fa_xi_A        = 0;
   fb_xi_A        = 0;
   fc_xi_A        = 0;
   fTau_A_min     = 0;
   fTau_A_max     = 0;
}
//_____________________________________________________________________________

//______________________________________________________________________________
Double_t * POLRADVariables::GetHPCoefficients(){

   // Solution to POLRAD B.10  for the six a,b,c coefficients

   using namespace TMath;
   TLorentzVector &k1  = (*fk1);
   TLorentzVector &k2  = (*fk1);
   TLorentzVector &P   = (*fP);
   TLorentzVector &Xi  = (*fXi);
   TLorentzVector &Eta = (*fEta);
   fKappa        = k1 + k2;
   eta_dot_q     = Eta*fq;
   eta_dot_p     = Eta*P;
   eta_dot_kappa = Eta*fKappa;
   xi_dot_eta    = Xi*Eta;
   xi_dot_p      = Xi*P;
   k2_dot_xi     = k2*Xi;
   Q2            = -1.0*(fq*fq);
   S             =  2.0*(k1*P);
   X             =  2.0*(k2*P);
   Sx            =  S - X;
   Sp            =  S + X;
   m2            = fm*fm;
   M2            = fM*fM;
   Q2m           = Q2 + 2.0*m2;

   Double_t den_eta = 4.*m2*(4.*M2*Q2 + Sx*(S - X)) + Q2*(4.*M2*Q2 + S*(-Sp + Sx) - (Sp + Sx)*X); 
   Double_t den_xi  = (4.*(8.*eta_dot_q*m2*M2 - 4.*eta_dot_p*m2*Sx + Q2*(-2.*eta_dot_kappa*M2 + 2.*eta_dot_q*M2 + eta_dot_p*Sp - eta_dot_p*Sx) - eta_dot_q*Sp*X 
            + eta_dot_kappa*Sx*X)*(4.*Power(m2,2.)*M2 + Q2m*(-(M2*Q2m) + S*X) - m2*(Power(S,2.) + Power(X,2.)))); 

   /// a_eta
   fHyperPlaneCoeffs[0] = (-8.*eta_dot_q*m2*M2 + 4.*eta_dot_p*m2*Sx  
         + Q2*(2.*eta_dot_kappa*M2 - 2.*eta_dot_q*M2 - eta_dot_p*Sp + eta_dot_p*Sx) 
         + eta_dot_q*Sp*X - eta_dot_kappa*Sx*X)/den_eta;
   /// b_eta
   fHyperPlaneCoeffs[1] = (8.*eta_dot_q*m2*M2 - eta_dot_q*S*Sp - 4.*eta_dot_p*m2*Sx + eta_dot_kappa*S*Sx 
         - Q2*(-2.*(eta_dot_kappa + eta_dot_q)*M2 + eta_dot_p*(Sp + Sx)))/den_eta;
   /// c_eta
   fHyperPlaneCoeffs[2] = ((4.*m2 + Q2)*(2.*eta_dot_p*Q2 + eta_dot_q*(S - X)) - eta_dot_kappa*Q2*(S + X))/den_eta;

   /// a_xi
   fHyperPlaneCoeffs[3] = (2.*k2_dot_xi*(4.*(eta_dot_kappa - eta_dot_q)*Power(M2,2.)*Q2*Q2m + 16.*Power(m2,2.)*M2*(2.*eta_dot_q*M2 - eta_dot_p*Sx) 
            + eta_dot_p*Q2*X*(S*(Sp - Sx) + (Sp + Sx)*X) + 4.*m2*(2.*Power(M2,2.)*((eta_dot_kappa + eta_dot_q)*Q2 - 2.*eta_dot_q*Q2m) 
               + eta_dot_p*Sx*X*(-S + X) - M2*(eta_dot_q*S*Sp - 2.*eta_dot_p*Q2m*Sx - eta_dot_kappa*S*Sx + eta_dot_p*Q2*(Sp + Sx) 
                  - 2.*eta_dot_q*S*X + 2.*eta_dot_q*Power(X,2.))) 
            - 2.*M2*(Q2m*(-(eta_dot_q*Sp) + eta_dot_kappa*Sx)*X + Q2*(eta_dot_p*Q2m*(Sp - Sx) + eta_dot_q*X*(-S + X) + eta_dot_kappa*X*(S + X)))) 
         - (4.*m2*M2 - Power(X,2.))*(4.*m2*(4.*M2*Q2 + Sx*(S - X)) + Q2*(4.*M2*Q2 + S*(-Sp + Sx) - (Sp + Sx)*X))*xi_dot_eta 
         + 16.*Power(m2,2.)*(4.*eta_dot_p*M2*Q2 + eta_dot_p*S*Sx - 2.*eta_dot_q*M2*X)*xi_dot_p 
         + 4.*m2*(M2*(4.*eta_dot_p*Power(Q2,2.) - 2.*(eta_dot_kappa + eta_dot_q)*Q2*X + 4.*eta_dot_q*Q2m*X) 
            + eta_dot_p*Q2*(-(S*Sp) + S*Sx - 4.*Power(X,2.)) + X*(eta_dot_q*S*Sp - 2.*eta_dot_p*Q2m*Sx - eta_dot_kappa*S*Sx - 2.*eta_dot_q*S*X 
               + 2.*eta_dot_q*Power(X,2.)))*xi_dot_p + 2.*X*(-2.*eta_dot_p*Power(Q2,2.)*X + Q2m*(-(eta_dot_q*Sp) + eta_dot_kappa*Sx)*X + 
            Q2*(Q2m*(-2.*eta_dot_kappa*M2 + 2.*eta_dot_q*M2 + eta_dot_p*Sp - eta_dot_p*Sx) + eta_dot_q*X*(-S + X) + eta_dot_kappa*X*(S + X)))*xi_dot_p)/
      den_xi;

   /// b_xi
   fHyperPlaneCoeffs[4] = (2.*k2_dot_xi*(-4.*(eta_dot_kappa + eta_dot_q)*Power(M2,2.)*Q2*Q2m + 16.*Power(m2,2.)*M2*(2.*eta_dot_q*M2 - eta_dot_p*Sx) 
            - eta_dot_p*Q2*S*(S*(Sp - Sx) + (Sp + Sx)*X) + 4.*m2*(-2.*Power(M2,2.)*((eta_dot_kappa - eta_dot_q)*Q2 + 2.*eta_dot_q*Q2m) 
               + eta_dot_p*S*Sx*(S - X) + M2*(eta_dot_p*Q2*(Sp - Sx) + 2.*eta_dot_p*Q2m*Sx - eta_dot_q*Sp*X + eta_dot_kappa*Sx*X 
                  + 2.*eta_dot_q*S*(-S + X))) + 2.*M2*(Q2m*S*(eta_dot_q*Sp - eta_dot_kappa*Sx) + Q2*(eta_dot_p*Q2m*(Sp + Sx) + S*((eta_dot_kappa - eta_dot_q)*S 
                     + (eta_dot_kappa + eta_dot_q)*X)))) + 4.*m2*(4.*M2*Q2 + Sx*(S - X))*(2.*M2*Q2m - S*X)*xi_dot_eta + Q2*(2.*M2*Q2m - S*X)*(4.*M2*Q2 + S*(-Sp + Sx) 
            - (Sp + Sx)*X)*xi_dot_eta + 16.*Power(m2,2.)*(-2.*eta_dot_q*M2 + eta_dot_p*Sx)*X*xi_dot_p - 4.*m2*(2.*eta_dot_p*Q2m*S*Sx 
            + X*(-2.*eta_dot_q*Power(S,2.) + eta_dot_p*Q2*(-4.*S + Sp - Sx) + 2.*eta_dot_q*S*X - eta_dot_q*Sp*X + eta_dot_kappa*Sx*X)  
            + 2.*M2*(-2.*eta_dot_q*Q2m*X + Q2*(4.*eta_dot_p*Q2m - eta_dot_kappa*X + eta_dot_q*X)))*xi_dot_p 
         - 2.*(2.*M2*Q2*Q2m*(2.*eta_dot_p*Q2 - (eta_dot_kappa + eta_dot_q)*X) + S*(-2.*eta_dot_p*Power(Q2,2.)*X 
               + Q2m*(eta_dot_q*Sp - eta_dot_kappa*Sx)*X + Q2*(eta_dot_p*Q2m*(-Sp + Sx) + eta_dot_q*X*(-S + X) + eta_dot_kappa*X*(S + X))))*xi_dot_p)/
      den_xi;

   /// c_xi
   fHyperPlaneCoeffs[5] = (-2.*k2_dot_xi*(-8.*Power(m2,2.)*(-2.*eta_dot_q*M2 + eta_dot_p*Sx)*(S + X) + Q2m*(2.*S*(eta_dot_q*Sp - eta_dot_kappa*Sx)*X 
               + Q2*(S*(2.*eta_dot_kappa*M2 - 2.*eta_dot_q*M2 - eta_dot_p*Sp + eta_dot_p*Sx) + (-2.*(eta_dot_kappa + eta_dot_q)*M2 + eta_dot_p*(Sp + Sx))*X)) 
            + 2.*m2*(2.*Q2m*(-2.*eta_dot_q*M2 + eta_dot_p*Sx)*(S + X) - Q2*(S*(-2.*(eta_dot_kappa + eta_dot_q)*M2 + eta_dot_p*(Sp + Sx)) 
                  + (2.*eta_dot_kappa*M2 - 2.*eta_dot_q*M2 - eta_dot_p*Sp + eta_dot_p*Sx)*X) - (eta_dot_q*Sp - eta_dot_kappa*Sx)*(Power(S,2.) + Power(X,2.)))) 
         + (2.*m2*S - Q2m*X)*(4.*m2*(4.*M2*Q2 + Sx*(S - X)) + Q2*(4.*M2*Q2 + S*(-Sp + Sx) - (Sp + Sx)*X))*xi_dot_eta 
         - 32.*Power(m2,3.)*(-2.*eta_dot_q*M2 + eta_dot_p*Sx)*xi_dot_p - 8.*Power(m2,2.)*(2.*eta_dot_q*Power(S,2.) + Q2*(2.*(eta_dot_kappa - eta_dot_q)*M2 
               + eta_dot_p*(4.*S - Sp + Sx)) - 2.*eta_dot_q*S*X + eta_dot_q*Sp*X - eta_dot_kappa*Sx*X)*xi_dot_p - 2.*Q2m*(-2.*eta_dot_p*Power(Q2,2.)*X 
            + Q2m*(-(eta_dot_q*Sp) + eta_dot_kappa*Sx)*X + Q2*(Q2m*(-2.*eta_dot_kappa*M2 + 2.*eta_dot_q*M2 + eta_dot_p*Sp - eta_dot_p*Sx) 
               + eta_dot_q*X*(-S + X) + eta_dot_kappa*X*(S + X)))*xi_dot_p + 4.*m2*(-2.*eta_dot_p*Power(Q2,2.)*S + 2.*Q2m*(Q2m*(-2.*eta_dot_q*M2 + eta_dot_p*Sx) 
               + eta_dot_q*(S - X)*X) + Q2*(4.*eta_dot_p*Q2m*X + S*((eta_dot_kappa - eta_dot_q)*S + (eta_dot_kappa + eta_dot_q)*X)))*xi_dot_p)/
      den_xi;

   return(fHyperPlaneCoeffs);

}
//______________________________________________________________________________
Double_t * POLRADVariables::GetHPCoefficients_A(){
   using namespace TMath;
   TLorentzVector &k1  = (*fk1);
   TLorentzVector &k2  = (*fk1);
   TLorentzVector &P   = (*fP);
   TLorentzVector &Xi  = (*fXi);
   TLorentzVector &Eta = (*fEta);
   fKappa        = k1 + k2;
   eta_dot_q     = Eta*fq;
   eta_dot_p     = Eta*fP_A;
   eta_dot_kappa = Eta*fKappa;
   xi_dot_eta    = Xi*Eta;
   xi_dot_p      = Xi*fP_A;
   k2_dot_xi     = k2*Xi;
   Q2            = -1.0*(fq*fq);
   S             =  2.0*(k1*fP_A);
   X             =  2.0*(k2*fP_A);
   Sx            =  S - X;
   Sp            =  S + X;
   m2            = fm*fm;
   M2            = fM_A*fM_A;
   Q2m           = Q2 + 2.0*m2;

   Double_t den_eta = 4.*m2*(4.*M2*Q2 + Sx*(S - X)) + Q2*(4.*M2*Q2 + S*(-Sp + Sx) - (Sp + Sx)*X); 
   Double_t den_xi  = (4.*(8.*eta_dot_q*m2*M2 - 4.*eta_dot_p*m2*Sx + Q2*(-2.*eta_dot_kappa*M2 + 2.*eta_dot_q*M2 + eta_dot_p*Sp - eta_dot_p*Sx) - eta_dot_q*Sp*X 
            + eta_dot_kappa*Sx*X)*(4.*Power(m2,2.)*M2 + Q2m*(-(M2*Q2m) + S*X) - m2*(Power(S,2.) + Power(X,2.)))); 

   /// a_eta
   fHyperPlaneCoeffs[0] = (-8.*eta_dot_q*m2*M2 + 4.*eta_dot_p*m2*Sx  
         + Q2*(2.*eta_dot_kappa*M2 - 2.*eta_dot_q*M2 - eta_dot_p*Sp + eta_dot_p*Sx) 
         + eta_dot_q*Sp*X - eta_dot_kappa*Sx*X)/den_eta;
   /// b_eta
   fHyperPlaneCoeffs[1] = (8.*eta_dot_q*m2*M2 - eta_dot_q*S*Sp - 4.*eta_dot_p*m2*Sx + eta_dot_kappa*S*Sx 
         - Q2*(-2.*(eta_dot_kappa + eta_dot_q)*M2 + eta_dot_p*(Sp + Sx)))/den_eta;
   /// c_eta
   fHyperPlaneCoeffs[2] = ((4.*m2 + Q2)*(2.*eta_dot_p*Q2 + eta_dot_q*(S - X)) - eta_dot_kappa*Q2*(S + X))/den_eta;

   /// a_xi
   fHyperPlaneCoeffs[3] = (2.*k2_dot_xi*(4.*(eta_dot_kappa - eta_dot_q)*Power(M2,2.)*Q2*Q2m + 16.*Power(m2,2.)*M2*(2.*eta_dot_q*M2 - eta_dot_p*Sx) 
            + eta_dot_p*Q2*X*(S*(Sp - Sx) + (Sp + Sx)*X) + 4.*m2*(2.*Power(M2,2.)*((eta_dot_kappa + eta_dot_q)*Q2 - 2.*eta_dot_q*Q2m) 
               + eta_dot_p*Sx*X*(-S + X) - M2*(eta_dot_q*S*Sp - 2.*eta_dot_p*Q2m*Sx - eta_dot_kappa*S*Sx + eta_dot_p*Q2*(Sp + Sx) 
                  - 2.*eta_dot_q*S*X + 2.*eta_dot_q*Power(X,2.))) 
            - 2.*M2*(Q2m*(-(eta_dot_q*Sp) + eta_dot_kappa*Sx)*X + Q2*(eta_dot_p*Q2m*(Sp - Sx) + eta_dot_q*X*(-S + X) + eta_dot_kappa*X*(S + X)))) 
         - (4.*m2*M2 - Power(X,2.))*(4.*m2*(4.*M2*Q2 + Sx*(S - X)) + Q2*(4.*M2*Q2 + S*(-Sp + Sx) - (Sp + Sx)*X))*xi_dot_eta 
         + 16.*Power(m2,2.)*(4.*eta_dot_p*M2*Q2 + eta_dot_p*S*Sx - 2.*eta_dot_q*M2*X)*xi_dot_p 
         + 4.*m2*(M2*(4.*eta_dot_p*Power(Q2,2.) - 2.*(eta_dot_kappa + eta_dot_q)*Q2*X + 4.*eta_dot_q*Q2m*X) 
            + eta_dot_p*Q2*(-(S*Sp) + S*Sx - 4.*Power(X,2.)) + X*(eta_dot_q*S*Sp - 2.*eta_dot_p*Q2m*Sx - eta_dot_kappa*S*Sx - 2.*eta_dot_q*S*X 
               + 2.*eta_dot_q*Power(X,2.)))*xi_dot_p + 2.*X*(-2.*eta_dot_p*Power(Q2,2.)*X + Q2m*(-(eta_dot_q*Sp) + eta_dot_kappa*Sx)*X + 
            Q2*(Q2m*(-2.*eta_dot_kappa*M2 + 2.*eta_dot_q*M2 + eta_dot_p*Sp - eta_dot_p*Sx) + eta_dot_q*X*(-S + X) + eta_dot_kappa*X*(S + X)))*xi_dot_p)/
      den_xi;

   /// b_xi
   fHyperPlaneCoeffs[4] = (2.*k2_dot_xi*(-4.*(eta_dot_kappa + eta_dot_q)*Power(M2,2.)*Q2*Q2m + 16.*Power(m2,2.)*M2*(2.*eta_dot_q*M2 - eta_dot_p*Sx) 
            - eta_dot_p*Q2*S*(S*(Sp - Sx) + (Sp + Sx)*X) + 4.*m2*(-2.*Power(M2,2.)*((eta_dot_kappa - eta_dot_q)*Q2 + 2.*eta_dot_q*Q2m) 
               + eta_dot_p*S*Sx*(S - X) + M2*(eta_dot_p*Q2*(Sp - Sx) + 2.*eta_dot_p*Q2m*Sx - eta_dot_q*Sp*X + eta_dot_kappa*Sx*X 
                  + 2.*eta_dot_q*S*(-S + X))) + 2.*M2*(Q2m*S*(eta_dot_q*Sp - eta_dot_kappa*Sx) + Q2*(eta_dot_p*Q2m*(Sp + Sx) + S*((eta_dot_kappa - eta_dot_q)*S 
                     + (eta_dot_kappa + eta_dot_q)*X)))) + 4.*m2*(4.*M2*Q2 + Sx*(S - X))*(2.*M2*Q2m - S*X)*xi_dot_eta + Q2*(2.*M2*Q2m - S*X)*(4.*M2*Q2 + S*(-Sp + Sx) 
            - (Sp + Sx)*X)*xi_dot_eta + 16.*Power(m2,2.)*(-2.*eta_dot_q*M2 + eta_dot_p*Sx)*X*xi_dot_p - 4.*m2*(2.*eta_dot_p*Q2m*S*Sx 
            + X*(-2.*eta_dot_q*Power(S,2.) + eta_dot_p*Q2*(-4.*S + Sp - Sx) + 2.*eta_dot_q*S*X - eta_dot_q*Sp*X + eta_dot_kappa*Sx*X)  
            + 2.*M2*(-2.*eta_dot_q*Q2m*X + Q2*(4.*eta_dot_p*Q2m - eta_dot_kappa*X + eta_dot_q*X)))*xi_dot_p 
         - 2.*(2.*M2*Q2*Q2m*(2.*eta_dot_p*Q2 - (eta_dot_kappa + eta_dot_q)*X) + S*(-2.*eta_dot_p*Power(Q2,2.)*X 
               + Q2m*(eta_dot_q*Sp - eta_dot_kappa*Sx)*X + Q2*(eta_dot_p*Q2m*(-Sp + Sx) + eta_dot_q*X*(-S + X) + eta_dot_kappa*X*(S + X))))*xi_dot_p)/
      den_xi;

   /// c_xi
   fHyperPlaneCoeffs[5] = (-2.*k2_dot_xi*(-8.*Power(m2,2.)*(-2.*eta_dot_q*M2 + eta_dot_p*Sx)*(S + X) + Q2m*(2.*S*(eta_dot_q*Sp - eta_dot_kappa*Sx)*X 
               + Q2*(S*(2.*eta_dot_kappa*M2 - 2.*eta_dot_q*M2 - eta_dot_p*Sp + eta_dot_p*Sx) + (-2.*(eta_dot_kappa + eta_dot_q)*M2 + eta_dot_p*(Sp + Sx))*X)) 
            + 2.*m2*(2.*Q2m*(-2.*eta_dot_q*M2 + eta_dot_p*Sx)*(S + X) - Q2*(S*(-2.*(eta_dot_kappa + eta_dot_q)*M2 + eta_dot_p*(Sp + Sx)) 
                  + (2.*eta_dot_kappa*M2 - 2.*eta_dot_q*M2 - eta_dot_p*Sp + eta_dot_p*Sx)*X) - (eta_dot_q*Sp - eta_dot_kappa*Sx)*(Power(S,2.) + Power(X,2.)))) 
         + (2.*m2*S - Q2m*X)*(4.*m2*(4.*M2*Q2 + Sx*(S - X)) + Q2*(4.*M2*Q2 + S*(-Sp + Sx) - (Sp + Sx)*X))*xi_dot_eta 
         - 32.*Power(m2,3.)*(-2.*eta_dot_q*M2 + eta_dot_p*Sx)*xi_dot_p - 8.*Power(m2,2.)*(2.*eta_dot_q*Power(S,2.) + Q2*(2.*(eta_dot_kappa - eta_dot_q)*M2 
               + eta_dot_p*(4.*S - Sp + Sx)) - 2.*eta_dot_q*S*X + eta_dot_q*Sp*X - eta_dot_kappa*Sx*X)*xi_dot_p - 2.*Q2m*(-2.*eta_dot_p*Power(Q2,2.)*X 
            + Q2m*(-(eta_dot_q*Sp) + eta_dot_kappa*Sx)*X + Q2*(Q2m*(-2.*eta_dot_kappa*M2 + 2.*eta_dot_q*M2 + eta_dot_p*Sp - eta_dot_p*Sx) 
               + eta_dot_q*X*(-S + X) + eta_dot_kappa*X*(S + X)))*xi_dot_p + 4.*m2*(-2.*eta_dot_p*Power(Q2,2.)*S + 2.*Q2m*(Q2m*(-2.*eta_dot_q*M2 + eta_dot_p*Sx) 
               + eta_dot_q*(S - X)*X) + Q2*(4.*eta_dot_p*Q2m*X + S*((eta_dot_kappa - eta_dot_q)*S + (eta_dot_kappa + eta_dot_q)*X)))*xi_dot_p)/
      den_xi;

   return(fHyperPlaneCoeffs);

}
//______________________________________________________________________________

   //______________________________________________________________________________
   POLRADKinematics::POLRADKinematics(){
      SetM(M_p/GeV);
      SetM_A(M_p/GeV);

      fVariables.Setk1(&fk1);
      fVariables.Setk2(&fk2);
      fVariables.SetP(&fP);
      fVariables.SetXi(&fXi);
      fVariables.SetEta(&fEta);

      fIsModified = true;

   }
//______________________________________________________________________________
POLRADKinematics::~POLRADKinematics(){


}

//______________________________________________________________________________
}}
