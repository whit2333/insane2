#include "insane/xsections/CoherentDVMPXSec.h"
#include "insane/base/PhysicalConstants.h"
#include <cmath>

using namespace insane::physics;

CoherentDVMPXSec::CoherentDVMPXSec()
{
   fID            = 201000000;
   SetTitle("CoherentDVMPXSec");
   SetPlotTitle("DVCS cross-section");
   fLabel         = "#frac{d#sigma}{dt dx dQ^{2} d#phi d#phi_{e'} }";
   fUnits         = "nb/sr/GeV^{4}";
   fnDim          = 5;
   fPIDs.clear();
   fnParticles    = 3;
   fPIDs.push_back(11);
   fPIDs.push_back(1000020040);
   fPIDs.push_back(333);

}
//______________________________________________________________________________

CoherentDVMPXSec::~CoherentDVMPXSec()
{ }
//______________________________________________________________________________

double CoherentDVMPXSec::xsec_e1dvcs(const dvcsvar_t &x,const dvcspar_t &p) const
{
   //std::cout << "x.Q2" << x.Q2 << std::endl;
   //std::cout << "x.x" << x.x << std::endl;
   //std::cout << "x.t" << x.t << std::endl;
   if(x.Q2 < 0.5 ) return 0.0;
   if(x.x < 0.01) return 0.0;
   return pow(p.Q2_0/x.Q2,p.alpha)     *  1./pow(1.+p.b*x.t,p.beta) *
      1./(1.+pow((x.x-0.3)/p.c,2)) * (1.-p.d*(1.-cos(x.phi)));
}
//______________________________________________________________________________

void CoherentDVMPXSec::InitializePhaseSpaceVariables()
{

   using namespace insane::units;
   PhaseSpace * ps = GetPhaseSpace();
   //if(ps) delete ps;
   //ps = nullptr;
   //std::cout << " Creating NEW PhaseSpace for CoherentDVMPXSec\n";
   //ps = new PhaseSpace();

   // ------------------------------
   // Electron
   auto * varEnergy = new PhaseSpaceVariable("energy_e", "E_{e'}",0.5, GetBeamEnergy());
   varEnergy->SetParticleIndex(0);
   varEnergy->SetDependent(true);
   ps->AddVariable(varEnergy);

   auto * varTheta = new PhaseSpaceVariable("theta_e", "#theta_{e'}",20.0*degree, 180.0*degree);
   varTheta->SetParticleIndex(0);
   varTheta->SetDependent(true);
   ps->AddVariable(varTheta);

   auto * varPhi = new PhaseSpaceVariable("phi_e", "#phi_{e'}",-180.0*degree, 180.0*degree);
   varPhi->SetParticleIndex(0);
   varPhi->SetUniform(true);
   //varPhi->SetDependent(true);
   ps->AddVariable(varPhi);


   // ------------------------------
   // Proton
   auto * varEnergy_p = new PhaseSpaceVariable("energy_p", "E_{p}",0.9,5.0);
   varEnergy_p->SetParticleIndex(1);
   varEnergy_p->SetDependent(true);
   ps->AddVariable(varEnergy_p);

   auto * varTheta_p = new PhaseSpaceVariable("theta_p", "#theta_{p}",0.5*degree, 170.0*degree);
   varTheta_p->SetParticleIndex(1);
   varTheta_p->SetDependent(true);
   ps->AddVariable(varTheta_p);

   auto * varPhi_p = new PhaseSpaceVariable("phi_p", "#phi_{p}", -1.0*TMath::Pi(), 2.0*TMath::Pi() );
   varPhi_p->SetParticleIndex(1);
   varPhi_p->SetDependent(true);
   varPhi_p->SetInverted(true);
   ps->AddVariable(varPhi_p);

   // ------------------------------
   // meson
   auto * varEnergy_g = new PhaseSpaceVariable("momentum_gamma", "P_{#phi}",0.1,8.0);
   varEnergy_g->SetParticleIndex(2);
   varEnergy_g->SetDependent(true);
   ps->AddVariable(varEnergy_g);

   auto * varTheta_g = new PhaseSpaceVariable("theta_gamma", "#theta_{#phi}",0.05*degree, 180.0*degree);
   varTheta_g->SetParticleIndex(2);
   varTheta_g->SetDependent(true);
   ps->AddVariable(varTheta_g);

   auto * varPhi_g = new PhaseSpaceVariable("phi_g", "#phi_{#phi}",-1.0*TMath::Pi(), 2.0*TMath::Pi() );
   varPhi_g->SetParticleIndex(2);
   varPhi_g->SetDependent(true);
   varPhi_g->SetInverted(true);
   ps->AddVariable(varPhi_g);


   // -------------------------
   auto * var_x = new PhaseSpaceVariable("x", "x",0.001, 1.0);
   var_x->SetParticleIndex(-1);
   ps->AddVariable(var_x);

   auto * var_t = new PhaseSpaceVariable("t", "t",-100.0, -0.1);
   var_t->SetParticleIndex(-1);
   ps->AddVariable(var_t);

   auto * var_Q2 = new PhaseSpaceVariable("Q2", "Q^{2}",0.00001, 100.0);
   var_Q2->SetParticleIndex(-1);
   ps->AddVariable(var_Q2);

   auto * var_phi = new PhaseSpaceVariable("phi", "#varphi",-180.0*degree, 180.0*degree);
   var_phi->SetParticleIndex(-1);
   ps->AddVariable(var_phi);


   SetPhaseSpace(ps);
}
//______________________________________________________________________________

void CoherentDVMPXSec::DefineEvent(Double_t * vars)
{

   Int_t totvars = 0;
   for (int i = 0; i < fParticles.GetEntries(); i++) {
      /// \todo fix this hard coding of 3 variables per event.
      /// here we are assuming the order E,theta,phi,then others
      /// \todo figure out how to handle vertex.
      insane::Kine::SetMomFromEThetaPhi((TParticle*)(fParticles.At(i)), &vars[totvars]);
      //((TParticle*)fParticles.At(i))->SetProductionVertex(GetRandomVertex());
      totvars += GetNParticleVars(i);
   }

}
//______________________________________________________________________________

Double_t * CoherentDVMPXSec::GetDependentVariables(const Double_t * x) const
{
   using namespace TMath;

   double phi_e    =  x[0];
   double xBjorken =  x[1];
   double t        =  x[2];
   double Q2       =  x[3];
   double phi      =  x[4];

   double M       = fTargetNucleus.GetMass();
   double E0      = GetBeamEnergy();
   double nu      = Q2/(2.0*M*xBjorken);
   double y       = nu/E0;
   double eprime  = E0 - nu;
   double theta_e = 2.0*ASin(M*xBjorken*y/Sqrt(Q2*(1-y)));
   //std::cout << "t = " << t << std::endl;;
   if(y>1.0) return nullptr;
   if(std::isnan(theta_e)) return nullptr;

   TVector3 k1(0, 0, fBeamEnergy); // incident electron
   TVector3 k2(0, 0, eprime);          // scattered electron
   k2.SetMagThetaPhi(eprime, theta_e, phi_e);
   TVector3 q1 = k1 - k2;
   TVector3 n_gamma = q1.Cross(k1);
   n_gamma.SetMag(1.0);
   TVector3 q1_B = q1;
   q1_B.Rotate(q1.Theta(), n_gamma);
   //q1_B.Print();

   double E2_A  = (2.0*M*M-t)/(2.0*M);
   double nu2_A = M + nu - E2_A;
   double P_phi_A  = Sqrt(E2_A*E2_A - 1.020*1.020);
   double P_2_A    = Sqrt(E2_A*E2_A - M*M);
   double cosTheta_qq2_B = (t+Q2+2.0*nu*nu2_A)/(2.0*nu2_A*q1.Mag());
   double theta_qq2_B    = ACos(cosTheta_qq2_B);
   double sintheta_qp2_B = P_phi_A*Sin(theta_qq2_B)/P_2_A;
   double theta_qp2_B    = ASin(sintheta_qp2_B);
   double phi_p2 = q1_B.Phi() + phi;
   TVector3 p2 = {0,0,1} ;
   TVector3 q2 = {0,0,1} ;
   p2.SetMagThetaPhi(P_2_A,   theta_qp2_B, phi_p2 );
   q2.SetMagThetaPhi(P_phi_A, theta_qq2_B, phi_p2+180.0*degree );

   q2.Rotate(-q1.Theta(), n_gamma);
   p2.Rotate(-q1.Theta(), n_gamma);

   fDependentVariables[0] = eprime;
   fDependentVariables[1] = k2.Theta();
   fDependentVariables[2] = k2.Phi();
   fDependentVariables[3] = E2_A;
   fDependentVariables[4] = p2.Theta();
   fDependentVariables[5] = p2.Phi();
   fDependentVariables[6] = P_phi_A;
   fDependentVariables[7] = q2.Theta();
   fDependentVariables[8] = q2.Phi();
   fDependentVariables[9] = xBjorken;
   fDependentVariables[10] = t;
   fDependentVariables[11] = Q2;
   fDependentVariables[12] = phi;
   //std::cout << "vectors" << std::endl;
   //std::cout << "fDependentVariables[0] = " << fDependentVariables[0] << std::endl;
   //std::cout << "fDependentVariables[1] = " << fDependentVariables[1] << std::endl;
   //std::cout << "fDependentVariables[2] = " << fDependentVariables[2] << std::endl;
   //std::cout << "fDependentVariables[3] = " << fDependentVariables[3] << std::endl;
   //std::cout << "fDependentVariables[4] = " << fDependentVariables[4] << std::endl;
   //std::cout << "fDependentVariables[5] = " << fDependentVariables[5] << std::endl;
   //k1.Print();
   //k2.Print();
   //p2.Print();
   //if (p2.Phi() < 0.0) fDependentVariables[5] = p2.Phi() + 2.0 * TMath::Pi() ;
   return(fDependentVariables);
}
//______________________________________________________________________________
Double_t CoherentDVMPXSec::GetEPrime(const Double_t theta)const  {
   // Returns the scattered electron energy using the angle. */
   Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
   return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M * TMath::Power(TMath::Sin(theta / 2.0), 2)));
}
//________________________________________________________________________

Double_t  CoherentDVMPXSec::EvaluateXSec(const Double_t * x) const
{
   if(!x) return(0.0);
   // Get the recoiling proton momentum
   if (!VariablesInPhaseSpace(9, x)) return(0.0);

   double xBjorken =  x[9];
   double t        =  x[10];
   double Q2       =  x[11];
   double phi      =  x[12];
   dvcsvar_t  vars; 
   vars.Q2 = Q2;
   vars.t = t;
   vars.x = xBjorken;
   vars.phi = phi;
   double norm = 0.12;
   //double norm = xsec_normalization();
   //std::cout << " Norm: " << norm << std::endl;
   double res = norm*xsec_e1dvcs(vars, PAR_COHDVCS);
   //std::cout << "res = " << res  << std::endl;
   //Double_t Eprime = x[0];
   //Double_t theta = x[1] ;
   //Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
   //Double_t Qsquared = 4.0 * Eprime * GetBeamEnergy() * TMath::Power(TMath::Sin(theta / 2.0), 2);
   //Double_t tau = Qsquared / (4.0 * M * M);
   //Double_t mottXSec = (1. / 137.) * (1. / 137.) *
   //   TMath::Power(TMath::Cos(theta / 2.0), 2) /
   //   (4.0 * GetBeamEnergy() * GetBeamEnergy() * TMath::Power(TMath::Sin(theta / 2.0), 4))
   //   * (Eprime / GetBeamEnergy());
   //Double_t GE2 = TMath::Power(fFormFactors->GEp(Qsquared), 2);
   //Double_t GM2 = TMath::Power(fFormFactors->GMp(Qsquared), 2);

   ////std::cout << " Qsq= " << Qsquared;
   ////std::cout << " tau= " << tau;
   ////std::cout << " GE2= " << GE2;
   ////std::cout << " GM2= " << GM2  << "\n";;

   //Double_t res = mottXSec * ((GE2 + tau * GM2) / (1.0 + tau) + 2.0 * tau * GM2 * TMath::Power(TMath::Tan(theta / 2.0), 2));
   //res = res * hbarc2_gev_nb;

   //if( IncludeJacobian() ) return( TMath::Sin(theta)*res);
   return(res);
}
//________________________________________________________________________



