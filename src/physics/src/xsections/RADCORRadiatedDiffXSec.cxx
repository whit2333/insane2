#include "RADCORRadiatedDiffXSec.h"

#include "InSANEPhysics.h"
namespace insane {
namespace physics {

RADCORRadiatedDiffXSec::RADCORRadiatedDiffXSec()
{
   fID         = 100010022;

   fPolType         = -1;
   fLabel           = "#frac{d^{2}#sigma}{dE'd#Omega}";
   fUnits           = "nb/GeV/sr";
   fApprox          = -1;    /// default is no setting  

   InitializePhaseSpaceVariables();

   /// RADCOR object 
   fRADCOR = new RADCOR(); 
   /// Proton and neutron form factors
   auto *amtFF = new AMTFormFactors();
   auto *Dipole = new DipoleFormFactors();
   /// 3He form factors 
   auto *mswFF       = new MSWFormFactors();
   auto *amrounFF = new AmrounFormFactors();

   /// F1F209 unpolarized cross section model 
   auto *F1F209 = new F1F209eInclusiveDiffXSec(); 
   F1F209->UseModifiedModel('n');  // y = use scaling of F1F209 to E01-012 and E94-010 data (for 3He) by D. Flay  

   /// Polarized structure functions 
   auto *DSSV = new DSSVPolarizedPDFs(); 

   auto *PolSFs = new PolarizedStructureFunctionsFromPDFs();
   PolSFs->SetPolarizedPDFs(DSSV);

   /// Polarized cross section difference 
   auto *PXS = new PolarizedCrossSectionDifference();
   PXS->SetPolarizedStructureFunctions(PolSFs);

   /// Set models and cross sections for RADCOR 
   // fRADCOR->SetFormFactors(amtFF); 
   fRADCOR->SetFormFactors(Dipole); 
   // fRADCOR->SetTargetFormFactors(mswFF); 
   fRADCOR->SetTargetFormFactors(amrounFF); 
   fRADCOR->SetUnpolXS(F1F209);
   fRADCOR->SetPolDiffXS(PXS);
   /// Set target type 
   fRADCOR->SetTargetNucleus(Nucleus::He3()); /// 3He is default. WHY? should be the proton! -whit

   /// Set integration threshold  
   fRADCOR->SetThreshold(1);    /// QE threshold is default  

}
//______________________________________________________________________________

RADCORRadiatedDiffXSec::~RADCORRadiatedDiffXSec()
{ }
//______________________________________________________________________________

Double_t RADCORRadiatedDiffXSec::EvaluateXSec(const Double_t *par) const
{
   // if (!VariablesInPhaseSpace(fnDim, par)){
   //         std::cout << "[RADCORRadiatedDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
   //         std::cout << "Phase space dimension: " << fnDim << std::endl;
   //         for(int i=0;i<3;i++) std::cout << "par[" << i << "]: " << par[i] << std::endl; 
   //         return(0.0);
   // }

   Double_t Ebeam  = GetBeamEnergy(); 
   Double_t Eprime = par[0]; 
   Double_t theta  = par[1]; 

   // std::cout << "[RADCORRadiatedDiffXSec::EvaluateXSec]: Kinematics" << std::endl;
   // std::cout << "Es = " << Ebeam         << std::endl;
   // std::cout << "Ep = " << Eprime        << std::endl; 
   // std::cout << "th = " << theta/degree  << std::endl;  
   // std::cout << "----------------------" << std::endl;

   Double_t sig=0; 

   if(fApprox==0){
      sig = fRADCOR->Exact(Ebeam,Eprime,theta);  
   }else if(fApprox==1){
      sig = fRADCOR->EnergyPeakingApprox(Ebeam,Eprime,theta);
   }else{
      std::cout << "[RADCORRadiatedDiffXSec::EvaluateXSec]: Invalid choice!  Exiting..." << std::endl;
      exit(1);
   }

   return sig; 

}
//______________________________________________________________________________

}}
