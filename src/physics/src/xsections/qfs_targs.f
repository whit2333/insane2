      SUBROUTINE TARGETCELL(TAG,INTERACTIVE,E,WMIN,WMAX,DELTAW,Tb,Ta,
     &                      density,zlength)
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER TAG*4
      LOGICAL INTERACTIVE

C-----DETERMINE TARGET CELL CHARACTERISTICS
C----- E01-012 Targets -----------------------------------------------
      if (TAG.EQ.'4_DU') then   ! Target is "DUKE" 
        E      = 4017.9         ! Incident Energy in MeV
        WMIN   = 1060.
        WMAX   = 2500.
        DELTAW = 10.0
        Tb     = 2.540E-3       ! Thickness before scatt., including 1/2 target
        Ta     = 5.435E-2       ! Thickness after scatt., including targ contribution
        density= 11.6           ! in Amagats
        zlength = 39.4          ! in cm
      elseif (TAG.EQ.'5_DU') then ! DUKE   
        E      = 5009.0          
        WMIN   = 1580.
        WMAX   = 2930.
        DELTAW = 10.0
        Tb     = 2.540E-3      
        Ta     = 5.480E-2
        density=  11.6        
        zlength = 39.4       
      elseif(TAG.EQ.'6_DU') then ! DUKE 
        E      = 5009.0
        WMIN   = 2165.
        WMAX   = 3285.
        DELTAW = 10.0
        Tb     = 2.540E-3
        Ta     = 4.480E-2
        density= 11.6  
        zlength = 39.4 
      elseif(TAG.EQ.'1_DU') then ! DUKE
        E      = 1046.0
        WMIN   = 0.
        WMAX   = 1050.0
        DELTAW = 5.0
        Tb     = 2.540E-3
        Ta     = 7.935E-2
        density= 11.6    
        zlength = 39.4   
      elseif(TAG.EQ.'3_EX') then ! EXODUS
        E      = 3028.1
        WMIN   = 600.
        WMAX   = 2250.
        DELTAW = 10.0
        Tb     = 2.460E-3
        Ta     = 4.660E-2
        density= 12.0          
        zlength = 39.6
      elseif(TAG.EQ.'6_EX') then ! EXODUS
        E      = 5009.0
        WMIN   = 2170.
        WMAX   = 3280.0
        DELTAW = 10.0
        Tb     = 2.460E-3
        Ta     = 3.870E-2
        density= 12.0
        zlength = 39.6        
C---------- E06-014 Targets ----------------------------------
      elseif(TAG.EQ.'4_SA') then  ! SAMANTHA (d2n)
        E      = 4730.0               ! Incident energy (MeV) 
        WMIN   = 2970.0
        WMAX   = 4150.0
        DELTAW = 10.0
        Tb     = 2.928E-03            ! Thickness in #X0 
        Ta     = 3.623E-02            ! Thickness in #X0 
        density= 10.8                 ! (APPROX) Density [in Amg]
        zlength = 38.1                ! Target length [cm]   
      elseif(TAG.EQ.'4_S4') then      ! SAMANTHA [3He] (d2n)
        E      = 4000.0               ! Incident energy (MeV) 
        WMIN   = 2344.0
        WMAX   = 3421.0
        DELTAW = 10.0
        Tb     = 2.928E-03            ! Thickness in #X0 [all materials before interaction]
        Ta     = 3.623E-02            ! Thickness in #X0 [all materials after interaction ]
        density= 10.8                 ! (APPROX) Density [in Amg]
        zlength = 38.1                ! Target length [cm]       
      elseif(TAG.EQ.'4_S3') then      ! SAMANTHA [3He] (d2n)
        E      = 4300.0               ! Incident energy (MeV) 
        WMIN   = 2644.0
        WMAX   = 3721.0
        DELTAW = 10.0
        Tb     = 2.928E-03            ! Thickness in #X0 [all materials before interaction]
        Ta     = 3.623E-02            ! Thickness in #X0 [all materials after interaction ]
        density= 10.8                 ! (APPROX) Density [in Amg]
        zlength = 38.1                ! Target length [cm]       
      elseif(TAG.EQ.'4_S2') then      ! SAMANTHA [3He] (d2n)
        E      = 4500.0               ! Incident energy (MeV) 
        WMIN   = 2844.0
        WMAX   = 3921.0
        DELTAW = 10.0
        Tb     = 2.928E-03            ! Thickness in #X0 [all materials before interaction]
        Ta     = 3.623E-02            ! Thickness in #X0 [all materials after interaction ]
        density= 10.8                 ! (APPROX) Density [in Amg]
        zlength = 38.1                ! Target length [cm]          
      elseif(TAG.EQ.'5_SA') then  ! SAMANTHA (d2n)
        E      = 5890.0               ! Incident energy (MeV) 
        WMIN   = 4130.0
        WMAX   = 5311.0
        DELTAW = 10.0
        Tb     = 2.928E-03            ! Thickness in #X0 
        Ta     = 3.623E-02            ! Thickness in #X0 
        density= 10.8                 ! (APPROX) Density [in Amg]
        zlength = 38.1                ! Target length [cm]
      elseif(TAG.EQ.'5_S4') then      ! SAMANTHA [3He] (d2n)
        E      = 5000.0               ! Incident energy (MeV) 
        WMIN   = 3060.0
        WMAX   = 4610.0
        DELTAW = 10.0
        Tb     = 2.928E-03            ! Thickness in #X0 [all materials before interaction]
        Ta     = 3.623E-02            ! Thickness in #X0 [all materials after interaction ] 
        density= 10.8                 ! (APPROX) Density [in Amg]
        zlength = 38.1                ! Target length [cm]       
      elseif(TAG.EQ.'5_S3') then      ! SAMANTHA [3He] (d2n)
        E      = 5300.0               ! Incident energy (MeV) 
        WMIN   = 3320.0
        WMAX   = 4910.0
        DELTAW = 10.0
        Tb     = 2.928E-03            ! Thickness in #X0 [all materials before interaction]
        Ta     = 3.623E-02            ! Thickness in #X0 [all materials after interaction ] 
        density= 10.8                 ! (APPROX) Density [in Amg]
        zlength = 38.1                ! Target length [cm]       
      elseif(TAG.EQ.'5_S2') then      ! SAMANTHA [3He] (d2n)
        E      = 5600.0               ! Incident energy (MeV) 
        WMIN   = 3580.0
        WMAX   = 5210.0
        DELTAW = 10.0
        Tb     = 2.928E-03            ! Thickness in #X0 [all materials before interaction]
        Ta     = 3.623E-02            ! Thickness in #X0 [all materials after interaction ] 
        density= 10.8                 ! (APPROX) Density [in Amg]
        zlength = 38.1                ! Target length [cm]   
      elseif(TAG.EQ.'4GMA') then      ! GMA [Nitrogen] (d2n) 
        E      = 4730.0               ! Incident energy (MeV) 
        WMIN   = 2970.0
        WMAX   = 4150.0
        DELTAW = 10.0
        Tb     = 3.209E-03            ! Thickness in #X0 
        Ta     = 3.724E-02            ! Thickness in #X0 
        density= 7.71                 ! (APPROX) Density [in Amg]
        zlength = 39.65               ! Target length [cm]       
      elseif(TAG.EQ.'5GMA') then      ! GMA [Nitrogen] (d2n) 
        E      = 5890.0               ! Incident energy (MeV) 
        WMIN   = 4130.0
        WMAX   = 5311.0
        DELTAW = 10.0
        Tb     = 3.209E-03            ! Thickness in #X0 
        Ta     = 3.724E-02            ! Thickness in #X0 
        density= 7.71                 ! (APPROX) Density [in Amg]
        zlength = 39.65               ! Target length [cm]          
C---------- XiaoChao Targets ----------------------------------
       elseif(TAG.EQ.'GORE') then     ! XiaoChao's target
c        E      = 1230.0              ! Incident energy (MeV) 
c        WMIN   = 0.0
c        WMAX   = 1090.0
c        DELTAW = 10.0
        Tb     = 2.098E-03           ! Thickness in #X0 
        Ta     = 5.785E-02           ! Thickness in #X0 
        density= 9.10                ! (APPROX) Density [in Amg]
        zlength = 25.0               ! Target length [cm] 
        elseif(TAG.EQ.'TIL') then    ! XiaoChao's target (2)
c        E      = 1230.0             ! Incident energy (MeV) 
c        WMIN   = 0.0
c        WMAX   = 1090.0
c        DELTAW = 10.0
        Tb     = 2.277E-03           ! Thickness in #X0 
        Ta     = 3.439E-02           ! Thickness in #X0 
        density= 8.28                ! (APPROX) Density [in Amg]
        zlength = 25.0               ! Target length [cm] 
C---------- E94-010 Targets ---------------------------------
      elseif (TAG.EQ.'8_SY') then    ! Target is "Sysiphos" 
        E      = 862.0               ! Incident Energy in MeV
        WMIN   = 15.
        WMAX   = 600.
        DELTAW = 5.0
        Tb     = 7.184E-3            ! Thickness before scatt., including 1/2 target
        Ta     = 4.166E-2            ! Thickness after scatt., including targ contibution
        density= 9.939               ! in Amagats
        zlength = 40.0               ! in cm
      elseif (TAG.EQ.'8hSY') then
        E      = 862.0
        WMIN   = 10.5
        WMAX   = 700.5
        DELTAW = 1.0
        Tb     = 7.184E-3
        Ta     = 4.273E-2
        density= 9.939
        zlength = 40.0
      elseif(TAG.EQ.'1_DW') then ! Dont worry
        E      = 1717.9
        WMIN   = 45.
        WMAX   = 975.
        DELTAW = 5.0
        Tb     = 7.338E-3
        Ta     = 4.189E-2
        density= 12.25
        zlength = 40.0
      elseif(TAG.EQ.'1hDW') then ! Dont worry
        E      = 1717.9
        WMIN   = 50.5
        WMAX   = 1500.5
        DELTAW = 1.0
        Tb     = 7.338E-3
        Ta     = 4.337E-2
        density= 12.25
        zlength = 40.0
      elseif(TAG.EQ.'1_BH') then ! Be Happy
        E      = 1717.9
        WMIN   = 50.5
        WMAX   = 1500.5
        DELTAW = 5.0
        Tb     = 7.271E-3
        Ta     = 4.128E-2
        density= 11.53
        zlength = 40.0
      elseif(TAG.EQ.'1hBH') then ! Be Happy
        E      = 1717.9
        WMIN   = 50.5
        WMAX   = 1500.5
        DELTAW = 1.0
        Tb     = 7.271E-3
        Ta     = 4.128E-2
        density= 11.53
        zlength = 40.0
      elseif(TAG.EQ.'1REF') then ! Reference Cell
        E      = 1720.0  ! to compare with Pibero's XS
        WMIN   = 10.5
        WMAX   = 1500.5
        DELTAW = 5.0
        Tb     = 4.931E-3  ! Educated Guess 
        Ta     = 5.041E-2
        density=  0.0
        zlength = 0.0
      elseif(TAG.EQ.'4REF') then ! ref cell.  
        E      = 4238.6
        WMIN   = 405.0
        WMAX   = 4015.0
        DELTAW = 10.0
        Tb     = 4.931E-3  ! Educated Guess
        Ta     = 5.041E-2
        density= 0.0
        zlength= 0.0
      elseif(TAG.EQ.'1_AR') then ! Armegeddon
        E      = 1716.9
        WMIN   = 45.0
        WMAX   = 970.
C        DELTAW = 1.0
        DELTAW=5.
        Tb     = 7.922E-3
        Ta     = 7.492E-2
        density= 12.32
        zlength = 40.0
      elseif(TAG.EQ.'1hAR') then ! Armegeddon
        E      = 1716.9
        WMIN   = 10.0
        WMAX   = 1500.5
        DELTAW = 1.0
        Tb     = 7.922E-3
C       Ta     = 8.505E-2
        Ta     = 9.92E-2    ! max possible win thickness.
        density= 12.32
        zlength = 40.0
      elseif(TAG.EQ.'2_JN') then ! JIN
        E      = 2580.5
        WMIN   = 85.0
        WMAX   = 1905.
        DELTAW = 10.0
        Tb     = 7.269E-3
        Ta     = 3.739E-2
        density= 10.23
        zlength = 40.0
      elseif(TAG.EQ.'2hJN') then ! JIN
        E      = 2580.5
        WMIN   = 50.0
        WMAX   = 2395.0
        DELTAW = 10.0
        Tb     = 7.269E-3
        Ta     = 3.902E-2
        density= 10.23
        zlength = 40.0
      elseif(TAG.EQ.'3_AR') then ! Armageddon
        E      = 3381.8
        WMIN   = 240.0
        WMAX   = 2440.0
        DELTAW = 20.0
        Tb     = 7.922E-3
        Ta     = 7.492E-2
        density= 12.32
        zlength = 40.0
      elseif(TAG.EQ.'3min') then ! Armageddon
        E      = 3381.8
        WMIN   = 255.0
        WMAX   = 3135.0
        DELTAW = 10.0
        Tb     = 7.922E-3
        Ta     = 6.968E-2
        density= 12.32
        zlength = 40.0
      elseif(TAG.EQ.'3hAR') then ! Armageddon
        E      = 3381.8
        WMIN   = 255.0
        WMAX   = 3135.0
        DELTAW = 10.0
        Tb     = 7.922E-3
C        Ta     = 8.505 ! alex's best estimate 
        Ta     = 9.92E-2    ! max possible win thickness. (NOt true)
        density= 12.32
        zlength = 40.0
      elseif(TAG.EQ.'4_NE') then ! Nephali
        E      = 4238.6
        WMIN   = 465.0
        WMAX   = 2925.0
        DELTAW = 30.0
        Tb     = 7.381E-3
        Ta     = 4.116E-2
        density= 13.77
        zlength = 40.0
      elseif(TAG.EQ.'4hNE') then ! Nephali
        E      = 4238.6
        WMIN   = 405.0
        WMAX   = 4015.0
        DELTAW = 10.0
        Tb     = 7.381E-3
        Ta     = 4.116E-2
        density= 13.77
        zlength = 40.0
      elseif(TAG.EQ.'4_SY') then ! Sysiphos
        E      = 4238.6
        WMIN   = 465.0
        WMAX   = 2925.0
        DELTAW = 30.0
        Tb     = 7.184E-3
        Ta     = 4.166E-2
        density= 9.939
        zlength = 40.0
      elseif(TAG.EQ.'4hSY') then ! Sysiphos
        E      = 4238.6
        WMIN   = 405.0
        WMAX   = 4015.0
        DELTAW = 10.0
        Tb     = 7.184E-3
        Ta     = 4.273E-2
        density= 9.939
        zlength = 40.0
      elseif(TAG.EQ.'5_JN') then ! JIN
        E      = 5058.2
        WMIN   = 400.0
        WMAX   = 4000.0
        DELTAW = 50.0
        Tb     = 7.269E-3
        Ta     = 3.739E-2
        density= 10.23
        zlength = 40.0
      elseif(TAG.EQ.'5hJN') then ! JIN
        E      = 5058.2
        WMIN   = 405.0
        WMAX   = 4015.0
        DELTAW = 10.0
        Tb     = 7.269E-3
        Ta     = 3.902E-2
        density= 10.23
        zlength = 40.0
C---------- Miscellaneous  ----------------------------------
      elseif(TAG.EQ.'1NH3') then ! NH3 for Meziani th=15
        E      = 2000.0
        WMIN   = 1160.0
        WMAX   = 1900.0
        DELTAW = 10.0
        Tb     = 6.078E-3
        Ta     = 6.290E-3
        density= 5.45E+21 
        zlength = 3.00 
      elseif(TAG.EQ.'2NH3') then ! NH3 for Meziani th=15
        E      = 3600.0
        WMIN   = 2750.0
        WMAX   = 3500.0
        DELTAW = 10.0
        Tb     = 6.078E-3
        Ta     = 6.290E-3
        density= 5.45E+21
        zlength = 3.00 
      elseif(TAG.EQ.'3NH3') then ! NH3 for Meziani th=15
        E      = 3300.0
        WMIN   = 2460.0
        WMAX   = 3200.0
        DELTAW = 10.0
        Tb     = 6.078E-3
        Ta     = 6.290E-3
        density= 5.45E+21
        zlength = 3.00 
      elseif(TAG.EQ.'4NH3') then ! NH3 for Meziani th=70
        E      = 2000.0
        WMIN   = 1160.0
        WMAX   = 1900.0
        DELTAW = 10.0
        Tb     = 6.078E-3
        Ta     = 1.124E-2
        density= 5.45E+21 
        zlength = 3.00 
      elseif(TAG.EQ.'5NH3') then ! NH3 for Meziani th=70
        E      = 3600.0
        WMIN   = 2750.0
        WMAX   = 3500.0
        DELTAW = 10.0
        Tb     = 6.078E-3
        Ta     = 1.124E-2
        density= 5.45E+21
        zlength = 3.00 
      elseif(TAG.EQ.'6NH3') then ! NH3 for Meziani th=70
        E      = 3300.0
        WMIN   = 2460.0
        WMAX   = 3200.0
        DELTAW = 10.0
        Tb     = 6.078E-3
        Ta     = 1.124E-2
        density= 5.45E+21
        zlength = 3.00 
      elseif(TAG.EQ.'7NH3') then ! NH3 for Meziani th=90
        E      = 2000.0
        WMIN   = 1160.0
        WMAX   = 1900.0
        DELTAW = 10.0
        Tb     = 6.078E-3
        Ta     = 1.056E-2
        density= 5.45E+21 
        zlength = 3.00 
      elseif(TAG.EQ.'8NH3') then ! NH3 for Meziani th=90
        E      = 3600.0
        WMIN   = 2750.0
        WMAX   = 3500.0
        DELTAW = 10.0
        Tb     = 6.078E-3
        Ta     = 1.056E-2
        density= 5.45E+21
        zlength = 3.00 
      elseif(TAG.EQ.'9NH3') then ! NH3 for Meziani th=90
        E      = 3300.0
        WMIN   = 2460.0
        WMAX   = 3200.0
        DELTAW = 10.0
        Tb     = 6.078E-3
        Ta     = 1.056E-2
        density= 5.45E+21
        zlength = 3.00 
c     FIXME: GMA Cell -- Tb, Ta and density are NOT correct! 
      elseif(TAG.EQ.'GMA') then ! Reference Cell (d2n -- H2 cell)
        E      = 1230.0  
        WMIN   = 0.
        WMAX   = 1300.0
        DELTAW = 5.0
        Tb     = 8.43E-3
        Ta     = 0.0832
        density= 9.61 * 2.0          
        zlength = 39.4               ! measured
c   END d2n Targets
C-- REFERENCE CELL
      elseif(TAG.EQ.'1REF') then ! Reference Cell (Duke's ref. cell)
        E      = 1046.0  
        WMIN   = 0.
        WMAX   = 1050.0
        DELTAW = 5.0
        Tb     = 8.43E-3
        Ta     = 0.0832
        density= 9.61 * 2.0  ! Pavg = 142 psig, cell leaked
        zlength = 39.2           ! measured
      elseif(TAG.EQ.'3REF') then ! Reference Cell (Exodus's ref. cell)
        E      = 3028.1
        WMIN   = 600.
        WMAX   = 2250.
        DELTAW = 20.0
        Tb = 8.76E-3
        Ta = 0.0606
        density= 9.94 * 2.0  ! P = 147 psig
        zlength= 39.2            ! approx. 
      elseif(TAG.EQ.'4REF') then ! Reference Cell (Duke's ref. cell) 
        E      = 4017.9
        WMIN   = 1060.
        WMAX   = 2500.
        DELTAW = 20.0
        Tb     = 8.36E-3
        Ta     = 0.0569
        density= 9.50 * 2.0  ! Pavg = 139 psig, cell leaked
        zlength=  39.2           ! measured
      elseif(TAG.EQ.'5REF') then ! Reference Cell (Duke's ref. cell) 
        E      = 5009.0
        WMIN   = 1580.
        WMAX   = 2930.
        DELTAW = 20.0
        Tb     = 8.34E-3 
        Ta     = 0.0595
        density= 9.48 * 2.0  ! Pavg = 140.5 psig, cell leaked
        zlength= 39.2            ! measured
      elseif(TAG.EQ.'6REF') then ! Reference Cell (Duke's ref. cell)
        E      = 5009.0
        WMIN   = 2170.
        WMAX   = 3280.
        DELTAW = 20.0
        Tb     = 8.40E-3
        Ta     = 0.0469
        density= 9.57 * 2.0  ! Pavg = 141.3 psig, cell leaked
        zlength= 39.2            ! measured
      elseif(TAG.EQ.'7REF') then ! Reference Cell (Exodus's ref. cell)
        E      = 5009.0
        WMIN   = 2170.
        WMAX   = 3280.
        DELTAW = 20.0
        Tb     = 8.75E-3
        Ta     = 0.0498
        density= 9.92 * 2.0  ! P = 147 psig
        zlength= 39.2            ! approx.
      elseif(TAG.EQ.'2_JP') then ! JP's thesis
        E      = 2699.7
        Tb     = 0.05910/2.+0.000285
        Ta     = 0.05910/2.+(0.00346+0.00259+0.00124)
        density= 7.83           ! g/cm**3
        zlength = 0.02908       ! inches
      elseif(INTERACTIVE) then ! will prompt user for input
        density = 0.0           ! g/cm**3
        zlength = 0.0           ! inches
        CONTINUE
      else
        WRITE(6,*) TAG,':  Unknown Energy'
        STOP
      endif
C      density = 7.4
      if((TAG.NE.'1NH3').OR.(TAG.NE.'2NH3').OR.(TAG.NE.'3NH3').OR.
     &   (TAG.NE.'4NH3').OR.(TAG.NE.'5NH3').OR.(TAG.NE.'6NH3').OR.
     &   (TAG.NE.'7NH3').OR.(TAG.NE.'8NH3').OR.(TAG.NE.'9NH3')) then
         density=density*2.6868E19 ! Convert from Amagat to (cm)^-3
      endif

C      write(8,'(A,F6.1,x,2(F9.5,x),E10.5,x,F5.1)') 
C     &         '#',E,    Tb,Ta,    density,zlength 
      RETURN 
      END
