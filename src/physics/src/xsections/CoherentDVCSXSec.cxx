#include "insane/xsections/CoherentDVCSXSec.h"
#include "insane/base/PhysicalConstants.h"
#include <cmath>
#include "insane/xsections/DVCS.h"

using namespace insane::physics;

CoherentDVCSXSec::CoherentDVCSXSec()
{
   fID            = 201000000;
   SetTitle("CoherentDVCSXSec");
   SetPlotTitle("DVCS cross-section");
   fLabel         = "#frac{d#sigma}{dt dx dQ^{2} d#phi d#phi_{e'} }";
   fUnits         = "nb/radian^{2}/GeV^{4}";
   fnDim          = 5;
   fPIDs.clear();
   fnParticles    = 3;
   fPIDs.push_back(11);
   fPIDs.push_back(1000020040);
   fPIDs.push_back(22);

}
//______________________________________________________________________________

CoherentDVCSXSec::~CoherentDVCSXSec()
{ }
//______________________________________________________________________________

void CoherentDVCSXSec::InitializePhaseSpaceVariables()
{

   using namespace insane::units;
   PhaseSpace * ps = GetPhaseSpace();
   //if(ps) delete ps;
   //ps = nullptr;
   //std::cout << " Creating NEW PhaseSpace for CoherentDVCSXSec\n";
   //ps = new PhaseSpace();

   // ------------------------------
   // Electron
   auto * varEnergy = new PhaseSpaceVariable("energy_e", "E_{e'}",0.5, 5.0);
   varEnergy->SetParticleIndex(0);
   varEnergy->SetDependent(true);
   ps->AddVariable(varEnergy);

   auto * varTheta = new PhaseSpaceVariable("theta_e", "#theta_{e'}",5.0*degree, 180.0*degree);
   varTheta->SetParticleIndex(0);
   varTheta->SetDependent(true);
   ps->AddVariable(varTheta);

   auto * varPhi = new PhaseSpaceVariable("phi_e", "#phi_{e'}",-180.0*degree, 180.0*degree);
   varPhi->SetParticleIndex(0);
   varPhi->SetUniform(true);
   ps->AddVariable(varPhi);

   // ------------------------------
   // struck nucleon
   auto * varEnergy_p = new PhaseSpaceVariable("P_A", "P_{A}",0.01,2.5);
   varEnergy_p->SetParticleIndex(1);
   varEnergy_p->SetDependent(true);
   ps->AddVariable(varEnergy_p);

   auto * varTheta_p = new PhaseSpaceVariable("theta_A", "#theta_{A}",1*degree, 180.0*degree);
   varTheta_p->SetParticleIndex(1);
   varTheta_p->SetDependent(true);
   ps->AddVariable(varTheta_p);

   auto * varPhi_p = new PhaseSpaceVariable("phi_A", "#phi_{A}", -1.0*TMath::Pi(), 1.0*TMath::Pi() );
   varPhi_p->SetParticleIndex(1);
   varPhi_p->SetDependent(true);
   ps->AddVariable(varPhi_p);

   // ------------------------------
   // photon
   auto * varEnergy_g = new PhaseSpaceVariable("energy_gamma", "E_{#gamma}",0.3,11.0);
   varEnergy_g->SetParticleIndex(2);
   varEnergy_g->SetDependent(true);
   ps->AddVariable(varEnergy_g);

   auto * varTheta_g = new PhaseSpaceVariable("theta_gamma", "#theta_{#gamma}",2.0*degree, 170.0*degree);
   varTheta_g->SetParticleIndex(2);
   varTheta_g->SetDependent(true);
   ps->AddVariable(varTheta_g);

   auto * varPhi_g = new PhaseSpaceVariable("phi_g", "#phi_{#gamma}",-1.0*TMath::Pi(), 1.0*TMath::Pi() );
   varPhi_g->SetParticleIndex(2);
   varPhi_g->SetDependent(true);
   ps->AddVariable(varPhi_g);


   // -------------------------
   auto * var_x = new PhaseSpaceVariable("x", "x",0.05, 1.0);
   var_x->SetParticleIndex(-1);
   ps->AddVariable(var_x);

   auto * var_t = new PhaseSpaceVariable("t", "t",-3.0, 0.0);
   var_t->SetParticleIndex(-1);
   ps->AddVariable(var_t);

   auto * var_Q2 = new PhaseSpaceVariable("Q2", "Q^{2}",1.0, 5.0);
   var_Q2->SetParticleIndex(-1);
   ps->AddVariable(var_Q2);

   auto * var_phi = new PhaseSpaceVariable("phi", "#varphi",-180.0*degree, 180.0*degree);
   var_phi->SetParticleIndex(-1);
   ps->AddVariable(var_phi);

   fNuclearTargetMass = GetTargetNucleus().GetMass();

   //SetPhaseSpace(ps);
}
//______________________________________________________________________________

void CoherentDVCSXSec::DefineEvent(Double_t * vars)
{

   Int_t totvars = 0;
   for (int i = 0; i < 3; i++) {
      if( (i==1) ) {
         insane::Kine::SetEFromMomThetaPhi((TParticle*)(fParticles.At(i)), &vars[totvars]);
      } else {
         insane::Kine::SetMomFromEThetaPhi((TParticle*)(fParticles.At(i)), &vars[totvars]);
      }
      //((TParticle*)fParticles.At(i))->SetProductionVertex(GetRandomVertex());
      totvars += GetNParticleVars(i);
   }

}
//______________________________________________________________________________

Double_t * CoherentDVCSXSec::GetDependentVariables(const Double_t * x) const
{
   using namespace TMath;

   double phi_e    =  x[0];
   double xBjorken =  x[1];
   double t        =  x[2];
   double Q2       =  x[3];
   double phi      =  x[4];
   double M        = fNuclearTargetMass;
   double MT       = fNuclearTargetMass;
   double t_abs    = std::abs(t);
   double E0       = GetBeamEnergy();
   double s        = M*M + 2.0*M*E0;
   double eps      = 2.0*xBjorken*M/Sqrt(Q2);
   double y_max    = 2.0*(std::sqrt(1.0+eps*eps)-1.0)/(eps*eps);

   double y_col    = (Q2+t)/(Q2+xBjorken*t);

   //std::cout << " y_max = " << y_max <<std::endl;
   //std::cout << " y_col = " << y_max <<std::endl;


   if( t_abs < std::abs(insane::physics::Delta2_min(Q2, xBjorken, M)) ) return nullptr;
   if( t_abs > std::abs(insane::physics::Delta2_max(Q2, xBjorken, M)) ) return nullptr;

   //std::cout << " M = " << M <<std::endl;
   double nu      = Q2/(2.0*MT*xBjorken);
   double y       = nu/E0;
   double eprime  = E0 - nu;
   double theta_e = 2.0*ASin(MT*xBjorken*y/Sqrt(Q2*(1-y)));
   //double theta_e = ASin(eps*Sqrt(1.0-y-y*y*eps*eps/4.0)/(Sqrt(1.0-eps*eps)));
   //std::cout << "eprime = " << eprime << std::endl;;
   //std::cout << "theta_e = " << theta_e/degree << std::endl;;
   //std::cout << "t = " << t << std::endl;;
   if(y>y_max) return nullptr;
   if(y>y_col) return nullptr;
   if(std::isnan(theta_e)) return nullptr;
   //std::cout << "t_min " << std::abs(insane::physics::Delta2_min2(Q2, xBjorken, M)) << ", t = " << std::abs(t) << ", t_max " << std::abs(insane::physics::Delta2_max(Q2, xBjorken, M))<< "\n";

   TVector3 k1(0, 0, fBeamEnergy); // incident electron
   TVector3 k2(0, 0, eprime);          // scattered electron
   k2.SetMagThetaPhi(eprime, theta_e, phi_e);

   TVector3 q1 = k1 - k2;
   TVector3 n_gamma = k1.Cross(k2);
   n_gamma.SetMag(1.0);
   
   TVector3 q1_B = q1;
   q1_B.Rotate(q1.Theta(), n_gamma);
   //q1_B.Print();

   double E2_A  = (2.0*MT*MT-t)/(2.0*MT);
   double P2_A  = Sqrt(E2_A*E2_A - MT*MT);
   double nu2_A = MT + nu - E2_A;

   double cosTheta_qq2_B = (t+Q2+2.0*nu*nu2_A)/(2.0*nu2_A*q1_B.Mag());
   double theta_qq2_B    = ACos(cosTheta_qq2_B);
   double sintheta_qp2_B = nu2_A*Sin(theta_qq2_B)/P2_A;
   double theta_qp2_B    = ASin(sintheta_qp2_B);
   //double phi_p2 = phi+180.0*degree;

   TVector3 p2 = {0,0,1} ;
   TVector3 p1 = {0,0,0} ;
   TVector3 q2 = {0,0,1} ;
   p2.SetMagThetaPhi(P2_A,  theta_qp2_B, phi);
   q2.SetMagThetaPhi(nu2_A, theta_qq2_B, phi+180.0*degree);
   //std::cout << " phi p2 = " << p2.Phi() << std::endl;

   q2.Rotate(-1.0*(q1.Theta()), n_gamma);
   p2.Rotate(-1.0*(q1.Theta()), n_gamma);
   q1_B.Rotate(-q1.Theta(), n_gamma);
   //q1_B.Print();

   fDependentVariables[0] = eprime;
   fDependentVariables[1] = k2.Theta();
   fDependentVariables[2] = k2.Phi();
   fDependentVariables[3] = P2_A;
   fDependentVariables[4] = p2.Theta();
   fDependentVariables[5] = p2.Phi();
   fDependentVariables[6] = nu2_A;
   fDependentVariables[7] = q2.Theta();
   fDependentVariables[8] = q2.Phi();
   fDependentVariables[9] = xBjorken;
   fDependentVariables[10] = t;
   fDependentVariables[11] = Q2;
   fDependentVariables[12] = phi;
   //std::cout << "vectors" << std::endl;
   //std::cout << "fDependentVariables[0] = " << fDependentVariables[0] << std::endl;
   //std::cout << "fDependentVariables[1] = " << fDependentVariables[1] << std::endl;
   //std::cout << "fDependentVariables[2] = " << fDependentVariables[2] << std::endl;
   //std::cout << "fDependentVariables[3] = " << fDependentVariables[3] << std::endl;
   //std::cout << "fDependentVariables[4] = " << fDependentVariables[4] << std::endl;
   //std::cout << "fDependentVariables[5] = " << fDependentVariables[5] << std::endl;
   //k1.Print();
   //k2.Print();
   //p2.Print();
   //if (p2.Phi() < 0.0) fDependentVariables[5] = p2.Phi() + 2.0 * TMath::Pi() ;
   auto net_p = (k1 - k2 - p2 - q2);
   if( net_p.Mag() > 0.001 ) {

     std::cout << "momentum not conserved\n";
     std::cout << "t_min " << std::abs(insane::physics::Delta2_min2(Q2, xBjorken, M)) << ", t = " << std::abs(t) << ", t_max " << std::abs(insane::physics::Delta2_max(Q2, xBjorken, M))<< "\n";
     std::cout << "s = " << s << ", sqrt(s) = " << std::sqrt(s) << std::endl;
     std::cout << "y = " << y << ", y_max = " << y_max << std::endl;
     std::cout << "k1 ";k1.Print();
     std::cout << "k2 ";k2.Print();
     std::cout << "q2 ";q2.Print();
     std::cout << "p2 ";p2.Print();
     net_p.Print();
     return nullptr;
   }
   return(fDependentVariables);
}
//______________________________________________________________________________

Double_t CoherentDVCSXSec::GetEPrime(const Double_t theta)const  {
  // Returns the scattered electron energy using the angle. */
  Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
  return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M * TMath::Power(TMath::Sin(theta / 2.0), 2)));
}
//________________________________________________________________________

Double_t  CoherentDVCSXSec::EvaluateXSec(const Double_t * x) const
{
   if(!x) return(0.0);
   // Get the recoiling proton momentum
   if (!VariablesInPhaseSpace(9, x)) return(0.0);
   using namespace TMath;
   using namespace insane::physics;

   double xB  =  x[9];
   double t   =  x[10];
   double Q2  =  x[11];
   double phi =  x[12];
   double M        = fNuclearTargetMass;
   double E0       = GetBeamEnergy();
   double eps      = epsilon(Q2,xB,M);
   double nu       = Q2/(2.0*M*xB);
   double y        = nu/E0;
   double Delta2   = t;
   double xi       = xi_BKM(xB,Q2,Delta2);

   double FC  = TMath::Abs(fFormFactors->FCHe4(TMath::Abs(t)));

   double F1  = FC;//GE/(1.0+Q2/(4.0*M));
   double F2  = 0.0;//-GE/(1.0+Q2/(4.0*M));

   //std::cout << " FC = " << FC << "\n";
   //std::cout << " xB = " << xB << "\n";
   //std::cout << " t  = " << t  << "\n";
   //std::cout << " Q2 = " << Q2 << "\n";
   //std::cout << " E0 = " << E0 << "\n";
   //std::cout << " nu = " << nu << "\n";
   //std::cout << " xi = " << xi << "\n";
   //std::cout << " y  = " << y  << "\n";
   //std::cout << " phi= " << phi<< "\n";


   DVCS_KinematicVariables dvcs_vars { xB, Q2, t, phi, xi, E0, M, y, nu, eps};
   DVCS_FormFactors        dvcs_ffs;
   dvcs_ffs.F1     = F1;
   dvcs_ffs.F2     = 0;
   dvcs_ffs.H      = 0.0;
   dvcs_ffs.Htilde = 0.0;

   //std::cout << dvcs_vars.phi*(180/TMath::Pi()) << std::endl;

   double p1_a = P1(E0, Q2, xB, Delta2, phi, M);
   double p2_a = P2(E0, Q2, xB, Delta2, phi, M);
   //std::cout << " p1_a = " << p1_a << "\n";
   //std::cout << " p2_a = " << p2_a << "\n";
   dvcs_vars.K = K_DVCS(dvcs_vars);
   dvcs_vars.J = J_DVCS(dvcs_vars);

   double K  = dvcs_vars.K;
   double K2 = K_DVCS2(dvcs_vars);
   double K3 = K_DVCS(E0, Q2, xB, Delta2, M);
   double K4 = K_DVCS2(E0, Q2, xB, Delta2, M);

   double J  =  dvcs_vars.J;
   double J2 = J_DVCS(E0, Q2, xB, Delta2, M);

   double p1 = P1(dvcs_vars);
   double p2 = P2(dvcs_vars);

   //std::cout << " J    = " << J  << "\n";
   //std::cout << " J2   = " << J2 << "\n";
   //std::cout << " K    = " << K  << "\n";
   //std::cout << " K2   = " << K2 << "\n";
   //std::cout << " K3   = " << K3 << "\n";
   //std::cout << " K4   = " << K4 << "\n";
   ////
   //std::cout << " p1   = " << p1 << "\n";
   //std::cout << " p2   = " << p2 << "\n";

   // -------------------------------
   // BH
   double c0BH = c0_BH_spin0(dvcs_vars, dvcs_ffs);
   double c1BH = c1_BH_spin0(dvcs_vars, dvcs_ffs);
   double c2BH = c2_BH_spin0(dvcs_vars, dvcs_ffs);

   //std::cout << " c0BH      = " << c0BH << "\n";
   //std::cout << " c1BH*K    = " << c1BH*K << "\n";
   //std::cout << " c2BH*K*K  = " << c2BH*K*K   << "\n";
   //std::cout << " K   = " << K << "\n";

   double num_BH = 1.0*FC*FC*(c0BH + c1BH*K*TMath::Cos(phi) + c2BH*K*K*TMath::Cos(2.0*phi)  );
   // note the negative sign is missing above because we are use dt in the differential as 
   // opposed to |dt|. Similarly for jacobian which gets an extra minus sign.
   double den_BH = Power(y*xB*(1.0+eps*eps),2.0)*Delta2*p1*p2;
   double den_BH_no_p1p2 = Power(y*xB*(1.0+eps*eps),2.0)*Delta2;
   //std::cout << "(c0BH + c1BH*K*TMath::Cos(phi) + c2BH*K*K*TMath::Cos(2.0*phi)  ) " <<(c0BH + c1BH*K*TMath::Cos(phi) + c2BH*K*K*TMath::Cos(2.0*phi)  )  << std::endl;

   double T_BH2  = (num_BH/den_BH);

   //std::cout << " num_BH   = " << num_BH << "\n";
   //std::cout << " den_BH   = " << den_BH << "\n";
   //std::cout << " T_BH2    = " << T_BH2 << "\n";
   //std::cout << " den_BH_no_p1p2   = " << den_BH_no_p1p2 << "\n";

   double t0 = Power(1.0/137.0,3.0)*xB/(16.0*pi*pi*Q2*Q2*Sqrt(1.0+eps*eps));
   // note we have made use of the Jacobian transform 
   // dQ2/dy = Q2/y = (s-M^2)x  to swap out y for Q2 in the differential.

   double sig_BH = t0*(num_BH/den_BH);// + num_DVCS/den_DVCS + num_I/den_I);

   //res = res * hbarc2_gev_nb;
   double sig0 = sig_BH*TMath::Power(hbarc2_gev_nb,1.5);;

   double A       = GetBeamSpinAsymmetry(x);
   double hel     = GetHelicity();

   double sig_plus  = sig0*(1.0 + hel*A)/2.0;
   double sig_minus = sig0*(1.0 - hel*A)/2.0;

   double jac = -1.0*Jacobian(x);
   //std::cout << " jac   = " << jac << "\n";
   //std::cout << " sig0   = " << sig0 << "\n";
   //if(TMath::Abs(phi)<0.2) std::cout << "phi " << phi << std::endl;
   //if(sig0 < 0 ) return 0;
   double res = (sig_plus + sig_minus);
   if( IncludeJacobian() ) return( jac*res);
   return(res);
}
//______________________________________________________________________________

Double_t   CoherentDVCSXSec::GetBeamSpinAsymmetry(const Double_t * x) const
{
   double xBjorken =  x[9];
   double t        =  x[10];
   double Q2       =  x[11];
   double phi      =  x[12];
   return( falpha*TMath::Sin(phi)/(1.0+fbeta*TMath::Cos(phi)));
}
//______________________________________________________________________________

double CoherentDVCSXSec::Jacobian(const double * vars) const
{
   using namespace TMath;
   auto Csc = [](double x){ return 1.0/TMath::Sin(x) ; };
   auto ArcCos = [](double x){ return TMath::ACos(x) ; };
   double M = fNuclearTargetMass;
   double k1 = fBeamEnergy;

   double k2      = vars[0];
   double thetak2 = vars[1];
   double phik2   = vars[2];

   double q2      = vars[6];
   double thetaq2 = vars[7];
   double phiq2   = vars[8];

   double res = (4.*Power(k1,2)*Power(k2,3)*(1. - 1.*Cos(thetak2))*(k1*(k2 - M) + 
k2*M - k1*k2*Cos(thetak2))*Power(Sin(thetak2),2)*Sin(thetaq2)*Power(
k1 - k2 + M - k1*Cos(thetaq2) + k2*Cos(thetak2)*Cos(thetaq2) + 
k2*Cos(phik2 - 
phiq2)*Sin(thetak2)*Sin(thetaq2),2)*(k2*Power(Sin(phik2 - 
phiq2),2)*Sin(thetak2)*(Power(k1,2) - 2*k1*k2*Cos(thetak2) + 
Power(k2,2)*Power(Cos(thetak2),2) + 
Power(k2,2)*Power(Sin(thetak2),2))*Power(Sin(thetaq2),2) + 
ArcCos((k2*Sin(thetak2)*(k2*Cos(thetaq2)*Sin(thetak2) + Cos(phik2 - 
phiq2)*(k1 - k2*Cos(thetak2))*Sin(thetaq2)))/(Power(k1,2) + 
Power(k2,2) - 2*k1*k2*Cos(thetak2)))*(k2*Power(Cos(phik2 - 
phiq2),2)*Cos(thetaq2)*Sin(thetak2) + k2*Cos(thetaq2)*Power(Sin(phik2 
- phiq2),2)*Sin(thetak2) + Cos(phik2 - phiq2)*(k1 - 
k2*Cos(thetak2))*Sin(thetaq2))*Sqrt(Power(k1,4) + 
2*Power(k1,2)*Power(k2,2) + Power(k2,4) - 
Power(k2,4)*Power(Cos(thetaq2),2)*Power(Sin(thetak2),4) - 
2*k1*Power(k2,3)*Cos(phik2 - 
phiq2)*Cos(thetaq2)*Power(Sin(thetak2),3)*Sin(thetaq2) - 
Power(k1,2)*Power(k2,2)*Power(Cos(phik2 - 
phiq2),2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2) + 
Cos(thetak2)*(-4*k1*k2*(Power(k1,2) + Power(k2,2)) + 
2*Power(k2,4)*Cos(phik2 - 
phiq2)*Cos(thetaq2)*Power(Sin(thetak2),3)*Sin(thetaq2) + 
2*k1*Power(k2,3)*Power(Cos(phik2 - 
phiq2),2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2)) + 
Power(Cos(thetak2),2)*(4*Power(k1,2)*Power(k2,2) - 
Power(k2,4)*Power(Cos(phik2 - 
phiq2),2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2)))))/(Power(k1 - 1.*k2,2)*Power(Power(k1,2) + Power(k2,2) - 
2*k1*k2*Cos(thetak2),1.5)*Power(-1.*k1 + k2 - 1.*M + (k1 - 
1.*k2*Cos(thetak2))*Cos(thetaq2) - 1.*k2*Cos(phik2 - 
phiq2)*Sin(thetak2)*Sin(thetaq2),4)*Sqrt(1 - 
(Power(k2,2)*Power(Sin(thetak2),2)*Power(k2*Cos(thetaq2)*Sin(thetak2) 
+ Cos(phik2 - phiq2)*(k1 - 
k2*Cos(thetak2))*Sin(thetaq2),2))/Power(Power(k1,2) + Power(k2,2) - 
2*k1*k2*Cos(thetak2),2)));
   return res;
}
//______________________________________________________________________________



