#include "insane/xsections/POLRADBornDiffXSec.h"
namespace insane {
namespace physics {

POLRADBornDiffXSec::POLRADBornDiffXSec()
{
   fID         = 100010001;
   SetTitle("POLRADBornDiffXSec");//,"POLRAD Born cross-section");
   SetPlotTitle("POLRAD Born XSec");
}
//______________________________________________________________________________

POLRADBornDiffXSec::~POLRADBornDiffXSec()
{ }
//______________________________________________________________________________

void POLRADBornDiffXSec::InitializePhaseSpaceVariables() {
   InclusiveDiffXSec::InitializePhaseSpaceVariables();
}
//______________________________________________________________________________
Double_t POLRADBornDiffXSec::EvaluateXSec(const Double_t *x) const {
   if (!VariablesInPhaseSpace(fnDim, x)){
      //std::cout << x[0] << std::endl;
      //std::cout << x[1] << std::endl;
      //std::cout << x[2] << std::endl;
      //std::cout << "[POLRADBornDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
      return(0.0);
   }
   fPOLRADCalc->SetTargetNucleus(GetTargetNucleus());
   Double_t Eprime  = x[0];
   Double_t theta   = x[1];
   Double_t Ebeam   = GetBeamEnergy();
   Double_t nu      = Ebeam-Eprime;
   Double_t Mtarg   = M_p/GeV;  
   Double_t S       = 2.0*Ebeam*Mtarg;
   Double_t X       = 2.0*Eprime*Mtarg;
   Double_t Q2      = 4.0*Eprime*Ebeam*TMath::Sin(theta/2.0)*TMath::Sin(theta/2.0);
   fPOLRADCalc->SetKinematics(Ebeam,Eprime,theta);
   Double_t bornXSec = fPOLRADCalc->sig_dis_born(S,X,Q2); 
   bornXSec = bornXSec * (Eprime/(2.0*pi*Mtarg*nu));
   if(IncludeJacobian()){
      bornXSec *= TMath::Sin(theta);
   }
   return(bornXSec*hbarc2_gev_nb);
} 
//______________________________________________________________________________

}}
