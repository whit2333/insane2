#include "insane/xsections/QuasiElasticInclusiveDiffXSec.h"
#include "insane/base/PhysicalConstants.h"
#include "insane/xsections/RadiativeEffects.h"

namespace insane {
   namespace physics {

QuasiElasticInclusiveDiffXSec::QuasiElasticInclusiveDiffXSec()
{
   fID            = 200000040;
   SetTitle("QuasiElasticInclusiveDiffXSec");
   SetPlotTitle("Elastic cross-section");
   fLabel         = "#frac{d#sigma}{d#Omega}";
   fUnits         = "nb/sr";
   fnDim          = 2;
   fPIDs.clear();
   fnParticles    = 2;
   fPIDs.push_back(11);
   fPIDs.push_back(2212);
}
//______________________________________________________________________________

QuasiElasticInclusiveDiffXSec::~QuasiElasticInclusiveDiffXSec(){
}
//______________________________________________________________________________

void QuasiElasticInclusiveDiffXSec::InitializePhaseSpaceVariables()
{

   PhaseSpace * ps = GetPhaseSpace();

   // ------------------------------
   // Electron
   auto * varEnergy = new PhaseSpaceVariable(
         "energy_e", "E_{e'}", 0.50, GetBeamEnergy());
   varEnergy->SetDependent(true);
   ps->AddVariable(varEnergy);

   auto *   varTheta = new PhaseSpaceVariable(
         "theta_e", "#theta_{e'}", 1.0*degree, 60.0*degree);
   //varTheta->SetDependent(true);
   ps->AddVariable(varTheta);

   auto *   varPhi = new PhaseSpaceVariable(
         "phi_e", "#phi_{e'}", -180.0*degree, 180.0*degree);
   varPhi->SetUniform(true);
   ps->AddVariable(varPhi);


   // ------------------------------
   //proton 
   auto * varP_p = new PhaseSpaceVariable(
         "P_p", "P_{p}",0.0,5.0);
   varP_p->SetParticleIndex(1);
   varP_p->SetDependent(true);
   ps->AddVariable(varP_p);

   auto *   varTheta_p = new PhaseSpaceVariable( 
         "theta_p", "#theta_{p}", 30*degree, 180.0*degree);
   varTheta_p->SetParticleIndex(1);
   varTheta_p->SetDependent(true);
   ps->AddVariable(varTheta_p);

   auto *   varPhi_p = new PhaseSpaceVariable(
         "phi_p", "#phi_{p}", -180.0*degree, 180.0*degree);
   varPhi_p->SetParticleIndex(1);
   varPhi_p->SetDependent(true);
   ps->AddVariable(varPhi_p);

   SetPhaseSpace(ps);
}
//______________________________________________________________________________

void QuasiElasticInclusiveDiffXSec::DefineEvent(Double_t * vars)
{
   // Virtual method to define the random event from the variables provided.
   // This is the transition point between the phase space variables and
   // the particles. The argument should be the full list of variables returned from
   // GetDependentVariables. 

   Int_t totvars = 0;

   insane::Kine::SetMomFromEThetaPhi((TParticle*)(fParticles.At(0)), &vars[totvars]);
   totvars += GetNParticleVars(0);

   insane::Kine::SetEFromMomThetaPhi((TParticle*)(fParticles.At(1)), &vars[totvars]);
   totvars += GetNParticleVars(1);

}
//______________________________________________________________________________

Double_t * QuasiElasticInclusiveDiffXSec::GetDependentVariables(const Double_t * x) const
{
   double mass = fTargetNucleus.GetMass();//GetParticlePDG(1)->Mass();
   TVector3 k1(0, 0, fBeamEnergy); // incident electron
   TVector3 k2(0, 0, 0);          // scattered electron
   double eprime  = GetEPrime(x[0]);
   double theta   = x[0];
   if(std::isnan(theta) ){
      return nullptr;
   }
   double phi    = x[1];

   k2.SetMagThetaPhi(eprime, theta, phi);
   TVector3 p2 = k1 - k2; // recoil proton
   fDependentVariables[0] = eprime;
   fDependentVariables[1] = theta;
   fDependentVariables[2] = phi;
   fDependentVariables[3] = p2.Mag();
   fDependentVariables[4] = p2.Theta();
   fDependentVariables[5] = p2.Phi();
   if (p2.Phi() < 0.0) fDependentVariables[5] = p2.Phi() + 2.0 * TMath::Pi() ;
   return(fDependentVariables);
}
//______________________________________________________________________________

Double_t QuasiElasticInclusiveDiffXSec::GetEPrime(const Double_t theta)const  {
   // Returns the scattered electron energy using the angle. */
   Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
   return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M * TMath::Power(TMath::Sin(theta / 2.0), 2)));
}
//________________________________________________________________________

Double_t QuasiElasticInclusiveDiffXSec::GetTheta(double ep) const {
   using namespace TMath;
   double M     = fTargetNucleus.GetMass();
   double Eb    = GetBeamEnergy();
   double theta = 2.0*ASin(Sqrt((Eb-ep)*M/(2.0*Eb*ep)));
   return theta;
}
//______________________________________________________________________________

Double_t  QuasiElasticInclusiveDiffXSec::EvaluateXSec(const Double_t * x) const {
   // Get the recoiling proton momentum
   if(!x) return(0.0);
   if (!VariablesInPhaseSpace(6, x)) return(0.0);

   using namespace insane::units;
   using namespace TMath;

   //double M = GetParticlePDG(1)->Mass();
   //if(x[3] < M ) return 0.0;

   double Eprime   = x[0];
   double theta    = x[1] ;
   double E0       = GetBeamEnergy();
   double M        = M_p/GeV;//GetParticlePDG(1)->Mass();
   double Qsquared = 4.0*Eprime*E0*Power(Sin(theta/2.0),2);
   double tau      = Qsquared/(4.0*M*M);

   double mottXSec = insane::Kine::Sig_Mott(E0, theta);
   double f_recoil = insane::Kine::fRecoil( E0, theta, M);

   double GE2     = Power(fFormFactors->GEp(Qsquared), 2);
   double GM2     = Power(fFormFactors->GMp(Qsquared), 2);
   double sig_el  = (mottXSec/f_recoil)*((GE2+tau*GM2)/(1.0+tau)+2.0*tau*GM2*Power(Tan(theta/2.0),2));

   //std::cout << " Qsq= " << Qsquared;
   //std::cout << " tau= " << tau;
   //std::cout << " GE2= " << GE2;
   //std::cout << " GM2= " << GM2  << "\n";;
   double F_RC  = insane::physics::F_Tsai(fBeamEnergy,Eprime,theta);  
   double RC    = insane::physics::RCToPeak_Tsai(fBeamEnergy,Eprime,theta);  
   //std::cout << " F_RC= " << F_RC;
   //std::cout << " RC  = " << RC    << "\n";;
   double res   = RC*F_RC*sig_el;
   res = res * hbarc2_gev_nb;

   if( IncludeJacobian() ) return( TMath::Sin(theta)*res);
   return(res);
}
//______________________________________________________________________________

Double_t  QuasiElasticInclusiveDiffXSec::EvaluateBaseXSec(const Double_t * x) const
{
   // Get the recoiling proton momentum
   if(!x) return(0.0);
   if (!VariablesInPhaseSpace(6, x)) return(0.0);

   //double M = GetParticlePDG(1)->Mass();
   double M = fTargetNucleus.GetMass();//GetParticlePDG(1)->Mass();

   //if(x[3] < M ) return 0.0;

   Double_t Eprime   = x[0];
   Double_t theta    = x[1] ;
   Double_t Qsquared = 4.0 * Eprime * GetBeamEnergy() * TMath::Power(TMath::Sin(theta / 2.0), 2);
   //Double_t tau = Qsquared / (4.0 * M * M);
   Double_t mottXSec = (1. / 137.) * (1. / 137.) *
      TMath::Power(TMath::Cos(theta / 2.0), 2) /
      (4.0 * GetBeamEnergy() * GetBeamEnergy() * TMath::Power(TMath::Sin(theta / 2.0), 4))
      * (Eprime / GetBeamEnergy());
   Double_t GE2 = TMath::Power(fFormFactors->FCHe4(Qsquared), 2);

   //std::cout << " Qsq= " << Qsquared;
   //std::cout << " tau= " << tau;
   //std::cout << " GE2= " << GE2;
   //std::cout << " GM2= " << GM2  << "\n";;

   Double_t res = mottXSec*GE2;// ((GE2 + tau * GM2) / (1.0 + tau) + 2.0 * tau * GM2 * TMath::Power(TMath::Tan(theta / 2.0), 2));
   res = res * hbarc2_gev_nb;

   if( IncludeJacobian() ) return( TMath::Sin(theta)*res);
   return(res);
}
//______________________________________________________________________________


}
}
