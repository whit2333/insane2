#include "insane/xsections/MAIDInclusiveDiffXSec.h"
#include <fstream>

namespace insane {
namespace physics {

MAIDInclusiveDiffXSec::MAIDInclusiveDiffXSec(const char* target)
{
   fID         = 100104031;
   fPion    = "pion"; 
   fNucleon = target; 
   frxn     = Form("%s",fNucleon.Data());
   SetTitle("MAIDInclusiveDiffXSec");
   SetPlotTitle("MAID #vec{p}(#vec{e},e'#pi)p");
   fLabel      = "#int #frac{d#sigma}{dEd#Omega#Omega_{#pi}} d#Omega_{#pi}";
   fUnits      = "nb/GeV/sr";
   TString grid_path = std::getenv("InSANE_PDF_GRID_DIR");
   fprefix     = grid_path + Form("/maid2007/"); // input directory name 
   fDebug      = false;                          // debug flag 
   fh          = 0.;                             // electron helicity (+/- 1) or 0 
   fP_t.SetXYZ(0.0,0.0,0.0);
   fPx         = 0.;                             // target polarization parallel to virtual photon momentum in the scattering plane
   fPy         = 0.;                             // target polarization perpendicular to the scattering plane
   fPz         = 0.;                             // target polarization perpendicular to virtual photon momentum in the scattering plane
   SetVirtualPhotonFluxConvention(2);            // default is the Hand convention
   // SetCrossSectionType(1);                        // cross section type 
   // SetUnits(1);                                   // default is nb, GeV  
   ImportData();
}
//______________________________________________________________________________

MAIDInclusiveDiffXSec::MAIDInclusiveDiffXSec(const char* pion,const char* nucleon)
{
   fPion    = pion; 
   fNucleon = nucleon; 
   frxn     = Form("%s_%s",fPion.Data(),fNucleon.Data());
   SetTitle("MAIDInclusiveDiffXSec");
   SetPlotTitle("MAID #vec{p}(#vec{e},e'#pi)p");
   fLabel      = "#int #frac{d#sigma}{dEd#Omega#Omega_{#pi}} d#Omega_{#pi}";
   fUnits      = "nb/GeV/sr";
   TString grid_path = std::getenv("InSANE_PDF_GRID_DIR");
   fprefix     = grid_path + Form("/maid2007/"); // input directory name 
   fDebug      = false;                          // debug flag 
   fh          = 0.;                             // electron helicity (+/- 1) or 0 
   fP_t.SetXYZ(0.0,0.0,0.0);
   fPx         = 0.;                             // target polarization parallel to virtual photon momentum in the scattering plane
   fPy         = 0.;                             // target polarization perpendicular to the scattering plane
   fPz         = 0.;                             // target polarization perpendicular to virtual photon momentum in the scattering plane
   SetVirtualPhotonFluxConvention(2);            // default is the Hand convention
   // SetCrossSectionType(1);                        // cross section type 
   // SetUnits(1);                                   // default is nb, GeV  
   ImportData();
}
//______________________________________________________________________________

MAIDInclusiveDiffXSec::~MAIDInclusiveDiffXSec()
{ }
//______________________________________________________________________________

void MAIDInclusiveDiffXSec::SetReactionChannel(TString pion,TString nucleon){
   fPion    = pion; 
   fNucleon = nucleon; 
   frxn     = Form("%s_%s",pion.Data(),nucleon.Data());
}
//______________________________________________________________________________

void MAIDInclusiveDiffXSec::SetVirtualPhotonFluxConvention(Int_t i)
{
   fConvention = i;
   switch(fConvention){
      case 0: 
         fConventionName = "A";  // axial? 
         break;
      case 1: 
         fConventionName = "Gilman"; 
         break;
      case 2: 
         fConventionName = "Hand";
         break;
      default:
         std::cout << "[MAIDInclusiveDiffXSec::EvaluateVirtualPhotonFlux]: Invalid convention!  Exiting..." << std::endl;
         exit(1);
   }

}
//______________________________________________________________________________

Double_t MAIDInclusiveDiffXSec::EvaluateXSec(const Double_t *par) const
{
   // compute needed variables in GeV   
   Double_t Es     = GetBeamEnergy(); 
   Double_t Ep     = par[0]; 
   Double_t th     = par[1];
   Double_t phi    = par[2];
   Double_t W      = insane::Kine::W_EEprimeTheta(Es,Ep,th); 
   Double_t Q2     = insane::Kine::Qsquared(Es,Ep,th); 
   Double_t w_L_th = (TMath::Power((M_p+M_pion)/GeV,2.0) - TMath::Power(M_p/GeV,2.0) + Q2)/(2.0*M_p/GeV);
   Double_t w_L    = Es - Ep;
   Double_t nu     = Es - Ep;

   //std::cout << "omega_L_th = " << w_L_th << std::endl;
   //std::cout << "omega      = " << w_L << std::endl;

   // convert W to MeV 
   W *= GeV;  
   W  = W/MeV;
   // interpolate from the grid (Bilinear Interpolation from Wikipedia) 
   fKineKey->fW   = W; 
   fKineKey->fQ2  = Q2; 
 
   // MAID model kinematic limits
   if( w_L < w_L_th ) return 0.0;
   if( W  > 2000.0 ) return(0.0);
   if( W  < 1073.2 ) return(0.0);
   if( Q2 > 5.0    ) return(0.0);
   if( Q2 < 0.0    ) return(0.0);

   // ix1 = lower bound, ix2 = upper bound; same is true for the y-coordinate  
   Int_t ia1,ia2; 
   BinarySearch(&fQ2,fKineKey->fQ2,ia1,ia2);

   Int_t ib1,ib2; 
   BinarySearch(&fW,fKineKey->fW,ib1,ib2);

   // variable definitions: 
   // a => Q2; b => W 
   Double_t a   = Q2;   
   Double_t b   = W; 
   Double_t a1  = fQ2.at(ia1); 
   Double_t a2  = fQ2.at(ia2); 
   Double_t b1  = fW.at(ib1);   
   Double_t b2  = fW.at(ib2); 

   // now get (Ep,th) for each (Q2,W) 
   // first, convert W values to GeV
   b  *= (MeV/GeV);  
   b1 *= (MeV/GeV); 
   b2 *= (MeV/GeV); 

   // get x: note the ordering -- Q2 is first, then W.   
   Double_t xBj   = insane::Kine::xBjorken_WQsq(b,a);  
   Double_t xBj11 = insane::Kine::xBjorken_WQsq(b1,a1); 
   Double_t xBj12 = insane::Kine::xBjorken_WQsq(b2,a1); 
   Double_t xBj21 = insane::Kine::xBjorken_WQsq(b1,a2); 
   Double_t xBj22 = insane::Kine::xBjorken_WQsq(b2,a2); 
   
   Double_t nu11  = insane::Kine::nu_WsqQsq(b1*b1,a1); 
   Double_t nu12  = insane::Kine::nu_WsqQsq(b2*b2,a1); 
   Double_t nu21  = insane::Kine::nu_WsqQsq(b1*b1,a2); 
   Double_t nu22  = insane::Kine::nu_WsqQsq(b2*b2,a2); 
   // get y = nu/Es 
   Double_t y     = nu/Es;  
   Double_t y11   = nu11/Es;  
   Double_t y12   = nu12/Es;  
   Double_t y21   = nu21/Es;  
   Double_t y22   = nu22/Es;  
   // get theta 
   Double_t th11  = insane::Kine::Theta_xQ2y(xBj11,a1,y11);   
   Double_t th12  = insane::Kine::Theta_xQ2y(xBj12,a1,y12);   
   Double_t th21  = insane::Kine::Theta_xQ2y(xBj21,a2,y21);   
   Double_t th22  = insane::Kine::Theta_xQ2y(xBj22,a2,y22);   
   // get Ep 
   Double_t Ep11  = insane::Kine::Eprime_W2theta(b1*b1,th11,Es);   
   Double_t Ep12  = insane::Kine::Eprime_W2theta(b2*b2,th12,Es);   
   Double_t Ep21  = insane::Kine::Eprime_W2theta(b1*b1,th21,Es);   
   Double_t Ep22  = insane::Kine::Eprime_W2theta(b2*b2,th22,Es);   

   // convert W boundaries back to MeV
   b  *= (GeV/MeV);  
   b1 *= (GeV/MeV); 
   b2 *= (GeV/MeV); 

   // First, check to see if we need a linear or bilinear interpolation 
   //Double_t F1=0,F2=0; 
   Double_t F11=0,F12=0,F21=0,F22=0,num=0,den=0,res=0; 
   //if( ((a!=a1)&&(a!=a2))&&((b==b1)&&(b==b2)) ){
   //   /// linear interpolation in a only 
   //   std::cout << " linear interp in a " << std::endl;
   //   F1  = ComputeXSecForBin(ia1,ib1,Es,Ep11,th11,phi); 
   //   F2  = ComputeXSecForBin(ia2,ib1,Es,Ep21,th21,phi); 
   //   res = F1 + (F2-F1)*(a-a1)/(a2-a1);  
   //}else if( ((a==a1)&&(a==a2))&&((b!=b1)&&(b!=b2)) ){
   //   /// linear interpolation in b only 
   //   std::cout << " linear interp in b " << std::endl;
   //   F1  = ComputeXSecForBin(ia1,ib1,Es,Ep11,th11,phi); 
   //   F2  = ComputeXSecForBin(ia1,ib2,Es,Ep12,th12,phi); 
   //   res = F1 + (F2-F1)*(b-b1)/(b2-b1);  
   //}else{
      /// bilinear interpolation 
      F11 = ComputeXSecForBin(ia1,ib1,Es,Ep11,th11,phi);  
      F12 = ComputeXSecForBin(ia1,ib2,Es,Ep12,th12,phi);  
      F21 = ComputeXSecForBin(ia2,ib1,Es,Ep21,th21,phi);  
      F22 = ComputeXSecForBin(ia2,ib2,Es,Ep22,th22,phi);  
      num = F11*(a2-a)*(b2-b) + F21*(a-a1)*(b2-b) + F12*(a2-a)*(b-b1) + F22*(a-a1)*(b-b1);  
      den = (a2-a1)*(b2-b1); 
      res = num/den; 
   //}

   // convert to nb/GeV 
   res = res*hbarc2_gev_nb;

   if(res<0){
      if(fDebug){
         std::cout << "[MAIDInclusiveDiffXSec::EvaluateXSec]: Cross section is less than zero! " << std::endl;
         std::cout << "E = "  << Es        << " GeV" << "\t" 
                   << "E' = " << Ep        << " GeV" << "\t" 
                   << "th = " << th/degree << " deg" << "\t" 
                   << "W  = " << b/GeV     << " GeV" << "\t" 
                   << "Q2 = " << Q2        << " GeV^2" << "\t" 
                   << "xs = " << res       << std::endl;
         std::cout << "a1    = " << Form("%.3E",a1) << "\t" << "a = " << Form("%.3E",a) << "\t" << "a2 = " << Form("%.3E",a2) << std::endl;
         std::cout << "b1    = " << Form("%.3E",b1) << "\t" << "b = " << Form("%.3E",b) << "\t" << "b2 = " << Form("%.3E",b2) << std::endl;
      }
      res = 0.;
   }

   Int_t IsNAN = TMath::IsNaN(res); 

   if(IsNAN){
      // std::cout << "[MAIDInclusiveDiffXSec::EvaluateXSec]: NAN! What happened??" << std::endl;
      // std::cout << "E = "  << Es        << " GeV" << "\t" 
      //    << "E' = " << Ep        << " GeV" << "\t" 
      //    << "th = " << th/degree << " deg" << "\t" 
      //    << "W  = " << b/GeV    << " GeV" << "\t" 
      //    << "Q2 = " << Q2        << " GeV^2" << "\t" 
      //    << "xs = " << res       << std::endl;
      // std::cout << "a1    = " << Form("%.3E",a1) << "\t" << "a = " << Form("%.3E",a) << "\t" << "a2 = " << Form("%.3E",a2) << std::endl;
      // std::cout << "b1    = " << Form("%.3E",b1) << "\t" << "b = " << Form("%.3E",b) << "\t" << "b2 = " << Form("%.3E",b2) << std::endl;
      res = 0.; 
   }
   if(fDebug){
      std::cout << "Ebeam = " << Form("%.3E",Es)    << "\t" 
                << "Ep   = "  << Form("%.3E",Ep)    << "\t" 
                << "th   = "  << Form("%.3E",th)    << "\t" 
                << "nu   = "  << Form("%.3E",nu)    << "\t" 
                << "W    = "  << Form("%.3E",W)     << "\t" 
                << "Q2   = "  << Form("%.3E",Q2)    << "\t" 
                << "x    = "  << Form("%.3E",xBj)   << "\t" 
                << "y    = "  << Form("%.3E",y)     << std::endl;
      std::cout << "-----------------------------------------------------------------------------------------------------------------" << std::endl; 
      std::cout << "x11   = " << Form("%.3E",xBj11) << "\t" 
                << "x12  = "  << Form("%.3E",xBj12) << "\t" 
                << "x21  = "  << Form("%.3E",xBj21) << "\t" 
                << "x22  = "  << Form("%.3E",xBj22) << std::endl;
      std::cout << "nu11  = " << Form("%.3E",nu11 ) << "\t" 
                << "nu12 = "  << Form("%.3E",nu12 ) << "\t" 
                << "nu21 = "  << Form("%.3E",nu21 ) << "\t" 
                << "nu22 = "  << Form("%.3E",nu22 ) << std::endl;
      std::cout << "y11   = " << Form("%.3E",y11  ) << "\t" 
                << "y12  = "  << Form("%.3E",y12  ) << "\t" 
                << "y21  = "  << Form("%.3E",y21  ) << "\t" 
                << "y22  = "  << Form("%.3E",y22  ) << std::endl;
      std::cout << "Ep11  = " << Form("%.3E",Ep11 ) << "\t" 
                << "Ep12 = "  << Form("%.3E",Ep12 ) << "\t" 
                << "Ep21 = "  << Form("%.3E",Ep21 ) << "\t" 
                << "Ep22 = "  << Form("%.3E",Ep22 ) << std::endl;
      std::cout << "th11  = " << Form("%.3E",th11 ) << "\t" 
                << "th12 = "  << Form("%.3E",th12 ) << "\t" 
                << "th21 = "  << Form("%.3E",th21 ) << "\t" 
                << "th22 = "  << Form("%.3E",th22 ) << std::endl;
      std::cout << "a1    = " << Form("%.3E",a1) << "\t" << "a = " << Form("%.3E",a) << "\t" << "a2 = " << Form("%.3E",a2) << std::endl;
      std::cout << "b1    = " << Form("%.3E",b1) << "\t" << "b = " << Form("%.3E",b) << "\t" << "b2 = " << Form("%.3E",b2) << std::endl;
      std::cout << "-----------------------------------------------------------------------------------------------------------------" << std::endl; 
   }

   // std::cout << "Ep = " << Ep << "\t" << "W = " << W << "\t" << "Q2 = " << Q2 << "\t" << "xs = " << res << std::endl;
   if(IncludeJacobian()) return res*TMath::Sin(th);
   return res;  

}
//______________________________________________________________________________
Double_t MAIDInclusiveDiffXSec::ComputeXSecForBin(Int_t ix,Int_t iy,Double_t E,Double_t Eprime,Double_t theta,Double_t phi) const{

   // First get the target polariztion in the scattering plane coordinates.
   TVector3 k1(0.0,0.0,E  );
   TVector3 k2(1.0,0.0,0.0);  // to avoid warnings, we give the x component a value of 1.

   Int_t IsNAN_Ep = TMath::IsNaN(Eprime); 
   Int_t IsNAN_th = TMath::IsNaN(theta); 
   Int_t IsNAN_ph = TMath::IsNaN(phi); 

   if(Eprime==0){
      std::cout << "[MAIDInclusiveDiffXSec::ComputeXSecForBin]: Eprime is zero! " << std::endl;
   }
   // if(IsNAN_Ep){
   //    std::cout << "[MAIDInclusiveDiffXSec::ComputeXSecForBin]: Eprime is NAN! " << std::endl;
   // }
   // if(IsNAN_th){
   //    std::cout << "[MAIDInclusiveDiffXSec::ComputeXSecForBin]: theta is NAN! " << std::endl;
   // }
   // if(IsNAN_ph){
   //    std::cout << "[MAIDInclusiveDiffXSec::ComputeXSecForBin]: phi is NAN! " << std::endl;
   // }

   k2.SetMagThetaPhi(Eprime,theta,phi);
   TVector3 q = k1 - k2;

   // Rotation Matrix from lab to scattering plane coords
   TRotation rot;
   rot.SetToIdentity();
   if(theta != 0.0) {
      // check that q is not a real photon (ie theta_q is along k1); 
      TVector3 YScat = k1.Cross(k2);
      YScat.SetMag(1.0);
      TVector3 ZScat = q;
      ZScat.SetMag(1.0);
      TVector3 XScat = YScat.Cross(ZScat);
      XScat.SetMag(1.0);
      rot.RotateAxes(XScat,YScat,ZScat);
   }
   rot.Invert();

   TVector3 Pt = fP_t;
   Pt.Transform(rot);
   fPx = Pt.X();
   fPy = Pt.Y();
   fPz = Pt.Z();

   Int_t i;   
   Double_t sig_L,sig_T,sig_LT,sig_LTpr,sig_TTpr;
   Double_t eps         = insane::Kine::epsilon(E,Eprime,theta); 
   Double_t Gamma       = EvaluateVirtualPhotonFlux(E,Eprime,theta); 

   fKineKey->fQ2 = fQ2.at(ix); 
   fKineKey->fW  = fW.at(iy); 
   i             = fGridData.BinarySearch(fKineKey); 
   if(i>=0){
      sig_L    = ( (MAIDKinematicKey*)fGridData.At(i) )->fSigma_L/hbarc2_gev_ub; 
      sig_T    = ( (MAIDKinematicKey*)fGridData.At(i) )->fSigma_T/hbarc2_gev_ub; 
      sig_LT   = ( (MAIDKinematicKey*)fGridData.At(i) )->fSigma_LT/hbarc2_gev_ub; 
      sig_LTpr = ( (MAIDKinematicKey*)fGridData.At(i) )->fSigma_LTpr/hbarc2_gev_ub; 
      sig_TTpr = ( (MAIDKinematicKey*)fGridData.At(i) )->fSigma_TTpr/hbarc2_gev_ub; 
      //((MAIDKinematicKey*)fGridData.At(i))->Dump();
   } else {
      std::cout << " key not found " << std::endl;
   }

   Double_t F  =  Gamma*(   sig_T 
                          + eps*sig_L 
                          + fPy*   TMath::Sqrt(2.*eps*(1.+eps))*sig_LT 
                          + fPx*fh*TMath::Sqrt(2.*eps*(1.-eps))*sig_LTpr 
                          + fPz*fh*TMath::Sqrt(1.0-eps*eps)*sig_TTpr
                        ); 
   //if( F == INFINITY || TMath::IsNaN(F) ) {
   //   ((MAIDKinematicKey*)fGridData.At(i))->Dump();
   //   std::cout << "eps = " << eps << std::endl;
   //   std::cout << "gamma = " << Gamma << std::endl;
   //   std::cout << "Eprime = " << Eprime << std::endl;
   //   std::cout << "theta = " << theta << std::endl;
   //   std::cout << "phi = " << phi << std::endl;
   //   k1.Print();
   //   k2.Print();
   //   q.Print();
   //   Pt.Print();
   //}
   return F; 

}
//______________________________________________________________________________
Double_t MAIDInclusiveDiffXSec::EvaluateVirtualPhotonFlux(Double_t Es,Double_t Ep,Double_t th) const{

   Double_t nu    = Es-Ep; 
   Double_t Q2    = insane::Kine::Qsquared(Es,Ep,th);
   // when photon is real, virtual photon flux = 0.
   if( Q2 == 0 ) return 0.0;
   Double_t eps   = insane::Kine::epsilon(Es,Ep,th); 
   Double_t W2    = insane::Kine::W2(Q2,nu); 
   //Double_t K     = (W2 - (M_p/GeV)*(M_p/GeV))/(2.0*(M_p/GeV)); // Hand convention
   Double_t K     = EvaluateFluxDensity(nu,Q2);  
   Double_t alpha = fine_structure_const; 
   Double_t T1    = alpha/(2.*pi*pi); 
   Double_t T2    = Ep/Es; 
   Double_t T3    = K/Q2; 
   Double_t T4    = 1./(1.-eps); 
   Double_t gamma = T1*T2*T3*T4; 
   return gamma; 

}
//______________________________________________________________________________
Double_t MAIDInclusiveDiffXSec::EvaluateFluxDensity(Double_t nu,Double_t Q2) const{

   Double_t K = 0.; 
   Double_t M = M_p/GeV;

   switch(fConvention){
      case 0: // virtual photon energy 
         K = nu; 
         break;
      case 1: // Gilman: use q-vector 
         K = TMath::Sqrt(nu*nu + Q2); 
         break;
      case 2: // Hand: use equivalent energy necessary for the same reaction from a real photon 
         K = nu - Q2/(2.*M); 
         break;
      default: 
         std::cout << "[MAIDInclusiveDiffXSec::EvaluateFluxDensity]: Invalid convention!  Exiting..." << std::endl;
         exit(1);
   }

   return K; 

}
//______________________________________________________________________________
void MAIDInclusiveDiffXSec::BinarySearch(std::vector<Double_t> *array,Double_t key,Int_t &lowerbound,Int_t &upperbound) const {
   Int_t comparisonCount = 1;    //count the number of comparisons (optional)
   Int_t n               = array->size();
   lowerbound            = 0;
   upperbound            = n-1;

   // To start, find the subscript of the middle position.
   Int_t position = ( lowerbound + upperbound) / 2;

   while((array->at(position) != key) && (lowerbound <= upperbound)){
      comparisonCount++;
      if (array->at(position) > key){
         // decrease position by one.
         upperbound = position - 1;
      }else{
         // Else, increase position by one.
         lowerbound = position + 1;
      }
      position = (lowerbound + upperbound) / 2;
   }

   Double_t lo=0,hi=0,mid;
   Int_t dump = lowerbound;

   if (lowerbound <= upperbound){
      // Here we have an exact match to the key
      // std::cout << "[BinarySearch]: The number was found in array subscript " << position << std::endl; 
      // std::cout << "                The binary search found the number after " << comparisonCount << " comparisons." << std::endl;             
      // lo  = array[lowerbound];
      // hi  = array[upperbound];
      // mid = array[position]  
      // if(lo==hi){
      lowerbound = position;
      upperbound = position+1;
      // }
   }else{
      // Here the lower bound surpassed the upper
      lowerbound = upperbound;
      upperbound = dump;
   }
   // to safeguard against values that are outside the boundaries of the grid 
   if(upperbound>=n){
      upperbound = n-1;
      lowerbound = n-2;
   }
   if(upperbound==0){
      lowerbound = 0;
      upperbound = 1;
   }
   //lo   = array->at(lowerbound);
   //hi   = array->at(upperbound);
   // std::cout << "[BinarySearch]: Sorry, the number is not in this array.  The binary search made " << comparisonCount << " comparisons." << std::endl;
   // std::cout << "                Target = "         << key << std::endl;
   // std::cout << "                Bounding values: " << std::endl;
   // std::cout << "                low  = "           << lo << std::endl;
   // std::cout << "                high = "           << hi << std::endl;
}

//______________________________________________________________________________
void MAIDInclusiveDiffXSec::ImportData(){

   fFileName = fprefix + Form("total_xsecs_%s.txt",frxn.Data());

   fKineKey = new MAIDKinematicKey();

   Double_t iQ2,iW,iWLab,iXS_T,iXS_L,iXS_LT,iXS_LTpr,iXS_TTpr,iXS_L0,iXS_LT0,iXS_LT0pr; 

   std::ifstream infile(fFileName.Data());
   Int_t N = 0;
   if(infile.fail()){
      std::cout << "[MAIDInclusiveDiffXSec::ImportData]: Cannot open the file: " << fFileName.Data() << std::endl;
      exit(1); 
   }else{
      std::cout << "[MAIDInclusiveDiffXSec::ImportData]: Opening the file: " << fFileName.Data() << std::endl;
      while(!infile.eof()){
         auto * akey = new MAIDKinematicKey(N);
         infile >> iQ2 >> iW >> iWLab >> iXS_T >> iXS_L >> iXS_LT >> iXS_LTpr >> iXS_TTpr >> iXS_L0 >> iXS_LT0 >> iXS_LT0pr; 
         akey->fW           = iW; 
         akey->fQ2          = iQ2; 
         akey->fSigma_L     = iXS_L;
         akey->fSigma_T     = iXS_T;
         akey->fSigma_LT    = iXS_LT;
         akey->fSigma_LTpr  = iXS_LTpr;
         akey->fSigma_TTpr  = iXS_TTpr;
         akey->fSigma_L0    = iXS_L0;
         akey->fSigma_LT0   = iXS_LT0;
         akey->fSigma_LT0pr = iXS_LT0pr;
         fGridData.Add(akey);
         N++;
      }
      infile.close(); 
   }
   fGridData.Sort();
   //fGridData.Print();

   auto * akey = (MAIDKinematicKey*)fGridData.At(0);
   fW.push_back(akey->fW);  
   fQ2.push_back(akey->fQ2);  
   for(int i = 0;i<fGridData.GetEntries();i++){
      akey = (MAIDKinematicKey*)fGridData.At(i);
      if(fW.back()  != akey->fW)  fW.push_back(akey->fW); 
      if(fQ2.back() != akey->fQ2) fQ2.push_back(akey->fQ2); 
   }

   // erase repeated entries 
   std::sort( fW.begin(),fW.end() ); 
   fW.erase( std::unique( fW.begin(),fW.end() ),fW.end() ); 
 
   std::sort( fQ2.begin(),fQ2.end() ); 
   fQ2.erase( std::unique( fQ2.begin(),fQ2.end() ),fQ2.end() );  
   
   // print the size of the grid  
   std::cout << "[MAIDInclusiveDiffXSec::ImportData]: Grid dimensions: " << fW.size() << " x " << fQ2.size() << std::endl;

   fNumberOfPoints = fW.size(); 

}
//______________________________________________________________________________
void MAIDInclusiveDiffXSec::Print(){
   PrintParameters(); 
   PrintData();
}
//______________________________________________________________________________
void MAIDInclusiveDiffXSec::PrintParameters(){

   std::cout << "-------------------- MAID Parameters --------------------" << std::endl; 
   std::cout << "Reaction channel (pion,nucleon) = " << "(" << fPion << "," << fNucleon << ")" << std::endl; 
   std::cout << "Virtual photon flux convention  = " << fConvention << " (" << fConventionName << ")" << std::endl;
   std::cout << "Number of data points           = " << fNumberOfPoints     << std::endl;
   std::cout << "---------------------------------------------------------" << std::endl; 

}
//______________________________________________________________________________
void MAIDInclusiveDiffXSec::PrintData(){

   std::cout << "-------------------- MAID Data --------------------" << std::endl; 
   if(fNumberOfPoints==0){
      std::cout << "[MAIDInclusiveDiffXSec::PrintData]: No data!  Exiting..." << std::endl;
      exit(1); 
   }

   // for(int i=0;i<fNumberOfPoints;i++){
   //    std::cout << "Q2       = " << Form("%.3f",fQ2[i]    )     << std::endl; 
   //    std::cout << "W        = " << Form("%.3f",fW[i]    )      << std::endl; 
   //    // std::cout << "Es       = " << Form("%.3f",fEs[i]   )      << std::endl; 
   //    // std::cout << "Ep       = " << Form("%.3f",fEprime[i])     << std::endl; 
   //    // std::cout << "nu       = " << Form("%.3f",fNu[i]   )      << std::endl; 
   //    // std::cout << "Gamma    = " << Form("%.3f",fGamma[i])      << std::endl; 
   //    std::cout << "xs_T     = " << Form("%.3f",fXS_T[i] )      << std::endl; 
   //    std::cout << "xs_L     = " << Form("%.3f",fXS_L[i] )      << std::endl; 
   //    std::cout << "xs_LT    = " << Form("%.3f",fXS_LT[i] )     << std::endl; 
   //    std::cout << "xs_LTpr  = " << Form("%.3f",fXS_LTpr[i] )   << std::endl; 
   //    std::cout << "xs_TTpr  = " << Form("%.3f",fXS_TTpr[i] )   << std::endl; 
   //    std::cout << "xs_L0    = " << Form("%.3f",fXS_L0[i] )     << std::endl; 
   //    std::cout << "xs_LT0   = " << Form("%.3f",fXS_LT0[i] )    << std::endl; 
   //    std::cout << "xs_LT0pr = " << Form("%.3f",fXS_LT0pr[i] )  << std::endl; 
   //    // std::cout << "xs_TT    = " << Form("%.3f",fXS_TT[i] )     << std::endl; 
   //    // std::cout << "xs_TL    = " << Form("%.3f",fXS_TL[i] )     << std::endl; 
   //    // std::cout << "xs_TLpr  = " << Form("%.3f",fXS_TLpr[i] )   << std::endl;  
   //    // std::cout << "xs_T31   = " << Form("%.3f",fXS_T31[i] )    << std::endl;  
   //    // std::cout << "xs   = "     << Form("%.3f",fXSf[i]  )      << std::endl;
   //    std::cout << "---------------------------------------------------------" << std::endl; 
   // }

}
//______________________________________________________________________________
void MAIDInclusiveDiffXSec::Clear(){

   fQ2.clear();
   fW.clear();
   fEprime.clear();
   fTheta.clear();
   fNu.clear(); 
   // fGamma.clear(); 
   fXS_T.clear(); 
   fXS_L.clear();
   fXS_LT.clear();
   fXS_LTpr.clear();
   fXS_TTpr.clear();
   fXS_L0.clear();
   fXS_LT0.clear();
   fXS_LT0pr.clear();
   // fXS_TT.clear(); 
   // fXS_TL.clear(); 
   // fXS_TLpr.clear(); 
   // fXS_T31.clear(); 
   // fXSf.clear(); 
   // fth = 0.; 
   // fQ2 = 0.; 

}
}}
