#include "insane/xsections/POLRADUltraRelInelasticTailDiffXSec.h"
#include "insane/base/InSANEPhysics.h"

namespace insane {
namespace physics {

POLRADUltraRelInelasticTailDiffXSec::POLRADUltraRelInelasticTailDiffXSec()
{
   SetTitle("POLRADInelasticTailDiffXSec");
   SetPlotTitle("POLRAD IRT cross-section");

   fPOLRADCalc = new POLRADUltraRelativistic();
   fPOLRADCalc->SetVerbosity(1);
   //fPOLRADCalc->DoQEFullCalc(false); 
   fPOLRADCalc->SetTargetNucleus(Nucleus::Proton());
   fPOLRADCalc->fErr   = 1E-7;   // integration error tolerance 
   fPOLRADCalc->fDepth = 6;     // number of iterations for integration 
   fPOLRADCalc->SetMultiPhoton(true); 
   //fPOLRADCalc->SetUltraRel(true); 


   // Structure functions F1,F2
   StructureFunctions * sf   = new DefaultStructureFunctions();
   SetUnpolarizedStructureFunctions(sf);

   // Structure functions g1,g2
   PolarizedStructureFunctions * psf   = new DefaultPolarizedStructureFunctions();
   SetPolarizedStructureFunctions(psf);

   // Quasi Elastic structure functions
   auto * F1F209QESFs = new F1F209QuasiElasticStructureFunctions();
   SetQEStructureFunctions(F1F209QESFs);

   // Nucleon form factors 
   FormFactors * FFs = new DefaultFormFactors(); 
   SetFormFactors(FFs);

   // Nuclei form factors 
   //FormFactors * NFFs = new AmrounFormFactors();
   FormFactors * NFFs = new MSWFormFactors();
   SetTargetFormFactors(NFFs);

}
//______________________________________________________________________________

POLRADUltraRelInelasticTailDiffXSec::~POLRADUltraRelInelasticTailDiffXSec()
{ }
//______________________________________________________________________________

POLRADUltraRelInelasticTailDiffXSec *  
POLRADUltraRelInelasticTailDiffXSec::Clone(const char * newname) const 
{
   std::cout << "POLRADUltraRelInelasticTailDiffXSec::Clone()\n";
   auto * copy = new POLRADUltraRelInelasticTailDiffXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

Double_t POLRADUltraRelInelasticTailDiffXSec::EvaluateXSec(
      const Double_t *x) const
{
   if (!VariablesInPhaseSpace(fnDim, x)){
      std::cout << "[POLRADInelasticTailDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
      return(0.0);
   }

   Double_t Eprime  = x[0];
   Double_t theta   = x[1];
   Double_t Ebeam   = GetBeamEnergy();
   Double_t nu      = Ebeam-Eprime;
   Double_t Mtarg   = M_p/GeV;// 0.938

   fPOLRADCalc->SetKinematics(Ebeam,Eprime,theta);

   Double_t sig_rad  = ((POLRADUltraRelativistic*)fPOLRADCalc)->sig_in();

   sig_rad = sig_rad * (Eprime/(2.0*TMath::Pi()*Mtarg*nu));
   if(IncludeJacobian()){
      sig_rad *= TMath::Sin(theta);
   }
   return sig_rad*hbarc2_gev_nb;
} 
//______________________________________________________________________________
}}
