#include "insane/xsections/IncoherentDVCSXSec.h"
#include "insane/base/PhysicalConstants.h"
#include "TMath.h"
#include "Finsane/formfactors/ormFactors.h"
#include "TVector3.h"
#include "insane/nuclear/FermiMomentumDist.h"
#include "insane/xsections/DVCS.h"
#include <cmath>

using namespace insane::physics;

IncoherentDVCSXSec::IncoherentDVCSXSec()
{
   fID            = 201100000;
   SetTitle("IncoherentDVCSXSec");
   SetPlotTitle("Incoherent DVCS cross-section");
   fLabel         = "#frac{d#sigma}{dt dx dQ^{2} d#phi }";
   fUnits         = "nb/sr";
   fnDim          = 8; // 4 for DVCS on the nucleon + 3 for nucleon fermi momentum
   fnParticles    = 4;
   fPIDs.clear();
   fPIDs.push_back(11);
   fPIDs.push_back(2212);
   fPIDs.push_back(22);
   fPIDs.push_back(1000020030);
}
//______________________________________________________________________________

IncoherentDVCSXSec::~IncoherentDVCSXSec()
{ }
//______________________________________________________________________________

void IncoherentDVCSXSec::SetRecoilNucleus(const Nucleus& spectator)
{
   Nucleus target     = GetTargetNucleus();
   fSpectatorTargetFragment = spectator;
   fActiveTargetFragment    = target - fSpectatorTargetFragment;
   //fActiveXSec->SetTargetNucleus(fActiveTargetFragment);
   
   fPIDs[1] = fActiveTargetFragment.GetPdgCode();
   fPIDs[3] = fSpectatorTargetFragment.GetPdgCode();

   std::cout << "Target:" << std::endl;
   target.Print();

   std::cout << "Active:" << std::endl;
   fActiveTargetFragment.Print();

   std::cout << "Spectator:" << std::endl;
   fSpectatorTargetFragment.Print();
}
//______________________________________________________________________________

void IncoherentDVCSXSec::SetRecoilNucleus(const Nucleus& spectator, const Nucleus& active)
{
   Nucleus target     = spectator + active;
   DiffXSec::SetTargetNucleus(target);
   fSpectatorTargetFragment = spectator;
   fActiveTargetFragment    = active;
   //fActiveXSec->SetTargetNucleus(fActiveTargetFragment);
   
   fPIDs[1] = fActiveTargetFragment.GetPdgCode();
   fPIDs[3] = fSpectatorTargetFragment.GetPdgCode();

   std::cout << "Target:" << std::endl;
   target.Print();

   std::cout << "Active:" << std::endl;
   fActiveTargetFragment.Print();

   std::cout << "Spectator:" << std::endl;
   fSpectatorTargetFragment.Print();
}
//______________________________________________________________________________

void IncoherentDVCSXSec::SetTargetNucleus(const Nucleus& target)
{
   DiffXSec::SetTargetNucleus( target );
   SetRecoilNucleus( fSpectatorTargetFragment );
}
//______________________________________________________________________________

double IncoherentDVCSXSec::xsec_e1dvcs(const dvcsvar_t &var,const dvcspar_t &par) const
{
   using namespace TMath;
   if(var.Q2<0.5) return 0.0;
   if(var.x<0.01) return 0.0;
   double T1 = Power( par.Q2_0/var.Q2, par.alpha);
   double dx = (var.x - par.xc)/par.c;
   double T2 = 1.0/(1.0+dx*dx);
   double T3 = Power(1.0/(1.0+par.b*var.t), par.beta);
   double T4 = (1.0-par.d*(1.0-Cos(var.phi)));
   double res = T1*T2*T3*T4;
   if(res>2) {
      std::cout << "Q2 " << var.Q2 << std::endl;
      std::cout << "x  " << var.x << std::endl;
      std::cout << "t  " << var.t << std::endl;
      std::cout << "phi" << var.phi << std::endl;
   }
   
   return(res  );
}
//______________________________________________________________________________

void IncoherentDVCSXSec::InitializePhaseSpaceVariables()
{

   using namespace insane::units;
   PhaseSpace * ps = GetPhaseSpace();
   // ------------------------------
   // Electron
   auto * varEnergy = new PhaseSpaceVariable("energy_e", "E_{e'}",0.5, 9.0);
   varEnergy->SetParticleIndex(0);
   varEnergy->SetDependent(true);
   ps->AddVariable(varEnergy);

   auto * varTheta = new PhaseSpaceVariable("theta_e", "#theta_{e'}",6.0*degree, 180.0*degree);
   varTheta->SetParticleIndex(0);
   varTheta->SetDependent(true);
   ps->AddVariable(varTheta);

   auto * varPhi = new PhaseSpaceVariable("phi_e", "#phi_{e'}",-180.0*degree, 180.0*degree);
   varPhi->SetParticleIndex(0);
   varPhi->SetDependent(true); // calculated from the phi below
   ps->AddVariable(varPhi);


   // ------------------------------
   // Proton i_start = 3
   auto * varP_p = new PhaseSpaceVariable("P_p", "P_{p}",0.500,10.0);
   varP_p->SetParticleIndex(1);
   varP_p->SetDependent(true);
   ps->AddVariable(varP_p);

   auto * varTheta_p = new PhaseSpaceVariable("theta_p", "#theta_{p}",5.0*degree, 180.0*degree);
   varTheta_p->SetParticleIndex(1);
   varTheta_p->SetDependent(true);
   ps->AddVariable(varTheta_p);

   auto * varPhi_p = new PhaseSpaceVariable("phi_p", "#phi_{p}", -1.0*TMath::Pi(), 1.0*TMath::Pi() );
   varPhi_p->SetParticleIndex(1);
   varPhi_p->SetDependent(true);
   ps->AddVariable(varPhi_p);

   // ------------------------------
   // photon i_start = 6
   auto * varEnergy_g = new PhaseSpaceVariable("energy_gamma", "E_{#gamma}",0.5,11.0);
   varEnergy_g->SetParticleIndex(2);
   varEnergy_g->SetDependent(true);
   ps->AddVariable(varEnergy_g);

   auto * varTheta_g = new PhaseSpaceVariable("theta_gamma", "#theta_{#gamma}",5.0*degree, 55.0*degree);
   varTheta_g->SetParticleIndex(2);
   varTheta_g->SetDependent(true);
   ps->AddVariable(varTheta_g);

   auto * varPhi_g = new PhaseSpaceVariable("phi_gamma", "#phi_{#gamma}",-1.0*TMath::Pi(), 1.0*TMath::Pi() );
   varPhi_g->SetParticleIndex(2);
   varPhi_g->SetDependent(true);
   ps->AddVariable(varPhi_g);


   // ------------------------------
   // i_start = 9
   auto * varPhi_p1RestFrame = new PhaseSpaceVariable(
         "phi_ep1rest", "#phi_{e' in p1 rest frame}", -180.0*degree, 180.0*degree);
   varPhi_p1RestFrame->SetParticleIndex(-1);
   varPhi_p1RestFrame->SetUniform(true);
   ps->AddVariable(varPhi_p1RestFrame);

   auto * var_x = new PhaseSpaceVariable("x", "x",0.05, 1.0);
   var_x->SetParticleIndex(-1);
   ps->AddVariable(var_x);

   auto * var_t = new PhaseSpaceVariable("t", "t",-5.0, 0.0);
   var_t->SetParticleIndex(-1);
   ps->AddVariable(var_t);

   auto * var_Q2 = new PhaseSpaceVariable("Q2", "Q^{2}",0.5, 10.0);
   var_Q2->SetParticleIndex(-1);
   ps->AddVariable(var_Q2);

   auto * var_phi = new PhaseSpaceVariable("phi", "#varphi",-180.0*degree, 180.0*degree);
   var_phi->SetParticleIndex(-1);
   ps->AddVariable(var_phi);

   // ------------------------------
   // Initial nucleon i_start = 14 
   auto * varP_p1 = new PhaseSpaceVariable("P_p1", "P_{p1}",0.05,1.0);
   varP_p1->SetParticleIndex(-1);
   ps->AddVariable(varP_p1);

   auto * varTheta_p1 = new PhaseSpaceVariable("theta_p1", "#theta_{p1}",0.0*degree, 180.0*degree);
   varTheta_p1->SetParticleIndex(-1);
   varTheta_p1->SetUniform(true);
   ps->AddVariable(varTheta_p1);

   auto * varPhi_p1 = new PhaseSpaceVariable("phi_p1", "#phi_{p1}", -1.0*TMath::Pi(), 1.0*TMath::Pi() );
   varPhi_p1->SetParticleIndex(-1);
   varPhi_p1->SetUniform(true);
   ps->AddVariable(varPhi_p1);

   // ------------------------------
   // Recoil A-1 i_start = 17
   auto * varP_p2 = new PhaseSpaceVariable("P_p2", "P_{p2}",0.0,1.0);
   varP_p2->SetParticleIndex(3);
   varP_p2->SetDependent(true);
   ps->AddVariable(varP_p2);

   auto * varTheta_p2 = new PhaseSpaceVariable("theta_p2", "#theta_{p2}",0.0*degree, 180.0*degree);
   varTheta_p2->SetParticleIndex(3);
   varTheta_p2->SetDependent(true);
   ps->AddVariable(varTheta_p2);

   auto * varPhi_p2 = new PhaseSpaceVariable("phi_p2", "#phi_{p2}", -1.0*TMath::Pi(), 1.0*TMath::Pi() );
   varPhi_p2->SetParticleIndex(3);
   varPhi_p2->SetDependent(true);
   ps->AddVariable(varPhi_p2);

   // ---------------------------------
   // 20
   auto * var_y = new PhaseSpaceVariable("y", "y",0.0005, 0.99999);
   var_y->SetParticleIndex(-1);
   var_y->SetDependent(true);
   ps->AddVariable(var_y);

   SetPhaseSpace(ps);
}
//______________________________________________________________________________

void IncoherentDVCSXSec::DefineEvent(Double_t * vars)
{
   // Virtual method to define the random event from the variables provided.
   // This is the transition point between the phase space variables and
   // the particles. The argument should be the full list of variables returned from
   // GetDependentVariables. 

   Int_t totvars = 0;

   insane::Kine::SetMomFromEThetaPhi((TParticle*)(fParticles.At(0)), &vars[totvars]);
   totvars += GetNParticleVars(0);

   insane::Kine::SetEFromMomThetaPhi((TParticle*)(fParticles.At(1)), &vars[totvars]);
   totvars += GetNParticleVars(1);

   insane::Kine::SetMomFromEThetaPhi((TParticle*)(fParticles.At(2)), &vars[totvars]);
   totvars += GetNParticleVars(2);

   totvars += (5+3);
   insane::Kine::SetEFromMomThetaPhi((TParticle*)(fParticles.At(3)), &vars[totvars]);
   totvars += GetNParticleVars(3);

}
//______________________________________________________________________________

Double_t * IncoherentDVCSXSec::GetDependentVariables(const Double_t * x) const
{
   using namespace TMath;

   fKine.fR_phi.SetToIdentity();
   fKine.fR_k1.SetToIdentity();
   fKine.fR_q1.SetToIdentity();

   double phi_e_A  = x[0];
   double xBjorken = x[1];
   double t        = x[2];
   double Q2       = x[3];
   double phi      = x[4];
   double P_p1     = x[5];
   double theta_p1 = x[6];
   double phi_p1   = x[7];
   double M        = M_p/GeV;
   double Mbar     = 0.7;//fOffShellMass;
   double E_p1     = Sqrt(P_p1*P_p1 + Mbar*Mbar);

   // --------------------------------------
   // Set the 4 vectors in the lab
   fKine.fk1 = {0, 0, fBeamEnergy, fBeamEnergy };
   fKine.fp1 = {0, 0, P_p1, E_p1 };
   fKine.fp1.SetTheta(theta_p1);
   fKine.fp1.SetPhi(  phi_p1);

   // --------------------------------------
   // test
   TLorentzVector  p1_init =  fKine.fp1;
   TLorentzVector  k1_init =  fKine.fk1;
   double          s_init  = (k1_init + p1_init)*(k1_init + p1_init);

   // --------------------------------------
   // Rotate p1 to phi_p1 = 0
   fKine.fR_phi.SetToIdentity();
   fKine.fR_phi.RotateZ(-phi_p1);
   //fKine.fp1.Print();
   fKine.fp1 = TLorentzRotation(fKine.fR_phi)*(fKine.fp1);
   fKine.fk1 = TLorentzRotation(fKine.fR_phi)*(fKine.fk1);

   // --------------------------------------
   // Boost to struck nucleon rest frame
   fKine.fLambda_p1 = TLorentzRotation(-1.0*(fKine.fp1.BoostVector()));
   fKine.fp1        = (fKine.fLambda_p1)*(fKine.fp1);
   fKine.fk1        = (fKine.fLambda_p1)*(fKine.fk1);
   //fKine.fp1.Print();

   // --------------------------------------
   // Rotate such that k1  is along boosted frame's z-axis.
   fKine.fR_k1.SetToIdentity();
   fKine.fR_k1.RotateY(fKine.fk1.Theta());
   fKine.fp1 = TLorentzRotation(fKine.fR_k1)*(fKine.fp1);
   fKine.fk1 = TLorentzRotation(fKine.fR_k1)*(fKine.fk1);
   //fKine.fk1.Print();

   // --------------------------------------
   // Now we are in the "fixed target" DIS frame 
   // Caclulate the (DIS) k2 kinematics
   double E0_A      = fKine.fk1.E();
   double nu_A      = Q2/(2.0*M*xBjorken);
   double y         = nu_A/E0_A;
   double eprime_A  = E0_A - nu_A;
   double theta_e_A = 2.0*ASin(M*xBjorken*y/Sqrt(Q2*(1.0-y)));

   double s       = Mbar*Mbar - Q2 + 2*Mbar*nu_A; // W^2
   double nu_cm   = Sqrt(Power(Mbar*nu_A-Q2,2.0)/(Mbar*Mbar+2.0*M*nu_A-Q2));
   double q_cm    = Sqrt(nu_cm*nu_cm+Q2);
   double t_min   = -Q2 - ((s-M*M)/(Sqrt(s)))*(nu_cm-q_cm);
   double t_max   = -Q2 - ((s-M*M)/(Sqrt(s)))*(nu_cm+q_cm);
   double ymax    = y_max(epsilon(Q2,xBjorken,M));

   //std::cout << t_min << " > t=" << t << " > " <<  t_max << std::endl;
   //std::cout << "Delta2_min " << Delta2_min(Q2,xBjorken,M) <<std::endl;
   //std::cout << ymax << " > y=" << y << std::endl;
   if(t<t_max) {
      return nullptr;
   }
   if(t>t_min) {
      return nullptr;
   }
   if( (y>ymax) || (y>1.0)){
      return nullptr;
   }
   if( std::isnan(eprime_A) ) {
      //std::cout << " Error: eprime_A is NaN " << std::endl;
      return nullptr;
   }

   // --------------------------------------
   // Set the electron kinematics
   fKine.fk2 = {0, 0, eprime_A, eprime_A };
   fKine.fk2.SetTheta( theta_e_A );
   fKine.fk2.SetPhi(   phi_e_A   );

   // --------------------------------------
   // virtual photon 4 vector
   fKine.fq1              = fKine.fk1 - fKine.fk2; 
   double q_A             = fKine.fq1.Vect().Mag();
   double theta_q         = fKine.fq1.Theta();
   TLorentzVector q1_init = fKine.fq1;

   // --------------------------------------
   // Rotate such that q1 is along z-axis
   fKine.fn_gamma = fKine.fk1.Vect().Cross( fKine.fk2.Vect() ); 
   fKine.fn_gamma.SetMag(1.0);

   fKine.fR_q1.SetToIdentity();
   fKine.fR_q1.Rotate( 1.0*fKine.fq1.Theta(), fKine.fn_gamma );

   fKine.fp1      = TLorentzRotation(fKine.fR_q1)*(fKine.fp1);
   fKine.fq1      = TLorentzRotation(fKine.fR_q1)*(fKine.fq1);
   fKine.fk1      = TLorentzRotation(fKine.fR_q1)*(fKine.fk1);
   fKine.fk2      = TLorentzRotation(fKine.fR_q1)*(fKine.fk2);
   //fKine.fq1.Print();
   //std::cout << " Q2 diff : " << Q2 + (fKine.fq1)*(fKine.fq1) << std::endl;

   TLorentzVector dq1_test = (fKine.fq1 - (fKine.fk1 -fKine.fk2));


   // --------------------------------------
   // Calculate the final state momenta in the hadronic plane
   double E2_A           = (M*M + Mbar*Mbar-t)/(2.0*Mbar);
   double P2_A           = Sqrt( E2_A*E2_A - M*M );
   double nu2_A          = Mbar + nu_A - E2_A;
   double cosTheta_qq2_B = (t+Q2+2.0*nu_A*nu2_A)/(2.0*nu2_A*q_A);
   double theta_qq2_B    = ACos( cosTheta_qq2_B );
   double sintheta_qp2_B = nu2_A*Sin(theta_qq2_B)/P2_A;
   double theta_qp2_B    = ASin(sintheta_qp2_B);
   double q1_phi_B       = 0.0;//fKine.fq1.Phi();
   double phi_p2         = q1_phi_B + phi;

   fKine.fp2 = {0, 0, P2_A, E2_A} ;
   fKine.fp2.SetTheta( theta_qp2_B );
   fKine.fp2.SetPhi(   phi_p2 );

   fKine.fq2 = {0, 0, nu2_A, nu2_A} ;
   fKine.fq2.SetTheta( theta_qq2_B );
   fKine.fq2.SetPhi(   phi_p2 + 180.0*degree );

   double t_min_2 = -Q2 - 2.0*nu2_A*(nu_A - q_A);
   double t_max_2 = -Q2 - 2.0*nu2_A*(nu_A + q_A);
   //std::cout << " delta t_min " << t_min_2 << std::endl;
   //std::cout << " delta t_max " << t_max_2 << std::endl;

   //if( t < t_max_2 ) {
   //   return nullptr;
   //}
   //if( (t > t_min_2) || (t_min_2 > 0) ){
   //   return nullptr;
   //}

   //TLorentzVector pT_test = fKine.fq2 + fKine.fp2;
   //TLorentzVector q1_test0 = fKine.fq1 - fKine.fq2 - fKine.fp2;
   //TLorentzVector q1_0 = fKine.fq1;
   //TLorentzVector q2_0 = fKine.fq2;
   //TLorentzVector p2_0 = fKine.fp2;
   //TLorentzVector p1_0 = fKine.fp1;

   //TLorentzVector q1_test2 = fKine.fk1 - fKine.fk2 - fKine.fp2 - fKine.fq2;

   // --------------------------------------
   // Rotate back to the A' frame
   TLorentzRotation R_q1_back = TLorentzRotation(fKine.fR_q1.Inverse());
   fKine.fk1      = R_q1_back*(fKine.fk1);
   fKine.fp1      = R_q1_back*(fKine.fp1);
   fKine.fq1      = R_q1_back*(fKine.fq1);
   fKine.fk2      = R_q1_back*(fKine.fk2);
   fKine.fp2      = R_q1_back*(fKine.fp2);
   fKine.fq2      = R_q1_back*(fKine.fq2);
   //std::cout << " q1 back  : "; fKine.fq1.Print();

   TLorentzVector k1_1 = fKine.fk1;
   TLorentzVector k2_1 = fKine.fk2;
   TLorentzVector q1_1 = fKine.fq1;
   TLorentzVector q2_1 = fKine.fq2;
   TLorentzVector p2_1 = fKine.fp2;
   TLorentzVector p1_1 = fKine.fp1;

   //TLorentzVector q1_test = fKine.fq1 - fKine.fq2 - fKine.fp2;

   // --------------------------------------
   // rotate back to the unrotated p1 rest frame 
   TLorentzRotation lambda_inv = 
      TLorentzRotation(fKine.fR_phi.Inverse())*
      (fKine.fLambda_p1.Inverse())*
      TLorentzRotation(fKine.fR_k1.Inverse());

   fKine.fk1 = (lambda_inv)*(fKine.fk1);
   fKine.fk2 = (lambda_inv)*(fKine.fk2);
   fKine.fq1 = (lambda_inv)*(fKine.fq1);
   fKine.fq2 = (lambda_inv)*(fKine.fq2);
   fKine.fp1 = (lambda_inv)*(fKine.fp1);
   fKine.fp2 = (lambda_inv)*(fKine.fp2);

   //double          s_final  = (fKine.fk1 + fKine.fp1)*(fKine.fk1 + fKine.fp1);
   TLorentzVector  mom_sum  = fKine.fk1 + fKine.fp1  - fKine.fk2 - fKine.fq2 - fKine.fp2 ;

   if( TMath::Abs(mom_sum.E()) > 0.000001 ) {
      //std::cout << " mom_sum : "; mom_sum.Print();
      return nullptr;
      //std::cout << " k1 : "; fKine.fk1.Print();
      //std::cout << " k2 : "; fKine.fk2.Print();
      //std::cout << " p1 : "; fKine.fp1.Print();
      //std::cout << " p2 : "; fKine.fp2.Print();
      //std::cout << " q1 : "; fKine.fq1.Print();
      //std::cout << " q2 : "; fKine.fq2.Print();

      //std::cout << " P1 test : \n";
      //fKine.fp1.Print();
      //p1_init.Print();
      //std::cout << " k1 test : \n";
      //fKine.fk1.Print();
      //k1_init.Print();
      //std::cout << " q1 test in rotated: \n";
      //q1_test0.Print();
      //std::cout << " q1 test : \n";
      //q1_test.Print();
      //std::cout << " q1 test : \n";
      //dq1_test.Print();
      //std::cout << " q1 test 2 : \n";
      //q1_test2.Print();
      //std::cout << " pT test   : \n";
      //pT_test.Print();
      ////std::cout << " test Q2= " << q_A*q_A - nu_A*nu_A << std::endl;
      ////std::cout << "      Q2= " << Q2 << std::endl;
      //std::cout << "t_min  : " << t_min  << std::endl;
      //std::cout << " test t = " << (fKine.fp1 - fKine.fp2)*(fKine.fp1 - fKine.fp2) << std::endl;
      //std::cout << "      t = " << t << std::endl;
      //std::cout << " init s = " << s_init  << std::endl;
      //std::cout << "      s = " << s_final << std::endl;
   }

   //std::cout << " test2 t = " << (fKine.fp1 - fKine.fp2)*(fKine.fp1 - fKine.fp2) << std::endl;
   //std::cout << " test2 Q2 = " << -1.0*(fKine.fk1 - fKine.fk2)*(fKine.fk1 - fKine.fk2) << std::endl;

   fDependentVariables[0]  = fKine.fk2.E();
   fDependentVariables[1]  = fKine.fk2.Theta();
   fDependentVariables[2]  = fKine.fk2.Phi();

   fDependentVariables[3]  = fKine.fp2.Vect().Mag();
   fDependentVariables[4]  = fKine.fp2.Theta();
   fDependentVariables[5]  = fKine.fp2.Phi();

   fDependentVariables[6]  = fKine.fq2.E();
   fDependentVariables[7]  = fKine.fq2.Theta();
   fDependentVariables[8]  = fKine.fq2.Phi();

   fDependentVariables[9]  = phi_e_A;
   fDependentVariables[10] = xBjorken;
   fDependentVariables[11] = t;
   fDependentVariables[12] = Q2;
   fDependentVariables[13] = phi;

   fDependentVariables[14] = fKine.fp1.Vect().Mag();
   fDependentVariables[15] = fKine.fp1.Theta();
   fDependentVariables[16] = fKine.fp1.Phi();

   TVector3 p2 = fKine.fp1.Vect();
   p2 *= -1.0;

   fDependentVariables[17] = p2.Mag();
   fDependentVariables[18] = p2.Theta();
   fDependentVariables[19] = p2.Phi();

   fDependentVariables[20] = y;

   //std::cout << "vectors" << std::endl;
   //std::cout << "fDependentVariables[0] = " << fDependentVariables[0] << std::endl;
   //std::cout << "fDependentVariables[1] = " << fDependentVariables[1] << std::endl;
   //std::cout << "fDependentVariables[2] = " << fDependentVariables[2] << std::endl;
   //std::cout << "fDependentVariables[3] = " << fDependentVariables[3] << std::endl;
   //std::cout << "fDependentVariables[4] = " << fDependentVariables[4] << std::endl;
   //std::cout << "fDependentVariables[5] = " << fDependentVariables[5] << std::endl;
   //k1.Print();
   //k2.Print();
   //p2.Print();
   //if (p2.Phi() < 0.0) fDependentVariables[5] = p2.Phi() + 2.0 * TMath::Pi() ;
   return(fDependentVariables);
}
//______________________________________________________________________________

Double_t IncoherentDVCSXSec::GetEPrime(const Double_t theta)const  {
   // Returns the scattered electron energy using the angle. */
   Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
   return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M * TMath::Power(TMath::Sin(theta / 2.0), 2)));
}
//________________________________________________________________________

Double_t  IncoherentDVCSXSec::EvaluateXSec(const Double_t * x) const
{
   using namespace TMath;
   using namespace insane::units;
   using namespace insane::physics;
   if( (!x) || (x == nullptr) ) {
      //std::cout << " bad vars \n";
      return(0.0);
   }
   if( !VariablesInPhaseSpace(9, x) ) return(0.0);
   //if( !VariablesInPhaseSpace(3, &x[17]) ) return(0.0);

   double M        = M_p/GeV;
   double xB       = x[10];
   double t        = x[11];
   double Q2       = x[12];
   double phi      = x[13];
   double P_p1     = x[14];
   double y        = x[20];
   double nu_rest  = Q2/(2.0*M*xB);
   double nu       = nu_rest;
   double E0       = nu_rest/y;//GetBeamEnergy();
   double eps      = epsilon(Q2,xB,M);
   double Delta2   = t;
   double xi       = xi_BKM(xB,Q2,Delta2);

   if(Q2< 0.8) {
      return 0.0;
   }
   //std::cout << " t  = " << t << std::endl;
   //std::cout << "xB  = " << xB << std::endl;
   //std::cout << "E0  = " << E0 << std::endl;
   //std::cout << "Q2  = " << Q2 << std::endl;
   //std::cout << "phi = " << phi << std::endl;
   //std::cout << " y  = " << y << std::endl;
   // Calculate BH cross section
   double t0 = Power(1.0/137.0,3.0)*xB/(16.0*pi*pi*Q2*Q2*Sqrt(1.0+eps*eps));

   double F1  = fFormFactors->F1p(Q2);
   double F2  = fFormFactors->F2p(Q2);

   DVCS_KinematicVariables dvcs_vars { xB, Q2, t, phi, xi, E0, M, y, nu, eps};
   DVCS_FormFactors        dvcs_ffs;
   dvcs_ffs.F1 = F1;
   dvcs_ffs.F2 = F2;
   dvcs_ffs.H      = 0.0;
   dvcs_ffs.Htilde = 0.0;

   dvcs_vars.K = K_DVCS(dvcs_vars);
   dvcs_vars.J = J_DVCS(dvcs_vars);
   double p1   = P1(dvcs_vars);
   double p2   = P2(dvcs_vars);

   double c0 = c0_BH_unp(dvcs_vars, dvcs_ffs);
   double c1 = c1_BH_unp(dvcs_vars, dvcs_ffs);
   double c2 = c2_BH_unp(dvcs_vars, dvcs_ffs);

   double den_BH = Power(xB*(1.0+eps*eps),2.0)*Delta2*p1*p2;
   double num_BH = c0 + c1*Cos(phi) + c2*Cos(2.0*phi);

   double sig_BH = t0*(num_BH/den_BH)*TMath::Power(hbarc2_gev_nb,1.5);

   double norm    = 1.0;//2.0*insane::units::twopi;
   double prob_p1 = P_p1*P_p1*fFermiDist.n(P_p1,fTargetNucleus.GetA());
   
   double Z      = fTargetNucleus.GetZ();
   double sig0   = Z*norm*prob_p1*sig_BH;
   if(sig0<0) {
      //std::cout << "sig_BH   " << sig_BH << std::endl;
      //std::cout << "sig0   " << sig0 << std::endl;
      return 0.0;
   }

   double A       = GetBeamSpinAsymmetry(x);
   double hel     = GetHelicity();

   double polarization = Abs(hel);

   double sig_plus  = sig0*(1.0 + hel*A)/2.0;
   double sig_minus = sig0*(1.0 - hel*A)/2.0;

   double sig_pol = sig0;
   if( hel > 0 ){
      sig_pol = polarization*sig_plus + (1.0-polarization)*sig0;
   } else if(hel<0){
      sig_pol = polarization*sig_plus + (1.0-polarization)*sig0;
   }

   //std::cout << "hel        " << hel << std::endl;
   //std::cout << "sig_plus   " << sig_plus << std::endl;
   //std::cout << "sig_minus  " << sig_minus << std::endl;
   //if(sig0>1.0){
   //   std::cout << "sig0   " << sig0 << std::endl;
   //   std::cout << "hel        " << hel << std::endl;
   //   std::cout << "sig_plus   " << sig_plus << std::endl;
   //   std::cout << "sig_minus  " << sig_minus << std::endl;
   //}
   //std::cout  << sig0 <<std::endl;
   if(std::isnan(sig0)){
      //std::cout << " t=" << t << ", Q2=" << Q2 << ", xB=" << xB << std::endl;
      //std::cout << "P1 " << prob_p1 << std::endl;
      //std::cout << "sig0   " << sig0 << std::endl;
      //std::cout << "hel        " << hel << std::endl;
      //std::cout << "sig_plus   " << sig_plus << std::endl;
      //std::cout << "sig_minus  " << sig_minus << std::endl;
      return 0.0;
   }

   double jac = 1.0;//Jacobian(x);//*Sin(x[1])*Sin(x[4])*Sin(x[7]);
   if( IncludeJacobian() ) {
      jac = Jacobian(x);
   }
   double res = jac*(sig_pol);
   if(res < 0) {
      return 0.0;
   }
   //if(jac < 1.0e-3){
   //std::cout << "jac = " << jac << std::endl;
   //}
   //std::cout << "P1 " << prob_p1 << std::endl;
   //std::cout << "jac = " << jac << std::endl;
   return res;
}
//______________________________________________________________________________

Double_t   IncoherentDVCSXSec::GetBeamSpinAsymmetry(const Double_t * x) const
{
   double xBjorken = x[10];
   double t        = x[11];
   double Q2       = x[12];
   double phi      = x[13];
   double P_p1     = x[14];
   double Asym     = 0.4*TMath::Sin(phi);
   //std::cout << " Asym = " << Asym << std::endl;
   return Asym;
}
//______________________________________________________________________________

double IncoherentDVCSXSec::Jacobian(const double * vars) const
{
   using namespace TMath;
   auto Csc = [](double x){ return 1.0/TMath::Sin(x) ; };
   auto ArcCos = [](double x){ return TMath::ACos(x) ; };
   double M = 0.938;
   double k1 = fBeamEnergy;

   double k2      = vars[0];
   double thetak2 = vars[1];
   double phik2   = vars[2];

   double q2      = vars[6];
   double thetaq2 = vars[7];
   double phiq2   = vars[8];

   double P1      = vars[14];
   double thetaP1 = vars[15];
   double phiP1   = vars[16];

   double res = 
(4*Power(k1,2)*(-((k2*Sqrt(Power(M,2) + Power(P1,2))*(Power(k1,2) +  
Power(k2,2) - 2*k1*k2*Cos(thetak2))*(-Power(k1,2) - Power(k2,2) +  
2*k1*k2*Cos(thetak2))*Sin(thetak2)*(-(k1*k2) + k1*Sqrt(Power(M,2) +  
Power(P1,2)) - k2*Sqrt(Power(M,2) + Power(P1,2)) - k1*P1*Cos(thetaP1)  
+ k2*Cos(thetak2)*(k1 + P1*Cos(thetaP1)) + k2*P1*Cos(phik2 -  
phiP1)*Sin(thetak2)*Sin(thetaP1))*((k1*k2*Sqrt(Power(M,2) +  
Power(P1,2))*(1.*Sqrt(Power(M,2) + Power(P1,2)) - 1.*P1*Cos(thetaP1)  
+ Cos(thetak2)*(-1.*Sqrt(Power(M,2) + Power(P1,2)) +  
1.*P1*Cos(thetaP1)))*Sin(thetak2)*Power(k1*Sqrt(Power(M,2) +  
Power(P1,2)) - 1.*k2*Sqrt(Power(M,2) + Power(P1,2)) -  
1.*k1*P1*Cos(thetaP1) + k2*P1*Cos(thetak2)*Cos(thetaP1) +  
k2*P1*Cos(phik2 - phiP1)*Sin(thetak2)*Sin(thetaP1),2))/Power((k1 -  
1.*k2)*Sqrt(Power(M,2) + Power(P1,2)) + (-1.*k1*P1 +  
k2*P1*Cos(thetak2))*Cos(thetaP1) + k2*P1*Cos(phik2 -  
phiP1)*Sin(thetak2)*Sin(thetaP1),2) - (1.*k2*Sqrt(Power(M,2) +  
Power(P1,2))*(1 - Cos(thetak2))*Power(k1*Sqrt(Power(M,2) +  
Power(P1,2)) - 1.*k2*Sqrt(Power(M,2) + Power(P1,2)) -  
1.*k1*P1*Cos(thetaP1) + k2*P1*Cos(thetak2)*Cos(thetaP1) +  
k2*P1*Cos(phik2 - phiP1)*Sin(thetak2)*Sin(thetaP1),2)*((1.*(1.*k1 -  
1.*k2)*Sqrt(Power(M,2) + Power(P1,2)) + (-1.*k1*P1 +  
1.*k2*P1)*Cos(thetaP1))*Sin(thetak2) + k2*P1*Cos(phik2 -  
phiP1)*Cos(thetak2)*(-1. + 1.*Cos(thetak2))*Sin(thetaP1) +  
1.*k2*P1*Cos(phik2 -  
phiP1)*Power(Sin(thetak2),2)*Sin(thetaP1)))/Power((k1 -  
1.*k2)*Sqrt(Power(M,2) + Power(P1,2)) + (-1.*k1*P1 +  
k2*P1*Cos(thetak2))*Cos(thetaP1) + k2*P1*Cos(phik2 -  
phiP1)*Sin(thetak2)*Sin(thetaP1),2))*Sin(thetaq2)*Power(k1 - k2 +  
Sqrt(Power(M,2) + Power(P1,2)) - k1*Cos(thetaq2) +  
k2*Cos(thetak2)*Cos(thetaq2) - P1*Cos(thetaP1)*Cos(thetaq2) +  
k2*Cos(phik2 - phiq2)*Sin(thetak2)*Sin(thetaq2) - P1*Cos(phiP1 -  
phiq2)*Sin(thetaP1)*Sin(thetaq2),2)*Sqrt((Power(k1,4) +  
2*Power(k1,2)*Power(k2,2) + Power(k2,4) -  
4*Power(k1,3)*k2*Cos(thetak2) - 4*k1*Power(k2,3)*Cos(thetak2) +  
4*Power(k1,2)*Power(k2,2)*Power(Cos(thetak2),2) -  
Power(k2,4)*Power(Cos(thetaq2),2)*Power(Sin(thetak2),4) -  
2*k1*Power(k2,3)*Cos(phik2 -  
phiq2)*Cos(thetaq2)*Power(Sin(thetak2),3)*Sin(thetaq2) +  
2*Power(k2,4)*Cos(phik2 -  
phiq2)*Cos(thetak2)*Cos(thetaq2)*Power(Sin(thetak2),3)*Sin(thetaq2) -  
Power(k1,2)*Power(k2,2)*Power(Cos(phik2 -  
phiq2),2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2) +  
2*k1*Power(k2,3)*Power(Cos(phik2 -  
phiq2),2)*Cos(thetak2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2) -  
Power(k2,4)*Power(Cos(phik2 -  
phiq2),2)*Power(Cos(thetak2),2)*Power(Sin(thetak2),2)*Power(Sin( 
thetaq2),2))/Power(Power(k1,2) + Power(k2,2) -  
2*k1*k2*Cos(thetak2),2))*((k1*Sqrt(Power(M,2) + Power(P1,2)) -  
k2*Sqrt(Power(M,2) + Power(P1,2))*Cos(thetak2) + (-k1 +  
k2)*P1*Cos(thetaP1))*Sin(thetaq2) + P1*Cos(phiP1 -  
phiq2)*Sin(thetaP1)*((k1 - k2)*Cos(thetaq2) + (-k1 +  
k2*Cos(thetak2))*Power(Cos(thetaq2),2) + (-k1 +  
k2*Cos(thetak2))*Power(Sin(thetaq2),2)) + k2*Cos(phik2 -  
phiq2)*Sin(thetak2)*(Sqrt(Power(M,2) + Power(P1,2))*Cos(thetaq2) -  
P1*Cos(thetaP1)*Power(Cos(thetaq2),2) -  
P1*Cos(thetaP1)*Power(Sin(thetaq2),2)))*(ArcCos((k2*Sin(thetak2)*(k2* 
Cos(thetaq2)*Sin(thetak2) + Cos(phik2 - phiq2)*(k1 -  
k2*Cos(thetak2))*Sin(thetaq2)))/(Power(k1,2) + Power(k2,2) -  
2*k1*k2*Cos(thetak2)))*Cos(phik2 - phiq2)*(Power(k1,2) + Power(k2,2)  
- 2*k1*k2*Cos(thetak2)) + (k2*(k1 - k2*Cos(thetak2))*Power(Sin(phik2  
- phiq2),2)*Sin(thetak2)*Sin(thetaq2))/Sqrt(1 -  
(Power(k2,2)*Power(Sin(thetak2),2)*Power(k2*Cos(thetaq2)*Sin(thetak2)  
+ Cos(phik2 - phiq2)*(k1 -  
k2*Cos(thetak2))*Sin(thetaq2),2))/Power(Power(k1,2) + Power(k2,2) -  
2*k1*k2*Cos(thetak2),2))))/Power(k1 - k2 + Sqrt(Power(M,2) +  
Power(P1,2)) - (k1 - k2*Cos(thetak2) + P1*Cos(thetaP1))*Cos(thetaq2)  
+ k2*Cos(phik2 - phiq2)*Sin(thetak2)*Sin(thetaq2) - P1*Cos(phiP1 -  
phiq2)*Sin(thetaP1)*Sin(thetaq2),2)) + (k2*Sqrt(Power(M,2) +  
Power(P1,2))*(Power(k1,2) + Power(k2,2) -  
2*k1*k2*Cos(thetak2))*(-Power(k1,2) - Power(k2,2) +  
2*k1*k2*Cos(thetak2))*Sin(phik2 - phiq2)*Sin(thetak2)*(-(k1*k2) +  
k1*Sqrt(Power(M,2) + Power(P1,2)) - k2*Sqrt(Power(M,2) + Power(P1,2))  
- k1*P1*Cos(thetaP1) + k2*Cos(thetak2)*(k1 + P1*Cos(thetaP1)) +  
k2*P1*Cos(phik2 -  
phiP1)*Sin(thetak2)*Sin(thetaP1))*((k1*k2*Sqrt(Power(M,2) +  
Power(P1,2))*(1.*Sqrt(Power(M,2) + Power(P1,2)) - 1.*P1*Cos(thetaP1)  
+ Cos(thetak2)*(-1.*Sqrt(Power(M,2) + Power(P1,2)) +  
1.*P1*Cos(thetaP1)))*Sin(thetak2)*Power(k1*Sqrt(Power(M,2) +  
Power(P1,2)) - 1.*k2*Sqrt(Power(M,2) + Power(P1,2)) -  
1.*k1*P1*Cos(thetaP1) + k2*P1*Cos(thetak2)*Cos(thetaP1) +  
k2*P1*Cos(phik2 - phiP1)*Sin(thetak2)*Sin(thetaP1),2))/Power((k1 -  
1.*k2)*Sqrt(Power(M,2) + Power(P1,2)) + (-1.*k1*P1 +  
k2*P1*Cos(thetak2))*Cos(thetaP1) + k2*P1*Cos(phik2 -  
phiP1)*Sin(thetak2)*Sin(thetaP1),2) - (1.*k2*Sqrt(Power(M,2) +  
Power(P1,2))*(1 - Cos(thetak2))*Power(k1*Sqrt(Power(M,2) +  
Power(P1,2)) - 1.*k2*Sqrt(Power(M,2) + Power(P1,2)) -  
1.*k1*P1*Cos(thetaP1) + k2*P1*Cos(thetak2)*Cos(thetaP1) +  
k2*P1*Cos(phik2 - phiP1)*Sin(thetak2)*Sin(thetaP1),2)*((1.*(1.*k1 -  
1.*k2)*Sqrt(Power(M,2) + Power(P1,2)) + (-1.*k1*P1 +  
1.*k2*P1)*Cos(thetaP1))*Sin(thetak2) + k2*P1*Cos(phik2 -  
phiP1)*Cos(thetak2)*(-1. + 1.*Cos(thetak2))*Sin(thetaP1) +  
1.*k2*P1*Cos(phik2 -  
phiP1)*Power(Sin(thetak2),2)*Sin(thetaP1)))/Power((k1 -  
1.*k2)*Sqrt(Power(M,2) + Power(P1,2)) + (-1.*k1*P1 +  
k2*P1*Cos(thetak2))*Cos(thetaP1) + k2*P1*Cos(phik2 -  
phiP1)*Sin(thetak2)*Sin(thetaP1),2))*Sin(thetaq2)*Power(k1 - k2 +  
Sqrt(Power(M,2) + Power(P1,2)) - k1*Cos(thetaq2) +  
k2*Cos(thetak2)*Cos(thetaq2) - P1*Cos(thetaP1)*Cos(thetaq2) +  
k2*Cos(phik2 - phiq2)*Sin(thetak2)*Sin(thetaq2) - P1*Cos(phiP1 -  
phiq2)*Sin(thetaP1)*Sin(thetaq2),2)*Sqrt((Power(k1,4) +  
2*Power(k1,2)*Power(k2,2) + Power(k2,4) -  
4*Power(k1,3)*k2*Cos(thetak2) - 4*k1*Power(k2,3)*Cos(thetak2) +  
4*Power(k1,2)*Power(k2,2)*Power(Cos(thetak2),2) -  
Power(k2,4)*Power(Cos(thetaq2),2)*Power(Sin(thetak2),4) -  
2*k1*Power(k2,3)*Cos(phik2 -  
phiq2)*Cos(thetaq2)*Power(Sin(thetak2),3)*Sin(thetaq2) +  
2*Power(k2,4)*Cos(phik2 -  
phiq2)*Cos(thetak2)*Cos(thetaq2)*Power(Sin(thetak2),3)*Sin(thetaq2) -  
Power(k1,2)*Power(k2,2)*Power(Cos(phik2 -  
phiq2),2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2) +  
2*k1*Power(k2,3)*Power(Cos(phik2 -  
phiq2),2)*Cos(thetak2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2) -  
Power(k2,4)*Power(Cos(phik2 -  
phiq2),2)*Power(Cos(thetak2),2)*Power(Sin(thetak2),2)*Power(Sin( 
thetaq2),2))/Power(Power(k1,2) + Power(k2,2) -  
2*k1*k2*Cos(thetak2),2))*(P1*Sin(phiP1 - phiq2)*Sin(thetaP1)*(k1 - k2  
+ (-k1 + k2*Cos(thetak2))*Cos(thetaq2) + k2*Cos(phik2 -  
phiq2)*Sin(thetak2)*Sin(thetaq2)) + k2*Sin(phik2 -  
phiq2)*Sin(thetak2)*(Sqrt(Power(M,2) + Power(P1,2)) -  
P1*Cos(thetaP1)*Cos(thetaq2) - P1*Cos(phiP1 -  
phiq2)*Sin(thetaP1)*Sin(thetaq2)))*(-(ArcCos((k2*Sin(thetak2)*(k2*Cos( 
thetaq2)*Sin(thetak2) + Cos(phik2 - phiq2)*(k1 -  
k2*Cos(thetak2))*Sin(thetaq2)))/(Power(k1,2) + Power(k2,2) -  
2*k1*k2*Cos(thetak2)))*(Power(k1,2) + Power(k2,2) -  
2*k1*k2*Cos(thetak2))*Cos(thetaq2)) +  
(k2*Sin(thetak2)*Sin(thetaq2)*(Cos(phik2 - phiq2)*(k1 -  
k2*Cos(thetak2))*Cos(thetaq2) - k2*Sin(thetak2)*Sin(thetaq2)))/Sqrt(1  
- (Power(k2,2)*Power(Sin(thetak2),2)*Power(k2*Cos(thetaq2)*Sin( 
thetak2) + Cos(phik2 - phiq2)*(k1 -  
k2*Cos(thetak2))*Sin(thetaq2),2))/Power(Power(k1,2) + Power(k2,2) -  
2*k1*k2*Cos(thetak2),2))))/Power(k1 - k2 + Sqrt(Power(M,2) +  
Power(P1,2)) - (k1 - k2*Cos(thetak2) + P1*Cos(thetaP1))*Cos(thetaq2)  
+ k2*Cos(phik2 - phiq2)*Sin(thetak2)*Sin(thetaq2) - P1*Cos(phiP1 -  
phiq2)*Sin(thetaP1)*Sin(thetaq2),2)))/((Power(M,2) +  
Power(P1,2))*Power(Power(k1,2) + Power(k2,2) -  
2*k1*k2*Cos(thetak2),2.5)*(-Power(k1,2) - Power(k2,2) +  
2*k1*k2*Cos(thetak2))*Power(k1*Sqrt(Power(M,2) + Power(P1,2)) -  
1.*k2*Sqrt(Power(M,2) + Power(P1,2)) - 1.*k1*P1*Cos(thetaP1) +  
k2*P1*Cos(thetak2)*Cos(thetaP1) + k2*P1*Cos(phik2 -  
phiP1)*Sin(thetak2)*Sin(thetaP1),2)*Power(k1 - k2 + Sqrt(Power(M,2) +  
Power(P1,2)) - k1*Cos(thetaq2) + k2*Cos(thetak2)*Cos(thetaq2) -  
P1*Cos(thetaP1)*Cos(thetaq2) + k2*Cos(phik2 -  
phiq2)*Sin(thetak2)*Sin(thetaq2) - P1*Cos(phiP1 -  
phiq2)*Sin(thetaP1)*Sin(thetaq2),2)*Sqrt((Power(k1,4) +  
2*Power(k1,2)*Power(k2,2) + Power(k2,4) -  
4*Power(k1,3)*k2*Cos(thetak2) - 4*k1*Power(k2,3)*Cos(thetak2) +  
4*Power(k1,2)*Power(k2,2)*Power(Cos(thetak2),2) -  
Power(k2,4)*Power(Cos(thetaq2),2)*Power(Sin(thetak2),4) -  
2*k1*Power(k2,3)*Cos(phik2 -  
phiq2)*Cos(thetaq2)*Power(Sin(thetak2),3)*Sin(thetaq2) +  
2*Power(k2,4)*Cos(phik2 -  
phiq2)*Cos(thetak2)*Cos(thetaq2)*Power(Sin(thetak2),3)*Sin(thetaq2) -  
Power(k1,2)*Power(k2,2)*Power(Cos(phik2 -  
phiq2),2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2) +  
2*k1*Power(k2,3)*Power(Cos(phik2 -  
phiq2),2)*Cos(thetak2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2) -  
Power(k2,4)*Power(Cos(phik2 -  
phiq2),2)*Power(Cos(thetak2),2)*Power(Sin(thetak2),2)*Power(Sin( 
thetaq2),2))/Power(Power(k1,2) + Power(k2,2) -  
2*k1*k2*Cos(thetak2),2)));
   
  return res;
}




