#include "insane/xsections/InclusivePhotoProductionXSec.h"
#include "insane/base/BremsstrahlungRadiator.h"

namespace insane {
namespace physics {

InclusivePhotoProductionXSec::InclusivePhotoProductionXSec()
{ 
   fID         = 100002001;
   fBaseID     = fID;
   fPIDs.clear();
   fPIDs.push_back(111);   // pi0

   fSig_0     = new PhotonDiffXSec();
   fSig_plus  = new PhotonDiffXSec();
   fSig_minus = new PhotonDiffXSec();

   fSig_0->SetProductionParticleType(111);
   fSig_plus->SetProductionParticleType(211);
   fSig_minus->SetProductionParticleType(-211);
}
//______________________________________________________________________________

InclusivePhotoProductionXSec::~InclusivePhotoProductionXSec()
{ 
   delete fSig_0     ;
   delete fSig_plus  ;
   delete fSig_minus ;
}
//______________________________________________________________________________

InclusivePhotoProductionXSec::InclusivePhotoProductionXSec(const InclusivePhotoProductionXSec& rhs) : 
   PhotonDiffXSec(rhs)
{
   (*this) = rhs;
}
//______________________________________________________________________________

InclusivePhotoProductionXSec& InclusivePhotoProductionXSec::operator=(const InclusivePhotoProductionXSec& rhs) 
{
   if (this != &rhs) {  // make sure not same object
      PhotonDiffXSec::operator=(rhs);
      fSig_0     = rhs.fSig_0->Clone();
      fSig_plus  = rhs.fSig_plus->Clone();
      fSig_minus = rhs.fSig_minus->Clone();
   }
   return *this;    // Return ref for multiple assignment
}
//______________________________________________________________________________

InclusivePhotoProductionXSec*  InclusivePhotoProductionXSec::Clone(const char * newname) const 
{
   std::cout << "InclusivePhotoProductionXSec::Clone()\n";
   auto * copy = new InclusivePhotoProductionXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

InclusivePhotoProductionXSec*  InclusivePhotoProductionXSec::Clone() const
{ 
   return( Clone("") );
} 
//______________________________________________________________________________

void  InclusivePhotoProductionXSec::SetParameters( int i, const std::vector<double>& pars )
{
   if(i == 0 ) {      fParameters0 = pars; fSig_0->fParameters0 = pars;     fSig_0->SetParameters(pars); fSig_0->SetProductionParticleType(fSig_0->GetParticleType());}  
   else if(i == 1 ) { fParameters1 = pars; fSig_plus->fParameters1 = pars;  fSig_plus->SetParameters(pars); fSig_plus->SetProductionParticleType(fSig_plus->GetParticleType());}  
   else if(i == 2 ) { fParameters2 = pars; fSig_minus->fParameters2 = pars; fSig_minus->SetParameters(pars); fSig_minus->SetProductionParticleType(fSig_minus->GetParticleType());}  
   else  { 
      fParameters0 = pars; fSig_0->fParameters0 = pars;     fSig_0->SetParameters(pars); fSig_0->SetProductionParticleType(fSig_0->GetParticleType());
      fParameters1 = pars; fSig_plus->fParameters1 = pars;  fSig_plus->SetParameters(pars); fSig_plus->SetProductionParticleType(fSig_plus->GetParticleType());
      fParameters2 = pars; fSig_minus->fParameters2 = pars; fSig_minus->SetParameters(pars); fSig_minus->SetProductionParticleType(fSig_minus->GetParticleType()); 
   }  
   // this is needed so that fParameters is set to the correct vector above.
   SetProductionParticleType(GetParticleType());
   //std::cout << "p0 = " << pars[0] << std::endl;
}
//______________________________________________________________________________
const std::vector<double>& InclusivePhotoProductionXSec::GetParameters() const {
   return fParameters;
}
//______________________________________________________________________________
void  InclusivePhotoProductionXSec::SetPhaseSpace(PhaseSpace * ps){
   fSig_0->SetPhaseSpace(ps);
   fSig_plus->SetPhaseSpace(ps);
   fSig_minus->SetPhaseSpace(ps);
   InclusiveDiffXSec::SetPhaseSpace(ps);
}
//______________________________________________________________________________
void InclusivePhotoProductionXSec::InitializeFinalStateParticles(){
   if(fSig_0)  fSig_0->InitializeFinalStateParticles();
   if(fSig_plus) fSig_plus->InitializeFinalStateParticles();
   if(fSig_minus) fSig_minus->InitializeFinalStateParticles();
   InclusiveDiffXSec::InitializeFinalStateParticles();
}
//_____________________________________________________________________________
void InclusivePhotoProductionXSec::InitializePhaseSpaceVariables() {
   fSig_0->InitializePhaseSpaceVariables()    ;
   PhaseSpace * ps = fSig_0->GetPhaseSpace();
   fSig_plus->SetPhaseSpace(ps);
   fSig_minus->SetPhaseSpace(ps);
   InclusiveDiffXSec::SetPhaseSpace(ps);
}
//______________________________________________________________________________
Double_t InclusivePhotoProductionXSec::EvaluateXSec(const Double_t * x) const {

   // Evaluates the inclusive particle production cross section on a proton or neutron.
   // If produced particle is charged meson, iso-spin symmetry is used to get the neutron
   // target cross section.

   double RES            = 0.0;
   int    PART           = fParticle->PdgCode();
   if( GetTargetNucleus() == Nucleus::Neutron() ) {
      // if target is a neutron use isospin to get the result
      if(fParticle->PdgCode() != 111) {
         // make sure it is not set to pi0
         PART = -1*fParticle->PdgCode();
      }
   } else if( !(GetTargetNucleus() == Nucleus::Proton()) ) {
      std::cout << " NOT Proton or neutron" << std::endl;
   }

   TargetMaterial           mat   = GetTargetMaterial();
   BremsstrahlungRadiator * brem  = mat.GetBremRadiator();
   Int_t                          matID = mat.GetMatID();

   //if(brem) std::cout << "bream: " << brem << std::endl;

   double EBEAM          = GetBeamEnergy();
   double Epart          = x[0];
   double Ppart          = TMath::Sqrt(Epart*Epart - M_pion*M_pion/(GeV*GeV));
   double THETA          = x[1];
   double radlen         = fRadiationLength;

   // TODO : this should be included in the "luminosity"...
   // If the bremsstrahlung spectrum is given (i.e. photon flux per electron)
   // then we calculate the equivalent   
   double U  = EBEAM;
   // If no brem spectrum is given calculate the appropriate equivalent quanta
   // for the radition length. 
   if(!brem) U = insane::Kine::I_gamma_1_k_avg(fRadiationLength,0.01,EBEAM);
   double EQ = U/EBEAM;

   // Minimum photon energy for photoproduction in gamma+p -> x+n
   // where x is a hadron and n is a nucleon
   // Ex and thetax are the produced hadron's energy and angle
   // mx is the hadron mass, mt is the target mass, mn is the recoil nucleon mass.
   double k_min = insane::Kine::k_min_photoproduction(Epart, THETA);//, M_pion/GeV,fTargetNucleus.GetMass() );//, mx, double mt, double mn) 

   if(k_min<0.0) k_min=0.0;

   // simple integration
   int    Nint    = 10;
   double delta_w = (EBEAM-k_min)/double(Nint);
   double w       = 0.0;
   double Igam    = 0.0;
   double tot     = 0.0;

   if( PART==111 ){
      // pi0 = (pi+ + pi-)/2.0
      for(int i = 0;i<Nint; i++)
      {

         w      = k_min + (double(i)+0.5)*delta_w;
         //Igam   = insane::Kine::I_gamma_1_approx(fRadiationLength,EBEAM,w);
         if(brem) Igam = brem->I_gamma( matID, w, EBEAM );
         else     Igam = insane::Kine::I_gamma_1_approx(fRadiationLength,EBEAM,w);

         // pi+
         //fSig_plus->SetProductionParticleType(211);
         fSig_plus->SetBeamEnergy(w);
         RES = fSig_plus->EvaluateXSec(x);
         if(RES<0.0) RES = 0.0;
         RES  *= (delta_w*Igam);
         tot  += RES;

         // pi-
         //fSig_minus->SetProductionParticleType(-211);
         fSig_minus->SetBeamEnergy(w);
         RES = fSig_minus->EvaluateXSec(x);
         if(RES<0.0) RES = 0.0;
         RES  *= (delta_w*Igam);
         tot  += RES;
      }
      tot = tot/2.0;

   } else if( PART==211 ){

      for(int i = 0;i<Nint; i++) 
      {
         w = k_min + (double(i)+0.5)*delta_w;

         //fSig_plus->SetProductionParticleType(211);
         fSig_plus->SetBeamEnergy(w);
         RES = fSig_plus->EvaluateXSec(x);
         if(RES<0.0) RES = 0.0;

         //Igam   = insane::Kine::I_gamma_1_approx(fRadiationLength,EBEAM,w);
         if(brem) Igam = brem->I_gamma( matID, w, EBEAM );
         else     Igam = insane::Kine::I_gamma_1_approx(fRadiationLength,EBEAM,w);

         RES  *= (delta_w*Igam);
         tot  += RES;
      }

   } else if( PART==-211 ){

      for(int i = 0;i<Nint; i++) 
      {
         w = k_min + (double(i)+0.5)*delta_w;

         //fSig_minus->SetProductionParticleType(211);
         fSig_minus->SetBeamEnergy(w);
         RES = fSig_minus->EvaluateXSec(x);

         if(RES<0.0) RES = 0.0;
         //Igam   = insane::Kine::I_gamma_1_approx(fRadiationLength,EBEAM,w);
         if(brem) Igam = brem->I_gamma( matID, w, EBEAM );
         else     Igam = insane::Kine::I_gamma_1_approx(fRadiationLength,EBEAM,w);

         RES  *= (delta_w*Igam);
         tot  += RES;
      }
   }


   tot      *= 1000.0; // converts ub to nb
   tot      /= EQ; // cross section per equivalent quant 

   //std::cout << " tot = " << tot << "\n";
   if( tot < 0.0  ) return 0.0;
   if( TMath::IsNaN(tot) ) return 0.0;
   if( IncludeJacobian() ) return( tot*TMath::Sin(x[1]) ); 

   return(tot);
}
//______________________________________________________________________________


//InclusivePhotoProductionXSec::InclusivePhotoProductionXSec()
//{
//   fID         = 100102001;
//   fBaseID     = 100102001;
//   fTitle = "InclusivePhotoProductionXSec";
//   fPlotTitle = "#frac{d#sigma}{dE_{#pi} d#Omega_{#pi}} nb/GeV-Sr";
//   fPIDs.clear();
//   fPIDs.push_back(111);//
//   fProtonXSec  = new InclusivePhotoProductionXSec();
//   fProtonXSec->SetTargetNucleus(Nucleus::Proton());
//   fNeutronXSec = new InclusivePhotoProductionXSec();
//   fNeutronXSec->SetTargetNucleus(Nucleus::Neutron());
//   fProtonXSec->UsePhaseSpace(false);
//   fNeutronXSec->UsePhaseSpace(false);
//   //std::cout << "done" << std::endl;
//}
////_____________________________________________________________________________
//InclusivePhotoProductionXSec::~InclusivePhotoProductionXSec()
//{
//}
////______________________________________________________________________________
//
//InclusivePhotoProductionXSec::InclusivePhotoProductionXSec(const InclusivePhotoProductionXSec& rhs) : 
//   CompositeDiffXSec(rhs)
//{
//   (*this) = rhs;
//}
////______________________________________________________________________________
//
//InclusivePhotoProductionXSec& InclusivePhotoProductionXSec::operator=(const InclusivePhotoProductionXSec& rhs) 
//{
//   if (this != &rhs) {  // make sure not same object
//      CompositeDiffXSec::operator=(rhs);
//   }
//   return *this;    // Return ref for multiple assignment
//}
////______________________________________________________________________________
//
//InclusivePhotoProductionXSec*  InclusivePhotoProductionXSec::Clone(const char * newname) const 
//{
//   std::cout << "InclusivePhotoProductionXSec::Clone()\n";
//   auto * copy = new InclusivePhotoProductionXSec();
//   (*copy) = (*this);
//   return copy;
//}
////______________________________________________________________________________
//
//InclusivePhotoProductionXSec*  InclusivePhotoProductionXSec::Clone() const
//{ 
//   return( Clone("") );
//} 
////______________________________________________________________________________
//
//Double_t  InclusivePhotoProductionXSec::EvaluateXSec(const Double_t * x) const
//{
//   //std::cout << " composite eval " << std::endl;
//   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);
//
//   if( !fProtonXSec )  return 0.0;
//   if( !fNeutronXSec ) return 0.0;
//
//   double th     = x[1];
//
//   Double_t z    = GetZ();
//   //if( z>0.0 ) z = 1.0;
//   Double_t n    = GetN();
//   //if( n>0.0 ) n = 1.0;
//
//   Double_t sigP = fProtonXSec->EvaluateXSec(x);
//   Double_t sigN = fNeutronXSec->EvaluateXSec(x);
//   Double_t xsec = z*sigP + n*sigN;
//
//   // Take into account nuclear transparency sigma_A = A^alpha sigma_p
//   Double_t a = z + n;
//   //if( a > 2.0 ) xsec = (xsec/a)*TMath::Power(a,0.7);
//
//   //std::cout << " composite eval " <<  xsec << std::endl;
//   if( xsec < 0.0  ) return 0.0;
//   if( TMath::IsNaN(xsec) ) return 0.0;
//   if ( IncludeJacobian() ) return xsec*TMath::Sin(th);
//   return xsec;
//
//}
//______________________________________________________________________________
}}
