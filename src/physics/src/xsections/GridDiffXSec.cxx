#include "GridDiffXSec.h"

#include <fstream>
namespace insane {
namespace physics {

//________________________________________________________________________________

GridDiffXSec::GridDiffXSec(const char * file)
{
   fFileName = file; 
   fKineKey = new DiffXSecKinematicKey();
   std::ifstream infile(fFileName.Data());
   Int_t N = 0;
   if(infile.fail()){
      std::cout << "Cannot open the file: " << fFileName.Data() << std::endl;
   }else{
      std::cout << "Opening the file: " << fFileName.Data() << std::endl;
      while(!infile.eof()){
         auto * akey = new DiffXSecKinematicKey(N);
         Double_t Eprime,theta,phi;
         Double_t sig = 0.0;

         infile >> Eprime >> theta >> phi >> sig;

         akey->fEbeam = 5.9;
         akey->fEprime = Eprime;
         akey->fTheta = theta;
         akey->fPhi = phi;
         akey->fSigma = sig;
         //GridXSecValue * asig = new GridXSecValue(sig);
         fGridData.Add(akey);
         N++;
      }
   }
   fGridData.Sort();
   //fGridData.Print();

   auto * akey = (DiffXSecKinematicKey*)fGridData.At(0);
   fEprime.push_back(akey->fEprime); 
   fTheta.push_back(akey->fTheta); 
   fPhi.push_back(akey->fPhi); 
   for(int i = 0;i<fGridData.GetEntries();i++){
      akey = (DiffXSecKinematicKey*)fGridData.At(i);
      if(fEprime.back() != akey->fEprime) fEprime.push_back(akey->fEprime);
      if(fTheta.back() != akey->fTheta) fTheta.push_back(akey->fTheta);
      if(fPhi.back() != akey->fPhi) fPhi.push_back(akey->fPhi);
   }

   std::sort(fEprime.begin(),fEprime.end());
   fEprime.erase(std::unique(fEprime.begin(),fEprime.end()),fEprime.end());
   std::sort(fTheta.begin(),fTheta.end());
   fTheta.erase(std::unique(fTheta.begin(),fTheta.end()),fTheta.end());
   std::sort(fPhi.begin(),fPhi.end());
   fPhi.erase(std::unique(fPhi.begin(),fPhi.end()),fPhi.end());

   std::cout << fEprime.size() << " x " << fTheta.size() << " x " << fPhi.size() << std::endl;
}
//________________________________________________________________________________
GridDiffXSec::~GridDiffXSec(){
}
//________________________________________________________________________________
Double_t GridDiffXSec::EvaluateXSec(const Double_t *x) const {
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   Double_t sig    = 0.0;
   fKineKey->fEprime   = x[0];               // scattered electron energy in GeV 
   fKineKey->fTheta    = x[1];               // electron scattering angle in radians 
   fKineKey->fPhi      = x[2];
   fKineKey->fEbeam    = GetBeamEnergy();

   //Int_t i0_lo = 0;
   //Int_t i0_hi = 0;

   Int_t i1_lo = 0;
   Int_t i1_hi = 0;
   BinarySearch(&fEprime,fKineKey->fEprime,i1_lo,i1_hi);

   Int_t i2_lo = 0;
   Int_t i2_hi = 0;
   BinarySearch(&fTheta,fKineKey->fTheta,i2_lo,i2_hi);

   Int_t i3_lo = 0;
   Int_t i3_hi = 0;
   BinarySearch(&fPhi,fKineKey->fPhi,i3_lo,i3_hi);

   // Following wikipedia entry "Trilinear Interpolation" 
   Double_t xd = (x[0] - fEprime.at( i1_lo))/(fEprime.at( i1_hi) - fEprime.at(i1_lo));
   Double_t yd = (x[1] - fTheta.at( i2_lo))/(fTheta.at( i2_hi) - fTheta.at(i2_lo));
   Double_t zd = (x[2] - fPhi.at( i3_lo))/(fPhi.at( i3_hi) - fPhi.at(i3_lo));

   // c00
   fKineKey->fEprime = fEprime.at( i1_lo);
   fKineKey->fTheta  = fTheta.at(  i2_lo);
   fKineKey->fPhi    = fPhi.at(    i3_lo);
   Int_t isearch =  fGridData.BinarySearch(fKineKey); 
   if(isearch >=0) sig = ((DiffXSecKinematicKey*)fGridData.At(isearch))->fSigma;
   Double_t c00 = sig*(1.0-xd);
   fKineKey->fEprime = fEprime.at( i1_hi);
   fKineKey->fTheta  = fTheta.at(  i2_lo);
   fKineKey->fPhi    = fPhi.at(    i3_lo);
   isearch =  fGridData.BinarySearch(fKineKey); 
   if(isearch >=0) sig = ((DiffXSecKinematicKey*)fGridData.At(isearch))->fSigma;
   c00 += sig*xd;
   // c10
   fKineKey->fEprime = fEprime.at( i1_lo);
   fKineKey->fTheta  = fTheta.at(  i2_hi);
   fKineKey->fPhi    = fPhi.at(    i3_lo);
   isearch =  fGridData.BinarySearch(fKineKey); 
   if(isearch >=0) sig = ((DiffXSecKinematicKey*)fGridData.At(isearch))->fSigma;
   Double_t c10 = sig*(1.0-xd);
   fKineKey->fEprime = fEprime.at( i1_hi);
   fKineKey->fTheta  = fTheta.at(  i2_hi);
   fKineKey->fPhi    = fPhi.at(    i3_lo);
   isearch =  fGridData.BinarySearch(fKineKey); 
   if(isearch >=0) sig = ((DiffXSecKinematicKey*)fGridData.At(isearch))->fSigma;
   c10 += sig*xd;
   // c01
   fKineKey->fEprime = fEprime.at( i1_lo);
   fKineKey->fTheta  = fTheta.at(  i2_lo);
   fKineKey->fPhi    = fPhi.at(    i3_hi);
   isearch =  fGridData.BinarySearch(fKineKey); 
   if(isearch >=0) sig = ((DiffXSecKinematicKey*)fGridData.At(isearch))->fSigma;
   Double_t c01 = sig*(1.0-xd);
   fKineKey->fEprime = fEprime.at( i1_hi);
   fKineKey->fTheta  = fTheta.at(  i2_lo);
   fKineKey->fPhi    = fPhi.at(    i3_hi);
   isearch =  fGridData.BinarySearch(fKineKey); 
   if(isearch >=0) sig = ((DiffXSecKinematicKey*)fGridData.At(isearch))->fSigma;
   c01 += sig*xd;
   // c11
   fKineKey->fEprime = fEprime.at( i1_lo);
   fKineKey->fTheta  = fTheta.at(  i2_hi);
   fKineKey->fPhi    = fPhi.at(    i3_hi);
   isearch =  fGridData.BinarySearch(fKineKey); 
   if(isearch >=0) sig = ((DiffXSecKinematicKey*)fGridData.At(isearch))->fSigma;
   Double_t c11 = sig*(1.0-xd);
   fKineKey->fEprime = fEprime.at( i1_hi);
   fKineKey->fTheta  = fTheta.at(  i2_hi);
   fKineKey->fPhi    = fPhi.at(    i3_hi);
   isearch =  fGridData.BinarySearch(fKineKey); 
   if(isearch >=0) sig = ((DiffXSecKinematicKey*)fGridData.At(isearch))->fSigma;
   c11 += sig*xd;

   Double_t c0 = c00*(1.0-yd)+c10*yd;
   Double_t c1 = c01*(1.0-yd)+c11*yd;
   Double_t c = c0*(1.0-zd)+c1*zd; 
   return(c);
}
//________________________________________________________________________________

void GridDiffXSec::BinarySearch(std::vector<Double_t> *array,Double_t key,Int_t &lowerbound,Int_t &upperbound) const {
   Int_t comparisonCount = 1;    //count the number of comparisons (optional)
   Int_t n               = array->size();
   lowerbound            = 0;
   upperbound            = n-1;

   // To start, find the subscript of the middle position.
   Int_t position = ( lowerbound + upperbound) / 2;

   while((array->at(position) != key) && (lowerbound <= upperbound)){
      comparisonCount++;
      if (array->at(position) > key){
         // decrease position by one.
         upperbound = position - 1;
      }else{
         // Else, increase position by one.
         lowerbound = position + 1;
      }
      position = (lowerbound + upperbound) / 2;
   }

   Double_t lo=0,hi=0,mid;
   Int_t dump = lowerbound;

   if (lowerbound <= upperbound){
      // std::cout << "[BinarySearch]: The number was found in array subscript " << position << std::endl; 
      // std::cout << "                The binary search found the number after " << comparisonCount << " comparisons." << std::endl;             
      // lo  = array[lowerbound];
      // hi  = array[upperbound];
      // mid = array[position]  
      // if(lo==hi){
      lowerbound = position; 
      upperbound = position;  
      // }
   }else{
      lowerbound = upperbound;
      upperbound = dump;
      // to safeguard against values that are outside the boundaries of the grid 
      if(upperbound>=n){
         upperbound = n-1;
         lowerbound = n-2;
      }
      if(upperbound==0){
         lowerbound = 0;
         upperbound = 1;
      }
      lo   = array->at(lowerbound);
      hi   = array->at(upperbound);
      // std::cout << "[BinarySearch]: Sorry, the number is not in this array.  The binary search made " << comparisonCount << " comparisons." << std::endl;
      // std::cout << "                Target = "         << key << std::endl;
      // std::cout << "                Bounding values: " << std::endl;
      // std::cout << "                low  = "           << lo << std::endl;
      // std::cout << "                high = "           << hi << std::endl;
   }

}
//________________________________________________________________________________
}}
