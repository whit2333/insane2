#include "insane/xsections/WiserXSection.h"


namespace insane {
namespace physics {
//______________________________________________________________________________
InclusiveWiserXSec::InclusiveWiserXSec()
{
   fID         = 100001002;
   fTitle = "Inclusive Pi0";
   fPlotTitle = "#frac{d#sigma}{dE_{#pi} d#Omega_{#pi}} mb/GeV-Sr";
   fPIDs.clear();
   fPIDs.push_back(111);//+
   fWiserParticleCode = 1;
   fRadiationLength = 0.01;
   /*      fParticleName = "PI0"; // argument for wiser fortran code*/
   /*      fParticle = TDatabasePDG::Instance()->GetParticle(111);//pi0 */
   // pdg code for pi0 see http://www.slac.stanford.edu/BFROOT/www/Computing/Environment/NewUser/htmlbug/node51.html
   SetProductionParticleType(111);
}
//_____________________________________________________________________________
void InclusiveWiserXSec::InitializePhaseSpaceVariables() {

   //std::cout << " o InclusiveWiserXSec::InitializePhaseSpaceVariables() \n";

   PhaseSpace * ps = GetPhaseSpace();

      auto * varEnergy2 = new PhaseSpaceVariable();
      varEnergy2 = new PhaseSpaceVariable();
      varEnergy2->SetNameTitle("energy_pi", "E_{#pi}");
      varEnergy2->SetMinimum(0.5); //GeV
      varEnergy2->SetMaximum(5.0); //GeV
      /*      varEnergy2->SetParticleIndex(0);*/
      //fMomentum_pi = varEnergy2->GetCurrentValueAddress();
      //fMomentum = varEnergy2->GetCurrentValueAddress();
      ps->AddVariable(varEnergy2);

      auto *   varTheta2 = new PhaseSpaceVariable();
      varTheta2->SetNameTitle("theta_pi", "#theta_{#pi}"); // ROOT string latex
      varTheta2->SetMinimum(25.0 * TMath::Pi() / 180.0); //
      varTheta2->SetMaximum(60.0 * TMath::Pi() / 180.0); //
      /*      varTheta2->SetParticleIndex(1);*/
      //fTheta_pi = varTheta2->GetCurrentValueAddress();
      //fTheta = varTheta2->GetCurrentValueAddress();
      ps->AddVariable(varTheta2);

      auto *   varPhi2 = new PhaseSpaceVariable();
      varPhi2->SetNameTitle("phi_pi", "#phi_{#pi}"); // ROOT string latex
      varPhi2->SetMinimum(-70.0 * TMath::Pi() / 180.0); //
      varPhi2->SetMaximum( 70.0 * TMath::Pi() / 180.0); //
      /*varPhi2->SetParticleIndex(1);*/
      //fPhi_pi = varPhi2->GetCurrentValueAddress();
      //fPhi = varPhi2->GetCurrentValueAddress();
      ps->AddVariable(varPhi2);

      SetPhaseSpace(ps);
}
//____________________________________________________________________

void InclusiveWiserXSec::PrintPossibleParticles()
{
   std::cout << " Particle = PDGcode \n";
   std::cout << "      pi0 = 111   \n";
   std::cout << "      pi+ = 211   \n";
   std::cout << "      pi- = -211  \n";
   std::cout << "       K+ = 321   \n";
   std::cout << "       K- = -321  \n";
   std::cout << "        p = 2212  \n";
   std::cout << "    p-bar = -2212 \n";
}
//____________________________________________________________________
Int_t InclusiveWiserXSec::GetWiserType(Int_t PDGcode) const {
   if      (PDGcode == 111) return   0;
   else if (PDGcode == 211) return  1;
   else if (PDGcode == -211) return  2;
   else if (PDGcode == 321) return  3;
   else if (PDGcode == -321) return  4;
   else if (PDGcode == 2212) return  5;
   else if (PDGcode == -2212) return  6;
   else {
      std::cout << " Bad particle code, " << PDGcode
                << ",  for Wiser inclusive cross section.\n";
   }
   return 1;
}
//____________________________________________________________________
void InclusiveWiserXSec::SetProductionParticleType(Int_t PDGcode, Int_t )
{
   SetParticleType(PDGcode);
   fParticle = TDatabasePDG::Instance()->GetParticle(PDGcode);
   fTitle = Form("Inclusive %s production ", fParticle->GetName());
   fPIDs.clear();
   fPIDs.push_back(PDGcode);
   GetWiserParticleType(PDGcode);
   fParticle->Print();
}
//___________________________________________________________________


Int_t InclusiveWiserXSec::GetWiserParticleType(Int_t PDGcode)
{
   if (fParticle->PdgCode() == 111) fWiserParticleCode = 0;
   else if (fParticle->PdgCode() == 211) fWiserParticleCode = 1;
   else if (fParticle->PdgCode() == -211) fWiserParticleCode = 2;
   else if (fParticle->PdgCode() == 321) fWiserParticleCode = 3;
   else if (fParticle->PdgCode() == -321) fWiserParticleCode = 4;
   else if (fParticle->PdgCode() == 2212) fWiserParticleCode = 5;
   else if (fParticle->PdgCode() == -2212) fWiserParticleCode = 6;
   else {
      std::cout << " Bad particle code, " << PDGcode
                << ",  for Wiser inclusive cross section.\n";
      std::cout << " Possible choices are:\n";
      PrintPossibleParticles();
      fWiserParticleCode = 0;
   }
   return int(fWiserParticleCode);
}
//______________________________________________________________________________
Double_t InclusiveWiserXSec::EvaluateXSec(const Double_t * x) const {
   //if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);
   double RES            = 0.0;
   auto    PART           = (int)fWiserParticleCode;
   if( GetZ()==0 ) {
      // if target is a neutron use isospin to get the result
      PART = GetWiserType(-1*fParticle->PdgCode());
   }
                              // since the wiser fit is just for proton targets
   double EBEAM          = GetBeamEnergy() * 1000.0; //(converting to MeV )
   double Epart          = x[0];
   double Ppart          = TMath::Sqrt(Epart*Epart - M_pion*M_pion/(GeV*GeV));
   double PscatteredPart = Ppart*1000.0; // 
   double THETA          = x[1]*180.0/TMath::Pi(); //(UNITS??)
   double radlen         = fRadiationLength*100.0;
   //wiser_all_sig_(&RES,&PART,&EBEAM,&PscatteredPart,&THETA);
   wiser_all_sig0_(&EBEAM, &PscatteredPart, &THETA, &radlen, &PART, &RES);
   if(RES<0.0) RES = 0.0;
   RES *= (Epart/Ppart);
   //std::cout << " wiser result is " << RES << " nb/GeV*str "
   //<< " for p,theta " << PscatteredPart << "," << THETA << "\n";
   if ( IncludeJacobian() ) return(RES * TMath::Sin(x[1]) ); 
   return(RES); // converts nb to mb
}
//______________________________________________________________________________



//_____________________________________________________________________________
//void PhotoWiserDiffXSec::InitializePhaseSpaceVariables() {
//
//   //std::cout << " o PhotoWiserDiffXSec::InitializePhaseSpaceVariables() \n";
//
//   PhaseSpace * ps = GetPhaseSpace();
//   if (ps)delete ps;
//   ps = 0;
//   if (!ps) {
//      ps = new PhaseSpace();
//      /// Production particle variables
//      PhaseSpaceVariable * varEnergy2 = new PhaseSpaceVariable();
//      varEnergy2 = new PhaseSpaceVariable();
//      varEnergy2->SetNameTitle("energy_pi", "E_{#pi}");
//      varEnergy2->SetMinimum(0.1); //GeV
//      varEnergy2->SetMaximum(4.9); //GeV
//      /*      varEnergy2->SetParticleIndex(0);*/
//      //fMomentum_pi = varEnergy2->GetCurrentValueAddress();
//      //fMomentum = varEnergy2->GetCurrentValueAddress();
//      ps->AddVariable(varEnergy2);
//
//      PhaseSpaceVariable *   varTheta2 = new PhaseSpaceVariable();
//      varTheta2->SetNameTitle("theta_pi", "#theta_{#pi}"); // ROOT string latex
//      varTheta2->SetMinimum(0.1 * TMath::Pi() / 180.0); //
//      varTheta2->SetMaximum(180.0 * TMath::Pi() / 180.0); //
//      /*      varTheta2->SetParticleIndex(1);*/
//      //fTheta_pi = varTheta2->GetCurrentValueAddress();
//      //fTheta = varTheta2->GetCurrentValueAddress();
//      ps->AddVariable(varTheta2);
//
//      PhaseSpaceVariable *   varPhi2 = new PhaseSpaceVariable();
//      varPhi2->SetNameTitle("phi_pi", "#phi_{#pi}"); // ROOT string latex
//      varPhi2->SetMinimum(-180.0 * TMath::Pi() / 180.0); //
//      varPhi2->SetMaximum(180.0 * TMath::Pi() / 180.0); //
//      /*      varPhi2->SetParticleIndex(1);*/
//      //fPhi_pi = varPhi2->GetCurrentValueAddress();
//      //fPhi = varPhi2->GetCurrentValueAddress();
//      ps->AddVariable(varPhi2);
//
//      SetPhaseSpace(ps);
//
//   } else {
//      std::cout << " Using existing phase space variables!\n";
//   }
//}

}}
