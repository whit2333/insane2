#include "insane/xsections/F1F209eInclusiveDiffXSec.h"


namespace insane {
namespace physics {
//______________________________________________________________________________
F1F209eInclusiveDiffXSec::F1F209eInclusiveDiffXSec()
{
   fID         = 100000012;
   //std::cout << "F1F209eInclusiveDiffXSec::F1F209eInclusiveDiffXSec" <<std::endl;
   SetTitle("F1F209eInclusiveDiffXSec");
   SetPlotTitle("F1F209 born DIS cross-section");
   fLabel      = "#frac{d#sigma}{dEd#Omega}";
   fUnits      = "nb/GeV/sr";
   fType       =  'n'; 
   fIsModifiedVersion = false; 
   inif1f209_();
}
//______________________________________________________________________________
F1F209eInclusiveDiffXSec::~F1F209eInclusiveDiffXSec(){

}
//______________________________________________________________________________
Double_t F1F209eInclusiveDiffXSec::EvaluateXSec(const Double_t *x) const {
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   auto Es       = (double)GetBeamEnergy(); 
   auto Ep       = (double)x[0];
   auto th       = (double)x[1];
   auto A        = (double)GetA();
   auto Z        = (double)GetZ();
   // double phi      = x[2];
   double Q2       = 0;
   double fullXsec = 0;
   double F1       = 0;
   double F2       = 0;
   double W2       = 0;

   // Now call the fortran subroutine
   // cross_tot/cross_tot_mod returns nb/GeV/sr.  
   if(fIsModifiedVersion){
      if(Es>0){
         cross_tot_mod_(&Z, &A, &Es, &Ep, &th, &fullXsec, &F1, &F2, &Q2, &W2);
      }else{
         std::cout << "[F1F209eInclusiveDiffXSec::EvaluateXSec]: Es <= 0!";
         std::cout << " Check Es settings.  Exiting..." << std::endl;
         exit(1);
      }
   }else{
      cross_tot_(&Z, &A, &Es, &Ep, &th, &fullXsec, &F1, &F2, &Q2, &W2);
   }

   if( TMath::IsNaN(fullXsec) ) return 0.0;
   //std::cout << "Es = " << Es << "\t" << "Ep = " << Ep << "\t" << "th = " << th/degree << "\t" << fullXsec << std::endl;         
   // fullXsec /= hbarc2_gev_b; // converts to natural units! 

   if(IncludeJacobian()) return fullXsec*TMath::Sin(th);
   return fullXsec;
}
//______________________________________________________________________________


//______________________________________________________________________________
F1F209QuasiElasticDiffXSec::F1F209QuasiElasticDiffXSec(){
   SetTitle("F1F209QuasiElasticDiffXSec");
   SetPlotTitle("F1F209 born QuasiElastic xsec");
}
//______________________________________________________________________________
F1F209QuasiElasticDiffXSec::~F1F209QuasiElasticDiffXSec(){
}
//______________________________________________________________________________
Double_t F1F209QuasiElasticDiffXSec::EvaluateXSec(const Double_t *x) const {
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   auto Es       = (double)GetBeamEnergy(); 
   auto Ep       = (double)x[0];
   auto th       = (double)x[1];
   auto A        = (double)GetA();
   auto Z        = (double)GetZ();
   // double phi      = x[2];
   double W = insane::Kine::W_EEprimeTheta(Es,Ep,th);
   if( GetA() < 2 ) return 0.0; // no quasi elastic for proton or neutron
   //if( TMath::Abs(  W - M_p/GeV ) > 0.2 ) return 0.0;
   double Q2       = 0;
   double fullXsec = 0;
   double F1       = 0;
   double F2       = 0;
   double W2       = 0;

   // Now call the fortran subroutine
   // cross_tot/cross_tot_mod returns nb/GeV/sr.  
   if(fIsModifiedVersion){
      if(Es>0){
         cross_qe_mod_(&Z, &A, &Es, &Ep, &th, &fullXsec, &F1, &F2, &Q2, &W2);
      }else{
         std::cout << "[F1F209eInclusiveDiffXSec::EvaluateXSec]: Es <= 0!";
         std::cout << " Check Es settings.  Exiting..." << std::endl;
         exit(1);
      }
   }else{
      cross_qe_(&Z, &A, &Es, &Ep, &th, &fullXsec, &F1, &F2, &Q2, &W2);
   }

   if( TMath::IsNaN(fullXsec) ) return 0.0;
   //std::cout << "Es = " << Es << "\t" << "Ep = " << Ep << "\t" << "th = " << th/degree << "\t" << fullXsec << std::endl;         
   // fullXsec /= hbarc2_gev_b; // converts to natural units! 

   if(IncludeJacobian()) return fullXsec*TMath::Sin(th);
   return fullXsec;
}
//______________________________________________________________________________

}}
