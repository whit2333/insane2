#include "insane/xsections/OARPionDiffXSec.h"
#include "insane/base/BremsstrahlungRadiator.h"


namespace insane {
namespace physics {

//______________________________________________________________________________
OARPionDiffXSec::OARPionDiffXSec() 
{
   fID         = 100001012;
   fTitle = "Inclusive Pi0 - OAR fit";
   fPlotTitle = "#frac{d#sigma}{dE_{#pi} d#Omega_{#pi}} mb/GeV-Sr";
   fPIDs.clear();
   fPIDs.push_back(111);   // pi0
   /*      fParticleName = "PI0"; // argument for wiser fortran code*/
   /*      fParticle = TDatabasePDG::Instance()->GetParticle(111);//pi0 */

   fRadiationLength = 0.00001;

   // pi0 parameters
   fa = 11150.0;
   fC = -8.67;
   
   // pi-
   fa_m =  9577.0;   // +- 469
   fC_m =    -8.759; // +- 0.092

   // pi+
   fa_p = 11289.0;   // +- 565 
   fC_p =    -8.536; // +- 0.091
}
//______________________________________________________________________________
OARPionDiffXSec::~OARPionDiffXSec() {
}
//______________________________________________________________________________
Double_t OARPionDiffXSec::OARFitFunc(Double_t Pt) const {
   // returns E d^3/dp^3 = a e^(c Pt) in untis of ub/sr/(GeV/c)^2
   //if( Pt < 0.2 ||  Pt > 0.8 ) { 
   //   Warning("OARFitFunc",Form("Pt(%f)  out of range 0.2 < Pt < 0.8",Pt));
   //}
   return(fa*TMath::Exp(fC*Pt));
}
//______________________________________________________________________________
Double_t OARPionDiffXSec::OARFitFunc(Double_t Pt, Double_t a, Double_t C) const {
   // returns E d^3/dp^3 = a e^(c Pt) in untis of ub/sr/(GeV/c)^2
   //if( Pt < 0.2 ||  Pt > 0.8 ) { 
   //   Warning("OARFitFunc",Form("Pt(%f)  out of range 0.2 < Pt < 0.8",Pt));
   //}
   return(a*TMath::Exp(C*Pt));
}
//______________________________________________________________________________
void OARPionDiffXSec::InitializePhaseSpaceVariables() {
   //std::cout << " o OARPionDiffXSec::InitializePhaseSpaceVariables() \n";

   PhaseSpace * ps = GetPhaseSpace();

      /// Production particle variables
      auto * varEnergy2 = new PhaseSpaceVariable();
      varEnergy2 = new PhaseSpaceVariable();
      varEnergy2->SetNameTitle("energy_pi", "E_{#pi}");
      varEnergy2->SetMinimum(0.5); //GeV
      varEnergy2->SetMaximum(4.5); //GeV
      ps->AddVariable(varEnergy2);

      auto *   varTheta2 = new PhaseSpaceVariable();
      varTheta2->SetNameTitle("theta_pi", "#theta_{#pi}"); // ROOT string latex
      varTheta2->SetMinimum(20.0 * TMath::Pi() / 180.0); //
      varTheta2->SetMaximum(60.0 * TMath::Pi() / 180.0); //
      ps->AddVariable(varTheta2);

      auto *   varPhi2 = new PhaseSpaceVariable();
      varPhi2->SetNameTitle("phi_pi", "#phi_{#pi}"); // ROOT string latex
      varPhi2->SetMinimum(-70.0 * TMath::Pi() / 180.0); //
      varPhi2->SetMaximum( 70.0 * TMath::Pi() / 180.0); //
      ps->AddVariable(varPhi2);

      SetPhaseSpace(ps);
}
//____________________________________________________________________
Double_t OARPionDiffXSec::EvaluateXSec(const Double_t * x) const {
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);


   double EBEAM          = GetBeamEnergy();
   double Epart          = x[0];
   double Ppart          = TMath::Sqrt(Epart*Epart - M_pion*M_pion/(GeV*GeV));
   double THETA          = x[1];
   double Pt             = Ppart*TMath::Sin(THETA); // 
   double sig0           = 0.0;
   //sig0 = OARFitFunc(Pt)/12.0; // returns E*dsig/dp^3
   
   int    pdgcode        = GetParticleType();

   if( pdgcode == 111 )
   {
      sig0 = OARFitFunc(Pt)/12.0; // returns E*dsig/dp^3
   } 
   else if( pdgcode == 211 )
   {
      sig0 = OARFitFunc(Pt, fa_p, fC_p)/12.0; // returns E*dsig/dp^3
   } 
   else if( pdgcode == -211 )
   {
      sig0 = OARFitFunc(Pt, fa_m, fC_m)/12.0; // returns E*dsig/dp^3
   }

   // The first E/p is from dp/dE 
   // the second p^2 is takes the cross section dsig/dp/dOmega = p^2 dsig/dp^3 
   // the last 1/E removes the E in the the fit, ie, E dsig/dp^3
   double RES            = (Ppart)*sig0;

   RES = 1000.0*RES; // Convert from ub to nb

   if ( IncludeJacobian() ) return(RES * TMath::Sin(x[1]) ); 
   return(RES);
}
//______________________________________________________________________________




//______________________________________________________________________________

OARPionElectroDiffXSec::OARPionElectroDiffXSec()
{
   fID         = 100003012;
   fTitle = "Inclusive pi0 electroproduction  OAR fit";
}
//______________________________________________________________________________

OARPionElectroDiffXSec::~OARPionElectroDiffXSec(){
}
//______________________________________________________________________________
Double_t OARPionElectroDiffXSec::EvaluateXSec(const Double_t * x) const {
   //if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);
   double RES            = 0.0;
   double EBEAM          = GetBeamEnergy();
   double Epart          = x[0];
   double Ppart          = TMath::Sqrt(Epart*Epart - M_pion*M_pion/(GeV*GeV));
   double THETA          = x[1];

   // Solution to Tiator-wright Eqn.5 with theta_e=0
   double num   = TMath::Power(Ppart,2.0) + TMath::Power(M_p/GeV,2.0) - TMath::Power(M_p/GeV - Epart, 2.0);
   double denom = 2.0*(Ppart*TMath::Cos(THETA) + M_p/GeV - Epart);

   double AMT = M_p/MeV;//is target mass?
   double AM1 = M_pion/MeV;//is produced particle mass (MeV/c2)
   double EI  = EBEAM*1000.0;//is the electron beam energy (MeV)
   double W0  = (num/denom)*1000.0;//is the theta=0 photon energy (MeV)
   double TP  = (Epart - M_pion/GeV)*1000.0;//is the kinetic energy of the produced particle (MeV)
   double TH  = THETA;//is the angle of the produced particle (radians)
   double GN  = 0.0;//the result of Ne*R/w0 of eqn.12 in Nuc.Phys.A379

   if( W0<0.0 || W0>EI ) {
      GN=0.0;
   } else {
      vtp_(&AMT,&AM1,&EI,&W0,&TP,&TH,&GN);
      GN *= 1000.0; // because GN has units 1/MeV
   }

   //std::cout << "W0 = " << W0 << std::endl;
   //std::cout << "GN = " << GN << std::endl;
   //wiser_all_sig_(&RES,&PART,&EBEAM,&PscatteredPart,&THETA);
   double tot = 0.0;
   //if(PART==0){
   //   // pi0 = (pi+ + pi-)/2.0
   //   PART=1;
   //   wiser_fit_(&PART, &Ppart, &THETA, &EBEAM, &RES);
   //   tot += RES;
   //   PART=2;
   //   wiser_fit_(&PART, &Ppart, &THETA, &EBEAM, &RES);
   //   tot += RES;
   //   tot = tot/2.0;
   //}else {
   //   wiser_fit_(&PART, &Ppart, &THETA, &EBEAM, &RES);
   //   tot += RES;
   //}
   tot = OARPionDiffXSec::EvaluateXSec(x);
   //tot *= (Ppart); // Takes E*dsig/dp^3 to dsig/dEdOmega 
   tot *= GN; // conv

   if(tot<0.0) tot = 0.0;
   if( TMath::IsNaN(tot) ) tot = 0.0;
   //std::cout << " wiser result is " << RES << " nb/GeV*str "
   //<< " for p,theta " << PscatteredPart << "," << THETA << "\n";
   if ( IncludeJacobian() ) return(tot * TMath::Sin(x[1]) ); 
   return(tot); // converts nb to mb
}
//______________________________________________________________________________





//______________________________________________________________________________

OARPionPhotoDiffXSec::OARPionPhotoDiffXSec()
{
   fID         = 100002012;
   fTitle = "Inclusive pi0 photoproduction  OAR fit";
}
//______________________________________________________________________________

OARPionPhotoDiffXSec::~OARPionPhotoDiffXSec(){
}
//______________________________________________________________________________
Double_t OARPionPhotoDiffXSec::EvaluateXSec(const Double_t * x) const {
   //if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);
   double RES            = 0.0;
   double EBEAM          = GetBeamEnergy();
   double Epart          = x[0];
   double Ppart          = TMath::Sqrt(Epart*Epart - M_pion*M_pion/(GeV*GeV));
   double THETA          = x[1];
   //double radlen         = fRadiationLength;

   // Minimum photon energy for photoproduction in gamma+p -> x+n
   // where x is a hadron and n is a nucleon
   // Ex and thetax are the produced hadron's energy and angle
   // mx is the hadron mass, mt is the target mass, mn is the recoil nucleon mass.
   double k_min = insane::Kine::k_min_photoproduction(Epart, THETA);//, M_pion/GeV,fTargetNucleus.GetMass() );//, mx, double mt, double mn) 
   if(k_min > EBEAM) return 0.0;

   if(k_min<0.0) k_min=0.0;
   //std::cout << "k_min = " << k_min << std::endl;
   // simple integration
   int Nint       = 50;
   double delta_w = (EBEAM-k_min)/double(Nint);
   double w       = 0.0;
   double Igam    = 0.0;
   double Igam2    = 0.0;
   double tot     = 0.0;
   double vars[3];
   vars[0] = x[0];
   vars[1] = THETA; 
   vars[2] = x[2];

   TargetMaterial           mat   = GetTargetMaterial();
   BremsstrahlungRadiator * brem  = mat.GetBremRadiator();
   Int_t                          matID = mat.GetMatID();

   for(int i = 0;i<Nint; i++){
      w      = k_min + (double(i)+0.5)*delta_w;
      //Igam   = InSANE::Kine::I_gamma_1_approx(fRadiationLength,EBEAM,w);
      if(brem) Igam = brem->I_gamma( matID, w, EBEAM );
      else     Igam = insane::Kine::I_gamma_1_approx(fRadiationLength,EBEAM,w);
      // pi+
      //PART   =1;
      //wiser_fit_(&PART, &Ppart, &THETA, &w, &RES);
      RES = OARPionDiffXSec::EvaluateXSec(vars);
      if(RES<0.0) RES = 0.0;
      RES  *= (delta_w*Igam);
      tot  += RES;
   }
   //tot = tot/2.0;
   double U  = insane::Kine::I_gamma_1_k_avg(fRadiationLength,0.001,EBEAM);
   double EQ = U/EBEAM;
   //tot *= 1000.0; // converts ub to nb
   tot /= EQ;     // cross section per equivalent quant 

   if( TMath::IsNaN(tot) ) tot = 0.0;
   if( IncludeJacobian() ) return(tot * TMath::Sin(x[1]) ); 
   return(tot); // converts nb to mb
}
//______________________________________________________________________________




//_____________________________________________________________________________

ElectroOARPionDiffXSec::ElectroOARPionDiffXSec()
{
   fID         = 100103012;
   fTitle = "Electro OAR Pion";
   fPlotTitle = "#frac{d#sigma}{dE_{#pi} d#Omega_{#pi}} nb/GeV-Sr";
   fPIDs.clear();
   fPIDs.push_back(111);//
   fProtonXSec  = new OARPionElectroDiffXSec();
   fProtonXSec->SetTargetNucleus(Nucleus::Proton());
   fNeutronXSec = new OARPionElectroDiffXSec();
   fNeutronXSec->SetTargetNucleus(Nucleus::Neutron());
   fProtonXSec->UsePhaseSpace(false);
   fNeutronXSec->UsePhaseSpace(false);
}
//_____________________________________________________________________________
ElectroOARPionDiffXSec::~ElectroOARPionDiffXSec(){
}
//______________________________________________________________________________
Double_t  ElectroOARPionDiffXSec::EvaluateXSec(const Double_t * x) const{

   //std::cout << " composite eval " << std::endl;
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   if( !fProtonXSec )  return 0.0;
   if( !fNeutronXSec ) return 0.0;

   Double_t z    = GetZ();
   //if( z>0.0 ) z = 1.0;
   Double_t n    = GetN();
   //if( n>0.0 ) n = 1.0;

   Double_t sigP = fProtonXSec->EvaluateXSec(x);
   Double_t sigN = fNeutronXSec->EvaluateXSec(x);
   Double_t xsec = z*sigP + n*sigN;
   //std::cout << " Z  = " << GetZ() << std::endl;
   //std::cout << "sp  = " << sigP << std::endl;
   //std::cout << " N  = " << GetN() << std::endl;
   //std::cout << "sn  = " << sigN << std::endl;

   //std::cout << " composite eval " <<  xsec << std::endl;
   if ( IncludeJacobian() ) return xsec*TMath::Sin(x[1]);
   return xsec;
}
//______________________________________________________________________________





////_____________________________________________________________________________
//
//PhotoOARPionDiffXSec::PhotoOARPionDiffXSec()
//{
//   fID = 100102012;
//   // note that we are not using OARPionPhotoDiffXSec becuase
//   // OARPionDiffXSec is actually a fit to photoproduction data 
//   // that has not been unfolded from the bremsstrahlung spectrum.
//   fTitle = "Photo OAR Pion";
//   fPlotTitle = "#frac{d#sigma}{dE_{#pi} d#Omega_{#pi}} nb/GeV-Sr";
//   fPIDs.clear();
//   fPIDs.push_back(111);//
//   fProtonXSec  = new OARPionDiffXSec();
//   fProtonXSec->SetTargetNucleus(Nucleus::Proton());
//   fNeutronXSec = new OARPionDiffXSec();
//   fNeutronXSec->SetTargetNucleus(Nucleus::Neutron());
//   fProtonXSec->UsePhaseSpace(false);
//   fNeutronXSec->UsePhaseSpace(false);
//   //std::cout << "done" << std::endl;
//}
////_____________________________________________________________________________
//
//PhotoOARPionDiffXSec::~PhotoOARPionDiffXSec()
//{ }
////______________________________________________________________________________
//
//Double_t  PhotoOARPionDiffXSec::EvaluateXSec(const Double_t * x) const
//{
//   //std::cout << " composite eval " << std::endl;
//   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);
//
//   if( !fProtonXSec )  return 0.0;
//   if( !fNeutronXSec ) return 0.0;
//
//   Double_t z    = GetZ();
//   //if( z>0.0 ) z = 1.0;
//   Double_t n    = GetN();
//   //if( n>0.0 ) n = 1.0;
//
//   Double_t sigP = fProtonXSec->EvaluateXSec(x);
//   Double_t sigN = fNeutronXSec->EvaluateXSec(x);
//   Double_t xsec = z*sigP + n*sigN;
//   //std::cout << " Z  = " << GetZ() << std::endl;
//   //std::cout << "sp  = " << sigP << std::endl;
//   //std::cout << " N  = " << GetN() << std::endl;
//   //std::cout << "sn  = " << sigN << std::endl;
//
//   //std::cout << " composite eval " <<  xsec << std::endl;
//   if ( IncludeJacobian() ) return xsec*TMath::Sin(x[1]);
//   return xsec;
//}
////______________________________________________________________________________

}
}
