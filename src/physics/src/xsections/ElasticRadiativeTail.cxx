#include "insane/xsections/ElasticRadiativeTail.h"

namespace insane {
namespace physics {

ElasticRadiativeTail::ElasticRadiativeTail()
{
   fID         = 100010014;

   SetTitle("ElasticRadiativeTail");//,"POLRAD Born cross-section");
   SetPlotTitle("Elastic Radiative Tail cross-section");

   fLabel = "#frac{d#sigma}{dEd#Omega}";
   fUnits = "nb/GeV/sr";

   //fPOLRAD = new POLRAD();
   //fPOLRAD->SetVerbosity(1);
   //fPOLRAD->DoQEFullCalc(false); 
   //fPOLRAD->SetTargetType(Nucleus::kProton);
   //fPOLRAD->fErr   = 1E-1;   // integration error tolerance 
   //fPOLRAD->fDepth = 3;     // number of iterations for integration 
   //fPOLRAD->SetMultiPhoton(true); 
   //fPOLRAD->SetUltraRel   (false);

   //fRadLen[0] = 0.025; 
   //fRadLen[1] = 0.025; 

   fElasticDiffXSec = new  POLRADElasticDiffXSec();
   fElasticDiffXSec->SetTargetNucleus(Nucleus::Proton());
   //fElasticDiffXSec->SetA(1);
   //fElasticDiffXSec->SetZ(1);
   fElasticDiffXSec->InitializePhaseSpaceVariables();
   fElasticDiffXSec->InitializeFinalStateParticles();

   //Nucleus::NucleusType Target = Nucleus::kProton; 
   //fRADCOR = new RADCOR();
   //fRADCOR->DoElastic(false); 
   //fRADCOR->SetThreshold(2); 
   //fRADCOR->UseMultiplePhoton();
   //fRADCOR->UseInternal(false);  
   //fRADCOR->UseExternal(true);  
   //fRADCOR->SetPolarization(0); 
   //fRADCOR->SetVerbosity(0); 
   GetRADCOR()->SetElasticCrossSection(fElasticDiffXSec); 
   GetRADCOR()->UseMultiplePhoton(); 
   //GetRADCOR()->SetElasticCrossSection(fElasticDiffXSec);
   //fRADCOR->SetTargetType(Target); 
   //fRADCOR->SetRadiationLengths(fRadLen);


   //// Structure functions F1,F2
   //StructureFunctions * sf   = funcMan->GetStructureFunctions();
   //SetUnpolarizedStructureFunctions(sf);

   //// Structure functions g1,g2
   //PolarizedStructureFunctions * psf   = funcMan->GetPolarizedStructureFunctions();
   //SetPolarizedStructureFunctions(psf);

   //// Quasi Elastic structure functions
   //F1F209QuasiElasticStructureFunctions * F1F209QESFs = new F1F209QuasiElasticStructureFunctions();
   //SetQEStructureFunctions(F1F209QESFs);

   //// Nucleon form factors 
   //FormFactors * FFs = funcMan->GetFormFactors(); 
   //SetFormFactors(FFs);

   //// Nuclei form factors 
   ////FormFactors * NFFs = new AmrounFormFactors();
   //FormFactors * NFFs = new MSWFormFactors();
   //SetTargetFormFactors(NFFs);
}

//________________________________________________________________________________
ElasticRadiativeTail::~ElasticRadiativeTail(){
}
//________________________________________________________________________________
Double_t ElasticRadiativeTail::EvaluateXSec(const Double_t *x) const {
   if (!VariablesInPhaseSpace(fnDim, x)){
      //std::cout << "[POLRADElasticTailDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
      return(0.0);
   }
   if( GetTargetMaterial().fA != 1 )  return 0.0;

   

   Double_t Eprime  = x[0];
   Double_t theta   = x[1];
   Double_t phi     = x[2];
   Double_t Ebeam   = GetBeamEnergy();
   Double_t nu      = Ebeam-Eprime;
   Double_t Mtarg   = fPOLRAD->GetTargetMass();  

   // external 
   Double_t sig1    = GetRADCOR()->ExternalOnly_ExactElasticTail(Ebeam,Eprime,theta,phi);

   fPOLRAD->SetKinematics(Ebeam,Eprime,theta);
   Double_t sig2  = fPOLRAD->fA*fPOLRAD->ERT();
   // converts from dsigma/dxdy to dsigma/dEdOmega
   sig2 = sig2*(Eprime/(2.0*pi*Mtarg*nu))*hbarc2_gev_nb;

   Double_t sig_rad = sig1 + sig2; 
   //std::cout << "sig_rad = " << sig_rad << std::endl;
   if( IncludeJacobian() ) sig_rad = sig_rad*TMath::Sin(theta);
   if( sig_rad <0.0 || TMath::IsNaN(sig_rad)) sig_rad = 0.0;
   return sig_rad;
} 
//________________________________________________________________________________
//________________________________________________________________________________
//________________________________________________________________________________
//________________________________________________________________________________
}}
