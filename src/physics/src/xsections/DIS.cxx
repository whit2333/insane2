#include "insane/xsections/DIS.h"

namespace insane {
  namespace physics {

    namespace xs {

      double DIS_xs_func(double E0, double x_bj, double Q2, double F1, double F2) {
        // formula for dis with fixed nucleon target
        // double   x_bj  = vars.at(0);
        // double   Q2    = vars.at(1);
        // double   phi   = vars.at(2);
        //
        double M     = 0.938;
        double alpha = 1. / 137.;
        double s     = M * M + 2.0 * M * E0;
        double Ee    = E0 - Q2 / (2.0 * M * x_bj);
        double y     = (E0 - Ee) / E0;
        double nu    = y * E0;
        double th_e  = 2.0 * std::asin(std::sqrt(M * x_bj * y / (2.0 * E0 * (1.0 - y))));
        double q     = std::sqrt(
            Q2 + nu * nu); // std::sqrt(E0 * E0 + Ee * Ee - 2.0 * E0 * Ee * std::cos(th_e));
        // double th_q           = std::asin(Ee * std::sin(th_e) / q);
        double W = std::sqrt(M * M - Q2 + 2.0 * M * nu);
        // double Ep    = E0 - Q2 / (2.0 * M * x_bj);
        //// double E0    = (s - M * M) / (2.0 * M);
        // double y  = Q2 / (x_bj * (s - M * M));
        // double nu = y * E0; // nu_func(is,vars);
        // double M  = 0.938;
        double W1 = (1. / M) * F1;
        double W2 = (1. / nu) * F2;
        //// compute the Mott cross section (units = mb):
        ////Double_t hbarc2 = 0.38939129; // (hbar*c)^2 = 0.38939129
        /// mb*GeV^2
        // double Ep   = (Q2 / (2.0 * M * x_bj)) * (1.0 - y) / y;
        // double th   = 2.0 * std::asin(M * x_bj * y / std::sqrt(Q2 * (1.0 - y)));
        double th = th_e; // 2.0 * std::asin(std::sqrt(M * x_bj * y / (2.0 * E0 * (1.0 - y))));
        // std::cout << th << "\n";
        double COS2 = std::cos(th) * std::cos(th);
        double SIN2 = std::sin(th) * std::sin(th);
        double TAN2 = SIN2 / COS2;
        double num  = alpha * alpha * COS2;
        double den  = 4. * E0 * E0 * SIN2;
        // return 1.0/vars.at(1);
        double MottXS   = num / den;
        double fullXsec = MottXS * (W2 + 2.0 * TAN2 * W1) * insane::units::hbarc2_gev_nb;
        //// compute the full cross section (units = nb/GeV/sr)
        // std::cout << " E0  " << E0    << "\n";
        // std::cout << " s   " << s    << "\n";
        // std::cout << " Ee  " << Ee    << "\n";
        // std::cout << " q   " << q    << "\n";
        // std::cout << " x   " << x_bj << "\n";
        // std::cout << " Q2  " << Q2   << "\n";
        // std::cout << " W1  " << W1  << "\n";
        // std::cout << " W1  " << W2  << "\n";
        // std::cout << " y   " << y  << "\n";
        // std::cout << " th  " << th  << "\n";
        // std::cout << " den " << den  << "\n";
        // std::cout << " num " << num  << "\n";
        return fullXsec;
      }

    }
  }
}

