#include "insane/xsections/XSections.h"
namespace insane {
namespace physics {

//_______________________________________________________//

Double_t  InclusiveMottXSec::EvaluateXSec(const Double_t * x) const
{
   Double_t y[3] = {GetEPrime(x[1]), x[1], x[2]};

   if (!VariablesInPhaseSpace(fnDim, y)) return(0.0);
   Double_t Eprime = y[0];
   Double_t theta = y[1];
   //Double_t phi = y[2];
//    Double_t Qsquared=4.0*fBeamEnergy*Eprime*TMath::Power(TMath::Sin(theta/2.0),2);
//    Double_t xbjorken=Qsquared/(2.0*0.938*(fBeamEnergy-Eprime));
//MeV and degrees
   Double_t hbarc2 = 0.38939129; /*(hbar*c)^2 = 0.38939129 GeV^2 mbarn */
//   double res = (alphaSquared*fBeamEnergy*fBeamEnergy/(4.0*p*p*p*p*std::pow(std::sin(theta/2.0),4)) )*(1.0-std::pow(p*std::sin(theta/2.0)/E,2));

   Double_t mottXSec = hbarc2 * (1. / 137.) * (1. / 137.) *
                       TMath::Power(TMath::Cos(theta / 2.0), 2) /
                       (4.0 * fBeamEnergy * fBeamEnergy * TMath::Power(TMath::Sin(theta / 2.0), 4))
                       * (Eprime / fBeamEnergy);
   return(mottXSec);
}
//_______________________________________________________//
void InclusiveMottXSec::InitializePhaseSpaceVariables()
{
   auto * ps = GetPhaseSpace();

   auto * varEnergy = new PhaseSpaceVariable();
   varEnergy = new PhaseSpaceVariable();
   varEnergy->SetNameTitle("energy_e", "E_{e'}");
   varEnergy->SetMinimum(0.8); //GeV
   varEnergy->SetMaximum(5.9); //GeV
   //fEnergy_e = varEnergy->GetCurrentValueAddress();
   //fEnergy = varEnergy->GetCurrentValueAddress();
   ps->AddVariable(varEnergy);

   auto *   varTheta = new PhaseSpaceVariable();
   varTheta->SetNameTitle("theta_e", "#theta_{e'}"); // ROOT string latex
   varTheta->SetMinimum(20.0 * TMath::Pi() / 180.0); //
   varTheta->SetMaximum(50.0 * TMath::Pi() / 180.0); //
   //fTheta_e = varTheta->GetCurrentValueAddress();
   //fTheta = varTheta->GetCurrentValueAddress();
   ps->AddVariable(varTheta);

   auto *   varPhi = new PhaseSpaceVariable();
   varPhi->SetNameTitle("phi_e", "#phi_{e'}"); // ROOT string latex
   varPhi->SetMinimum(-50.0 * TMath::Pi() / 180.0); //
   varPhi->SetMaximum(50.0 * TMath::Pi() / 180.0); //
   //fPhi_e = varPhi->GetCurrentValueAddress();
   //fPhi = varPhi->GetCurrentValueAddress();
   ps->AddVariable(varPhi);

   SetPhaseSpace(ps);
}




}}
