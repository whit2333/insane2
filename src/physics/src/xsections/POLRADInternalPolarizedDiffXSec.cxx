#include "insane/xsections/POLRADInternalPolarizedDiffXSec.h"


namespace insane {
namespace physics {
//______________________________________________________________________________

POLRADInternalPolarizedDiffXSec::POLRADInternalPolarizedDiffXSec()
{
   fID         = 100010004;
   SetTitle("POLRADInternalPolarizedDiffXSec");//,"POLRAD Born cross-section");
   SetPlotTitle("POLRAD Internal Polarized cross-section");

   fLabel = "#frac{d#sigma}{dxdy}";
   fUnits = "nb";

   fPOLRADCalc = nullptr;
   fPOLRADCalc = new POLRAD();
   fPOLRADCalc->SetVerbosity(1);
   //fPOLRADCalc->DoQEFullCalc(false); 
   fPOLRADCalc->SetTargetNucleus(Nucleus::Proton());
   fPOLRADCalc->fErr   = 1E-1;   // integration error tolerance 
   fPOLRADCalc->fDepth = 3;     // number of iterations for integration 
   fPOLRADCalc->SetMultiPhoton(true); 
   //fPOLRADCalc->SetUltraRel   (false);


   // Structure functions F1,F2
   StructureFunctions * sf   = new DefaultStructureFunctions();
   SetUnpolarizedStructureFunctions(sf);

   // Structure functions g1,g2
   PolarizedStructureFunctions * psf   = new DefaultPolarizedStructureFunctions();
   SetPolarizedStructureFunctions(psf);

   // Quasi Elastic structure functions
   auto * F1F209QESFs = new F1F209QuasiElasticStructureFunctions();
   SetQEStructureFunctions(F1F209QESFs);

   // Nucleon form factors 
   FormFactors * FFs = new DefaultFormFactors(); 
   SetFormFactors(FFs);

   // Nuclei form factors 
   //FormFactors * NFFs = new AmrounFormFactors();
   FormFactors * NFFs = new MSWFormFactors();
   SetTargetFormFactors(NFFs);

   fPOLRADCalc = GetPOLRAD();


}
//______________________________________________________________________________
POLRADInternalPolarizedDiffXSec::~POLRADInternalPolarizedDiffXSec(){
}
//______________________________________________________________________________
void POLRADInternalPolarizedDiffXSec::InitializePhaseSpaceVariables(){

   if(fPOLRADCalc->GetVerbosity() > 0) std::cout << " o POLRADInternalPolarizedDiffXSec::InitializePhaseSpaceVariables() \n";

   PhaseSpace * ps = GetPhaseSpace();

      auto * varx = new PhaseSpaceVariable();
      varx = new PhaseSpaceVariable();
      varx->SetNameTitle("x", "x");
      varx->SetMinimum(0.01); //GeV
      varx->SetMaximum(0.999); //GeV
      //fxBjorken_var = varx->GetCurrentValueAddress();
      ps->AddVariable(varx);

      auto *   vary = new PhaseSpaceVariable();
      vary->SetNameTitle("y", "y"); // ROOT string latex
      vary->SetMinimum(0.01); //
      vary->SetMaximum(0.9999); //
      //fy_var = vary->GetCurrentValueAddress();
      ps->AddVariable(vary);

      auto *   varPhi = new PhaseSpaceVariable();
      varPhi->SetNameTitle("phi_e", "#phi_{e'}"); // ROOT string latex
      varPhi->SetMinimum(-60.0 * TMath::Pi() / 180.0); //
      varPhi->SetMaximum(60.0 * TMath::Pi() / 180.0); //
      //fPhi_var = varPhi->GetCurrentValueAddress();
      ps->AddVariable(varPhi);

      SetPhaseSpace(ps);

}

//______________________________________________________________________________

}}
