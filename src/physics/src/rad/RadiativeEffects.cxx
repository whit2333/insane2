#include "insane/rad/RadiativeEffects.h"
#include "insane/rad/RadCor.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "Math/Vector4D.h"
#include "TMath.h"

namespace insane::rad {

  using namespace units;
  using namespace constants;

      double F_Tsai(double Es, double Ep, double theta, double T)
      {
         // "Structure function" that multiplies the non-radiative cross section
         // Eqn 2.8, Tsai, SLAC-PUB-848 1971
         using namespace TMath;
         double T1 = F_tilde_internal(Es,Ep,theta);
         double b  = 4.0/3.0;
         double T2 = 0.5772*b*T;
         return T1+T2;
      }

      double F_tilde_internal(double Es, double Ep, double theta) {
        using namespace TMath;
        using namespace insane::constants;
        double alpha_over_pi = fine_structure_const / pi;
        double Q2            = 4.0 * Es * Ep * std::pow(std::sin(theta / 2.0), 2.0);
        double res           = 1.0;
        res += 2.0 * alpha_over_pi *
               (-14.0 / 9.0 + 13.0 / 12.0 * Log(Q2 / ((M_e / GeV) * (M_e / GeV))));
        res += -0.5 * alpha_over_pi * (Power(Log(Es / Ep), 2.0));
        res += alpha_over_pi * (pi * pi / 6.0 - DiLog(Power(Cos(theta / 2.0), 2.0)));
        return res;
      }

      double BremShape_phi(double v) { return (1.0 - v + (3.0 / 4.0) * v * v); }

      double LandauStraggling_xi(double T, double Z, double A)
      {
         // Eqn 2.5, Tsai, SLAC-PUB-848 1971
         using namespace TMath;
         double X0     = insane::rad::rad_length(Z,A);
         double xi     = 0.000154*(Z/A)*T*X0*insane::units::GeV;
         return xi;
      }

      //double b_Tsai(double T, double Z) {
      //  double Z23 = TMath::Power(Z, -2.0 / 3.0);
      //  double Z13 = TMath::Power(Z, -1.0 / 3.0);
      //  // From Rev. Mod. Phys. 41, 205 (1969)
      //  // Double_t f23 = 1440.0;
      //  // Double_t f13 = 183.0;
      //  // From Rev. Mod. Phys. 46, 815 (1974) [UPDATED from 1969]
      //  double f23 = 1194.;
      //  double f13 = 184.15;
      //  double fEta = TMath::Log(f23 * Z23) / TMath::Log(f13 * Z13);
      //  double fb =
      //      (4. / 3.) * (1. + (1. / 9.) * ((fZ + 1.) / (fZ + fEta)) * (1. / TMath::Log(f13 * Z13)));
      //  double fXi = (pi * fm / (2. * alpha)) * (T / ((fZ + fEta) * TMath::Log(f13 * Z13)));
      //  return b;
      //}
      //double Ie_Tsai(double E0, double E, double theta, double t) {
      //  /// Bremsstrahlung loss
      //  /// Tsai, SLAC-PUB-0848 (1971), Eq 5.3
      //  /// This is utilizing the equivalent radiator method,
      //  /// so t should be 1/2 the thickness of the radiating material
      //  // general terms
      //  double b    = b_Tsai(t, 1.0);
      //  double Q2   = insane::kine::Q2(E0, E, theta);
      //  double t_r  = GetTr(Q2);
      //  double v    = (E0 - E) / E0;
      //  double exp1 = b * (t + t_r);
      //  // Term 1
      //  double T1 = TMath::Power(v, exp1);
      //  // Term 2
      //  double T2 = exp1 / (E0 - E);
      //  // Term 3
      //  double phi = TMath::DiLog(v);
      //  double T3  = phi;
      //  // Put it together
      //  double ie = T1 * T2 * T3;
      //  return ie;
      //}

      double RCToPeak_Tsai(double Es, double EpPeak, double theta, double T, double Z, double A)
      {
         // Multiplicative factor for the radiative correction to the jth peak.
         // Eqn 2.3, Tsai, SLAC-PUB-848 1971
         // This returns the term multiplying sig_j^eff on the RHS. 
         using namespace TMath;
         double Q2     = 4.0*Es*EpPeak*Power(Sin(theta/2.0),2);
         //double EpPeak = Es/(1.0 + 2.0*Es/M*TMath::Power(TMath::Sin(theta/2.0), 2));
         double R      = 1.0;
         double b      = 4.0/3.0;
         double deltaE = 0.01;
         double tr     = tr_Tsai(Q2);
         double X0     = insane::rad::rad_length(Z,A);
         double xi     = 0.000154*(Z/A)*T*X0;
         double Tprime = b*(T/2.0 + tr);
         double res  = Power(R*deltaE*deltaE/(Es*EpPeak),Tprime)*(1.0-xi/deltaE);
         //std::cout  << (1.0-xi/deltaE) << std::endl;
         return res;
      }

      double Ib_integral(double E0, double Eprime, double DeltaE, double T, double Z, double A)
      {
         // Integral of Ib near tip 
         // Eqn B.36, Tsai, SLAC-PUB-848 1971
         using namespace TMath;
         double b      = 4.0/3.0;
         double part1  = (1.0+0.5722*b*T)*Power(DeltaE/E0,b*T);
         double X0     = insane::rad::rad_length(Z,A);
         double xi     = 0.000154*(Z/A)*T*X0;
         double part2  = 1.0-xi/((1.0-b*T)*DeltaE);
         double res    = part1*part2;
         return res;
      }

      double tr_Tsai(double Q2)
      {
         // Eqn 2.7, Tsai, SLAC-PUB-848 1971
         double b      = 4.0/3.0;
         double m2    = (M_e)*(M_e); 
         double alpha = fine_structure_const; 
         double T1    = (1.0/b)*(alpha/pi); 
         double T2    = TMath::Log(Q2/m2) - 1.0; 
         return T1*T2;
      }

      double omega_s_Tsai(double Es, double Ep, double theta, double M)
      {
         // Eqn C.11, Tsai, SLAC-PUB-848 1971
         using namespace TMath;
         double Q2 = 4.0*Es*Ep*Power(Sin(theta/2.0),2.0);
         double W2 = M*M - Q2 + 2.0*M*(Es-Ep);
         return( (W2-M*M)/(2.0*(M-Ep*(1.0-Cos(theta))))) ;
      }

      double omega_p_Tsai(double Es, double Ep, double theta, double M)
      {
         // Eqn C.12, Tsai, SLAC-PUB-848 1971
         using namespace TMath;
         double Q2 = 4.0*Es*Ep*Power(Sin(theta/2.0),2.0);
         double W2 = M*M - Q2 + 2.0*M*(Es-Ep);
         return( (W2-M*M)/(2.0*(M+Es*(1.0-Cos(theta))))) ;
      }

      double Ib(const double& E0, const double& E, const double& t) {
        const double b     = 4.0 / 3.0; // fKinematics.Getb();
        const double gamma = TMath::Gamma(1. + b * t);
        // Double_t Wb    = W_b(E0,E0-E);
        // Wb is (X0 cancels out when combined with everything in this method):
        // Double_t b     = fKinematicsExt.Getb();
        // Double_t X0    = fX0Eff;
        // Double_t Wb    = (1./X0)*(b/eps)*GetPhi(eps/E0);
        const double v   = (E0 - E) / E0;
        const double phi = (1.0 - v + (3.0 / 4.0) * v * v);
        const double T1  = (b * t / gamma) * (1. / (E0 - E));
        const double T2  = std::pow(v, b * t);
        const double T3  = phi;
        const double ib  = T1 * T2 * T3;
        return ib;
      }

      double Delta_Tsai71(double E, double t) {
        /// Most probable energy loss due to ionization of an
        /// electron passing through material of thickness t.
        /// For Es, use t_b; for Ep, use t_a
        /// Tsai, SLAC-PUB-0848 (1971), Eq 1.8
        double Z         = 1;//fZEff;
        double A         = 1;//fAEff;
        double m         = insane::masses::M_e/insane::units::GeV;//KinematicsExt.Getm();
        double X0    = 0.025;//fX0Eff;
        double a     = 0.154E-3 * (Z / A); /// B19 [in GeV]
        double arg   = 3E+9 * a * t * X0 * E * E / (m * m * Z * Z);
        double delta = 0;
        if (t > 0) {
          delta = a * t * X0 * (TMath::Log(arg) - 0.5772);
        } else {
          delta = 0.;
        }
        return delta*insane::units::GeV;
      }
     
      double Delta_0_Tsai71(double E0, double t) {
        /// Most probable energy loss due to ionization
        /// Tsai, SLAC-PUB-0848 (1971)
        /// Eq B22
        double Z         = 1;//fZEff;
        double A         = 1;//fAEff;
        double m         = insane::masses::M_e/insane::units::GeV;//KinematicsExt.Getm();
        double X0    = 0.025;//fX0Eff;
        double a         = 0.154E-3 * (Z / A); /// B19 [in GeV]
        double I         = 13.2E-6 * Z;        /// B20 [in GeV] (nota bene!)
        double eps_prime = m * TMath::Exp(1.) * I * I / (2. * E0 * E0); /// B20
        double delta_0   = 0;
        if (t > 0) {
          delta_0 = a * t * X0 * (TMath::Log(a * t * X0 / eps_prime) + 1. - 0.5772);
        }
        return delta_0*insane::units::GeV;
      }

      //double StripApproxIntegrand1_Tsai71(double Es, double Ep, double theta, double T,
      //                                    double EsPrime) {
      //  // Mo and Tsai 1971 C.23
      //  // This is the Es' integrand
      //  double b      = 4.0 / 3.0;           // fKinematics.Getb();
      //  double M      = insane::masses::M_p; // fKinematics.GetM();
      //  double max_Ep = GetEpMax(Es, theta);
      //  double R = (M + Es * (1.0 - TMath::Cos(theta))) / (M - max_Ep * (1.0 - TMath::Cos(theta)));
      //  // double R = (M + Es*(1.0 - TMath::Cos(theta)))/(M - Ep*(1.0 - TMath::Cos(theta)));
      //  double T1    = TMath::Power((Es - EsPrime) * (Es - EsPrime) / (Ep * R * Es), b * T / 2.0);
      //  double T2    = b * T / (2.0 * (Es - EsPrime));
      //  double par[] = {Ep, theta, 0.0};

      //  //fUnpolXS->SetBeamEnergy(EsPrime);

      //  double f_tilde = insane::kine::F_tilde_internal(EsPrime, Ep, theta);
      //  // std::cout << " T1 = " << T1 << std::endl;
      //  // std::cout << " T2 = " << T2 << std::endl;
      //  // std::cout << " R = " << R << std::endl;
      //  // std::cout << " sig = " << fUnpolXS->EvaluateBaseXSec(par) << std::endl;
      //  return T1 * T2 * f_tilde * fUnpolXS->EvaluateBaseXSec(par) * GetPhi((Es - EsPrime) / Es);
      //}
      //double StripApproxIntegrand2_Tsai71(double Es, double Ep, double theta, double T,
      //                                    double EpPrime) {
      //  // Mo and Tsai 1971 C.23
      //  // This is the Ep' integrand
      //  double b      = fKinematics.Getb();
      //  double M      = fKinematics.GetM();
      //  double max_Ep = GetEpMax(Es, theta);
      //  double R = (M + Es * (1.0 - TMath::Cos(theta))) / (M - max_Ep * (1.0 - TMath::Cos(theta)));
      //  // double R = (M + Es*(1.0 - TMath::Cos(theta)))/(M - Ep*(1.0 - TMath::Cos(theta)));
      //  double T1 = TMath::Power((EpPrime - Ep) * (EpPrime - Ep) * R / (EpPrime * Es), b * T / 2.0);
      //  double T2 = b * T / (2.0 * (EpPrime - Ep));
      //  double par[]   = {EpPrime, theta, 0.0};
      //  double f_tilde = insane::kine::F_tilde_internal(Es, EpPrime, theta);
      //  fUnpolXS->SetBeamEnergy(Es);
      //  return T1 * T2 * f_tilde * fUnpolXS->EvaluateBaseXSec(par) *
      //         GetPhi((EpPrime - Ep) / EpPrime);
      //}

      double InternalIntegrand_MoTsai69(double COSTHK, double Es, double Ep, double theta,
                                        double phi, double M, std::array<double, 2> ffs) {

        double fZ    = 1;
        double alpha = insane::constants::fine_structure_const;
        // for Q2, etc
        double SIN  = std::sin(theta / 2.0);
        double SIN2 = SIN * SIN;
        // for 4-vectors
        double SIN_TH = SIN;
        double COS_TH = std::cos(theta);
        double SIN_PH = std::sin(phi);
        double COS_PH = std::cos(phi);
        // powers of Z
        double Z23 = TMath::Power(fZ, -2.0 / 3.0);
        double Z13 = TMath::Power(fZ, -1.0 / 3.0);

        // if(fUnits==1){
        //   // convert to MeV.  Default is GeV (fUnits = 0)
        //   // energies are NOT converted, because they are input
        //   // by the user (including M_A)
        //   fM      *= fCONV;
        //   fm      *= fCONV;
        //   fM_pion *= fCONV;
        //}

        double MASS = M;

        double fTb = 0.;
        double fTa = 0.;
        double fT  = fTb + fTa;

        // kinematics, etc.
        // From Rev. Mod. Phys. 41, 205 (1969)
        // Double_t f23 = 1440.0;
        // Double_t f13 = 183.0;
        // From Rev. Mod. Phys. 46, 815 (1974) [UPDATED from 1969]
        double f23 = 1194.;
        double f13 = 184.15;
        double fm  = insane::masses::M_e / GeV;

        double fEta = TMath::Log(f23 * Z23) / TMath::Log(f13 * Z13);
        double fb =
            (4. / 3.) * (1. + (1. / 9.) * ((fZ + 1.) / (fZ + fEta)) * (1. / TMath::Log(f13 * Z13)));
        double fXi = (pi * fm / (2. * alpha)) * (fT / ((fZ + fEta) * TMath::Log(f13 * Z13)));

        double fM_A = insane::masses::M_p / GeV;
        // other kinematical quantities
        double fR  = (fM_A + 2.0 * Es * SIN2) / (fM_A - 2.0 * Ep * SIN2);
        double fQ2 = insane::kine::Q2(Es, Ep, theta);
        double fx  = fQ2 / (2. * MASS * (Es - Ep));
        double fy  = (Es - Ep) / Es;
        double fW2 = MASS * MASS + 2. * MASS * (Es - Ep) - fQ2;
        double fW  = TMath::Sqrt(fW2);

        /// 4-vectors
        /// theta_scat = e- in-plane scattering angle in RADIANS
        /// phi_scat   = e- out-of-plane scattering angle in RADIANS
        /// +x is out of the page
        /// +y is to the right
        /// +z is up

        // incident electron: s
        double s_x = 0;
        double s_y = 0;
        double s_z = TMath::Sqrt(Es * Es - fm * fm);
        double s_t = Es;
        // scattered electron: p
        double p0  = TMath::Sqrt(Ep * Ep - fm * fm);
        double p_x = p0 * SIN_TH * COS_PH;
        double p_y = p0 * SIN_TH * SIN_PH;
        double p_z = p0 * COS_TH;
        double p_t = Ep;
        // for the (FIXED) target: t
        double t_x = 0;
        double t_y = 0;
        double t_z = 0;
        // double t_t = fM_A;
        double t_t = MASS; // inelastic scattering from a nucleon!
        // note: time component goes last
        ROOT::Math::XYZTVector s(s_x, s_y, s_z, s_t);
        ROOT::Math::XYZTVector p(p_x, p_y, p_z, p_t);
        ROOT::Math::XYZTVector t(t_x, t_y, t_z, t_t);
        ROOT::Math::XYZTVector u = s + t - p;
        // zero component and 3-vector terms
        double u_0   = Es - Ep + MASS;
        double pVect = TMath::Sqrt(p.E() * p.E() - p.Dot(p));
        double sVect = TMath::Sqrt(s.E() * s.E() - s.Dot(s));
        double tVect = TMath::Sqrt(t.E() * t.E() - t.Dot(t));
        double uVect = TMath::Sqrt(u_0 * u_0 - u.Dot(u));
        /// Mo & Tsai, Rev Mod Phys 41, 205 (1969)  (Eq B5)
        // double Es      = fKinematicsInt.GetEs();
        // double Ep      = fKinematicsInt.GetEp();
        // double theta   = fKinematicsInt.GetTheta();
        // double m       = fKinematicsInt.Getm();
        // double M_A     = fKinematicsInt.GetM_A();
        // double uVect   = fKinematicsInt.GetuVect();
        // double sVect   = fKinematicsInt.GetsVect();
        // double pVect   = fKinematicsInt.GetpVect();
        // double u_0     = fKinematicsInt.Getu_0();
        // double Wsq     = fKinematicsInt.GetW2();    // M_f = W, see eq A1 in Tsai, SLAC-PUB-848
        // -- this seems to give omega = 0 though!

        double s_dot_p = s.Dot(p);
        double u_sq    = u.Dot(u);
        double omega   = 0.5 * (u_sq - M_A * M_A) / (u_0 - uVect * COSTHK);
        double q2 =
            2. * m * m - 2. * (s_dot_p)-2. * omega * (Es - Ep) + 2. * omega * uVect * COSTHK;
        double SINTH  = TMath::Sin(theta);
        double COSTH  = TMath::Cos(theta);
        double COSTHP = (sVect * COSTH - pVect) / uVect;
        double COSTHS = (sVect - pVect * COSTH) / uVect;
        double SINTHS = TMath::Sqrt(1. - COSTHS * COSTHS);
        double SINTHP = TMath::Sqrt(1. - COSTHP * COSTHP);
        double SINTHK = TMath::Sqrt(1. - COSTHK * COSTHK);
        double a      = omega * (Ep - pVect * COSTHP * COSTHK);
        double apr    = omega * (Es - sVect * COSTHS * COSTHK);
        double b      = (-1.) * omega * pVect * SINTHP * SINTHK;
        double bpr    = (-1.) * omega * sVect * SINTHS * SINTHK;
        double vden =
            omega * (Ep * sVect * SINTHS - Es * pVect * SINTHP + sVect * pVect * SINTH * COSTHK);
        double v   = (-1.) * pVect * SINTHP / vden;
        double vpr = (-1.) * sVect * SINTHS / vden;
        double x   = TMath::Sqrt(a * a - b * b);         /// WARNING: NOT the scaling variable!
        double y   = TMath::Sqrt(apr * apr - bpr * bpr); /// WARNING: NOT the scaling variable!

        /// Calculate FTilde, GE and GM
        // Nucleus::NucleusType Target = fTargetNucleus.GetType();
        // double FTildeTerm = 0.;
        // switch(Target){
        //   case Nucleus::kProton:
        //      break;
        //   case Nucleus::kNeutron:
        //      FTildeTerm = 1.;
        //      break;
        //   default:
        //      FTildeTerm = GetFTildeExactInternal(-q2);
        //      break;
        //}
        double FTildeTerm = 1.;

        // ProcessFormFactors(-q2,ff);
        double tau = -q2 / (4. * M_A * M_A);
        double GE  = ffs[0];
        double GM  = ffs[1];
        /// Compute W1Tilde and W2Tilde (see A6 of Stein et al)
        double W1 = tau * GM * GM;
        double W2 = (GE * GE + tau * GM * GM) / (1. + tau);
        // double W1Tilde = FTildeTerm*W1;                       // G in Mo & Tsai (see Stein for
        // Tilde) double W2Tilde = FTildeTerm*W2;                       // F in Mo & Tsai (see Stein
        // for Tilde)
        double G      = 4. * M_A * M_A * W1;
        double F      = 4. * W2;
        double GTilde = FTildeTerm * G;
        double FTilde = FTildeTerm * F;

        // rEs_int     = Es;
        // rEp_int     = Ep;
        // rQ2_int     = -q2;
        // rCOSTHK_int = COSTHK;
        // rOmega_int  = omega;
        // fDebugTree->Fill();

        // if(fDebug > 4){
        //   std::cout << "----------------- InternalIntegrand_MoTsai69 -----------------" <<
        //   std::endl; std::cout << "m          = " << m          << std::endl; std::cout << "M_A
        //   = " << M_A        << std::endl; std::cout << "Es         = " << Es         <<
        //   std::endl; std::cout << "Ep         = " << Ep         << std::endl; std::cout << "-q2
        //   = " << -q2        << std::endl; std::cout << "tau        = " << tau        <<
        //   std::endl; std::cout << "s_dot_p    = " << s_dot_p    << std::endl; std::cout << "u_0
        //   = " << u_0        << std::endl; std::cout << "uVect      = " << uVect      <<
        //   std::endl; std::cout << "sVect      = " << sVect      << std::endl; std::cout << "pVect
        //   = " << pVect      << std::endl; std::cout << "omega      = " << omega      <<
        //   std::endl; std::cout << "COSTHK     = " << COSTHK     << std::endl; std::cout <<
        //   "SINTHK     = " << SINTHK     << std::endl; std::cout << "COSTHP     = " << COSTHP <<
        //   std::endl; std::cout << "COSTHS     = " << COSTHS     << std::endl; std::cout << "a = "
        //   << a          << std::endl; std::cout << "apr        = " << apr        << std::endl;
        //   std::cout << "bpr        = " << bpr        << std::endl;
        //   std::cout << "v          = " << v          << std::endl;
        //   std::cout << "x          = " << x          << std::endl;
        //   std::cout << "y          = " << y          << std::endl;
        //   std::cout << "GE         = " << GE         << std::endl;
        //   std::cout << "GM         = " << GM         << std::endl;
        //   std::cout << "FTildeTerm = " << FTildeTerm << std::endl;
        //   std::cout << "G          = " << G          << std::endl;
        //   std::cout << "F          = " << F          << std::endl;
        //   std::cout << "GTilde     = " << GTilde     << std::endl;
        //   std::cout << "FTilde     = " << FTilde     << std::endl;
        //}

        // F term
        double FT1 = ((-2. * pi * a * m * m) / (x * x * x)) * (2. * Es * (Ep + omega) + 0.5 * q2);
        double FT2 = ((-2. * pi * apr * m * m) / (y * y * y)) * (2. * Ep * (Es - omega) + 0.5 * q2);
        double FT3 = -4. * pi;
        double FT4 = 4. * pi * ((v / x) - (vpr / y)) *
                     (m * m * (s_dot_p - omega * omega) +
                      s_dot_p * (2. * Es * Ep - s_dot_p + omega * (Es - Ep)));
        double FT5 =
            (2. * pi / x) * (2. * (Es * Ep + Es * omega + Ep * Ep) + 0.5 * q2 - s_dot_p - m * m);
        double FT6 = (-1.) * (2. * pi / y) *
                     (2. * (Es * Ep - Ep * omega + Es * Es) + 0.5 * q2 - s_dot_p - m * m);
        double FTerm = M_A * M_A * FTilde * (FT1 + FT2 + FT3 + FT4 + FT5 + FT6);
        // G term
        double GT1 = ((2. * pi * a) / (x * x * x) + (2. * pi * apr) / (y * y * y)) * (m * m) *
                     (2. * m * m + q2);
        double GT2   = 8. * pi;
        double GT3   = 8. * pi * ((v / x) - (vpr / y)) * s_dot_p * (s_dot_p - 2. * m * m);
        double GT4   = 2. * pi * ((1. / x) - (1. / y)) * (2. * s_dot_p + 2. * m * m - q2);
        double GTerm = GTilde * (GT1 + GT2 + GT3 + GT4);
        // Put it together
        double T   = omega / (2. * q2 * q2 * (u_0 - uVect * COSTHK));
        double arg = T * (FTerm + GTerm);
        return arg;
      }

    ///** Returns the radition length */
    //double rad_length(int Z, int A) {
    //  double res   = 1.0 / (716.408 * A);
    //  double alpha = 1.0 / 137.0;
    //  double Lrad, Lradprime;
    //  if (Z == 1) {
    //    Lrad      = 5.31;
    //    Lradprime = 6.144;
    //  } else if (Z == 2) {
    //    Lrad      = 4.79;
    //    Lradprime = 5.621;
    //  } else if (Z == 3) {
    //    Lrad      = 4.74;
    //    Lradprime = 5.805;
    //  } else if (Z == 4) {
    //    Lrad      = 4.71;
    //    Lradprime = 5.924;
    //  } else {
    //    Lrad      = TMath::Log(184.15 * TMath::Power(double(Z), -1.0 / 3.0));
    //    Lradprime = TMath::Log(1194.0 * TMath::Power(double(Z), -2.0 / 3.0));
    //  }
    //  res = res * (double(Z * Z) * (Lrad - f_rad_length(alpha * double(Z))) + double(Z) * Lradprime);
    //  return (1.0 / res);
    //}
} // namespace insane::radcor

