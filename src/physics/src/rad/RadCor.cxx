#include "insane/rad/RadCor.h"
#include "insane/base/SystemOfUnits.h"
#include "TMath.h"

using namespace insane::units;

namespace insane::rad {

  double f_rad_length(double a) {
    double res = 0;
    res        = a * a *
          (1.0 / (1.0 + a * a) + 0.20206 - 0.0369 * a * a + 0.0083 * a * a * a * a -
           0.002 * a * a * a * a * a * a);
    return (res);
  }

  /** Returns the radition length */
  double rad_length(int Z, int A) {
    double res   = 1.0 / (716.408 * A);
    double alpha = 1.0 / 137.0;
    double Lrad, Lradprime;
    if (Z == 1) {
      Lrad      = 5.31;
      Lradprime = 6.144;
    } else if (Z == 2) {
      Lrad      = 4.79;
      Lradprime = 5.621;
    } else if (Z == 3) {
      Lrad      = 4.74;
      Lradprime = 5.805;
    } else if (Z == 4) {
      Lrad      = 4.71;
      Lradprime = 5.924;
    } else {
      Lrad      = TMath::Log(184.15 * TMath::Power(float(Z), -1.0 / 3.0));
      Lradprime = TMath::Log(1194.0 * TMath::Power(float(Z), -2.0 / 3.0));
    }
    res = res * (float(Z * Z) * (Lrad - f_rad_length(alpha * float(Z))) + float(Z) * Lradprime);
    return (1.0 / res);
  }

} // namespace insane::rad
