#include "insane/rad/Bremsstrahlung.h"
#include "TMath.h"


namespace insane::brem {

    double I_gamma_1_approx(double t, double E0, double k) {
      // Bremsstrahlung bremsstrahlung spectrum.
      // Approximation formula
      // Tsai and Whitis, SLAC-PUB-184 1966 eqn.25
      using namespace TMath;
      double num = Power(1.0 - k / E0, 4.0 * t / 3.0) - Exp(-7.0 * t / 9.0);
      double den = 7.0 / 9.0 + (4.0 / 3.0) * Log(1.0 - k / E0);
      return (num / (k * den));
    }
    double I_gamma_1_sumterm(int n, double tprime, double u) {
      // sum over n term from below
      using namespace TMath;
      double num =
          ((4.0 / 3.0) * Power(-1.0, double(n)) - u * u) * Power(Log(1.0 / u), double(n + 1));
      double den = double(Factorial(n)) * (double(n) + 4.0 * tprime / 3.0 + 1.0);
      return (num / den);
    }
    double I_gamma_1_integrand(double tprime, double u) {
      // integrand used below
      using namespace TMath;
      double num    = Exp(7.0 * tprime / 9.0) * Power(Log(1.0 / u), 4.0 * tprime / 3.0);
      double den    = Gamma(4.0 * tprime / 3.0 + 1.0);
      double sum    = u;
      int    Nterms = 5;
      for (int n = 0; n < Nterms; n++) {
        double nterm = I_gamma_1_sumterm(n, tprime, u);
        sum += nterm;
        // std::cout << "n = " << n << "  " << nterm << std::endl;
      }
      return sum * num / den;
    }
    double I_gamma_1(double t, double E0, double k) {
      // Bremsstrahlung bremsstrahlung spectrum.
      // Tsai and Whitis, SLAC-PUB-184 1966 eqn.24
      // Exact formula (but with a finite sum. See eqn 24.)
      // simple integration
      // This result is still wrong. Cannot find bug.
      // using approximate instead.
      // return I_gamma_1_approx(t,E0,k);
      if (k >= E0)
        return 0.0;
      using namespace TMath;
      double u = k / E0;
      if (u < 0.0005)
        u = 0.0005; // for small u just fix the coefficient and let the spectrum go like 1/k
      int    Nint    = 20;
      double delta_t = t / double(Nint);
      double tprime  = 0.0;
      double tot     = 0.0;
      for (int i = 0; i < Nint; i++) {
        tprime = (double(i) + 0.5) * delta_t;
        tot += I_gamma_1_integrand(tprime, u) * delta_t;
      }
      return (Exp(-7.0 * t / 9.0) * tot / k);
    }

    double I_gamma_1_test(double t, double E0, double k) {
      // Bremsstrahlung bremsstrahlung spectrum.
      // Tsai and Whitis, SLAC-PUB-184 1966 eqn.24
      // Exact formula (but with a finite sum. See eqn 24.)
      // simple integration
      // This result is still wrong. Cannot find bug.
      if (k >= E0)
        return 0.0;
      using namespace TMath;
      double u = k / E0;
      if (u < 0.0005)
        u = 0.0005; // for small u just fix the coefficient and let the spectrum go like 1/k
      int    Nint    = 100;
      double delta_t = t / double(Nint);
      double tprime  = 0.0;
      double tot     = 0.0;
      for (int i = 0; i < Nint; i++) {
        tprime       = (double(i) + 0.5) * delta_t;
        double iterm = I_gamma_1_integrand(tprime, u) * delta_t;
        tot += iterm;
        // std::cout << "tprime = " << tprime << "  " << iterm << std::endl;
      }
      return (Exp(-7.0 * t / 9.0) * tot / k);
    }

    double N_gamma(double dOverX0, double kmin, double kmax, double E0) {
      // Number of photons from bremstrahlung beam.
      // Formula from Particle Data Group. Eq(27.29)-2012
      // If E0 is optional. By default it is set to kmax
      // dOverX0 is the target length (in units of radiation length over the radiation length)
      // E0 is the electron beam energy
      // kmax is the the maximum photon energy and kmin is the min.
      if (E0 < 0.0)
        E0 = kmax;
      if (kmin > kmax)
        return 0.0;
      if (kmin > E0)
        return 0.0;
      return (dOverX0 * ((4.0 / 3.0) * TMath::Log(kmax / kmin) - 4.0 * (kmax - kmin) / (3.0 * E0) +
                         (kmax * kmax - kmin * kmin) / (2 * E0 * E0)));
    }

    double I_gamma_1_k_avg(double t, double kmin, double E0) {
      // Average photon energy for a bremsstrahlung spectrum
      // E0 is the electron beam energy
      // kmin defines the range of photon energies to average over.
      if (E0 < 0.0)
        return 0.0;
      int    npoints = 50;
      double deltak  = (E0 - kmin) / ((double)npoints);
      double k       = 0.0;
      double num     = 0.0;
      for (int i = 0; i < npoints; i++) {
        k = kmin + ((double)i + 0.5) * deltak;
        num += k * I_gamma_1_approx(t, E0, k) * deltak;
        // std::cout << "deltak = " << deltak << std::endl;
        // std::cout << "k = " << k << std::endl;
        // std::cout << "num = " << num << std::endl;
      }
      return (num);
    }

    double Ebrem_avg(double t, double kmin, double E0) {
      // Average photon energy for a bremsstrahlung spectrum
      // E0 is the electron beam energy
      // kmin defines the range of photon energies to average over.
      if (E0 < 0.0)
        return 0.0;
      int    npoints = 5;
      double deltak  = (E0 - kmin) / ((double)npoints);
      double k       = 0.0;
      double num     = 0.0;
      double denom   = 0.0;
      for (int i = 0; i < npoints; i++) {
        k           = kmin + ((double)i + 0.5) * deltak;
        double Igam = I_gamma_1(t, E0, kmin);
        num += k * Igam * deltak;
        denom += Igam * deltak;
      }
      return (num / denom);
    }

    double Q_equiv_quanta(double t, double kmin, double E0) {
      // Number of equivalent quanta for a bremsstrahlung spectrum
      // E0 is the electron beam energy
      // kmin defines the range of photon energies to average over.
      return (Ebrem_avg(t, kmin, E0) / E0);
    }

    double k_min_photoproduction(double Ex, double thetax, double mx, double mt, double mn) {
      // Minimum photon energy for photoproduction in gamma+p -> x+n
      // where x is a hadron and n is a nucleon
      // Ex and thetax are the produced hadron's energy and angle
      // mx is the hadron mass, mt is the target mass, mn is the recoil nucleon mass.
      return ((mn * mn - mx * mx - mt * mt + 2.0 * mt * Ex) /
              (2.0 * (mt - Ex + TMath::Sqrt(Ex * Ex - mx * mx) * TMath::Cos(thetax))));
    }
}
