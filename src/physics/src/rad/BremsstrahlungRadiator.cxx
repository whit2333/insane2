#include "insane/base/BremsstrahlungRadiator.h"
#include "insane/base/Bremsstrahlung.h"
#include <iostream>

namespace insane {

//______________________________________________________________________________
BremsstrahlungRadiator::BremsstrahlungRadiator() 
   : fTotalRL(0.0)
{
   fRadiatorLengths.clear();
}
//______________________________________________________________________________
BremsstrahlungRadiator::~BremsstrahlungRadiator()
{ }
//______________________________________________________________________________
//BremsstrahlungRadiator::BremsstrahlungRadiator(const BremsstrahlungRadiator& v)
//{
//   std::cout << " BremsstrahlungRadiator::BremsstrahlungRadiator(const BremsstrahlungRadiator& v) " << std::endl;
//   fTotalRL         = v.fTotalRL ;
//   fRadiatorLengths = v.fRadiatorLengths ;
//}
////______________________________________________________________________________
//BremsstrahlungRadiator& BremsstrahlungRadiator::operator=(const BremsstrahlungRadiator& v)
//{
//   std::cout << "BremsstrahlungRadiator& BremsstrahlungRadiator::operator=(const BremsstrahlungRadiator& v)" << std::endl;
//   if (this == &v)      // Same object?
//      return *this;        // Yes, so skip assignment, and just return *this.
//   fTotalRL         = v.fTotalRL ;
//   fRadiatorLengths = v.fRadiatorLengths ;
//   fZPositions      = v.fZPositions;
//   fZPositions_map  = v.fZPositions_map;
//   fCumulativeRL    = v.fCumulativeRL;
//   return *this;
//}
//______________________________________________________________________________
void     BremsstrahlungRadiator::AddRadiator( TargetMaterial * mat )
{
   Double_t t     = mat->GetNumberOfRadiationLengths();
   Double_t z_pos = mat->fZposition;
   Int_t    id    = mat->GetMatID();//fRadiatorLengths.size();//mat->fMatID; 
   //mat->SetBremRadiator(this);

   auto search = fRadiatorLengths.find(id);
   if(search != fRadiatorLengths.end()) {
      std::cout << "Error: material with id=" << id << " already exists.\n";
      std::cout << "       Ignoring the following material :\n";
      mat->Print();
   } else {
      fRadiatorLengths[id] = t;
      fZPositions.push_back(std::make_pair(id,z_pos));

      fZPositions_map.insert(std::make_pair(z_pos,id));
   }

   std::sort(fZPositions.begin(), fZPositions.end(),
              [](const std::pair<int, double>& lhs, const std::pair<int, double>& rhs)
              { return lhs.second < rhs.second; } );

   // Since the zpositions are sorted create a map that can be quickly used for a given material
   // to get all the upstream materal (not including the indexed material)
   // If there is material placed at the same position it is undefiend! TODO fix this. 
   fCumulativeRL.clear();
   fTotalRL = 0.0;
   for(std::pair<int,double>& x: fZPositions) {

      // check that that the material ID is the first in the equal_range so that we can
      // loop over all of them just once. Repeated z postions then just add   
      if( x.first == (fZPositions_map.equal_range(x.second).first->second) ) {

         double temp = 0.0;
         std::multimap<double,int>::iterator it;
         for ( it = fZPositions_map.equal_range(x.second).first; 
               it!= fZPositions_map.equal_range(x.second).second;
               ++it)
         {
            temp += fRadiatorLengths[(*it).second]/2.0;
         }
         //std::cout << x.first << "  " << temp << std::endl;
         for ( it = fZPositions_map.equal_range(x.second).first; 
               it!= fZPositions_map.equal_range(x.second).second;
               ++it)
         {
            fCumulativeRL[(*it).second]    = fTotalRL +temp;
            //fRadiatorLengths[(*it).second] = temp*2.0; // This is OK because this radiation length is only used by the cross section for photo production.
         }
         
      }

      fTotalRL += fRadiatorLengths[x.first];
   }
}
//______________________________________________________________________________
Double_t BremsstrahlungRadiator::GetRadiatorLength( Int_t i )
{
   if( i < fRadiatorLengths.size() && i >= 0 ) {
      return fRadiatorLengths[i];
   }
   std::cerr << "Error BremsstrahlungRadiator::GetRadiatorLength(i = " 
             << i 
             << ") raditor does not exist.\n";
   return 0.0;
}
//______________________________________________________________________________
Double_t  BremsstrahlungRadiator::I_gamma( Int_t id, Double_t k_gamma, Double_t E0 ) const 
{
   // I_gamma returns the bremsstrahlung intensity at the
   // target material, with id, including all upstream radiators
   // at the photo energy k_gamma, 
   // for an initial electron beam energy E0

   Double_t thickness = fCumulativeRL.at(id);// +  fRadiatorLengths.at(id)/2.0;
   Double_t Igam      = insane::brem::I_gamma_1_approx( thickness,E0,k_gamma);

   return Igam;
}
//______________________________________________________________________________
void BremsstrahlungRadiator::Print(Option_t * opt) const
{
   std::cout << " Bremsstrahlung radiator has " << fZPositions.size() << " materials in the following order.\n";
   for(const std::pair<int,double>& x: fZPositions) {
      double rl1 = fRadiatorLengths.find(x.first)->second;
      double rl2 = fCumulativeRL.find(x.first)->second;
      std::cout << " - material(" << x.first << ") is at " << x.second 
                << " cm with  " 
                << rl1 //fRadiatorLengths[x.first] 
                << " r.l. and " 
                << rl2//fCumulativeRL[x.first] 
                << " r.l. upstream.\n";
   }
   std::cout 
      << "Note, upstream is the amout of radiator upstream from the material \n"
      << "which contributes to the photon flux seen. The first material sees \n"
      << "only 1/2 of its own r.l.\n";
}
//______________________________________________________________________________

void BremsstrahlungRadiator::Print(std::ostream &stream) const 
{
   stream << " Bremsstrahlung radiator has " << fZPositions.size() << " materials in the following order.\n";
   for(const std::pair<int,double>& x: fZPositions) {
      double rl1 = fRadiatorLengths.find(x.first)->second;
      double rl2 = fCumulativeRL.find(x.first)->second;
      stream << " - material(" << x.first << ") is at " << x.second 
                << " cm with  " 
                << rl1 //fRadiatorLengths[x.first] 
                << " r.l. and " 
                << rl2//fCumulativeRL[x.first] 
                << " r.l. upstream.\n";
   }
   stream 
      << "Note, upstream is the amout of radiator upstream from the material \n"
      << "which contributes to the photon flux seen. The first material sees \n"
      << "only 1/2 of its own r.l.\n";
}
//______________________________________________________________________________

}
