#include "insane/tmds/FragmentationFunctions.h"

namespace insane::physics {

  FragmentationFunctions::FragmentationFunctions() {}

  FragmentationFunctions::~FragmentationFunctions() {}

  double FragmentationFunctions::D1(Parton q, Hadron h, double z, double pt, double Q2) const {
    if (h == Hadron::pi_plus) {
      return D1_pi_plus(q, z, pt, Q2);
    } else if (h == Hadron::pi_minus) {
      return D1_pi_minus(q, z, pt, Q2);
    } else if (h == Hadron::K_plus) {
      return D1_K_plus(q, z, pt, Q2);
    } else if (h == Hadron::K_minus) {
      return D1_K_minus(q, z, pt, Q2);
    }
    return 0.0;
  }
  double FragmentationFunctions::D(Parton q, Hadron h, double z, double Q2) const {
    if (h == Hadron::pi_plus) {
      return D_pi_plus(q, z, Q2);
    } else if (h == Hadron::pi_minus) {
      return D_pi_minus(q, z, Q2);
    } else if (h == Hadron::K_plus) {
      return D_K_plus(q, z, Q2);
    } else if (h == Hadron::K_minus) {
      return D_K_minus(q, z, Q2);
    }
    return 0.0;
  }

  double FragmentationFunctions::D1_pi_plus(Parton q, double z, double pt, double Q2) const {
    switch (q) {
    case Parton::u:
      return D1_u_pi_plus(z, pt, Q2);
      break;
    case Parton::d:
      return D1_d_pi_plus(z, pt, Q2);
      break;
    case Parton::ubar:
      return D1_ubar_pi_plus(z, pt, Q2);
      break;
    case Parton::dbar:
      return D1_dbar_pi_plus(z, pt, Q2);
      break;
    default:
      return 0.0;
      break;
    }
    return 0.0;
  }
  double FragmentationFunctions::D1_pi_minus(Parton q, double z, double pt, double Q2) const {
    switch (q) {
    case Parton::u:
      return D1_u_pi_minus(z, pt, Q2);
      break;
    case Parton::d:
      return D1_d_pi_minus(z, pt, Q2);
      break;
    case Parton::ubar:
      return D1_ubar_pi_minus(z, pt, Q2);
      break;
    case Parton::dbar:
      return D1_dbar_pi_minus(z, pt, Q2);
      break;
    default:
      return 0.0;
      break;
    }
    return 0.0;
  }
  double FragmentationFunctions::D_pi_plus(Parton q, double z,double Q2) const {
    switch (q) {
    case Parton::u:
      return D_u_piplus(z,  Q2);
      break;
    case Parton::d:
      return D_d_piplus(z,  Q2);
      break;
    case Parton::ubar:
      return D_ubar_piplus(z,  Q2);
      break;
    case Parton::dbar:
      return D_dbar_piplus(z,  Q2);
      break;
    default:
      return 0.0;
      break;
    }
    return 0.0;
  }
  double FragmentationFunctions::D_pi_minus(Parton q, double z,  double Q2) const {
    switch (q) {
    case Parton::u:
      return D_u_piminus(z, Q2);
      break;
    case Parton::d:
      return D_d_piminus(z, Q2);
      break;
    case Parton::ubar:
      return D_ubar_piminus(z, Q2);
      break;
    case Parton::dbar:
      return D_dbar_piminus(z, Q2);
      break;
    default:
      return 0.0;
      break;
    }
    return 0.0;
  }

  double FragmentationFunctions::D1_K_plus(Parton q, double z, double pt, double Q2) const {
    switch (q) {
    case Parton::u:
      return D1_u_K_plus(z, pt, Q2);
      break;
    case Parton::d:
      return D1_d_K_plus(z, pt, Q2);
      break;
    case Parton::ubar:
      return D1_ubar_K_plus(z, pt, Q2);
      break;
    case Parton::dbar:
      return D1_dbar_K_plus(z, pt, Q2);
      break;
    default:
      return 0.0;
      break;
    }
    return 0.0;
  }

  double FragmentationFunctions::D1_K_minus(Parton q, double z, double pt, double Q2) const {
    switch (q) {
    case Parton::u:
      return D1_u_K_minus(z, pt, Q2);
      break;
    case Parton::d:
      return D1_d_K_minus(z, pt, Q2);
      break;
    case Parton::ubar:
      return D1_ubar_K_minus(z, pt, Q2);
      break;
    case Parton::dbar:
      return D1_dbar_K_minus(z, pt, Q2);
      break;
    default:
      return 0.0;
      break;
    }
    return 0.0;
  }
  double FragmentationFunctions::D_K_plus(Parton q, double z, double Q2) const {
    switch (q) {
    case Parton::u:
      return D_u_Kplus(z,  Q2);
      break;
    case Parton::d:
      return D_d_Kplus(z,  Q2);
      break;
    case Parton::ubar:
      return D_ubar_Kplus(z,  Q2);
      break;
    case Parton::dbar:
      return D_dbar_Kplus(z,  Q2);
      break;
    default:
      return 0.0;
      break;
    }
    return 0.0;
  }

  double FragmentationFunctions::D_K_minus(Parton q, double z, double Q2) const {
    switch (q) {
    case Parton::u:
      return D_u_Kminus(z,  Q2);
      break;
    case Parton::d:
      return D_d_Kminus(z,  Q2);
      break;
    case Parton::ubar:
      return D_ubar_Kminus(z,  Q2);
      break;
    case Parton::dbar:
      return D_dbar_Kminus(z,  Q2);
      break;
    default:
      return 0.0;
      break;
    }
    return 0.0;
  }

  double FragmentationFunctions::D_u_pi0(double z, double Q2) const {
    double Dplus  = D_u_piplus(z, Q2);
    double Dminus = D_u_piminus(z, Q2);
    return ((Dplus + Dminus) / 2.0);
  }
  //______________________________________________________________________________
  double FragmentationFunctions::D_ubar_pi0(double z, double Q2) const {
    double Dplus  = D_ubar_piplus(z, Q2);
    double Dminus = D_ubar_piminus(z, Q2);
    return ((Dplus + Dminus) / 2.0);
  }
  //______________________________________________________________________________
  double FragmentationFunctions::D_d_pi0(double z, double Q2) const {
    double Dplus  = D_d_piplus(z, Q2);
    double Dminus = D_d_piminus(z, Q2);
    return ((Dplus + Dminus) / 2.0);
  }
  //______________________________________________________________________________
  double FragmentationFunctions::D_dbar_pi0(double z, double Q2) const {
    double Dplus  = D_dbar_piplus(z, Q2);
    double Dminus = D_dbar_piminus(z, Q2);
    return ((Dplus + Dminus) / 2.0);
  }
  //______________________________________________________________________________
  double FragmentationFunctions::D_s_pi0(double z, double Q2) const {
    double Dplus  = D_s_piplus(z, Q2);
    double Dminus = D_s_piminus(z, Q2);
    return ((Dplus + Dminus) / 2.0);
  }
  //______________________________________________________________________________
  double FragmentationFunctions::D_sbar_pi0(double z, double Q2) const {
    double Dplus  = D_sbar_piplus(z, Q2);
    double Dminus = D_sbar_piminus(z, Q2);
    return ((Dplus + Dminus) / 2.0);
  }
  //______________________________________________________________________________
} // namespace insane::physics
