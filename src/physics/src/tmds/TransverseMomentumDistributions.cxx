#include "insane/tmds/TransverseMomentumDistributions.h"

namespace insane::physics {

  TransverseMomentumDistributions::TransverseMomentumDistributions() {}

  TransverseMomentumDistributions::~TransverseMomentumDistributions() {}

  double TransverseMomentumDistributions::f1(Nuclei target, Parton q, double x, double kt,
                                             double Q2) const {
    switch (target) {
    case Nuclei::p:
      return f1_p(q, x, kt, Q2);
    case Nuclei::n:
      return f1_p(q, x, kt, Q2);
    default:
      return 0.0;
    }
    return 0.0;
  }

  double TransverseMomentumDistributions::f1_p(Parton q, double x, double kt, double Q2) const {
    switch (q) {
    case Parton::u:
      return f1_u(x, kt, Q2);
    case Parton::d:
      return f1_d(x, kt, Q2);
    case Parton::ubar:
      return f1_ubar(x, kt, Q2);
    case Parton::dbar:
      return f1_dbar(x, kt, Q2);
    default:
      return 0.0;
    }
    return 0.0;
  }

  double TransverseMomentumDistributions::f1_n(Parton q, double x, double kt, double Q2) const {
    switch (q) {
    case Parton::u:
      // use charge symmetry: u_n = d_p
      return f1_d(x, kt, Q2);
    case Parton::d:
      return f1_u(x, kt, Q2);
    case Parton::ubar:
      return f1_dbar(x, kt, Q2);
    case Parton::dbar:
      return f1_ubar(x, kt, Q2);
    default:
      return 0.0;
    }
    return 0.0;
  }
} // namespace insane::physics
