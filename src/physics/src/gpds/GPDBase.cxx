#include "insane/gpds/GPDBase.h"

namespace insane {
  namespace physics {

    GPDBase::GPDBase()
    { }
    //__________________________________________________________________________

    GPDBase::~GPDBase()
    { }
    //__________________________________________________________________________

    void GPDBase::Reset()
    {
      for(int i = 0; i<NPartons ; i++) {
        fValues[i]        = 0.0;
        fUncertainties[i] = 0.0;
      }
      f_Q2 = 0.0;
    }
    //______________________________________________________________________________

    bool GPDBase::IsComputed(const GPD_vars& x, double Q2) const
    {
      //if( x  != fXbjorken ) return false;
      //if( Q2 != fQsquared ) return false;
      //return true;
      return false;
    }
    //______________________________________________________________________________

    double  GPDBase::Get(const Parton& f, const GPD_vars& x, double Q2) const 
    {
      if( !IsComputed(x, Q2) ) {
        Calculate(x,Q2);
        Uncertainties(x,Q2);
      }
      return( fValues[int(f)] );
    }
    //______________________________________________________________________________
    
    double  GPDBase::Get(Parton f, const GPD_vars& x, double Q2) const 
    {
      if( !IsComputed(x, Q2) ) {
        Calculate(x,Q2);
        Uncertainties(x,Q2);
      }
      return fValues[static_cast<unsigned int>(f)];
    }
    //______________________________________________________________________________
    
    double  GPDBase::Get(const Parton& f) const 
    {
      return( fValues[int(f)] );
    }
    //______________________________________________________________________________
    
    double  GPDBase::Get(Parton f) const 
    {
      return fValues[static_cast<unsigned int>(f)];
    }
    //______________________________________________________________________________
    
    const std::array<double,NPartons>& GPDBase::Calculate(const GPD_vars& x, double Q2) const
    {
      // do calculations
      return fValues;
    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& GPDBase::Uncertainties(const GPD_vars& x, double Q2) const 
    {
      // do calculations
      return fUncertainties;
    }

  }
}


