#include "insane/gpds/GeneralizedPartonDistributions.h"

namespace insane {
  namespace physics {

    double GeneralizedPartonDistributions::H(int i, double x, double xi, double t, double Q2) const
    { return 0.0;}

      double GeneralizedPartonDistributions::E(int i, double x, double xi, double t, double Q2) const
      { return 0.0;}

      double GeneralizedPartonDistributions::H_tilde(int i, double x, double xi, double t, double Q2) const
      { return 0.0;}

      double GeneralizedPartonDistributions::E_tilde(int i, double x, double xi, double t, double Q2) const
      { return 0.0;}

      double GeneralizedPartonDistributions::H_u(double x, double xi, double t, double Q2) const
      { return 0.0;}
      double GeneralizedPartonDistributions::H_d(double x, double xi, double t, double Q2) const
      { return 0.0;}
      double GeneralizedPartonDistributions::H_ubar(double x, double xi, double t, double Q2) const
      { return 0.0;}
      double GeneralizedPartonDistributions::H_dbar(double x, double xi, double t, double Q2) const
      { return 0.0;}

      double GeneralizedPartonDistributions::Htilde_u(double x, double xi, double t, double Q2) const
      { return 0.0;}
      double GeneralizedPartonDistributions::Htilde_d(double x, double xi, double t, double Q2) const
      { return 0.0;}
      double GeneralizedPartonDistributions::Htilde_ubar(double x, double xi, double t, double Q2) const
      { return 0.0;}
      double GeneralizedPartonDistributions::Htilde_dbar(double x, double xi, double t, double Q2) const
      { return 0.0;}

      double GeneralizedPartonDistributions::H_g(   double x, double xi, double t, double Q2) const 
      { return 0.0;}
      double GeneralizedPartonDistributions::Htilde_g(   double x, double xi, double t, double Q2) const 
      { return 0.0;}

   }
}

