#include "insane/formfactors/BilenkayaFormFactors.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
BilenkayaFormFactors::BilenkayaFormFactors(){
        /// Electric constants 
	fa_e = 1.2742; 
	fb_e = 0.6394; 
	fc_e = 0.2742; 
	fd_e = 1.5820; 
        /// Magnetic constants 
        fa_m = 1.3262; 
        fb_m = 0.6397; 
        fc_m = 0.3262;  
        fd_m = 1.3137; 
}
//______________________________________________________________________________
BilenkayaFormFactors::~BilenkayaFormFactors(){

}
//______________________________________________________________________________
Double_t BilenkayaFormFactors::GEp(Double_t Q2){

	Double_t L1 = TMath::Power(fb_e,2.0); 
	Double_t T1 = fa_e/(1. + Q2/L1); 
        Double_t L2 = TMath::Power(fd_e,2.0); 
	Double_t T2 = fc_e/(1. + Q2/L2);
        Double_t GE = T1 - T2; 
        return GE;  

}
//______________________________________________________________________________
Double_t BilenkayaFormFactors::GMp(Double_t Q2){

	Double_t L1 = TMath::Power(fb_m,2.0); 
	Double_t T1 = fa_m/(1. + Q2/L1); 
        Double_t L2 = TMath::Power(fd_m,2.0); 
	Double_t T2 = fc_m/(1. + Q2/L2);
        Double_t GM = Mu_p*(T1 - T2); 
        return GM;  

}
}}
