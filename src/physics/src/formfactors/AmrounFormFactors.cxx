#include "insane/formfactors/AmrounFormFactors.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
AmrounFormFactors::AmrounFormFactors(){

        /// switch 
        fIsFortran=false; 
        /// A and Z 
        fA = 3.0; 
        fZ = 2.0; 
        /// Generic R term 
        fR[0]  = 0.1,
        fR[1]  = 0.5,
        fR[2]  = 0.9;  
        fR[3]  = 1.3;
        fR[4]  = 1.6;
        fR[5]  = 2.0;
        fR[6]  = 2.4;
        fR[7]  = 2.9;
        fR[8]  = 3.4;
        fR[9]  = 4.0;
        fR[10] = 4.6;
        fR[11] = 5.2;
        /// Electric terms 
        fQCh[0]  =  0.027614; 
        fQCh[1]  =  0.170847;
        fQCh[2]  =  0.219805;
        fQCh[3]  =  0.170486;
        fQCh[4]  =  0.134453;
        fQCh[5]  =  0.100953;
        fQCh[6]  =  0.074310;
        fQCh[7]  =  0.053970;
        fQCh[8]  =  0.023689;
        fQCh[9]  =  0.017502;
        fQCh[10] =  0.002034;
        fQCh[11] =  0.004338;
        /// Magnetic terms 
        fQM[0]  = 0.059785; 
        fQM[1]  = 0.138368;
        fQM[2]  = 0.281326;
        fQM[3]  = 0.000037;
        fQM[4]  = 0.289808;
        fQM[5]  = 0.019056;
        fQM[6]  = 0.114825;
        fQM[7]  = 0.042296;
        fQM[8]  = 0.028345;
        fQM[9]  = 0.018312;
        fQM[10] = 0.007843;
        fQM[11] = 0.000000;
}
//______________________________________________________________________________
AmrounFormFactors::~AmrounFormFactors(){

}
//______________________________________________________________________________
Double_t AmrounFormFactors::GEHe3(Double_t Q2){
        Double_t qsq = Q2/(hbarc_gev_fm*hbarc_gev_fm); 
        auto q2    = (double)qsq; 
        Double_t FF=0,FCH=0;
	if(fIsFortran){
		FCH = Double_t( formc_(&q2) ); 
	}else{
		FCH = GetFormFactor(0,Q2);
	}
        FF = fZ*FCH; 
	return FF;
}
//______________________________________________________________________________
Double_t AmrounFormFactors::GMHe3(Double_t Q2){
        Double_t qsq = Q2/(hbarc_gev_fm*hbarc_gev_fm); 
        auto q2    = (double)qsq; 
        Double_t FF=0,FMAG=0;
        if(fIsFortran){
		FMAG = Double_t( formm_(&q2) ); 
	}else{
		FMAG = GetFormFactor(1,Q2);
	}
        FF = fZ*(1.+Kappa_He3)*FMAG; 
	return FF;
}
//______________________________________________________________________________
Double_t AmrounFormFactors::GetFormFactor(Int_t Type,Double_t Q2){

        Double_t gam  = 0.65;    
        // Double_t gam  = 0.653197;                         // same as below     
        // Double_t gam  = TMath::Sqrt(2./3.)*0.8;           // in fm 
        Double_t gam2 = gam*gam;
        Double_t q2   = Q2/(hbarc_gev_fm*hbarc_gev_fm);   // in fm^-2     
        Double_t q    = TMath::Sqrt(q2);                  // in fm^-1           
        Double_t T1   = TMath::Exp(-q2*gam2/4.0);
        Double_t R=0,Q=0,sum=0,num=0,den=0,T2=0,T3=0;
        Int_t N = 12;
        for(Int_t i=0;i<N;i++){
		R    = fR[i];
		if(Type==0){
			Q    = fQCh[i];
		}else if(Type==1){
			Q    = fQM[i];
                }else{
			std::cout << "[AmrounFormFactors::GetFormFactor]: Error!  ";
                        std::cout << "Invalid FF type!  Exiting..." << std::endl;
			exit(1);
                }
                num  = Q;
                den  = 1. + 2.*R*R/gam2;
                T2   = num/den;
                if(q*R!=0){
			T3 = TMath::Cos(q*R) + (2.*R*R/gam2)*( TMath::Sin(q*R)/(q*R) );
		}else{
			T3 = 1. + 2.*R*R/gam2; 
		}
                sum += T2*T3;
        }
        Double_t F = T1*sum;
        return F;

}

}}
