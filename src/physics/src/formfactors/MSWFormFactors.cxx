#include "insane/formfactors/MSWFormFactors.h"
namespace insane {
namespace physics {
//______________________________________________________________________________
MSWFormFactors::MSWFormFactors(){

	fA   = 3.0;
	fZ   = 2.0; 
	ff_0 = 0;
	ff_m = 0;
	fd_f = 0;

}
//______________________________________________________________________________
MSWFormFactors::~MSWFormFactors(){

}
//______________________________________________________________________________
void MSWFormFactors::CalculateFunctions(Double_t Q2){

	// Q2_fm = Q2 in fm 
	Double_t Q2_fm = Q2/TMath::Power(hbarc_gev_fm,2.0); 
        Double_t q     = TMath::Sqrt(Q2_fm); 
        // Parameters 
	Double_t a_c =  0.675;
	Double_t b_c =  0.366; 
	Double_t c_c =  0.836; 
	Double_t d_c = -6.78E-3; 
	Double_t a_m =  0.654;
	Double_t b_m =  0.456; 
	Double_t c_m =  0.821; 
	Double_t p   =  0.900;
	Double_t q0  =  3.980; 

        ff_0 = TMath::Exp(-a_c*a_c*q*q) - (b_c*b_c*q*q)*TMath::Exp(-c_c*c_c*q*q);
        ff_m = TMath::Exp(-a_m*a_m*q*q) - (b_m*b_m*q*q)*TMath::Exp(-c_m*c_m*q*q); 
        fd_f = d_c*TMath::Exp( -TMath::Power((q-q0)/p,2.0) );

        if(Q2<0){
		ff_0 = 0;
		ff_m = 0;
		fd_f = 0;
        }

}
//______________________________________________________________________________
Double_t MSWFormFactors::GEHe3(Double_t Q2){

	CalculateFunctions(Q2); 
	Double_t GE = fZ*(ff_0 + fd_f); 
	return GE; 

}
//______________________________________________________________________________
Double_t MSWFormFactors::GMHe3(Double_t Q2){

	CalculateFunctions(Q2); 
	Double_t GM = ff_m*fA*(-2.13); 
	return GM; 

}
}}
