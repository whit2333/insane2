#include "insane/formfactors/F1F209QuasiElasticFormFactors.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
F1F209QuasiElasticFormFactors::F1F209QuasiElasticFormFactors(){
      // Fit parameters 
      // Reference: arXiv: 1203.2262v2, Table III 
      fP[0]  =  5.1377e-03;    
      fP[1]  =  9.8071e-01;    
      fP[2]  =  4.6379e-02;    
      fP[3]  =  1.6433e+00; 
      fP[4]  =  6.9826e+00;   
      fP[5]  = -2.2655e-01;    
      fP[6]  =  1.1095e-01;    
      fP[7]  =  2.7945e-02; 
      fP[8]  =  4.0643e-01;    
      fP[9]  =  1.6076e+00;   
      fP[10] = -7.5460e+00;    
      fP[11] =  4.4418e+00; 
      fP[12] = -3.7464e-01;    
      fP[13] =  1.0414e-01;   
      fP[14] = -2.6852e-01;    
      fP[15] =  9.6653e-01; 
      fP[16] = -1.9055e+00;    
      fP[17] =  9.8965e-01;    
      fP[18] =  2.0613e+02;   
      fP[19] = -4.5536e-02; 
      fP[20] =  2.4902e-01;   
      fP[21] = -1.3728e-01;     
      fP[22] =  2.9201e+01;    
      fP[23] =  4.9280e-03;  
}
//______________________________________________________________________________
F1F209QuasiElasticFormFactors::~F1F209QuasiElasticFormFactors(){

}
//______________________________________________________________________________
void F1F209QuasiElasticFormFactors::SetParameters(Int_t A){
   if(A==2){
      fKF = 0.085;
      fES = 0.0022;
   }else if(A==3){
      fKF = 0.115;
      fES = 0.001;
   }else if( (A>3)&&(A<8) ){
      fKF = 0.190;
      fES = 0.017;
   }else if( (A>7)&&(A<17) ){
      fKF = 0.228;
      fES = 0.0165;
   }else if( (A>16)&&(A<26) ){
      fKF = 0.230;
      fES = 0.023;
   }else if( (A>25)&&(A<39) ){
      fKF = 0.236;
      fES = 0.018;
   }else if( (A>38)&&(A<56) ){
      fKF = 0.241;
      fES = 0.028;
   }else if( (A>55)&&(A<61) ){
      fKF = 0.241;
      fES = 0.023;
   }else if(A>60){
      fKF = 0.245;
      fES = 0.018;
   }
}
//______________________________________________________________________________
Double_t F1F209QuasiElasticFormFactors::Beta(Double_t x,Double_t Q2){
   Double_t M      = M_p/GeV;
   Double_t nu     = Q2/(2.*M*x);
   Double_t qVect  = TMath::Sqrt(nu*nu + Q2);
   Double_t kappa  = qVect/(2.*M);
   Double_t eta_F  = fKF/M;
   Double_t xi_F   = TMath::Sqrt(1. + eta_F*eta_F) - 1.;
   Double_t psi    = Psi(x,Q2,0.);
   Double_t beta   = 2.*kappa*( 1. + 0.5*xi_F*(1. + psi*psi) ); 
   return beta; 
}
//______________________________________________________________________________
Double_t F1F209QuasiElasticFormFactors::Delta(Double_t x,Double_t Q2){
   Double_t M      = M_p/GeV;
   Double_t nu     = Q2/(2.*M*x);
   Double_t qVect  = TMath::Sqrt(nu*nu + Q2);
   Double_t kappa  = qVect/(2.*M);
   Double_t tau    = Q2/(4.*M*M);
   Double_t eta_F  = fKF/M;
   Double_t xi_F   = TMath::Sqrt(1. + eta_F*eta_F) - 1.;
   Double_t psi    = Psi(x,Q2,0.);
   Double_t T1     = xi_F*(1.-psi*psi);
   Double_t T2     = TMath::Sqrt( tau*(1.+tau) )/kappa;
   Double_t T3     = (1./3.)*xi_F*(1.-psi*psi)*( tau/(kappa*kappa) );
   Double_t delta  = T1*(T2+T3);
   return delta;
}
//______________________________________________________________________________
Double_t F1F209QuasiElasticFormFactors::F_y(Double_t x,Double_t Q2){
   // see Bosted and Mamyan, arXiv: 1203.2262v2, Eq 11
   // First three terms 
   Double_t psi_pr = Psi(x,Q2,fES);
   Double_t T1     = 1.5576/fKF;
   Double_t T2     = 1. + TMath::Power(1.7720*(psi_pr+ 0.3014),2.);
   Double_t T3     = 1. + TMath::Exp(-2.4291*psi_pr);
   // put it together  
   Double_t sf     = T1/T2/T3;
   return sf;
}
//______________________________________________________________________________
Double_t F1F209QuasiElasticFormFactors::Psi(Double_t x,Double_t Q2,Double_t Es){
   // For psi', use Es != 0 
   Double_t M      = M_p/GeV;
   Double_t nu     = Q2/(2.*M*x);
   Double_t qVect  = TMath::Sqrt(nu*nu + Q2);
   Double_t kappa  = qVect/(2.*M);
   Double_t lambda = (nu-Es)/(2.*M);
   Double_t tau    = kappa*kappa - lambda*lambda;
   Double_t eta_F  = fKF/M;
   Double_t xi_F   = TMath::Sqrt(1. + eta_F*eta_F) - 1.;
   // First term  
   Double_t T1     = 1./TMath::Sqrt(xi_F);
   // Second term 
   Double_t num    = lambda - tau;
   Double_t den    = TMath::Sqrt( (1.+lambda)*tau + kappa*TMath::Sqrt( tau*(1.+tau) ) );
   Double_t T2     = num/den;
   // Put it together  
   Double_t psi    = T1*T2;
   return psi;
}
//______________________________________________________________________________
Double_t F1F209QuasiElasticFormFactors::GE(Int_t A,Double_t x,Double_t Q2,Double_t F1,Double_t F2){
   SetParameters(A);
   Double_t M     = M_p/GeV; 
   Double_t tau   = Q2/(4.*M*M); 
   Double_t Fy    = F_y(x,Q2); 
   Double_t delta = Delta(x,Q2); 
   Double_t beta  = Beta(x,Q2);  
   Double_t nu    = Q2/(2.*M*x);
   Double_t qVect = TMath::Sqrt(nu*nu + Q2);
   Double_t W2    = M*M + Q2*( (1./x) - 1. ); 
   Double_t y     = (W2 - M*M)/qVect; 
   Double_t kappa = qVect/(2.*M);
   Double_t nuL   = TMath::Power(tau/(kappa*kappa),2.); 
   Double_t nuT   = tau/(2.*kappa*kappa);  // see note in F1F209 fortran code! 
   Double_t S     = 1. + fP[7] + fP[8] + fP[9]*y*y + fP[10]*y*y*y + fP[11]*y*y*y*y;  
   Double_t a     = S*(0.5*M*Fy/beta)*( delta/(1.+tau) ); 
   Double_t b     = S*(0.5*M*Fy/beta)*(2.*tau + delta*tau/(1.+tau) );
   Double_t c     = S*( nu*Fy*nuL*kappa*kappa/(tau*beta) )*(1. + delta/(1.+tau) ) + nu*Fy*(nuT/beta)*(delta/(1.+tau)); 
   Double_t d     = S*( nu*Fy*nuL*kappa*kappa/(tau*beta) )*(delta*tau/(1.+tau) ) + nu*Fy*(nuT/beta)*(2.*tau + delta*tau/(1.+tau) ); 
   if(F1==0 || nu==0 ){
      F2 = 0.;  
   }
   Double_t arg   = (d*F1 - b*F2)/(a*d - b*c);
   Double_t ge    = 0;
   if(arg>=0) ge = TMath::Sqrt(arg);  
   return ge; 
}
//______________________________________________________________________________
Double_t F1F209QuasiElasticFormFactors::GM(Int_t A,Double_t x,Double_t Q2,Double_t F1,Double_t F2){
   SetParameters(A);
   Double_t M     = M_p/GeV; 
   Double_t tau   = Q2/(4.*M*M); 
   Double_t Fy    = F_y(x,Q2); 
   Double_t delta = Delta(x,Q2); 
   Double_t beta  = Beta(x,Q2);  
   Double_t nu    = Q2/(2.*M*x);
   Double_t qVect = TMath::Sqrt(nu*nu + Q2);
   Double_t W2    = M*M + Q2*( (1./x) - 1. ); 
   Double_t y     = (W2 - M*M)/qVect; 
   Double_t kappa = qVect/(2.*M);
   Double_t nuL   = TMath::Power(tau/(kappa*kappa),2.); 
   Double_t nuT   = tau/(2.*kappa*kappa);  // see note in F1F209 fortran code!  
   Double_t S     = 1. + fP[7] + fP[8] + fP[9]*y*y + fP[10]*y*y*y + fP[11]*y*y*y*y;  
   Double_t a     = S*(0.5*M*Fy/beta)*( delta/(1.+tau) ); 
   Double_t b     = S*(0.5*M*Fy/beta)*(2.*tau + delta*tau/(1.+tau) );
   Double_t c     = S*( nu*Fy*nuL*kappa*kappa/(tau*beta) )*(1. + delta/(1.+tau) ) + nu*Fy*(nuT/beta)*(delta/(1.+tau)); 
   Double_t d     = S*( nu*Fy*nuL*kappa*kappa/(tau*beta) )*(delta*tau/(1.+tau) ) + nu*Fy*(nuT/beta)*(2.*tau + delta*tau/(1.+tau) ); 
   if(F1==0 || nu==0 ){
      F2 = 0.;  
   }
   Double_t arg   = (-c*F1 + a*F2)/(a*d - b*c);
   Double_t gm    = 0.;
   if(arg>=0) gm = TMath::Sqrt(arg); 
   return gm; 
}
//______________________________________________________________________________
Double_t F1F209QuasiElasticFormFactors::GE(Double_t x,Double_t Q2,Double_t F1,Double_t F2){
   Double_t M   = M_p/GeV;                
   Double_t tau = Q2/(4.*M*M);             
   Double_t T1  = -1./M; 
   Double_t T2  = 2.*M*x*(1. + tau)/Q2;   // uses nu = Q2/(2*M*x)  
   // Double_t T2  = 2.*M*(1. + tau)/Q2;     // uses nu = Q2/(2M) [QE limit, W = M] 
   Double_t arg = TMath::Abs(T2*F2 + T1*F1);   
   Double_t ge  = TMath::Sqrt(arg); 
   return ge;
}
//______________________________________________________________________________
Double_t F1F209QuasiElasticFormFactors::GM(Double_t x,Double_t Q2,Double_t F1,Double_t F2){
   Double_t M   = M_p/GeV;                 
   Double_t tau = Q2/(4.*M*M);            
   Double_t arg = F1/(tau*M); 
   Double_t gm  = TMath::Sqrt(arg); 
   return gm;  
}
//______________________________________________________________________________
Double_t F1F209QuasiElasticFormFactors::GEdQE(Double_t x,Double_t Q2){
   Double_t F1 = fSF.F1d(x,Q2); 
   Double_t F2 = fSF.F2d(x,Q2); 
   Double_t ge = GE(2,x,Q2,F1,F2);
   return ge;   
}
//______________________________________________________________________________
Double_t F1F209QuasiElasticFormFactors::GMdQE(Double_t x,Double_t Q2){
   Double_t F1 = fSF.F1d(x,Q2); 
   Double_t F2 = fSF.F2d(x,Q2); 
   Double_t gm = GM(2,x,Q2,F1,F2);
   return gm;   
}
//______________________________________________________________________________
Double_t F1F209QuasiElasticFormFactors::GEHe3QE(Double_t x,Double_t Q2){
   Double_t F1 = fSF.F1He3(x,Q2); 
   Double_t F2 = fSF.F2He3(x,Q2); 
   // Double_t ge = GE(3,x,Q2,F1,F2);
   Double_t ge = GE(x,Q2,F1,F2);
   return ge;   
}
//______________________________________________________________________________
Double_t F1F209QuasiElasticFormFactors::GMHe3QE(Double_t x,Double_t Q2){
   Double_t F1 = fSF.F1He3(x,Q2); 
   Double_t F2 = fSF.F2He3(x,Q2); 
   // Double_t gm = GM(3,x,Q2,F1,F2);
   Double_t gm = GM(x,Q2,F1,F2);
   return gm;   
}
}}
