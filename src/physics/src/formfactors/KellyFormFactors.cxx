#include "insane/formfactors/KellyFormFactors.h"

//______________________________________________________________________________
namespace insane {
namespace physics {

double KellyFormFactors::G(int type,double Q2){
   int n = 1;
   double num=0,den=0;
   double M   = M_p/GeV; 
   double tau = Q2/(4.*M*M);
   // for the numerator 
   for(int k=0;k<=n;k++){
      if(type==0){
         // GEp 
         num += fa_gep[k]*TMath::Power(tau,k);
      }else if(type==1){
         // GMp 
         num += fa_gmp[k]*TMath::Power(tau,k); 
      }else if(type==2){
         // GMn 
         num += fa_gmn[k]*TMath::Power(tau,k); 
      }
   } 
   // for the denominator  
   for(int k=1;k<=n+2;k++){
      if(type==0){
         // GEp 
         den += fb_gep[k]*TMath::Power(tau,k);
      }else if(type==1){
         // GMp 
         den += fb_gmp[k]*TMath::Power(tau,k); 
      }else if(type==2){
         // GMn 
         den += fb_gmn[k]*TMath::Power(tau,k); 
      }
   }  
   // put it together 
   double g = num/(1. + den); 
   return g; 
}
//______________________________________________________________________________

KellyFormFactors::KellyFormFactors(){
   for(int i=0;i<2;i++){
      fa_gep[i] = 0;
      fa_gmp[i] = 0;
      fa_gmn[i] = 0;
   }
   for(int i=0;i<4;i++){
      fb_gep[i] = 0;
      fb_gmp[i] = 0;
      fb_gmn[i] = 0;
   }
   fa_gep[0] =  1.;
   fa_gep[1] = -0.24;  // +/- 0.12 
   fa_gmp[0] =  1.;
   fa_gmp[1] =  0.12;  // +/- 0.04   
   fa_gmn[0] =  1.; 
   fa_gmn[1] =  2.33;  // +/- 1.4 
   fb_gep[1] =  10.98; // +/- 0.19   
   fb_gep[2] =  12.82; // +/- 1.1    
   fb_gep[3] =  21.97; // +/- 6.8  
   fb_gmp[1] =  10.97; // +/- 0.11 
   fb_gmp[2] =  18.86; // +/- 0.28   
   fb_gmp[3] =  6.55;  // +/- 1.2  
   fb_gmn[1] =  14.72; // +/- 1.7
   fb_gmn[2] =  24.20; // +/- 9.8 
   fb_gmn[3] =  84.1;  // +/- 41 
   fA        =  1.70;  // +/- 0.04 
   fB        =  3.30;  // +/- 0.32  
}
//______________________________________________________________________________
KellyFormFactors::~KellyFormFactors(){} 
//______________________________________________________________________________
double KellyFormFactors::GEp(double Q2){
   double gep = G(0,Q2);
   return gep; 
}
//______________________________________________________________________________
double KellyFormFactors::GMp(double Q2){
   double gmp = Mu_p*G(1,Q2);
   return gmp; 
} 
//______________________________________________________________________________
double KellyFormFactors::GEn(double Q2){
   double M   = M_n/GeV;  
   double tau = Q2/(4.*M*M); 
   double num = fA*tau; 
   double den = 1. + fB*tau; 
   double T1  = num/den;
   double T2  = DipoleFormFactor(Q2);
   double gen = T1*T2;   
   return gen; 
} 
//______________________________________________________________________________
double KellyFormFactors::GMn(double Q2){
   double gmn = Mu_n*G(2,Q2); 
   return gmn;
} 
//______________________________________________________________________________

}}
