#include "insane/formfactors/FormFactors.h"

namespace insane {

  using namespace units;
  namespace physics {

    //FormFactors * FormFactors::fgFormFactors = nullptr;
    //________________________________________________________________________________
    
    FormFactors::FormFactors(){
      SetNameTitle("FormFactors","Form Factors");
      SetLabel("Form Factor");
    }
    //______________________________________________________________________________
    
    FormFactors::~FormFactors() {
    }
    //______________________________________________________________________________
    
    double FormFactors::Ad(double Qsq) {
      double eta = Qsq/(4.0*(insane::units::M_d*insane::units::M_d)/(GeV*GeV));
      double GC = F_C0(Qsq);
      double GM = F_M1(Qsq);
      double GQ = F_C2(Qsq);
      return( GC*GC+(8.0/9.0)*eta*eta*GQ*GQ+(2.0/3.0)*eta*GM*GM );
    }
    //______________________________________________________________________________
    
    double FormFactors::Bd(double Qsq) {
      double eta = Qsq/(4.0*(insane::units::M_d*insane::units::M_d)/(GeV*GeV));
      double GM = F_M1(Qsq);
      return( (4.0/3.0)*eta*(1.0+eta)*GM*GM );
    }
    //______________________________________________________________________________
    
    double FormFactors::T20d(double Qsq) {
         double eta = Qsq/(4.0*(insane::units::M_d*insane::units::M_d)/(GeV*GeV));
         //double GC = GCd(Qsq);
         //double GM = GMd(Qsq);
         //double GQ = GQd(Qsq);
         double GC = F_C0(Qsq);
         double GM = F_M1(Qsq);
         double GQ = F_C2(Qsq);
         double y = 2.0*eta*GQ/(3.0*GC);
         return( std::sqrt(2.0)*y*(2.0+y)/(1.0+2.0*y*y) );
      }
    //______________________________________________________________________________
    double FormFactors::GE_A(double Z,double A,double Q2){
      // GE, GM for Z > 6 
      // Forms for GE and GM derived from expressions for W1 and W2  
      // from Stein et al, Phys Rev D 12, 1884 (1975)
      double M_A   = A*M_p/GeV; 
      double tau   = Q2/(4.*M_A*M_A);
      double Q2_fm = Q2/(hbarc_gev_fm*hbarc_gev_fm);
      double b     = 2.4;                         // fm 
      double c     = 1.07*TMath::Power(A,-1./3.); // fm 
      double F     = TMath::Exp( (-1.)*(Q2_fm*b*b/6.) )/(1. + Q2_fm*c*c/6.);
      double GEA   = TMath::Sqrt(1.+tau)*Z*F;
      return GEA;
    }
    //______________________________________________________________________________
    double FormFactors::GM_A(double Z,double A,double Q2)
    {
      return 0.0;
    }

    //______________________________________________________________________________
    double FormFactors::GENuclear(double Q2, double Z, double A){
      // Electric Form Factor for nuclei 
      // The nuclear form factors are calculated from the proton and neutron form factors via
      // GE^2 = Z GEp^2 + N GEn^2 
      return( TMath::Sqrt(GENuclear2(Q2,Z,A)) );
    }
    //______________________________________________________________________________
    double FormFactors::GMNuclear(double Q2, double Z, double A){
      // Magnetic form factor for nuclei
      // The nuclear form factors are calculated from the proton and neutron form factors via
      // GM^2 = Z GMp^2 + N GMn^2 
      return( TMath::Sqrt(GMNuclear2(Q2,Z,A)) );
    }
    //______________________________________________________________________________
    double FormFactors::GENuclear2(double Q2, double Z, double A){
      // Returns the electric form factor squared. Often GE and GM show up as squared in cross section
      // forumla. 
      // The nuclear form factors are calculated from the proton and neutron form factors via
      // GE^2 = Z GEp^2 + N GEn^2 
      double GE_p = GEp(Q2);
      double GE_n = GEn(Q2);
      double GE2 = Z*GE_p*GE_p + (Z-A)*GE_n*GE_n;
      return( GE2 );
    }
    //______________________________________________________________________________
    double FormFactors::GMNuclear2(double Q2, double Z, double A){
      // Returns the magnetic form factor squared. Often GE and GM show up as squared in cross section
      // forumla.
      // The nuclear form factors are calculated from the proton and neutron form factors via
      // GM^2 = Z GMp^2 + N GMn^2 
      double GM_p = GMp(Q2);
      double GM_n = GMn(Q2);
      double GM2 = Z*GM_p*GM_p + (Z-A)*GM_n*GM_n;
      return( GM2 );
    }
    //______________________________________________________________________________

    double FormFactors::g1p_el(double Qsq) { 
      // The x=1 part of g2p
      double tau = Qsq/(4.0*(M_p/GeV)*(M_p/GeV));
      double GM = GMp(Qsq);
      double GE = GEp(Qsq);
      double c1 = (GM)*(GE+tau*GM)/(2.0*(1.0+tau));
      return c1;
    }
    //______________________________________________________________________________
    double FormFactors::g2p_el(double Qsq) { 
      // The x=1 part of g2p
      double tau = Qsq/(4.0*(M_p/GeV)*(M_p/GeV));
      double GM = GMp(Qsq);
      double GE = GEp(Qsq);
      //std::cout << GE <<  " " << GM << std::endl;
      double c1 = (tau*GM)*(GE-GM)/(2.0*(1.0+tau));
      return c1;
    }
    //______________________________________________________________________________
    double FormFactors::g1p_el_TMC(double Q2) { 
      double x         = 1.0;
      double xi     = insane::kine::xi_Nachtmann(1.0, Q2);
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = (x/(xi*rho*rho*rho))*g1p_el(Q2);
      return res;
    }
    double FormFactors::g2p_el_TMC(double Q2) { 
      double x         = 1.0;
      double xi        = insane::kine::xi_Nachtmann(1.0, Q2);
      double M         = (insane::units::M_p/insane::units::GeV);
      double gamma2    = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho       = TMath::Sqrt(1.0+gamma2);
      double res       = -1.0*(x/(xi*rho*rho*rho))*g2p_el(Q2);
      return res;
    }
    
    double FormFactors::g1n_el(double Qsq) { 
      // The x=1 part of g2p
      double tau = Qsq/(4.0*(M_p/GeV)*(M_p/GeV));
      double GM = GMn(Qsq);
      double GE = GEn(Qsq);
      double c1 = (GM)*(GE+tau*GM)/(2.0*(1.0+tau));
      return c1;
    }
    //______________________________________________________________________________
   
    double FormFactors::g2n_el(double Qsq) { 
      // The x=1 part of g2p
      double tau = Qsq/(4.0*(M_p/GeV)*(M_p/GeV));
      double GM = GMn(Qsq);
      double GE = GEn(Qsq);
      double c1 = (tau*GM)*(GE-GM)/(2.0*(1.0+tau));
      return c1;
    }
    //______________________________________________________________________________
    
    double FormFactors::Mellin_mn_p( int m, int n, double Q2)
    {
      switch(m) {
        case 1: return Mellin_g1p(n,Q2);
                break;
        case 2: return Mellin_g2p(n,Q2);
                break;
      }
      return 0.0;
    }
    
    double FormFactors::Mellin_mn_TMC_p( int m, int n, double Q2)
    {
      switch(m) {
        case 1: return g1p_el_TMC(Q2);
                break;
        case 2: return g2p_el_TMC(Q2);
                break;
      }
      return 0.0;
    }
  
    double FormFactors::d2p_el(double Qsq){
      return(2.0*g1p_el(Qsq) + 3.0*g2p_el(Qsq));
    }
    //______________________________________________________________________________
 
    double FormFactors::d2n_el(double Qsq){
      // d2n elastic contribution
      return(2.0*g1n_el(Qsq) + 3.0*g2n_el(Qsq));
    }
    //______________________________________________________________________________

    double FormFactors::d2p_el_Nachtmann(double Qsq){
      double tau = Qsq/(4.0*(M_p/GeV)*(M_p/GeV));
      double GM = GMp(Qsq);
      double GE = GEp(Qsq);
      double M  = (M_p/GeV);
      double xi = insane::kine::xi_Nachtmann(1.0,Qsq);
      double A = xi*xi*GM*GM/(2.0*(1.0+tau));
      double B = 2.0*xi*(GE/GM + tau);
      double C = 3.0*(1.0 - xi*xi*M*M/(2.0*Qsq));
      double D = tau*(GE/GM -1.0);
      double M_3_2_el = A*(B+C*D);
      return M_3_2_el;
    }
    //______________________________________________________________________________

    double FormFactors::d2n_el_Nachtmann(double Qsq){
      double tau = Qsq/(4.0*(M_p/GeV)*(M_p/GeV));
      double GM = GMn(Qsq);
      double GE = GEn(Qsq);
      double M  = (M_p/GeV);
      double xi = insane::kine::xi_Nachtmann(1.0,Qsq);
      double A = xi*xi*GM*GM/(2.0*(1.0+tau));
      double B = 2.0*xi*(GE/GM + tau);
      double C = 3.0*(1.0 - xi*xi*M*M/(2.0*Qsq));
      double D = tau*(GE/GM -1.0);
      double M_3_2_el = A*(B+C*D);
      return M_3_2_el;
    }
    //______________________________________________________________________________
    
    double FormFactors::M1n_p(int n,double Q2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double x  = 1.0;
      double xi = insane::kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n+1))/(x*x);
      double c1 = x/xi - TMath::Power(double(n*n)/double(n+2),2.0)*y2*x*xi;
      double c2 = -y2*x*x*double(4*n)/double(n+2);
      return( c0*(c1*g1p_el(Q2) + c2*g2p_el(Q2)) );
    }
    //______________________________________________________________________________
    
    double FormFactors::M2n_p(int n, double Q2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double x  = 1.0;
      double xi = insane::kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n+1))/(x*x);
      double c1 = x/xi;
      double c2 = (x/xi)*(x/xi)*double(n)/double(n-1) - y2*x*x*double(n)/double(n+1);
      return( c0*(c1*g1p_el(Q2) + c2*g2p_el(Q2)) );
    }
    //______________________________________________________________________________

    double FormFactors::M1n_n(int n,double Q2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double x  = 1.0;
      double xi = insane::kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n+1))/(x*x);
      double c1 = x/xi - TMath::Power(double(n*n)/double(n+2),2.0)*y2*x*xi;
      double c2 = -y2*x*x*double(4*n)/double(n+2);
      return( c0*(c1*g1n_el(Q2) + c2*g2n_el(Q2)) );
    }
    //______________________________________________________________________________
    
    double FormFactors::M2n_n(int n, double Q2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double x  = 1.0;
      double xi = insane::kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n+1))/(x*x);
      double c1 = x/xi;
      double c2 = (x/xi)*(x/xi)*double(n)/double(n-1) - y2*x*x*double(n)/double(n+1);
      return( c0*(c1*g1n_el(Q2) + c2*g2n_el(Q2)) );
    }
    //______________________________________________________________________________
    
    double FormFactors::M1n_TMC_p(int n, double Q2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double x = 1.0;
      double xi = insane::kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n+1))/(x*x);
      double c1 = x/xi - TMath::Power(double(n*n)/double(n+2),2.0)*y2*x*xi;
      double c2 = -y2*x*x*double(4*n)/double(n+2);
      return( c0*(c1*g1p_el(Q2) + c2*g2p_el(Q2)) );
    }
    //______________________________________________________________________________

    double FormFactors::M2n_TMC_p(int n, double Q2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double x  = 1.0;
      double xi = insane::kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n)+1.0)/(x*x);
      double c1 = x/xi;
      double c2 = (x/xi)*(x/xi)*double(n)/(double(n)-1.0) - y2*x*x*double(n)/(double(n)+1.0);
      return( c0*(c1*g1p_el(Q2) + c2*g2p_el(Q2)) );
    }
    //______________________________________________________________________________
    
    double FormFactors::Mmn_p( int m, int n, double Q2 )
    {
      switch(m) {
        case 1: return M1n_p(n,Q2);
                break;
        case 2: return M2n_p(n,Q2);
                break;
      }
      return 0.0;
    }
    double FormFactors::Mmn_n( int m, int n, double Q2 )
    {
      switch(m) {
        case 1: return M1n_n(n,Q2);
                break;
        case 2: return M2n_n(n,Q2);
                break;
      }
      return 0.0;
    }
    //______________________________________________________________________________
    double FormFactors::Mmn_TMC_p( int m, int n, double Q2 )
    {
      switch(m) {
        case 1: return M1n_TMC_p(n,Q2);
                break;
        case 2: return M2n_TMC_p(n,Q2);
                break;
      }
      return 0.0;
    }
    //double FormFactors::Mmn_TMC_n( int m, int n, double Q2 )
    //{
    //  switch(m) {
    //    case 1: return M1n_TMC_n(n,Q2);
    //            break;
    //    case 2: return M2n_TMC_n(n,Q2);
    //            break;
    //  }
    //  return 0.0;
    //}

    FormFactors2::FormFactors2()
    { }
    //______________________________________________________________________________

    FormFactors2::~FormFactors2()
    { }
    //______________________________________________________________________________

    double FormFactors2::Get(double Q2, std::tuple<FormFactor,Nuclei> ff) const {
      auto fftype = std::get<FormFactor>(ff);
      auto target = std::get<Nuclei>(ff);

      double result = 0.0;
      switch(fftype) {

        case FormFactor::F1 :
          result = F1(Q2, target); 
          break;

        case FormFactor::F2 :
          result = F2(Q2, target); 
          break;
      }
      return result;
    }
    //______________________________________________________________________________

    double FormFactors2::F1(double Q2, Nuclei target) const
    {
      switch(target) {

        case Nuclei::p : 
          return F1p(Q2);
          break;

        case Nuclei::n : 
          return F1n(Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double FormFactors2::F2(double Q2, Nuclei target) const
    {
      switch(target) {

        case Nuclei::p : 
          return F2p(Q2);
          break;

        case Nuclei::n : 
          return F2n(Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

  }
}
