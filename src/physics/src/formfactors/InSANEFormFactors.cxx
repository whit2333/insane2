#include "insane/formfactors/FormFactors.h"

namespace insane {
namespace physics {

//______________________________________________________________________________
FormFactors::FormFactors(){
   SetNameTitle("FormFactors","Form Factors");
   SetLabel("Form Factor");
}
//______________________________________________________________________________
FormFactors::~FormFactors() {
}
//______________________________________________________________________________
Double_t FormFactors::g1p_el(Double_t Qsq) { 
   // The x=1 part of g2p
   double tau = Qsq/(4.0*(M_p/GeV)*(M_p/GeV));
   double GM = GMp(Qsq);
   double GE = GEp(Qsq);
   double c1 = (GM)*(GE+tau*GM)/(2.0*(1.0+tau));
   return c1;
}
//______________________________________________________________________________
Double_t FormFactors::g2p_el(Double_t Qsq) { 
   // The x=1 part of g2p
   double tau = Qsq/(4.0*(M_p/GeV)*(M_p/GeV));
   double GM = GMp(Qsq);
   double GE = GEp(Qsq);
   //std::cout << GE <<  " " << GM << std::endl;
   double c1 = (tau*GM)*(GE-GM)/(2.0*(1.0+tau));
   return c1;
}
//______________________________________________________________________________
Double_t FormFactors::g1n_el(Double_t Qsq) { 
   // The x=1 part of g2p
   double tau = Qsq/(4.0*(M_p/GeV)*(M_p/GeV));
   double GM = GMn(Qsq);
   double GE = GEn(Qsq);
   double c1 = (GM)*(GE+tau*GM)/(2.0*(1.0+tau));
   return c1;
}
//______________________________________________________________________________
Double_t FormFactors::g2n_el(Double_t Qsq) { 
   // The x=1 part of g2p
   double tau = Qsq/(4.0*(M_p/GeV)*(M_p/GeV));
   double GM = GMn(Qsq);
   double GE = GEn(Qsq);
   double c1 = (tau*GM)*(GE-GM)/(2.0*(1.0+tau));
   return c1;
}
//______________________________________________________________________________
Double_t FormFactors::d2p_el(Double_t Qsq){
   return(2.0*g1p_el(Qsq) + 3.0*g2p_el(Qsq));
}
//______________________________________________________________________________
Double_t FormFactors::d2n_el(Double_t Qsq){
   // d2n elastic contribution
   return(2.0*g1n_el(Qsq) + 3.0*g2n_el(Qsq));
}
//______________________________________________________________________________
Double_t FormFactors::d2p_el_Nachtmann(Double_t Qsq){
   double tau = Qsq/(4.0*(M_p/GeV)*(M_p/GeV));
   double GM = GMp(Qsq);
   double GE = GEp(Qsq);
   double M  = (M_p/GeV);
   double xi = ::Kine::xi_Nachtmann(1.0,Qsq);
   double A = xi*xi*GM*GM/(2.0*(1.0+tau));
   double B = 2.0*xi*(GE/GM + tau);
   double C = 3.0*(1.0 - xi*xi*M*M/(2.0*Qsq));
   double D = tau*(GE/GM -1.0);
   double M_3_2_el = A*(B+C*D);
   return M_3_2_el;
}
//______________________________________________________________________________
Double_t FormFactors::d2n_el_Nachtmann(Double_t Qsq){
   double tau = Qsq/(4.0*(M_p/GeV)*(M_p/GeV));
   double GM = GMn(Qsq);
   double GE = GEn(Qsq);
   double M  = (M_p/GeV);
   double xi = ::Kine::xi_Nachtmann(1.0,Qsq);
   double A = xi*xi*GM*GM/(2.0*(1.0+tau));
   double B = 2.0*xi*(GE/GM + tau);
   double C = 3.0*(1.0 - xi*xi*M*M/(2.0*Qsq));
   double D = tau*(GE/GM -1.0);
   double M_3_2_el = A*(B+C*D);
   return M_3_2_el;
}
//______________________________________________________________________________
Double_t FormFactors::GENuclear(Double_t Q2, Double_t Z, Double_t A){
   // Electric Form Factor for nuclei 
   // The nuclear form factors are calculated from the proton and neutron form factors via
   // GE^2 = Z GEp^2 + N GEn^2 
   return( TMath::Sqrt(GENuclear2(Q2,Z,A)) );
}
//______________________________________________________________________________
Double_t FormFactors::GMNuclear(Double_t Q2, Double_t Z, Double_t A){
   // Magnetic form factor for nuclei
   // The nuclear form factors are calculated from the proton and neutron form factors via
   // GM^2 = Z GMp^2 + N GMn^2 
   return( TMath::Sqrt(GMNuclear2(Q2,Z,A)) );
}
//______________________________________________________________________________
Double_t FormFactors::GENuclear2(Double_t Q2, Double_t Z, Double_t A){
   // Returns the electric form factor squared. Often GE and GM show up as squared in cross section
   // forumla. 
   // The nuclear form factors are calculated from the proton and neutron form factors via
   // GE^2 = Z GEp^2 + N GEn^2 
   double GE_p = GEp(Q2);
   double GE_n = GEn(Q2);
   double GE2 = Z*GE_p*GE_p + (Z-A)*GE_n*GE_n;
   return( GE2 );
}
//______________________________________________________________________________
Double_t FormFactors::GMNuclear2(Double_t Q2, Double_t Z, Double_t A){
   // Returns the magnetic form factor squared. Often GE and GM show up as squared in cross section
   // forumla.
   // The nuclear form factors are calculated from the proton and neutron form factors via
   // GM^2 = Z GMp^2 + N GMn^2 
   double GM_p = GMp(Q2);
   double GM_n = GMn(Q2);
   double GM2 = Z*GM_p*GM_p + (Z-A)*GM_n*GM_n;
   return( GM2 );
}
//______________________________________________________________________________


}}

