#include "insane/formfactors/AMTFormFactors.h"

namespace insane {
namespace physics {

AMTFormFactors::AMTFormFactors() {
   SetNameTitle("AMTFormFactors","AMT Form Factors");
   SetLabel("AMT FFs");
   // Fit parameters for G_E
   a_GE[0] = 3.439;
   a_GE[1] = -1.602;
   a_GE[2] = 0.068;
   b_GE[0] = 15.055;
   b_GE[1] = 48.061;
   b_GE[2] = 99.304;
   b_GE[3] = 0.012;
   b_GE[4] = 8.650;
   // Fit parameters for G_M/\mu_p
   a_GM_over_Mu[0] = -1.465;
   a_GM_over_Mu[1] = 1.260;
   a_GM_over_Mu[2] = 0.262;
   b_GM_over_Mu[0] = 9.627;
   b_GM_over_Mu[1] = 0.0;
   b_GM_over_Mu[2] = 0.0;
   b_GM_over_Mu[3] = 11.179;
   b_GM_over_Mu[4] = 13.245;

}
//______________________________________________________________________________

AMTFormFactors::~AMTFormFactors(){ }
//______________________________________________________________________________

double AMTFormFactors::GEp(double Qsq) const {
   double tau = Qsq / (4.0 * M_p/GeV * M_p/GeV);
   double numerator   = 1.0;
   double denominator = 1.0;
   for (int i = 0; i < 3; i++) numerator += a_GE[i] * TMath::Power(tau, i + 1);
   for (int i = 0; i < 5; i++) denominator += b_GE[i] * TMath::Power(tau, i + 1);
   return(numerator / denominator);
}
//______________________________________________________________________________
double AMTFormFactors::GMp(double Qsq) const {
   double tau = Qsq / (4.0 * M_p/GeV * M_p/GeV);
   double numerator   = 1.0;
   double denominator = 1.0;
   for (int i = 0; i < 3; i++) numerator   += a_GM_over_Mu[i] * TMath::Power(tau, i + 1);
   for (int i = 0; i < 5; i++) denominator += b_GM_over_Mu[i] * TMath::Power(tau, i + 1);
   return(1.00*Mu_p * numerator / denominator);
}
//______________________________________________________________________________

}}
