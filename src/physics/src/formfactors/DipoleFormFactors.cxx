#include "insane/formfactors/DipoleFormFactors.h"
#include "insane/base/Math.h"
#include "Math/SpecFunc.h"

namespace insane {
  namespace physics {

    //FormFactors * FormFactors::fgFormFactors = new DipoleFormFactors;
    
    double DipoleFormFactors::F_C0(double Qsq)
    { return WF_FFs.F_C0(this,Qsq); }

    double DipoleFormFactors::F_M1(double Qsq)
    { return WF_FFs.F_M1(this,Qsq); }

    double DipoleFormFactors::F_C2(double Qsq)
    { return WF_FFs.F_C2(this,Qsq); }

    double DipoleFormFactors::WaveFunctionIAFormFactors::F_l(double Qsq) const
    {
      double q = std::sqrt(Qsq);
      auto integrand = [&](double r){
        auto  w = wf.w_a(r);
        auto  j0 =  ROOT::Math::cyl_bessel_j(0, q*r/2.0)+ ROOT::Math::cyl_bessel_j(2, q*r/2.0);
        return w*w*j0;
      };
      double I_res = insane::integrate::simple( integrand, 0.0, 100.0);
      return 3.0*I_res/2.0;
    }

    double DipoleFormFactors::WaveFunctionIAFormFactors::F_s(double Qsq) const
    {
      double q = std::sqrt(Qsq);
      auto integrand = [&](double r){
        auto  u = wf.u_a(r);
        auto  w = wf.w_a(r);
        auto  j2 =  ROOT::Math::cyl_bessel_j(2, q*r/2.0);
        auto  j0 =  ROOT::Math::cyl_bessel_j(0, q*r/2.0);
        return ( (u*u - w*w/2.0)*j0 + (w/std::sqrt(2.0))*(u+w/std::sqrt(2.0))*j2);
      };
      double I_res = insane::integrate::simple( integrand, 0.0, 100.0);
      return I_res;
    }

    double DipoleFormFactors::WaveFunctionIAFormFactors::G_es(FormFactors* ffs,double Qsq) const
    {
      return (ffs->GEp(Qsq)+ffs->GEn(Qsq)/2.0);
    }

    double DipoleFormFactors::WaveFunctionIAFormFactors::G_ms(FormFactors* ffs,double Qsq) const
    {
      return (ffs->GMp(Qsq)+ffs->GMn(Qsq)/2.0);
    }
    
    double DipoleFormFactors::WaveFunctionIAFormFactors::F_C0(FormFactors* ffs,double Qsq) const
    {
      double q = std::sqrt(Qsq);
      auto integrand = [&](double r){
        auto  u = wf.u_a(r);
        auto  w = wf.w_a(r);
        auto  j0 =  ROOT::Math::cyl_bessel_j(0, q*r/2.0);
        return (u*u + w*w)*j0;
      };
      double I_res = insane::integrate::simple( integrand, 0.0, 100.0);
      return 2.0*G_es(ffs,Qsq)*I_res;
    }

    double DipoleFormFactors::WaveFunctionIAFormFactors::F_M1(FormFactors* ffs,double Qsq) const
    {
      double mu = 0.85744;
      double res = (G_es(ffs,Qsq)*F_l(Qsq)+G_ms(ffs,Qsq)*F_s(Qsq))/mu;
      return res;
    }

    double DipoleFormFactors::WaveFunctionIAFormFactors::F_C2(FormFactors* ffs,double Qsq) const
    {
      double q = std::sqrt(Qsq);
      auto integrand = [&](double r){
        auto  u = wf.u_a(r);
        auto  w = wf.w_a(r);
        auto  j0 =  ROOT::Math::cyl_bessel_j(2, q*r/2.0);
        return w*(u - w/(2.0*std::sqrt(2)))*j0;
      };
      double I_res = insane::integrate::simple( integrand, 0.0, 100.0);
      return 12.0*std::sqrt(2)*G_es(ffs,Qsq)*I_res/(Qsq*q);
    }




  }
}

