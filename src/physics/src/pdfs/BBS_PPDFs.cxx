#include "insane/pdfs/BBS_PPDFs.h"

namespace insane {
  namespace physics {
    
    BBS_PPDFs::BBS_PPDFs(){
      SetNameTitle("BBS_PPDFs","BBS pol. PDFs");
      SetLabel("BBS");

      // These values are always zero.
     m_PDFValues.fValues[4] = 0.0;//Delta b
     m_PDFValues.fValues[5] = 0.0;//Delta t
     m_PDFValues.fValues[11] = 0.0;//Delta BBSar
     m_PDFValues.fValues[12] = 0.0;//Delta tbar

      for(Int_t i=0;i<12;i++) m_PDFValues.fUncertainties[i] = 0.; 
    }
    
    BBS_PPDFs::~BBS_PPDFs()
    { }
    
    const std::array<double,NPartons>& BBS_PPDFs::Calculate    (double x, double Q2) const
    {

      m_PDFValues.fx = x;
      m_PDFValues.fQ2 = Q2;

   double up = fqhd.uPlus(x); 
   double um = fqhd.uMinus(x); 
   double dp = fqhd.dPlus(x); 
   double dm = fqhd.dMinus(x); 
   double sp = fqhd.sPlus(x); 
   double sm = fqhd.sMinus(x); 
   double gp = fqhd.gPlus(x); 
   double gm = fqhd.gMinus(x); 

   double u = up - um; 
   double d = dp - dm; 
   double s = sp - sm; 
   double g = gp - gm; 

   for(int i=0;i<13;i++) m_PDFValues.fValues[i] = 0;

   m_PDFValues.fValues[0] = u;
   m_PDFValues.fValues[1] = d;
   m_PDFValues.fValues[2] = s;
   m_PDFValues.fValues[6] = g;



     m_PDFValues.fUncertainties[0] = 0.0;
     m_PDFValues.fUncertainties[1] = 0.0;
     m_PDFValues.fUncertainties[2] = 0.0;
     m_PDFValues.fUncertainties[3] = 0.0;
     m_PDFValues.fUncertainties[6] = 0.0;
     m_PDFValues.fUncertainties[7] = 0.0;
     m_PDFValues.fUncertainties[8] = 0.0;
     m_PDFValues.fUncertainties[9] = 0.0;
     m_PDFValues.fUncertainties[10] = 0.0;

      return(m_PDFValues.fValues);

    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& BBS_PPDFs::Uncertainties(double x, double Q2) const
    {

     m_PDFValues.fUncertainties[0] = 0.0;
     m_PDFValues.fUncertainties[1] = 0.0;
     m_PDFValues.fUncertainties[2] = 0.0;
     m_PDFValues.fUncertainties[3] = 0.0;
     m_PDFValues.fUncertainties[6] = 0.0;
     m_PDFValues.fUncertainties[7] = 0.0;
     m_PDFValues.fUncertainties[8] = 0.0;
     m_PDFValues.fUncertainties[9] = 0.0;
     m_PDFValues.fUncertainties[10] = 0.0;

      return(m_PDFValues.fUncertainties);

    }
    //______________________________________________________________________________
  }
}

