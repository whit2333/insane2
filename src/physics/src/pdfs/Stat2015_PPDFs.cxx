#include "insane/pdfs/Stat2015_PPDFs.h"
#include <iostream>

namespace insane {
  namespace physics {

    Stat2015_PPDFs::Stat2015_PPDFs(){

      SetNameTitle("Stat2015_PPDFs","Stat2015 pol. PDFs");
      SetLabel("Stat2015");
      SetLineColor(2);
      SetLineStyle(1);

      fiPol    = 2;
      fiSingle = 0;
      fiNum    = 0;

      // These values are always zero.
      m_PDFValues.fValues[4] = 0.0;//Delta b
      m_PDFValues.fValues[5] = 0.0;//Delta t
      m_PDFValues.fValues[11] = 0.0;//Delta bbar
      m_PDFValues.fValues[12] = 0.0;//Delta tbar

      for(Int_t i=0;i<12;i++) m_PDFValues.fUncertainties[i] = 0.; 
    }
    //______________________________________________________________________________
    Stat2015_PPDFs::~Stat2015_PPDFs(){

    }
    //______________________________________________________________________________
    const std::array<double,NPartons>& Stat2015_PPDFs::Calculate    (double x, double Q2) const
    {

      m_PDFValues.fx = x;
      m_PDFValues.fQ2 = Q2;

      //* PDF NOTATION FOR UNUnpolarized AND Unpolarized PDF
      //*  -6   -5   -4   -3   -2   -1   0    1  2  3  4  5  6
      //* TBAR BBAR CBAR SBAR DBAR UBAR GLUON U  D  S  C  B  T
      //   0     1    2   3    4    5     6   7  8  9  10 11 12
      partonevol_(&m_PDFValues.fx, &m_PDFValues.fQ2,&fiPol,pdfs,&fiSingle,&fiNum);

      m_PDFValues.fValues[0]  = pdfs[7]/x;   // up
      m_PDFValues.fValues[1]  = pdfs[8]/x;   // down
     m_PDFValues.fValues[2]  = pdfs[9]/x;   // s
     m_PDFValues.fValues[3]  = pdfs[10]/x;  // c
     m_PDFValues.fValues[6]  = pdfs[6]/x;   // g
     m_PDFValues.fValues[7]  = pdfs[5]/x;   // ubar
     m_PDFValues.fValues[8]  = pdfs[4]/x;   // dbar
     m_PDFValues.fValues[9]  = pdfs[3]/x;   // sbar
     m_PDFValues.fValues[10] = pdfs[2]/x;   // cbar

     m_PDFValues.fUncertainties[0] = 0.0;
     m_PDFValues.fUncertainties[1] = 0.0;
     m_PDFValues.fUncertainties[2] = 0.0;
     m_PDFValues.fUncertainties[3] = 0.0;
     m_PDFValues.fUncertainties[6] = 0.0;
     m_PDFValues.fUncertainties[7] = 0.0;
     m_PDFValues.fUncertainties[8] = 0.0;
     m_PDFValues.fUncertainties[9] = 0.0;
     m_PDFValues.fUncertainties[10] = 0.0;

      //std::cout << " u check: " << Get(kUP) << " vs " << pdfs[7]/x << std::endl;;
      return(m_PDFValues.fValues);

    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& Stat2015_PPDFs::Uncertainties(double x, double Q2) const
    {

     m_PDFValues.fUncertainties[0] = 0.0;
     m_PDFValues.fUncertainties[1] = 0.0;
     m_PDFValues.fUncertainties[2] = 0.0;
     m_PDFValues.fUncertainties[3] = 0.0;
     m_PDFValues.fUncertainties[6] = 0.0;
     m_PDFValues.fUncertainties[7] = 0.0;
     m_PDFValues.fUncertainties[8] = 0.0;
     m_PDFValues.fUncertainties[9] = 0.0;
     m_PDFValues.fUncertainties[10] = 0.0;

      return(m_PDFValues.fUncertainties);

    }
    //______________________________________________________________________________

  }
}
