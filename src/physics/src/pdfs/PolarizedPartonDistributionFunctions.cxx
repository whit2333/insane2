#include "insane/pdfs/PolarizedPartonDistributionFunctions.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "insane/base/Math.h"
#include "TH1.h"
#include "TH2.h"

namespace insane {
namespace physics {

    //PolarizedPartonDistributionFunctions * PolarizedPartonDistributionFunctions::fgPolarizedPartonDistributionFunctions = nullptr;
    //________________________________________________________________________________

PolarizedPartonDistributionFunctions::PolarizedPartonDistributionFunctions()
{
   fNintegrate = 100;
}
//______________________________________________________________________________

PolarizedPartonDistributionFunctions::~PolarizedPartonDistributionFunctions()
{ }
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::g1p_Twist2(double x, double Qsq)
{
   // Leading twist g1
   double result = 0.0;
   GetPDFs(x, Qsq);
   result += 0.5 * (4.0 / 9.0) * Deltau();
   result += 0.5 * (1.0 / 9.0) * Deltad();
   result += 0.5 * (1.0 / 9.0) * Deltas();
   result += 0.5 * (4.0 / 9.0) * Deltaubar();
   result += 0.5 * (1.0 / 9.0) * Deltadbar();
   result += 0.5 * (1.0 / 9.0) * Deltasbar();
   return(result);
}
//_____________________________________________________________________________

double PolarizedPartonDistributionFunctions::g1n_Twist2(double x, double Qsq)
{
   // Leading twist g1
   double result = 0.0;
   GetPDFs(x, Qsq);
   result += 0.5 * (4.0 / 9.0) * Deltad();
   result += 0.5 * (1.0 / 9.0) * Deltau();
   result += 0.5 * (1.0 / 9.0) * Deltas();
   result += 0.5 * (4.0 / 9.0) * Deltadbar();
   result += 0.5 * (1.0 / 9.0) * Deltaubar();
   result += 0.5 * (1.0 / 9.0) * Deltasbar();
   return(result);
}
//_____________________________________________________________________________

double PolarizedPartonDistributionFunctions::g1d_Twist2(double x, double Qsq)
{
   // Leading twist g1
   double wD      = 0.058;         // D-wave state probability  
   double g1n_val = g1n_Twist2(x,Qsq); 
   double g1p_val = g1p_Twist2(x,Qsq); 
   double result  = 0.5*(1.-1.5*wD)*(g1p_val + g1n_val);
   return(result);
}
//_____________________________________________________________________________

double PolarizedPartonDistributionFunctions::g1He3_Twist2(double x, double Qsq)
{
   // Leading twist g1
   double Pn      = 0.879;         // neutron polarization in 3He 
   double Pp      = -0.021;        // proton polarization in 3He 
   double g1n_val = g1n_Twist2(x,Qsq); 
   double g1p_val = g1p_Twist2(x,Qsq); 
   double result  = (Pn + 0.056)*g1n_val + (2.*Pp - 0.014)*g1p_val; 
   return(result);
}
//______________________________________________________________________________
double PolarizedPartonDistributionFunctions::g1p_Twist2_TMC  ( double x, double Q2)
{
  double xi        = insane::kine::xi_Nachtmann(x  , Q2);
  double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
  if(xi>xi_thresh) {return 0.0;}
  double M      = (insane::units::M_p/insane::units::GeV);
  double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
  double rho    = TMath::Sqrt(1.0+gamma2);
  double res    = (x/(xi*rho*rho*rho))*g1p_Twist2(xi,Q2);
  double t1     = (rho*rho-1.0)/(rho*rho*rho*rho);
  double integral_result =  insane::integrate::gaus(
      [&,this](double z){
      return( (((x+xi)/xi-(3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1p_Twist2(z,Q2) );
      }, xi,xi_thresh);
  //Int_t    N      = fNintegrate;
  //double dy     = (xi_thresh - xi) / ((double)N);
  //// quick simple integration
  //for (int i = 0; i < N; i++) {
  //  double y = xi + dy*((double)i);
  //  integral_result += integrand(y)*g1p_Twist2(y,Q2)*dy;
  //}
  return( res + t1*integral_result );
}
//______________________________________________________________________________
double PolarizedPartonDistributionFunctions::g2p_Twist2_TMC  ( double x, double Q2)
{
  double xi        = insane::kine::xi_Nachtmann(x  , Q2);
  double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
  if(xi>xi_thresh) {return 0.0;}
  double M         = (insane::units::M_p/insane::units::GeV);
  double gamma2    = TMath::Power(2.0*M*x,2.0)/Q2;
  double rho       = TMath::Sqrt(1.0+gamma2);
  double res       = -1.0*(x/(xi*rho*rho*rho))*g1p_Twist2(xi,Q2);
  double t1        = (1.0)/(rho*rho*rho*rho);
  double integral_result =  insane::integrate::gaus(
      [&,this](double z){
      return( (((x)/xi - (rho*rho-1.0)+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1p_Twist2(z,Q2) );
      }, xi,xi_thresh);
  //auto integrand   = [&](double z){
  //  return( ((x)/xi - (rho*rho-1.0)+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/(2.0*rho))/z );
  //};
  //Int_t    N      = fNintegrate;
  //double dy     = (xi_thresh - xi) / ((double)N);
  //// quick simple integration
  //double integral_result = 0.0;
  //for (int i = 0; i < N; i++) {
  //  double y = xi + dy*((double)i);
  //  integral_result += integrand(y)*g1p_Twist2(y,Q2)*dy;
  //}
  return( res + t1*integral_result );
}
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::g1n_Twist2_TMC  ( double x, double Q2)
{
  double xi        = insane::kine::xi_Nachtmann(x  , Q2);
  double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
  if(xi>xi_thresh) {return 0.0;}
  double M         = (insane::units::M_p/insane::units::GeV);
  double gamma2    = TMath::Power(2.0*M*x,2.0)/Q2;
  double rho       = TMath::Sqrt(1.0+gamma2);
  double res       = (x/(xi*rho*rho*rho))*g1n_Twist2(xi,Q2);
  double t1        = (rho*rho-1.0)/(rho*rho*rho*rho);
  double integral_result =  insane::integrate::gaus(
      [&,this](double z){
      return( (((x+xi)/xi - (3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1n_Twist2(z,Q2) );
      }, xi,xi_thresh);
  //auto integrand   = [&](double z){
  //  return( ((x+xi)/xi - (3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z );
  //};
  //Int_t    N      = fNintegrate;
  //double dy     = (xi_thresh - xi) / ((double)N);
  //// quick simple integration
  //double integral_result = 0.0;
  //for (int i = 0; i < N; i++) {
  //  double y = xi + dy*((double)i);
  //  integral_result += integrand(y)*g1n_Twist2(y,Q2)*dy;
  //}
  return( res + t1*integral_result );
}
//______________________________________________________________________________
double PolarizedPartonDistributionFunctions::g1d_Twist2_TMC  ( double x, double Q2)
{
  double xi        = insane::kine::xi_Nachtmann(x  , Q2);
  double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
  if(xi>xi_thresh) {return 0.0;}
  double gamma2 = TMath::Power(2.0*insane::units::M_p*x,2.0)/Q2;
  double rho    = TMath::Sqrt(1.0+gamma2);
  double res    = (x/(xi*rho*rho*rho))*g1d_Twist2(xi,Q2);
  double t1     = (rho*rho-1.0)/(rho*rho*rho*rho);
  double integral_result =  insane::integrate::gaus(
      [&](double z){
      return( (((x+xi)/xi - (3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1d_Twist2(z,Q2) );
      }, xi,xi_thresh);
  //auto integrand = [&](double z){
  //  return( ((x+xi)/xi - (3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z );
  //};
  //Int_t    N      = fNintegrate;
  //double dy     = (xi_thresh - xi) / ((double)N);
  //// quick simple integration
  //double integral_result = 0.0;
  //for (int i = 0; i < N; i++) {
  //  double y = xi + dy*((double)i);
  //  integral_result += integrand(y)*g1d_Twist2(y,Q2)*dy;
  //}
  return( res + t1*integral_result );
}
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::g1He3_Twist2_TMC( double x, double Q2)
{
  double xi        = insane::kine::xi_Nachtmann(x  , Q2);
  double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
  if(xi>xi_thresh) {return 0.0;}
  double gamma2 = TMath::Power(2.0*insane::units::M_p*x,2.0)/Q2;
  double rho    = TMath::Sqrt(1.0+gamma2);
  double res    = (x/(xi*rho*rho*rho))*g1He3_Twist2(xi,Q2);
  double t1     = (rho*rho-1.0)/(rho*rho*rho*rho);
  double integral_result =  insane::integrate::gaus(
      [&](double z){
      return( (((x+xi)/xi - (3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*g1He3_Twist2(z,Q2) );
      }, xi,xi_thresh);
  //auto integrand = [&](double z){
  //  return( ((x+xi)/xi - (3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z );
  //};
  //Int_t    N      = fNintegrate;
  //double dy     = (xi_thresh - xi) / ((double)N);
  //// quick simple integration
  //double integral_result = 0.0;
  //for (int i = 0; i < N; i++) {
  //  double y = xi + dy*((double)i);
  //  integral_result += integrand(y)*g1He3_Twist2(y,Q2)*dy;
  //}
  return( res + t1*integral_result );
}
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::g2pWW(double x, double Qsq)
{
  // g2WW
  double result          = -1.0*g1p_Twist2(x, Qsq);
  double integral_result = insane::integrate::gaus(
      [&](double z){
      return( g1p_Twist2(z,Qsq)/z );
      }, x,1.0);
  //Int_t N         = fNintegrate;
  //double dy     = (1.0 - x)/(double(N));
  //for (int i = 0; i < N; i++) {
  //  double y = x + dy*double(i);
  //  result += g1p_Twist2(y, Qsq)*(dy/y);
  //}
  return( result + integral_result );
}
//_____________________________________________________________________________

double PolarizedPartonDistributionFunctions::g1p_BT_Twist3(double x, double Qsq)
{
  // BT Relation - effectively a TMC only to O(M^2/Q^2)
  // g1p_Twist3_TMC - has the TMC treatment
  // g1 twist-3
  double g2p    =  g2p_Twist3_TMC(x, Qsq);
  double integral_result = insane::integrate::gaus(
      [&](double z){
      return( g2p_Twist3_TMC(z,Qsq)/z );
      }, x,1.0);
  //Int_t    N      = fNintegrate;
  //double dy     = (1.0 - x) / ((double)N);
  //double y = 0.0;
  //double integral = 0.0;
  //// quick simple integration
  //for (int i = 0; i < N; i++) {
  //  y = x0 + dy*((double)i);
  //  integral += (g2p_Twist3_TMC(y,Qsq0)*dy/(y));
  ////  result += (-2.0*g2p_Twist3_TMC(y,Qsq0)*dy/(y));
  //}
  double M = (M_p/GeV);
  double coeff = (4.0*M*M*x*x)/Qsq;
  double result = coeff*(g2p - 2.0*integral_result );
  return(result);
}
//_____________________________________________________________________________

double PolarizedPartonDistributionFunctions::Dp_BT(double x, double Q2)
{
  // BT Relation - effectively a TMC only to O(M^2/Q^2)
  double integral_result = insane::integrate::gaus(
      [&](double z){
      return( (3.0-2.0*TMath::Log(z/x))*Dp_Twist3(z,Q2)/z );
      }, x,1.0);
  //double result = 0.0;
  //Int_t    N      = fNintegrate;
  //double dy     = (1.0 - x) / ((double)N);
  //double y = 0.0;
  //// quick simple integration
  //for (int i = 0; i < N; i++) {
  //  y = x + dy*double(i);
  //  double Dp = Dp_Twist3(y, Q2);
  //  result += ((dy/y)*(3.0-2.0*TMath::Log(y/x))*Dp);
  //}
  return(integral_result);
}
//_____________________________________________________________________________

double PolarizedPartonDistributionFunctions::g1p_Twist3_TMC(double x, double Q2)
{
  // Twist three part of g1p with full TMC treatment
  double xi        = insane::kine::xi_Nachtmann(x  , Q2);
  double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
  if(xi>xi_thresh) {return 0.0;}
  double M      = (insane::units::M_p/insane::units::GeV);
  double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
  double rho    = TMath::Sqrt(1.0+gamma2);
  double res    = (rho*rho-1.0)/(rho*rho*rho)*Dp_Twist3(xi,Q2);
  double t1     = (1.0-rho*rho)/(rho*rho*rho*rho);
  double integral_result =  insane::integrate::gaus(
      [&](double z){
      return( ((3.0-(3.0-rho*rho)*TMath::Log(z/xi)/rho)/z)*Dp_Twist3(z,Q2) );
      }, xi,xi_thresh);
  //auto integrand = [&](double z){
  //return( ((3.0-(3.0-rho*rho)*TMath::Log(z/xi)/rho)/z)*Dp_Twist3(z,Q2) );
  //};
  //Int_t    N      = fNintegrate;
  //double dy     = (1.0 - xi) / ((double)N);
  //// quick simple integration
  //double integral_result = 0.0;
  //for (int i = 0; i < N; i++) {
  //  double y = xi + dy*((double)i);
  //  integral_result += integrand(y)*Dp_Twist3(y,Q2)*dy;
  //}
  return( res + t1*integral_result );
}
//_____________________________________________________________________________

double PolarizedPartonDistributionFunctions::g2p_Twist3_TMC(double x, double Q2)
{
  // Twist three part of g1p with full TMC treatment
  double xi        = insane::kine::xi_Nachtmann(x  , Q2);
  double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
  if(xi>xi_thresh) {return 0.0;}
  double M      = (insane::units::M_p/insane::units::GeV);
  double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
  double rho    = TMath::Sqrt(1.0+gamma2);
  double res    = 1.0/(rho*rho*rho)*Dp_Twist3(xi,Q2);
  double t1     = (-1.0)/(rho*rho*rho*rho);
  double integral_result =  insane::integrate::gaus(
      [&](double z){
      return(((3.0-2.0*rho*rho+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/rho)/z)*Dp_Twist3(z,Q2) );
      }, xi,xi_thresh);
  //auto integrand = [&](double z){
  //  return( (3.0-2.0*rho*rho+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/rho)/z );
  //};
  //Int_t    N      = fNintegrate;
  //double dy     = (xi_thresh - xi) / ((double)N);
  //// quick simple integration
  //double integral_result = 0.0;
  //for (int i = 0; i < N; i++) {
  //  double y = xi + dy*((double)i);
  //  integral_result += integrand(y)*Dp_Twist3(y,Q2)*dy;
  //}
  return( res + t1*integral_result );
}
//_____________________________________________________________________________

double PolarizedPartonDistributionFunctions::g2nWW(double x, double Qsq)
{
  double result = - g1n_Twist2(x, Qsq);
  double integral_result =  insane::integrate::gaus(
      [&](double z){
      return( g1n_Twist2(z,Qsq)/z );
      }, x,1.0);
  //double x0 = x;
  //double Qsq0 = Qsq;
  //Int_t N = fNintegrate;
  //double dx = (1.0 - x) / ((double)N);
  //// quick simple integration
  //for (int i = 0; i < N; i++) {
  //  result += g1n_Twist2(x0 + dx * ((double)i), Qsq0) * dx / (x0 + dx * ((double)i));
  // }
  // // return pdfs to initial x,Qsq
  return(result + integral_result );
}
//_____________________________________________________________________________

double PolarizedPartonDistributionFunctions::g1n_BT_Twist3(double x, double Qsq)
{
  // BT Relation
  // g1 twist-3
  double result          =  g2n_Twist3(x, Qsq);
  double integral_result =  insane::integrate::gaus(
      [&](double z){
      return( g2n_Twist3(z,Qsq)/z );
      }, x,1.0);
  //Int_t    N      = fNintegrate;
  //double dy     = (1.0 - x) / ((double)N);
  //double y = 0.0;
  //// quick simple integration
  //for (int i = 0; i < N; i++) {
  //  y = x0 + dy*((double)i);
  //  result += (-2.0*g2n_Twist3(y,Qsq0)*dy/(y));
  //}
  double M = (M_p/GeV);
  double coeff = (4.0*M*M*x*x)/Qsq;
  return( coeff*(result - 2.0*integral_result) );
}
//_____________________________________________________________________________

double PolarizedPartonDistributionFunctions::g2dWW(double x, double Qsq)
{
  double result          = -1.0*g1d_Twist2(x, Qsq);
  double integral_result =  insane::integrate::gaus(
      [&](double z){
      return( g1d_Twist2(z,Qsq)/z );
      }, x,1.0);
  //double x0 = x;
  //double Qsq0 = Qsq;
  //Int_t N = fNintegrate;
  //double dx = (1.0 - x) / ((double)N);
  //// quick simple integration
  //for (int i = 0; i < N; i++) {
  //  result += g1d_Twist2(x0 + dx * ((double)i), Qsq0) * dx / (x0 + dx * ((double)i));
  //}
  // return pdfs to initial x,Qsq
  /*      fPolarizedPDFs->GetPDFs(x0,Qsq0);*/
  return(result + integral_result);
}
//_____________________________________________________________________________

double PolarizedPartonDistributionFunctions::g1d_BT_Twist3(double x, double Qsq)
{
  // BT Relation
  // g1 twist-3
  double result          =  g2d_Twist3(x, Qsq);
  double integral_result =  insane::integrate::gaus(
      [&](double z){
      return( g2d_Twist3(z,Qsq)/z );
      }, x,1.0);
  //Int_t    N      = fNintegrate;
  //double dy     = (1.0 - x) / ((double)N);
  //double y = 0.0;
  //// quick simple integration
  //for (int i = 0; i < N; i++) {
  //  y = x0 + dy*((double)i);
  //  result += (-2.0*g2d_Twist3(y,Qsq0)*dy/(y));
  //}
  //result *= (4.0*(M_p/GeV)*(M_p/GeV)*x*x);
  double M     = (M_p/GeV);
  double coeff = (4.0*M*M*x*x)/Qsq;
  return( coeff*(result - 2.0*integral_result) );
}
//_____________________________________________________________________________

double PolarizedPartonDistributionFunctions::g2He3WW(double x, double Qsq)
{
  double result          = -1.0*g1He3_Twist2(x, Qsq);
  double integral_result =  insane::integrate::gaus(
      [&](double z){
      return( g1He3_Twist2(z,Qsq)/z );
      }, x,1.0);
  return( result + integral_result );
}
//_____________________________________________________________________________

double PolarizedPartonDistributionFunctions::g1He3_BT_Twist3(double x, double Qsq)
{
  // BT Relation g1 twist-3
  double result          =  g2He3_Twist3(x, Qsq);
  double integral_result =  insane::integrate::gaus(
      [&](double z){
      return( g2He3_Twist3(z,Qsq)/z );
      }, x,1.0);
  double M = (M_p/GeV);
  double coeff = (4.0*M*M*x*x)/Qsq;
  return( coeff*(result - 2.0*integral_result) );
}
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::Dp_Twist3(double x, double Q2)
{ 
   double result = 0.0;
   GetPDFs(x, Q2);
   result += (4.0 / 9.0) * fxDu_Twist3/x;
   result += (1.0 / 9.0) * fxDd_Twist3/x;
   //result += 0.5 * (1.0 / 9.0) * Deltas();
   //result += 0.5 * (4.0 / 9.0) * Deltaubar();
   //result += 0.5 * (1.0 / 9.0) * Deltadbar();
   //result += 0.5 * (1.0 / 9.0) * Deltasbar();
   //std::cout << "Dp_Twist3 " <<  result << '\n';
   return(result);
}
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::Dn_Twist3(double x, double Q2)
{ 
   double result = 0.0;
   GetPDFs(x, Q2);
   result += (4.0 / 9.0) * fxDd_Twist3;
   result += (1.0 / 9.0) * fxDu_Twist3;
   //result += 0.5 * (1.0 / 9.0) * Deltas();
   //result += 0.5 * (4.0 / 9.0) * Deltaubar();
   //result += 0.5 * (1.0 / 9.0) * Deltadbar();
   //result += 0.5 * (1.0 / 9.0) * Deltasbar();
   //std::cout << "Dp_Twist3 " <<  result << '\n';
   return(result);
}
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::Moment_Dp_Twist3(int n, double Q2, double x1, double x2) 
{
  double integral_result =  insane::integrate::gaus(
      [&](double z){
      return( TMath::Power(z,double(n-1.0))*Dp_Twist3(z,Q2) );
      }, x1, x2);
  return(integral_result);
}
//_____________________________________________________________________________

double PolarizedPartonDistributionFunctions::g2p_Twist3(double x, double Q2)
{ 
  double result          = Dp_Twist3(x, Q2);
  double integral_result = insane::integrate::gaus(
      [&](double z){
      return( Dp_Twist3(z,Q2)/z );
      }, x,1.0);
  return( result - integral_result );
}
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::g2n_Twist3(double x, double Q2)
{ 
  double result          = Dn_Twist3(x, Q2);
  double integral_result = insane::integrate::gaus(
      [&](double z){
      return( Dn_Twist3(z,Q2)/z );
      }, x,1.0);
  return( result - integral_result );
}
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::g2d_Twist3(double,double)   { return 0; }
double PolarizedPartonDistributionFunctions::g2He3_Twist3(double,double) { return 0; }

//______________________________________________________________________________
double PolarizedPartonDistributionFunctions::Deltau()    const { return(fPDFValues[0]); }
double PolarizedPartonDistributionFunctions::Deltad()    const { return(fPDFValues[1]); }
double PolarizedPartonDistributionFunctions::Deltas()    const { return(fPDFValues[2]); }
double PolarizedPartonDistributionFunctions::Deltac()    const { return(fPDFValues[3]); }
double PolarizedPartonDistributionFunctions::Deltab()    const { return(fPDFValues[4]); }
double PolarizedPartonDistributionFunctions::Deltat()    const { return(fPDFValues[5]); }
double PolarizedPartonDistributionFunctions::Deltag()    const { return(fPDFValues[6]); }
double PolarizedPartonDistributionFunctions::Deltaubar() const { return(fPDFValues[7]); }
double PolarizedPartonDistributionFunctions::Deltadbar() const { return(fPDFValues[8]); }
double PolarizedPartonDistributionFunctions::Deltasbar() const { return(fPDFValues[9]); }
double PolarizedPartonDistributionFunctions::Deltacbar() const { return(fPDFValues[10]); }
double PolarizedPartonDistributionFunctions::Deltabbar() const { return(fPDFValues[11]); }
double PolarizedPartonDistributionFunctions::Deltatbar() const { return(fPDFValues[12]); }
//______________________________________________________________________________
double PolarizedPartonDistributionFunctions::DeltauError()    const { return(fPDFErrors[0]); }
double PolarizedPartonDistributionFunctions::DeltadError()    const { return(fPDFErrors[1]); }
double PolarizedPartonDistributionFunctions::DeltasError()    const { return(fPDFErrors[2]); }
double PolarizedPartonDistributionFunctions::DeltacError()    const { return(fPDFErrors[3]); }
double PolarizedPartonDistributionFunctions::DeltabError()    const { return(fPDFErrors[4]); }
double PolarizedPartonDistributionFunctions::DeltatError()    const { return(fPDFErrors[5]); }
double PolarizedPartonDistributionFunctions::DeltagError()    const { return(fPDFErrors[6]); }
double PolarizedPartonDistributionFunctions::DeltaubarError() const { return(fPDFErrors[7]); }
double PolarizedPartonDistributionFunctions::DeltadbarError() const { return(fPDFErrors[8]); }
double PolarizedPartonDistributionFunctions::DeltasbarError() const { return(fPDFErrors[9]); }
double PolarizedPartonDistributionFunctions::DeltacbarError() const { return(fPDFErrors[10]); }
double PolarizedPartonDistributionFunctions::DeltabbarError() const { return(fPDFErrors[11]); }
double PolarizedPartonDistributionFunctions::DeltatbarError() const { return(fPDFErrors[12]); }
//______________________________________________________________________________
double PolarizedPartonDistributionFunctions::Deltau(double x, double Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[0]);
}
double PolarizedPartonDistributionFunctions::DeltauBar(double x, double Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[7]);
}
double PolarizedPartonDistributionFunctions::Deltad(double x, double Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[1]);
}
double PolarizedPartonDistributionFunctions::DeltadBar(double x, double Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[8]);
}
double PolarizedPartonDistributionFunctions::Deltas(double x, double Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[2]);
}
double PolarizedPartonDistributionFunctions::DeltasBar(double x, double Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[9]);
}
double PolarizedPartonDistributionFunctions::Deltag(double x,double Qsq){
   GetPDFs(x,Qsq);
   return fPDFValues[6]; 
}
double PolarizedPartonDistributionFunctions::Deltac(double x, double Qsq)  {
   GetPDFs(x, Qsq);
   return(fPDFValues[3]);
}
double PolarizedPartonDistributionFunctions::DeltacBar(double x, double Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[10]);
}
double PolarizedPartonDistributionFunctions::Deltab(double x, double Qsq)  {
   GetPDFs(x, Qsq);
   return(fPDFValues[4]);
}
double PolarizedPartonDistributionFunctions::DeltabBar(double x, double Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[11]);
}
double PolarizedPartonDistributionFunctions::Deltat(double x, double Qsq)  {
   GetPDFs(x, Qsq);
   return(fPDFValues[5]);
}
double PolarizedPartonDistributionFunctions::DeltatBar(double x, double Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFValues[12]);
}
double PolarizedPartonDistributionFunctions::DeltauError(double x, double Qsq) {
   GetPDFErrors(x, Qsq);
   return(fPDFErrors[0]);
}
double PolarizedPartonDistributionFunctions::DeltauBarError(double x, double Qsq) {
   GetPDFErrors(x, Qsq);
   return(fPDFErrors[7]);
}
double PolarizedPartonDistributionFunctions::DeltadError(double x, double Qsq) {
   GetPDFErrors(x, Qsq);
   return(fPDFErrors[1]);
}
double PolarizedPartonDistributionFunctions::DeltadBarError(double x, double Qsq) {
   GetPDFErrors(x, Qsq);
   return(fPDFErrors[8]);
}
double PolarizedPartonDistributionFunctions::DeltasError(double x, double Qsq) {
   GetPDFErrors(x, Qsq);
   return(fPDFErrors[2]);
}
double PolarizedPartonDistributionFunctions::DeltasBarError(double x, double Qsq) {
   GetPDFErrors(x, Qsq);
   return(fPDFErrors[9]);
}
double PolarizedPartonDistributionFunctions::DeltacError(double x, double Qsq)  {
   GetPDFs(x, Qsq);
   return(fPDFErrors[3]);
}
double PolarizedPartonDistributionFunctions::DeltacBarError(double x, double Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFErrors[10]);
}
double PolarizedPartonDistributionFunctions::DeltabError(double x, double Qsq)  {
   GetPDFs(x, Qsq);
   return(fPDFErrors[4]);
}
double PolarizedPartonDistributionFunctions::DeltabBarError(double x, double Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFErrors[11]);
}
double PolarizedPartonDistributionFunctions::DeltatError(double x, double Qsq)  {
   GetPDFs(x, Qsq);
   return(fPDFErrors[5]);
}
double PolarizedPartonDistributionFunctions::DeltatBarError(double x, double Qsq) {
   GetPDFs(x, Qsq);
   return(fPDFErrors[12]);
}
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::D_u(double x, double Q2)
{ 
   GetPDFs(x, Q2);
   return fxDu_Twist3/x;
}
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::D_d(double x, double Q2)
{ 
   GetPDFs(x, Q2);
   return fxDd_Twist3/x;
}
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::gT_u_WW(double x, double Q2)
{
  double integral_result = insane::integrate::gaus(
      [&](double z){
      return( Deltau(z,Q2)/z );
      }, x,1.0);
  return( integral_result );
  //// quick simple integration
  //Int_t N         = 100;
  //double dy     = (1.0 - x) / ((double)N);
  //double result = 0.0;
  //double x0     = x;
  //for (int i = 0; i < N; i++) {
  //  double y = x0 + dy*((double)i);
  //  result    += (Deltau(y,Q2)*dy/(y));
  //}
  //return(result);
}
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::gT_d_WW(double x, double Q2)
{
  double integral_result = insane::integrate::gaus(
      [&](double z){
      return( Deltad(z,Q2)/z );
      }, x,1.0);
  return( integral_result );
}
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::gT_ubar_WW(double x, double Q2){
  double integral_result = insane::integrate::gaus(
      [&](double z){
      return( DeltauBar(z,Q2)/z );
      }, x,1.0);
  return( integral_result );
}
//______________________________________________________________________________

double PolarizedPartonDistributionFunctions::gT_dbar_WW(double x, double Q2){
  double integral_result = insane::integrate::gaus(
      [&](double z){
      return( DeltadBar(z,Q2)/z );
      }, x,1.0);
  return( integral_result );
}

////______________________________________________________________________________
//double PolarizedStructureFunctionsFromPDFs::g1p(double x, double Qsq) {
//   //double result = xg1p(x, Qsq) / x;
//   double result = 0.0;
//   GetPDFs(x, Qsq);
//   result += 0.5 * (4.0 / 9.0) * Deltau();
//   result += 0.5 * (1.0 / 9.0) * Deltad();
//   result += 0.5 * (1.0 / 9.0) * Deltas();
//   result += 0.5 * (4.0 / 9.0) * Deltaubar();
//   result += 0.5 * (1.0 / 9.0) * Deltadbar();
//   result += 0.5 * (1.0 / 9.0) * Deltasbar();
//   return(result);
//}
////_____________________________________________________________________________
//double PolarizedStructureFunctionsFromPDFs::g1n(double x, double Qsq) {
//   //double result = xg1n(x, Qsq) / x;
//   double result = 0.0;
//   if(!fPolarizedPDFs) {
//      Error("xg1n(x,Qsq)","No polarized PDFs set");
//      return(result);
//   }
//   fPolarizedPDFs->GetPDFs(x, Qsq);
//   result += 0.5 * (4.0 / 9.0) * fPolarizedPDFs->Deltad();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltau();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltas();
//   result += 0.5 * (4.0 / 9.0) * fPolarizedPDFs->Deltadbar();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltaubar();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltasbar();
//   return(result);
//}
////_____________________________________________________________________________
//double PolarizedStructureFunctionsFromPDFs::g1d(double x, double Qsq) {
//   double wD      = 0.058;         // D-wave state probability  
//   double g1n_val = g1n(x,Qsq); 
//   double g1p_val = g1p(x,Qsq); 
//   double result  = 0.5*(1.-1.5*wD)*(g1p_val + g1n_val);
//   return(result);
//}
////_____________________________________________________________________________
//double PolarizedStructureFunctionsFromPDFs::g1He3(double x, double Qsq) {
//   double Pn      = 0.879;         // neutron polarization in 3He 
//   double Pp      = -0.021;        // proton polarization in 3He 
//   double g1n_val = g1n(x,Qsq); 
//   double g1p_val = g1p(x,Qsq); 
//   double result  = (Pn + 0.056)*g1n_val + (2.*Pp - 0.014)*g1p_val; 
//   return(result);
//}
//_____________________________________________________________________________
//double PolarizedStructureFunctionsFromPDFs::xg1p(double x, double Qsq) {
//   double result = 0.0;
//   fPolarizedPDFs->GetPDFs(x, Qsq);
//   result += 0.5 * (4.0 / 9.0) * fPolarizedPDFs->Deltau();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltad();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltas();
//   result += 0.5 * (4.0 / 9.0) * fPolarizedPDFs->Deltaubar();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltadbar();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltasbar();
//   return(result*x);
//}
////_____________________________________________________________________________
//double PolarizedStructureFunctionsFromPDFs::xg1n(double x, double Qsq) {
//   double result = 0.0;
//   if(!fPolarizedPDFs) {
//      Error("xg1n(x,Qsq)","No polarized PDFs set");
//      return(result);
//   }
//   fPolarizedPDFs->GetPDFs(x, Qsq);
//   result += 0.5 * (4.0 / 9.0) * fPolarizedPDFs->Deltad();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltau();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltas();
//   result += 0.5 * (4.0 / 9.0) * fPolarizedPDFs->Deltadbar();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltaubar();
//   result += 0.5 * (1.0 / 9.0) * fPolarizedPDFs->Deltasbar();
//   return(result*x);
//}
////_____________________________________________________________________________
//TF1 * PolarizedPartonDistributionFunctions::GetFunction(PDFBase::PartonFlavor q) {
//   if (fFunctions[q]){
//      fFunctions[q]->TAttLine::operator=(*this);
//      return(fFunctions[q]);
//   } else {
//      Int_t npar = 1;
//      switch (q) {
//         case PDFBase::kUP :
//            fFunctions[q] = new TF1(Form("delta xu %s", GetLabel()), this, &PolarizedPartonDistributionFunctions::EvaluatexDeltau,
//                  fxPlotMin, fxPlotMax, npar, "PolarizedPartonDistributionFunctions", "EvaluatexDeltau");
//            break;
//         case PDFBase::kDOWN :
//            fFunctions[q] = new TF1(Form("delta xd %s", GetLabel()), this, &PolarizedPartonDistributionFunctions::EvaluatexDeltad,
//                  fxPlotMin, fxPlotMax, npar, "PolarizedPartonDistributionFunctions", "EvaluatexDeltad");
//            break;
//
//         case PDFBase::kSTRANGE :
//            fFunctions[q] = new TF1("delta xs", this, &PolarizedPartonDistributionFunctions::EvaluatexDeltas,
//                  fxPlotMin, fxPlotMax, npar, "PolarizedPartonDistributionFunctions", "EvaluatexDeltas");
//            break;
//
//         case PDFBase::kGLUON :
//            fFunctions[q] = new TF1("delta xg", this, &PolarizedPartonDistributionFunctions::EvaluatexDeltag,
//                  fxPlotMin, fxPlotMax, npar, "PolarizedPartonDistributionFunctions", "EvaluatexDeltag");
//            break;
//
//         case PDFBase::kANTIUP :
//            fFunctions[q] = new TF1("delta xubar", this, &PolarizedPartonDistributionFunctions::EvaluatexDeltaubar,
//                  fxPlotMin, fxPlotMax, npar, "PolarizedPartonDistributionFunctions", "EvaluatexDeltaubar");
//            break;
//
//         case PDFBase::kANTIDOWN :
//            fFunctions[q] = new TF1("delta xdbar", this, &PolarizedPartonDistributionFunctions::EvaluatexDeltadbar,
//                  fxPlotMin, fxPlotMax, npar, "PolarizedPartonDistributionFunctions", "EvaluatexDeltadbar");
//            break;
//
//         case PDFBase::kANTISTRANGE :
//            fFunctions[q] = new TF1("delta xsbar", this, &PolarizedPartonDistributionFunctions::EvaluatexDeltasbar,
//                  fxPlotMin, fxPlotMax, npar, "PolarizedPartonDistributionFunctions", "EvaluatexDeltasbar");
//            break;
//         default :
//            fFunctions[q] = new TF1(Form("delta xu %s", GetLabel()), this, &PolarizedPartonDistributionFunctions::EvaluatexDeltau,
//                  fxPlotMin, fxPlotMax, npar, "PolarizedPartonDistributionFunctions", "EvaluatexDeltau");
//            break;
//
//      }
//   }
//   //fFunctions[q]->SetLineColor(fDefaultLineColor);
//   //fFunctions[q]->SetLineStyle(fDefaultLineStyle);
//   fFunctions[q]->TAttLine::operator=(*this);
//   return(fFunctions[q]);
//}
//_____________________________________________________________________________
void PolarizedPartonDistributionFunctions::GetValues(TObject *obj, double Q2, Parton q){
   // Fills histogram with values.
   //  For error band use GetErrorBand
   //if ( !(obj->InheritsFrom(TH1::Class())) ) {
   //   Error("GetErrorBand","Not a TH1 class");
   //   return;
   //}
   if(!obj) {
      return;
   }
   //  returns errorsband
   auto *hfit = (TH1*)obj;
   Int_t hxfirst = hfit->GetXaxis()->GetFirst();
   Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
   Int_t hyfirst = hfit->GetYaxis()->GetFirst();
   Int_t hylast  = hfit->GetYaxis()->GetLast(); 
   Int_t hzfirst = hfit->GetZaxis()->GetFirst();
   Int_t hzlast  = hfit->GetZaxis()->GetLast(); 

   TAxis *xaxis  = hfit->GetXaxis();
   TAxis *yaxis  = hfit->GetYaxis();
   TAxis *zaxis  = hfit->GetZaxis();

   double x[3];

   for (Int_t binz=hzfirst; binz<=hzlast; binz++){
      x[2]=zaxis->GetBinCenter(binz);
      for (Int_t biny=hyfirst; biny<=hylast; biny++) {
         x[1]=yaxis->GetBinCenter(biny);
         for (Int_t binx=hxfirst; binx<=hxlast; binx++) {
            x[0]=xaxis->GetBinCenter(binx);

            GetPDFs(x[0], Q2);
            hfit->SetBinContent(binx, biny, binz, x[0]*fPDFValues[q]);
   //GetPDFErrors(x[0], Q2);
   //         hfit->SetBinError(binx, biny, binz, x[0]*fPDFErrors[q]);
         }
      }
   }

}
//_____________________________________________________________________________
void PolarizedPartonDistributionFunctions::GetErrorBand(TObject *obj, double Q2, Parton q){
   //if ( !(obj->InheritsFrom(TH1::Class())) ) {
   //   Error("GetErrorBand","Not a TH1 class");
   //   return;
   //}
   if(!obj) {
      return;
   }
   //  returns errorsband
   auto *hfit = (TH1*)obj;
   Int_t hxfirst = hfit->GetXaxis()->GetFirst();
   Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
   Int_t hyfirst = hfit->GetYaxis()->GetFirst();
   Int_t hylast  = hfit->GetYaxis()->GetLast(); 
   Int_t hzfirst = hfit->GetZaxis()->GetFirst();
   Int_t hzlast  = hfit->GetZaxis()->GetLast(); 

   TAxis *xaxis  = hfit->GetXaxis();
   TAxis *yaxis  = hfit->GetYaxis();
   TAxis *zaxis  = hfit->GetZaxis();

   double x[3];
   double err = 0.0;

   
   for (Int_t binz=hzfirst; binz<=hzlast; binz++){
      x[2]=zaxis->GetBinCenter(binz);
      for (Int_t biny=hyfirst; biny<=hylast; biny++) {
         x[1]=yaxis->GetBinCenter(biny);
         for (Int_t binx=hxfirst; binx<=hxlast; binx++) {

            x[0] = xaxis->GetBinCenter(binx);

            GetPDFs(x[0], Q2);
            hfit->SetBinContent(binx, biny, binz, x[0]*fPDFValues[q]);

            GetPDFErrors(x[0], Q2);
            err = fPDFErrors[q];
            if( err == 0.0 ) err = 0.000001;
            hfit->SetBinError(binx, biny, binz, x[0]*err);
         }
      }
   }

}

//void PolarizedPartonDistributionFunctions::PlotPDFs(double Qsq, Int_t log)
//{
//
//   const Int_t N = 30;
//   double xmin = 0.0001;
//   double xmax = 0.9999;
//   //double Qsqmin=1.0;
//   //double Qsqmax=12.0;
//   //double QMean=5.4;
//   double dx = (xmax - xmin) / ((double)N);
//   //double dQsq=(Qsqmax-Qsqmin)/((double)N);
//
//   double x = 0; //, val1,val2;
//   Int_t pointNumber = 0;
//
//   TMultiGraph *mg = new TMultiGraph();
//
//   TGraph * gUpVsX = new TGraph();
//   gUpVsX->SetTitle(Form("x#Deltau(x) [%s]", GetLabel()));
//   gUpVsX->SetMarkerColor(1);
//   gUpVsX->SetMarkerStyle(21);
//   gUpVsX->SetMarkerSize(1.3);
//   gUpVsX->SetLineColor(1);
//   gUpVsX->SetLineWidth(3);
//
//   TGraph * gDownVsX = new TGraph();
//   gDownVsX->SetMarkerColor(2);
//   gDownVsX->SetMarkerStyle(22);
//   gDownVsX->SetMarkerSize(1.3);
//   gDownVsX->SetLineColor(2);
//   gDownVsX->SetLineWidth(3);
//   gDownVsX->SetTitle(Form("x#Deltad(x) [%s]", GetLabel()));
//
//   TGraph * gStrangeVsX = new TGraph();
//   gStrangeVsX->SetMarkerColor(3);
//   gStrangeVsX->SetMarkerStyle(23);
//   gStrangeVsX->SetMarkerSize(1.3);
//   gStrangeVsX->SetLineColor(3);
//   gStrangeVsX->SetLineWidth(3);
//   gStrangeVsX->SetTitle(Form("x#Deltas(x) [%s]", GetLabel()));
//
//   TGraph * gUpbarVsX = new TGraph();
//   gUpbarVsX->SetMarkerColor(4);
//   gUpbarVsX->SetMarkerStyle(24);
//   gUpbarVsX->SetMarkerSize(1.3);
//   gUpbarVsX->SetLineColor(4);
//   gUpbarVsX->SetLineWidth(3);
//   gUpbarVsX->SetTitle(Form("x #Delta#bar{u}(x) [%s]", GetLabel()));
//
//   TGraph * gDownbarVsX = new TGraph();
//   gDownbarVsX->SetMarkerColor(5);
//   gDownbarVsX->SetMarkerStyle(25);
//   gDownbarVsX->SetMarkerSize(1.3);
//   gDownbarVsX->SetLineColor(5);
//   gDownbarVsX->SetLineWidth(3);
//   gDownbarVsX->SetTitle(Form("x#Delta#bar{d}(x) [%s]", GetLabel()));
//
//   TGraph * gStrangebarVsX = new TGraph();
//   gStrangebarVsX->SetMarkerColor(6);
//   gStrangebarVsX->SetMarkerStyle(26);
//   gStrangebarVsX->SetMarkerSize(1.3);
//   gStrangebarVsX->SetLineColor(6);
//   gStrangebarVsX->SetLineWidth(3);
//   gStrangebarVsX->SetTitle(Form("x#Delta#bar{s}(x) [%s]", GetLabel()));
//
//   //       u->SetMarkerColor(1);
//   //       aGraph->SetLineColor(2);
//   //       aGraph->SetLineWidth(3);
//   //       aGraph->SetMarkerSize(1.3);
//   //       aGraph->SetMarkerStyle(23);
//   //       aGraph->SetTitle(" ");
//   //       aGraph->GetXaxis()->SetTitle("Run Number");
//   //       aGraph->GetYaxis()->SetTitle(" ");
//   double * pdfvals;
//   pointNumber = 0;
//   for (int i = 0; i < N; i++) {
//      x = xmin + (double)i * dx;
//      pdfvals =  GetPDFs(x, Qsq);
//      gUpVsX->SetPoint(pointNumber, x, Deltau());
//      gUpbarVsX->SetPoint(pointNumber, x, Deltaubar());
//      gDownVsX->SetPoint(pointNumber, x, Deltad());
//      gDownbarVsX->SetPoint(pointNumber, x, Deltadbar());
//      gStrangeVsX->SetPoint(pointNumber, x, Deltas());
//      gStrangebarVsX->SetPoint(pointNumber, x, Deltasbar());
//      pointNumber++;
//   }
//
//
//
//   TCanvas *fCanvas = (TCanvas *) gROOT->FindObject(Form("cPolPdfs%d", (Int_t)Qsq));
//   if (!fCanvas) fCanvas = new TCanvas(Form("cPolPdfs%d", (Int_t)Qsq), "Polarzied PDFs", 0, 0, 600, 400);
//   fCanvas->cd(0)->SetLogx(log);
//
//   mg->Add(gUpVsX, "LP");
//   mg->Add(gDownVsX, "LP");
//   mg->Add(gStrangeVsX, "LP");
//   mg->Add(gUpbarVsX, "LP");
//   mg->Add(gDownbarVsX, "LP");
//   mg->Add(gStrangebarVsX, "LP");
//   mg->Draw("A");
//
//   TLegend * leg = new TLegend(0.7, 0.7, 0.95, 0.95);
//   leg->SetHeader(Form("%s Polarized PDFs", GetLabel()));
//   leg->AddEntry(gUpVsX, gUpVsX->GetTitle(), "lp");
//   leg->AddEntry(gDownVsX, gDownVsX->GetTitle(), "lp");
//   leg->AddEntry(gStrangeVsX, gStrangeVsX->GetTitle(), "lp");
//   leg->Draw();
//
//   TLatex * t = new TLatex();
//   t->SetNDC();
//   t->SetTextFont(62);
//   t->SetTextColor(36);
//   t->SetTextSize(0.06);
//   t->SetTextAlign(12);
//   t->DrawLatex(0.05, 0.95, Form("Q^{2} = %d GeV^{2}", (Int_t)Qsq));
//
//   //    fQsqText->AddText();
//   //   fQsqText->Draw();
//
//   //   fCanvas->Divide(3,2);
//   //   fCanvas->cd(1)->SetLogx(log);
//   //   gUpVsX->Draw("ALP");
//   //   fCanvas->cd(2)->SetLogx(log);
//   //   gDownVsX->Draw("ALP");
//   //   fCanvas->cd(3)->SetLogx(log);
//   //   gStrangeVsX->Draw("ALP");
//   //   fCanvas->cd(4)->SetLogx(log);
//   //   gUpbarVsX->Draw("ALP");
//   //   fCanvas->cd(5)->SetLogx(log);
//   //   gDownbarVsX->Draw("ALP");
//   //   fCanvas->cd(6)->SetLogx(log);
//   //   gStrangebarVsX->Draw("ALP");
//}
//_____________________________________________________________________________
}}
