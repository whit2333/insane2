#include "insane/pdfs/PDFBase.h"

namespace insane {
  namespace physics {

    PDFBase2::PDFBase2()
    { }
    //__________________________________________________________________________

    PDFBase2::~PDFBase2()
    { }
    //__________________________________________________________________________

    void PDFBase2::Reset()
    {
      for(int i = 0; i<NPartons ; i++) {
        m_PDFValues.fValues[i]        = 0.0;
        m_PDFValues.fUncertainties[i] = 0.0;
      }
      m_PDFValues.fx = -1.0;
      m_PDFValues.fQ2 = 0.0;
    }
    //______________________________________________________________________________

    bool PDFBase2::IsComputed(double x, double Q2) const
    {
      if( x  != m_PDFValues.fx ) return false;
      if( Q2 != m_PDFValues.fQ2 ) return false;
      return true;
      //return false;
    }
    //______________________________________________________________________________

    PDFValues  PDFBase2::Get(double x, double Q2) const 
    {
      if( !IsComputed(x, Q2) ) {
        Calculate(x,Q2);
        Uncertainties(x,Q2);
      }
      return m_PDFValues;
    }

    double  PDFBase2::Get(const Parton& f, double x, double Q2) const 
    {
      if( !IsComputed(x, Q2) ) {
        Calculate(x,Q2);
        Uncertainties(x,Q2);
      }
      return( m_PDFValues.fValues[int(f)] );
    }

    //double  PDFBase2::Get(Parton f, double x, double Q2) const
    //{
    //  if( !IsComputed(x, Q2) ) {
    //    Calculate(x,Q2);
    //    Uncertainties(x,Q2);
    //  }
    //  return m_PDFValues.fValues[static_cast<unsigned int>(f)];
    //}

    double  PDFBase2::Get(const Parton& f) const
    {
      return( m_PDFValues.fValues[int(f)] );
    }

    //double  PDFBase2::Get(Parton f) const 
    //{
    //  return m_PDFValues.fValues[static_cast<unsigned int>(f)];
    //}

    const std::array<double,NPartons>& PDFBase2::Calculate(double x, double Q2) const
    {
      // do calculations
      return m_PDFValues.fValues;
    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& PDFBase2::Uncertainties(double x, double Q2) const 
    {
      // do calculations
      return m_PDFValues.fUncertainties;
    }

  }
}


