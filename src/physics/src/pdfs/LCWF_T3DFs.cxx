#include "insane/pdfs/LCWF_T3DFs.h"

namespace insane {
  namespace physics {

    LCWF_T3DFs::LCWF_T3DFs() :
      fInputScale(1.0), 
      //fA(0.0486772), fB(1.57357), fC(5.94918), fD(6.74412), fE(2.19114),
      fParam_p( {0.0486772, 1.57357,  5.94918, 6.74412, 2.19114} ),
      fParam_n( {0.0655158, 0.130996, 1.12101, 2.31342, 1.20598} )
      {
      // Braun et al.

      // Monfared et al. http://inspirehep.net/record/1297075
      //fInputScale = 4.0;
      //fA = 0.034;
      //fB = 0.554;
      //fC =-0.387;
      //fD =-1.17;
      //fE = 0.969;

      // JAM http://inspirehep.net/record/1258401
      //fInputScale = 1.0;
      //fA = -0.0936;
      //fB = 0.2837;
      //fC = 0.7542;
      //fD = -1.5177;
      //fE = 0.0;
    }
    //______________________________________________________________________________

    LCWF_T3DFs::~LCWF_T3DFs()
    { }
    //__________________________________________________________________________

    double LCWF_T3DFs::g2pTwist3_model(double x) const
    {
      return( g2_model(x, fParam_p));
    }
    //__________________________________________________________________________
    double LCWF_T3DFs::g2nTwist3_model(double x) const
    {
      return( g2_model(x, fParam_n));
    }
    //__________________________________________________________________________
    
    double LCWF_T3DFs::g2_model(double x, const std::array<double,5>& p) const
    {
      double xbar = 1.0-x; 
      double  t1  = p[0]*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
      double  t2  = TMath::Power(xbar,3.0)*(p[1] - p[2]*xbar + p[3]*xbar*xbar - p[4]*xbar*xbar*xbar);
      return( t1 + t2 );
    }
    //______________________________________________________________________________

    //const std::array<double,NPartons>& LCWF_T3DFs::Calculate(double x, double Q2) const
    //{
    //  m_PDFValues.fx      = x;
    //  m_PDFValues.fQ2      = Q2;
    //  double  t3_val = 0.0;

    //  // The following is just a quick approximation ...   
    //  double Dp = (-1.0/x)*insane::integrate::gaus(
    //      [&](double z){
    //      return z*insane::math::derivative(
    //          [&](double y){return g2_model(y,fParam_p);},
    //          z);},
    //      x, 1.0);
    //  double Dn = (-1.0/x)*insane::integrate::gaus(
    //      [&](double z){
    //      return z*insane::math::derivative(
    //          [&](double y){return g2_model(y,fParam_n);},
    //          z);},
    //      x, 1.0);
    //  double Du = (9.0/15.0)*(4.0*Dp - Dn);
    //  double Dd = (9.0/15.0)*(4.0*Dn - Dp);

    //  fValues[PartonFlavor::kUP]   = Du;
    //  fValues[PartonFlavor::kDOWN] = Dd;

    //  // do calculations
    //  return fValues;
    //}
    ////______________________________________________________________________________

    //const std::array<double,NPartons>& LCWF_T3DFs::Uncertainties(double x, double Q2) const 
    //{
    //  // do calculations
    //  return fUncertainties;
    //}
    ////__________________________________________________________________________

    double LCWF_T3DFs::g2p_Twist3(double x, double Q2) const
    { 
      return g2pTwist3_model(x);
    }

    double LCWF_T3DFs::g2n_Twist3(double x, double Q2) const
    { 
      return g2nTwist3_model(x);
    }
    ////______________________________________________________________________________
    //
    //double LCWF_T3DFs::g1p_Twist3_TMC(double x, double Q2) const
    //{
    //  // Twist three part of g1p with full TMC treatment
    //  double xi        = insane::Kine::xi_Nachtmann(x  , Q2);
    //  double xi_thresh = insane::Kine::xi_Nachtmann(1.0, Q2);
    //  if(xi>xi_thresh) {return 0.0;}
    //  double M      = (insane::units::M_p/insane::units::GeV);
    //  double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
    //  double rho    = TMath::Sqrt(1.0+gamma2);
    //  double res    = (rho*rho-1.0)/(rho*rho*rho)*Dp_Twist3(xi,Q2);
    //  double t1     = (1.0-rho*rho)/(rho*rho*rho*rho);
    //  double integral_result =  0.0;
    //  //insane::integrate::gaus(
    //  //    [&](double z){
    //  //    return( ((3.0-(3.0-rho*rho)*TMath::Log(z/xi)/rho)/z)*Dp_Twist3(z,Q2) );
    //  //    }, xi,xi_thresh);
    //  return( res + t1*integral_result );
    //}
    ////______________________________________________________________________________
    //
    //double LCWF_T3DFs::g2p_Twist3_TMC(double x, double Q2) const
    //{
    //  // Twist three part of g1p with full TMC treatment
    //  double xi        = insane::Kine::xi_Nachtmann(x  , Q2);
    //  double xi_thresh = insane::Kine::xi_Nachtmann(1.0, Q2);
    //  if(xi>xi_thresh) {return 0.0;}
    //  double M      = (insane::units::M_p/insane::units::GeV);
    //  double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
    //  double rho    = TMath::Sqrt(1.0+gamma2);
    //  double res    = 1.0/(rho*rho*rho)*Dp_Twist3(xi,Q2);
    //  double t1     = (-1.0)/(rho*rho*rho*rho);
    //  double integral_result =  0.0;
    //  //insane::integrate::gaus(
    //  //    [&](double z){
    //  //    return(((3.0-2.0*rho*rho+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/rho)/z)*Dp_Twist3(z,Q2) );
    //  //    }, xi,xi_thresh);
    //  return( res + t1*integral_result );
    //}
    ////______________________________________________________________________________

    //double LCWF_T3DFs::g2n_Twist3(double x, double Q2) const
    //{ 
    //  double result          = Dn_Twist3(x, Q2);
    //  double integral_result = 0.0;
    //  //insane::integrate::gaus(
    //  //    [&](double z){
    //  //    return( Dn_Twist3(z,Q2)/z );
    //  //    }, x,1.0);
    //  return( result - integral_result );
    //}
    ////______________________________________________________________________________
    //
    //double LCWF_T3DFs::g1n_Twist3_TMC(double x, double Q2) const
    //{
    //  // Twist three part of g1n with full TMC treatment
    //  double xi        = insane::Kine::xi_Nachtmann(x  , Q2);
    //  double xi_thresh = insane::Kine::xi_Nachtmann(1.0, Q2);
    //  if(xi>xi_thresh) {return 0.0;}
    //  double M      = (insane::units::M_p/insane::units::GeV);
    //  double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
    //  double rho    = TMath::Sqrt(1.0+gamma2);
    //  double res    = (rho*rho-1.0)/(rho*rho*rho)*Dn_Twist3(xi,Q2);
    //  double t1     = (1.0-rho*rho)/(rho*rho*rho*rho);
    //  double integral_result =  0.0;
    //  //insane::integrate::gaus(
    //  //    [&](double z){
    //  //    return( ((3.0-(3.0-rho*rho)*TMath::Log(z/xi)/rho)/z)*Dn_Twist3(z,Q2) );
    //  //    }, xi,xi_thresh);
    //  return( res + t1*integral_result );
    //}
    ////______________________________________________________________________________

    //double LCWF_T3DFs::g2n_Twist3_TMC(double x, double Q2) const
    //{
    //  // Twist three part of g1n with full TMC treatment
    //  double xi        = insane::Kine::xi_Nachtmann(x  , Q2);
    //  double xi_thresh = insane::Kine::xi_Nachtmann(1.0, Q2);
    //  if(xi>xi_thresh) {return 0.0;}
    //  double M      = (insane::units::M_p/insane::units::GeV);
    //  double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
    //  double rho    = TMath::Sqrt(1.0+gamma2);
    //  double res    = 1.0/(rho*rho*rho)*Dn_Twist3(xi,Q2);
    //  double t1     = (-1.0)/(rho*rho*rho*rho);
    //  double integral_result =  0.0;
    //  //insane::integrate::gaus(
    //  //    [&](double z){
    //  //    return(((3.0-2.0*rho*rho+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/rho)/z)*Dn_Twist3(z,Q2) );
    //  //    }, xi,xi_thresh);
    //  return( res + t1*integral_result );
    //}
    ////______________________________________________________________________________


  }
}
