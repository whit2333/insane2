#include "insane/pdfs/LCWF_PPDFs.h"
#include "TMath.h"
namespace insane {
namespace physics {


LCWF_PPDFs::LCWF_PPDFs(){
   SetNameTitle("LCWFpolpdfs","LCWF polarized PDFs");
   SetLabel("LCWF Braun, et.al.");
   SetLineColor(1);
   SetLineStyle(1);
   // Parameters in Table 3 of Diehl etal.
   fuv3[0] = 40.0*140.0/87.0;
   fuv3[1] = -21.0/20.0;
   fuv3[2] = 9.0/40.0;
   fuv3[3] = 3.0;

   fdv3[0] = -140.0/87.0;
   fdv3[1] = 3.0;
   fdv3[2] = 9.0/5.0;
   fdv3[3] = 3.0;

   fuv4[0] = 16.0*990.0/37.0;
   fuv4[1] = -3.0/2.0;
   fuv4[2] = 9.0/16.0;
   fuv4[3] = 7.0;

   fdv4[0] = 0.0;
   fdv4[1] = 0.0;
   fdv4[2] = 0.0;
   fdv4[3] = 7.0;

   fuv5[0] = 40.0*264.0/29.0;
   fuv5[1] = -6.0/5.0;
   fuv5[2] = 27.0/80.0;
   fuv5[3] = 7.0;

   fdv5[0] = -264.0/29.0;
   fdv5[1] = 3.0/2.0;
   fdv5[2] = 1.0/2.0;
   fdv5[3] = 7.0;

   fP3 = 0.17;
   fP4 = 0.1;
   fP5 = 0.1;
   Info("LCWF_PPDFs","Note the LCWF pdfs do not evolve and are fixed at Q2 = 1(GeV/c)^2");
}
LCWF_PPDFs::~LCWF_PPDFs(){
}

double LCWF_PPDFs::ModelFunction(double x, double PN, double na, const double *pars) const{
   // eqn 58 of Deihl etal.
   // na = 1 for quarks, 3 for gluons
   double t1 = pars[0]*PN*TMath::Power(x,na)*TMath::Power(1.0-x,pars[3]);
   double t2 = 1.0+pars[1]*(1.0-x)+pars[2]*TMath::Power(1.0-x,2.0);
   return t1*t2;
}

    const std::array<double,NPartons>& LCWF_PPDFs::Calculate    (double x, double Q2) const
    {
      m_PDFValues.fx = x;
      m_PDFValues.fQ2 = Q2;
   double Q   = TMath::Sqrt(Q2);
   double res = 0.0;
   int i      = 0;

   double  sea = 0.0;//ModelFunction(x,fP5,1,fdv5)/3.0;

   double beta     = 0.052;
   double P3qg     = 0.33;
   double P3qgUp   = 0.20625;
   double P3qgDown = 0.12375;

   double d3qgDown = 56.0*P3qgDown*x*TMath::Power(1.0-x,6.0);
   double u3qgDown = 2.0*d3qgDown;
   double d3qgUp   = 56.0*P3qgUp*x*TMath::Power(1.0-x,6.0);
   double u3qgUp   = 2.0*d3qgUp;

   double delta_d3qgDown = d3qgDown;
   double delta_u3qgDown = 2.0*d3qgDown;
   double delta_d3qgUp   = -1.0*(1.0- beta*4.0/3.0)*d3qgUp;
   double delta_u3qgUp   = beta*2.0/3.0*u3qgUp;

   double xdeltag3qg    = 168.0*(P3qgUp - P3qgDown)*TMath::Power(x,3.0)*TMath::Power(1.0-x,5.0);

   // up quark 
   m_PDFValues.fValues[0] = ModelFunction(x,fP3,1,fuv3)
      + (delta_u3qgUp + delta_u3qgDown)
      + ModelFunction(x,fP5,1,fuv5);
   // down quark 
   m_PDFValues.fValues[1] = ModelFunction(x,fP3,1,fdv3)
      + (delta_d3qgUp + delta_d3qgDown)
      + ModelFunction(x,fP5,1,fdv5);
   // strange 
   m_PDFValues.fValues[2] = sea;
   // charm 
   m_PDFValues.fValues[3] = 0.0;
   // bottom
   m_PDFValues.fValues[4] = 0.0;
   // top 
   m_PDFValues.fValues[5] = 0.0;
   // gluon 
   m_PDFValues.fValues[6] = xdeltag3qg/x;
   // u-bar 
   m_PDFValues.fValues[7] = sea; 
   // d-bar 
   m_PDFValues.fValues[8] = sea; 
   // s-bar 
   m_PDFValues.fValues[9] = sea;
   // c-bar 
   m_PDFValues.fValues[10] = 0.0;
   // b-bar  
   m_PDFValues.fValues[11] = 0.0;
   // t-bar 
   m_PDFValues.fValues[12] = 0.0;



   return m_PDFValues.fValues;
}

    const std::array<double,NPartons>& LCWF_PPDFs::Uncertainties(double x, double Q2) const
    {

     m_PDFValues.fUncertainties[0] = 0.0;
     m_PDFValues.fUncertainties[1] = 0.0;
     m_PDFValues.fUncertainties[2] = 0.0;
     m_PDFValues.fUncertainties[3] = 0.0;
     m_PDFValues.fUncertainties[6] = 0.0;
     m_PDFValues.fUncertainties[7] = 0.0;
     m_PDFValues.fUncertainties[8] = 0.0;
     m_PDFValues.fUncertainties[9] = 0.0;
     m_PDFValues.fUncertainties[10] = 0.0;

      return(m_PDFValues.fUncertainties);

    }
    //______________________________________________________________________________

} // namespace physics
} // namespace insane
