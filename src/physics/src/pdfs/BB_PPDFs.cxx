#include "insane/pdfs/BB_PPDFs.h"
#include "insane/pdfs/FortranWrappers.h"
#include "TMath.h"

namespace insane {
  namespace physics {

    BB_PPDFs::BB_PPDFs() {
      SetNameTitle("BB_PPDFs", "BB pol. PDFs");
      SetLabel("BB");

      // These values are always zero.
      m_PDFValues.fValues[3]  = 0.0; // Delta b
      m_PDFValues.fValues[4]  = 0.0; // Delta b
      m_PDFValues.fValues[5]  = 0.0; // Delta t
      m_PDFValues.fValues[10] = 0.0; // Delta bbar
      m_PDFValues.fValues[11] = 0.0; // Delta bbar
      m_PDFValues.fValues[12] = 0.0; // Delta tbar

      for (Int_t i = 0; i < 12; i++)
        m_PDFValues.fUncertainties[i] = 0.;
    }

    BB_PPDFs::~BB_PPDFs() {}

    const std::array<double, NPartons>& BB_PPDFs::Calculate(double x, double Q2) const {

      int fiSet = 1;
      double fg1p, fDg1p, fg1n, fDg1n;
      m_PDFValues.fx  = x;
      m_PDFValues.fQ2 = Q2;
      double fPDFErrors[12];
      // auto vals = old_pdfs.GetPDFs(x,Q2);
      // for(Int_t i=0;i<12;i++) m_PDFValues.fValues[i] = vals[i];
      double qbar  = 0.0;
      double GL    = 0.0;
      double UV    = 0.0;
      double DV    = 0.0;
      double Dqbar = 0.0;
      ppdf_(&fiSet, &m_PDFValues.fx, &m_PDFValues.fQ2,
            /*UV=*/&UV, /*DUV=*/&fPDFErrors[0],
            /*DV=*/&DV, /*DDV=*/&fPDFErrors[1],
            /*GL=*/&GL, /*DGL=*/&fPDFErrors[6],
            /*QB=*/&qbar, /*DQB=*/&Dqbar, &fg1p, &fDg1p, &fg1n, &fDg1n);

      m_PDFValues.fValues[0] = (UV + qbar) / x; // Delta up
      m_PDFValues.fValues[1] = (DV + qbar) / x; // Delta down
      m_PDFValues.fValues[2] = qbar / x;        // Delta s
      m_PDFValues.fValues[6] = GL / x;          // Delta g
      m_PDFValues.fValues[7] = qbar / x;        // Delta ubar
      m_PDFValues.fValues[8] = qbar / x;        // Delta dbar
      m_PDFValues.fValues[9] = qbar / x;        // Delta sbar

      m_PDFValues.fUncertainties[0] =
          TMath::Sqrt(fPDFErrors[0] * fPDFErrors[0] + Dqbar * Dqbar) / x;
      m_PDFValues.fUncertainties[1] =
          TMath::Sqrt(fPDFErrors[1] * fPDFErrors[1] + Dqbar * Dqbar) / x;
      m_PDFValues.fUncertainties[2] = Dqbar / x;
      m_PDFValues.fUncertainties[6] = fPDFErrors[6] / x;
      m_PDFValues.fUncertainties[7] = Dqbar / x;
      m_PDFValues.fUncertainties[8] = Dqbar / x;
      m_PDFValues.fUncertainties[9] = Dqbar / x;

      m_PDFValues.fUncertainties[3]  = 0.0;
      m_PDFValues.fUncertainties[4]  = 0.0;
      m_PDFValues.fUncertainties[5]  = 0.0;
      m_PDFValues.fUncertainties[9]  = 0.0;
      m_PDFValues.fUncertainties[10] = 0.0;

      return (m_PDFValues.fValues);
    }
    //______________________________________________________________________________

    const std::array<double, NPartons>& BB_PPDFs::Uncertainties(double x, double Q2) const {
      return (m_PDFValues.fUncertainties);
    }
    //______________________________________________________________________________
  } // namespace physics
} // namespace insane
