#include "insane/pdfs/PartonDistributionFunctionsFromPHDs.h"

namespace insane {
namespace physics {

//______________________________________________________________________________
UnpolarizedPDFsFromPHDs::UnpolarizedPDFsFromPHDs(){
}
//______________________________________________________________________________
UnpolarizedPDFsFromPHDs::~UnpolarizedPDFsFromPHDs(){
}
//______________________________________________________________________________
double * UnpolarizedPDFsFromPHDs::GetPDFs(double x, double Q2){
   m_PDFValues.fx      = x;
   m_PDFValues.fQ2      = Q2;

   fPHDSet = fPHDs->GetU(x,Q2);
   fPDFValues[0]  = fPHDSet.fQ_Plus    + fPHDSet.fQ_Minus    ; //up
   fPDFValues[7]  = fPHDSet.fQbar_Plus + fPHDSet.fQbar_Minus ; //ubar

   fPHDSet = fPHDs->GetD(x,Q2);
   fPDFValues[1]  = fPHDSet.fQ_Plus    + fPHDSet.fQ_Minus    ; //d
   fPDFValues[8]  = fPHDSet.fQbar_Plus + fPHDSet.fQbar_Minus ; //dbar

   fPHDSet = fPHDs->GetS(x,Q2);
   fPDFValues[2]  = fPHDSet.fQ_Plus    + fPHDSet.fQ_Minus    ; //s
   fPDFValues[9]  = fPHDSet.fQbar_Plus + fPHDSet.fQbar_Minus ; //sbar

   fPDFValues[3]  = 0;//c
   fPDFValues[4]  = 0;//b
   fPDFValues[5]  = 0;//t

   fPHDSet = fPHDs->GetG(x,Q2);
   fPDFValues[6]  = fPHDSet.fQ_Plus    + fPHDSet.fQ_Minus    ; //gluon

   fPDFValues[10] = 0;//cbar
   fPDFValues[11] = 0;//bbar
   fPDFValues[12] = 0;//tbar
   return fPDFValues;
}
//______________________________________________________________________________
double * UnpolarizedPDFsFromPHDs::GetPDFErrors(double x, double Q2){
   m_PDFValues.fx = x;
   m_PDFValues.fQ2 = Q2;
   // No Errors yet. Need to implement!!!!!
   for(int i = 0;i< NPARTONDISTS; i++) fPDFErrors[i] = 0.0;
   return fPDFErrors;
}
//______________________________________________________________________________



//______________________________________________________________________________
PolarizedPDFsFromPHDs::PolarizedPDFsFromPHDs(){
}
//______________________________________________________________________________
PolarizedPDFsFromPHDs::~PolarizedPDFsFromPHDs(){
}
//______________________________________________________________________________
double * PolarizedPDFsFromPHDs::GetPDFs(double x, double Q2){
   m_PDFValues.fx      = x;
   m_PDFValues.fQ2      = Q2;

   fPHDSet = fPHDs->GetU(x,Q2);
   fPDFValues[0]  = fPHDSet.fQ_Plus    - fPHDSet.fQ_Minus    ; //up
   fPDFValues[7]  = fPHDSet.fQbar_Plus - fPHDSet.fQbar_Minus ; //ubar

   fPHDSet = fPHDs->GetD(x,Q2);
   fPDFValues[1]  = fPHDSet.fQ_Plus    - fPHDSet.fQ_Minus    ; //d
   fPDFValues[8]  = fPHDSet.fQbar_Plus - fPHDSet.fQbar_Minus ; //dbar

   fPHDSet = fPHDs->GetS(x,Q2);
   fPDFValues[2]  = fPHDSet.fQ_Plus    - fPHDSet.fQ_Minus    ; //s
   fPDFValues[9]  = fPHDSet.fQbar_Plus - fPHDSet.fQbar_Minus ; //sbar

   fPDFValues[3]  = 0;//c
   fPDFValues[4]  = 0;//b
   fPDFValues[5]  = 0;//t

   fPHDSet = fPHDs->GetG(x,Q2);
   fPDFValues[6]  = fPHDSet.fQ_Plus    - fPHDSet.fQ_Minus    ; //gluon

   fPDFValues[10] = 0;//cbar
   fPDFValues[11] = 0;//bbar
   fPDFValues[12] = 0;//tbar

   return fPDFValues;
}
//______________________________________________________________________________
double * PolarizedPDFsFromPHDs::GetPDFErrors(double x, double Q2){
   m_PDFValues.fx = x;
   m_PDFValues.fQ2 = Q2;
   // No Errors yet. Need to implement!!!!!
   for(int i = 0;i< NPARTONDISTS; i++) fPDFErrors[i] = 0.0;
   return fPDFErrors;
}
//______________________________________________________________________________
}}
