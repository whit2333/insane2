#ifndef PartonDistributionFunctionsFromPHDs_HH
#define PartonDistributionFunctionsFromPHDs_HH 1

#include "PartonDistributionFunctions.h"
#include "PolarizedPartonDistributionFunctions.h"
#include "PartonHelicityDistributions.h"
namespace insane {
namespace physics {


/** Unpolaried PDFs from the helicity distribution functions. */
class UnpolarizedPDFsFromPHDs : public PartonDistributionFunctions {
   private:
      PHDSet    fPHDSet;

   protected:
      PartonHelicityDistributions  * fPHDs; //!

   public:
      UnpolarizedPDFsFromPHDs();
      ~UnpolarizedPDFsFromPHDs();

      void SetPHDs(PartonHelicityDistributions * phd){ fPHDs = phd; }
      PartonHelicityDistributions* GetPHDs() const { return fPHDs ; }

      virtual double * GetPDFs(double x, double Q2);
      virtual double * GetPDFErrors(double x, double Q2);

   ClassDef(UnpolarizedPDFsFromPHDs,1)
};



/** Polaried PDFs from the helicity distribution functions. */
class PolarizedPDFsFromPHDs : public PolarizedPartonDistributionFunctions {
   private:
      PHDSet    fPHDSet;

   protected:
      PartonHelicityDistributions  * fPHDs; //!

   public:
      PolarizedPDFsFromPHDs();
      ~PolarizedPDFsFromPHDs();

      void SetPHDs(PartonHelicityDistributions * phd){ fPHDs = phd; }
      PartonHelicityDistributions* GetPHDs() const { return fPHDs; }

      virtual double * GetPDFs(double x, double Q2);
      virtual double * GetPDFErrors(double x, double Q2);

   ClassDef(PolarizedPDFsFromPHDs,1)
};
}}

#endif

