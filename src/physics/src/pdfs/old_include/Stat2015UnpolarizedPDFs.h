#ifndef Stat2015UnpolarizedPDFs_HH
#define Stat2015UnpolarizedPDFs_HH 2 

#include "PartonDistributionFunctions.h"
#include "FortranWrappers.h"
namespace insane {
namespace physics {

/** Statistical 2015 PDFs.
 * C. BOURRELY and J. SOFFER
 * arXiv:1502.02517[hep-ph] submitted to EPJC
 * http://inspirehep.net/record/1343508
 *
 * \ingroup ppdfs
 */
class Stat2015UnpolarizedPDFs : public PartonDistributionFunctions {

   private:
      int    fiPol;
      int    fiSingle;
      int    fiNum;
      double pdfs[13];

   public:

      Stat2015UnpolarizedPDFs(); 
      virtual ~Stat2015UnpolarizedPDFs(); 

      virtual double *GetPDFs(double x, double Q2); 
      virtual double *GetPDFErrors(double x, double Q2); 

      ClassDef(Stat2015UnpolarizedPDFs,2)
};
}}
#endif

