#ifndef INSANE_PHYSICS_JAM_PPDFS_HH
#define INSANE_PHYSICS_JAM_PPDFS_HH

#include "PPDFs.h"

namespace insane {
  namespace physics {

    class JAM_PPDFs : public PPDFs {
      public:
        JAM_PPDFs();
        virtual ~JAM_PPDFs();

        virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
        virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

        ClassDef(JAM_PPDFs,1)
    };
  }
}

#endif
