#ifndef LSS2006PolarizedPDFs_HH
#define LSS2006PolarizedPDFs_HH 2 

#include "PolarizedPartonDistributionFunctions.h"
#include "FortranWrappers.h"
namespace insane {
namespace physics {

/** LSS2006 NLO Polarized PDFs (broken)
 *
 *  \todo Doesn't work... crashes with dfint no idea why....
 *
 * \ingroup ppdfs
 */
class LSS2006PolarizedPDFs : public PolarizedPartonDistributionFunctions {

   private: 
      double Extrapolate(double,double,double,double,double);

   public:
      /**
       */
      LSS2006PolarizedPDFs();
      virtual ~LSS2006PolarizedPDFs();

      int fiSet;
      /** Virtual method should get all values of pdfs and set
       *  values of fX and fQsquared.
       *
       * \code
       *   OUTPUT:  UUB = x *(DELTA u + DELTA ubar)
       *            DDB = x *(DELTA d + DELTA dbar)
       *            SSB = x *(DELTA s + DELTA sbar)
       *            GL  = x * DELTA GLUON
       *            UV  = x * DELTA uv
       *            DV  = x * DELTA dv
       *            UB  = x * DELTA ubar
       *            DB  = x * DELTA dbar
       *            ST  = x * DELTA sbar
       *            g1p = x*g_1^proton = x*g1p_LT + x*g1p_HT
       *            g1pLT = x*g1p_{NLO+TMC}
       *            g1n = x*g_1^neutron = x*g1n_LT + x*g1n_HT
       *            g1nLT = x*g1n_{NLO+TMC}
       *
       *      NOTE: The valence parts DELTA uv, DELTA dv
       *            DELTA uv = (DELTA u + DELTA ubar) - 2*DELTA ubar
       *            DELTA dv = (DELTA d + DELTA dbar) - 2*DELTA dbar
       * \endcode
       *  Calls subroutine
       *  LSS2006(ISET,X,Q2,UUB,DDB,SSB,GL,UV,DV,UB,DB,ST,g1pLT,g1p,g1nLT,g1n).
       */
      double * GetPDFs(double,double); 
      double * GetPDFErrors(double /*x*/,double /*Q2*/){return fPDFErrors;}  

      ClassDef(LSS2006PolarizedPDFs,2)
};
}}
#endif


