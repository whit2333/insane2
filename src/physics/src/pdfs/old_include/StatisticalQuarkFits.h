#ifndef StatisticalQuarkFITS_H 
#define StatisticalQuarkFITS_H 

#include <cstdlib> 
#include <iostream> 
#include <iomanip> 
#include <cmath> 
#include "TMath.h"
#include "TString.h"
#include "FortranWrappers.h"
namespace insane {
namespace physics {

/** StatisticalQuarkFits.
 * StatisticalQuarkFits define helicity dependent quark distributions based on:
 * C. Bourrely, F. Buccella and J. Soffer, Eur. Phys. J. C 23 (2002) 487 (hep-ph/0109160)
 * C. Bourrely, F. Buccella and J. Soffer, Phys. Lett. B 648 (2007) 39 (hep-ph/0702221)
 *
 * \ingroup helicitydependentpartondistributions 
 * \ingroup StatisticalQuark
 */
class StatisticalQuarkFits {
   private:
      Bool_t fIsInterp; 
      /// Fit parameters
      /// xbar = temprature 
      /// X0ph = thermodynamical potential (p = parton, h = helicity)
      double fxbar, fX0uplus, fX0uminus, fX0dplus, fX0dminus;   ///< Fermi-Dirac fit pars
      double fdxbar,fdX0uplus, fdX0uminus,fdX0dplus,fdX0dminus; ///< errors
      double fb, fbtilda, fAtilda;                              ///< fit parameters
      double fdb, fdbtilda, fdAtilda;                           ///< errors
      double fA,fAbar,fAg;                                      ///< Normalization constants
      double fbbar, fk, fbg;
      double fdAbar, fdbbar, fdbg;                              ///< error
      double fX0splus,fX0sminus,fbs,fAtildas;                   ///< Strange quark parameters

      /// flavor = u,ubar,d,dbar,s,sbar
      /// helicity = -1 (anti-parallel) +1 (parallel) quark helicity
      /// x = x Bjorken
      /// Qsq = 4-momentum transfer Note: set to 4GeV^2 until DGLAP is ready
      /// Note quark and gluon distributions fitted as x*parton, so return x*parton/x
      double fQ0sq;                                             ///< Q2 value used for quark fits
      double FormQuark(TString ,Int_t , double , double);   ///< takes flavor,helicity,x,Q2 arguments 
      double FormGluon(double , double );                   ///< takes helicity,x,Q2 arguments
      double FormPolarizedGluon( Int_t, double , double ){return 0.0;} //takes helicity,x,Q2 arguments
      double CheckX(double );                                 ///< takes x as argument
      void FixWarning(double );                                 ///< takes Q2 value

   public: 
      StatisticalQuarkFits();
      virtual ~StatisticalQuarkFits();

      void UseQ2Interpolation(Bool_t ans=true){ 
         //if(ans==true)  std::cout << "[StatisticalQuarkFits]: Will do Q2 interpolation." << std::endl; 
         //if(ans==false) std::cout << "[StatisticalQuarkFits]: Will NOT do Q2 interpolation." << std::endl; 
         fIsInterp=ans;
      }
      Bool_t IsInterpolated() const {return fIsInterp;}
      double  GetUQuark( Int_t helicity, double x, double Qsq ){return FormQuark("u",helicity,x,Qsq);} 
      double  GetAntiUQuark( Int_t helicity, double x, double Qsq ){return FormQuark("ubar",helicity,x,Qsq);}
      double  GetDQuark( Int_t helicity, double x, double Qsq ){return FormQuark("d",helicity,x,Qsq);}
      double  GetAntiDQuark( Int_t helicity, double x, double Qsq ){return FormQuark("dbar",helicity,x,Qsq);}
      double  GetSQuark( Int_t helicity, double x, double Qsq ){return FormQuark("s",helicity,x,Qsq);}
      double  GetAntiSQuark( Int_t helicity, double x, double Qsq ){return FormQuark("sbar",helicity,x,Qsq);} 
      double  GetGluon( double x, double Qsq ){return FormGluon(x,Qsq);}
      double  GetPolarizedGluon( Int_t helicity, double x, double Qsq ){return FormPolarizedGluon(helicity,x,Qsq);}
      double  GetFermiDiracFunc(TString flavor, Int_t helicity, double x, double Qsq);

      /// These functions use the updated code from Soffer that utilizes Q2 dependence
      /// NOTE: x and Q2 are reversed in the fortran!
      ///       partondf_() returns x*p, where p is the parton distribution  
      double  GetParton(double x,double Q2,int part)           {return partondf_(&Q2,&x,&part);}   
      double  GetUQuarkInterp(double x,double Qsq)         {return (1./x)*GetParton(x,Qsq,1);} 
      double  GetDQuarkInterp(double x,double Qsq)         {return (1./x)*GetParton(x,Qsq,2);} 
      double  GetSQuarkInterp(double x,double Qsq)         {return (1./x)*GetParton(x,Qsq,3);} 
      double  GetAntiUQuarkInterp(double x,double Qsq)     {return (1./x)*GetParton(x,Qsq,4);} 
      double  GetAntiDQuarkInterp(double x,double Qsq)     {return (1./x)*GetParton(x,Qsq,5);} 
      double  GetAntiSQuarkInterp(double x,double Qsq)     {return (1./x)*GetParton(x,Qsq,6);} 
      double  GetGluonInterp(double x,double Qsq)          {return (1./x)*GetParton(x,Qsq,7);} 
      double  GetDeltaUQuarkInterp(double x,double Qsq)    {return (1./x)*GetParton(x,Qsq,8);} 
      double  GetDeltaDQuarkInterp(double x,double Qsq)    {return (1./x)*GetParton(x,Qsq,9);} 
      double  GetDeltaSQuarkInterp(double x,double Qsq)    {return (1./x)*GetParton(x,Qsq,10);} 
      double  GetDeltaAntiUQuarkInterp(double x,double Qsq){return (1./x)*GetParton(x,Qsq,11);} 
      double  GetDeltaAntiDQuarkInterp(double x,double Qsq){return (1./x)*GetParton(x,Qsq,12);} 
      double  GetDeltaAntiSQuarkInterp(double x,double Qsq){return (1./x)*GetParton(x,Qsq,13);} 
      double  GetDeltaGluonInterp(double x,double Qsq)     {return (1./x)*GetParton(x,Qsq,14);} 
      double  GetCQuarkInterp(double x,double Qsq)         {return (1./x)*GetParton(x,Qsq,15);} 
      double  GetDeltaCQuarkInterp(double x,double Qsq)    {return (1./x)*GetParton(x,Qsq,16);} 

      ClassDef(StatisticalQuarkFits,1)
};
}}
#endif 
