#ifndef LCWFPartonDistributionFunctions_HH
#define LCWFPartonDistributionFunctions_HH

#include "PartonDistributionFunctions.h"
#include "PolarizedPartonDistributionFunctions.h"
namespace insane {
namespace physics {

/** PDFs from light cone wave functions and fock state contributions.
 *
 *  References:
 *   - M. Diehl (DESY) , T. Feldmann (Wuppertal U.) , R. Jakob (Pavia U. & INFN, Pavia) , P. Kroll (Wuppertal U.)  Eur.Phys.J. C8 (1999) 409-434  
 *   - J. Bolz, P. Kroll (Wuppertal U.) Mar 1996 - 32 pages Z.Phys. A356 (1996) 327
 *   - V.M. Braun, T. Lautenschlager (Regensburg U.) , A.N. Manashov (Regensburg U. & St. Petersburg State U.) , B. Pirnay (Regensburg U.)
 *       Mar 2011 - 22 pages Phys.Rev. D83 (2011) 094023
 *
 *   ubar = dbar = s = sbar (5 quark fock states)
 *   and
 *   dbar = dv/3
 *
 *  The 3q-gluon distributions are the same as in Diehl etal, but the gluon distributions are modified to follow Braun etal. 
 *
 */   

class LCWFPartonDistributionFunctions : public PartonDistributionFunctions {

   protected:
      double fuv3[4]; // 3 quark fock state coefficients (Table 2)
      double fdv3[4]; // 3 quark fock state coefficients (Table 2)
      double fuv4[4]; // 4 quark fock state coefficients (Table 2)
      double fdv4[4]; // 4 quark fock state coefficients (Table 2)
      double fqg4[4]; // 3 quark gluon fock state coefficients (Table 2)
      double fuv5[4]; // 5 quark fock state coefficients (Table 2)
      double fdv5[4]; // 5 quark fock state coefficients (Table 2)

      double fP3,fP4,fP5;


   public: 
      LCWFPartonDistributionFunctions();
      virtual ~LCWFPartonDistributionFunctions();

      double   ModelFunction(double x, double PN, double na, double *pars);
      double * GetPDFs(double x ,double Q2) ;
      double * GetPDFErrors(double /*x*/,double /*Q2*/){ for(Int_t i=0;i<13;i++) fPDFErrors[i] = 0.; return fPDFErrors;}

   ClassDef(LCWFPartonDistributionFunctions,1)
};

/** PDFs from light cone wave functions and fock state contributions.
 *
 *  References:
 *   - M. Diehl (DESY) , T. Feldmann (Wuppertal U.) , R. Jakob (Pavia U. & INFN, Pavia) , P. Kroll (Wuppertal U.)  Eur.Phys.J. C8 (1999) 409-434  
 *   - J. Bolz, P. Kroll (Wuppertal U.) Mar 1996 - 32 pages Z.Phys. A356 (1996) 327
 *   - V.M. Braun, T. Lautenschlager (Regensburg U.) , A.N. Manashov (Regensburg U. & St. Petersburg State U.) , B. Pirnay (Regensburg U.)
 *       Mar 2011 - 22 pages Phys.Rev. D83 (2011) 094023
 *
 *  The 3q-gluon distributions are the same as in Diehl etal, but the gluon distributions are modified to follow Braun etal. 
 *  
 *
 */   

class LCWFPolarizedPartonDistributionFunctions : public PolarizedPartonDistributionFunctions {

   protected:
      double fuv3[4]; // 3 quark fock state coefficients (Table 3)
      double fdv3[4]; // 3 quark fock state coefficients (Table 3)
      double fuv4[4]; // 4 quark fock state coefficients (Table 3)
      double fdv4[4]; // 4 quark fock state coefficients (Table 3)
      double fuv5[4]; // 5 quark fock state coefficients (Table 3)
      double fdv5[4]; // 5 quark fock state coefficients (Table 3)

      double fP3,fP4,fP5;


   public: 
      LCWFPolarizedPartonDistributionFunctions();
      virtual ~LCWFPolarizedPartonDistributionFunctions();

      double   ModelFunction(double x, double PN, double na, double *pars);
      double * GetPDFs(double x ,double Q2);
      double * GetPDFErrors(double /*x*/,double /*Q2*/){ for(Int_t i=0;i<13;i++) fPDFErrors[i] = 0.; return fPDFErrors;}


   ClassDef(LCWFPolarizedPartonDistributionFunctions,1)
};

}}
#endif
