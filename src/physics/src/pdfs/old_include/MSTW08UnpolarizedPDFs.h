#ifndef MSTW08UNPOLARIZEDPDFS_H 
#define MSTW08UNPOLARIZEDPDFS_H

#include "PartonDistributionFunctions.h"
#include "FortranWrappers.h"
#include "TMath.h"
namespace insane {
namespace physics {

/** MSTW08 unpolarized parton distributions. 
  * 
  * Reference: A. D. Martin, W. J. Stirling, R. S. Thorne and G. Watt,
  *            "Parton distributions for the LHC", Eur. Phys. J. C63 (2009) 189-285
  * e-print:   arXiv:0901.0002 [hep-ph]
  *
  * \ingroup updfs
  */
class MSTW08UnpolarizedPDFs: public PartonDistributionFunctions{

   private: 
      int fPDFSet; 

      void Fit(double,double); 

   public: 
      MSTW08UnpolarizedPDFs(int iset=0);
      virtual ~MSTW08UnpolarizedPDFs();  

      double *GetPDFs(double,double); 
      double *GetPDFErrors(double,double); 

      void SetGrid(int i){fPDFSet = i;} 

      ClassDef(MSTW08UnpolarizedPDFs,1)

};
}}
#endif  
