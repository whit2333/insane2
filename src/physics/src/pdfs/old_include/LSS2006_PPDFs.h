#ifndef insane_physics_LSS2006_PPDFs_HH
#define insane_physics_LSS2006_PPDFs_HH 1 

#include "LSS2006PolarizedPDFs.h"
#include "PPDFs.h"

namespace  insane {
  namespace physics {

    class LSS2006_PPDFs : public PPDFs {
    protected:
      mutable LSS2006PolarizedPDFs   old_pdfs;
    public:
      LSS2006_PPDFs(); 
      virtual ~LSS2006_PPDFs(); 

      virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
      virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(LSS2006_PPDFs,2)
    };
  }
}

#endif

