#ifndef insane_physics_BB_PPDFs_HH
#define insane_physics_BB_PPDFs_HH 1 

#include "BBPolarizedPDFs.h"
#include "PPDFs.h"

namespace  insane {
  namespace physics {

    class BB_PPDFs : public PPDFs {
    protected:
      mutable BBPolarizedPDFs   old_pdfs;
    public:
      BB_PPDFs(); 
      virtual ~BB_PPDFs(); 

      virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
      virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(BB_PPDFs,2)
    };
  }
}

#endif

