#ifndef insane_physics_DNS2005_PPDFs_HH
#define insane_physics_DNS2005_PPDFs_HH 1 

#include "DNS2005PolarizedPDFs.h"
#include "PPDFs.h"

namespace  insane {
  namespace physics {

    class DNS2005_PPDFs : public PPDFs {
    protected:
      mutable DNS2005PolarizedPDFs   old_pdfs;
    public:
      DNS2005_PPDFs(); 
      virtual ~DNS2005_PPDFs(); 

      virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
      virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(DNS2005_PPDFs,2)
    };
  }
}

#endif

