#ifndef LSS2010PolarizedPDFs_HH
#define LSS2010PolarizedPDFs_HH 2 

#include "PolarizedPartonDistributionFunctions.h"
#include "FortranWrappers.h"
namespace insane {
namespace physics {

/** LSS2010 NLO Polarized PDFs.  
 *  A FORTRAN package containing two sets of polarized NLO parton densities 
 *  corresponding to positive and sign changing gluon densities are presented 
 *  in MS-bar scheme. The sets of PDFs are obtained from a combined NLO QCD 
 *  analysis of the world polarized inclusive and semi-inclusive DIS data. 
 *  Paper reference: Phys. Rev. D82 (2010) 114018 [arXiv:1010.0574]
 *
 *            ISET = 1   NEXT-TO-LEADING ORDER (xDelta G > 0)         
 *                      (DATA FILE 'NLO_MS_delGpos.grid' UNIT=11)     
 *                                                                    
 *            ISET = 2   NEXT-TO-LEADING ORDER (sign-changing         
 *                       xDelta G)                                    
 *                      (DATA FILE 'NLO_MS_chsign_delG.grid' UNIT=22) 
 *                                                                    
 *            X  = Bjorken-x       (between  1.E-5  and  1)           
 *            Q2 = scale in GeV**2 (between  1.0 and 0.58E6)          
 * 
 * \ingroup ppdfs
 */
class LSS2010PolarizedPDFs : public PolarizedPartonDistributionFunctions {

   private: 
      double Extrapolate(double,double,double,double,double);

   protected:
      int fiSet;

   public:
      /**
       */
      LSS2010PolarizedPDFs();
      virtual ~LSS2010PolarizedPDFs();

      /** Virtual method should get all values of pdfs and set
       *  values of fX and fQsquared.
       *
       * \code
       *   OUTPUT:  UUB = x *(DELTA u + DELTA ubar)
       *            DDB = x *(DELTA d + DELTA dbar)
       *            SSB = x *(DELTA s + DELTA sbar)
       *            GL  = x * DELTA GLUON
       *            UV  = x * DELTA uv
       *            DV  = x * DELTA dv
       *            UB  = x * DELTA ubar
       *            DB  = x * DELTA dbar
       *            ST  = x * DELTA sbar
       *
       *      NOTE: The valence parts DELTA uv, DELTA dv
       *            DELTA uv = (DELTA u + DELTA ubar) - 2*DELTA ubar
       *            DELTA dv = (DELTA d + DELTA dbar) - 2*DELTA dbar
       * \endcode
       *  Calls subroutine
       *  LSS2010(ISET,X,Q2,UUB,DDB,UV,DV,UB,DB,ST,GL)
       */
      double * GetPDFs(double,double); 
      double * GetPDFErrors(double,double){ return fPDFErrors; }  

      ClassDef(LSS2010PolarizedPDFs,2)
};
}}
#endif


