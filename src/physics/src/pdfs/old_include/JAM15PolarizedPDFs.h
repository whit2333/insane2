#ifndef JAM15PolarizedPDFs_HH
#define JAM15PolarizedPDFs_HH 2 

#include "PolarizedPartonDistributionFunctions.h"
#include "FortranWrappers.h"

namespace insane {
namespace physics {
/** JAM15.
 *
 * https://github.com/JeffersonLab/JAMLIB
 *
 * \ingroup ppdfs
 */
class JAM15PolarizedPDFs : public PolarizedPartonDistributionFunctions {
  private:
    int fiSet;
    double fg1p, fDg1p, fg1n, fDg1n;

  public:

    /** C'tor initializes the "pointer to a pointer" gradient matrix, fGrad,
     *  which returned by the subroutine
     */
    JAM15PolarizedPDFs(int set=0); 
    virtual ~JAM15PolarizedPDFs(); 

    /** Virtual method should get all values of pdfs and set
     *  values of fX and fQsquared
     *
     *  Calls
     *  PPDF(ISET, X, Q2,
     *  UV, DUV,
     *  DV, DDV,
     *  GL, DGL,
     *  QB, DQB,
     *  G1P, DG1P, G1N, DG1N)
     *  where quantities with a leading "D" are the corresponding errors.
     *  Note that the quark distributions are valence: q = (qvalence + qsea)
     */
    double *GetPDFs(double,double); 
    double *GetPDFErrors(double,double); 

    virtual double Hp_Twist4(double,double);
    virtual double Hn_Twist4(double,double);

    ClassDef(JAM15PolarizedPDFs,2)
};
}}

#endif

