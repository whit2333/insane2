#ifndef AvakianPolarizedPDFs_H
#define AvakianPolarizedPDFs_H

#include "AvakianQuarkHelicityDistributions.h"
#include "PolarizedPartonDistributionFunctions.h"

namespace insane {
namespace physics {

/** Avakian polarized parton distribution functions.  
  * A fit from H. Avakian, S. Brodsky, A. Deur and F. Yuan
  *
  * Paper reference: Phys. Rev. Lett. 99, 082001 (2007)  
  * 
  * \ingroup ppdfs
  */
class AvakianPolarizedPDFs: public PolarizedPartonDistributionFunctions{

   private: 
      AvakianQuarkHelicityDistributions fqhd; 

   public: 
      AvakianPolarizedPDFs();
      virtual ~AvakianPolarizedPDFs();

      double *GetPDFs(double,double); 
      double *GetPDFErrors(double /*x*/,double /*Q2*/){ for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.; return fPDFErrors;}

      ClassDef(AvakianPolarizedPDFs,1) 

};

}
}
#endif 
