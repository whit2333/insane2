#ifndef AvakianQuarkHelicityDistributions_H
#define AvakianQuarkHelicityDistributions_H

#include <cstdlib> 
#include <iostream> 
#include <iomanip>
#include <cmath>  
#include "TMath.h"
#include "PartonHelicityDistributions.h"

namespace insane {
namespace physics {

/** Quark helicity distribtuions at FIXED Q2 = 4.0 GeV2.
  *
  * Reference: Phys. Rev. Lett. 99, 082001 (2007)   
  *
  */
class AvakianQuarkHelicityDistributions : public PartonHelicityDistributions {

   private:
      /// polynomial coefficients 
      double fAu,fAd,fAs,fAg;
      double fBu,fBd,fBs,fBg;
      double fCu,fCd,fCs;
      double fDu,fDd,fDs;
      double fAlpha,fAlpha_g;
      double fCup,fCdp;  

   public: 
      AvakianQuarkHelicityDistributions(); 
      virtual ~AvakianQuarkHelicityDistributions();

      double uPlus(double);  
      double uMinus(double);  
      double dPlus(double);  
      double dMinus(double);  
      double sPlus(double);  
      double sMinus(double);  
      double gPlus(double);  
      double gMinus(double);  

      virtual Int_t CalculateDistributions(double x,double Q2);

      ClassDef(AvakianQuarkHelicityDistributions,1)
 
};

}
}

#endif 

