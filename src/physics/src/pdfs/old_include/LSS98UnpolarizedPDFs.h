#ifndef LSS98UnpolarizedPDFs_H
#define LSS98UnpolarizedPDFs_H

#include "LSS98QuarkHelicityDistributions.h"
#include "PartonDistributionFunctions.h"

namespace insane {
namespace physics {
/** LSS98 unpolarized parton distribution functions.  
  * A fit from E. Leader, A. Siderov, D. Stamenov 
  * 
  * Published in Int. J. Mod. Phys. A 13 (1998) 5573-5592
  * DOI: 10.1142/S0217751X98002547
  * e-Print: hep-ph/9708335 
  * Abstract: We have carried out a next-to-leading order QCD analysis of 
  * the experimental data on polarized DIS in the scheme. We have studied 
  * two models of the parametrizations of the input parton densities — the 
  * first due to Brodsky, Burkhardt and Schmidt (BBS), which gives a simultaneous 
  * parametrization for the polarized and unpolarized densities and in which the 
  * counting rules are strictly imposed; in the second, the input polarized densities 
  * are written in terms of the unpolarized ones in the generic form Δq(x)=f(x)q(x), 
  * with f(x) some simple smooth function. In both cases a good fit to the polarized 
  * data is achieved. As expected, the polarized data do not allow a precise determination 
  * of the polarized gluon density. Concerning the polarized sea quark densities, these 
  * are fairly well determined in the BBS model because of the interplay of polarized 
  * and unpolarized data, whereas in the second model, where only the polarized data 
  * are relevant, the polarized sea quark densities are largely undetermined.
  *
  * \ingroup updfs
  */
class LSS98UnpolarizedPDFs: public PartonDistributionFunctions{

   private: 
      LSS98QuarkHelicityDistributions fqhd; 

   public: 
      LSS98UnpolarizedPDFs();
      virtual ~LSS98UnpolarizedPDFs();

      double *GetPDFs(double,double); 
      double *GetPDFErrors(double x,double Q2){ for(Int_t i=0;i<13;i++) fPDFErrors[i] = 0.; return fPDFErrors;}

      ClassDef(LSS98UnpolarizedPDFs,1) 

};

}}
#endif 
