#ifndef StatisticalUNPOLARIZEDPDFS_H
#define StatisticalUNPOLARIZEDPDFS_H 1  

#include "PartonDistributionFunctions.h"
#include "FortranWrappers.h"
#include "TMath.h"
#include "StatisticalQuarkFits.h"
namespace insane {
namespace physics {

/** Statistical parton distribution functions.
 *
 * \ingroup updfs
 */
class StatisticalUnpolarizedPDFs : public PartonDistributionFunctions {

   private: 
      StatisticalQuarkFits fStatisticalFits; 

   public:

      StatisticalUnpolarizedPDFs(); 
      virtual ~StatisticalUnpolarizedPDFs(); 

      void UseQ2Interpolation(Bool_t ans=true){fStatisticalFits.UseQ2Interpolation(ans);} 

      double *GetPDFs(double,double); 
      double *GetPDFErrors(double /*x*/,double /*Q2*/){return fPDFErrors;}  

      ClassDef(StatisticalUnpolarizedPDFs,1)

}; 
}}
#endif 
