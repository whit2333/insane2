#ifndef AvakianUnpolarizedPDFs_H
#define AvakianUnpolarizedPDFs_H

#include "AvakianQuarkHelicityDistributions.h"
#include "PartonDistributionFunctions.h"
namespace insane {
namespace physics {

/** Avakian unpolarized parton distribution functions.  
  * A fit from H. Avakian, S. Brodsky, A. Deur and F. Yuan 
  *
  * Paper reference: Phys. Rev. Lett. 99, 082001 (2007)
  * 
  * \ingroup updfs
  */
class AvakianUnpolarizedPDFs: public PartonDistributionFunctions{

   private: 
      AvakianQuarkHelicityDistributions fqhd; 

   public: 
      AvakianUnpolarizedPDFs();
      virtual ~AvakianUnpolarizedPDFs();

      double *GetPDFs(double,double); 
      double *GetPDFErrors(double /*x*/,double /*Q2*/){ for(Int_t i=0;i<13;i++) fPDFErrors[i] = 0.; return fPDFErrors;}

      ClassDef(AvakianUnpolarizedPDFs,1) 

};
}
}

#endif 
