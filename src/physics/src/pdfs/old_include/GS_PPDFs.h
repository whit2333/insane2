#ifndef insane_physics_GS_PPDFs_HH
#define insane_physics_GS_PPDFs_HH 1 

#include "GSPolarizedPDFs.h"
#include "PPDFs.h"

namespace  insane {
  namespace physics {

    class GS_PPDFs : public PPDFs {
    protected:
      mutable GSPolarizedPDFs   old_pdfs;
    public:
      GS_PPDFs(); 
      virtual ~GS_PPDFs(); 

      virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
      virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(GS_PPDFs,2)
    };
  }
}

#endif

