#ifndef insane_physics_DSSV_PPDFs_HH
#define insane_physics_DSSV_PPDFs_HH 1 

#include "DSSVPolarizedPDFs.h"
#include "PPDFs.h"

namespace  insane {
  namespace physics {

    class DSSV_PPDFs : public PPDFs {
    protected:
      mutable DSSVPolarizedPDFs   old_pdfs;
    public:
      DSSV_PPDFs(); 
      virtual ~DSSV_PPDFs(); 

      virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
      virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(DSSV_PPDFs,2)
    };
  }
}

#endif

