#ifndef DNS2005PolarizedPDFs_HH
#define DNS2005PolarizedPDFs_HH 1

#include "PolarizedPartonDistributionFunctions.h"
#include "FortranWrappers.h"
namespace insane {
namespace physics {

/** DNS2005 LO and NLO Polarized PDFs
 *
 *  Uses the subroutine POLFIT which returns the valence up and down quark polarized pdfs,
 *  the anti-up and anti-down quark polarized pdfs and also the strange and gluon polarized
 *  pdfs are calculated. The full quark distributions are calculated from the valence and
 *  anti-quark distributions following the formula
 *  \f$ \Delta q(x) = \Delta q_v(x) + \Delta \bar{q}(x)\f$
 *
 *
 *
 * \ingroup ppdfs
 */
class DNS2005PolarizedPDFs : public PolarizedPartonDistributionFunctions {
   private:
      int fiSet;
      double fg1p, fDg1p, fg1n, fDg1n;

   public:

      /**
       */
      DNS2005PolarizedPDFs();
      virtual ~DNS2005PolarizedPDFs();

      /** Virtual method should get all values of pdfs and set
       *  values of fX and fQsquared
       *
       *  Calls
       *  POLFIT( MODE,X,Q2,
       *          DUV,DDV,
       *          DUBAR,DDBAR,
       *          DSTR,DGLU,
       *          G1P,G1N)
       *
       */
      double *GetPDFs(double,double);
      double *GetPDFErrors(double /*x*/,double /*Q2*/){ for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.; return fPDFErrors;}


      ClassDef(DNS2005PolarizedPDFs, 1)
};

}}

#endif

