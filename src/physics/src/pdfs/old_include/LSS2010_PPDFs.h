#ifndef insane_physics_LSS2010_PPDFs_HH
#define insane_physics_LSS2010_PPDFs_HH 1 

#include "LSS2010PolarizedPDFs.h"
#include "PPDFs.h"

namespace  insane {
  namespace physics {

    class LSS2010_PPDFs : public PPDFs {
    protected:
      mutable LSS2010PolarizedPDFs   old_pdfs;
    public:
      LSS2010_PPDFs(); 
      virtual ~LSS2010_PPDFs(); 

      virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
      virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(LSS2010_PPDFs,2)
    };
  }
}

#endif

