#ifndef LHAPDFPolarizedPDFs_HH
#define LHAPDFPolarizedPDFs_HH 2 

#include "PolarizedPartonDistributionFunctions.h"
#include "FortranWrappers.h"
#ifndef __CINT__
#include "LHAPDF/LHAPDF.h"
#else
namespace LHAPDF {
   enum SetType { EVOLVE = 0, LHPDF = 0, INTERPOLATE = 1, LHGRID = 1};
}
#endif

#include "TMath.h"

namespace insane {
namespace physics {

/** LHAPDF interface for concrete implementation of parton distributions
 *
 *  From the <a href="http://projects.hepforge.org/lhapdf/manual">LHAPDF Manual</a>:
 *  \code
 *  std::vector< double > xfx(const double &x;, const double &Q;);
 *  // returns a vector xf(x, Q) with index 0 < i < 12.
 *  // 0..5 = tbar, ..., ubar, dbar;
 *  // 6 = g;
 *  // 7..12 = d, u, ..., t
 *  double xfx(const double &x;, const double &Q;, int fl);
 *  // returns xf(x, Q) for flavour fl - this time the flavour encoding
 *  // is as in the LHAPDF manual...
 *  // -6..-1 = tbar,...,ubar, dbar
 *  // 1..6 = duscbt
 *  // 0 = g
 * \endcode
 *
 * \ingroup ppdfs
 */
class LHAPDFPolarizedPDFs : public PolarizedPartonDistributionFunctions {
	public:
		Int_t fSubset;
	public:
		LHAPDFPolarizedPDFs();
		virtual ~LHAPDFPolarizedPDFs(); 


		virtual double *GetPDFs(double x,double Qsq);
                virtual double *GetPDFErrors(double x,double Q2){ for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.; return fPDFErrors;}


		/** wrapper to set type = 0 = LHAPDF::LHGRID
		 *   type = 1 (or nonzero) = LHAPDF::EVOLVE
		 */
		void SetPDFType(const char * pdfset, Int_t type = 0) ;

		/** Change LHAPDF data set */
		void SetPDFDataSet(const char * pdfset, LHAPDF::SetType type = LHAPDF::LHGRID, Int_t subset = 0) ;

		/** up quark distribution, \f$ u(x) \f$ */
		//    virtual double u(double x, double Qsq) {
		//       return(LHAPDF::xfx(x, TMath::Sqrt(Qsq), 2)/x);
		//    }

		/** down quark distribution, \f$ d(x) \f$  */
		//    virtual double d(double x, double Qsq) {
		//       return(LHAPDF::xfx(x, TMath::Sqrt(Qsq), 1)/x);
		//    }

		/** up anti-quark distribution, \f$ \bar{u}(x) \f$  */
		//    virtual double uBar(double x, double Qsq) {
		//       return(LHAPDF::xfx(x, TMath::Sqrt(Qsq), -2)/x);
		//    }

		/** down anti-quark distribution, \f$ \bar{d}(x) \f$  */
		//    virtual double dBar(double x, double Qsq) {
		//       return(LHAPDF::xfx(x, TMath::Sqrt(Qsq), -1)/x);
		//    }

		ClassDef(LHAPDFPolarizedPDFs,2)
};


}}
#endif
