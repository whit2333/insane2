#ifndef MRST2001UnpolarizedPDFs_H 
#define MRST2001UnpolarizedPDFs_H 

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>
#include "PartonDistributionFunctions.h"
#include "FortranWrappers.h"
namespace insane {
namespace physics {

/** MRST2001 unpolarized parton distruction functions.
 *  reference: Eur. Phys. J. C 28, 455 (2003); arXiv: 0211080 [hep-ph] 
 *
 * \ingroup updfs
 */
class MRST2001UnpolarizedPDFs: public PartonDistributionFunctions{

   private:
      int fPDFSet; 
      double fuv,fubar;
      double fdv,fdbar;
      double fstr,fsbar;
      double fchm,fbot;
      double fglu; 
 
      void Fit(double,double); 
      double Extrapolate(double,double,double,double,double);

   public:
      MRST2001UnpolarizedPDFs(int iset=0);
      ~MRST2001UnpolarizedPDFs();

      /// inputs are x and Q2. 
      double *GetPDFs(double,double);
      double *GetPDFErrors(double,double);

      /// for switching between PDF sets (to calculate errors on observables that are combos of the PDFs). 
      void SetGrid(int i){fPDFSet = i;}

      ClassDef(MRST2001UnpolarizedPDFs,1)
};
}}
#endif  
