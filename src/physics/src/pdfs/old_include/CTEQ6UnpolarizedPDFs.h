#ifndef CTEQ6UNPOLARIZEDPDFS_H
#define CTEQ6UNPOLARIZEDPDFS_H 2  

#include "PartonDistributionFunctions.h"
#include "FortranWrappers.h"
#include "TMath.h"

namespace insane {
namespace physics {
/** CTEQ6 parton distribution functions.
 *
 * \ingroup updfs
 */
class CTEQ6UnpolarizedPDFs : public PartonDistributionFunctions {

	public:

		CTEQ6UnpolarizedPDFs(); 
		virtual ~CTEQ6UnpolarizedPDFs(); 

		double *GetPDFs(double,double); 
		double *GetPDFErrors(double /*x*/,double /*Q2*/){return fPDFErrors;}  

		ClassDef(CTEQ6UnpolarizedPDFs,2)

}; 

}
}
#endif 
