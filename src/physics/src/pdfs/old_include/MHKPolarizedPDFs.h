#ifndef MHKPolarizedPDFs_HH
#define MHKPolarizedPDFs_HH 2 

#include "PolarizedPartonDistributionFunctions.h"
#include "FortranWrappers.h"
#include "TSpline.h"
#include "TGraph.h"
namespace insane {
namespace physics {

/** Monfared et al http://inspirehep.net/record/1297075 
 *
 *
 * \ingroup ppdfs
 */
class MHKPolarizedPDFs : public PolarizedPartonDistributionFunctions {

   private:
      double fPars_uplus[5];
      double fPars_dplus[5];
      double fPars_g[5];
      double fPars_ubar[5];
      double fPars_dbar[5];
      double fPars_sbar[5];

      double fParsErr_uplus[5];
      double fParsErr_dplus[5];
      double fParsErr_g[5];
      double fParsErr_ubar[5];
      double fParsErr_dbar[5];
      double fParsErr_sbar[5];

      double fPars_Twist3_p[5];
      double fPars_Twist3_n[5];

      double fKnots_x_p[6];
      double fKnots_y_p[6];

      double fKnots_x_n[6];
      double fKnots_y_n[6];

      TGraph   * fh_Twist4_p_gr;
      TSpline3 * fh_Twist4_p_spline;
      TGraph   * fh_Twist4_n_gr;
      TSpline3 * fh_Twist4_n_spline;

      double fQ20;


   protected:

      double xDeltaf_model(double x,double *p){
         // Model has 4 parameters
         double etaf = p[0]; // first moment
         double af = p[1];
         double bf = p[2];
         double cf = p[3];
         double Nf = 1.0/( (1.0+cf*(af/(af+bf+1.0)))*TMath::Beta(af,bf+1.0));

         double res = Nf*etaf*TMath::Power(x,af)*TMath::Power(1.0-x,bf)*(1.0+cf*x);
         return res;
      }
      

   public:

      /** C'tor initializes the "pointer to a pointer" gradient matrix, fGrad,
       *  which returned by the subroutine
       */
      MHKPolarizedPDFs(); 
      virtual ~MHKPolarizedPDFs(); 

      virtual double g1p_Twist4_Q20(double x) {
         return 0.0;
         //double res = fh_Twist4_p_spline->Eval(x)/fQ20;
         ////std::cout << "h(x) = " << res << std::endl;
         //return res;
      }
      virtual double g1p_Twist4(double x, double Q2) {
         return 0.0;
         //double res = fh_Twist4_p_spline->Eval(x)/Q2;
         ////std::cout << "h(x) = " << res << std::endl;
         //return res;
      }
      virtual double g1n_Twist4_Q20(double x) {
         return 0.0;
         //double res = fh_Twist4_n_spline->Eval(x)/fQ20;
         //return res;
      }
      virtual double g1n_Twist4(double x, double Q2) {
         return 0.0;
         //double res = fh_Twist4_n_spline->Eval(x)/Q2;
         //return res;
      }

      virtual double g2p_Twist3_Q20(double x) {
         double xbar = 1.0-x; 
         double fA = fPars_Twist3_p[0];
         double fB = fPars_Twist3_p[1];
         double fC = fPars_Twist3_p[2]; 
         double fD = fPars_Twist3_p[3];
         double fE = fPars_Twist3_p[4];

         double  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         double  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }
      virtual double g2p_Twist3(double x,double Q2) {
         double xbar = 1.0-x; 
         double fA = fPars_Twist3_p[0];
         double fB = fPars_Twist3_p[1];
         double fC = fPars_Twist3_p[2];
         double fD = fPars_Twist3_p[3];
         double fE = fPars_Twist3_p[4];

         double  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         double  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }
      virtual double g2n_Twist3_Q20(double x) {
         double xbar = 1.0-x; 
         double fA = fPars_Twist3_n[0];
         double fB = fPars_Twist3_n[1];
         double fC = fPars_Twist3_n[2]; 
         double fD = fPars_Twist3_n[3];
         double fE = fPars_Twist3_n[4];

         double  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         double  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }
      virtual double g2n_Twist3(double x,double Q2) {
         double xbar = 1.0-x; 
         double fA = fPars_Twist3_n[0];
         double fB = fPars_Twist3_n[1];
         double fC = fPars_Twist3_n[2];
         double fD = fPars_Twist3_n[3];
         double fE = fPars_Twist3_n[4];

         double  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         double  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }

      /** Virtual method should get all values of pdfs and set
       *  values of fX and fQsquared
       */
      double *GetPDFs(double,double); 
      double *GetPDFErrors(double,double); 

      ClassDef(MHKPolarizedPDFs,2)
};
}}
#endif

