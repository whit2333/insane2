#ifndef insane_physics_BBS_PPDFs_HH
#define insane_physics_BBS_PPDFs_HH 1 

#include "BBSPolarizedPDFs.h"
#include "PPDFs.h"

namespace  insane {
  namespace physics {

    class BBS_PPDFs : public PPDFs {
    protected:
      mutable BBSPolarizedPDFs   old_pdfs;
    public:
      BBS_PPDFs(); 
      virtual ~BBS_PPDFs(); 

      virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
      virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(BBS_PPDFs,2)
    };
  }
}

#endif

