#ifndef StatisticalPOLARIZEDPDFS_H 
#define StatisticalPOLARIZEDPDFS_H 

#include <cstdlib> 
#include <iostream> 
#include <iomanip> 
#include <cmath> 
#include "PolarizedPartonDistributionFunctions.h"
#include "FortranWrappers.h"
#include "StatisticalQuarkFits.h"

namespace insane {
namespace physics {
/** Statistical Polarized parton distruction functions.
 *
 * \ingroup ppdfs
 */
class StatisticalPolarizedPDFs: public PolarizedPartonDistributionFunctions{

   private:
      /// quark distributions  
      /// NOTE: In the class PolarizedPDFs, the labeling in the array fPDFValues is: 
      ///  \f$ (0,1,2,3,4,5,6,7,8,9,10,11) = (u,d,s,c,b,t,g,\bar{u},\bar{d},\bar{s},\bar{c},\bar{b},\bar{t}) \f$
      double fdu,fdubar;        ///< delta u, u-bar (valence) 
      double fdd,fddbar;        ///< delta d, d-bar (valence)  
      double fds,fdsbar,fdglu;  ///< delta s, s-bar, delta g 

      StatisticalQuarkFits fStatisticalFits; 

      void Init();
      void FormPDFs(double,double);           // input is x, Q2 ONLY 

   public: 
      StatisticalPolarizedPDFs();
      ~StatisticalPolarizedPDFs();

      void UseQ2Interpolation(Bool_t ans=true){fStatisticalFits.UseQ2Interpolation(ans);} 

      double * GetPDFs(double x,double Qsq){ FormPDFs(x,Qsq); return (fPDFValues); } 
      double *GetPDFErrors(double /*x*/,double /*Q2*/){ for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.; return fPDFErrors;}

      ClassDef(StatisticalPolarizedPDFs,1)
};
}}
#endif 
