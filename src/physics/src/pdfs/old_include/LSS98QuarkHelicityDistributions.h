#ifndef LSS98QuarkHelicityDistributions_H
#define LSS98QuarkHelicityDistributions_H

#include <cstdlib> 
#include <iostream> 
#include <iomanip>
#include <cmath>  
#include "TMath.h"

namespace insane {
namespace physics {
class LSS98QuarkHelicityDistributions{

   private:
      /// polynomial coefficients 
      double fAu,fAd,fAs,fAg;
      double fBu,fBd,fBs,fBg;
      double fCu,fCd,fCs;
      double fDu,fDd,fDs;
      double fAlpha,fAlpha_g; 

   public: 
      LSS98QuarkHelicityDistributions(); 
      virtual ~LSS98QuarkHelicityDistributions();

      double uPlus(double);  
      double uMinus(double);  
      double dPlus(double);  
      double dMinus(double);  
      double sPlus(double);  
      double sMinus(double);  
      double gPlus(double);  
      double gMinus(double);  

      ClassDef(LSS98QuarkHelicityDistributions,1)
 
};
}}

#endif 
