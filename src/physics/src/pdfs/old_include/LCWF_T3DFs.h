#ifndef INSANE_PHYSICS_LCWF_T3DFs_HH
#define INSANE_PHYSICS_LCWF_T3DFs_HH


#include "T3DFsFromTwist3SSFs.h"

namespace insane {
  namespace physics {

    /** Light cone wave function model.
     *  Taken from eqution 109 of
     *     Higher twist parton distributions from light-cone wave functions 
     *     V.M. Braun, T. Lautenschlager (Regensburg U.), A.N. Manashov (Regensburg U. & St. Petersburg State U.), B. Pirnay (Regensburg U.). Mar 2011. 22 pp.
     *     Published in Phys.Rev. D83 (2011) 094023 
     *
     *  Just a simple model to see that evolution reporduces their results. 
     */ 
    class LCWF_T3DFs : public T3DFsFromTwist3SSFs {

      protected:
        double fInputScale; // Model input scale

        std::array<double,5> fParam_p;
        std::array<double,5> fParam_n;

      protected:

        double g2_model(double x, const std::array<double,5>& p) const;


        //{
        //  double xbar = 1.0-x; 
        //  double  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
        //  double  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
        //  //double  t1  = 0.0486772*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
        //  //double  t2  = TMath::Power(xbar,3.0)*(1.57357 - 5.94918*xbar + 6.74412*xbar*xbar - 2.19114*xbar*xbar*xbar);
        //  //std::cout << "calling model g2ptwist3" << std::endl;
        //  return t1 + t2;
        //}

      public:
        LCWF_T3DFs();
        virtual ~LCWF_T3DFs();

        double g2pTwist3_model(double x) const ;
        double g2nTwist3_model(double x) const ;

        //virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
        //virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

        virtual double g2p_Twist3(    double x, double Q2) const;
        //virtual double g2p_Twist3_TMC(double x, double Q2) const;
        //virtual double g1p_Twist3(    double x, double Q2) const{ return 0.0; }
        //virtual double g1p_Twist3_TMC(double x, double Q2) const;

        virtual double g2n_Twist3(    double x, double Q2) const ;
        //virtual double g2n_Twist3_TMC(double x, double Q2) const;
        //virtual double g1n_Twist3(    double x, double Q2) const{ return 0.0; }
        //virtual double g1n_Twist3_TMC(double x, double Q2) const;

        ClassDef(LCWF_T3DFs,1)
    };
  }
}

#endif

