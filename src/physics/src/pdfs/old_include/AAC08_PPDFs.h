#ifndef insane_physics_AAC08_PPDFs_HH
#define insane_physics_AAC08_PPDFs_HH 1

#include "AAC08PolarizedPDFs.h"
#include "PartonDistributionFunctions.h"
#include "PolarizedPartonDistributionFunctions.h"
#include "FortranWrappers.h"
#include "PPDFs.h"

namespace insane {
  namespace physics {

    class AAC08_PPDFs : public PolarizedPDFs {
    protected:
      mutable AAC08PolarizedPDFs   old_pdfs;
    public:
      AAC08_PPDFs(); 
      virtual ~AAC08_PPDFs(); 

      virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
      virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(AAC08_PPDFs,2)
    };


  }
}
#endif
