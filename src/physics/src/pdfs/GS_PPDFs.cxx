#include "insane/pdfs/GS_PPDFs.h"

namespace insane {
  namespace physics {
    
    GS_PPDFs::GS_PPDFs(){
      SetNameTitle("GS_PPDFs","GS pol. PDFs");
      SetLabel("GS");

      // These values are always zero.
      m_PDFValues.fValues[4] = 0.0;//Delta b
      m_PDFValues.fValues[5] = 0.0;//Delta t
      m_PDFValues.fValues[11] = 0.0;//Delta bbar
      m_PDFValues.fValues[12] = 0.0;//Delta tbar

      for(Int_t i=0;i<12;i++) m_PDFValues.fUncertainties[i] = 0.; 

   nloini_();
   // These values are always zero.
    }
    
    GS_PPDFs::~GS_PPDFs()
    { }
    
    const std::array<double,NPartons>& GS_PPDFs::Calculate    (double x, double Q2) const
    {

      m_PDFValues.fx = x;
      m_PDFValues.fQ2 = Q2;

   int fiSet = 1;
   double uval = 0.0;
   double dval = 0.0;
   double glue = 0.0;
   double ubar = 0.0;
   double dbar = 0.0;
   double str = 0.0;
   polnlo_(&fiSet, &m_PDFValues.fx, &m_PDFValues.fQ2, &uval, &dval, &glue, &ubar, &dbar, &str);
   m_PDFValues.fValues[0] = (uval + ubar)/x; //Delta up
   m_PDFValues.fValues[1] = (dval + dbar)/x; //Delta down
   m_PDFValues.fValues[2] = str/x;      //Delta s
   m_PDFValues.fValues[6] = glue/x;       //Delta g
   m_PDFValues.fValues[7] = ubar/x;     //Delta ubar
   m_PDFValues.fValues[8] = dbar/x;     //Delta dbar
   m_PDFValues.fValues[9] = str/x;      //Delta sbar

      m_PDFValues.fUncertainties[0] = 0.0;
      m_PDFValues.fUncertainties[1] = 0.0;
      m_PDFValues.fUncertainties[2] = 0.0;
      m_PDFValues.fUncertainties[3] = 0.0;
      m_PDFValues.fUncertainties[6] = 0.0;
      m_PDFValues.fUncertainties[7] = 0.0;
      m_PDFValues.fUncertainties[8] = 0.0;
      m_PDFValues.fUncertainties[9] = 0.0;
      m_PDFValues.fUncertainties[10] = 0.0;

      return(m_PDFValues.fValues);

    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& GS_PPDFs::Uncertainties(double x, double Q2) const
    {

      m_PDFValues.fUncertainties[0] = 0.0;
      m_PDFValues.fUncertainties[1] = 0.0;
      m_PDFValues.fUncertainties[2] = 0.0;
      m_PDFValues.fUncertainties[3] = 0.0;
      m_PDFValues.fUncertainties[6] = 0.0;
      m_PDFValues.fUncertainties[7] = 0.0;
      m_PDFValues.fUncertainties[8] = 0.0;
      m_PDFValues.fUncertainties[9] = 0.0;
      m_PDFValues.fUncertainties[10] = 0.0;

      return(m_PDFValues.fUncertainties);

    }
    //______________________________________________________________________________
  }
}

