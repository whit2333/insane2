#include "insane/pdfs/DSSV_PPDFs.h"

namespace insane {
  namespace physics {

    DSSV_PPDFs::DSSV_PPDFs(int iset) {
      SetNameTitle("DSSV_PPDFs", "DSSV pol. PDFs");
      SetLabel("DSSV");

      dssvini_(&fiSet);

      for (Int_t i = 0; i < 12; i++) {
        m_PDFValues.fUncertainties[i] = 0.;
        m_PDFValues.fValues[i]        = 0.0;
      }
    }

    DSSV_PPDFs::~DSSV_PPDFs() {}

    const std::array<double, NPartons>& DSSV_PPDFs::Calculate(double x, double Q2) const {
      // These values are always zero.
      m_PDFValues.fValues[3]  = 0.0; // Delta c
      m_PDFValues.fValues[4]  = 0.0; // Delta b
      m_PDFValues.fValues[5]  = 0.0; // Delta t
      m_PDFValues.fValues[10] = 0.0; // Delta cbar
      m_PDFValues.fValues[11] = 0.0; // Delta bbar
      m_PDFValues.fValues[12] = 0.0; // Delta tbar

      m_PDFValues.fx  = x;
      m_PDFValues.fQ2 = Q2;
      // Calculate all quark distributions here
      // WARNING: dssvfit returns x*f(x), so we divide by x!
      // std::cout << "[DSSV]: " << x << std::endl;

      // if Q2 < 0.8, we do a linear interpolation...
      // k-1 point is Q2 = 1.0
      // k point is Q2 = 0.80
      //

      double fduv   = 0.;
      double fddv   = 0.;
      double fdubar = 0.;
      double fddbar = 0.;
      double fdstr  = 0.;
      double fdglu  = 0.;

      //using quark      = insane::physics::Parton;
      //const auto& q_id = to_underlying<quark>;

      if (Q2 < 1.0) {

        double Q2_km1 = 1.2;
        double Q2_k   = 1.00;
        double duv_km1, ddv_km1, dubar_km1, ddbar_km1, dstr_km1, dglu_km1;
        double duv_k, ddv_k, dubar_k, ddbar_k, dstr_k, dglu_k;

        // Not sure why this extrapolation is needed.
        dssvfit_(&x, &Q2_km1, &duv_km1, &ddv_km1, &dubar_km1, &ddbar_km1, &dstr_km1, &dglu_km1);
        dssvfit_(&x, &Q2_k, &duv_k, &ddv_k, &dubar_k, &ddbar_k, &dstr_k, &dglu_k);
        fduv   = Extrapolate(Q2, Q2_km1, duv_km1, Q2_k, duv_k);
        fddv   = Extrapolate(Q2, Q2_km1, ddv_km1, Q2_k, ddv_k);
        fdubar = Extrapolate(Q2, Q2_km1, dubar_km1, Q2_k, dubar_k);
        fddbar = Extrapolate(Q2, Q2_km1, ddbar_km1, Q2_k, ddbar_k);
        fdstr  = Extrapolate(Q2, Q2_km1, dstr_km1, Q2_k, dstr_k);
        fdglu  = Extrapolate(Q2, Q2_km1, dglu_km1, Q2_k, dglu_k);

      } else {

        dssvfit_(&x, &Q2, &fduv, &fddv, &fdubar, &fddbar, &fdstr, &fdglu);
      }

      // NOTE: In the class PolarizedPDFs, the labeling in the array fPDFValues is:
      //  \f$ (0,1,2,3,4,5,6,7,8,9,10,11) =
      //  (u,d,s,c,b,t,g,\bar{u},\bar{d},\bar{s},\bar{c},\bar{b},\bar{t}) \f$

      if (x > 0.0) {
        fduv *= 1.0 / x;
        fdubar *= 1.0 / x;
        fddv *= 1.0 / x;
        fddbar *= 1.0 / x;
        fdstr *= 1.0 / x;
        fdglu *= 1.0 / x;
      }

      for (int i = 0; i < 13; i++) {
        m_PDFValues.fValues[i] = 0;
      }

      // Convert q_v to q
      //  q_v = q - qbar => q = q_v + qbar
      m_PDFValues.fValues[0]  = (fduv + fdubar);
      m_PDFValues.fValues[1]  = (fddv + fddbar);
      m_PDFValues.fValues[2]  = fdstr;
      m_PDFValues.fValues[6]  = fdglu;
      m_PDFValues.fValues[7]  = fdubar;
      m_PDFValues.fValues[8]  = fddbar;
      m_PDFValues.fValues[9]  = fdstr;
      m_PDFValues.fValues[3]  = 0.0; // Delta c
      m_PDFValues.fValues[4]  = 0.0; // Delta b
      m_PDFValues.fValues[5]  = 0.0; // Delta t
      m_PDFValues.fValues[10] = 0.0; // Delta cbar
      m_PDFValues.fValues[11] = 0.0; // Delta bbar
      m_PDFValues.fValues[12] = 0.0; // Delta tbar

      // for(Int_t i=0;i<12;i++) {
      //  m_PDFValues.fValues[i] = vals[i];
      //}
      // NOTE: In the class PolarizedPDFs, the labeling in the array fPDFValues is:
      //  \f$ (0,1,2,3,4,5,6,7,8,9,10,11) =
      //  (u,d,s,c,b,t,g,\bar{u},\bar{d},\bar{s},\bar{c},\bar{b},\bar{t}) \f$

      return (m_PDFValues.fValues);
    }
    //______________________________________________________________________________

    const std::array<double, NPartons>& DSSV_PPDFs::Uncertainties(double x, double Q2) const {

      m_PDFValues.fUncertainties[0]  = 0.0;
      m_PDFValues.fUncertainties[1]  = 0.0;
      m_PDFValues.fUncertainties[2]  = 0.0;
      m_PDFValues.fUncertainties[3]  = 0.0;
      m_PDFValues.fUncertainties[6]  = 0.0;
      m_PDFValues.fUncertainties[7]  = 0.0;
      m_PDFValues.fUncertainties[8]  = 0.0;
      m_PDFValues.fUncertainties[9]  = 0.0;
      m_PDFValues.fUncertainties[10] = 0.0;

      return (m_PDFValues.fUncertainties);
    }
    //______________________________________________________________________________
double DSSV_PPDFs::Extrapolate(double x,double x_km1,double f_km1,double x_k,double f_k) const{
   // extrapolation to point x, using the k-1 and k points preceeding x 
   double f = f_km1 + ( (x-x_km1)/(x_k - x_km1) )*(f_k - f_km1);
   return f;  
}
  } // namespace physics
} // namespace insane
