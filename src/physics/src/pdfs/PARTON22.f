!
!	!!! Compile with g77 -c -g -fcase-upper -fno-underscoring -malign-double -fno-automatic -finit-local-zero -fdebug-kludge
!
	IMPLICIT REAL*8 (A-H,L-Z)
        REAL*8 UX(121),DX(121),SX(121),XPDF(-6:6),G1P(121)
	REAL*8 UVX(121),DVX(121),UVS(121),DVS(121),SS(121)
        REAL*8 DM(10),XK(10),SCALE
	REAL*4 F2,F2P,F2D,ST,SY,STH,SYH,STD,SYD,X,QSQ,SLOPE,DSLOPE
        REAL*4 SLOPEH,DSLOPEH,SLOPED,DSLOPED,Y,RX,DRX,QSQR
!        REAL*4 X43(100),Q443(100),Q743(100)
        REAL*4 X43(200),Q443(200),Q743(200)
	CHARACTER*1 SEL,TARGET,MOD,SELF,PARD,OUR,EMC,TABLE,CHENG,XBIN
	CHARACTER PARC*4,SIGN*1,TESTR*1,FUNCT*2,FNP*1,QSQF*1,SORM*1
	LOGICAL GOODFIT, GOODFITH, GOODFITD,GOODR
        INTEGER MODEL,LOGUN1
        CHARACTER*1 SPEC7,SELX,SEAF
        
!        DATA MP/0.93828D0/
	DATA LAMBDA2/0.031329/	! QCD Lambda^2 (D-O L = 177 MeV)
        DATA PI/3.1415928/

C	For EMC's F2

	DATA DM/
     * 0.784, 0.699, 0.633, 0.562, 0.459, 0.358, 
     * 0.295, 0.246, 0.216, 0.216/

	DATA XK /	! x's greater than EMC data for logical IF
     * 0.020, 0.030, 0.040, 0.060, 0.100, 0.150,
     * 0.200, 0.300, 0.400, 0.700/

        WRITE(*,'('' E143 bins ("Y") or own("RETURN")?'')')
        READ(*,150) XBIN

        IF(XBIN.EQ.'Y') THEN

        OPEN(UNIT=34,FORM='FORMATTED',STATUS='OLD',
     *   FILE='XQSQ.DAT')
!	 DO JX=1,34
	 JX=1
         DO WHILE(JX.GE.1)
	 READ(34,152,END=444) X43(JX),Q443(JX),Q743(JX)
152	 FORMAT(3F9.4)
	 JX=JX+1
	 END DO
444	 CLOSE(34)
!         JMAX = JX-1
         JMAX = JX
         JXMAX = JMAX

        WRITE(*,'('' 7 deg ("Y") or 4.5("RETURN")?'')')
        READ(*,150) SPEC7

        ELSE     

        WRITE(*,'('' Enter x_b increment (min. 0.01)'')'
     *)
        READ(*,151) XI
151     FORMAT(G16.3)

        IF(XI.LE.0.0) XI = 0.01d0
        XSM = 20.0d0*(0.01d0/XI)
        jxsm = xsm
        JXMAX = 1.0D0/XI + JXSM + 1
        SCALE = 0.05d0*20.0D0/JXSM



        END IF

        WRITE(*,'('' Distributions ("Y") or F_2 ("RETURN")?'')')
	READ(*,150) SELF
150	FORMAT(A)


	IF(SELF.EQ.'Y') THEN

	WRITE(*,'('' Enter Q^2 [GeV/c]^2'')')
        READ (*,151) QSQR

        WRITE(*,'('' DFLM parton distributions ("Y") or D-Owens ("RETUR
     *N")?'')')
	READ(*,150) PARD

	IF(PARD.EQ.'Y') THEN
	PARC = 'DFLM'
	
	WRITE(*,'('' Our DFLM distributions ("Y") or DESY ("RETURN")?''
     *)')
	READ(*,150) OUR
	LAMBDA2 = 0.2**2
	NF = 6.0D0

	IF(OUR.NE.'Y') THEN
	
	WRITE(*,'('' u-d quark sea (-1) or s-quark sea (-3)?'')')
	READ (*,151) IS

	END IF!OUR

	ELSE

	PARC = 'D-O '

	
	WRITE(*,'('' Our D-O parton distributions ("Y") or DESY ("RETUR
     *N")?'')')
	READ(*,150) OUR
	LAMBDA2 = 0.177**2
	NF = 4.0D0

	END IF!PARD

	
	WRITE(*,'('' EMC F2 and R ("Y") or LWWs (RETURN)'')')
	READ(*,150) EMC


	
	WRITE(*,'('' Plot q(x) ("Y") or xq(x) ("RETURN")?'')')
	READ(*,150) SELX

!       ASQ2 = 12.0D0/(27.0D0*LOG(QSQR/LAMBDA2))   ! => alpha_s/(pi)
        ASQ2 = 12.0D0/((33.0D0-2.0D0*NF)*LOG(QSQR/LAMBDA2))! => alpha_s/(pi)
	E = 200.0D0

	DO JX = 1,JXMAX

!        IF(JX.LE.21) THEN
        IF(JX.LE.(JXSM+1)) THEN
!        X = XI*(JX-1)*.05
        X = XI*(JX-1)*SCALE
        ELSE
!        X = XI*(JX-20)
        X = XI*(JX-JXSM)
        END IF

C	Compute fitted EMC g_1

	IF(X.GT.0.) THEN

	IF(EMC.EQ.'Y') THEN

!	IF(X.LE.0.08) THEN	! EMC energies and average Q^2's
!	Q2 = 10.3
!	ELSE
!	Q2 = 0.0
!	END IF
!	Y = Q2/(2.0D0*MP*E*X)	! Approximate Y
!	IF(Y.GT.1.0D0) Y = 1.0D0

	JY = 1
	DO WHILE (XK(JY).LE.X)
	JY = JY + 1
	END DO
!	JY = JY - 1

	RX = 0.0122/(X+0.041)**1.096

	Y = (1.0D0 + DM(JY) + RX*DM(JY) -
     *  SQRT(1.0D0 - DM(JY)**2 + (RX*DM(JY))**2))/
     * (1.0D0 + DM(JY))


	CALL F2EMC(X,Y,RX,F2)

	ELSE

C	LWW's F2 and R

        CALL R1990(X,QSQR,RX,DRX,GOODR)
        CALL F2GLOB(X,QSQR,'H',12,F2,ST,SY,SLOPE,DSLOPE,GOODFIT)

	END IF	! End EMC F2 and R option

	A1EMC = 1.025*X**0.12*(1 - EXP(-2.7*X))		! Paper
	G1P(JX) = F2*A1EMC/(2.*X*(1.+RX))
!	G1P(JX) = F2*A1EMC/(2.*X)!*(1.+RX))

!	WRITE(*,140) X,Y,F2,G1P(JX)
140	FORMAT(4G16.3)

	END IF

C	DFLM

	IF(PARD.EQ.'Y') THEN
	IF(OUR.EQ.'Y') THEN
	ONEMINX = 1.0D0 - X
	XUV = 2.26*X**0.54*ONEMINX**2.52*(1.-1.617*ONEMINX+
     *   3.467*ONEMINX**2 - 1.998*ONEMINX**3)
	XDV = 0.57*ONEMINX*XUV
	XS = 0.70*ONEMINX**8.5*(1.0D0 + X*(-4.18 + 20.3*X
     *   - 15.3*X**2))
	XUVDV = XUV + XDV
	XS = XS/2.8D0
	UVX(JX) = XUV
	DVX(JX) = XDV

	ELSE

C...ISET = 1 - LO,              Lambda=0.2 GeV, N_f=6                 
C...       2 - NLO, DIS scheme, Lambda=0.3 GeV, N_f=6                 
C...X          - Bjorken x                                            
C...Q2         - square of the momentum scale  (in GeV**2)            
C...XPDF(-6:6) - matrix containing  x*p(x,Q2)                         
C...     IPDF = -6 ,  -5 ,  -4 ,  -3 ,  -2 ,  -1 ,0 ,1,2,3,4,5,6      
C...          t_bar,b_bar,c_bar,s_bar,u_bar,d_bar,gl,d,u,s,c,b,t      
	IF(X.GT.0.) THEN
	ISET = 1
        CALL PDDFLM(ISET,X,QSQR,XPDF)
	XUVDV = XPDF(2) + XPDF(1) - 2.0D0*XPDF(-1)
	XDV = XPDF(1) - XPDF(-1)
	XS = XPDF(IS)
	UVX(JX) = XPDF(2) - XPDF(-2)
	DVX(JX) = XPDF(1) - XPDF(-1)
	END IF
	END IF

	IF (SELX.EQ.'Y') THEN

	IF(X.GT.0.) THEN
	UX(JX) = (XUVDV - XDV)/X
	DX(JX) = XDV/X
	SX(JX) = XS/X
	UVS(JX) = XPDF(2)/X
	DVS(JX) = XPDF(1)/X
	SS(JX) = 2.0D0*XPDF(IS)/X
	END IF

	ELSE
	UX(JX) = XUVDV - XDV
	DX(JX) = XDV
	SX(JX) = XS
	UVS(JX) = XPDF(2)
	DVS(JX) = XPDF(1)
	SS(JX) = 2.0D0*XPDF(IS)
	END IF
 
	ELSE	! Duke-Owens

	IF(OUR.EQ.'Y') THEN
        CALL PARTONS(X,QSQR,XUVDV,XDV,XS)
	UVX(JX) = XUVDV - XDV
	DVX(JX) = XDV
	ELSE

C...ISET = 1 - DO(1)  , Lambda=0.200 GeV, Nfl=4
C...       2 - DO(2)  ,       =0.400 GeV,    =4
C...       3 - DO(1.1),       =0.177 GeV,    =4
C...X          - Bjorken x
C...Q2         - square of the momentum scale  (in GeV**2)
C...XPDF(-6:6) - matrix containing  x*p(x,Q2)
C...     IPDF = -6 ,  -5 ,  -4 ,  -3 ,  -2 ,  -1 ,0 ,1,2,3,4,5,6
C...          t_bar,b_bar,c_bar,s_bar,u_bar,d_bar,gl,d,u,s,c,b,t
C...range of validity:
C...     D-04 < X  < 1
C...      4   < Q2 < D4  GeV^2

	IF(X.GT.0.) THEN
	ISET = 3
        CALL PDDO(ISET,X,QSQR,XPDF)
	XUVDV = XPDF(2) + XPDF(1) - 2.0D0*XPDF(-1)
	XDV = XPDF(1) - XPDF(-1)
	XS = XPDF(-1)*6.0D0
	UVX(JX) = XPDF(2) - XPDF(-2)
	DVX(JX) = XPDF(1) - XPDF(-1)
	END IF
	END IF

	IF (SELX.EQ.'Y') THEN

	IF(X.GT.0.) THEN
	UX(JX) = (XUVDV - XDV)/X
	DX(JX) = XDV/X
	SX(JX) = XS/X/6.0D0
	UVS(JX) = XPDF(2)/X
	DVS(JX) = XPDF(1)/X
	SS(JX) = 2.0D0*XPDF(-1)/X
	END IF

	ELSE
	UX(JX) = XUVDV - XDV
	DX(JX) = XDV
	SX(JX) = XS/6.0D0
	UVS(JX) = XPDF(2)
	DVS(JX) = XPDF(1)
	SS(JX) = 2.0D0*XPDF(-1)
	END IF

	END IF

	END DO

	ELSE

	
	WRITE(*,'('' Deuterium ("D"), or hydrogen ("RETURN")?'')')
	READ(*,150) TARGET
	IF(TARGET.NE.'D') THEN
	TARGET = 'H'
	END IF

	WRITE(*,'('' Compute R ("Y") or F2 ("RETURN")?'')')
	READ(*,150) TESTR
	FUNCT = 'F2'

	IF(TESTR.EQ.'Y') THEN

	FUNCT = 'R '

	ELSE
	
	WRITE(*,'('' Compute F2n/F2p ("Y") or F2 ("RETURN")?'')')
	READ(*,150) FNP

	
	WRITE(*,'('' SLAC F2 ("Y") or NMC F2 ("RETURN")?'')')
	READ(*,150) SORM

        IF(SORM.EQ.'Y') THEN
	WRITE(*,'('' Model Omega-9 ("Y"), or Lambda-12 ("RETURN")?'')')
	READ(*,150) MOD
	MODEL = 12
	IF(MOD.EQ.'Y') MODEL = 9

        ELSE
        LAMBDA2=0.25**2
        END IF!SORM

	END IF!TESTR

	
        WRITE(*,'('' Fixed Q^2 ("Y") or Q^2(x) ("RETURN")?'')')
	READ(*,150) QSQF

	IF(QSQF.NE.'Y') THEN
	
        IF(XBIN.NE.'Y') THEN
1111    CONTINUE
        WRITE(*,'('' Enter beam energy [GeV], scatt. angle [deg]'')')
        READ (*,151) E,TH
        IF(E.EQ.0..OR.TH.EQ.0) GO TO 1111
        S2=SIN(TH*PI/360)**2
        END IF!XBIN
        
	ELSE
	
	WRITE(*,'('' Enter Q^2 [GeV/c]^2'')')
        READ (*,151) QSQR

	END IF!QSQF

!	END IF
	

!        CALL LIB$GET_LUN(LOGUN1)
        logun1 = 33

        OPEN(UNIT=LOGUN1,FORM='FORMATTED',STATUS='UNKNOWN',
     *   FILE='F2RX.TOP')

	WRITE(LOGUN1,192)
192	FORMAT('  SET SIZE 13.1 BY 10.',/,'  SET ORDER X Y',/,
     * '  SET LIMITS X 0.01 1. Y 0.0  1.0',/,
     * '  SET SCALE X LOG')
	IF(QSQF.NE.'Y') THEN
	WRITE(LOGUN1,193) TARGET
193	FORMAT('  TITLE 8. 8. SIZE 1.5  ',''' Target, [x - Q^2 - F2 - d
     *F2] '
     * ,A3,''' ')
	ELSE
        WRITE(LOGUN1,1931) TARGET,QSQR
1931	FORMAT('  TITLE 8. 8. SIZE 1.5  ',''' Target, Q^2 ',A3,
     * F8.2,''' ')
	END IF
!        WRITE(LOGUN1,195) JXMAX-2       

        WRITE(LOGUN1,195) JXMAX-2,FUNCT,FUNCT,FUNCT,TARGET,QSQR
!195     FORMAT ('( Format=XYY ASCII  Curves=3 Points=',I3,/)
195     FORMAT ('( Format=XYY ASCII Curves=3 Points=',I3,'\\',/,
     * '("Title="' ,A2,' structure function;Xaxis=x_B;"',
     * '"Yaxis=',A2,';Label=',A2,A1,F8.2,'"',/,
     * '(     "X0"        "Y0"         "Y1"        "Y2"')

        IF(XBIN.EQ.'Y') JXMAX = JMAX

	DO JX = 1,JXMAX

        IF(XBIN.NE.'Y') THEN


!        IF(JX.LE.19) THEN
        IF(JX.LE.(JXSM+1)) THEN
!        X = XI*(JX-1)*.05
!        X = XI*(JX-1)*.05 + .001
        X = XI*(JX-1)*SCALE
        ELSE
!        X = XI*(JX-20)
!        X = XI*(JX-18)
        X = XI*(JX-JXSM)
        END IF!jx
        
        ELSE
        
        X=X43(JX)
        
        END IF!jx
        
!	IF(JX.GT.1.AND.X.LE.1.0D0) THEN
	IF(X.GT.0.0.AND.X.LE.1.0D0) THEN

	IF(QSQF.NE.'Y') THEN
	EF =  E/(1.0D0+2.0D0*E*S2/(.939D0*X))
	QSQ = 4.0D0*E*EF*S2

        IF(XBIN.EQ.'Y') THEN
        
        IF(SPEC7.EQ.'Y') THEN
        QSQ=Q743(JX)
        ELSE
        QSQ=Q443(JX)
	END IF!spec7

	END IF!xbin

        ELSE
        QSQ=QSQR
        END IF!qsqf

!        IF(QSQ.NE.0.) THEN
        IF(QSQ.GE.LAMBDA2) THEN
        IF(TESTR.EQ.'Y') THEN

	IF(QSQF.NE.'Y') THEN
        CALL R1990(X,QSQ,RX,DRX,GOODR)
        WRITE(LOGUN1,697) X,QSQ,RX,DRX
	ELSE
        CALL R1990(X,QSQR,RX,DRX,GOODR)
        WRITE(LOGUN1,677) X,RX,DRX
	END IF!qsqf

        WRITE(*,100) X,QSQ,RX,DRX
	WRITE(*,101) GOODR

        ELSE

        IF(SORM.EQ.'Y') THEN
        CALL F2GLOB(X,QSQ,TARGET,MODEL,F2,ST,SY,SLOPE,DSLOPE,GOODFIT)
        ELSE
!        IF(QSQ.GE.LAMBDA2)
        CALL F2NMC(X,QSQ,TARGET,F2)
        ENDIF!sorm
        
        IF(X.LT.1.) THEN

        IF(SORM.EQ.'Y') THEN
	CALL F2GLOB(X,QSQ,'H',MODEL,F2P,STH,SYH,SLOPEH,DSLOPEH,GOODFITH
     *)
	CALL F2GLOB(X,QSQ,'D',MODEL,F2D,STD,SYD,SLOPED,DSLOPED,GOODFITD
     *)
        ELSE
!        IF(QSQ.GE.LAMBDA2) THEN
        CALL F2NMC(X,QSQ,'H',F2P)
        CALL F2NMC(X,QSQ,'D',F2D)
        ENDIF!sorm

	F2NP=2.0D0*F2D/F2P-1.0D0
!	DF2NP=2.0D0*F2D*SQRT((STD**2+SYD**2)+(STH**2+SYH**2))/
!     * (2.0D0*F2D-F2P)
	DF2NP=2.0D0*SQRT((STD**2+SYD**2)+(STH**2+SYH**2)*(F2D/F2P)**2)/
     * (2.0D0*F2D-F2P)
	END IF!x

	WRITE(*,100) X,QSQ,F2,SY
	WRITE(*,100) X,QSQ,F2NP,DF2NP
	WRITE(*,101) GOODFIT
101	FORMAT(' Fit: ',L1)

	IF(QSQF.NE.'Y') THEN


	IF(FNP.EQ.'Y') THEN
	WRITE(LOGUN1,697) X,QSQ,F2NP,DF2NP
697	FORMAT(4G13.3)
	ELSE
	WRITE(LOGUN1,697) X,QSQ,F2,SY
	ENDIF!fnp

	ELSE

!	WRITE(LOGUN1,677) X,F2
       IF(SORM.EQ.'Y') THEN
	WRITE(LOGUN1,677) X,F2,SY
677     FORMAT(F8.3,2G13.3)
	ELSE
	WRITE(LOGUN1,677) X,F2
	ENDIF!sorm

	ENDIF!qsqf


	END IF	! End of R or F2
        END IF!qsq.ne.0
	END IF	! End of JX > 1 or x.gt.0

	END DO
	WRITE(LOGUN1,'(''  JOIN SOLID'')')

	CLOSE (LOGUN1)

	END IF!self
        

	IF (SELF.EQ.'Y') THEN

	
	WRITE(*,'('' Display table ("Y") or not (RETURN)?'')')
	READ(*,150) TABLE

	IF(TABLE.EQ.'Y') THEN

	DO JX = 1,JXMAX

        IF(JX.LE.(JXSM+1)) THEN
!        X = XI*(JX-1)*.05
        X = XI*(JX-1)*SCALE
	ELSE
!        X = XI*(JX-20)
        X = XI*(JX-JXSM)
        END IF

	WRITE(*,100) X,UX(JX),DX(JX),SX(JX)
100	FORMAT(5G16.6)
	END DO

	END IF
 
	END IF

C	Plot 

	
	WRITE(*,'('' Plot distributions? "P" = yes, "RETURN" = no'')')
	READ(*,150) SEL

	IF(SEL.EQ.'P') THEN

	
	WRITE(*,'('' Plot sea flavor quantitites ("Y") or not (RETURN)?
     *'')')
	READ(*,150) SEAF

	IF(SEAF.NE.'Y') THEN

	
	WRITE(*,'('' Plot Cheng xds(x) ("Y") or fit xds(x) (RETURN)?'')
     *')
	READ(*,150) CHENG

	IF(CHENG.NE.'Y') THEN
	
	WRITE(*,'('' Enter Callaway-Ellis x_o, p_u, p_d parameters'')')
	READ (*,151) X0,PU,PD
	
	WRITE(*,'('' Sign of x*ds(x): (-) = "-", + = RETURN'')')
	READ(*,150) SIGN
	SI = 1.0D0
	IF(SIGN.EQ.'-') SI = -1.0D0
	END IF

!        CALL LIB$GET_LUN(LOGUN1)
        LOGUN1=34

	OPEN(LOGUN1,FORM='FORMATTED',STATUS='NEW',FILE='DOPD.TOP')
	WRITE(LOGUN1,111)
111	FORMAT('  SET SIZE 13.1 BY 10.',/,'  SET ORDER X Y DY',/,
     *	'  SET LIMITS X 0. 1. Y 0. 1.0')
	WRITE(LOGUN1,112) QSQ,PARC
112	FORMAT('  TITLE 8. 8. SIZE 1.5  ',''' Q223 = 
     *',F7.1,' PD: ',A4,''' ')
	WRITE(LOGUN1,113)
113	FORMAT('  CASE ','''  X X')


        IF(XI.GT.0.01) JXMAX =120
        DO JX =1,JXMAX,3
	IF(JX.LE.19) THEN
	X = XI*(JX-1)*.05
	X1 = XI*(JX)*.05
	X2 = XI*(JX+1)*.05
	ELSE
	X = XI*(JX-20)
	X1 = XI*(JX-19)
	X2 = XI*(JX-18)
	END IF
	WRITE(LOGUN1,110) X,UX(JX),X1,UX(JX+1),X2,UX(JX+2)
110	FORMAT(3(G12.5,G13.5,';'))
	END DO

	WRITE(LOGUN1,'(''  JOIN SOLID'')')

	DO JX =1,JXMAX,3
!       IF(JX.LE.21) THEN
!	X = XI*(JX-1)*.05
!	ELSE
!	X = XI*(JX-21)
!	END IF
!	WRITE(LOGUN1,110) X,DX(JX)
	IF(JX.LE.19) THEN
	X = XI*(JX-1)*.05
	X1 = XI*(JX)*.05
	X2 = XI*(JX+1)*.05
	ELSE
	X = XI*(JX-20)
	X1 = XI*(JX-19)
	X2 = XI*(JX-18)
	END IF
	WRITE(LOGUN1,110) X,DX(JX),X1,DX(JX+1),X2,DX(JX+2)
	END DO

	WRITE(LOGUN1,'(''  JOIN DASHES'')')

	DO JX =1,JXMAX,3
	IF(JX.LE.19) THEN
	X = XI*(JX-1)*.05
	X1 = XI*(JX)*.05
	X2 = XI*(JX+1)*.05
	ELSE
	X = XI*(JX-20)
	X1 = XI*(JX-19)
	X2 = XI*(JX-18)
	END IF
	WRITE(LOGUN1,110) X,SX(JX),X1,SX(JX+1),X2,SX(JX+2)
	END DO

	WRITE(LOGUN1,'(''  JOIN DOTDASH'')')

!	X0 = 0.75
!	PU = 0.287
!	PD = 0.76

	C1125 = 11.0D0/25.0D0
	C1325 = 13.0D0/25.0D0
	C2325 = 23.0D0/25.0D0

	C3AS = 3.0D0*(1 + C1125*ASQ2)

	DO JX =1,JXMAX,3
	IF(JX.LE.19) THEN
	X = XI*(JX-1)*.05
	X1 = XI*(JX)*.05
	X2 = XI*(JX+1)*.05
	ELSE
	X = XI*(JX-20)
	X1 = XI*(JX-19)
	X2 = XI*(JX-18)
	END IF

	IF(CHENG.EQ.'Y') THEN
	DSX = 11.8*X**0.94*(1.0D0 - X)**5*SX(JX)
	DSX1 = 11.8*X1**0.94*(1.0D0 - X1)**5*SX(JX+1)
	DSX2 = 11.8*X2**0.94*(1.0D0 - X2)**5*SX(JX+2)

	ELSE

!	DSXA = 18.D0*X*G1P(JX)/(1.0D0 - ASQ2) 
!	DSXB = 4.0D0*X**PU*UVX(JX) +
!     * (X - X0)*X**PD*DVX(JX)/(1.0D0 - X0)
!	DSX = SI*(DSXA - DSXB)/6.0D0

	DSXA = 18.D0*X*G1P(JX)
	DSXB = 4.0D0*X**PU*UVX(JX)*(1.0D0 - C1325*ASQ2) +
     * (X - X0)*X**PD*DVX(JX)*(1.0D0 + C2325*ASQ2)/(1.0D0 - X0)
	DSX = SI*(DSXA - DSXB)/C3AS
	WRITE(*,140) X,DSXA,DSXB,DSX
	DSXA = 18.D0*X1*G1P(JX+1)
	DSXB = 4.0D0*X1**PU*UVX(JX+1)*(1.0D0 - C1325*ASQ2) +
     * (X1 - X0)*X1**PD*DVX(JX+1)*(1.0D0 + C2325*ASQ2)/
     * (1.0D0 - X0)
	DSX1 = SI*(DSXA - DSXB)/C3AS
	DSXA = 18.D0*X2*G1P(JX+2)
	DSXB = 4.0D0*X2**PU*UVX(JX+2)*(1.0D0 - C1325*ASQ2) +
     * (X2 - X0)*X2**PD*DVX(JX+2)*(1.0D0 + C2325*ASQ2)/
     * (1.0D0 - X0)
	DSX2 = SI*(DSXA - DSXB)/C3AS

!	DSXA = 18.D0*X1*G1P(JX+1)/(1.0D0 - ASQ2) 
!	DSXB = 4.0D0*X1**PU*UVX(JX+1) +
!     * (X1 - X0)*X1**PD*DVX(JX+1)/(1.0D0 - X0)
!	DSX1 = SI*(DSXA - DSXB)/6.0D0
!	DSXA = 18.D0*X2*G1P(JX+2)/(1.0D0 - ASQ2) 
!	DSXB = 4.0D0*X2**PU*UVX(JX+2) +
!     * (X2 - X0)*X2**PD*DVX(JX+2)/(1.0D0 - X0)
!	DSX2 = SI*(DSXA - DSXB)/6.0D0
!	DSX1 = SI*(18.D0*X1*G1P(JX+1)/(1.0D0 - ASQ2) -
!     * 4.0D0*X1**PU*UVX(JX+1) -
!     * (X1 - X0)*X1**PD*DVX(JX+1)/(1.0D0 - X0))/6.0D0
!	DSX2 = SI*(18.D0*X2*G1P(JX+2)/(1.0D0 - ASQ2) -
!     * 4.0D0*X2**PU*UVX(JX+2) -
!     * (X2 - X0)*X2**PD*DVX(JX+2)/(1.0D0 - X0))/6.0D0

!	DSX2 = SI*(X2*G1P(JX+2) - (1 - ASQ2)*
!     * (4.0D0*X2**PU*UVX(JX+2) + 
!     * (X2 - X0)*X2**PD*DVX(JX+2)/(1 - X0))/18.D0)/6.0D0

	END IF

	WRITE(LOGUN1,110) X,DSX,X1,DSX1,X2,DSX2
	END DO

	WRITE(LOGUN1,'(''  JOIN SOLID'')')

	DO JX =1,JXMAX,3
	IF(JX.LE.19) THEN
	X = XI*(JX-1)*.05
	X1 = XI*(JX)*.05
	X2 = XI*(JX+1)*.05
	ELSE
	X = XI*(JX-20)
	X1 = XI*(JX-19)
	X2 = XI*(JX-18)
	END IF
	WRITE(LOGUN1,110) X,G1P(JX),X1,G1P(JX+1),X2,G1P(JX+2)
	END DO

	WRITE(LOGUN1,'(''  JOIN DOTS'')')

	ELSE	! End of Callaway-Ellis plots

!        CALL LIB$GET_LUN(LOGUN1)

        LOGUN1 = 32

	OPEN(LOGUN1,FORM='FORMATTED',STATUS='NEW',FILE='SEAF.TOP')
	WRITE(LOGUN1,111)
	WRITE(LOGUN1,112) QSQ,PARC
	WRITE(LOGUN1,113)


	DO JX =1,50,3 	! JXMAX=0.3/XI+20
	IF(JX.LE.19) THEN
	X = XI*(JX-1)*.05
	X1 = XI*(JX)*.05
	X2 = XI*(JX+1)*.05
	ELSE
	X = XI*(JX-20)
	X1 = XI*(JX-19)
	X2 = XI*(JX-18)
	END IF

	IF(UVS(JX).EQ.0.0D0) THEN
	PDUX = (DVS(JX+1) + SS(JX+1))/UVS(JX+1)
	ELSE
	PDUX = (DVS(JX) + SS(JX))/UVS(JX)
	END IF
	PDUX1 = (DVS(JX+1) + SS(JX+1))/UVS(JX+1)
	PDUX2 = (DVS(JX+2) + SS(JX+2))/UVS(JX+2)

	WRITE(LOGUN1,110) X,PDUX,X1,PDUX1,X2,PDUX2
	END DO

	WRITE(LOGUN1,'(''  JOIN SOLID'')')


	DO JX =1,50,3 	! JXMAX=0.3/XI+20
	IF(JX.LE.19) THEN
	X = XI*(JX-1)*.05
	X1 = XI*(JX)*.05
	X2 = XI*(JX+1)*.05
	ELSE
	X = XI*(JX-20)
	X1 = XI*(JX-19)
	X2 = XI*(JX-18)
	END IF

	IF(UVS(JX).EQ.0.0D0) THEN
	PDUX = (DVS(JX+1))/UVS(JX+1)
	ELSE
	PDUX = DVS(JX)/UVS(JX)
	END IF
	PDUX1 = (DVS(JX+1))/UVS(JX+1)
	PDUX2 = (DVS(JX+2))/UVS(JX+2)

	WRITE(LOGUN1,110) X,PDUX,X1,PDUX1,X2,PDUX2
	END DO

	WRITE(LOGUN1,'(''  JOIN DASHES'')')

	CLOSE (LOGUN1)

	OPEN(LOGUN1,FORM='FORMATTED',STATUS='NEW',FILE='SEAFD.TOP')
	WRITE(LOGUN1,111)
	WRITE(LOGUN1,112) QSQ,PARC
	WRITE(LOGUN1,113)

	DO JX =1,50,3 	! JXMAX=0.3/XI+20
	IF(JX.LE.19) THEN
	X = XI*(JX-1)*.05
	X1 = XI*(JX)*.05
	X2 = XI*(JX+1)*.05
	ELSE
	X = XI*(JX-20)
	X1 = XI*(JX-19)
	X2 = XI*(JX-18)
	END IF

	IF(UVS(JX).EQ.0.0D0) THEN
	DDUX = 1 + 2.0D0*SS(JX+1)/(UVS(JX+1)+DVS(JX+1))
	ELSE
	DDUX = 1 + 2.0D0*SS(JX)/(UVS(JX)+DVS(JX))
	END IF
	DDUX1 = 1 + 2.0D0*SS(JX+1)/(UVS(JX+1)+DVS(JX+1))
	DDUX2 = 1 + 2.0D0*SS(JX+2)/(UVS(JX+2)+DVS(JX+2))

	WRITE(LOGUN1,110) X,DDUX,X1,DDUX1,X2,DDUX2
	END DO

	WRITE(LOGUN1,'(''  JOIN DOTDASH'')')

	END IF	! End of Sea flavors or not.

	END IF	! End of plots

!        CALL EXIT
	END


C	Duke - Owens QSQ evolution of parton distributions PRD30(84)49,
C	and update FSU-HEP-910606

	SUBROUTINE PARTONS(X,QSQ,XUVDV,XDV,XS)
	IMPLICIT REAL*8 (A-H,N-Z)

        REAL*4 X,QSQ
        REAL*8 ETA(4),GETA(4),GETA1(4),NUD,ND,BETA12,BETA34
	REAL*8 GETA12,GGETA12,GETA34,GGETA34
        REAL*8 CETA(3,4),CGD(3,2),CGUD(3,2),ONE,QSQ0,PIE,LAMBDA
     *2
	REAL*8 GETA2(4),GD(2),GUD(2)
	REAL*8 SA(3),SAL(3),SBL(3),SALPHA(3),SBETA(3),SGAMMA(3)

	DATA CETA/ 0.665D0, -0.1097D0, -0.002442D0,
     *   3.614D0,   0.8395D0, -0.02186D0,
     *   0.8388D0, -0.2092D0,  0.02657D0,
     *   4.667D0,   0.7951D0,  0.1081D0/
	DATA CGUD/ 0.8673D0, -1.6637D0,  0.3420D0,
     *    0.0D0,      1.1049D0, -0.2369D0/
	DATA CGD/  0.0D0,     -1.0232D0, 0.05799D0,
     *    0.0D0,      0.8616D0, 0.1530D0/

	DATA SA/0.909D0, -0.4023D0,  0.006305D0/
	DATA SAL/ 0.0D0, -0.3823D0,  0.02766D0/
	DATA SBL/ 7.278D0, -0.7904D0, 0.8108D0/
	DATA SALPHA/0.0D0, -1.6629D0, 0.5719D0/
	DATA SBETA /0.0D0, -0.01333D0, 0.5299D0/
	DATA SGAMMA /0.0D0, 0.1211D0, -0.1739D0/

	DATA QSQ0/4.0D0/,LAMBDA2/0.031329D0/,ONE/1.0D0/
     * ,PIE/3.141592654D0/
	DATA TWO/2.0D0/,ZERO/0.0D0/

	S = LOG(LOG(QSQ/LAMBDA2)/LOG(QSQ0/LAMBDA2))

	DO JH = 1,4
	ETA(JH) = 0.D0
	END DO

	DO JH = 1,4
	IF(S.NE.0.0D0) THEN
	DO JC = 1,3
	ETA(JH) = CETA(JC,JH)*S**(JC-1)+ETA(JH)
	END DO
	ELSE
	ETA(JH) = CETA(1,JH)
	END IF
	END DO

	GD(1) = 0.D0
	GD(2) = 0.D0
	GUD(1) = 0.D0
	GUD(2) = 0.D0

	DO JQ = 1,2
	IF(S.NE.0.0D0) THEN
	DO JC = 1,3
	GD(JQ) = CGD(JC,JQ)*S**(JC-1) + GD(JQ)
	GUD(JQ) = CGUD(JC,JQ)*S**(JC-1) + GUD(JQ)
	END DO
	ELSE
	GD(JQ) = CGD(1,JQ)
	GUD(JQ) = CGUD(1,JQ)	
	END IF
	END DO

	DO JH = 1,4

	IF(ETA(JH).LT.ONE) THEN
	ZETA = ONE - ETA(JH)
	GETA(JH) = PIE*ZETA/(DEXP(GAMMALN(ONE + ZETA))*DSIN(PIE*ZETA))
!	GETA(JH) = PIE*ETA(JH)/(DEXP(GAMMALN(ONE + ETA(JH)))
!     *   *DSIN(PIE*ETA(JH)))
	GETA(JH) = DEXP(GAMMALN(ONE + ETA(JH)))/ETA(JH)
	GETA(JH) = DLOG(GETA(JH))
	ELSE
	GETA(JH) = GAMMALN(ETA(JH))
	END IF

	GETA1(JH) = GAMMALN(ETA(JH)+ONE)
	GETA2(JH) = GAMMALN(ETA(JH)+TWO)

	END DO

	GETA12 = (ETA(1)+ETA(2)+ONE)
	GGETA12 = GAMMALN(GETA12)
	GETA112 = (ETA(1)+ONE+ETA(2)+ONE)
	GGETA112 = GAMMALN(GETA112)
	GETA122 = (ETA(1)+TWO+ETA(2)+ONE)
	GGETA122 = GAMMALN(GETA122)
	GETA34 = (ETA(3)+ETA(4)+ONE)
	GGETA34 = GAMMALN(GETA34)
	GETA334 = (ETA(3)+ONE+ETA(4)+ONE)
	GGETA334 = GAMMALN(GETA334)
	GETA344 = (ETA(3)+TWO+ETA(4)+ONE)
	GGETA344 = GAMMALN(GETA344)

	BETA12 = EXP(GETA(1) + GETA1(2) - GGETA12)
!	BETA112 = EXP(GETA1(1) + GETA1(2) - GGETA122)
	BETA112 = EXP(GETA1(1) + GETA1(2) - GGETA112)
	BETA122 = EXP(GETA2(1) + GETA1(2) - GGETA122)
	BETA34 = EXP(GETA(3) + GETA1(4) - GGETA34)
	BETA334 = EXP(GETA1(3) + GETA1(4) - GGETA334)
!	BETA334 = EXP(GETA2(3) + GETA1(4) - GGETA344)
	BETA344 = EXP(GETA2(3) + GETA1(4) - GGETA344)

	NUD = 3.0D0/(BETA12 + GUD(1)*BETA112 + GUD(2)*BETA122)
	ND  = 1.0D0/(BETA34 + GD(1)*BETA334 + GD(2)*BETA344)

	XUVDV = NUD*X**ETA(1)*(ONE - X)**ETA(2)*(ONE + GUD(1)*X +
     * GUD(2)*X**2)
	XDV = ND*X**ETA(3)*(ONE - X)**ETA(4)*(ONE + GD(1)*X +
     * GD(2)*X**2)

	AS = ZERO
	ASL = ZERO
	BSL = ZERO
	ALPHAS = ZERO
	BETAS = ZERO
	GAMMAS = ZERO

	DO JS = 1,3
	IF(S.NE.ZERO) THEN
	PS = S**(JS-1)
	ELSE
	PS = ZERO
	IF(JS.EQ.1) PS = ONE
	END IF
	AS = SA(JS)*PS + AS
	ASL = SAL(JS)*PS + ASL
	BSL = SBL(JS)*PS + BSL
	ALPHAS = SALPHA(JS)*PS + ALPHAS
	BETAS = SBETA(JS)*PS + BETAS
	GAMMAS = SGAMMA(JS)*PS + GAMMAS
	END DO

C	This test is needed because VAX FORTRAN cannot do 0.^(-x).

	IF(X.GT.ZERO) THEN
	XS = AS*X**ASL*(ONE - X)**BSL*
     * (ONE + X*(ALPHAS + BETAS*X + GAMMAS*X*X))
	ELSE
	XS = ZERO
	END IF

	RETURN

	END

	DOUBLE PRECISION FUNCTION GAMMALN(XX)
	IMPLICIT REAL*8 (A-H,N-Z)
	REAL*8 COF(6),STP,HALF,ONE,FPF,X,TMP,SER,XX
	DATA COF,STP/
     *  76.18009173D0, -86.50532033D0 ,  24.01409822D0,
     * -1.231739516D0,   0.120858003D-2, -0.536382D-5,
     *  2.50662827465D0/

	DATA HALF,ONE,FPF/ 0.5D0,1.0D0,5.5D0/

	X = XX - ONE
	TMP = X + FPF
	TMP = (X + HALF)*DLOG(TMP) -  TMP
	SER = ONE
	DO J = 1,6
	X = X + ONE
	SER = SER + COF(J)/X
	END DO
	GAMMALN = TMP + DLOG(STP*SER)
	RETURN
	END

	
	SUBROUTINE F2EMC(X,Y,RX,F_2P)

C	EMC F_2 Nucl. Phys. B259(85)189

	F2SC = 3.373*X**0.985*(1-X)**3.688 + 
     * 0.276*(1-X)**10.629
	F2SCB = (0.282*(1-X)**8.995 - 0.078)*LOG(10.7D0/3.0D0)
	F_2P = F2SC*(1 + F2SCB)
!	RX = 0.0122/(X+0.041)**1.096
	F_2P = (1.- Y + 0.5*Y**2)*F_2P/
     * (1. - Y + 0.5*Y**2/(1.+RX))
	
	RETURN
	END
                                                      
!	SUBROUTINE F2NMC(X,QSQ,TARGET,F_2)
	SUBROUTINE F2NMC(X,QSQ,TARGET,F_2)


       REAL*4 A(7,2),B(4,2),C(4,2)
        REAL*8 AX,BX,CX,LQ,LAMBDA2,LQ0L2
        CHARACTER*1 TARGET

!        DATA A	!	"Old" NMC
!     *  / -0.1011,  2.5620,  0.4121, -0.5180,  5.9670,-10.1970, 4.6850,
!     *    -0.0996,  2.4890,  0.4684, -1.9240,  8.1590,-10.8930, 4.5350/
!        DATA B/  0.3640, -2.7640,  0.0150,  0.0186,
!     *           0.2520, -2.7130,  0.0254,  0.0299/
!        DATA C/ -1.1790,  8.2400,-36.3600, 47.7600,
!     *          -1.2210,  7.5000,-30.4900, 40.2300/
!        DATA LAMBDA2/.0625/,LQ0L2/5.768320996/ !lambda=.25GeV, Q_o=20GeV

        DATA A	!	"New" NMC (1995) PLB364
     *  / -0.02778, 2.926,   1.0362, -1.8400,  8.123, -13.0740, 6.2150,
     *    -0.0996,  2.4890,  0.4684, -1.9240,  8.1590,-10.8930, 4.5350/
        DATA B/  0.2850, -2.6940,  0.0188,  0.0274,
     *           0.2520, -2.7130,  0.0254,  0.0299/
        DATA C/ -1.4130,  9.3660,-37.7900, 47.1000,
     *          -1.2210,  7.5000,-30.4900, 40.2300/
        DATA LAMBDA2/.0625/,LQ0L2/5.768320996/ !lambda=.25GeV, Q_o=20GeV
        
        IT = 1
        IF(TARGET.EQ.'D') IT =2

        AX1 = X**A(1,IT)*(1.0D0 - X)**A(2,IT)
        AX2 = A(3,IT) + A(4,IT)*(1.0D0 - X) + A(5,IT)*(1.0D0 - X)**2
	AX3 = A(6,IT)*(1.0D0 - X)**3 + A(7,IT)*(1.0D0 - X)**4
        AX = AX1*(AX2 + AX3)
        BX = B(1,IT) + B(2,IT)*X + B(3,IT)/(X + B(4,IT))
        CX = C(1,IT)*X + C(2,IT)*X**2 + C(3,IT)*X**3 + C(4,IT)*X**4
        LQ = LOG(QSQ/LAMBDA2)/LQ0L2        
        F_2 = AX*LQ**BX*(1 + CX/QSQ)
        RETURN
        END
        
