!>  JAMLIB
!!    Authors:                                                   
!!    Nobuo Sato         (Jefferson Lab)                         
!!    Jake Ethier        (College of William and Mary)           
!!    Wally Melnitchouk  (Jefferson Lab)                         
!!    Alberto Accardi    (Hampton University and Jefferson Lab)
!!
!!
!!  HISTORY:
!!     10-20-2016 Modified to simultaneously load load all 
!!                twists with PPDF distributions. Get the higher
!!                twist distributions with "u3,d3,p4,n4" for the
!!                twist3 up, twist3 down, twist4 proton, and twist4
!!                neutron, respectively.
!!                Also added helper subroutine to call the main
!!                get_xF as a subroutine.
!!
!! @ingroup JAM      
!!
************************************************************************
*
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
*          _   _    __  __ _     ___ ____                        
*         | | / \  |  \/  | |   |_ _| __ )                       
*         | |/ _ \ | |\/| | |    | ||  _ \                       
*     | |_| / ___ \| |  | | |___ | || |_) |                      
*      \___/_/   \_\_|  |_|_____|___|____/                       
*                                                           
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
*                                                           
*     Authors:                                                   
*     Nobuo Sato         (Jefferson Lab)                         
*     Jake Ethier        (College of William and Mary)           
*     Wally Melnitchouk  (Jefferson Lab)                         
*     Alberto Accardi    (Hampton University and Jefferson Lab)
*
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
*
*     FUNCTION get_xF(x,Q2,flav)
*
*     INPUT: x,Q^2, and flavor
*
*     flav (character*2): 'up','dp','sp','cp','bp','gl','u','d','s'
*                         'c','b','ub','db','sb','cb','bb','p','n'
*
*     Flavors 'qp' = q + qb (e.g. 'up' = u + ubar)
*             'p'  = proton (for T4PPDF and structure functions)
*             'n'  = neutron (for T4PPDF and structure functions)
*
*     Returns x times the function (PPDF,FF,etc.)
*
*     GRID_INIT must be called only once before using FUNCTION get_xF!
*
*     SUBROUTINE GRID_INIT requires three inputs:
*
*     - lib (character*10): library (JAM15,JAM16,etc.)
*     - dist (character*10): distribution type (PPDF,FFpion,FFkaon,etc.)
*     - ipos (integer): posterior number (0 to 199) from MC analysis
*
************************************************************************

      !> JAMLIB Get xf(x,Q2).
      !!
      !!  @ingroup JAM
      !!
      FUNCTION get_xF(x,Q2,flav)
      IMPLICIT NONE
      INTEGER nx,nq2,nc,ipos,INIT,ier,lwrk,kwrk
      PARAMETER (nx=104,nq2=67,nc=6300,lwrk=8,kwrk=2)
      INTEGER iwrk(kwrk)
      REAL*8 TX(nx),TQ2(nq2),CUP(nc),CDP(nc),CSP(nc),CCP(nc),CBP(nc)
      REAL*8 CG(nc),CU(nc),CD(nc),CS(nc),CC(nc),CB(nc),CUB(nc),CDB(nc)
      REAL*8 CSB(nc),CCB(nc),CBB(nc),CP(nc),CN(nc)
      REAL*8 T3U(nc),T3D(nc),T4P(nc),T4N(nc),CQ(nc)
      REAL*8 TX3(nx),TQ23(nq2),TX4(nx),TQ24(nq2)
      REAL*8 TXFIN(nx),TQ2FIN(nq2)
      REAL*8 x,Q2,xarr(1),qarr(1),xF(1,1),get_xF
      REAL*8 wrk(lwrk)
      CHARACTER flav*2
      COMMON/GRID_INI/INIT
      COMMON/BSPLINE_COEFFS/CUP,CDP,CSP,CCP,CBP,CG,CU,CD,CS,CC,CB,CUB,
     &                      CDB,CSB,CCB,CBB,CP,CN,T3U,T3D,T4P,T4N
      COMMON/X_Q2_KNOTS/TX,TQ2,TX3,TQ23,TX4,TQ24

      IF (INIT.eq.0) then
         print *,'Grid was not initialized! Must call GRID_INIT first.'
         stop
      ENDIF

      TXFIN = TX
      TQ2FIN = TQ2
      IF (flav.eq.'up') THEN
         CQ = CUP
      ELSEIF (flav.eq.'dp') THEN
         CQ = CDP
      ELSEIF (flav.eq.'sp') THEN
         CQ = CSP
      ELSEIF (flav.eq.'cp') THEN
         CQ = CCP
      ELSEIF (flav.eq.'bp') THEN
         CQ = CBP
      ELSEIF (flav.eq.'gl') THEN
         CQ = CG
      ELSEIF (flav.eq.'u') THEN
         CQ = CU
      ELSEIF (flav.eq.'d') THEN
         CQ = CD
      ELSEIF (flav.eq.'s') THEN
         CQ = CU
      ELSEIF (flav.eq.'c') THEN
         CQ = CU
      ELSEIF (flav.eq.'b') THEN
         CQ = CU
      ELSEIF (flav.eq.'ub') THEN
         CQ = CU
      ELSEIF (flav.eq.'db') THEN
         CQ = CU
      ELSEIF (flav.eq.'sb') THEN
         CQ = CU
      ELSEIF (flav.eq.'cb') THEN
         CQ = CU
      ELSEIF (flav.eq.'bb') THEN
         CQ = CU
      ELSEIF (flav.eq.'p') THEN
         CQ = CP
      ELSEIF (flav.eq.'n') THEN
         CQ = CN
      ELSEIF (flav.eq.'u3') THEN
        CQ = T3U
        TXFIN = TX3
        TQ2FIN = TQ23
      ELSEIF (flav.eq.'d3') THEN
        CQ = T3D
        TXFIN = TX3
        TQ2FIN = TQ23
      ELSEIF (flav.eq.'p4') THEN
         CQ = T4P
        TXFIN = TX4
        TQ2FIN = TQ24
      ELSEIF (flav.eq.'n4') THEN
         CQ = T4N
        TXFIN = TX4
        TQ2FIN = TQ24
      ENDIF

      xarr(1)=x
      qarr(1)=Q2

      CALL bispev(TXFIN,nx,TQ2FIN,nq2,CQ,3,3,xarr,1,qarr,1,xF,wrk,lwrk,
     &            iwrk,kwrk,ier)

      get_xF=xF(1,1)

      RETURN
      END

************************************************************************

      !> JAMLIB Get xf(x,Q2).
      !!
      !!  @ingroup JAM
      !!
      !! A wrapper on the function since only subroutines can be called from
      !! C/C++
      SUBROUTINE jam_xF(res,x,Q2,flav)
      IMPLICIT NONE
      REAL*8 x,Q2,res,get_xF
      CHARACTER flav*2
      res = get_xF(x,Q2,flav)
      return
      END

************************************************************************

      !> JAMLIB initialization.
      !!
      !!  @ingroup JAM
      SUBROUTINE GRID_INIT(path2,lib,dist,ipos)
Cf2py intent(in) path
Cf2py intent(in) lib
Cf2py intent(in) dist
Cf2py intent(in) ipos
      IMPLICIT NONE
      INTEGER ipos,INIT
      INTEGER nq2,nx,nc
      PARAMETER (nx=104,nq2=67,nc=6300)
      CHARACTER path5*180,path4*180,path3*180,path*120,path2*120,lib*10,dist*10,filename*10
      REAL*8 TX(nx),TQ2(nq2),CUP(nc),CDP(nc),CSP(nc),CCP(nc),CBP(nc)
      REAL*8 CG(nc),CU(nc),CD(nc),CS(nc),CC(nc),CB(nc),CUB(nc),CDB(nc)
      REAL*8 CSB(nc),CCB(nc),CBB(nc),CP(nc),CN(nc)
      REAL*8 T3U(nc),T3D(nc),T4P(nc),T4N(nc)
      REAL*8 TX3(nx),TQ23(nq2),TX4(nx),TQ24(nq2)
      LOGICAL PLUS
      COMMON/GRID_INI/INIT
      COMMON/BSPLINE_COEFFS/CUP,CDP,CSP,CCP,CBP,CG,CU,CD,CS,CC,CB,CUB,
     &                      CDB,CSB,CCB,CBB,CP,CN,T3U,T3D,T4P,T4N
      COMMON/X_Q2_KNOTS/TX,TQ2,TX3,TQ23,TX4,TQ24

      path=InSANE_PDF_GRID_DIR//'/'
!      write(*,*) 'path ',path
!      write(*,*) 'lib ',lib
!      write(*,*) 'dist ',dist

      !! Need to update for future JAM libraries
      IF (lib.eq.'JAM15'.and.dist.eq.'PPDF') THEN
         PLUS = .TRUE.
      ELSEIF (lib.eq.'JAM16'.and.dist(1:2).eq.'FF') THEN
         PLUS = .TRUE.
      ELSE
         PLUS = .FALSE.
      ENDIF

      if (ipos.lt.10) then
         WRITE(filename,'(A3,I1)') 'xF-',ipos
      elseif (ipos.ge.10.and.ipos.lt.100) then
         WRITE(filename,'(A3,I2)') 'xF-',ipos
      elseif (ipos.ge.100) then
         WRITE(filename,'(A3,I3)') 'xF-',ipos
      endif

      IF (lib.eq.'JAM15'.and.dist.eq.'PPDF') THEN
         path3=trim(path)//trim(lib)//'/'//trim(dist)//'/'//trim(filename)//'.tab'
         path4=trim(path)//trim(lib)//'/T3PPDF/'//trim(filename)//'.tab'
         path5=trim(path)//trim(lib)//'/g1T4/'//trim(filename)//'.tab'
       endif


!      write(*,*)'Opening ',path3
      OPEN(10,FILE=trim(path3),STATUS='old')
C      OPEN(10,FILE=trim(path)//trim(lib)//'/'//trim(dist)//'/'//
C     &               trim(filename)//'.tab',STATUS='old')
      READ(10,*) TX
      READ(10,*) TQ2
      !! Need to update for future JAM libraries
      IF (dist.eq.'PPDF'.or.dist(1:2).eq.'FF') THEN
         IF (PLUS.eqv..TRUE.) THEN
            READ(10,*) CUP
            READ(10,*) CDP
            READ(10,*) CSP
            READ(10,*) CCP
            READ(10,*) CBP
            READ(10,*) CG
         ELSE
            READ(10,*) CSB
            READ(10,*) CDB
            READ(10,*) CUB
            READ(10,*) CU
            READ(10,*) CD
            READ(10,*) CS
            READ(10,*) CC
            READ(10,*) CB
            READ(10,*) CG
         ENDIF
      ENDIF
      CLOSE(10)

!      write(*,*)'Opening ',path4
      OPEN(10,FILE=trim(path4),STATUS='old')
!      ELSEIF (dist.eq.'T3PPDF') THEN
      READ(10,*) TX3
      READ(10,*) TQ23
      READ(10,*) T3U
      READ(10,*) T3D
      CLOSE(10)

!      write(*,*)'Opening ',path5
      OPEN(10,FILE=trim(path5),STATUS='old')
!      ELSEIF (dist.eq.'T4PPDF') THEN
      READ(10,*) TX4
      READ(10,*) TQ24
      READ(10,*) T4P
      READ(10,*) T4N
      CLOSE(10)
!      ENDIF
      INIT = 2
      RETURN
      END

************************************************************************
*
*     BIVARIATE SPLINE ROUTINE FROM FITPACK
*     http://www.netlib.org/dierckx/
*
************************************************************************

      subroutine bispev(tx,nx,ty,ny,c,kx,ky,x,mx,y,my,z,wrk,lwrk,
     * iwrk,kwrk,ier)
      integer nx,ny,kx,ky,mx,my,lwrk,kwrk,ier
      integer iwrk(kwrk)
      real*8 tx(nx),ty(ny),c((nx-kx-1)*(ny-ky-1)),x(mx),y(my),z(mx*my),
     * wrk(lwrk)
      integer i,iw,lwest
      ier = 10
      lwest = (kx+1)*mx+(ky+1)*my
      if(lwrk.lt.lwest) go to 100
      if(kwrk.lt.(mx+my)) go to 100
      if(mx-1) 100,30,10
 10     do 20 i=2,mx
        if(x(i).lt.x(i-1)) go to 100
 20       continue
 30         if(my-1) 100,60,40
 40           do 50 i=2,my
        if(y(i).lt.y(i-1)) go to 100
 50       continue
 60         ier = 0
      iw = mx*(kx+1)+1
      call fpbisp(tx,nx,ty,ny,c,kx,ky,x,mx,y,my,z,wrk(1),wrk(iw),
     * iwrk(1),iwrk(mx+1))
 100    return
      end

************************************************************************

      subroutine fpbisp(tx,nx,ty,ny,c,kx,ky,x,mx,y,my,z,wx,wy,lx,ly)
      integer nx,ny,kx,ky,mx,my
      integer lx(mx),ly(my)
      real*8 tx(nx),ty(ny),c((nx-kx-1)*(ny-ky-1)),x(mx),y(my),z(mx*my),
     * wx(mx,kx+1),wy(my,ky+1)
      integer kx1,ky1,l,l1,l2,m,nkx1,nky1
      real*8 arg,sp,tb,te
      real*8 h(6)
      kx1 = kx+1
      nkx1 = nx-kx1
      tb = tx(kx1)
      te = tx(nkx1+1)
      l = kx1
      l1 = l+1
      do 40 i=1,mx
        arg = x(i)
        if(arg.lt.tb) arg = tb
        if(arg.gt.te) arg = te
 10         if(arg.lt.tx(l1) .or. l.eq.nkx1) go to 20
        l = l1
        l1 = l+1
        go to 10
 20         call fpbspl(tx,nx,kx,arg,l,h)
        lx(i) = l-kx1
        do 30 j=1,kx1
          wx(i,j) = h(j)
 30           continue
 40             continue
      ky1 = ky+1
      nky1 = ny-ky1
      tb = ty(ky1)
      te = ty(nky1+1)
      l = ky1
      l1 = l+1
      do 80 i=1,my
        arg = y(i)
        if(arg.lt.tb) arg = tb
        if(arg.gt.te) arg = te
 50         if(arg.lt.ty(l1) .or. l.eq.nky1) go to 60
        l = l1
        l1 = l+1
        go to 50
 60         call fpbspl(ty,ny,ky,arg,l,h)
        ly(i) = l-ky1
        do 70 j=1,ky1
          wy(i,j) = h(j)
 70           continue
 80             continue
      m = 0
      do 130 i=1,mx
        l = lx(i)*nky1
        do 90 i1=1,kx1
          h(i1) = wx(i,i1)
 90           continue
        do 120 j=1,my
          l1 = l+ly(j)
          sp = 0.
          do 110 i1=1,kx1
            l2 = l1
            do 100 j1=1,ky1
              l2 = l2+1
              sp = sp+c(l2)*h(i1)*wy(j,j1)
 100                  continue
            l1 = l1+nky1
 110              continue
          m = m+1
          z(m) = sp
 120          continue
 130            continue
      return
      end

************************************************************************

      subroutine fpbspl(t,n,k,x,l,h)
      real*8 x
      integer n,k,l
      real*8 t(n),h(6)
      real*8 f,one
      integer i,j,li,lj
      real*8 hh(5)
      one = 0.1e+01
      h(1) = one
      do 20 j=1,k
        do 10 i=1,j
          hh(i) = h(i)
 10           continue
        h(1) = 0.
        do 20 i=1,j
          li = l+i
          lj = li-j
          f = hh(i)/(t(li)-t(lj))
          h(i) = h(i)+f*(t(li)-x)
          h(i+1) = f*(x-t(lj))
 20         continue
      return
      end

************************************************************************
