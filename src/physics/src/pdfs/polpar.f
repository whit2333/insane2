      subroutine polpar(iflag,x,q2,uval,dval,glue,qbar,str)
      implicit real*8(a-h,o-z)
      dimension aux(5)

c ---- LO polarized parton distributions as described in
c ---- T. Gehrmann and W.J. Stirling: "Polarized Parton Distributions 
c ---- of the Nucleon", Phys.Rev. D53 (1996) 6100.
c ----
c ---- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c ---- before calling this subroutine for the first time, the
c ---- LO grids need to be installed by CALL POLINI 
c ---- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c ----
c ---- INPUT parameters:
c ---- iflag = selects gluon set    0:A, 1:B, 2:C 
c ---- x     = xbjorken
c ---- q2    = Q^2
c ---- OUTPUT parameters (note that always x*distribution is returned)
c ---- uval  = u-ubar
c ---- dval  = d-dbar 
c ---- glue 
c ---- qbar  = ubar = dbar = 1/2 usea = 1/2 dsea
c ---- str   = sbar = 1/2 strsea
c ---- 
c ---- please report any problems to: gehrt@mail.desy.de

      call rdarry(x,q2,aux,iflag)
      uval=aux(1)*(x**0.6d0*(1.d0-x)**3)
      dval=aux(2)*(x**0.75d0*(1.d0-x)**4)
      glue=aux(3)*(x**0.5d0*(1.d0-x)**5)
      qbar=aux(4)*(x**0.5d0*(1.d0-x)**6)
      str=aux(5)*(x**0.5d0*(1.d0-x)**6)
            
      return
      end


      subroutine rdarry(x,q2,aux,iflag)
      implicit real*8(a-h,o-z)
      implicit integer(i-n)
      dimension aux(5)
      common/pdist/arraya(151,20,5),arrayb(151,20,5),arrayc(151,20,5)
     
      nx=151
      ndata=nx-1
      nq2pts=20
      nq2inv=nq2pts-1
      q2sta=1.d0
      q2fin=1.d6
      ymin=5.d0
      xmin=10.d0**(-ymin)
      xmax=1.d0

      if (q2.lt.q2sta) then
         write (6,*) '*** Q^2-value out of range ***'
         write (6,*) '*** Q^2 set to minimal value ***'
         q2=q2sta
         write (6,*) q2
      endif
      if (q2.gt.q2fin) then
         write (6,*) '*** Q^2-value out of range ***'
         write (6,*) '*** Q^2 set to maximal value ***'
         q2=q2fin
         write (6,*) q2
      endif
      if (x.lt.xmin) then
         write (6,*) '*** x-value out of range ***'
         write (6,*) '*** x set to minimal value ***'
         x=xmin
         write (6,*) x
      endif
       if (x.gt.xmax) then
         write (6,*) '*** x-value out of range ***'
         write (6,*) '*** x set to minimal value ***'
         x=xmax
         write (6,*) x
      endif

      y=dlog10(x)
      ram=(y+ymin)*ndata/ymin+1.d0
      iram=int(ram)
      fraci=ram-dble(iram)
      ram=dlog(q2/q2sta)*nq2inv/dlog(q2fin/q2sta)+1.d0
      jram=int(ram)
      fracj=ram-dble(jram)
      
      if (iflag.eq.0) then
         do 100 i=1,5
            aux(i)=(arraya(iram,jram,i)*(1.d0-fraci)
     .             +arraya(iram+1,jram,i)*fraci)*(1.d0-fracj)+
     .             (arraya(iram,jram+1,i)*(1.d0-fraci)
     .             +arraya(iram+1,jram+1,i)*fraci)*fracj
  100    continue
      else
      if (iflag.eq.1) then
         do 200 i=1,5
            aux(i)=(arrayb(iram,jram,i)*(1.d0-fraci)
     .             +arrayb(iram+1,jram,i)*fraci)*(1.d0-fracj)+
     .             (arrayb(iram,jram+1,i)*(1.d0-fraci)
     .             +arrayb(iram+1,jram+1,i)*fraci)*fracj
  200    continue
      else
         do 300 i=1,5
           aux(i)=(arrayc(iram,jram,i)*(1.d0-fraci)
     .            +arrayc(iram+1,jram,i)*fraci)*(1.d0-fracj)+
     .            (arrayc(iram,jram+1,i)*(1.d0-fraci)
     .            +arrayc(iram+1,jram+1,i)*fraci)*fracj
  300    continue
      endif
      endif

      return
      end


      subroutine polini
      implicit real*8(a-h,o-z)
      common/pdist/arraya(151,20,5),arrayb(151,20,5),arrayc(151,20,5)

      open(11,file=InSANE_PDF_GRID_DIR//'/polparA.dat')
      open(12,file=InSANE_PDF_GRID_DIR//'/polparB.dat')
      open(13,file=InSANE_PDF_GRID_DIR//'/polparC.dat')

      do i=1,20
         do j=1,151
            read(11,901) arraya(j,i,1),arraya(j,i,2),
     .                   arraya(j,i,3),arraya(j,i,4),arraya(j,i,5)
            read(12,901) arrayb(j,i,1),arrayb(j,i,2),arrayb(j,i,3),
     .                   arrayb(j,i,4),arrayb(j,i,5)
            read(13,901) arrayc(j,i,1),arrayc(j,i,2),arrayc(j,i,3),
     .                   arrayc(j,i,4),arrayc(j,i,5)
         enddo
      enddo
      close(13)
      close(12)
      close(11)

  901 format(5f14.9)

      return
      end
