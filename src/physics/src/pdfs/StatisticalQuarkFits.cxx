#include "insane/pdfs/StatisticalQuarkFits.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
StatisticalQuarkFits::StatisticalQuarkFits()
{
   fQ0sq = 4.0; //GeV Iintitial fit scale
   //--Set fit parameters/constants
   //--Fermi-Dirac pars
   fxbar      = 0.09907;
   fdxbar     = 0.00110;
   fX0uplus   = 0.46128;
   fdX0uplus  = 0.00338;
   fX0uminus  = 0.29766;
   fdX0uminus = 0.00303;
   fX0dplus   = 0.22775;
   fdX0dplus  = 0.00294;
   fX0dminus  = 0.30174;
   fdX0dminus = 0.00239;

   //--Other pars
   fb       = 0.40926;
   fdb      = 0.00438;
   fbtilda  = -0.25347;
   fdbtilda = 0.00318;
   fAtilda  = 0.08318;
   fdAtilda = 0.00157;
   fbbar    = 2.0*fb;
   fdbbar   = 2.0*fdb;
   fbg      = 1.0 + fbtilda;
   fdbg     = fdbtilda;
   fk       = 1.42;

   //-- Normalization Contants
   fA     = 1.74938; 
   fAbar  = 1.90801;
   fAg    = 14.27535;
   fdAbar = 0.12627;

   //--Strange quark
   fX0splus  = 0.08101;
   fX0sminus = 0.20290;
   fbs       = 2.05305;
   fAtildas  = 0.05762;

   fIsInterp = true;

}
//______________________________________________________________________________
StatisticalQuarkFits::~StatisticalQuarkFits()
{

}
//______________________________________________________________________________
void StatisticalQuarkFits::FixWarning(double Qsq)
{
   if(Qsq != fQ0sq)
   {
      std::cout << "in classes: \n";
      std::cout << "StatisticalQuarkFits::FormGluon()\n";
      std::cout << "StatisticalQuarkFits::FormQuark()\n";
      std::cout << "Can't use " << Qsq << " GeV^2 until DGLAP is done\n";
      std::cout << "Setting Q2 to " << fQ0sq << " GeV^2\n";
   }
}
//______________________________________________________________________________
double StatisticalQuarkFits::FormGluon( double x, double Qsq)
{
   double xgluon;
   double denom,num;

   FixWarning(Qsq); Qsq = fQ0sq;
   CheckX(x);

   num = fAg*TMath::Power(x,fbg);
   denom = TMath::Exp(x/fxbar)-1.0;
   xgluon = num/denom;

   return xgluon/x;  
}
//______________________________________________________________________________
double StatisticalQuarkFits::GetFermiDiracFunc(TString flavor, Int_t helicity, double x, double Qsq )
{
   FixWarning(Qsq); Qsq = fQ0sq;
   CheckX(x);

   double pot; //quark potential
   double Fq,denom,num;  

   if(flavor == "u" || flavor == "d")
   { 
      if(flavor == "u" && helicity == 1){pot = fX0uplus;}
      else if(flavor == "u" && helicity == -1){pot = fX0uminus;}
      else if(flavor == "d" && helicity == 1){pot = fX0dplus;}
      else if(flavor == "d" && helicity == -1){ pot = fX0dminus;}
      else
      {
         std::cout << "BBFits::FormQuark()\n";
         std::cout << "Need to enter correct helicity (-1 or +1)\n";
      }
      num = pot;
      denom = TMath::Exp(( x-pot )/fxbar) + 1.0;  
      Fq = num/denom;
   }
   //--For qbar X0^h_q = -X0^{-h}_qbar 
   //-- Leads to X0^h_qbar = X0^{-h}_q , where h = helicity 
   else if(flavor == "ubar" || flavor == "dbar")
   {
      if(flavor == "ubar" && helicity == -1){pot = fX0uplus;}
      else if(flavor == "ubar" && helicity == 1){pot = fX0uminus;}
      else if(flavor == "dbar" && helicity == -1){pot = fX0dplus;}
      else if(flavor == "dbar" && helicity == 1){ pot = fX0dminus;}
      else
      {
         std::cout << "BBFits::FormQuark()\n";
         std::cout << "Need to enter correct helicity (-1 or +1)\n";
      }
      num = 1.0;
      denom = pot *( TMath::Exp( ( x+pot )/fxbar )  + 1.0 ) ;
      Fq = num/denom;
   }
   else if(flavor == "s")
   {
      if(helicity == 1){pot = fX0splus;}          
      else if(helicity == -1){pot = fX0sminus;}
      else
      {
         std::cout << "BBFits::FormQuark()\n";
         std::cout << "Need to enter correct helicity (-1 or +1)\n";
      }
      num = fX0uplus;
      denom = TMath::Exp( (x-pot)/fxbar )+1.0;
      Fq = num/denom;
   }
   else if(flavor == "sbar")
   {
      if(helicity == 1){pot = fX0sminus;}
      else if(helicity == -1){pot = fX0splus;}
      else
      {
         std::cout << "BBFits::FormQuark()\n";
         std::cout << "Need to enter correct helicity (-1 or +1)\n";
      }
      num = 1.0;
      denom = fX0dplus*(TMath::Exp( (x+pot)/fxbar )  + 1.0 );
      Fq = num/denom;
   }
   else
   {
      std::cout << "Error in StatisticalQuarkFits::GetFermiDiracFunc()\n";
      std::cout << "Check your inputs\n";
   }
   return Fq;
}
//______________________________________________________________________________
double StatisticalQuarkFits::FormQuark(TString flavor, Int_t helicity, double x, double Qsq)
{
   FixWarning(Qsq); Qsq = fQ0sq;
   CheckX(x);

   double xquark;
   double xq[2];
   double num, denom;
   double Fq;
   double pot; //quark potential
   //--u,d,ubar,dbar Quarks 
   if(flavor == "u" || flavor == "d")
   {
      Fq = GetFermiDiracFunc(flavor,helicity,x,Qsq);
      xq[0] = Fq*fA*TMath::Power(x,fb);
      num = fAtilda*TMath::Power(x,fbtilda);
      denom = TMath::Exp(x/fxbar) + 1.0;
      xq[1] = num/denom;
   }
   else if(flavor == "ubar" || flavor == "dbar")
   {
      Fq = GetFermiDiracFunc(flavor,helicity,x,Qsq);
      xq[0] = Fq*fAbar*TMath::Power(x,fbbar);
      num = fAtilda*TMath::Power(x,fbtilda);
      denom = TMath::Exp(x/fxbar)+1.0;
      xq[1] = num/denom;
   }
   else if(flavor == "s")
   {
      if(helicity == 1){pot = fX0splus;}
      else if(helicity == -1){pot = fX0sminus;}
      else
      {
         std::cout << "BBFits::FormQuark()\n";
         std::cout << "Need to enter correct helicity (-1 or +1)\n";
      }
      Fq = GetFermiDiracFunc(flavor,helicity,x,Qsq);
      xq[0] = Fq*fA*TMath::Power(x,fbs) * TMath::Log(1.0 + TMath::Exp( fk*pot/fxbar) )/ TMath::Log( 1.0 + TMath::Exp( fk*fX0uplus/fxbar) );
      num = fAtildas*TMath::Power(x,fbtilda);
      denom = TMath::Exp(x/fxbar)+1.0;
      xq[1] = num/denom;
   }
   else if(flavor == "sbar")
   {
      if(helicity == -1){pot = fX0splus;}
      else if(helicity == 1){pot = fX0sminus;}
      else
      {
         std::cout << "BBFits::FormQuark()\n";
         std::cout << "Need to enter correct helicity (-1 or +1)\n";
      }
      Fq = GetFermiDiracFunc(flavor,helicity,x,Qsq);
      xq[0] = Fq*fAbar*TMath::Power(x,2.0*fbs) * TMath::Log(1.0 + TMath::Exp( -fk*pot/fxbar) )/ TMath::Log( 1.0 + TMath::Exp( -fk*fX0dplus/fxbar) );
      num = fAtildas*TMath::Power(x,fbtilda);
      denom = TMath::Exp(x/fxbar)+1.0;
      xq[1] = num/denom;
   }
   else
   {
      std::cout << "Error in StatisticalQuarkFits::FormQuark()\n";
      std::cout << "Check your inputs\n";
   }

   xquark = xq[0] + xq[1];
   return xquark/x;

}
//______________________________________________________________________________
double StatisticalQuarkFits::CheckX(double x)
{
   if(x == 0)
   {
      std::cout << " x can not be equal to 0\n";
      std::cout << " Setting x = " << x <<  " to x = 0.0001\n";
      x = 0.0001;
   }
   return x;
}
//______________________________________________________________________________

}}
