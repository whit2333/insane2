#include "insane/pdfs/CTEQ10_UPDFs.h"
#include "insane/pdfs/FortranWrappers.h"
#include "TMath.h"

namespace  insane {
  namespace physics {

    CTEQ10_UPDFs::CTEQ10_UPDFs()
    {
      int iSet = 100;     // default
      setct10_(&iSet);  
      SetLabel("CTEQ10");
      SetNameTitle("CTEQ10UnpolarizedPDFs","CTEQ10 PDFs");
      SetLineColor(kCyan+2);
    }
    //______________________________________________________________________________

    CTEQ10_UPDFs::~CTEQ10_UPDFs()
    { }
    //______________________________________________________________________________
    
    const std::array<double,NPartons>& CTEQ10_UPDFs::Calculate(double x, double Q2) const
    {
      /** Implementation of pure virtual method.
       *
       *  The function getct10 (Iparton, X, Q)
       *  returns the parton distribution inside the proton for parton [Iparton]
       *  at [X] Bjorken_X and scale [Q] (GeV) in PDF set [Iset].
       *  Iparton  is the parton label (5, 4, 3, 2, 1, 0, -1, ......, -5)
       *                           for (b, c, s, d, u, g, u_bar, ..., b_bar),
       */
      //for(int i=0;i<13;i++) m_PDFValues.fValues[i] = 0.; 

      m_PDFValues.fx = x;
      m_PDFValues.fQ2 = Q2;
      double Q = TMath::Sqrt(Q2);
      double res = 0.0;
      int i = 0;
      /// up quark 
      i = 1;
      getct10_(&i,&x,&Q,&res);
      m_PDFValues.fValues[q_id(Parton::u)] = res; 
      /// down quark 
      i = 2;
      getct10_(&i,&x,&Q,&res);
      m_PDFValues.fValues[q_id(Parton::d)] = res; 
      /// strange 
      i = 3;
      getct10_(&i,&x,&Q,&res);
      m_PDFValues.fValues[q_id(Parton::s)] = res; 
      /// charm 
      i = 4; 
      getct10_(&i,&x,&Q,&res);
      m_PDFValues.fValues[q_id(Parton::c)] = res; 
      /// bottom
      i = 5;  
      getct10_(&i,&x,&Q,&res);
      m_PDFValues.fValues[q_id(Parton::b)] = res; 
      /// top 
      m_PDFValues.fValues[q_id(Parton::t)] = res; 
      /// gluon 
      i = 0;
      getct10_(&i,&x,&Q,&res);
      m_PDFValues.fValues[q_id(Parton::g)] = res; 
      /// u-bar 
      i = -1;
      getct10_(&i,&x,&Q,&res);
      m_PDFValues.fValues[q_id(Parton::ubar)] = res; 
      /// d-bar 
      i = -2;
      getct10_(&i,&x,&Q,&res);
      m_PDFValues.fValues[q_id(Parton::dbar)] = res; 
      /// s-bar 
      i = -3;
      getct10_(&i,&x,&Q,&res);
      m_PDFValues.fValues[q_id(Parton::sbar)] = res; 
      /// c-bar 
      i = -4; 
      getct10_(&i,&x,&Q,&res);
      m_PDFValues.fValues[q_id(Parton::cbar)] = res; 
      /// b-bar  
      i = -5; 
      getct10_(&i,&x,&Q,&res);
      m_PDFValues.fValues[q_id(Parton::bbar)] = res; 
      /// t-bar 
      m_PDFValues.fValues[q_id(Parton::tbar)] = res; 


      return m_PDFValues.fValues;
    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& CTEQ10_UPDFs::Uncertainties(double x, double Q2) const 
    {
      // do calculations
      return m_PDFValues.fUncertainties;
    }


  }
}
