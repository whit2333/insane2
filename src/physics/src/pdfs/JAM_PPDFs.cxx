#include "insane/pdfs/JAM_PPDFs.h"
#include "insane/pdfs/FortranWrappers.h"

namespace insane {
  namespace physics {

    JAM_PPDFs::JAM_PPDFs()
    {
      SetNameTitle("JAM_PPDFs","JAM pol. PDFs");
      SetLabel("JAM");
      char * lib_string  = const_cast<char*>("JAM15        ");
      char * dist_string = const_cast<char*>("PPDF         ");
      char * path_string = const_cast<char*>("             ");
      int    ipos = 0;
      // lib (character*10): library (JAM15,JAM16,etc.)
      // dist (character*10): distribution type (PPDF,FFpion,FFkaon,etc.)
      // ipos (integer): posterior number (0 to 199) from MC analysis
      grid_init_( path_string, lib_string, dist_string, &ipos );
      Reset();
    }
    //______________________________________________________________________________

    JAM_PPDFs::~JAM_PPDFs()
    { }
    //______________________________________________________________________________
    
    const std::array<double,NPartons>& JAM_PPDFs::Calculate(double x, double Q2) const
    {
      m_PDFValues.fx      = x;
      m_PDFValues.fQ2      = Q2;
      double  f = 0.0;
      char *  flav;

      flav =  const_cast<char*>("up"); double u_plus = 0.0;
      jam_xf_(&u_plus, &m_PDFValues.fx, &m_PDFValues.fQ2, flav );

      flav =  const_cast<char*>("dp"); double d_plus = 0.0;
      jam_xf_(&d_plus,&m_PDFValues.fx, &m_PDFValues.fQ2, flav );

      flav =  const_cast<char*>("sp"); double s_plus = 0.0;
      jam_xf_(&s_plus, &m_PDFValues.fx, &m_PDFValues.fQ2, flav );

      flav =  const_cast<char*>("u"); double u      = 0.0;
      jam_xf_(&u, &m_PDFValues.fx, &m_PDFValues.fQ2, flav );

      flav =  const_cast<char*>("d"); double d      = 0.0;
      jam_xf_(&d, &m_PDFValues.fx, &m_PDFValues.fQ2, flav );

      flav =  const_cast<char*>("s"); double s      = 0.0;
      jam_xf_(&s, &m_PDFValues.fx, &m_PDFValues.fQ2, flav );

      flav =  const_cast<char*>("gl"); double gl     = 0.0;
      jam_xf_(&gl, &m_PDFValues.fx, &m_PDFValues.fQ2, flav );

      m_PDFValues.fValues[q_id(Parton::u)   ] = u/x; //Delta up
      m_PDFValues.fValues[q_id(Parton::d)   ] = d/x; //Delta down
      m_PDFValues.fValues[q_id(Parton::s)   ] = s/x;      //Delta s
      m_PDFValues.fValues[q_id(Parton::g)   ] = gl/x;       //Delta g
      m_PDFValues.fValues[q_id(Parton::ubar)] = (u_plus-u)/x;     //Delta ubar
      m_PDFValues.fValues[q_id(Parton::dbar)] = (d_plus-d)/x;     //Delta dbar
      m_PDFValues.fValues[q_id(Parton::sbar)] = (s_plus-s)/x;     //Delta sbar

      return m_PDFValues.fValues;
    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& JAM_PPDFs::Uncertainties(double x, double Q2) const 
    {
      // do calculations
      return m_PDFValues.fUncertainties;
    }


  }
}
