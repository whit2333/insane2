#include "insane/pdfs/AAC08_PPDFs.h"

namespace insane {
  namespace physics {

    AAC08_PPDFs::AAC08_PPDFs(){
      SetNameTitle("AAC08_PPDFs","AAC08 pol. PDFs");
      SetLabel("AAC08");

      // These values are always zero.
     m_PDFValues.fValues[4] = 0.0;//Delta b
     m_PDFValues.fValues[5] = 0.0;//Delta t
     m_PDFValues.fValues[11] = 0.0;//Delta bbar
     m_PDFValues.fValues[12] = 0.0;//Delta tbar
      //for (int i = 0; i < 11; i++) gradientcol[0] = &gradient[i][0];
      //fGRAD = &gradientcol[0];

      for(Int_t i=0;i<12;i++) m_PDFValues.fUncertainties[i] = 0.; 
    }

    AAC08_PPDFs::~AAC08_PPDFs()
    { }

    //double* AAC08_PPDFs::GetPDFs(double x, double Qsq) const {
    //  m_PDFValues.fx = x;
    //  m_PDFValues.fQ2 = Qsq;
    //  int iset = 1;
    //  aac08pdf_(&m_PDFValues.fQ2, &m_PDFValues.fx, &iset, fXPDF, fGRAD);
    //  fPDFValues[0] = fXPDF[4]/x;//Delta up
    //  fPDFValues[1] = fXPDF[5]/x;//Delta down
    //  fPDFValues[2] = fXPDF[6]/x;//Delta s
    //  fPDFValues[3] = 0.0;//Delta c
    //  fPDFValues[4] = 0.0;//Delta b
    //  fPDFValues[5] = 0.0;//Delta t
    //  fPDFValues[6] = fXPDF[3]/x;//Delta g
    //  fPDFValues[7] = fXPDF[2]/x;//Delta ubar
    //  fPDFValues[8] = fXPDF[1]/x;//Delta dbar
    //  fPDFValues[9] = fXPDF[0]/x;//Delta sbar
    //  fPDFValues[10] = 0.0;//Delta cbar
    //  fPDFValues[11] = 0.0;//Delta bbar
    //  fPDFValues[12] = 0.0;//Delta tbar
    //  return(fPDFValues);
    //}

    const std::array<double,NPartons>& AAC08_PPDFs::Calculate    (double x, double Q2) const
    {

      double fXPDF[7];
      double * gradientcol[11];
      double gradient[11][11];
      double ** fGRAD; // note reverse array order for C->Fortran
      for (int i = 0; i < 11; i++) gradientcol[0] = &gradient[i][0];
      fGRAD = &gradientcol[0];

      m_PDFValues.fx = x;
      m_PDFValues.fQ2 = Q2;
      int iset = 1;
      aac08pdf_(&m_PDFValues.fQ2, &m_PDFValues.fx, &iset, fXPDF, fGRAD);

      //auto vals = GetPDFs(x,Q2);
      //for(Int_t i=0;i<12;i++) m_PDFValues.fValues[i] = vals[i];

      m_PDFValues.fValues[0]  = fXPDF[4] / x; // Delta up
      m_PDFValues.fValues[1]  = fXPDF[5] / x; // Delta down
      m_PDFValues.fValues[2]  = fXPDF[6] / x; // Delta s
      m_PDFValues.fValues[3]  = 0.0;          // Delta c
      m_PDFValues.fValues[4]  = 0.0;          // Delta b
      m_PDFValues.fValues[5]  = 0.0;          // Delta t
      m_PDFValues.fValues[6]  = fXPDF[3] / x; // Delta g
      m_PDFValues.fValues[7]  = fXPDF[2] / x; // Delta ubar
      m_PDFValues.fValues[8]  = fXPDF[1] / x; // Delta dbar
      m_PDFValues.fValues[9]  = fXPDF[0] / x; // Delta sbar
      m_PDFValues.fValues[10] = 0.0;          // Delta cbar
      m_PDFValues.fValues[11] = 0.0;          // Delta bbar
      m_PDFValues.fValues[12] = 0.0;          // Delta tbar

      m_PDFValues.fUncertainties[0]  = 0.0;
      m_PDFValues.fUncertainties[1]  = 0.0;
      m_PDFValues.fUncertainties[2]  = 0.0;
      m_PDFValues.fUncertainties[3]  = 0.0;
      m_PDFValues.fUncertainties[4]  = 0.0;
      m_PDFValues.fUncertainties[5]  = 0.0;
      m_PDFValues.fUncertainties[6]  = 0.0;
      m_PDFValues.fUncertainties[7]  = 0.0;
      m_PDFValues.fUncertainties[8]  = 0.0;
      m_PDFValues.fUncertainties[9]  = 0.0;
      m_PDFValues.fUncertainties[10] = 0.0;

      return(m_PDFValues.fValues);

    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& AAC08_PPDFs::Uncertainties(double x, double Q2) const
    {

     m_PDFValues.fUncertainties[0] = 0.0;
     m_PDFValues.fUncertainties[1] = 0.0;
     m_PDFValues.fUncertainties[2] = 0.0;
     m_PDFValues.fUncertainties[3] = 0.0;
     m_PDFValues.fUncertainties[6] = 0.0;
     m_PDFValues.fUncertainties[7] = 0.0;
     m_PDFValues.fUncertainties[8] = 0.0;
     m_PDFValues.fUncertainties[9] = 0.0;
     m_PDFValues.fUncertainties[10] = 0.0;

      return(m_PDFValues.fUncertainties);

    }
    //______________________________________________________________________________
  }
}

