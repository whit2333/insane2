#include "insane/pdfs/JAM_T3DFs.h"
#include "insane/pdfs/FortranWrappers.h"

namespace insane {
  namespace physics {

    JAM_T3DFs::JAM_T3DFs()
    {
      char * lib_string  = const_cast<char*>("JAM15        ");
      char * dist_string = const_cast<char*>("PPDF         ");
      char * path_string = const_cast<char*>("             ");
      int    ipos = 0;
      // lib (character*10): library (JAM15,JAM16,etc.)
      // dist (character*10): distribution type (PPDF,FFpion,FFkaon,etc.)
      // ipos (integer): posterior number (0 to 199) from MC analysis
      grid_init_( path_string, lib_string, dist_string, &ipos );
      Reset();
    }
    //______________________________________________________________________________

    JAM_T3DFs::~JAM_T3DFs()
    { }
    //______________________________________________________________________________

    const std::array<double,NPartons>& JAM_T3DFs::Calculate(double x, double Q2) const
    {
      m_PDFValues.fx      = x;
      m_PDFValues.fQ2      = Q2;
      double  t3_val = 0.0;
      char *  flav;

      flav =  const_cast<char*>("u3");
      jam_xf_(&t3_val, &m_PDFValues.fx, &m_PDFValues.fQ2, flav );
      m_PDFValues.fValues[q_id(Parton::u)] = t3_val;

      flav =  const_cast<char*>("d3");
      jam_xf_(&t3_val, &m_PDFValues.fx, &m_PDFValues.fQ2, flav );
      m_PDFValues.fValues[q_id(Parton::d)] = t3_val;

      //fxDd_Twist3 = t3d;
      // do calculations
      return m_PDFValues.fValues;
    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& JAM_T3DFs::Uncertainties(double x, double Q2) const 
    {
      // do calculations
      return m_PDFValues.fUncertainties;
    }


  }
} // namespace insane
