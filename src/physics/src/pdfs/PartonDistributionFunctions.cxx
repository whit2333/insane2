#include "insane/pdfs/PartonDistributionFunctions.h"
#include "TStyle.h"
#include "TMultiGraph.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TROOT.h"

namespace insane {
namespace physics {
//_____________________________________________________________________________
PDFBase::PDFBase() : fLabel("PDFbase"),
   fxPlotMin(0.01),fxPlotMax(1.0),
   fModelMin_x(0.00001),fModelMax_x(0.99999),
   fModelMin_W(1.0),fModelMax_W(1.0e9),
   fModelMin_Q2(1.0),fModelMax_Q2(1.0e9)
{
   for (int i = 0; i < NPARTONDISTS; i++) {
      //fFunctions[i] = nullptr;
      fPDFValues[i] = 0.0;
      fPDFErrors[i] = 0.0;
   }
   SetLineWidth(2);
   //ClearValues();
   //fDefaultLineColor = 1;
   //fDefaultLineStyle = 1;
}
//_____________________________________________________________________________

PDFBase::~PDFBase() {
}
//_____________________________________________________________________________

void PDFBase::ClearValues() {
   for (int i = 0; i < NPARTONDISTS; i++) {
      fPDFValues[i] = 0.0;
      fPDFErrors[i] = 0.0;
   }
}
//_____________________________________________________________________________

//_____________________________________________________________________________

    PartonDistributionFunctions * PartonDistributionFunctions::fgPartonDistributionFunctions = nullptr;
    //________________________________________________________________________________

//TF1 * PartonDistributionFunctions::GetFunction(PDFBase::PartonFlavor q ) {
//   if (fFunctions[q]){ 
//      //fFunctions[q]->SetLineColor(fDefaultLineColor);
//      fFunctions[q]->TAttLine::operator=(*this);
//      return(fFunctions[q]);
//   } else {
//      Int_t npar = 1;
//      switch (q) {
//         case PDFBase::kUP :
//            fFunctions[q] = new TF1(Form("xu %s", GetLabel()), this, &PartonDistributionFunctions::Evaluatexu,
//                  fxPlotMin, fxPlotMax, npar, "PartonDistributionFunctions", "Evaluate_u");
//            break;
//         case PDFBase::kDOWN :
//            fFunctions[q] = new TF1(Form("xd %s", GetLabel()), this, &PartonDistributionFunctions::Evaluatexd,
//                  fxPlotMin, fxPlotMax, npar, "PartonDistributionFunctions", "Evaluate_d");
//            break;
//
//         case PDFBase::kSTRANGE :
//            fFunctions[q] = new TF1("xs", this, &PartonDistributionFunctions::Evaluatexs,
//                  fxPlotMin, fxPlotMax, npar, "PartonDistributionFunctions", "Evaluatexs");
//            break;
//
//         case PDFBase::kGLUON :
//            fFunctions[q] = new TF1("xg", this, &PartonDistributionFunctions::Evaluatexg,
//                  fxPlotMin, fxPlotMax, npar, "PartonDistributionFunctions", "Evaluatexg");
//            break;
//
//         case PDFBase::kANTIUP :
//            fFunctions[q] = new TF1("xubar", this, &PartonDistributionFunctions::Evaluatexubar,
//                  fxPlotMin, fxPlotMax, npar, "PartonDistributionFunctions", "Evaluatexubar");
//            break;
//
//         case PDFBase::kANTIDOWN :
//            fFunctions[q] = new TF1("xdbar", this, &PartonDistributionFunctions::Evaluatexdbar,
//                  fxPlotMin, fxPlotMax, npar, "PartonDistributionFunctions", "Evaluatexdbar");
//            break;
//
//         case PDFBase::kANTISTRANGE :
//            fFunctions[q] = new TF1("xsbar", this, &PartonDistributionFunctions::Evaluatexsbar,
//                  fxPlotMin, fxPlotMax, npar, "PartonDistributionFunctions", "Evaluatexsbar");
//            break;
//         default :
//            fFunctions[q] = new TF1(Form("xu %s", GetLabel()), this, &PartonDistributionFunctions::Evaluatexu,
//                  fxPlotMin, fxPlotMax, npar, "PartonDistributionFunctions", "Evaluatexu");
//            break;
//
//      }
//   }
//   //fFunctions[q]->SetLineColor(fDefaultLineColor);
//   fFunctions[q]->TAttLine::operator=(*this);
//   return(fFunctions[q]);
//}
////_____________________________________________________________________________
void PartonDistributionFunctions::GetValues(TObject *obj, double Q2, Parton q){
   // Fills histogram with values.
   //  For error band use GetErrorBand
   //if ( !(obj->InheritsFrom(TH1::Class())) ) {
   //   Error("GetErrorBand","Not a TH1 class");
   //   return;
   //}
   if(!obj) {
      return;
   }
   //  returns errorsband
   auto *hfit = (TH1*)obj;
   Int_t hxfirst = hfit->GetXaxis()->GetFirst();
   Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
   Int_t hyfirst = hfit->GetYaxis()->GetFirst();
   Int_t hylast  = hfit->GetYaxis()->GetLast(); 
   Int_t hzfirst = hfit->GetZaxis()->GetFirst();
   Int_t hzlast  = hfit->GetZaxis()->GetLast(); 

   TAxis *xaxis  = hfit->GetXaxis();
   TAxis *yaxis  = hfit->GetYaxis();
   TAxis *zaxis  = hfit->GetZaxis();

   double x[3];

   for (Int_t binz=hzfirst; binz<=hzlast; binz++){
      x[2]=zaxis->GetBinCenter(binz);
      for (Int_t biny=hyfirst; biny<=hylast; biny++) {
         x[1]=yaxis->GetBinCenter(biny);
         for (Int_t binx=hxfirst; binx<=hxlast; binx++) {
            x[0]=xaxis->GetBinCenter(binx);

   GetPDFs(x[0], Q2);
            hfit->SetBinContent(binx, biny, binz, x[0]*fPDFValues[q]);
   //GetPDFErrors(x[0], Q2);
   //         hfit->SetBinError(binx, biny, binz, x[0]*fPDFErrors[q]);
         }
      }
   }

}
//_____________________________________________________________________________
void PartonDistributionFunctions::GetErrorBand(TObject *obj, double Q2, Parton q){
   //if ( !(obj->InheritsFrom(TH1::Class())) ) {
   //   Error("GetErrorBand","Not a TH1 class");
   //   return;
   //}
   if(!obj) {
      return;
   }
   //  returns errorsband
   auto *hfit = (TH1*)obj;
   Int_t hxfirst = hfit->GetXaxis()->GetFirst();
   Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
   Int_t hyfirst = hfit->GetYaxis()->GetFirst();
   Int_t hylast  = hfit->GetYaxis()->GetLast(); 
   Int_t hzfirst = hfit->GetZaxis()->GetFirst();
   Int_t hzlast  = hfit->GetZaxis()->GetLast(); 

   TAxis *xaxis  = hfit->GetXaxis();
   TAxis *yaxis  = hfit->GetYaxis();
   TAxis *zaxis  = hfit->GetZaxis();

   double x[3];

   
   for (Int_t binz=hzfirst; binz<=hzlast; binz++){
      x[2]=zaxis->GetBinCenter(binz);
      for (Int_t biny=hyfirst; biny<=hylast; biny++) {
         x[1]=yaxis->GetBinCenter(biny);
         for (Int_t binx=hxfirst; binx<=hxlast; binx++) {
            x[0]=xaxis->GetBinCenter(binx);

   GetPDFs(x[0], Q2);
            hfit->SetBinContent(binx, biny, binz, x[0]*fPDFValues[q]);
   GetPDFErrors(x[0], Q2);
            hfit->SetBinError(binx, biny, binz, x[0]*fPDFErrors[q]);
         }
      }
   }

}
//______________________________________________________________________________

//void PartonDistributionFunctions::PlotPDFs(double Qsq, Int_t log)
//{
//
//   const Int_t N = 30;
//   double xmin = 0.0001;
//   double xmax = 0.9999;
//   //double Qsqmin=1.0;
//   //double Qsqmax=12.0;
//   //double QMean=5.4;
//   double dx = (xmax - xmin) / ((double)N);
//   //double dQsq=(Qsqmax-Qsqmin)/((double)N);
//
//
//   double x = 0;
//   //, val1,val2;
//   Int_t pointNumber = 0;
//
//   auto *mg = new TMultiGraph();
//
//   auto * gUpVsX = new TGraph();
//   gUpVsX->SetTitle(Form("xu(x) [%s]", GetLabel()));
//   gUpVsX->SetMarkerColor(1);
//   gUpVsX->SetMarkerStyle(21);
//   gUpVsX->SetMarkerSize(1.3);
//   gUpVsX->SetLineColor(1);
//   gUpVsX->SetLineWidth(3);
//
//   auto * gDownVsX = new TGraph();
//   gDownVsX->SetMarkerColor(2);
//   gDownVsX->SetMarkerStyle(22);
//   gDownVsX->SetMarkerSize(1.3);
//   gDownVsX->SetLineColor(2);
//   gDownVsX->SetLineWidth(3);
//   gDownVsX->SetTitle(Form("xd(x) [%s]", GetLabel()));
//
//   auto * gStrangeVsX = new TGraph();
//   gStrangeVsX->SetMarkerColor(3);
//   gStrangeVsX->SetMarkerStyle(23);
//   gStrangeVsX->SetMarkerSize(1.3);
//   gStrangeVsX->SetLineColor(3);
//   gStrangeVsX->SetLineWidth(3);
//   gStrangeVsX->SetTitle(Form("xs(x) [%s]", GetLabel()));
//
//   auto * gUpbarVsX = new TGraph();
//   gUpbarVsX->SetMarkerColor(4);
//   gUpbarVsX->SetMarkerStyle(24);
//   gUpbarVsX->SetMarkerSize(1.3);
//   gUpbarVsX->SetLineColor(4);
//   gUpbarVsX->SetLineWidth(3);
//   gUpbarVsX->SetTitle(Form("x#bar{u}(x) [%s]", GetLabel()));
//
//   auto * gDownbarVsX = new TGraph();
//   gDownbarVsX->SetMarkerColor(5);
//   gDownbarVsX->SetMarkerStyle(25);
//   gDownbarVsX->SetMarkerSize(1.3);
//   gDownbarVsX->SetLineColor(5);
//   gDownbarVsX->SetLineWidth(3);
//   gDownbarVsX->SetTitle(Form("x#bar{d}(x) [%s]", GetLabel()));
//
//   auto * gStrangebarVsX = new TGraph();
//   gStrangebarVsX->SetMarkerColor(6);
//   gStrangebarVsX->SetMarkerStyle(26);
//   gStrangebarVsX->SetMarkerSize(1.3);
//   gStrangebarVsX->SetLineColor(6);
//   gStrangebarVsX->SetLineWidth(3);
//   gStrangebarVsX->SetTitle(Form("x#bar{s}(x) [%s]", GetLabel()));
//
//   double * pdfvals;
//   pointNumber = 0;
//   for (int i = 0; i < N; i++) {
//      x = xmin + (double)i * dx;
//      pdfvals =  GetPDFs(x, Qsq);
//      gUpVsX->SetPoint(pointNumber, x, u());
//      gUpbarVsX->SetPoint(pointNumber, x, ubar());
//      gDownVsX->SetPoint(pointNumber, x, d());
//      gDownbarVsX->SetPoint(pointNumber, x, dbar());
//      gStrangeVsX->SetPoint(pointNumber, x, s());
//      gStrangebarVsX->SetPoint(pointNumber, x, sbar());
//      pointNumber++;
//   }
//
//   /*  TCanvas * fCanvas = new TCanvas("pdfs","Parton Dist Functions",0,0,600,400);*/
//   /*  fCanvas->Divide(3,2);*/
//   auto *fCanvas = new TCanvas(Form("cUnPolPdfs%d", (Int_t)Qsq), "Unpolarized PDFs", 0, 0, 600, 400);
//   fCanvas->cd(0)->SetLogx(log);
//
//   mg->Add(gUpVsX, "LP");
//   mg->Add(gDownVsX, "LP");
//   mg->Add(gStrangeVsX, "LP");
//   mg->Add(gUpbarVsX, "LP");
//   mg->Add(gDownbarVsX, "LP");
//   mg->Add(gStrangebarVsX, "LP");
//   mg->Draw("A");
//
//   auto * leg = new TLegend(0.7, 0.7, 0.95, 0.95);
//   leg->SetHeader(Form("%s Polarized PDFs", GetLabel()));
//   leg->AddEntry(gUpVsX, gUpVsX->GetTitle(), "lp");
//   leg->AddEntry(gDownVsX, gDownVsX->GetTitle(), "lp");
//   leg->AddEntry(gStrangeVsX, gStrangeVsX->GetTitle(), "lp");
//   leg->Draw();
//
//   auto * t = new TLatex();
//   t->SetNDC();
//   t->SetTextFont(62);
//   t->SetTextColor(36);
//   t->SetTextSize(0.06);
//   t->SetTextAlign(12);
//   t->DrawLatex(0.05, 0.95, Form("Q^{2} = %d GeV^{2}", (Int_t)Qsq));
//
//
////   fCanvas->cd(1)->SetLogx(log);
////   gUpVsX->Draw("ALP");
////   fCanvas->cd(2)->SetLogx(log);
////   gDownVsX->Draw("ALP");
////   fCanvas->cd(3)->SetLogx(log);
////   gStrangeVsX->Draw("ALP");
////   fCanvas->cd(4)->SetLogx(log);
////   gUpbarVsX->Draw("ALP");
////   fCanvas->cd(5)->SetLogx(log);
////   gDownbarVsX->Draw("ALP");
////   fCanvas->cd(6)->SetLogx(log);
////   gStrangebarVsX->Draw("ALP");
//}
////_____________________________________________________________________________
//
//TCanvas *  PartonDistributionFunctions::PlotPDF(double Qsq, Int_t log)
//{
//
//   return(new TCanvas());
//}
//_____________________________________________________________________________

}}
