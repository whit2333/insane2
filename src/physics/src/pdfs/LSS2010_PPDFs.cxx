#include "insane/pdfs/LSS2010_PPDFs.h"

namespace insane {
  namespace physics {

    LSS2010_PPDFs::LSS2010_PPDFs() : fiSet(1) {

      SetNameTitle("LSS2010_PPDFs", "LSS2010 pol. PDFs");
      SetLabel("LSS2010");
      // These values are always zero.
      m_PDFValues.fValues[4]  = 0.0; // Delta b
      m_PDFValues.fValues[5]  = 0.0; // Delta t
      m_PDFValues.fValues[11] = 0.0; // Delta bbar
      m_PDFValues.fValues[12] = 0.0; // Delta tbar

      for (Int_t i = 0; i < 12; i++)
        m_PDFValues.fUncertainties[i] = 0.;
      lss2010init_();
    }

    LSS2010_PPDFs::~LSS2010_PPDFs() {}

    const std::array<double, NPartons>& LSS2010_PPDFs::Calculate(double x, double Q2) const {

      m_PDFValues.fx  = x;
      m_PDFValues.fQ2 = Q2;

      m_PDFValues.fUncertainties[0]  = 0.0;
      m_PDFValues.fUncertainties[1]  = 0.0;
      m_PDFValues.fUncertainties[2]  = 0.0;
      m_PDFValues.fUncertainties[3]  = 0.0;
      m_PDFValues.fUncertainties[6]  = 0.0;
      m_PDFValues.fUncertainties[7]  = 0.0;
      m_PDFValues.fUncertainties[8]  = 0.0;
      m_PDFValues.fUncertainties[9]  = 0.0;
      m_PDFValues.fUncertainties[10] = 0.0;

      auto   FISET    = (int)fiSet;
      auto   xBjorken = (double)x;
      double UUB, DDB, UV, DV, UB, DB, ST, GL;

      if (Q2 < 1.0) {

        double Q2_km1 = 1.0;
        double Q2_k   = 1.1;
        double UUB_km1, DDB_km1, GL_km1, UV_km1, DV_km1, UB_km1, DB_km1, ST_km1;
        double UUB_k, DDB_k, GL_k, UV_k, DV_k, UB_k, DB_k, ST_k;
        lss2010_(&FISET, &xBjorken, &Q2_km1, &UUB_km1, &DDB_km1, &UV_km1, &DV_km1, &UB_km1, &DB_km1,
                 &ST_km1, &GL_km1);
        lss2010_(&FISET, &xBjorken, &Q2_k, &UUB_k, &DDB_k, &UV_k, &DV_k, &UB_k, &DB_k, &ST_k,
                 &GL_k);
        UUB = Extrapolate(Q2, Q2_km1, UUB_km1, Q2_k, UUB_k);
        UB  = Extrapolate(Q2, Q2_km1, UB_km1, Q2_k, UB_k);
        DDB = Extrapolate(Q2, Q2_km1, DDB_km1, Q2_k, DDB_k);
        DB  = Extrapolate(Q2, Q2_km1, DB_km1, Q2_k, DB_k);
        ST  = Extrapolate(Q2, Q2_km1, ST_km1, Q2_k, ST_k);
        GL  = Extrapolate(Q2, Q2_km1, GL_km1, Q2_k, GL_k);

      } else {

        lss2010_(&FISET, &xBjorken, &Q2, &UUB, &DDB, &UV, &DV, &UB, &DB, &ST, &GL);
      }

      m_PDFValues.fValues[0]  = (UUB - UB) / x; // Delta up
      m_PDFValues.fValues[1]  = (DDB - DB) / x; // Delta down
      m_PDFValues.fValues[2]  = ST / x;         // Delta s
      m_PDFValues.fValues[3]  = 0.0;            // Delta c
      m_PDFValues.fValues[4]  = 0.0;            // Delta b
      m_PDFValues.fValues[5]  = 0.0;            // Delta t
      m_PDFValues.fValues[6]  = GL / x;         // Delta g
      m_PDFValues.fValues[7]  = UB / x;         // Delta ubar
      m_PDFValues.fValues[8]  = DB / x;         // Delta dbar
      m_PDFValues.fValues[9]  = ST / x;         // Delta sbar
      m_PDFValues.fValues[10] = 0.0;            // Delta cbar
      m_PDFValues.fValues[11] = 0.0;            // Delta bbar
      m_PDFValues.fValues[12] = 0.0;            // Delta tbar
      return (m_PDFValues.fValues);
    }
    //______________________________________________________________________________

    double LSS2010_PPDFs::Extrapolate(double x, double x_km1, double f_km1, double x_k,
                                      double f_k) const {
      // extrapolation to point x, using the k-1 and k points preceeding x
      double f = f_km1 + ((x - x_km1) / (x_k - x_km1)) * (f_k - f_km1);
      return f;
    }
    //______________________________________________________________________________

    const std::array<double, NPartons>& LSS2010_PPDFs::Uncertainties(double x, double Q2) const {

      m_PDFValues.fUncertainties[0]  = 0.0;
      m_PDFValues.fUncertainties[1]  = 0.0;
      m_PDFValues.fUncertainties[2]  = 0.0;
      m_PDFValues.fUncertainties[3]  = 0.0;
      m_PDFValues.fUncertainties[6]  = 0.0;
      m_PDFValues.fUncertainties[7]  = 0.0;
      m_PDFValues.fUncertainties[8]  = 0.0;
      m_PDFValues.fUncertainties[9]  = 0.0;
      m_PDFValues.fUncertainties[10] = 0.0;

      return (m_PDFValues.fUncertainties);
    }
    //______________________________________________________________________________
  } // namespace physics
} // namespace insane
