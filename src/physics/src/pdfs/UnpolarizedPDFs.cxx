#include "insane/pdfs/UnpolarizedPDFs.h"
#include "TMath.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "insane/base/Math.h"

namespace insane {
  namespace physics {

    UnpolarizedPDFs::UnpolarizedPDFs()
    { }
    //______________________________________________________________________________

    UnpolarizedPDFs::~UnpolarizedPDFs()
    { }
    //______________________________________________________________________________
    
    double UnpolarizedPDFs::F1(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return F1p_Twist2(x, Q2);
          break;
        case Nuclei::n : 
          return F1n_Twist2(x, Q2);
          break;
        default :
          return 0.0;
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double UnpolarizedPDFs::F2(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return F2p_Twist2(x, Q2);
          break;
        case Nuclei::n : 
          return F2n_Twist2(x, Q2);
          break;
        default :
          return 0.0;
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________
        
    double UnpolarizedPDFs::F1_TMC(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return F1p_Twist2_TMC(x, Q2);
          break;
        case Nuclei::n : 
          return F1n_Twist2_TMC(x, Q2);
          break;
        default :
          return 0.0;
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double UnpolarizedPDFs::F2_TMC(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return F2p_Twist2_TMC(x, Q2);
          break;
        case Nuclei::n : 
          return F2n_Twist2_TMC(x, Q2);
          break;
        default :
          return 0.0;
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________
        
    double UnpolarizedPDFs::F1p_Twist2(  double x, double Q2) const
    {
      if(!IsComputed(x,Q2)){
        Calculate(x,Q2);
      }
      double res = 0.0;
      for(auto f: LightQuarks) {
        res += PartonCharge2[q_id(f)]*Get(f);
      }
      return res/2.0;
    }
    //______________________________________________________________________________

    double UnpolarizedPDFs::F1n_Twist2(  double x, double Q2) const
    {
      if(!IsComputed(x,Q2)){
        Calculate(x,Q2);
      }
      double res = 0.0;
      for(auto f: LightQuarks) {
        res += IsoSpinConjugatePartonCharge2[q_id(f)]*Get(f);
      }
      return res/2.0;
    }
    //______________________________________________________________________________

    double UnpolarizedPDFs::F1d_Twist2(  double x, double Q2) const
    {
      if(!IsComputed(x,Q2)){
        Calculate(x,Q2);
      }
      double wD      = 0.058;         // D-wave state probability  
      double F1n_val = F1n_Twist2(x,Q2); 
      double F1p_val = F1p_Twist2(x,Q2); 
      double result  = 0.5*(1.-1.5*wD)*(F1p_val + F1n_val);
      return(result);
    }
    //_____________________________________________________________________________

    double UnpolarizedPDFs::F1He3_Twist2(double x, double Q2) const
    {
      if(!IsComputed(x,Q2)){
        Calculate(x,Q2);
      }
      // Leading twist F1
      double Pn      = 0.879;         // neutron polarization in 3He 
      double Pp      = -0.021;        // proton polarization in 3He 
      double F1n_val = F1n_Twist2(x,Q2); 
      double F1p_val = F1p_Twist2(x,Q2); 
      double result  = (Pn + 0.056)*F1n_val + (2.*Pp - 0.014)*F1p_val; 
      return(result);
    }
    //______________________________________________________________________________

    double UnpolarizedPDFs::F2p_Twist2(double x, double Q2) const
    {
      if(!IsComputed(x,Q2)){
        Calculate(x,Q2);
      }
      double res = 0.0;
      for(auto f: LightQuarks) {
        res += PartonCharge2[q_id(f)]*Get(f);
         //std::cout << " " << PartonCharge2[f] << "*" << Get(f);;
      }
      //std::cout << '\n';
      return res*x;
    }
    //______________________________________________________________________________

    double UnpolarizedPDFs::F2n_Twist2(  double x, double Q2) const
    {
      if(!IsComputed(x,Q2)){
        Calculate(x,Q2);
      }
      double res = 0.0;
      for(auto f: LightQuarks) {
        res += IsoSpinConjugatePartonCharge2[q_id(f)]*Get(f);
      }
      return res*x;
    }
    //______________________________________________________________________________

    double UnpolarizedPDFs::F2d_Twist2(  double x, double Q2) const
    {
      if(!IsComputed(x,Q2)){
        Calculate(x,Q2);
      }
      double wD      = 0.058;         // D-wave state probability  
      double F2n_val = F2n_Twist2(x,Q2); 
      double F2p_val = F2p_Twist2(x,Q2); 
      double result  = 0.5*(1.-1.5*wD)*(F2p_val + F2n_val);
      return(result);
    }
    //______________________________________________________________________________

    double UnpolarizedPDFs::F2He3_Twist2(double x, double Q2) const
    {
      if(!IsComputed(x,Q2)){
        Calculate(x,Q2);
      }
      // Leading twist F1
      double Pn      = 0.879;         // neutron polarization in 3He 
      double Pp      = -0.021;        // proton polarization in 3He 
      double F2n_val = F2n_Twist2(x,Q2); 
      double F2p_val = F2p_Twist2(x,Q2); 
      double result  = (Pn + 0.056)*F2n_val + (2.*Pp - 0.014)*F2p_val; 
      return(result);
    }
    //______________________________________________________________________________
    
    double UnpolarizedPDFs::F1p_Twist2_TMC(  double x, double Q2) const
    {
      double result = 0.0;
      // leading order in 1/Q2 
      double M         = M_p/GeV; 
      double rho       = TMath::Sqrt(1. + 4.*x*x*M*M/Q2); 
      double xi        = 2.*x/(1. + rho); 
      double F1_0      = F1p_Twist2(xi,Q2); 
      double h2        = insane::physics::TMCs::h2([&](double z){return this->F2p_Twist2(z,Q2);},xi,Q2);
      double g2        = insane::physics::TMCs::g2([&](double z){return this->F2p_Twist2(z,Q2);},xi,Q2);
      result           = (F1_0*x)/(xi*rho) + M*M*x*x*h2/(Q2*rho*rho) + 2.0*M*M*M*M*x*x*x*g2/(Q2*Q2*rho*rho*rho);
      return(result);
    }
    //______________________________________________________________________________

    double UnpolarizedPDFs::F1n_Twist2_TMC(  double x, double Q2) const
    {
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = (x/(xi*rho*rho*rho))*F1n_Twist2(xi,Q2);
      double t1     = (rho*rho-1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::gaus(
          [&,this](double z){
          return( (((x+xi)/xi-(3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*this->F1n_Twist2(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double UnpolarizedPDFs::F1d_Twist2_TMC(  double x, double Q2) const
    {
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = (x/(xi*rho*rho*rho))*F1d_Twist2(xi,Q2);
      double t1     = (rho*rho-1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::gaus(
          [&,this](double z){
          return( (((x+xi)/xi-(3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*this->F1d_Twist2(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________
    
    double UnpolarizedPDFs::F1He3_Twist2_TMC(double x, double Q2) const
    {
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = (x/(xi*rho*rho*rho))*F1He3_Twist2(xi,Q2);
      double t1     = (rho*rho-1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::gaus(
          [&,this](double z){
          return( (((x+xi)/xi-(3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*this->F1He3_Twist2(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double UnpolarizedPDFs::F2p_Twist2_TMC(  double x, double Q2) const
    {
      double M         = M_p/GeV; 
      double rho       = TMath::Sqrt(1. + 4.*x*x*M*M/Q2); 
      double xi        = 2.*x/(1. + rho); 
      double F2_0      = F2p_Twist2(xi,Q2); 
      double h2        = insane::physics::TMCs::h2([&](double z){return this->F2p_Twist2(z,Q2);}, xi,Q2);
      double g2        = insane::physics::TMCs::g2([&](double z){return this->F2p_Twist2(z,Q2);}, xi,Q2);
      double result    = (F2_0*x*x)/(xi*xi*rho*rho*rho) + 6.0*M*M*x*x*x*h2/(Q2*rho*rho*rho*rho) + 12.0*M*M*M*M*x*x*x*x*g2/(Q2*Q2*rho*rho*rho*rho*rho);
      return(result);
    }
    //______________________________________________________________________________

    double UnpolarizedPDFs::F2n_Twist2_TMC(  double x, double Q2) const
    {
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M         = (insane::units::M_p/insane::units::GeV);
      double gamma2    = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho       = TMath::Sqrt(1.0+gamma2);
      double res       = -1.0*(x/(xi*rho*rho*rho))*F1n_Twist2(xi,Q2);
      double t1        = (1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::gaus(
          [&,this](double z){
          return( (((x)/xi - (rho*rho-1.0)+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/(2.0*rho))/z)*this->F1n_Twist2(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double UnpolarizedPDFs::F2d_Twist2_TMC(  double x, double Q2) const
    {
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M         = (insane::units::M_p/insane::units::GeV);
      double gamma2    = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho       = TMath::Sqrt(1.0+gamma2);
      double res       = -1.0*(x/(xi*rho*rho*rho))*F1d_Twist2(xi,Q2);
      double t1        = (1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::gaus(
          [&,this](double z){
          return( (((x)/xi - (rho*rho-1.0)+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/(2.0*rho))/z)*this->F1d_Twist2(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double UnpolarizedPDFs::F2He3_Twist2_TMC(double x, double Q2) const
    {
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M         = (insane::units::M_p/insane::units::GeV);
      double gamma2    = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho       = TMath::Sqrt(1.0+gamma2);
      double res       = -1.0*(x/(xi*rho*rho*rho))*F1He3_Twist2(xi,Q2);
      double t1        = (1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::gaus(
          [&,this](double z){
          return( (((x)/xi - (rho*rho-1.0)+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/(2.0*rho))/z)*this->F1He3_Twist2(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    
        double UnpolarizedPDFs::u()    const { return 0.0; } 
        double UnpolarizedPDFs::d()    const { return 0.0; } 
        double UnpolarizedPDFs::s()    const { return 0.0; } 
        double UnpolarizedPDFs::c()    const { return 0.0; } 
        double UnpolarizedPDFs::b()    const { return 0.0; } 
        double UnpolarizedPDFs::t()    const { return 0.0; } 
        double UnpolarizedPDFs::g()    const { return 0.0; } 
        double UnpolarizedPDFs::ubar() const { return 0.0; } 
        double UnpolarizedPDFs::dbar() const { return 0.0; } 
        double UnpolarizedPDFs::sbar() const { return 0.0; } 
        double UnpolarizedPDFs::cbar() const { return 0.0; } 
        double UnpolarizedPDFs::bbar() const { return 0.0; } 
        double UnpolarizedPDFs::tbar() const { return 0.0; } 

        double UnpolarizedPDFs::u_uncertainty()    const {return 0.0;}
        double UnpolarizedPDFs::d_uncertainty()    const {return 0.0;}
        double UnpolarizedPDFs::s_uncertainty()    const {return 0.0;}
        double UnpolarizedPDFs::c_uncertainty()    const {return 0.0;}
        double UnpolarizedPDFs::b_uncertainty()    const {return 0.0;}
        double UnpolarizedPDFs::t_uncertainty()    const {return 0.0;}
        double UnpolarizedPDFs::g_uncertainty()    const {return 0.0;}
        double UnpolarizedPDFs::ubar_uncertainty() const {return 0.0;}
        double UnpolarizedPDFs::dbar_uncertainty() const {return 0.0;}
        double UnpolarizedPDFs::sbar_uncertainty() const {return 0.0;}
        double UnpolarizedPDFs::cbar_uncertainty() const {return 0.0;}
        double UnpolarizedPDFs::bbar_uncertainty() const {return 0.0;}
        double UnpolarizedPDFs::tbar_uncertainty() const {return 0.0;}


  }
}

