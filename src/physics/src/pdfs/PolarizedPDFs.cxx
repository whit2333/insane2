#include "insane/pdfs/PolarizedPDFs.h"
#include "TMath.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "insane/base/Math.h"

namespace insane {
  namespace physics {

    PolarizedPDFs::PolarizedPDFs()
    { }
    //______________________________________________________________________________

    PolarizedPDFs::~PolarizedPDFs()
    { }
    //______________________________________________________________________________

    double PolarizedPDFs::g1(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return g1p_Twist2(x, Q2);
          break;
        case Nuclei::n : 
          return g1n_Twist2(x, Q2);
          break;
        default :
          return 0.0;
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g2(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return g2p_Twist2(x, Q2);
          break;
        case Nuclei::n : 
          return g2n_Twist2(x, Q2);
          break;
        default :
          return 0.0;
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g1_TMC(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return g1p_Twist2_TMC(x, Q2);
          break;
        case Nuclei::n : 
          return g1n_Twist2_TMC(x, Q2);
          break;
        default :
          return 0.0;
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g2_TMC(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return g2p_Twist2_TMC(x, Q2);
          break;
        case Nuclei::n : 
          return g2n_Twist2_TMC(x, Q2);
          break;
        default :
          return 0.0;
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g1p_Twist2(  double x, double Q2) const
    {
      if(!IsComputed(x,Q2)){
        Calculate(x,Q2);
      }
      double res = 0.0;
      for(auto f: LightQuarks) {
        res += PartonCharge2[q_id(f)]*Get(f);
      }
      return res/2.0;
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g1n_Twist2(  double x, double Q2) const
    {
      if(!IsComputed(x,Q2)){
        Calculate(x,Q2);
      }
      double res = 0.0;
      for(auto f: LightQuarks) {
        res += IsoSpinConjugatePartonCharge2[q_id(f)]*Get(f);
      }
      return res/2.0;
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g1d_Twist2(  double x, double Q2) const
    {
      double wD      = 0.058;         // D-wave state probability  
      double g1n_val = g1n_Twist2(x,Q2); 
      double g1p_val = g1p_Twist2(x,Q2); 
      double result  = 0.5*(1.-1.5*wD)*(g1p_val + g1n_val);
      return(result);
    }
    //_____________________________________________________________________________

    double PolarizedPDFs::g1He3_Twist2(double x, double Q2) const
    {
      // Leading twist g1
      double Pn      = 0.879;         // neutron polarization in 3He 
      double Pp      = -0.021;        // proton polarization in 3He 
      double g1n_val = g1n_Twist2(x,Q2); 
      double g1p_val = g1p_Twist2(x,Q2); 
      double result  = (Pn + 0.056)*g1n_val + (2.*Pp - 0.014)*g1p_val; 
      return(result);
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g1p_Twist2_TMC(  double x, double Q2) const
    {
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = (x/(xi*rho*rho*rho))*g1p_Twist2(xi,Q2);
      double t1     = (rho*rho-1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::simple(
          [&,this](double z){
          return( (((x+xi)/xi-(3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1p_Twist2(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g1n_Twist2_TMC(  double x, double Q2) const
    {
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = (x/(xi*rho*rho*rho))*g1n_Twist2(xi,Q2);
      double t1     = (rho*rho-1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::simple(
          [&,this](double z){
          return( (((x+xi)/xi-(3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1n_Twist2(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g1d_Twist2_TMC(  double x, double Q2) const
    {
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = (x/(xi*rho*rho*rho))*g1d_Twist2(xi,Q2);
      double t1     = (rho*rho-1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::simple(
          [&,this](double z){
          return( (((x+xi)/xi-(3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1d_Twist2(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________
    
    double PolarizedPDFs::g1He3_Twist2_TMC(double x, double Q2) const
    {
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = (x/(xi*rho*rho*rho))*g1He3_Twist2(xi,Q2);
      double t1     = (rho*rho-1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::simple(
          [&,this](double z){
          return( (((x+xi)/xi-(3.0-rho*rho)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1He3_Twist2(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g2p_Twist2_TMC(  double x, double Q2) const
    {
      // WW relation with twist-2 target mass effects included.
      // Note the WW relation  holds in the presence of TMCs.
      //double integral_result =  insane::physics::WW( [&,this](double z){
      //    return( g1p_Twist2(z,Q2) );
      //    //return( g1p_Twist2_TMC(z,Q2) );
      //    }, x,1.0);
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M         = (insane::units::M_p/insane::units::GeV);
      double gamma2    = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho       = TMath::Sqrt(1.0+gamma2);
      double res       = -1.0*(x/(xi*rho*rho*rho))*g1p_Twist2(xi,Q2);
      double t1        = (1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::simple(
          [&,this](double z){
          return( (((x)/xi - (rho*rho-1.0)+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1p_Twist2(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
      //return( integral_result );
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g2n_Twist2_TMC(  double x, double Q2) const
    {
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M         = (insane::units::M_p/insane::units::GeV);
      double gamma2    = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho       = TMath::Sqrt(1.0+gamma2);
      double res       = -1.0*(x/(xi*rho*rho*rho))*g1n_Twist2(xi,Q2);
      double t1        = (1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::simple(
          [&,this](double z){
          return( (((x)/xi - (rho*rho-1.0)+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1n_Twist2(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g2d_Twist2_TMC(  double x, double Q2) const
    {
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M         = (insane::units::M_p/insane::units::GeV);
      double gamma2    = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho       = TMath::Sqrt(1.0+gamma2);
      double res       = -1.0*(x/(xi*rho*rho*rho))*g1d_Twist2(xi,Q2);
      double t1        = (1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::simple(
          [&,this](double z){
          return( (((x)/xi - (rho*rho-1.0)+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1d_Twist2(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g2He3_Twist2_TMC(double x, double Q2) const
    {
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M         = (insane::units::M_p/insane::units::GeV);
      double gamma2    = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho       = TMath::Sqrt(1.0+gamma2);
      double res       = -1.0*(x/(xi*rho*rho*rho))*g1He3_Twist2(xi,Q2);
      double t1        = (1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::simple(
          [&,this](double z){
          return( (((x)/xi - (rho*rho-1.0)+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/(2.0*rho))/z)*this->g1He3_Twist2(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g2p_WW(double x, double Q2) const
    {
      return insane::physics::WW([&](double z){return this->g1p_Twist2(z,Q2);},x,1.0);
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g2n_WW(  double x, double Q2) const
    {
      return insane::physics::WW([&](double z){return this->g1n_Twist2(z,Q2);},x,1.0);
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g2d_WW(  double x, double Q2) const
    {
      return insane::physics::WW( [&](double z){return this->g1d_Twist2(z,Q2);} ,x,1.0);
    }
    //______________________________________________________________________________

    double PolarizedPDFs::g2He3_WW(double x, double Q2) const
    {
      return insane::physics::WW([&](double z){return this->g1He3_Twist2(z,Q2);},x,1.0);
    }
    //______________________________________________________________________________
    
        double PolarizedPDFs::Delta_u()    const { return m_PDFValues.fValues[0]; } 
        double PolarizedPDFs::Delta_d()    const { return m_PDFValues.fValues[1]; } 
        double PolarizedPDFs::Delta_s()    const { return m_PDFValues.fValues[2]; } 
        double PolarizedPDFs::Delta_c()    const { return m_PDFValues.fValues[3]; } 
        double PolarizedPDFs::Delta_b()    const { return m_PDFValues.fValues[4]; } 
        double PolarizedPDFs::Delta_t()    const { return m_PDFValues.fValues[5]; } 
        double PolarizedPDFs::Delta_g()    const { return m_PDFValues.fValues[6]; } 
        double PolarizedPDFs::Delta_ubar() const { return m_PDFValues.fValues[7]; } 
        double PolarizedPDFs::Delta_dbar() const { return m_PDFValues.fValues[8]; } 
        double PolarizedPDFs::Delta_sbar() const { return m_PDFValues.fValues[9]; } 
        double PolarizedPDFs::Delta_cbar() const { return m_PDFValues.fValues[10]; } 
        double PolarizedPDFs::Delta_bbar() const { return m_PDFValues.fValues[11]; } 
        double PolarizedPDFs::Delta_tbar() const { return m_PDFValues.fValues[12]; } 

        double PolarizedPDFs::Delta_u_uncertainty()    const {return m_PDFValues.fUncertainties[0]; }
        double PolarizedPDFs::Delta_d_uncertainty()    const {return m_PDFValues.fUncertainties[1]; }
        double PolarizedPDFs::Delta_s_uncertainty()    const {return m_PDFValues.fUncertainties[2]; }
        double PolarizedPDFs::Delta_c_uncertainty()    const {return m_PDFValues.fUncertainties[3]; }
        double PolarizedPDFs::Delta_b_uncertainty()    const {return m_PDFValues.fUncertainties[4]; }
        double PolarizedPDFs::Delta_t_uncertainty()    const {return m_PDFValues.fUncertainties[5]; }
        double PolarizedPDFs::Delta_g_uncertainty()    const {return m_PDFValues.fUncertainties[6]; }
        double PolarizedPDFs::Delta_ubar_uncertainty() const {return m_PDFValues.fUncertainties[7]; }
        double PolarizedPDFs::Delta_dbar_uncertainty() const {return m_PDFValues.fUncertainties[8]; }
        double PolarizedPDFs::Delta_sbar_uncertainty() const {return m_PDFValues.fUncertainties[9]; }
        double PolarizedPDFs::Delta_cbar_uncertainty() const {return m_PDFValues.fUncertainties[10];}
        double PolarizedPDFs::Delta_bbar_uncertainty() const {return m_PDFValues.fUncertainties[11];}
        double PolarizedPDFs::Delta_tbar_uncertainty() const {return m_PDFValues.fUncertainties[12];}


  }
}

