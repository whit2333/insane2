#include "insane/pdfs/DNS2005_PPDFs.h"

namespace insane {
  namespace physics {

    DNS2005_PPDFs::DNS2005_PPDFs() {
      SetNameTitle("DNS2005_PPDFs", "DNS2005 pol. PDFs");
      SetLabel("DNS2005");

      // These values are always zero.
      m_PDFValues.fValues[4]  = 0.0; // Delta b
      m_PDFValues.fValues[5]  = 0.0; // Delta t
      m_PDFValues.fValues[11] = 0.0; // Delta bbar
      m_PDFValues.fValues[12] = 0.0; // Delta tbar

      for (Int_t i = 0; i < 12; i++)
        m_PDFValues.fUncertainties[i] = 0.;
      //fiSet = 3;
      ini_();
    }

    DNS2005_PPDFs::~DNS2005_PPDFs() {}

    const std::array<double, NPartons>& DNS2005_PPDFs::Calculate(double x, double Q2) const {

      double fPDFValues[12];
      m_PDFValues.fx  = x;
      m_PDFValues.fQ2 = Q2;
      //      double qbar=0.0;
      //      double Dqbar=0.0;
      
      int fiSet = 3;
      double fg1p, fDg1p, fg1n, fDg1n;
      polfit_(&fiSet, &m_PDFValues.fx, &m_PDFValues.fQ2,
              /*DUV=*/&fPDFValues[0], /*DDV=*/&fPDFValues[1],
              /*DUBAR=*/&fPDFValues[7], /*DDBAR=*/&fPDFValues[8],
              /*DSTR=*/&fPDFValues[2], /*DGLU=*/&fPDFValues[6], &fg1p, &fg1n);

      m_PDFValues.fValues[0]  = fPDFValues[0] / x; // Delta up
      m_PDFValues.fValues[1]  = fPDFValues[1] / x; // Delta down
      m_PDFValues.fValues[2]  = fPDFValues[2] / x; // Delta s
      m_PDFValues.fValues[3]  = 0.0;               // Delta c
      m_PDFValues.fValues[4]  = 0.0;               // Delta b
      m_PDFValues.fValues[5]  = 0.0;               // Delta t
      m_PDFValues.fValues[6]  = fPDFValues[6] / x; // Delta g
      m_PDFValues.fValues[7]  = fPDFValues[7] / x; // Delta ubar
      m_PDFValues.fValues[8]  = fPDFValues[8] / x; // Delta dbar
      m_PDFValues.fValues[9]  = 0.0;               // Delta sbar
      m_PDFValues.fValues[10] = 0.0;               // Delta cbar
      m_PDFValues.fValues[11] = 0.0;               // Delta bbar
      m_PDFValues.fValues[12] = 0.0;               // Delta tbar
      return (m_PDFValues.fValues);
    }
    //______________________________________________________________________________

    const std::array<double, NPartons>& DNS2005_PPDFs::Uncertainties(double x, double Q2) const {

      m_PDFValues.fUncertainties[0]  = 0.0;
      m_PDFValues.fUncertainties[1]  = 0.0;
      m_PDFValues.fUncertainties[2]  = 0.0;
      m_PDFValues.fUncertainties[3]  = 0.0;
      m_PDFValues.fUncertainties[6]  = 0.0;
      m_PDFValues.fUncertainties[7]  = 0.0;
      m_PDFValues.fUncertainties[8]  = 0.0;
      m_PDFValues.fUncertainties[9]  = 0.0;
      m_PDFValues.fUncertainties[10] = 0.0;

      return (m_PDFValues.fUncertainties);
    }
    //______________________________________________________________________________
  } // namespace physics
} // namespace insane
