#include "insane/pdfs/Twist3DistributionFunctions.h" 
#include "insane/kinematics/KinematicFunctions.h"

namespace insane {
  namespace physics {

    Twist3DistributionFunctions::Twist3DistributionFunctions()
    { }
    //______________________________________________________________________________

    Twist3DistributionFunctions::~Twist3DistributionFunctions()
    { }
    //______________________________________________________________________________

    double Twist3DistributionFunctions::g1(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return g1p_Twist3(x, Q2);
          break;
        case Nuclei::n : 
          return g1n_Twist3(x, Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double Twist3DistributionFunctions::g2(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return g2p_Twist3(x, Q2);
          break;
        case Nuclei::n : 
          return g2n_Twist3(x, Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double Twist3DistributionFunctions::g1_TMC(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return g1p_Twist3_TMC(x, Q2);
          break;
        case Nuclei::n : 
          return g1n_Twist3_TMC(x, Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double Twist3DistributionFunctions::g2_TMC(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return g2p_Twist3_TMC(x, Q2);
          break;
        case Nuclei::n : 
          return g2n_Twist3_TMC(x, Q2);
          break;
        default :
          return 0.0;
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double Twist3DistributionFunctions::g2p_Twist3(double x, double Q2) const
    { 
      double result          = Dp_Twist3(x, Q2);
      double integral_result = insane::integrate::simple(
          [&](double z){
          return( Dp_Twist3(z,Q2)/z );
          }, x,1.0,100);
      return( result - integral_result );
    }
    //______________________________________________________________________________
    
    double Twist3DistributionFunctions::g1p_Twist3_TMC(double x, double Q2) const
    {
      // Twist three part of g1p with full TMC treatment
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = (rho*rho-1.0)/(rho*rho*rho)*Dp_Twist3(xi,Q2);
      double t1     = (1.0-rho*rho)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::simple(
          [&](double z){
          return( ((3.0-(3.0-rho*rho)*TMath::Log(z/xi)/rho)/z)*Dp_Twist3(z,Q2) );
          }, xi,xi_thresh,100);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________
    
    double Twist3DistributionFunctions::g2p_Twist3_TMC(double x, double Q2) const
    {
      //std::cout << "  Twist3DistributionFunctions::g2p_Twist3_TMC ...\n";
      // Twist three part of g1p with full TMC treatment
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      //std::cout << " xi_thresh = " << xi_thresh << "\n";
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = 1.0/(rho*rho*rho)*Dp_Twist3(xi,Q2);
      double t1     = (-1.0)/(rho*rho*rho*rho);
      //std::cout << res << " at (" << xi << ", " << Q2 << ")\n";
      //std::cout << res << " rho, M (" << rho << ", " << M << ")\n";
      double integral_result =  insane::integrate::simple(
          [&](double z){
          return(((3.0-2.0*rho*rho+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/rho)/z)*Dp_Twist3(z,Q2) );
          }, xi,xi_thresh,100);
      //std::cout << " res = " << res + t1*integral_result << "\n";
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double Twist3DistributionFunctions::g2n_Twist3(double x, double Q2) const
    { 
      double result          = Dn_Twist3(x, Q2);
      double integral_result = insane::integrate::simple(
          [&](double z){
          return( Dn_Twist3(z,Q2)/z );
          }, x,1.0);
      return( result - integral_result );
    }
    //______________________________________________________________________________
    
    double Twist3DistributionFunctions::g1n_Twist3_TMC(double x, double Q2) const
    {
      // Twist three part of g1n with full TMC treatment
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = (rho*rho-1.0)/(rho*rho*rho)*Dn_Twist3(xi,Q2);
      double t1     = (1.0-rho*rho)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::simple(
          [&](double z){
          return( ((3.0-(3.0-rho*rho)*TMath::Log(z/xi)/rho)/z)*Dn_Twist3(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double Twist3DistributionFunctions::g2n_Twist3_TMC(double x, double Q2) const
    {
      // Twist three part of g1n with full TMC treatment
      double xi        = insane::kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = 1.0/(rho*rho*rho)*Dn_Twist3(xi,Q2);
      double t1     = (-1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::simple(
          [&](double z){
          return(((3.0-2.0*rho*rho+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/rho)/z)*Dn_Twist3(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double Twist3DistributionFunctions::Dp_Twist3(double x, double Q2) const
    { 
      //std::cout << "Twist3DistributionFunctions::Dp_Twist3\n";
      double result = 0.0;
      Calculate(x, Q2);
      result += (4.0 / 9.0) * Get(Parton::u);
      result += (1.0 / 9.0) * Get(Parton::d);
      //result += 0.5 * (1.0 / 9.0) * Deltas();
      //result += 0.5 * (4.0 / 9.0) * Deltaubar();
      //result += 0.5 * (1.0 / 9.0) * Deltadbar();
      //result += 0.5 * (1.0 / 9.0) * Deltasbar();
      //std::cout << "Dp_Twist3 " <<  result << '\n';
      return(result);
    }
    //______________________________________________________________________________

    double Twist3DistributionFunctions::Dn_Twist3(double x, double Q2) const
    { 
      double result = 0.0;
      Calculate(x, Q2);
      result += (1.0 / 9.0) * Get(Parton::u);
      result += (4.0 / 9.0) * Get(Parton::d);
      //result += 0.5 * (1.0 / 9.0) * Deltas();
      //result += 0.5 * (4.0 / 9.0) * Deltaubar();
      //result += 0.5 * (1.0 / 9.0) * Deltadbar();
      //result += 0.5 * (1.0 / 9.0) * Deltasbar();
      //std::cout << "Dp_Twist3 " <<  result << '\n';
      return(result);
    }
    //______________________________________________________________________________

    // Twist 3 quark distribution functions
    double Twist3DistributionFunctions::D_u(   double x, double Q2) const
    {return Get(Parton::u,x,Q2);}
    //______________________________________________________________________________

    double Twist3DistributionFunctions::D_d(   double x, double Q2) const
    {return Get(Parton::d,x,Q2);}
    //______________________________________________________________________________

    double Twist3DistributionFunctions::D_s(   double x, double Q2) const
    {return Get(Parton::s,x,Q2);}
    //______________________________________________________________________________

    double Twist3DistributionFunctions::D_ubar(double x, double Q2) const
    {return Get(Parton::ubar,x,Q2);}
    //______________________________________________________________________________

    double Twist3DistributionFunctions::D_dbar(double x, double Q2) const
    {return Get(Parton::dbar,x,Q2);}
    //______________________________________________________________________________

    double Twist3DistributionFunctions::D_sbar(double x, double Q2) const
    {return Get(Parton::sbar,x,Q2);}
    //______________________________________________________________________________

    double Twist3DistributionFunctions::d2p(double Q2,double x1,double x2)
    {
      // d2 tilde (no elastic contribution) 
      //double x1     = 0.1;
      //double x2     = 0.9;
      double result = 0.0;
      Int_t N         = 100;
      double dx     = (x2 - x1) / ((double)N);
      // quick simple integration
      for (int i = 0; i < N; i++) {
        double x = x1 + dx*double(i);
        result += (dx*(x*x)*( 3.0*g2p_Twist3(x,Q2) ));
      }
      return(result);
    }
    //______________________________________________________________________________

    double Twist3DistributionFunctions::d2p_TMC(double Q2,double x1,double x2)
    {
      // d2 tilde (no elastic contribution) 
      //double x1     = 0.1;
      //double x2     = 0.9;
      double result = 0.0;
      Int_t N         = 100;
      double dx     = (x2 - x1) / ((double)N);
      // quick simple integration
      for (int i = 0; i < N; i++) {
        double x = x1 + dx*double(i);
        result += (dx*(x*x)*( 3.0*g2p_Twist3_TMC(x,Q2) ));
      }
      return(result);
    }
    //_____________________________________________________________________________

  }
}

