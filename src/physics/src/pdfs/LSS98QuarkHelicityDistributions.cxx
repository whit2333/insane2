#include "insane/pdfs/LSS98QuarkHelicityDistributions.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
LSS98QuarkHelicityDistributions::LSS98QuarkHelicityDistributions(){
  
   // FIXME: 1. Is there any Q2 dependence? (There doesn't seem to be; I think it's Q2 = 4 GeV^2)
   //        2. What about anti-quarks?
 
   /// coefficients from Eq 40  
   fAu      =  3.088;              //  3.784;
   fAd      =  0.343;              //  0.757;
   fBu      = -3.010; // +/- 0.156 // -3.672; 
   fBd      = -0.265;              // -0.645; 
   fCu      =  2.143; // +/- 0.137 //  2.004; 
   fCd      =  1.689; // +/- 0.227 //  3.230; 
   fDu      = -2.065; // +/- 0.148 // -1.892;
   fDd      = -1.610;              // -3.118; 
   fCs      =  0.334; // +/- 0.044 //  1.0;        
   fAs      =  0.001;              // -0.6980 + 0.9877*fCs; 
   fBs      =  0.041;              //  0.8534 - 1.1171*fCs;  
   fDs      = -0.292; // +/- 0.042 //  0.1551 - 1.1294*fCs;
   fAlpha   =  1.313; // +/- 0.056 //  1.12;     /// QCD Pomeron intercept  
   fAlpha_g =  1.233; // +/- 0.073 //  1.0;  
   fAg      =  1.019;              //  0.2381;                   
   fBg      = -0.339; // +/- 0.056 //  1.1739; 

}
//______________________________________________________________________________
LSS98QuarkHelicityDistributions::~LSS98QuarkHelicityDistributions(){

}
//______________________________________________________________________________
double LSS98QuarkHelicityDistributions::uPlus(double x){
  double arg = fAu*TMath::Power(1.-x,3.) + fBu*TMath::Power(1.-x,4.); 
  double res = arg/( TMath::Power(x,fAlpha) ); 
  return res; 
}
//______________________________________________________________________________
double LSS98QuarkHelicityDistributions::uMinus(double x){
  double arg = fCu*TMath::Power(1.-x,5.) + fDu*TMath::Power(1.-x,6.); 
  double res = arg/( TMath::Power(x,fAlpha) ); 
  return res; 
}
//______________________________________________________________________________
double LSS98QuarkHelicityDistributions::dPlus(double x){
  double arg = fAd*TMath::Power(1.-x,3.) + fBd*TMath::Power(1.-x,4.); 
  double res = arg/( TMath::Power(x,fAlpha) ); 
  return res; 
}
//______________________________________________________________________________
double LSS98QuarkHelicityDistributions::dMinus(double x){
  double arg = fCd*TMath::Power(1.-x,5.) + fDd*TMath::Power(1.-x,6.); 
  double res = arg/( TMath::Power(x,fAlpha) ); 
  return res; 
}
//______________________________________________________________________________
double LSS98QuarkHelicityDistributions::sPlus(double x){
  double arg = fAs*TMath::Power(1.-x,5.) + fBs*TMath::Power(1.-x,6.); 
  double res = arg/( TMath::Power(x,fAlpha) ); 
  return res; 
}
//______________________________________________________________________________
double LSS98QuarkHelicityDistributions::sMinus(double x){
  double arg = fCs*TMath::Power(1.-x,7.) + fDs*TMath::Power(1.-x,8.); 
  double res = arg/( TMath::Power(x,fAlpha) ); 
  return res; 
}
//______________________________________________________________________________
double LSS98QuarkHelicityDistributions::gPlus(double x){
  double arg = fAg*TMath::Power(1.-x,4.) + fBg*TMath::Power(1.-x,5.); 
  double res = arg/( TMath::Power(x,fAlpha_g) ); 
  return res; 
}
//______________________________________________________________________________
double LSS98QuarkHelicityDistributions::gMinus(double x){
  double arg = fAg*TMath::Power(1.-x,6.) + fBg*TMath::Power(1.-x,7.); 
  double res = arg/( TMath::Power(x,fAlpha_g) ); 
  return res; 
}
}}
