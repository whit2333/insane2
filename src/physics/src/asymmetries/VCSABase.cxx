#include "insane/asymmetries/VCSABase.h"

//namespace insane {
//  namespace physics {
//
//    VCSABase::VCSABase()
//    { }
//    //______________________________________________________________________________
//
//    VCSABase::~VCSABase()
//    { }
//    //______________________________________________________________________________
//
//    double VCSABase::GetXBjorken(std::tuple<SF,Nuclei> sf) const
//    {
//      auto f  = static_cast<int>(std::get<SF>(sf));
//      auto n  = static_cast<int>(std::get<Nuclei>(sf));
//      return(fx_values.at(n).at(f));
//    }
//    //______________________________________________________________________________
//
//    double VCSABase::GetQSquared(std::tuple<SF,Nuclei> sf) const
//    {
//      auto f  = static_cast<int>(std::get<SF>(sf));
//      auto n  = static_cast<int>(std::get<Nuclei>(sf));
//      return(fQ2_values.at(n).at(f));
//    }
//    //______________________________________________________________________________
//
//    bool   VCSABase::IsComputed( std::tuple<SF,Nuclei> sf, double x, double Q2) const
//    {
//      auto f  = static_cast<int>(std::get<SF>(sf));
//      auto n  = static_cast<int>(std::get<Nuclei>(sf));
//      if(  fx_values.at(n).at(f) != x ) return false;
//      if( fQ2_values.at(n).at(f) != Q2 ) return false;
//      return true;
//    }
//    //______________________________________________________________________________
//
//    void VCSABase::Reset()
//    {
//      for(int n = 0; n<NNuclei;  n++) {
//        for(int i = 0; i<NStructureFunctions ; i++) {
//          fx_values[n][i]        = 0.0;
//          fQ2_values[n][i]       = 0.0;
//          fValues[n][i]          = 0.0;
//          fUncertainties[n][i]   = 0.0;
//        }
//      }
//    }
//    //______________________________________________________________________________
//
//    double  VCSABase::Get(std::tuple<SF,Nuclei> sf, double x, double Q2) const 
//    {
//      return 0.0;
//    }
//    //______________________________________________________________________________
//
//    double  VCSABase::Get(std::tuple<SF,Nuclei> sf) const {
//      return 0.0;
//    }
//    //______________________________________________________________________________
//
//  }
//}
