#include "insane/asymmetries/AsymmetriesFromStructureFunctions.h"

namespace insane {
  namespace physics {

    AsymmetriesFromStructureFunctions::AsymmetriesFromStructureFunctions()
    {
      fUnpolSFs = nullptr;
      fPolSFs = nullptr;
      SetUnpolarizedSFs(new DefaultStructureFunctions()); 
      SetPolarizedSFs(new DefaultPolarizedStructureFunctions()); 

    }
    //______________________________________________________________________________
    AsymmetriesFromStructureFunctions::~AsymmetriesFromStructureFunctions(){
      // Leave these for the function manager to delete
      //delete fUnpolSFs; 
      //delete fPolSFs; 
    }
    //______________________________________________________________________________
    Double_t AsymmetriesFromStructureFunctions::A1(Double_t x,Double_t Q2){

      Double_t g1,g2,F1,F2,M; 

      SetValues(x,Q2,g1,g2,F1,F2,M);

      Double_t GAM  = 2.*M*x/TMath::Sqrt(Q2); 
      Double_t GAM2 = GAM*GAM; 
      Double_t num  = g1 - GAM2*g2; 
      Double_t den  = F1; 
      Double_t ans=0;

      if(TMath::Abs(den)>0){
        ans = num/den;  
      }else{
        ans = 0;
      }

      return ans; 

    }
    //______________________________________________________________________________
    Double_t AsymmetriesFromStructureFunctions::A2(Double_t x,Double_t Q2){

      Double_t g1,g2,M,F1,F2; 

      SetValues(x,Q2,g1,g2,F1,F2,M); 

      Double_t GAM  = 2.*M*x/TMath::Sqrt(Q2); 
      Double_t num  = g1 + g2; 
      Double_t den  = F1; 
      Double_t ans=0; 

      if(TMath::Abs(den)>0){
        ans = GAM*(num/den);  
      }else{
        ans = 0;
      }

      return ans; 

    }
    //______________________________________________________________________________
    void AsymmetriesFromStructureFunctions::SetValues(Double_t x,Double_t Q2,
                                                      Double_t &g1,Double_t &g2,Double_t &F1,Double_t &F2,Double_t &M){

      Nucleus::NucleusType Target = fTargetNucleus.GetType(); 

      switch(Target){
        case Nucleus::kProton:
          g1 = fPolSFs->g1p(x,Q2);
          g2 = fPolSFs->g2p(x,Q2);
          F1 = fUnpolSFs->F1p(x,Q2); 
          F2 = fUnpolSFs->F2p(x,Q2); 
          M  = M_p/GeV;
          break;
        case Nucleus::kNeutron:
          g1 = fPolSFs->g1n(x,Q2);
          g2 = fPolSFs->g2n(x,Q2);
          F1 = fUnpolSFs->F1n(x,Q2); 
          F2 = fUnpolSFs->F2n(x,Q2); 
          M  = M_n/GeV;
          break;
        case Nucleus::kDeuteron:
          g1 = fPolSFs->g1d(x,Q2);
          g2 = fPolSFs->g2d(x,Q2);
          F1 = fUnpolSFs->F1d(x,Q2); 
          F2 = fUnpolSFs->F2d(x,Q2); 
          M  = M_p/GeV;   // WARNING: For resonance & DIS kinematics!
          break;
        case Nucleus::k3He: 
          g1 = fPolSFs->g1He3(x,Q2);
          g2 = fPolSFs->g2He3(x,Q2);
          F1 = fUnpolSFs->F1He3(x,Q2); 
          F2 = fUnpolSFs->F2He3(x,Q2); 
          M  = M_p/GeV;  // WARNING: For resonance & DIS kinematics! 
          break;
        default:
          std::cout << "[AsymmetriesFromStructureFunctions::SetValues]: Invalid target!  Exiting..." << std::endl;
          exit(1); 
      }

    }
    //______________________________________________________________________________
    Double_t AsymmetriesFromStructureFunctions::A_Measured(Double_t targ_angle , Double_t E_beam ,Double_t E_prime, Double_t theta, Double_t phie){
      return (0);
    }
    //______________________________________________________________________________
    Double_t AsymmetriesFromStructureFunctions::AMeasured(Double_t targ_angle,Double_t x ,Double_t Q2, Double_t y, Double_t phie){

      Double_t g1,g2,M,F1,F2; 

      SetValues(x,Q2,g1,g2,F1,F2,M); 

      using namespace TMath;
      Double_t E0 = insane::Kine::BeamEnergy_xQ2y(x,Q2,y);
      Double_t Ep = insane::Kine::Eprime_xQ2y(x,Q2,y);
      Double_t theta = insane::Kine::Theta_xQ2y(x,Q2,y);
      Double_t nu = E0-Ep;

      Double_t res = (2.0*Q2*(Cos(targ_angle)*(E0*(g2*Q2 - 2.0*E0*g2*M*x + g1*M*x*nu) + M*x*(E0 - nu)*(2.0*E0*g2 + g1*nu)*Cos(theta)) + 
                              M*x*(E0 - nu)*(2.0*E0*g2 + g1*nu)*Cos(phie)*Sin(targ_angle)*Sin(theta)))/(M*nu*(2.0*E0*F2*Q2 - 4.0*Power(E0,2.0)*F2*M*x + Q2*x*(F2*M - 2.0*F1*nu)));
      if( res != res ) res = 0;

      /*    std::cout << "(x,Q2,y) = (" << x << ","<< Q2 << "," << y << ") , A = " << res << "\n";*/
      return(res);
    }
    //______________________________________________________________________________
    Double_t AsymmetriesFromStructureFunctions::AMeasuredFixedBeamEnergy(Double_t targ_angle ,Double_t x,Double_t Q2,Double_t E0,Double_t phie) {
      Double_t M = M_p/GeV;
      Double_t y = (Q2/(2.*M*x))/E0;
      return(AMeasured(targ_angle,x,Q2,y,phie) );
    }
    //______________________________________________________________________________
  }}
