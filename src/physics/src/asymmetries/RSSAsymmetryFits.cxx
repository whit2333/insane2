#include "insane/asymmetries/RSSAsymmetryFits.h"

namespace insane {
   namespace physics {

      double RSSAsymmetryFits::kappa_i(int i, double Q2) const
      {
         double term1 = (w_rss.at(i)*w_rss.at(i) + M*M + Q2)/(2.0*w_rss.at(i)) ;
         double res   = TMath::Sqrt( term1*term1 - M*M );
         return res;
      }
      //______________________________________________________________________________

      double RSSAsymmetryFits::q_i(int i, double Q2) const
      {
         double term1 = (w_rss.at(i)*w_rss.at(i) + M*M - m_pi*m_pi)/(2.0*w_rss.at(i)) ;
         double res   = TMath::Sqrt( term1*term1 - M*M );
         return res;
      }
      //______________________________________________________________________________
      
      double RSSAsymmetryFits::kappa_cm(double Q2, double W) const
      {
         double term1 = (W*W + M*M + Q2)/(2.0*W) ;
         double res   = TMath::Sqrt( term1*term1 - M*M );
         return res;
      }
      //______________________________________________________________________________
      
      double RSSAsymmetryFits::q_cm(double W) const
      {
         double term1 = (W*W + M*M - m_pi*m_pi)/(2.0*W) ;
         double res   = TMath::Sqrt( term1*term1 - M*M );
         if( std::isnan(res) ) {
            return 0.0;
         }
         return res;
      }
      //______________________________________________________________________________
      double RSSAsymmetryFits::kappa2_i(int i, double Q2) const
      {
         double term1 = (w2_rss.at(i)*w2_rss.at(i) + M*M + Q2)/(2.0*w2_rss.at(i)) ;
         double res   = TMath::Sqrt( term1*term1 - M*M );
         return res;
      }
      //______________________________________________________________________________

      double RSSAsymmetryFits::q2_i(int i, double Q2) const
      {
         double term1 = (w2_rss.at(i)*w2_rss.at(i) + M*M - m_pi*m_pi)/(2.0*w2_rss.at(i)) ;
         double res   = TMath::Sqrt( term1*term1 - M*M );
         return res;
      }
      //______________________________________________________________________________
      
      double RSSAsymmetryFits::Gamma_i(int i, double Q2, double W) const
      {
         double arg   = q_cm(W)/q_i(i,Q2);
         double term1 = g_rss.at(i)*TMath::Power( arg , 2.0*L_res.at(i)+1.0) ;
         double num2  = q_i(i,Q2)*q_i(i,Q2) + X_res.at(i)*X_res.at(i);
         double den2  = q_cm(W)*q_i(i,W)    + X_res.at(i)*X_res.at(i);
         double term2 = TMath::Power( num2/den2 , L_res.at(i));
         return term1*term2;
      }
      //______________________________________________________________________________
      
      double RSSAsymmetryFits::Gamma_gamma_i(int i, double Q2, double W) const
      {
         double term1 = g_rss.at(i)*TMath::Power( kappa_cm(Q2,W)/kappa_i(i,Q2) , 2.0*J_res.at(i)) ;
         double num2  = kappa_i(i,Q2)*kappa_i(i,Q2)   + X_res.at(i)*X_res.at(i);
         double den2  = kappa_cm(Q2,W)*kappa_cm(Q2,W) + X_res.at(i)*X_res.at(i);
         double term2 = TMath::Power( num2/den2 , J_res.at(i));
         return term1*term2;
      }
      //______________________________________________________________________________
      
      double RSSAsymmetryFits::Gamma2_i(int i, double Q2, double W) const
      {
         double arg   = q_cm(W)/q_i(i,Q2);
         double term1 = g2_rss.at(i)*TMath::Power( arg , 2.0*L_res.at(i)+1.0) ;
         double num2  = q_i(i,Q2)*q_i(i,Q2) + X_res.at(i)*X_res.at(i);
         double den2  = q_cm(W)*q_i(i,W)    + X_res.at(i)*X_res.at(i);
         double term2 = TMath::Power( num2/den2 , L_res.at(i));
         return term1*term2;
      }
      //______________________________________________________________________________
      
      double RSSAsymmetryFits::Gamma2_gamma_i(int i, double Q2, double W) const
      {
         double term1 = g2_rss.at(i)*TMath::Power( kappa_cm(Q2,W)/kappa_i(i,Q2) , 2.0*J_res.at(i)) ;
         double num2  = kappa_i(i,Q2)*kappa_i(i,Q2)   + X_res.at(i)*X_res.at(i);
         double den2  = kappa_cm(Q2,W)*kappa_cm(Q2,W) + X_res.at(i)*X_res.at(i);
         double term2 = TMath::Power( num2/den2 , J_res.at(i));
         return term1*term2;
      }
      //______________________________________________________________________________

      double RSSAsymmetryFits::BW(int i, double Q2, double W) const
      {
        using namespace TMath;
        double  kapi     = kappa_i(i,Q2);
        double  kapcm    = kappa_cm(Q2,W);
        double  Q2_dep   = (Q2 < 1.3 ? 1.3 : Q2);
        double  ai       = a_rss.at(i);//*Exp(q2_dep.at(i)*Q2_dep)/(Exp(q2_dep.at(i)*1.3));
        double  wi       = w_rss.at(i);
        double  Gami     = Gamma_i(i,Q2,W);
        double  Gamgami  = Gamma_gamma_i(i,Q2,W);
        double  delta_W2 = wi*wi - W*W;
        double  res      = ai*kapi*kapi*wi*wi*Gami*Gamgami/(kapcm*kapcm*(delta_W2*delta_W2 + wi*wi*Gami*Gami));
        return res;
      }
      //______________________________________________________________________________

      double RSSAsymmetryFits::BW2(int i, double Q2, double W) const
      {
        using namespace TMath;
         double  kapi     = kappa_i(i,Q2);
         double  kapcm    = kappa_cm(Q2,W);
        double  Q2_dep   = (Q2 < 1.3 ? 1.3 : Q2);
         double  ai       = a2_rss.at(i);//*Exp(q2_dep.at(i)*Q2_dep)/(Exp(q2_dep.at(i)*1.3));
         double  wi       = w2_rss.at(i);
         double  Gami     = Gamma_i(i,Q2,W);
         double  Gamgami  = Gamma_gamma_i(i,Q2,W);
         double  delta_W2 = wi*wi - W*W;
         double  res      = ai*kapi*kapi*wi*wi*Gami*Gamgami/(kapcm*kapcm*(delta_W2*delta_W2 + wi*wi*Gami*Gami));
         return res;
      }
      //______________________________________________________________________________

      double RSSAsymmetryFits::A1_fit(double* x, double* pars) const
      {
         // function of W
         double W   = x[0];
         double Q2  = pars[0];
         double xbj = Q2/(W*W-M*M+Q2);
         //if( xbj > 0.9 ){
         //   return 0.0;
         //}
         double A1_res = 0.0;
         for(int i = 0;i<4;i++){
            double bw = BW(i,Q2,W);
            A1_res += bw;
         }
         double alpha = A1_alpha;
         double DIS_term = 0.0;
         //for(int i = 0;i<3;i++){
         //   DIS_term += A1_beta.at(i)*TMath::Power(xbj,i);
         //}
         //DIS_term *= TMath::Power(xbj,alpha);
         return A1_res + DIS_term;
      }
      //______________________________________________________________________________

      double RSSAsymmetryFits::A2_fit(double* x, double* pars) const
      {
         // function of W
         double W   = x[0];
         double Q2  = pars[0];
         double xbj = Q2/(W*W-M*M+Q2);
         //if( xbj > 0.9 ){
         //   return 0.0;
         //}
         double A2_res = 0.0;
         for(int i = 0;i<4;i++){
            double bw = BW2(i,Q2,W);
            A2_res += bw;
         }
         double alpha = A2_alpha;
         double DIS_term = 0.0;
         //for(int i = 0;i<3;i++){
         //   DIS_term += A2_beta.at(i)*TMath::Power(xbj,i);
         //}
         //DIS_term *= TMath::Power(xbj,alpha);
         return A2_res + DIS_term;
      }
      //______________________________________________________________________________

      double RSSAsymmetryFits::A1_fit_x(double* x, double* pars) const
      {
         // function of x
         double xbj = x[0];
         double Q2  = pars[0];
         double W   = TMath::Sqrt(M*M - Q2 + Q2/xbj);
         //if(W < 0.95 ) return 0.0;
         double yvars[2] = { W, 0.0 };
         return A1_fit(yvars,pars);
      }
      //______________________________________________________________________________
      
      double RSSAsymmetryFits::A1_x(double x, double Q2) const
      {
         // function of x
         double xbj = x; 
         double W   = TMath::Sqrt(M*M - Q2 + Q2/xbj);
         //if(W < 0.95 ) return 0.0;
         double yvars[2] = { W, 0.0 };
         double pars[1] = { Q2 };
         double res =  A1_fit(yvars,pars);
         if( std::isnan(res) ) {
            return 0.0;
         }
         return res;
      }
      //______________________________________________________________________________
      
      double RSSAsymmetryFits::A1_W(double W, double Q2) const
      {
         // function of x
         //double xbj = x; 
         //double W   = TMath::Sqrt(M*M - Q2 + Q2/xbj);
         //if(W < 0.95 ) return 0.0;
         double yvars[2] = { W, 0.0 };
         double pars[1] = { Q2 };
         return A1_fit(yvars,pars);
      }
      //______________________________________________________________________________

      double RSSAsymmetryFits::A2_fit_x(double* x, double* pars) const
      {
         // function of x
         double xbj = x[0];
         double Q2  = pars[0];
         double W   = TMath::Sqrt(M*M - Q2 + Q2/xbj);
         //if(W < 0.95 ) return 0.0;
         double yvars[2] = { W, 0.0 };
         return A2_fit(yvars,pars);
      }
      //______________________________________________________________________________
      
      double RSSAsymmetryFits::A2_x(double x, double Q2) const
      {
         // function of x
         double xbj = x; 
         double W   = TMath::Sqrt(M*M - Q2 + Q2/xbj);
         //if(W < 0.95 ) return 0.0;
         double yvars[2] = { W, 0.0 };
         double pars[1] = { Q2 };
         double res =  A2_fit(yvars,pars);
         if( std::isnan(res) ) {
            return 0.0;
         }
         return res;
      }
      //______________________________________________________________________________
      
      double RSSAsymmetryFits::A2_W(double W, double Q2) const
      {
         // function of x
         //double xbj = x; 
         //double W   = TMath::Sqrt(M*M - Q2 + Q2/xbj);
         //if(W < 0.95 ) return 0.0;
         double yvars[2] = { W, 0.0 };
         double pars[1] = { Q2 };
         return A2_fit(yvars,pars);
      }

   }
}

