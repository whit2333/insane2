#include "insane/asymmetries/VirtualComptonAsymmetries.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "TH1F.h"
#include "insane/structurefunctions/StructureFunctions.h"
#include "insane/structurefunctions/SpinStructureFunctions.h"

namespace insane {
  namespace physics {
    VirtualComptonAsymmetries::VirtualComptonAsymmetries()
    {
      fSFs    = nullptr;//new DefaultStructureFunctions();
      fPolSFs = nullptr;//new DefaultPolarizedStructureFunctions();
    }
    //______________________________________________________________________________

    VirtualComptonAsymmetries::VirtualComptonAsymmetries(
      StructureFunctions* sf, SpinStructureFunctions* ssf) :
      fSFs(sf), fPolSFs(ssf)
    { }
    //______________________________________________________________________________

    VirtualComptonAsymmetries::~VirtualComptonAsymmetries()
    { }
    //______________________________________________________________________________

    Double_t VirtualComptonAsymmetries::A1p(Double_t x, Double_t Q2)
    {
      Double_t g1 = fPolSFs->g1p(x,Q2);
      Double_t g2 = fPolSFs->g2p(x,Q2);
      Double_t F1 = fSFs->F1p(x,Q2); 
      //Double_t F2 = fSFs->F2p(x,Q2); 
      Double_t M  = M_p/GeV;

      Double_t GAM  = 2.*M*x/TMath::Sqrt(Q2); 
      Double_t GAM2 = GAM*GAM; 
      Double_t num  = g1 - GAM2*g2; 
      //Double_t den  = F2/(2.0*x); 
      Double_t den  = F1;
      Double_t ans  = 0;
      if(TMath::Abs(den)>0){
        ans = num/den;  
      }else{
        ans = 0;
      }
      return(ans);
    } 
    //______________________________________________________________________________

    Double_t VirtualComptonAsymmetries::A2p(Double_t x, Double_t Q2){ 

      Double_t g1 = fPolSFs->g1p(x,Q2);
      Double_t g2 = fPolSFs->g2p(x,Q2);
      Double_t F1 = fSFs->F1p(x,Q2); 
      Double_t F2 = fSFs->F2p(x,Q2); 
      Double_t M  = M_p/GeV;

      Double_t GAM  = 2.*M*x/TMath::Sqrt(Q2); 
      Double_t num  = GAM*(g1 + g2); 
      Double_t den  = F1; 
      Double_t ans=0; 
      if(TMath::Abs(den)>0){
        ans = num/den;  
      }else{
        ans = 0;
      }
      return(ans);
    } 
    //______________________________________________________________________________

    Double_t VirtualComptonAsymmetries::A1p_TMC(Double_t x, Double_t Q2)
    {
      Double_t g1 = fPolSFs->g1p_TMC(x,Q2);
      Double_t g2 = fPolSFs->g2p_TMC(x,Q2);
      Double_t F1 = fSFs->F1p(x,Q2); 
      //Double_t F2 = fSFs->F2p(x,Q2); 
      Double_t M  = M_p/GeV;

      Double_t GAM  = 2.*M*x/TMath::Sqrt(Q2); 
      Double_t GAM2 = GAM*GAM; 
      Double_t num  = g1 - GAM2*g2; 
      //Double_t den  = F2/(2.0*x); 
      Double_t den  = F1;
      Double_t ans  = 0;
      if(TMath::Abs(den)>0){
        ans = num/den;  
      }else{
        ans = 0;
      }
      return(ans);
    } 
    //______________________________________________________________________________

    Double_t VirtualComptonAsymmetries::A2p_TMC(Double_t x, Double_t Q2){ 

      Double_t g1 = fPolSFs->g1p_TMC(x,Q2);
      Double_t g2 = fPolSFs->g2p_TMC(x,Q2);
      Double_t F1 = fSFs->F1p(x,Q2); 
      Double_t F2 = fSFs->F2p(x,Q2); 
      Double_t M  = M_p/GeV;

      Double_t GAM  = 2.*M*x/TMath::Sqrt(Q2); 
      Double_t num  = GAM*(g1 + g2); 
      Double_t den  = F1; 
      Double_t ans=0; 
      if(TMath::Abs(den)>0){
        ans = num/den;  
      }else{
        ans = 0;
      }
      return(ans);
    } 
    //______________________________________________________________________________

    Double_t VirtualComptonAsymmetries::A1n(Double_t x, Double_t Q2){ 
      Double_t g1 = fPolSFs->g1n(x,Q2);
      Double_t g2 = fPolSFs->g2n(x,Q2);
      Double_t F1 = fSFs->F1n(x,Q2); 
      Double_t F2 = fSFs->F2n(x,Q2); 
      Double_t M  = M_p/GeV;

      Double_t GAM  = 2.*M*x/TMath::Sqrt(Q2); 
      Double_t GAM2 = GAM*GAM; 
      Double_t num  = g1 - GAM2*g2; 
      //Double_t den  = F2/(2.0*x); 
      Double_t den  = F1;
      Double_t ans  = 0;
      if(TMath::Abs(den)>0){
        ans = num/den;  
      }else{
        ans = 0;
      }
      return(ans);
    } 
    //______________________________________________________________________________
    Double_t VirtualComptonAsymmetries::A2n(Double_t x, Double_t Q2){ 
      Double_t g1 = fPolSFs->g1n(x,Q2);
      Double_t g2 = fPolSFs->g2n(x,Q2);
      Double_t F1 = fSFs->F1n(x,Q2); 
      Double_t F2 = fSFs->F2n(x,Q2); 
      Double_t M  = M_p/GeV;

      Double_t GAM  = 2.*M*x/TMath::Sqrt(Q2); 
      Double_t num  = GAM*(g1 + g2); 
      Double_t den  = F1; 
      Double_t ans=0; 
      if(TMath::Abs(den)>0){
        ans = num/den;  
      }else{
        ans = 0;
      }
      return(ans);
    } 
    //______________________________________________________________________________
    Double_t VirtualComptonAsymmetries::A1d(Double_t x, Double_t Q2){ 
      Double_t g1 = 0.0;//fPolSFs->g1d(x,Q2);
      Double_t g2 = 0.0;//fPolSFs->g2d(x,Q2);
      Double_t F1 = fSFs->F1d(x,Q2); 
      Double_t F2 = fSFs->F2d(x,Q2); 
      Double_t M  = M_p/GeV;

      Double_t GAM  = 2.*M*x/TMath::Sqrt(Q2); 
      Double_t GAM2 = GAM*GAM; 
      Double_t num  = g1 - GAM2*g2; 
      //Double_t den  = F2/(2.0*x); 
      Double_t den  = F1;
      Double_t ans  = 0;
      if(TMath::Abs(den)>0){
        ans = num/den;  
      }else{
        ans = 0;
      }
      return(ans);
    } 
    //______________________________________________________________________________
    Double_t VirtualComptonAsymmetries::A2d(Double_t x, Double_t Q2){ 
      Double_t g1 = 0.0;//fPolSFs->g1d(x,Q2);
      Double_t g2 = 0.0;//fPolSFs->g2d(x,Q2);
      Double_t F1 = fSFs->F1d(x,Q2); 
      Double_t F2 = fSFs->F2d(x,Q2); 
      Double_t M  = M_p/GeV;

      Double_t GAM  = 2.*M*x/TMath::Sqrt(Q2); 
      Double_t num  = GAM*(g1 + g2); 
      Double_t den  = F1; 
      Double_t ans=0; 
      if(TMath::Abs(den)>0){
        ans = num/den;  
      }else{
        ans = 0;
      }
      return(ans);
    } 
    //______________________________________________________________________________
    Double_t VirtualComptonAsymmetries::A1He3(Double_t x, Double_t Q2){ 

      Double_t g1 = 0.0;//fPolSFs->g1He3(x,Q2);
      Double_t g2 = 0.0;//fPolSFs->g2He3(x,Q2);
      Double_t F1 = fSFs->F1He3(x,Q2); 
      Double_t F2 = fSFs->F2He3(x,Q2); 
      Double_t M  = M_p/GeV;

      Double_t GAM  = 2.*M*x/TMath::Sqrt(Q2); 
      Double_t GAM2 = GAM*GAM; 
      Double_t num  = g1 - GAM2*g2; 
      //Double_t den  = F2/(2.0*x); 
      Double_t den  = F1;
      Double_t ans  = 0;
      if(TMath::Abs(den)>0){
        ans = num/den;  
      }else{
        ans = 0;
      }
      return(ans);

    }
    //______________________________________________________________________________
    Double_t VirtualComptonAsymmetries::A2He3(Double_t x, Double_t Q2){ 

      Double_t g1 = 0.0;//fPolSFs->g1He3(x,Q2);
      Double_t g2 = 0.0;//fPolSFs->g2He3(x,Q2);
      Double_t F1 = fSFs->F1He3(x,Q2); 
      Double_t F2 = fSFs->F2He3(x,Q2); 
      Double_t M  = M_p/GeV;

      Double_t GAM  = 2.*M*x/TMath::Sqrt(Q2); 
      Double_t num  = GAM*(g1 + g2); 
      Double_t den  = F1; 
      Double_t ans=0; 
      if(TMath::Abs(den)>0){
        ans = num/den;  
      }else{
        ans = 0;
      }
      return(ans);
    }
    //______________________________________________________________________________
    Double_t VirtualComptonAsymmetries::A1p_Error(   Double_t x, Double_t Q2){ return 0.0; }
    Double_t VirtualComptonAsymmetries::A2p_Error(   Double_t x, Double_t Q2){ return 0.0; }
    Double_t VirtualComptonAsymmetries::A1n_Error(   Double_t x, Double_t Q2){ return 0.0; }
    Double_t VirtualComptonAsymmetries::A2n_Error(   Double_t x, Double_t Q2){ return 0.0; }
    Double_t VirtualComptonAsymmetries::A1d_Error(   Double_t x, Double_t Q2){ return 0.0; }
    Double_t VirtualComptonAsymmetries::A2d_Error(Double_t x, Double_t Q2) { return 0.0; }
    Double_t VirtualComptonAsymmetries::A1He3_Error( Double_t x, Double_t Q2){ return 0.0; }
    Double_t VirtualComptonAsymmetries::A2He3_Error( Double_t x, Double_t Q2){ return 0.0; }
    //______________________________________________________________________________
    //void VirtualComptonAsymmetries::GetValues(TObject *obj, Double_t Q2, VCSABase::AsymmetryType q,insane::kine::Variable var){
    //   // Fills histogram with values.
    //   //  For error band use GetErrorBand
    //   //if ( !(obj->InheritsFrom(TH1::Class())) ) {
    //   //   Error("GetErrorBand","Not a TH1 class");
    //   //   return;
    //   //}
    //   if(!obj) {
    //      return;
    //   }
    //   //  returns errorsband
    //   auto *hfit = (TH1*)obj;
    //   Int_t hxfirst = hfit->GetXaxis()->GetFirst();
    //   Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
    //   Int_t hyfirst = hfit->GetYaxis()->GetFirst();
    //   Int_t hylast  = hfit->GetYaxis()->GetLast(); 
    //   Int_t hzfirst = hfit->GetZaxis()->GetFirst();
    //   Int_t hzlast  = hfit->GetZaxis()->GetLast(); 
    //
    //   TAxis *xaxis  = hfit->GetXaxis();
    //   TAxis *yaxis  = hfit->GetYaxis();
    //   TAxis *zaxis  = hfit->GetZaxis();
    //
    //   Double_t x[3];
    //   double val       = 0.0;
    //   double x_bjorken = 0.0;
    //
    //   for (Int_t binz=hzfirst; binz<=hzlast; binz++){
    //      x[2]=zaxis->GetBinCenter(binz);
    //      for (Int_t biny=hyfirst; biny<=hylast; biny++) {
    //         x[1]=yaxis->GetBinCenter(biny);
    //         for (Int_t binx=hxfirst; binx<=hxlast; binx++) {
    //
    //            x[0] = xaxis->GetBinCenter(binx);
    //
    //            switch( var ) {
    //
    //               case insane::kine::kx : 
    //                  x_bjorken = x[0];
    //                  break;
    //
    //               case insane::kine::kW : 
    //                  // here x[0] is W
    //                  x_bjorken = insane::kine::xBjorken_WQsq(x[0],Q2);
    //                  break;
    //               
    //               // more....
    //
    //               default : 
    //                  x_bjorken = x[0];
    //                  
    //            }
    //
    //
    //            switch( q ) {
    //
    //               case VCSABase::kA1p : 
    //                  val = this->A1p(x_bjorken,Q2);
    //                  break;
    //
    //               case VCSABase::kA2p : 
    //                  val = this->A2p(x_bjorken,Q2);
    //                  break;
    //
    //            }
    //
    //            hfit->SetBinContent(binx, biny, binz, val );
    //
    //         }
    //      }
    //   }
    //
    //}
    ////_____________________________________________________________________________
    //void VirtualComptonAsymmetries::GetErrorBand(TObject *obj, Double_t Q2, 
    //       VCSABase::AsymmetryType q, insane::kine::Variable var) {
    //   //if ( !(obj->InheritsFrom(TH1::Class())) ) {
    //   //   Error("GetErrorBand","Not a TH1 class");
    //   //   return;
    //   //}
    //   if(!obj) {
    //      return;
    //   }
    //   //  returns errorsband
    //   auto *hfit = (TH1*)obj;
    //   Int_t hxfirst = hfit->GetXaxis()->GetFirst();
    //   Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
    //   Int_t hyfirst = hfit->GetYaxis()->GetFirst();
    //   Int_t hylast  = hfit->GetYaxis()->GetLast(); 
    //   Int_t hzfirst = hfit->GetZaxis()->GetFirst();
    //   Int_t hzlast  = hfit->GetZaxis()->GetLast(); 
    //
    //   TAxis *xaxis  = hfit->GetXaxis();
    //   TAxis *yaxis  = hfit->GetYaxis();
    //   TAxis *zaxis  = hfit->GetZaxis();
    //
    //   Double_t x[3];
    //
    //   
    //   for (Int_t binz=hzfirst; binz<=hzlast; binz++){
    //      x[2]=zaxis->GetBinCenter(binz);
    //      for (Int_t biny=hyfirst; biny<=hylast; biny++) {
    //         x[1]=yaxis->GetBinCenter(biny);
    //         for (Int_t binx=hxfirst; binx<=hxlast; binx++) {
    //            x[0]=xaxis->GetBinCenter(binx);
    //
    //            hfit->SetBinContent(binx, biny, binz, this->A1p(x[0],Q2));
    //            double err = this->A1p_Error(x[0],Q2);
    //            if( err == 0.0 )  err = 1.0e-8;
    //            hfit->SetBinError(binx, biny, binz, err);
    //         }
    //      }
    //   }
    //
    //}
    ////______________________________________________________________________________
  }
}
