#include "insane/asymmetries/MAID_VCSAs.h"
#include "insane/kinematics/KinematicFunctions.h"
#include "insane/pdfs/FortranWrappers.h"
#include <iostream> 

namespace insane {
  namespace physics {

    MAID_VCSAs::MAID_VCSAs()
    {
      fSigT    = 0.0;
      fSigL    = 0.0;
      fSigLT   = 0.0;
      fSigLTp  = 0.0;
      fSigTTp  = 0.0;
      fSigL0   = 0.0;
      fSigLT0  = 0.0;
      fSigLT0p = 0.0;
    }
    //______________________________________________________________________________
   
    MAID_VCSAs::~MAID_VCSAs()
    { }
    //______________________________________________________________________________
    
    double MAID_VCSAs::A1p(double x, double Q2) const
    {
      // Note the MAID definitions:
      // sigT=1/2(sig_1/2 + sig_3/2), sigTT'=1/2(sig_3/2 - sig_1/2)
      // sigL0=sigL*(wcm/Q)**2, sigLT0=sigLT*(wcm/Q), sigLT0'=sigLT'*(wcm/Q)

      double W = insane::kine::W_xQsq(x,Q2)*1000.0; // W in MeV
      double num = 0.0;
      double den = 0.0;
      //std::cout << "W  = " << W  << "\n";
      //std::cout << "Q2 = " << Q2 << "\n";
      //std::cout << "x  = " << x  << "\n";
      if( Q2 < 0 || Q2 > 5.0) return 0.0; 
      if( W < 1073.2 || W > 1800.0 ) return 0.0;

      int iso = 1; // gamma + p -> pi0 + p
      maid07tot_(&iso,&W,&Q2,&fSigT,&fSigL,&fSigLT,&fSigLTp,&fSigTTp, &fSigL0,&fSigLT0,&fSigLT0p);
      num += (-2.0*fSigTTp);
      den += (2.0*fSigT);

      // pi+ threshold is slightly larger than pi0
      if( W > 1079.1 ) {
        iso = 3; // gamma + p -> pi+ + n
        maid07tot_(&iso,&W,&Q2,&fSigT,&fSigL,&fSigLT,&fSigLTp,&fSigTTp, &fSigL0,&fSigLT0,&fSigLT0p);
        num += (-2.0*fSigTTp);
        den += (2.0*fSigT);
      }

      if(den==0.0) return 0;
      return(num/den);
    } 
    //______________________________________________________________________________
    
    double MAID_VCSAs::A2p(double x, double Q2) const
    { 
      // note the maid definitions:
      // sigt=1/2(sig_1/2 + sig_3/2), sigtt'=1/2(sig_3/2 - sig_1/2)
      // sigl0=sigl*(wcm/q)**2, siglt0=siglt*(wcm/q), siglt0'=siglt'*(wcm/q)

      double W = insane::kine::W_xQsq(x,Q2)*1000.0; // W in MeV
      double num = 0.0;
      double den = 0.0;
      //std::cout << "W  = " << W  << "\n";
      //std::cout << "Q2 = " << Q2 << "\n";
      //std::cout << "x  = " << x  << "\n";
      if( Q2 < 0 || Q2 > 5.0) return 0.0; 
      if( W < 1073.2 || W > 1800.0 ) return 0.0;

      int iso = 1; // gamma + p -> pi0 + p
      maid07tot_(&iso,&W,&Q2,&fSigT,&fSigL,&fSigLT,&fSigLTp,&fSigTTp, &fSigL0,&fSigLT0,&fSigLT0p);
      num += (-2.0*fSigLTp);
      den += (2.0*fSigT);

      if( W > 1079.1 ) {
        iso = 3; // gamma + p -> pi+ + n
        maid07tot_(&iso,&W,&Q2,&fSigT,&fSigL,&fSigLT,&fSigLTp,&fSigTTp, &fSigL0,&fSigLT0,&fSigLT0p);
        num += (-2.0*fSigLTp);
        den += (2.0*fSigT);
      }

      if(den==0.0) return 0;
      return(num/den);
    } 
    //______________________________________________________________________________
    
    double MAID_VCSAs::A1n(double x, double Q2) const 
    { 
      // Note the MAID definitions:
      // sigT=1/2(sig_1/2 + sig_3/2), sigTT'=1/2(sig_3/2 - sig_1/2)
      // sigL0=sigL*(wcm/Q)**2, sigLT0=sigLT*(wcm/Q), sigLT0'=sigLT'*(wcm/Q)

      double W = insane::kine::W_xQsq(x,Q2)*1000.0; // W in MeV
      double num = 0.0;
      double den = 0.0;
      //std::cout << "W  = " << W  << "\n";
      //std::cout << "Q2 = " << Q2 << "\n";
      //std::cout << "x  = " << x  << "\n";
      if( Q2 < 0 || Q2 > 5.0) return 0.0; 
      if( W < 1073.2 || W > 1800.0 ) return 0.0;

      int iso = 2; // gamma + n -> pi0 + n
      maid07tot_(&iso,&W,&Q2,&fSigT,&fSigL,&fSigLT,&fSigLTp,&fSigTTp, &fSigL0,&fSigLT0,&fSigLT0p);
      num += (-2.0*fSigTTp);
      den += (2.0*fSigT);

      iso = 4; // gamma + p -> pi- + p
      maid07tot_(&iso,&W,&Q2,&fSigT,&fSigL,&fSigLT,&fSigLTp,&fSigTTp, &fSigL0,&fSigLT0,&fSigLT0p);
      num += (-2.0*fSigTTp);
      den += (2.0*fSigT);

      if(den==0.0) return 0;
      return(num/den);
    } 
    //______________________________________________________________________________
    
    double MAID_VCSAs::A2n(double x, double Q2) const 
    { 
      // note the maid definitions:
      // sigt=1/2(sig_1/2 + sig_3/2), sigtt'=1/2(sig_3/2 - sig_1/2)
      // sigl0=sigl*(wcm/q)**2, siglt0=siglt*(wcm/q), siglt0'=siglt'*(wcm/q)

      double W = insane::kine::W_xQsq(x,Q2)*1000.0; // W in MeV
      double num = 0.0;
      double den = 0.0;
      //std::cout << "W  = " << W  << "\n";
      //std::cout << "Q2 = " << Q2 << "\n";
      //std::cout << "x  = " << x  << "\n";
      if( Q2 < 0 || Q2 > 5.0) return 0.0; 
      if( W < 1073.2 || W > 1800.0 ) return 0.0;

      int iso = 2; // gamma + p -> pi0 + n
      maid07tot_(&iso,&W,&Q2,&fSigT,&fSigL,&fSigLT,&fSigLTp,&fSigTTp, &fSigL0,&fSigLT0,&fSigLT0p);
      num += (-2.0*fSigLTp);
      den += (2.0*fSigT);

      iso = 4; // gamma + n -> pi- + p
      maid07tot_(&iso,&W,&Q2,&fSigT,&fSigL,&fSigLT,&fSigLTp,&fSigTTp, &fSigL0,&fSigLT0,&fSigLT0p);
      num += (-2.0*fSigLTp);
      den += (2.0*fSigT);

      if(den==0.0) return 0;
      return(num/den);
    } 
    //______________________________________________________________________________

  }
}
