#include "insane/asymmetries/fwd_VCSAs.h"

namespace insane {
  namespace physics {

    template class VCSAsFromSFs<Stat2015_SFs,Stat2015_SSFs>;
    template class VCSAsFromSFs<
        insane::physics::F1F209_SFs,
        insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs, insane::physics::JAM_T3DFs,
                                      insane::physics::JAM_T4DFs>>;
    template class VCSAsFromSFs<
        insane::physics::SFsFromPDFs<insane::physics::CTEQ10_UPDFs>,
        insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs, insane::physics::JAM_T3DFs,
                                      insane::physics::JAM_T4DFs>>;
  }
}
