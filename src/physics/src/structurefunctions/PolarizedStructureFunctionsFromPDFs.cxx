#include "insane/structurefunctions/PolarizedStructureFunctionsFromPDFs.h"
#include "insane/structurefunctions/Stat2015PolarizedPDFs.h"

namespace insane {
namespace physics {



PolarizedStructureFunctionsFromPDFs::PolarizedStructureFunctionsFromPDFs()
{
   SetNameTitle("PolarizedStructureFunctionsFromPDFs","Pol.SFs from PDFsF");
   SetLabel("pol.SFs from PDFs");
   fPolarizedPDFs = new Stat2015PolarizedPDFs(); 
}
//_____________________________________________________________________________

PolarizedStructureFunctionsFromPDFs::PolarizedStructureFunctionsFromPDFs(
    PolarizedPartonDistributionFunctions * ppdfs) : fPolarizedPDFs(ppdfs)
{
   SetLabel(Form("%s",ppdfs->GetLabel()));
   SetNameTitle(Form("Spin SFs from %s",ppdfs->GetName()),Form("%s",ppdfs->GetName()));
}
//______________________________________________________________________________

PolarizedStructureFunctionsFromPDFs::~PolarizedStructureFunctionsFromPDFs()
{ }
//_____________________________________________________________________________

void PolarizedStructureFunctionsFromPDFs::SetPolarizedPDFs(PolarizedPartonDistributionFunctions * ppdfs)
{
   fPolarizedPDFs = ppdfs;
   SetLabel(Form("%s",ppdfs->GetLabel()));
   SetNameTitle(Form("Spin SFs from %s",ppdfs->GetName()),Form("%s",ppdfs->GetName()));
}
//______________________________________________________________________________

PolarizedPartonDistributionFunctions * PolarizedStructureFunctionsFromPDFs::GetPolarizedPDFs()
{
   return(fPolarizedPDFs);
}
//______________________________________________________________________________

double PolarizedStructureFunctionsFromPDFs::g1pError(double x,double Q2)
{
   if(!fPolarizedPDFs) {
      Error("g1pErr(x,Q2)","No polarized PDFs set");
      return(0.);
   }
   fPolarizedPDFs->GetPDFErrors(x,Q2); 
   double Qu = Q_up;
   double Qd = Q_down;  
   double Qs = Q_strange;  

   double DeltaUErr    = fPolarizedPDFs->DeltauError(); 
   double DeltaDErr    = fPolarizedPDFs->DeltadError(); 
   double DeltaSErr    = fPolarizedPDFs->DeltasError(); 
   double DeltaUBarErr = fPolarizedPDFs->DeltaubarError(); 
   double DeltaDBarErr = fPolarizedPDFs->DeltadbarError(); 
   double DeltaSBarErr = fPolarizedPDFs->DeltasbarError(); 

   double du_sq  = TMath::Power(Qu*Qu,2.)*(DeltaUErr*DeltaUErr + DeltaUBarErr*DeltaUBarErr); 
   double dd_sq  = TMath::Power(Qd*Qd,2.)*(DeltaDErr*DeltaDErr + DeltaDBarErr*DeltaDBarErr); 
   double ds_sq  = TMath::Power(Qs*Qs,2.)*(DeltaSErr*DeltaSErr + DeltaSBarErr*DeltaSBarErr); 

   double sum_sq = du_sq + dd_sq + ds_sq; 
   double result = 0.5*TMath::Sqrt(sum_sq);  

   if(result < 1E-10) result = 0.; 
   return result;  

}
//_____________________________________________________________________________

double PolarizedStructureFunctionsFromPDFs::g1nError(double x,double Q2)
{

   if(!fPolarizedPDFs) {
      Error("g1nErr(x,Q2)","No polarized PDFs set");
      return(0.);
   }
   fPolarizedPDFs->GetPDFErrors(x,Q2); 
   double Qu = Q_up;
   double Qd = Q_down;  
   double Qs = Q_strange;  
   double DeltaUErr=0,DeltaUBarErr=0; 
   double DeltaDErr=0,DeltaDBarErr=0; 
   double DeltaSErr=0,DeltaSBarErr=0; 

   DeltaUErr    = fPolarizedPDFs->DeltauError(); 
   DeltaDErr    = fPolarizedPDFs->DeltadError(); 
   DeltaSErr    = fPolarizedPDFs->DeltasError(); 
   DeltaUBarErr = fPolarizedPDFs->DeltaubarError(); 
   DeltaDBarErr = fPolarizedPDFs->DeltadbarError(); 
   DeltaSBarErr = fPolarizedPDFs->DeltasbarError(); 

   double du_sq  = TMath::Power(Qd*Qd,2.)*(DeltaUErr*DeltaUErr + DeltaUBarErr*DeltaUBarErr); 
   double dd_sq  = TMath::Power(Qu*Qu,2.)*(DeltaDErr*DeltaDErr + DeltaDBarErr*DeltaDBarErr); 
   double ds_sq  = TMath::Power(Qs*Qs,2.)*(DeltaSErr*DeltaSErr + DeltaSBarErr*DeltaSBarErr); 

   double sum_sq = du_sq + dd_sq + ds_sq; 
   double result = 0.5*TMath::Sqrt(sum_sq);  

   if(result < 1E-10) result = 0.; 
   return result;  

}
//_____________________________________________________________________________

double PolarizedStructureFunctionsFromPDFs::g1He3Error(double x,double Q2)
{
   double Pn      = 0.879;         // neutron polarization in 3He 
   double Pp      = -0.021;        // proton polarization in 3He 
   double g1n_err = g1nError(x,Q2); 
   double g1p_err = g1pError(x,Q2); 
   double sum_sq  = TMath::Power( (Pn + 0.056)*g1n_err , 2. ) 
      + TMath::Power( (2.*Pp - 0.014)*g1p_err, 2.); 
   double result  = TMath::Sqrt(sum_sq); 
   return result;  
}
//_____________________________________________________________________________

double PolarizedStructureFunctionsFromPDFs::g2pWW_TMC(   double x, double Q2)
{
  // Wrong way to do; but this is used as a check
   // g2WW using the Twist-2 TMC corrected g1
   double x0     = x;
   double Q20    = Q2;
   double result = - g1p_Twist2_TMC(x0, Q20);
   Int_t N         = fNintegrate;
   double dx     = (1.0 - x) / ((double)N);
   // quick simple integration
   for (int i = 0; i < N; i++) {
     double y = x0+dx*double(i);
     result += g1p_Twist2_TMC(y, Q20)*dx/y;
   }
   return(result);
}
//______________________________________________________________________________

double PolarizedStructureFunctionsFromPDFs::g2pWW_TMC_t3(double x, double Q2)
{
  // Wrong way to do; but this is used as a check
  // g2WW using the Twist-2 TMC corrected g1
  double x0     = x;
  double Q20    = Q2;
  double result = - g1p_Twist3_TMC(x0, Q20);
  Int_t N         = fNintegrate;
  double dx     = (1.0 - x) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double y = x0+dx*double(i);
    result += g1p_Twist3_TMC(y, Q20)*dx/y;
  }
  return(result);
}
//______________________________________________________________________________

double PolarizedStructureFunctionsFromPDFs::g1p(double x, double Q2) {
  double result = g1p_Twist2(x,Q2) 
    + g1p_Twist3(x,Q2)
    + g1p_Twist4(x,Q2);
  return(result);
}
//_____________________________________________________________________________
double PolarizedStructureFunctionsFromPDFs::g1n(double x, double Q2) {
  double result = g1n_Twist2(x,Q2) 
                   + g1n_Twist3(x,Q2)
                   + g1n_Twist4(x,Q2);
   return(result);
}
//_____________________________________________________________________________
double PolarizedStructureFunctionsFromPDFs::g1d(double x, double Q2) {
   double result = g1d_Twist2(x,Q2) 
                   + g1d_Twist3(x,Q2)
                   + g1d_Twist4(x,Q2);
   return(result);
}
//_____________________________________________________________________________
double PolarizedStructureFunctionsFromPDFs::g1He3(double x, double Q2) {
   double result = g1He3_Twist2(x,Q2) 
                   + g1He3_Twist3(x,Q2)
                   + g1He3_Twist4(x,Q2);
   return(result);
}
//______________________________________________________________________________
double PolarizedStructureFunctionsFromPDFs::g2p(double x, double Q2) {
   double result = g2p_Twist2(x,Q2) 
                   + g2p_Twist3(x,Q2)
                   + g2p_Twist4(x,Q2);
   return(result);
}
//_____________________________________________________________________________
double PolarizedStructureFunctionsFromPDFs::g2n(double x, double Q2) {
   double result = g2n_Twist2(x,Q2) 
                   + g2n_Twist3(x,Q2)
                   + g2n_Twist4(x,Q2);
   return(result);
}
//_____________________________________________________________________________
double PolarizedStructureFunctionsFromPDFs::g2d(double x, double Q2) {
   double result = g2d_Twist2(x,Q2) 
                   + g2d_Twist3(x,Q2)
                   + g2d_Twist4(x,Q2);
   return(result);
}
//_____________________________________________________________________________
double PolarizedStructureFunctionsFromPDFs::g2He3(double x, double Q2) {
   double result = g2He3_Twist2(x,Q2) 
                   + g2He3_Twist3(x,Q2)
                   + g2He3_Twist4(x,Q2);
   return(result);
}
//______________________________________________________________________________

double PolarizedStructureFunctionsFromPDFs::g1p_TMC(double x, double Q2) {
   double result = g1p_Twist2_TMC(x,Q2) 
                   + g1p_Twist3_TMC(x,Q2)
                   + g1p_Twist4_TMC(x,Q2);
   return(result);
}
//_____________________________________________________________________________
double PolarizedStructureFunctionsFromPDFs::g1n_TMC(double x, double Q2) {
   double result = g1n_Twist2_TMC(x,Q2) 
                   + g1n_Twist3_TMC(x,Q2)
                   + g1n_Twist4_TMC(x,Q2);
   return(result);
}
//_____________________________________________________________________________
double PolarizedStructureFunctionsFromPDFs::g1d_TMC(double x, double Q2) {
   double result = g1d_Twist2_TMC(x,Q2) 
                   + g1d_Twist3_TMC(x,Q2)
                   + g1d_Twist4_TMC(x,Q2);
   return(result);
}
//_____________________________________________________________________________
double PolarizedStructureFunctionsFromPDFs::g1He3_TMC(double x, double Q2) {
   double result = g1He3_Twist2_TMC(x,Q2) 
                   + g1He3_Twist3_TMC(x,Q2)
                   + g1He3_Twist4_TMC(x,Q2);
   return(result);
}
//______________________________________________________________________________
double PolarizedStructureFunctionsFromPDFs::g2p_TMC(double x, double Q2) {
   double result = g2p_Twist2_TMC(x,Q2) 
                   + g2p_Twist3_TMC(x,Q2)
                   + g2p_Twist4_TMC(x,Q2);
   return(result);
}
//_____________________________________________________________________________
double PolarizedStructureFunctionsFromPDFs::g2n_TMC(double x, double Q2) {
   double result = g2n_Twist2_TMC(x,Q2) 
                   + g2n_Twist3_TMC(x,Q2)
                   + g2n_Twist4_TMC(x,Q2);
   return(result);
}
//_____________________________________________________________________________
double PolarizedStructureFunctionsFromPDFs::g2d_TMC(double x, double Q2) {
   double result = g2d_Twist2_TMC(x,Q2) 
                   + g2d_Twist3_TMC(x,Q2)
                   + g2d_Twist4_TMC(x,Q2);
   return(result);
}
//_____________________________________________________________________________
double PolarizedStructureFunctionsFromPDFs::g2He3_TMC(double x, double Q2) {
   double result = g2He3_Twist2_TMC(x,Q2) 
                   + g2He3_Twist3_TMC(x,Q2)
                   + g2He3_Twist4_TMC(x,Q2);
   return(result);
}
//______________________________________________________________________________
//

}}
