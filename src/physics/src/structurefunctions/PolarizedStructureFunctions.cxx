#include "insane/structurefunctions/PolarizedStructureFunctions.h"

namespace insane {
namespace physics {

  //PolarizedStructureFunctions * PolarizedStructureFunctions::fgPolarizedStructureFunctions = nullptr;
  //______________________________________________________________________________
    
PolarizedStructureFunctions::PolarizedStructureFunctions()
{
  SetLabel(" pol. SF");
  //SetNameTitle("PolarizedStructureFunctions"," pol. SF");
  fNintegrate = 100;
}
//_____________________________________________________________________________

PolarizedStructureFunctions::~PolarizedStructureFunctions()
{ }
//______________________________________________________________________________

double PolarizedStructureFunctions::G1p(double x, double Q2) {
  // The non-bjorken limit spin strucutre functions
  double nu = Q2 / (2.0 * (M_p/GeV) * x);
  return(g1p(x, Q2) / (nu * M_p/GeV * M_p/GeV));
}
//______________________________________________________________________________

double PolarizedStructureFunctions::G2p(double x, double Q2) {
  // The non-bjorken limit spin strucutre functions
  double nu = Q2 / (2.0 * (M_p/GeV) * x);
  return(g2p(x, Q2) / (nu * nu * (M_p/GeV)));
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::g2pWW(double x, double Q2) {
  // g2WW
  double x0     = x;
  double Q20   = Q2;
  double result = - g1p_Twist2(x0, Q20);
  Int_t N         = fNintegrate;
  double dx     = (1.0 - x) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    result += g1p_Twist2(x0 + dx * ((double)i), Q20) * dx / (x0 + dx * ((double)i));
  }
  // return pdfs to initial x,Q2
  /*      fPolarizedPDFs->GetPDFs(x0,Q20);*/
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::g1p_Twist3(double x, double Q2) {
  // g1 twist-3
  // BT Relation
  double x0     = x;
  double Q20   = Q2;
  double result =  g2p_Twist3(x0, Q20);
  Int_t    N      = fNintegrate;
  double dy     = (1.0 - x) / ((double)N);
  double y = 0.0;
  // quick simple integration
  for (int i = 0; i < N; i++) {
    y = x0 + dy*((double)i);
    result += (-2.0*g2p_Twist3(y,Q20)*dy/(y));
  }
  result *= (4.0*(M_p/GeV)*(M_p/GeV)*x*x);
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::g2nWW(double x, double Q2)
{
  double x0 = x;
  double Q20 = Q2;
  double result = - g1n_Twist2(x0, Q20);
  Int_t N = fNintegrate;
  double dx = (1.0 - x) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    result += g1n_Twist2(x0 + dx * ((double)i), Q20) * dx / (x0 + dx * ((double)i));
  }
  // return pdfs to initial x,Q2
  /*      fPolarizedPDFs->GetPDFs(x0,Q20);*/
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::g1n_Twist3(double x, double Q2)
{
  // BT Relation
  // g1 twist-3
  double x0     = x;
  double Q20   = Q2;
  double result =  g2n_Twist3(x0, Q20);
  Int_t    N      = fNintegrate;
  double dy     = (1.0 - x) / ((double)N);
  double y = 0.0;
  // quick simple integration
  for (int i = 0; i < N; i++) {
    y = x0 + dy*((double)i);
    result += (-2.0*g2n_Twist3(y,Q20)*dy/(y));
  }
  result *= (4.0*(M_p/GeV)*(M_p/GeV)*x*x);
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::g2dWW(double x, double Q2)
{
  double x0 = x;
  double Q20 = Q2;
  double result = - g1d_Twist2(x0, Q20);
  Int_t N = fNintegrate;
  double dx = (1.0 - x) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    result += g1d_Twist2(x0 + dx * ((double)i), Q20) * dx / (x0 + dx * ((double)i));
  }
  // return pdfs to initial x,Q2
  /*      fPolarizedPDFs->GetPDFs(x0,Q20);*/
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::g1d_Twist3(double x, double Q2)
{
  // BT Relation
  // g1 twist-3
  double x0     = x;
  double Q20   = Q2;
  double result =  g2d_Twist3(x0, Q20);
  Int_t    N      = fNintegrate;
  double dy     = (1.0 - x) / ((double)N);
  double y = 0.0;
  // quick simple integration
  for (int i = 0; i < N; i++) {
    y = x0 + dy*((double)i);
    result += (-2.0*g2d_Twist3(y,Q20)*dy/(y));
  }
  result *= (4.0*(M_p/GeV)*(M_p/GeV)*x*x);
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::g2He3WW(double x, double Q2)
{
  double x0 = x;
  double Q20 = Q2;
  double result = - g1He3_Twist2(x0, Q20);
  Int_t N = fNintegrate;
  double dx = (1.0 - x) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    result += g1He3_Twist2(x0 + dx * ((double)i), Q20) * dx / (x0 + dx * ((double)i));
  }
  // return pdfs to initial x,Q2
  /*      fPolarizedPDFs->GetPDFs(x0,Q20);*/
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::g1He3_Twist3(double x, double Q2)
{
  // BT Relation
  // g1 twist-3
  double x0     = x;
  double Q20   = Q2;
  double result =  g2He3_Twist3(x0, Q20);
  Int_t    N      = fNintegrate;
  double dy     = (1.0 - x) / ((double)N);
  double y = 0.0;
  // quick simple integration
  for (int i = 0; i < N; i++) {
    y = x0 + dy*((double)i);
    result += (-2.0*g2He3_Twist3(y,Q20)*dy/(y));
  }
  result *= (4.0*(M_p/GeV)*(M_p/GeV)*x*x);
  return(result);
}
//______________________________________________________________________________

double PolarizedStructureFunctions::g2pWW_Error(   double x, double Q2)
{
  // For now just return the error on g1
  // Need to add error on the integral piece.
  return this->g1p_Error(x,Q2);
} 
//______________________________________________________________________________

double PolarizedStructureFunctions::g2nWW_Error(   double x, double Q2)
{
  // For now just return the error on g1
  // Need to add error on the integral piece.
  return this->g1n_Error(x,Q2);
}
//______________________________________________________________________________

double PolarizedStructureFunctions::g2dWW_Error(   double x, double Q2)
{
  // For now just return the error on g1
  // Need to add error on the integral piece.
  return this->g1d_Error(x,Q2);
}
//______________________________________________________________________________

double PolarizedStructureFunctions::g2He3WW_Error( double x, double Q2)
{
  // For now just return the error on g1
  // Need to add error on the integral piece.
  return this->g1He3_Error(x,Q2);
}
//______________________________________________________________________________

double PolarizedStructureFunctions::g2p_Error(   double x, double Q2)
{ return g2pWW_Error(x,Q2);}
//______________________________________________________________________________

double PolarizedStructureFunctions::g2n_Error(   double x, double Q2)
{ return g2nWW_Error(x,Q2);}
//______________________________________________________________________________

double PolarizedStructureFunctions::g2d_Error(   double x, double Q2)
{ return g2dWW_Error(x,Q2);}
//______________________________________________________________________________

double PolarizedStructureFunctions::g2He3_Error( double x, double Q2)
{ return g2He3WW_Error(x,Q2);}
//_____________________________________________________________________________

double PolarizedStructureFunctions::g1p_Twist2(double   x, double Q2) 
{
  return( g1p(x,Q2)   - g1p_Twist3(x,Q2) - g1p_Twist4(x,Q2));
}
//______________________________________________________________________________

double PolarizedStructureFunctions::g1n_Twist2(double   x, double Q2)
{
  return( g1n(x,Q2)   - g1n_Twist3(x,Q2) - g1n_Twist4(x,Q2));
}
//______________________________________________________________________________

double PolarizedStructureFunctions::g1d_Twist2(double   x, double Q2)
{
  return( g1d(x,Q2)   - g1d_Twist3(x,Q2) - g1d_Twist4(x,Q2));
}
//______________________________________________________________________________

double PolarizedStructureFunctions::g1He3_Twist2(double x, double Q2)
{
  return( g1He3(x,Q2) - g1He3_Twist3(x,Q2) - g1He3_Twist4(x,Q2));
}
//______________________________________________________________________________

double PolarizedStructureFunctions::g1p_Twist4(double,double) { return 0; }
double PolarizedStructureFunctions::g1n_Twist4(double,double) { return 0; }
double PolarizedStructureFunctions::g1d_Twist4(double,double) { return 0; }
double PolarizedStructureFunctions::g1He3_Twist4(double,double) { return 0; }
//______________________________________________________________________________

double PolarizedStructureFunctions::g2p_Twist2(double x,double Q2) {   return g2pWW(x,Q2); }
double PolarizedStructureFunctions::g2n_Twist2(double x,double Q2) {   return g2nWW(x,Q2); }
double PolarizedStructureFunctions::g2d_Twist2(double x,double Q2) {   return g2dWW(x,Q2); }
double PolarizedStructureFunctions::g2He3_Twist2(double x,double Q2) { return g2He3WW(x,Q2); }
//______________________________________________________________________________

double PolarizedStructureFunctions::g2p_Twist3(double,double) { return 0; }
double PolarizedStructureFunctions::g2n_Twist3(double,double) { return 0; }
double PolarizedStructureFunctions::g2d_Twist3(double,double) { return 0; }
double PolarizedStructureFunctions::g2He3_Twist3(double,double) { return 0; }
//______________________________________________________________________________

double PolarizedStructureFunctions::g2p_Twist4(double,double) { return 0; }
double PolarizedStructureFunctions::g2n_Twist4(double,double) { return 0; }
double PolarizedStructureFunctions::g2d_Twist4(double,double) { return 0; }
double PolarizedStructureFunctions::g2He3_Twist4(double,double) { return 0; }
//______________________________________________________________________________

double PolarizedStructureFunctions::g2pbar(double x, double Q2) { return( (g2p(x, Q2)-g2pWW(x,Q2)) ); }
double PolarizedStructureFunctions::g2nbar(double x, double Q2) { return( (g2n(x, Q2)-g2nWW(x,Q2)) ); }
double PolarizedStructureFunctions::g2dbar(double x, double Q2) { return( (g2d(x, Q2)-g2dWW(x,Q2)) ); }
double PolarizedStructureFunctions::g2He3bar(double x, double Q2) { return( (g2He3(x, Q2)-g2He3WW(x,Q2)) ); }
//______________________________________________________________________________

double PolarizedStructureFunctions::g1pError(double   x, double Q2) {return 0;}
double PolarizedStructureFunctions::g1dError(double   x, double Q2) {return 0;}
double PolarizedStructureFunctions::g1nError(double   x, double Q2) {return 0;}
double PolarizedStructureFunctions::g1He3Error(double x, double Q2) {return 0;}

//______________________________________________________________________________

double PolarizedStructureFunctions::xg1p(double x, double Q2)    { return(x * g1p(x, Q2)); }
double PolarizedStructureFunctions::x2g1p(double x, double Q2)   { return(x * xg1p(x, Q2)); }
double PolarizedStructureFunctions::xg1n(double x, double Q2)    { return( x*g1n(x,Q2) ); }
double PolarizedStructureFunctions::x2g1n(double x, double Q2)   { return( x*xg1n(x,Q2) ); }
double PolarizedStructureFunctions::xg1d(double x, double Q2)    { return( x*g1d(x,Q2) ); }
double PolarizedStructureFunctions::x2g1d(double x, double Q2)   { return( x*xg1d(x,Q2) ); }
double PolarizedStructureFunctions::xg1He3(double x, double Q2)  { return( x*g1He3(x,Q2) ); }
double PolarizedStructureFunctions::x2g1He3(double x, double Q2) { return( x*xg1He3(x,Q2) ); }

double PolarizedStructureFunctions::xg2p(  double x, double Q2)    { return(x * g2p(x, Q2)); }
double PolarizedStructureFunctions::x2g2p(  double x, double Q2)   { return(x * x * g2p(x, Q2)); }
double PolarizedStructureFunctions::xg2pWW(double x, double Q2)    { return(x * g2pWW(x, Q2)); }
double PolarizedStructureFunctions::x2g2pWW(double x, double Q2)   { return(x * x * g2pWW(x, Q2)); }

double PolarizedStructureFunctions::xg2n(double x, double Q2)      { return( x*g2n(x,Q2) ); }
double PolarizedStructureFunctions::x2g2n(double x, double Q2)     { return(x * x * g2n(x, Q2)); }
double PolarizedStructureFunctions::xg2nWW(double x, double Q2)    { return(x * g2nWW(x,Q2) ); }
double PolarizedStructureFunctions::x2g2nWW(double x, double Q2)   { return(x * x * g2nWW(x, Q2)); }

double PolarizedStructureFunctions::xg2d(double x, double Q2)      { return( x*g2d(x,Q2) ); }
double PolarizedStructureFunctions::x2g2d(double x, double Q2)     { return(x * x * g2n(x, Q2)); }
double PolarizedStructureFunctions::xg2dWW(double x, double Q2)    { return(x * g2dWW(x,Q2) ); }
double PolarizedStructureFunctions::x2g2dWW(double x, double Q2)   { return(x * x * g2dWW(x, Q2)); }

double PolarizedStructureFunctions::xg2He3(double x, double Q2)    { return( x*g2He3(x,Q2) ); }
double PolarizedStructureFunctions::x2g2He3(double x, double Q2)   { return(x * x * g2He3(x, Q2)); }
double PolarizedStructureFunctions::xg2He3WW(double x, double Q2)  { return(x * g2He3WW(x,Q2) ); }
double PolarizedStructureFunctions::x2g2He3WW(double x, double Q2) { return(x * x * g2He3WW(x, Q2)); }
//______________________________________________________________________________

double PolarizedStructureFunctions::g1p_TMC(double x, double Q2)
{
  double result = g1p_Twist2_TMC(x,Q2) 
    + g1p_Twist3_TMC(x,Q2)
    + g1p_Twist4_TMC(x,Q2);
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::g1n_TMC(double x, double Q2)
{
  double result = g1n_Twist2_TMC(x,Q2) 
    + g1n_Twist3_TMC(x,Q2)
    + g1n_Twist4_TMC(x,Q2);
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::g1d_TMC(double x, double Q2)
{
  double result = g1d_Twist2_TMC(x,Q2) 
    + g1d_Twist3_TMC(x,Q2)
    + g1d_Twist4_TMC(x,Q2);
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::g1He3_TMC(double x, double Q2)
{
  double result = g1He3_Twist2_TMC(x,Q2) 
    + g1He3_Twist3_TMC(x,Q2)
    + g1He3_Twist4_TMC(x,Q2);
  return(result);
}
//______________________________________________________________________________

double PolarizedStructureFunctions::g2p_TMC(double x, double Q2)
{
  double result = g2p_Twist2_TMC(x,Q2) 
    + g2p_Twist3_TMC(x,Q2)
    + g2p_Twist4_TMC(x,Q2);
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::g2n_TMC(double x, double Q2)
{
  double result = g2n_Twist2_TMC(x,Q2) 
    + g2n_Twist3_TMC(x,Q2)
    + g2n_Twist4_TMC(x,Q2);
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::g2d_TMC(double x, double Q2)
{
  double result = g2d_Twist2_TMC(x,Q2) 
    + g2d_Twist3_TMC(x,Q2)
    + g2d_Twist4_TMC(x,Q2);
  return(result);
}
//_____________________________________________________________________________
double PolarizedStructureFunctions::g2He3_TMC(double x, double Q2)
{
  double result = g2He3_Twist2_TMC(x,Q2) 
    + g2He3_Twist3_TMC(x,Q2)
    + g2He3_Twist4_TMC(x,Q2);
  return(result);
}
//______________________________________________________________________________

double PolarizedStructureFunctions::Mellin_g1p(Int_t n, double Q2,double x1, double x2)
{
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    double xn = TMath::Power(x, double(n)-1.0);
    result += (dx*xn*g1p(x,Q2));
  }
  return result;
}
//______________________________________________________________________________

double PolarizedStructureFunctions::Mellin_g2p(Int_t n, double Q2,double x1,double x2)
{
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    double xn = TMath::Power(x, double(n)-1.0);
    result += (dx*xn*g2p(x,Q2));
  }
  return result;
}
//______________________________________________________________________________

double PolarizedStructureFunctions::d2p_WW(double Q2,double x1,double x2)
{
  // d2 tilde (no elastic contribution) 
  // Calculated using only the WW g2 and leading twist g1
  //double x1     = 0.1;
  //double x2     = 0.9;
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*(x*x)*( 2.0*g1p_Twist2(x,Q2) + 3.0*g2pWW(x,Q2) ));
  }
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::d2p_Twist2_TMC(double Q2,double x1,double x2)
{
  // d2 tilde (no elastic contribution) 
  //double x1     = 0.1;
  //double x2     = 0.9;
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*(x*x)*( 2.0*g1p_Twist2_TMC(x,Q2) + 3.0*g2p_Twist2_TMC(x,Q2) ));
  }
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::d2p_Twist3(double Q2,double x1,double x2)
{
  // d2 tilde (no elastic contribution) 
  // Calculated using only the WW g2 and leading twist g1
  //double x1     = 0.1;
  //double x2     = 0.9;
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*(x*x)*( 2.0*g1p_Twist3(x,Q2) + 3.0*g2p_Twist3(x,Q2) ));
  }
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::d2p_Twist3_TMC(double Q2,double x1,double x2)
{
  // d2 tilde (no elastic contribution) 
  // Calculated using only the WW g2 and leading twist g1
  //double x1     = 0.1;
  //double x2     = 0.9;
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*(x*x)*( 2.0*g1p_Twist3_TMC(x,Q2) + 3.0*g2p_Twist3_TMC(x,Q2) ));
  }
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::d2p_tilde(double Q2,double x1,double x2)
{
  // d2 tilde (no elastic contribution) 
  //double x1     = 0.1;
  //double x2     = 0.9;
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*(x*x)*( 2.0*g1p(x,Q2) + 3.0*g2p(x,Q2) ));
  }
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::d2p_tilde_TMC(double Q2,double x1,double x2)
{
  // d2 tilde (no elastic contribution) 
  //double x1     = 0.1;
  //double x2     = 0.9;
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for(int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*(x*x)*( 2.0*g1p_TMC(x,Q2) + 3.0*g2p_TMC(x,Q2) ));
  }
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::d2n_tilde(double Q2,double x1,double x2)
{
  // d2 tilde (no elastic contribution) 
  //double x1     = 0.001;
  //double x2     = 0.999;
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*(x*x)*( 2.0*g1n_Twist2(x,Q2) + 3.0*g2n_Twist3(x,Q2) ));
  }
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::Gamma1_p(double Q2,double x1,double x2 )
{
  // First moment of g1 
  //double x1     = 0.001;
  //double x2     = 0.999;
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*(g1p_Twist2(x,Q2) ));
  }
  return(result);
}
//______________________________________________________________________________

double PolarizedStructureFunctions::Gamma2_p(double Q2,double x1,double x2 )
{
  // d2 tilde (no elastic contribution) 
  //double x1     = 0.001;
  //double x2     = 0.999;
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*(g2p(x,Q2)));
  }
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::Gamma1_n(double Q2,double x1,double x2 )
{
  // First moment of g1 
  //double x1     = 0.001;
  //double x2     = 0.999;
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*(g1n(x,Q2) ));
  }
  return(result);
}
//______________________________________________________________________________

double PolarizedStructureFunctions::Gamma2_n(double Q2,double x1,double x2 )
{
  // d2 tilde (no elastic contribution) 
  //double x1     = 0.001;
  //double x2     = 0.999;
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*(g2n(x,Q2)));
  }
  return(result);
}
//_____________________________________________________________________________

double PolarizedStructureFunctions::Gamma1_p_minus_n(double Q2,double x1,double x2 )
{
  // First moment of g1 
  //double x1     = 0.001;
  //double x2     = 0.999;
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*(g1p(x,Q2)-g1n(x,Q2) ));
  }
  return(result);
}
//______________________________________________________________________________

double PolarizedStructureFunctions::M1n_p(Int_t n, double Q2, double x1, double x2)
{
  // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
  // proton
  if( (n%2 == 0) || n<1 ) {
    Error("M1n_p","First argument can be n = 3,5,7,...");
    return 0;
  }
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*M1nIntegrand_p(n,x,Q2));
  }
  return result;
}
//______________________________________________________________________________

double PolarizedStructureFunctions::M1nIntegrand_p(Int_t n, double x, double Q2)
{
  // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
  // proton Integrand
  double xi = insane::Kine::xi_Nachtmann(x,Q2);
  double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
  double c0 = TMath::Power(xi,double(n+1))/(x*x);
  double c1 = x/xi - TMath::Power(double(n*n)/double(n+2),2.0)*y2*x*xi;
  double c2 = -y2*x*x*double(4*n)/double(n+2);
  return( c0*(c1*g1p(x,Q2) + c2*g2p(x,Q2)) );
}
//______________________________________________________________________________

double PolarizedStructureFunctions::M1n_TMC_p(Int_t n, double Q2, double x1, double x2)
{
  // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
  // proton
  if( (n%2 == 0) || n<1 ) {
    Error("M1n_p","First argument can be n = 3,5,7,...");
    return 0;
  }
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*M1nIntegrand_TMC_p(n,x,Q2));
  }
  return result;
}
//______________________________________________________________________________

double PolarizedStructureFunctions::M1nIntegrand_TMC_p(Int_t n, double x, double Q2)
{
  // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
  // proton Integrand
  double xi = insane::Kine::xi_Nachtmann(x,Q2);
  double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
  double c0 = TMath::Power(xi,double(n+1))/(x*x);
  double c1 = x/xi - TMath::Power(double(n*n)/double(n+2),2.0)*y2*x*xi;
  double c2 = -y2*x*x*double(4*n)/double(n+2);
  return( c0*(c1*g1p_TMC(x,Q2) + c2*g2p_TMC(x,Q2)) );
}
//______________________________________________________________________________

double PolarizedStructureFunctions::M2n_p(Int_t n, double Q2, double x1, double x2)
{
  // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
  // proton
  if( (n%2 == 0) || n<3 ) { 
    Error("M2n_p","First argument can be n = 3,5,7,...");
    return 0;
  }
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*M2nIntegrand_p(n,x,Q2));
  }
  return result;
}
//______________________________________________________________________________

double PolarizedStructureFunctions::M2nIntegrand_p(Int_t n, double x, double Q2)
{
  // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
  // proton Integrand
  double xi = insane::Kine::xi_Nachtmann(x,Q2);
  double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
  double c0 = TMath::Power(xi,double(n+1))/(x*x);
  double c1 = x/xi;
  double c2 = (x/xi)*(x/xi)*double(n)/double(n-1) - y2*x*x*double(n)/double(n+1);
  return( c0*(c1*g1p(x,Q2) + c2*g2p(x,Q2)) );
}
//______________________________________________________________________________

double PolarizedStructureFunctions::M2n_TMC_p(Int_t n, double Q2, double x1, double x2)
{
  // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
  // proton
  if( (n%2 == 0) || n<3 ) { 
    Error("M2n_p","First argument can be n = 3,5,7,...");
    return 0;
  }
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*M2nIntegrand_TMC_p(n,x,Q2));
  }
  return result;
}
//______________________________________________________________________________

double PolarizedStructureFunctions::M2nIntegrand_TMC_p(Int_t n, double x, double Q2)
{
  // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
  // proton Integrand
  double xi = insane::Kine::xi_Nachtmann(x,Q2);
  double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
  double c0 = TMath::Power(xi,double(n)+1.0)/(x*x);
  double c1 = x/xi;
  double c2 = (x/xi)*(x/xi)*double(n)/(double(n)-1.0) - y2*x*x*double(n)/(double(n)+1.0);
  return( c0*(c1*g1p_TMC(x,Q2) + c2*g2p_TMC(x,Q2)) );
}
//______________________________________________________________________________

double PolarizedStructureFunctions::M2n_TMC_test_p(Int_t n, double Q2, double x1, double x2)
{
  // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
  // proton
  if( (n%2 == 0) || n<3 ) { 
    Error("M2n_p","First argument can be n = 3,5,7,...");
    return 0;
  }
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += (dx*M2nIntegrand_TMC_test_p(n,x,Q2));
  }
  return result;
}
//______________________________________________________________________________

double PolarizedStructureFunctions::M2nIntegrand_TMC_test_p(Int_t n, double x, double Q2)
{
  // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
  // proton Integrand
  double xi = insane::Kine::xi_Nachtmann(x,Q2);
  double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
  double c0 = TMath::Power(xi,double(n+1))/(x*x);
  double c1 = x/xi;
  double c2 = (x/xi)*(x/xi)*double(n)/double(n-1) - y2*x*x*double(n)/double(n+1);
  return( c0*(c1*g1p_Twist2_TMC(x,Q2) + c2*g2p_TMC(x,Q2)) );
}
//______________________________________________________________________________

double PolarizedStructureFunctions::d2_Oscar(double Q2, double x1, double x2)
{
  // Use g_T with full TMC  but g1 w/out twist-3
  auto integrand  = [&](double x, double g1, double gT ) {
    double xi = insane::Kine::xi_Nachtmann(x,Q2);
    double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
    double c0 = xi*xi;
    double c1 = 2.0*(xi/x)-3.0*(1.0-xi*xi*y2/2.0);
    double c2 = 3.0*(1.0-xi*xi*y2/2.0);
    return( c0*(c1*g1 + c2*gT) );
  };
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1) / ((double)N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x  = x1 + dx*double(i);
    double g1 = g1p_Twist2_TMC(x,Q2);
    double gT = g1p_TMC(x,Q2) + g2p_TMC(x,Q2);
    result += (dx*integrand(x,g1,gT));
  }
  return result;
}
//______________________________________________________________________________

double PolarizedStructureFunctions::Moment_g2p_Twist3(int n, double Q2, double x1, double x2)
{
  double result = 0.0;
  Int_t N         = fNintegrate;
  double dx     = (x2 - x1)/double(N);
  // quick simple integration
  for (int i = 0; i < N; i++) {
    double x = x1 + dx*double(i);
    result += dx*TMath::Power(x,double(n-1.0))*g2p_Twist3(x,Q2);
  }
  return(result);
}
//_____________________________________________________________________________

TF1 * PolarizedStructureFunctions::GetFunction1() { 
  TF1 * f1  = new TF1(Form("func%s",this->GetName()) , this, &PolarizedStructureFunctions::Evaluateg2pWW, 0.01,0.99,1);  
  return f1;
}
//______________________________________________________________________________
TF1 * PolarizedStructureFunctions::GetFunction2() { 
  TF1 * f1  = new TF1(Form("func%s",this->GetName()) , this, &PolarizedStructureFunctions::Evaluatex2g2pWW, 0.01,0.99,1);  
  return f1;
}
//______________________________________________________________________________
TF1 * PolarizedStructureFunctions::GetFunction3() { 
  TF1 * f1  = new TF1(Form("func%s",this->GetName()) , this, &PolarizedStructureFunctions::Evaluateg2pWW, 0.01,0.99,1);  
  return f1;
}
//______________________________________________________________________________
TF1 * PolarizedStructureFunctions::GetFunction4() { 
  TF1 * f1  = new TF1(Form("func%s",this->GetName()) , this, &PolarizedStructureFunctions::Evaluateg2pWW, 0.01,0.99,1);  
  return f1;
}
//______________________________________________________________________________
void PolarizedStructureFunctions::GetValues(TObject *obj, double Q2, StructureFunctionBase::PolarizedSFType q){
  // Fills histogram with values.
  //  For error band use GetErrorBand
  //if ( !(obj->InheritsFrom(TH1::Class())) ) {
  //   Error("GetErrorBand","Not a TH1 class");
  //   return;
  //}
  if(!obj) {
    return;
  }
  //  returns errorsband
  auto *hfit = (TH1*)obj;
  Int_t hxfirst = hfit->GetXaxis()->GetFirst();
  Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
  Int_t hyfirst = hfit->GetYaxis()->GetFirst();
  Int_t hylast  = hfit->GetYaxis()->GetLast(); 
  Int_t hzfirst = hfit->GetZaxis()->GetFirst();
  Int_t hzlast  = hfit->GetZaxis()->GetLast(); 

  TAxis *xaxis  = hfit->GetXaxis();
  TAxis *yaxis  = hfit->GetYaxis();
  TAxis *zaxis  = hfit->GetZaxis();

  double x[3];

  for (Int_t binz=hzfirst; binz<=hzlast; binz++){
    x[2]=zaxis->GetBinCenter(binz);
    for (Int_t biny=hyfirst; biny<=hylast; biny++) {
      x[1]=yaxis->GetBinCenter(biny);
      for (Int_t binx=hxfirst; binx<=hxlast; binx++) {
        x[0]=xaxis->GetBinCenter(binx);

        switch( q ) {

          // ---- proton
          case StructureFunctionBase::kg1p :
            hfit->SetBinContent(binx, biny, binz, this->xg1p(x[0],Q2));
            break;

          case StructureFunctionBase::kg2pWW :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2pWW(x[0],Q2));
            break;

          case StructureFunctionBase::kg2p :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2p(x[0],Q2));
            break;

            // ---- neutron
          case StructureFunctionBase::kg1n :
            hfit->SetBinContent(binx, biny, binz, this->xg1n(x[0],Q2));
            break;

          case StructureFunctionBase::kg2nWW :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2nWW(x[0],Q2));
            break;

          case StructureFunctionBase::kg2n :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2n(x[0],Q2));
            break;

            // ---- deuteron
          case StructureFunctionBase::kg1d :
            hfit->SetBinContent(binx, biny, binz, this->xg1d(x[0],Q2));
            break;

          case StructureFunctionBase::kg2dWW :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2dWW(x[0],Q2));
            break;

          case StructureFunctionBase::kg2d :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2d(x[0],Q2));
            break;

            // ---- He3
          case StructureFunctionBase::kg1He3 :
            hfit->SetBinContent(binx, biny, binz, this->xg1He3(x[0],Q2));
            break;

          case StructureFunctionBase::kg2He3WW :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2He3WW(x[0],Q2));
            break;

          case StructureFunctionBase::kg2He3 :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2He3(x[0],Q2));
            break;

        }
        //GetPDFErrors(x[0], Q2);
        //         hfit->SetBinError(binx, biny, binz, x[0]*fPDFErrors[q]);
      }
    }
  }

}
//_____________________________________________________________________________

void PolarizedStructureFunctions::GetErrorBand(TObject *obj, double Q2, StructureFunctionBase::PolarizedSFType q)
{
  // Returns error band.
  // Note: use draw option "e3"  
  //if ( !(obj->InheritsFrom(TH1::Class())) ) {
  //   Error("GetErrorBand","Not a TH1 class");
  //   return;
  //}
  if(!obj) {
    return;
  }
  //  returns errorsband
  auto *hfit = (TH1*)obj;
  Int_t hxfirst = hfit->GetXaxis()->GetFirst();
  Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
  Int_t hyfirst = hfit->GetYaxis()->GetFirst();
  Int_t hylast  = hfit->GetYaxis()->GetLast(); 
  Int_t hzfirst = hfit->GetZaxis()->GetFirst();
  Int_t hzlast  = hfit->GetZaxis()->GetLast(); 

  TAxis *xaxis  = hfit->GetXaxis();
  TAxis *yaxis  = hfit->GetYaxis();
  TAxis *zaxis  = hfit->GetZaxis();

  double x[3];

  double err = 0.0;

  for (Int_t binz=hzfirst; binz<=hzlast; binz++){
    x[2]=zaxis->GetBinCenter(binz);
    for (Int_t biny=hyfirst; biny<=hylast; biny++) {
      x[1]=yaxis->GetBinCenter(biny);
      for (Int_t binx=hxfirst; binx<=hxlast; binx++) {
        x[0]=xaxis->GetBinCenter(binx);

        switch( q ) {

          // ------ proton
          case StructureFunctionBase::kg1p :
            hfit->SetBinContent(binx, biny, binz, this->xg1p(x[0],Q2));
            err = x[0]*this->g1p_Error(x[0],Q2);
            if( err == 0.0 )  err = 1.0e-8;
            hfit->SetBinError(binx, biny, binz, err);
            break;

          case StructureFunctionBase::kg2pWW :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2pWW(x[0],Q2));
            err = x[0]*this->g2pWW_Error(x[0],Q2);
            if( err == 0.0 )  err = 1.0e-8;
            hfit->SetBinError(binx, biny, binz, err);
            break;

          case StructureFunctionBase::kg2p :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2p(x[0],Q2));
            err = x[0]*this->g2p_Error(x[0],Q2);
            if( err == 0.0 )  err = 1.0e-8;
            hfit->SetBinError(binx, biny, binz, err);
            break;

            // ------ neutron
          case StructureFunctionBase::kg1n :
            hfit->SetBinContent(binx, biny, binz, this->xg1n(x[0],Q2));
            err = x[0]*this->g1n_Error(x[0],Q2);
            if( err == 0.0 )  err = 1.0e-8;
            hfit->SetBinError(binx, biny, binz, err);
            break;

          case StructureFunctionBase::kg2nWW :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2nWW(x[0],Q2));
            err = x[0]*this->g2nWW_Error(x[0],Q2);
            if( err == 0.0 )  err = 1.0e-8;
            hfit->SetBinError(binx, biny, binz, err);
            break;

          case StructureFunctionBase::kg2n :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2n(x[0],Q2));
            err = x[0]*this->g2n_Error(x[0],Q2);
            if( err == 0.0 )  err = 1.0e-8;
            hfit->SetBinError(binx, biny, binz, err);
            break;

            // ------ deuteron
          case StructureFunctionBase::kg1d :
            hfit->SetBinContent(binx, biny, binz, this->xg1d(x[0],Q2));
            err = x[0]*this->g1d_Error(x[0],Q2);
            if( err == 0.0 )  err = 1.0e-8;
            hfit->SetBinError(binx, biny, binz, err);
            break;

          case StructureFunctionBase::kg2dWW :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2dWW(x[0],Q2));
            err = x[0]*this->g2dWW_Error(x[0],Q2);
            if( err == 0.0 )  err = 1.0e-8;
            hfit->SetBinError(binx, biny, binz, err);
            break;

          case StructureFunctionBase::kg2d :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2d(x[0],Q2));
            err = x[0]*this->g2d_Error(x[0],Q2);
            if( err == 0.0 )  err = 1.0e-8;
            hfit->SetBinError(binx, biny, binz, err);
            break;

            // ------ He3
          case StructureFunctionBase::kg1He3 :
            hfit->SetBinContent(binx, biny, binz, this->xg1He3(x[0],Q2));
            err = x[0]*this->g1He3_Error(x[0],Q2);
            if( err == 0.0 )  err = 1.0e-8;
            hfit->SetBinError(binx, biny, binz, err);
            break;

          case StructureFunctionBase::kg2He3WW :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2He3WW(x[0],Q2));
            err = x[0]*this->g2He3WW_Error(x[0],Q2);
            if( err == 0.0 )  err = 1.0e-8;
            hfit->SetBinError(binx, biny, binz, err);
            break;

          case StructureFunctionBase::kg2He3 :
            hfit->SetBinContent(binx, biny, binz, x[0]*this->g2He3(x[0],Q2));
            err = x[0]*this->g2He3_Error(x[0],Q2);
            if( err == 0.0 )  err = 1.0e-8;
            hfit->SetBinError(binx, biny, binz, err);
            break;
        }
      }
    }
  }

}
//______________________________________________________________________________
}}
