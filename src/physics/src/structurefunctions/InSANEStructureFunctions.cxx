#include "insane/structurefunctions/StructureFunctions.h"
#include "TMath.h"
#include <iterator>
#include <iostream>
#include <cstdlib>
#include "TCanvas.h"
#include "TAxis.h"
#include "TGraph2D.h"
#include "TGraph.h"
#include "TStyle.h"
#include "TVirtualPad.h"
#include "TLatex.h"
#include "TF1.h"
#include "TF2.h"
#include "TLegend.h"
#include "TROOT.h"

namespace insane {
namespace physics {

double EMC_Effect(double *x, double *p) { 
   // Parameter is A 
   float RES = 0.0;
   int goodfit;
   float X = x[0];
   float A = p[0];
   emc_09_( &X, &A, &goodfit, &RES);
   //std::cout << "A = " << A << " " ;
   //std::cout << "x = " << X << " res = " ;
   //std::cout << RES << std::endl;
   return(double(RES));
}
//______________________________________________________________________________
double EMC_Effect(double x, double A) { 
   // Parameter is A 
   float RES = 0.0;
   int goodfit;
   float XX = x;
   float AA = A;
   emc_09_( &XX, &AA, &goodfit, &RES);
   //std::cout << "A = " << A << " " ;
   //std::cout << "x = " << X << " res = " ;
   //std::cout << RES << std::endl;
   return(double(RES));
}

ClassImp(StructureFunctionBase)
//_____________________________________________________________________________
StructureFunctionBase::StructureFunctionBase(){
   SetNameTitle("StructureFunctionBase","sf");
   fLabel    = "";
   fComments = "";
}
//______________________________________________________________________________
StructureFunctionBase::~StructureFunctionBase() {
}
//______________________________________________________________________________
void StructureFunctionBase::Print(Option_t * opt) const {
   TNamed::Print(opt);
   std::cout << fComments << std::endl;
}
//______________________________________________________________________________



ClassImp(StructureFunctions)
//______________________________________________________________________________
StructureFunctions::StructureFunctions(){ 
}
StructureFunctions::~StructureFunctions(){
}
//______________________________________________________________________________
double StructureFunctions::F1p_Error(   double x, double Q2){return 0.0;}
double StructureFunctions::F2p_Error(   double x, double Q2){return 0.0;}
double StructureFunctions::F1n_Error(   double x, double Q2){return 0.0;}
double StructureFunctions::F2n_Error(   double x, double Q2){return 0.0;}
double StructureFunctions::F1d_Error(   double x, double Q2){return 0.0;}
double StructureFunctions::F2d_Error(   double x, double Q2){return 0.0;}
double StructureFunctions::F1He3_Error( double x, double Q2){return 0.0;}
double StructureFunctions::F2He3_Error( double x, double Q2){return 0.0;}
//______________________________________________________________________________
double StructureFunctions::xF1p(   double x, double Q2) { return(x*F1p(x, Q2)); }
double StructureFunctions::xF2p(   double x, double Q2) { return(x*F2p(x, Q2)); }
double StructureFunctions::xF1n(   double x, double Q2) { return(x*F1n(x, Q2)); }
double StructureFunctions::xF2n(   double x, double Q2) { return(x*F2p(x, Q2)); }
double StructureFunctions::xF1d(   double x, double Q2) { return(x*F1d(x, Q2)); }
double StructureFunctions::xF2d(   double x, double Q2) { return(x*F2d(x, Q2)); }
double StructureFunctions::xF1He3( double x, double Q2) { return(x*F1He3(x, Q2)); }
double StructureFunctions::xF2He3( double x, double Q2) { return(x*F2He3(x, Q2)); }
//______________________________________________________________________________

double StructureFunctions::R(double x, double Q2) {
   double res = F2p(x, Q2) / (2.0 * x * F1p(x, Q2)) ;
   res = res * (1 + 4.0 * M_p/GeV * M_p/GeV * x * x / Q2) - 1.0;
   if(TMath::IsNaN(res) ) return 0.0;
   return res;
}
//______________________________________________________________________________
double StructureFunctions::Rnp(double x, double Q2){
   double F2_n = this->F2n(x,Q2);
   double F2_p = this->F2p(x,Q2);
   return(F2_n/F2_p);
}
//______________________________________________________________________________
double StructureFunctions::Rnp_Error(double x, double Q2){
   double F2_n  = F2n(x,Q2);
   double F2_p  = F2p(x,Q2);
   double eF2_n = F2n_Error(x,Q2);
   double eF2_p = F2p_Error(x,Q2);
   double R_np  = F2_n/F2_p;
   // Here we assume zero covariance term in the unc. prop.
   double eR_np = TMath::Sqrt((eF2_n*eF2_n)/(F2_n*F2_n) + (eF2_p*eF2_p)/(F2_p*F2_p));
   return(R_np*eR_np);
}
//______________________________________________________________________________
double StructureFunctions::W1p(double x, double Qsq) { return(F1p(x, Qsq) / (M_p/GeV)); }
double StructureFunctions::W2p(double x, double Qsq) { return(F2p(x, Qsq) / (Qsq / (2.0 * (M_p/GeV) * x))); }
double StructureFunctions::W1n(double x, double Qsq) { return(F1n(x, Qsq) / (M_n/GeV)); }
double StructureFunctions::W2n(double x, double Qsq) { return(F2n(x, Qsq) / (Qsq / (2.0 * (M_n/GeV) * x))); }
//______________________________________________________________________________
// Returns F2 per nucleus (not per nucleon)
double StructureFunctions::F2Nuclear(double x,double Qsq,double Z, double A){
   Error("F2Nuclear","Not implemented");
   return 0.0; 
}
double StructureFunctions::xF2Nuclear(double x, double Qsq,double Z, double A) {
   return(x * F2Nuclear(x, Qsq,Z,A));
}
double StructureFunctions::F1Nuclear(double x,double Qsq,double Z, double A){
   Error("F1Nuclear","Not implemented");
   return 0.0; 
}
double StructureFunctions::xF1Nuclear(double x, double Qsq,double Z, double A) {
   return(x * F1Nuclear(x, Qsq,Z,A));
}
//______________________________________________________________________________
void StructureFunctions::GetValues(TObject *obj, double Q2, StructureFunctionBase::UnpolarizedSFType q){
   // Fills histogram with values.
   //  For error band use GetErrorBand
   //if ( !(obj->InheritsFrom(TH1::Class())) ) {
   //   Error("GetErrorBand","Not a TH1 class");
   //   return;
   //}
   if(!obj) {
      return;
   }
   //  returns errorsband
   auto *hfit = (TH1*)obj;
   Int_t hxfirst = hfit->GetXaxis()->GetFirst();
   Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
   Int_t hyfirst = hfit->GetYaxis()->GetFirst();
   Int_t hylast  = hfit->GetYaxis()->GetLast(); 
   Int_t hzfirst = hfit->GetZaxis()->GetFirst();
   Int_t hzlast  = hfit->GetZaxis()->GetLast(); 

   TAxis *xaxis  = hfit->GetXaxis();
   TAxis *yaxis  = hfit->GetYaxis();
   TAxis *zaxis  = hfit->GetZaxis();

   double x[3];

   for (Int_t binz=hzfirst; binz<=hzlast; binz++){
      x[2]=zaxis->GetBinCenter(binz);
      for (Int_t biny=hyfirst; biny<=hylast; biny++) {
         x[1]=yaxis->GetBinCenter(biny);
         for (Int_t binx=hxfirst; binx<=hxlast; binx++) {
            x[0]=xaxis->GetBinCenter(binx);

            hfit->SetBinContent(binx, biny, binz, this->Rnp(x[0],Q2));
   //GetPDFErrors(x[0], Q2);
   //         hfit->SetBinError(binx, biny, binz, x[0]*fPDFErrors[q]);
         }
      }
   }

}
//_____________________________________________________________________________
void StructureFunctions::GetErrorBand(TObject *obj, double Q2, StructureFunctionBase::UnpolarizedSFType q){
   //if ( !(obj->InheritsFrom(TH1::Class())) ) {
   //   Error("GetErrorBand","Not a TH1 class");
   //   return;
   //}
   if(!obj) {
      return;
   }
   //  returns errorsband
   auto *hfit = (TH1*)obj;
   Int_t hxfirst = hfit->GetXaxis()->GetFirst();
   Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
   Int_t hyfirst = hfit->GetYaxis()->GetFirst();
   Int_t hylast  = hfit->GetYaxis()->GetLast(); 
   Int_t hzfirst = hfit->GetZaxis()->GetFirst();
   Int_t hzlast  = hfit->GetZaxis()->GetLast(); 

   TAxis *xaxis  = hfit->GetXaxis();
   TAxis *yaxis  = hfit->GetYaxis();
   TAxis *zaxis  = hfit->GetZaxis();

   double x[3];

   
   for (Int_t binz=hzfirst; binz<=hzlast; binz++){
      x[2]=zaxis->GetBinCenter(binz);
      for (Int_t biny=hyfirst; biny<=hylast; biny++) {
         x[1]=yaxis->GetBinCenter(biny);
         for (Int_t binx=hxfirst; binx<=hxlast; binx++) {
            x[0]=xaxis->GetBinCenter(binx);

            hfit->SetBinContent(binx, biny, binz, this->Rnp(x[0],Q2));
            double err = x[0]*this->Rnp_Error(x[0],Q2);
            if( err == 0.0 )  err = 1.0e-8;
            hfit->SetBinError(binx, biny, binz, err);
         }
      }
   }

}
//______________________________________________________________________________


}}
