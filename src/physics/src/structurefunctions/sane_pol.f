      !> Calculates the Jlab beam polarization 
      !!
      !! @ingroup beam
      !!
      !! From sanewiki:
      !! Finally, I have encapsulated all of the above information in a fortran subroutine here: sane_pol.f !! It requires as inputs:
      !! 
      !! @param hallcp Beam energy --> from HALLC:p
      !! 
      !! @param wien Wien angle in degrees
      !! 
      !! @param qe Quantum efficiency --> I have been using the value nearest in time to the measurement recorded in the ELOG
      !! 
      !! @param npass Number of passes (4 pass or 5 pass)
      !! 
      !! @param ihwp Half wave plate state (0=IN, 1=OUT)
      !! 
      !! @param polarization beam polarization
      !! 
      !! Note that right now, it will return a beam polarization with the sign consistent with what the Moller would measure. In general, when the measured polarization is "negative" this means that the "h+" signal corresponds to an electron with polarization pointing UPstream.
      !! 
      subroutine sane_pol(hallcp,wien,qe,npass,ihwp,polarization)

      real*8 polarization
      real*8 hallcp,wien,qe
      integer npass,ihwp


      integer npassc,j,i
      real*8 cbend,NLE,SLE,INJE,spin
      real*8 g2,me
      real*8 etot,Eavg,deltaE,etotc
      real*8 cpol
      real*8 ratio,qecor
      real*8 polmag,ecor
      
c fitted parameters	
      parameter(polmag=89.506)
      parameter(ecor=0.99992)

c correct HALLC:p 
      etot = ecor*hallcp

C Here - correct for qe dependence

        qecor=(82.439+13.468*qe-14.837*qe**2+4.3993*qe**3)/
     >   (82.439+13.468*0.3124-14.837*0.3124**2+4.3993*0.3124**3)

	cbend=37.52d0

	g2=0.001159652193d0
	me=0.51099892


        INJE=65.54
        deltaE=2.9752
        if(npass.gt.4) then
           if (etot.lt.5000.0) then
              INJE=52.66
              deltaE = -1.0
           endif
        endif
	npassc=int(npass)

        Eavg = (etot-inje)/npassc/2.0

        NLE = Eavg + deltaE
        SLE = Eavg - deltaE

        etotc=INJE+npassc*(NLE+SLE) ! should be the same as etot

        spin = wien

c trips through east arc
        do i=1,npassc
           spin = spin + g2/me*(INJE+i*NLE+(i-1)*SLE)*180.0d0
        enddo

c trips through west arc
        do i=1,npassc-1
           spin = spin + g2/me*(INJE+i*NLE+i*SLE)*180.0d0
        enddo

        spin=spin-cbend*g2*etot/me
        cpol=cos(spin*3.141592654/180.0)

        if(ihwp.gt.0.5) then
           relpol=-cpol*qecor
        else
           relpol=cpol*qecor
        endif

        polarization=relpol*polmag

        return
	end
