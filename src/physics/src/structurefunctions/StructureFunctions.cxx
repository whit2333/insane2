#include "insane/structurefunctions/StructureFunctions.h"
#include "insane/pdfs/FortranWrappers.h"
#include "insane/base/Physics.h"
#include "TH1F.h"

namespace insane {

  using namespace units;

  namespace physics {

    double R1998(double x, double Q2) {
      double R = (1. / 3.) * (Ra1998(x, Q2) + Rb1998(x, Q2) + Rc1998(x, Q2));
      return R;
    }

    double RErr1998(double x, double Q2) {
      // First term
      double T1 = 0.0078;
      // Second term
      double T2 = -0.013 * x;
      // Third term
      double a  = 0.070 - 0.39 * x + 0.70 * x * x;
      double b  = 1.7 + Q2;
      double T3 = a / b;
      // Put it together
      double dR = T1 + T2 + T3;
      return dR;
    }
    double Ra1998(double x, double Q2) {
      // First term
      double n1 = 0.0485;
      double d1 = log(Q2 / 0.04);
      double T1 = (n1 / d1) * Theta1998(x, Q2);
      // Second term
      double n2 = 0.5470;
      double Q4 = Q2 * Q2;
      double Q8 = Q2 * Q4;
      double p4 = pow(2.0621, 4.);
      double d2 = pow(Q8 + p4, 1. / 4.);
      double a  = n2 / d2;
      double b  = 1. - 0.3804 * x + 0.5090 * x * x;
      double T2 = a * b * pow(x, -0.0285);
      // Put it together
      double Ra = T1 + T2;
      return Ra;
    }
    double Rb1998(double x, double Q2) {
      // First term
      double n1 = 0.0481;
      double d1 = log(Q2 / 0.04);
      double T1 = (n1 / d1) * Theta1998(x, Q2);
      // Second term
      double n21 = 0.6114;
      double a   = n21 / Q2;
      double Q4  = Q2 * Q2;
      double p   = 0.3;
      double p2  = p * p;
      double n22 = 0.3509;
      double d22 = Q4 + p2;
      double b   = n22 / d22;
      double c   = 1. - 0.4611 * x + 0.7172 * x * x;
      double T2  = (a - b) * c * pow(x, -0.0317);
      // Put it together
      double Rb = T1 + T2;
      return Rb;
    }
    double Rc1998(double x, double Q2) {
      // First term
      double n1 = 0.0577;
      double d1 = log(Q2 / 0.04);
      double T1 = (n1 / d1) * Theta1998(x, Q2);
      // Second term
      double a  = 0.4644;
      double b  = 1.8288;
      double c  = Q2 - 12.3708 * x + 43.1043 * x * x - 41.7415 * x * x * x;
      double b2 = b * b;
      double c2 = c * c;
      double T2 = a * pow(b2 + c2, -0.5);
      // Put it together
      double Rc = T1 + T2;
      return Rc;
    }
    double Theta1998(double x, double Q2) {
      // First term
      double T1 = 1.;
      // Second term
      double a  = 12.;
      double b  = Q2 / (Q2 + 1.);
      double p  = 0.125;
      double p2 = p * p;
      double x2 = x * x;
      double c  = p2 / (p2 + x2);
      double T2 = a * b * c;
      // Put it together
      double Theta = T1 + T2;
      return Theta;
    }

    //StructureFunctions * StructureFunctions::fgStructureFunctions = nullptr;
    //________________________________________________________________________________

    double EMC_Effect(double *x, double *p) { 
      // Parameter is A 
      float RES = 0.0;
      int goodfit;
      float X = x[0];
      float A = p[0];
      emc_09_( &X, &A, &goodfit, &RES);
      //std::cout << "A = " << A << " " ;
      //std::cout << "x = " << X << " res = " ;
      //std::cout << RES << std::endl;
      return(double(RES));
    }
    //______________________________________________________________________________
    double EMC_Effect(double x, double A) { 
      // Parameter is A 
      float RES = 0.0;
      int goodfit;
      float XX = x;
      float AA = A;
      emc_09_( &XX, &AA, &goodfit, &RES);
      //std::cout << "A = " << A << " " ;
      //std::cout << "x = " << X << " res = " ;
      //std::cout << RES << std::endl;
      return(double(RES));
    }

    //_____________________________________________________________________________

    StructureFunctionBase::StructureFunctionBase() : fLabel(""), fComments("") {}
    StructureFunctionBase::StructureFunctionBase(const std::string& l, const std::string& c ) : fLabel(l), fComments(c) {}
    //______________________________________________________________________________

    StructureFunctionBase::~StructureFunctionBase() {}
    //______________________________________________________________________________
   
    void StructureFunctionBase::Print(Option_t * opt) const {
      std::cout << fComments << std::endl;
    }
    ////______________________________________________________________________________

    //StructureFunctions::StructureFunctions() {}
    ////______________________________________________________________________________

    //StructureFunctions::~StructureFunctions() {}
    ////______________________________________________________________________________
    //
    //double StructureFunctions::F1p_Error(   double x, double Q2){return 0.0;}
    //double StructureFunctions::F2p_Error(   double x, double Q2){return 0.0;}
    //double StructureFunctions::F1n_Error(   double x, double Q2){return 0.0;}
    //double StructureFunctions::F2n_Error(   double x, double Q2){return 0.0;}
    //double StructureFunctions::F1d_Error(   double x, double Q2){return 0.0;}
    //double StructureFunctions::F2d_Error(   double x, double Q2){return 0.0;}
    //double StructureFunctions::F1He3_Error( double x, double Q2){return 0.0;}
    //double StructureFunctions::F2He3_Error( double x, double Q2){return 0.0;}
    ////______________________________________________________________________________
    //
    //double StructureFunctions::xF1p(   double x, double Q2) { return(x*F1p(x, Q2)); }
    //double StructureFunctions::xF2p(   double x, double Q2) { return(x*F2p(x, Q2)); }
    //double StructureFunctions::xF1n(   double x, double Q2) { return(x*F1n(x, Q2)); }
    //double StructureFunctions::xF2n(   double x, double Q2) { return(x*F2p(x, Q2)); }
    //double StructureFunctions::xF1d(   double x, double Q2) { return(x*F1d(x, Q2)); }
    //double StructureFunctions::xF2d(   double x, double Q2) { return(x*F2d(x, Q2)); }
    //double StructureFunctions::xF1He3( double x, double Q2) { return(x*F1He3(x, Q2)); }
    //double StructureFunctions::xF2He3( double x, double Q2) { return(x*F2He3(x, Q2)); }
    ////______________________________________________________________________________

    //double StructureFunctions::R(double x, double Q2) {
    //  double res = F2p(x, Q2) / (2.0 * x * F1p(x, Q2)) ;
    //  res = res * (1 + 4.0 * M_p/GeV * M_p/GeV * x * x / Q2) - 1.0;
    //  if(TMath::IsNaN(res) ) return 0.0;
    //  return res;
    //}
    ////______________________________________________________________________________

    //double StructureFunctions::Rnp(double x, double Q2) {
    //  double F2_n = this->F2n(x,Q2);
    //  double F2_p = this->F2p(x,Q2);
    //  return(F2_n/F2_p);
    //}
    ////______________________________________________________________________________
    //
    //double StructureFunctions::Rnp_Error(double x, double Q2){
    //  double F2_n  = F2n(x,Q2);
    //  double F2_p  = F2p(x,Q2);
    //  double eF2_n = F2n_Error(x,Q2);
    //  double eF2_p = F2p_Error(x,Q2);
    //  double R_np  = F2_n/F2_p;
    //  // Here we assume zero covariance term in the unc. prop.
    //  double eR_np = TMath::Sqrt((eF2_n*eF2_n)/(F2_n*F2_n) + (eF2_p*eF2_p)/(F2_p*F2_p));
    //  return(R_np*eR_np);
    //}
    ////______________________________________________________________________________
    //
    //double StructureFunctions::W1p(double x, double Qsq) { return(F1p(x, Qsq) / (M_p/GeV)); }
    //double StructureFunctions::W2p(double x, double Qsq) { return(F2p(x, Qsq) / (Qsq / (2.0 * (M_p/GeV) * x))); }
    //double StructureFunctions::W1n(double x, double Qsq) { return(F1n(x, Qsq) / (M_n/GeV)); }
    //double StructureFunctions::W2n(double x, double Qsq) { return(F2n(x, Qsq) / (Qsq / (2.0 * (M_n/GeV) * x))); }
    ////______________________________________________________________________________
    //
    //// Returns F2 per nucleus (not per nucleon)
    //double StructureFunctions::F2Nuclear(double x,double Qsq,double Z, double A){
    //  Error("F2Nuclear","Not implemented");
    //  return 0.0; 
    //}
    //double StructureFunctions::xF2Nuclear(double x, double Qsq,double Z, double A) {
    //  return(x * F2Nuclear(x, Qsq,Z,A));
    //}
    //double StructureFunctions::F1Nuclear(double x,double Qsq,double Z, double A){
    //  Error("F1Nuclear","Not implemented");
    //  return 0.0; 
    //}
    //double StructureFunctions::xF1Nuclear(double x, double Qsq,double Z, double A) {
    //  return(x * F1Nuclear(x, Qsq,Z,A));
    //}
    ////______________________________________________________________________________
    
    //void StructureFunctions::GetValues(TObject *obj, double Q2, StructureFunctionBase::UnpolarizedSFType q){
    //  // Fills histogram with values.
    //  //  For error band use GetErrorBand
    //  //if ( !(obj->InheritsFrom(TH1::Class())) ) {
    //  //   Error("GetErrorBand","Not a TH1 class");
    //  //   return;
    //  //}
    //  if(!obj) {
    //    return;
    //  }
    //  //  returns errorsband
    //  auto *hfit = (TH1*)obj;
    //  Int_t hxfirst = hfit->GetXaxis()->GetFirst();
    //  Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
    //  Int_t hyfirst = hfit->GetYaxis()->GetFirst();
    //  Int_t hylast  = hfit->GetYaxis()->GetLast(); 
    //  Int_t hzfirst = hfit->GetZaxis()->GetFirst();
    //  Int_t hzlast  = hfit->GetZaxis()->GetLast(); 

    //  TAxis *xaxis  = hfit->GetXaxis();
    //  TAxis *yaxis  = hfit->GetYaxis();
    //  TAxis *zaxis  = hfit->GetZaxis();

    //  double x[3];

    //  for (Int_t binz=hzfirst; binz<=hzlast; binz++){
    //    x[2]=zaxis->GetBinCenter(binz);
    //    for (Int_t biny=hyfirst; biny<=hylast; biny++) {
    //      x[1]=yaxis->GetBinCenter(biny);
    //      for (Int_t binx=hxfirst; binx<=hxlast; binx++) {
    //        x[0]=xaxis->GetBinCenter(binx);

    //        hfit->SetBinContent(binx, biny, binz, this->Rnp(x[0],Q2));
    //        //GetPDFErrors(x[0], Q2);
    //        //         hfit->SetBinError(binx, biny, binz, x[0]*fPDFErrors[q]);
    //      }
    //    }
    //  }

    //}
    ////_____________________________________________________________________________
    //
    //void StructureFunctions::GetErrorBand(TObject *obj, double Q2, StructureFunctionBase::UnpolarizedSFType q){
    //  //if ( !(obj->InheritsFrom(TH1::Class())) ) {
    //  //   Error("GetErrorBand","Not a TH1 class");
    //  //   return;
    //  //}
    //  if(!obj) {
    //    return;
    //  }
    //  //  returns errorsband
    //  auto *hfit = (TH1*)obj;
    //  Int_t hxfirst = hfit->GetXaxis()->GetFirst();
    //  Int_t hxlast  = hfit->GetXaxis()->GetLast(); 
    //  Int_t hyfirst = hfit->GetYaxis()->GetFirst();
    //  Int_t hylast  = hfit->GetYaxis()->GetLast(); 
    //  Int_t hzfirst = hfit->GetZaxis()->GetFirst();
    //  Int_t hzlast  = hfit->GetZaxis()->GetLast(); 

    //  TAxis *xaxis  = hfit->GetXaxis();
    //  TAxis *yaxis  = hfit->GetYaxis();
    //  TAxis *zaxis  = hfit->GetZaxis();

    //  double x[3];


    //  for (Int_t binz=hzfirst; binz<=hzlast; binz++){
    //    x[2]=zaxis->GetBinCenter(binz);
    //    for (Int_t biny=hyfirst; biny<=hylast; biny++) {
    //      x[1]=yaxis->GetBinCenter(biny);
    //      for (Int_t binx=hxfirst; binx<=hxlast; binx++) {
    //        x[0]=xaxis->GetBinCenter(binx);

    //        hfit->SetBinContent(binx, biny, binz, this->Rnp(x[0],Q2));
    //        double err = x[0]*this->Rnp_Error(x[0],Q2);
    //        if( err == 0.0 )  err = 1.0e-8;
    //        hfit->SetBinError(binx, biny, binz, err);
    //      }
    //    }
    //  }

    //}
    //______________________________________________________________________________

    StructureFunctions::StructureFunctions()
    { }
    //______________________________________________________________________________

    StructureFunctions::~StructureFunctions()
    { }
    //______________________________________________________________________________
 
    double StructureFunctions::Get(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t, OPELimit l) const {
    //double StructureFunctions2::Get(std::tuple<SF,Nuclei> sf, double x, double Q2) const {
      auto sftype = std::get<SF>(sf);
      auto target = std::get<Nuclei>(sf);

      double result = 0.0;
      switch(sftype) {

        case SF::F1 :
          if( l == OPELimit::Massless) {
            result = F1(x, Q2, target,t); 
          } else {
            result = F1_TMC(x, Q2, target,t); 
          }
          break;

        case SF::F2 :
          if( l == OPELimit::Massless) {
            result = F2(x, Q2, target,t); 
          } else {
            result = F2_TMC(x, Q2, target,t); 
          }
          break;
      }
      return result;
    }
    //______________________________________________________________________________

    double StructureFunctions::F1(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return F1p(x,Q2);
          break;

        case Nuclei::n : 
          return F1n(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double StructureFunctions::F2(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return F2p(x,Q2);
          break;

        case Nuclei::n : 
          return F2n(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________
    
    double StructureFunctions::F1_TMC(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return F1p_TMC(x,Q2);
          break;

        case Nuclei::n : 
          return F1n_TMC(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double StructureFunctions::F2_TMC(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return F2p_TMC(x,Q2);
          break;

        case Nuclei::n : 
          return F2n_TMC(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double StructureFunctions::FL(double x, double Q2, Nuclei target, Twist t) const
    {
      // F_L = \rho^2 F_2 - 2x F_1) = 2x F_1 R 
      double r2  = insane::kinematics::rho2(x,Q2);
      double res = r2*F2_TMC(x,Q2,target,t)-2.0*x*F1_TMC(x,Q2,target,t);
      return res;
    }
    //______________________________________________________________________________

    double StructureFunctions::R(double x, double Q2, Nuclei target, Twist t) const
    {
      double r2  = insane::kinematics::rho2(x,Q2);
      double F_1 = F1_TMC(x,Q2,target,t);
      double F_L = r2*F2_TMC(x,Q2,target,t)-2.0*x*F_1;
      double res = F_L/(2.0*x*F_1);
      return res;
    }
    //______________________________________________________________________________
    
    double StructureFunctions::xF1p(   double x, double Q2) const { return(x*F1p(x, Q2)); }
    double StructureFunctions::xF2p(   double x, double Q2) const { return(x*F2p(x, Q2)); }
    double StructureFunctions::xF1n(   double x, double Q2) const { return(x*F1n(x, Q2)); }
    double StructureFunctions::xF2n(   double x, double Q2) const { return(x*F2p(x, Q2)); }
    double StructureFunctions::xF1d(   double x, double Q2) const { return(/*x*F1d(x, Q2)*/0.0); }
    double StructureFunctions::xF2d(   double x, double Q2) const { return(/*x*F2d(x, Q2)*/0.0); }
    double StructureFunctions::xF1He3( double x, double Q2) const { return(/*x*F1He3(x, Q2)*/0.0); }
    double StructureFunctions::xF2He3( double x, double Q2) const { return(/*x*F2He3(x, Q2)*/0.0); }
    //______________________________________________________________________________

  }
}
