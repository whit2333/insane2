#include "insane/structurefunctions/F1F209StructureFunctions.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
F1F209StructureFunctions::F1F209StructureFunctions(){
   //SetNameTitle("F1F209StructureFunctions","F1F209 Structure Functions");
   SetLabel("F1F209");
   fEs              = 0;  
   //fF1              = 0;
   //fF2              = 0;
   //fM               = 0;
   //fR               = 0; 
   //fA               = 1;
   //fZ               = 1;
   fIsFull          = true;
   fIsQEOnly        = false; 
   fIsInelasticOnly = false; 
   fType = 'n'; // deprecated 
   fModifiedVersion = false;
}
//______________________________________________________________________________
F1F209StructureFunctions::~F1F209StructureFunctions(){

}
//______________________________________________________________________________
double F1F209StructureFunctions::F2Nuclear(double x,double Qsq,double Z, double A){
   double res = GetNuclearSF(x,Qsq,Z,A,2);
   return res; 
}
//______________________________________________________________________________
double F1F209StructureFunctions::F1Nuclear(double x,double Qsq,double Z, double A){
   double res = GetNuclearSF(x,Qsq,Z,A,1);
   return res; 
}
//______________________________________________________________________________
double F1F209StructureFunctions::GetNuclearSF(double x, double Qsq, double z, double a, Int_t i ){ 
   // Used for Z>1 AND A>1 , i.e., not a proton or a neutron !
   double A    = a;
   double Z    = z;
   auto E    = (double)fEs; 
   auto XBj  = (double)x; 
   auto Q2   = (double)Qsq;
   double M    = (double)M_p/GeV; 
   double W2   = M*M + (Q2/XBj) - Q2;
   double R    = 0;
   double F1=0,F2=0;
   double F1in=0,F2in=0;
   double F1qe=0,F2qe=0;

   // quasi-elastic terms 
   f1f2qe09_(&Z,&A,&Q2,&W2,&F1qe,&F2qe);

   // inelastic terms 
   //if(fType=='n'){
   if( !fModifiedVersion ){
      //f1f2qe09_(&Z,&A,&Q2,&W2,&F1qe,&F2qe);
      f1f2in09_(&Z,&A,&Q2,&W2,&F1in,&F2in,&R);
   } else if(fModifiedVersion){
      // not sure what Es is? -whit
      // Please document what this is.
      if(fEs>0){
         //f1f2qe09_(&Z,&A,&Q2,&W2,&F1qe,&F2qe);
         f1f2in09_mod_(&Z,&A,&E,&Q2,&W2,&F1in,&F2in,&R);
      }else{
         std::cout << "  Es <= 0!  Check Es settings.  Exiting..." << std::endl;
         //exit(1); // really exit? why not return zero with the warning? -whit?
         return 0;
      }
   }

   if(fIsFull){
      F1 = F1qe+F1in;
      F2 = F2qe+F2in;
      R  = R;
   }else if(fIsQEOnly){
      F1 = F1qe;
      F2 = F2qe;
      R  = 0.0;
   }else if(fIsInelasticOnly){
      F1 = F1in;
      F2 = F2in;
      R  = R;
   }
   if(i==0) return R;
   if(i==1) return F1;
   /*else*/ return F2;
}
//______________________________________________________________________________
double F1F209StructureFunctions::R(double x,double Qsq){
   double A    = 1;
   double Z    = 1;
   auto E    = (double)fEs; 
   auto XBj  = (double)x; 
   auto Q2   = (double)Qsq;
   double M    = (double)M_p/GeV; 
   double W2   = M*M + (Q2/XBj) - Q2;
   double R    = 0;
   double F1in=0,F2in=0;
   f1f2in09_(&Z,&A,&Q2,&W2,&F1in,&F2in,&R);
   return R; 
}
//______________________________________________________________________________
double F1F209StructureFunctions::F1p(double x,double Qsq) {
   double A    = 1;
   double Z    = 1;
   auto E    = (double)fEs; 
   auto XBj  = (double)x; 
   auto Q2   = (double)Qsq;
   double M    = (double)M_p/GeV; 
   double W2   = M*M + (Q2/XBj) - Q2;
   double R    = 0;
   double F1in=0,F2in=0;
   f1f2in09_(&Z,&A,&Q2,&W2,&F1in,&F2in,&R);
   return(F1in);
}
//______________________________________________________________________________
double F1F209StructureFunctions::F2p(double x, double Qsq) {
   double A    = 1;
   double Z    = 1;
   auto E    = (double)fEs; 
   auto XBj  = (double)x; 
   auto Q2   = (double)Qsq;
   double M    = (double)M_p/GeV; 
   double W2   = M*M + (Q2/XBj) - Q2;
   double R    = 0;
   double F1in=0,F2in=0;
   f1f2in09_(&Z,&A,&Q2,&W2,&F1in,&F2in,&R);
   return(F2in);
}
//______________________________________________________________________________
double F1F209StructureFunctions::F1n(double x, double Qsq) {
   double A    = 1;
   double Z    = 0;
   auto E    = (double)fEs; 
   auto XBj  = (double)x; 
   auto Q2   = (double)Qsq;
   double M    = (double)M_p/GeV; 
   double W2   = M*M + (Q2/XBj) - Q2;
   double R    = 0;
   double F1in=0,F2in=0;
   f1f2in09_(&Z,&A,&Q2,&W2,&F1in,&F2in,&R);
   return(F1in);
}
//______________________________________________________________________________
double F1F209StructureFunctions::F2n(double x, double Qsq) {
   double A    = 1;
   double Z    = 0;
   auto E    = (double)fEs; 
   auto XBj  = (double)x; 
   auto Q2   = (double)Qsq;
   double M    = (double)M_p/GeV; 
   double W2   = M*M + (Q2/XBj) - Q2;
   double R    = 0;
   double F1in=0,F2in=0;
   f1f2in09_(&Z,&A,&Q2,&W2,&F1in,&F2in,&R);
   return(F2in);
}
//______________________________________________________________________________
double F1F209StructureFunctions::F1d(double x, double Qsq) {
   double A    = 2;
   double Z    = 1;
   return GetNuclearSF(x,Qsq,Z,A,1)/2.0;
}
//______________________________________________________________________________
double F1F209StructureFunctions::F2d(double x, double Qsq) {
   double A    = 2;
   double Z    = 1;
   return GetNuclearSF(x,Qsq,Z,A,2)/2.0;
}
//______________________________________________________________________________
double F1F209StructureFunctions::F1He3(double x, double Qsq) {
   double A    = 3;
   double Z    = 2;
   return GetNuclearSF(x,Qsq,Z,A,1)/3.0;
}
//______________________________________________________________________________
double F1F209StructureFunctions::F2He3(double x, double Qsq) {
   double A    = 3;
   double Z    = 2;
   return GetNuclearSF(x,Qsq,Z,A,2)/3.0;
}
//______________________________________________________________________________




//______________________________________________________________________________
F1F209QuasiElasticStructureFunctions::F1F209QuasiElasticStructureFunctions(){
   fEs   = 0;  
   fF1   = 0;
   fF2   = 0;
   fM    = 0;
   fR    = 0; 
   fA    = 0;
   fZ    = 0;
   fType = 'n'; // deprecated 
}
//______________________________________________________________________________
F1F209QuasiElasticStructureFunctions::~F1F209QuasiElasticStructureFunctions(){
}
//______________________________________________________________________________
double F1F209QuasiElasticStructureFunctions::F2Nuclear(double x,double Qsq,double Z, double A){
   fZ = Z;
   fA = A;
   GetSFs(x,Qsq);
   return fF2; 
}
//______________________________________________________________________________
double F1F209QuasiElasticStructureFunctions::F1Nuclear(double x,double Qsq,double Z, double A){
   fZ = Z;
   fA = A;
   GetSFs(x,Qsq);
   return fF1; 
}
//______________________________________________________________________________
void F1F209QuasiElasticStructureFunctions::GetSFs(double x, double Qsq) {

   fN          = fA-fZ;
   double A    = fA;
   double Z    = fZ;
   auto E    = (double)fEs; 
   auto XBj  = (double)x; 
   auto Q2   = (double)Qsq;
   double M    = (double)M_p/GeV; 
   double W2   = M*M + (Q2/XBj) - Q2;
   double R    = 0;
   double F1qe = 0;
   double F2qe = 0;

   f1f2qe09_(&Z,&A,&Q2,&W2,&F1qe,&F2qe);

   fR  = 0;
   fF1 = double(F1qe);
   fF2 = double(F2qe);

}
//______________________________________________________________________________
double F1F209QuasiElasticStructureFunctions::F1p(double x,double Qsq) {
   return 0.0;
   //fA = 1.0;
   //fZ = 1.0;
   //GetSFs(x,Qsq);
   //return(fF1);
}
//______________________________________________________________________________
double F1F209QuasiElasticStructureFunctions::F2p(double x, double Qsq) {
   return 0.0;
   //fA = 1.0;
   //fZ = 1.0;
   //GetSFs(x,Qsq);
   //return(fF2);
}
//______________________________________________________________________________
double F1F209QuasiElasticStructureFunctions::F1n(double x, double Qsq) {
   return 0.0;
   //fA = 1.0;
   //fZ = 0.0;
   //GetSFs(x,Qsq);
   //return(fF1);
}
//______________________________________________________________________________
double F1F209QuasiElasticStructureFunctions::F2n(double x, double Qsq) {
   return 0.0;
   //fA = 1.0;
   //fZ = 0.0;
   //GetSFs(x,Qsq);
   //return(fF2);
}
//______________________________________________________________________________
double F1F209QuasiElasticStructureFunctions::F1d(double x, double Qsq) {
   fA = 2.0;
   fZ = 1.0;
   GetSFs(x,Qsq);
   return(fF1/2.0); 
}
//______________________________________________________________________________
double F1F209QuasiElasticStructureFunctions::F2d(double x, double Qsq) {
   fA = 2.0;
   fZ = 1.0;
   GetSFs(x,Qsq);
   return(fF2/2.0);
}
//______________________________________________________________________________
double F1F209QuasiElasticStructureFunctions::F1He3(double x, double Qsq) {
   fA = 3.0;
   fZ = 2.0;
   GetSFs(x,Qsq);
   return(fF1/3.0);
}
//______________________________________________________________________________
double F1F209QuasiElasticStructureFunctions::F2He3(double x, double Qsq) {
   fA = 3.0;
   fZ = 2.0;
   GetSFs(x,Qsq);
   return(fF2/3.0); 
}
//______________________________________________________________________________

//StructureFunctions * StructureFunctions::fgStructureFunctions = new F1F209StructureFunctions();

}}
