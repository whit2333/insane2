#include "insane/structurefunctions/fwd_SFs.h"

namespace insane {
  namespace physics {


    template class insane::physics::SFsFromPDFs<insane::physics::CTEQ10_UPDFs>;
    template class insane::physics::SFsFromPDFs<insane::physics::Stat2015_UPDFs>;
    //template class insane::physics::SFsFromVPACs<insane::physics::MAID_VPACs>;

    template class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs>;
    template class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs>;
    template class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs,insane::physics::JAM_T4DFs>;

    template class  insane::physics::SSFsFromPDFs<insane::physics::Stat2015_PPDFs>;
    template class  insane::physics::SSFsFromPDFs<insane::physics::Stat2015_PPDFs,insane::physics::JAM_T3DFs>;

    template class insane::physics::SSFsFromPDFs<insane::physics::AAC08_PPDFs>;
    template class insane::physics::SSFsFromPDFs<insane::physics::AAC08_PPDFs,insane::physics::JAM_T3DFs>;
    
    template class insane::physics::SSFsFromPDFs<insane::physics::BB_PPDFs>;
    template class insane::physics::SSFsFromPDFs<insane::physics::BB_PPDFs,insane::physics::JAM_T3DFs>;

    template class insane::physics::SSFsFromPDFs<insane::physics::DSSV_PPDFs>;
    template class insane::physics::SSFsFromPDFs<insane::physics::DSSV_PPDFs,insane::physics::JAM_T3DFs>;

    template class insane::physics::SSFsFromPDFs<insane::physics::BBS_PPDFs>;
    template class insane::physics::SSFsFromPDFs<insane::physics::BBS_PPDFs,insane::physics::JAM_T3DFs>;

    template class insane::physics::SSFsFromPDFs<insane::physics::GS_PPDFs>;
    template class insane::physics::SSFsFromPDFs<insane::physics::GS_PPDFs,insane::physics::JAM_T3DFs>;

    template class insane::physics::SSFsFromPDFs<insane::physics::LSS2010_PPDFs>;
    template class insane::physics::SSFsFromPDFs<insane::physics::LSS2010_PPDFs,insane::physics::JAM_T3DFs>;

    template class insane::physics::SSFsFromPDFs<insane::physics::LSS2006_PPDFs>;
    template class insane::physics::SSFsFromPDFs<insane::physics::LSS2006_PPDFs,insane::physics::JAM_T3DFs>;

    template class insane::physics::SSFsFromPDFs<insane::physics::DNS2005_PPDFs>;
    template class insane::physics::SSFsFromPDFs<insane::physics::DNS2005_PPDFs,insane::physics::JAM_T3DFs>;

    template class insane::physics::SSFsFromPDFs<insane::physics::ABDY_PPDFs>;
    template class insane::physics::SSFsFromPDFs<insane::physics::ABDY_PPDFs,insane::physics::JAM_T3DFs>;

    //template class insane::physics::SSFsFromVPACs<insane::physics::MAID_VPACs>;
  }
}
