#include "insane/structurefunctions/CompositeSSFs.h"
#include "insane/structurefunctions/SSFsFromVPACs.h"
#include "insane/structurefunctions/SSFsFromPDFs.h"
#include "insane/pdfs/Stat2015_PPDFs.h"

namespace insane {
  namespace physics {

    struct CompositeSSFs::SSFs_impl {
        SSFsFromPDFs<Stat2015_PPDFs>   fAsf;
        SSFsFromPDFs<Stat2015_PPDFs>   fBsf;
    };

    CompositeSSFs::CompositeSSFs()
    { fSFs = new SSFs_impl(); }
    //______________________________________________________________________________

    CompositeSSFs::~CompositeSSFs()
    { }
    //______________________________________________________________________________

    double CompositeSSFs::g2p(double x, double Qsq) const
    {
      auto& fAsf = (fSFs->fAsf);
      auto& fBsf = (fSFs->fAsf);
      double res = 0.0;

      double w = GetWeight(0,x,Qsq);
      if( w > 0.0 ) {
        res        +=  w*fAsf.g2p(x,Qsq);
      }
      w =  GetWeight(1,x,Qsq);
      if( w > 0.0 ) {
        res        +=  w*fBsf.g2p(x,Qsq);
      }
      return( res );
    }
    //______________________________________________________________________________
    
    double CompositeSSFs::g1p(double x, double Qsq) const
    {
      auto& fAsf = (fSFs->fAsf);
      auto& fBsf = (fSFs->fAsf);
      double res = 0.0;

      double w = GetWeight(0,x,Qsq);
      if( w > 0.0 ) {
        res        +=  w*fAsf.g1p(x,Qsq);
      }
      w =  GetWeight(1,x,Qsq);
      if( w > 0.0 ) {
        res        +=  w*fBsf.g1p(x,Qsq);
      }
      return( res );
      //InSANEStructureFunctions * fAsf = (InSANEStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
      //return(fAsf.g1p(x,Qsq) );
    }
    //______________________________________________________________________________

    double CompositeSSFs::g2n(double x, double Qsq) const
    {
      auto& fAsf = (fSFs->fAsf);
      auto& fBsf = (fSFs->fAsf);
      // Proton
      double res = GetWeight(0,x,Qsq)*fAsf.g2n(x,Qsq);
      res        +=  GetWeight(1,x,Qsq)*fBsf.g2n(x,Qsq);
      return( res );
      //InSANEStructureFunctions * fAsf = (InSANEStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
      //return(fAsf.g2n(x,Qsq) );
    }
    //______________________________________________________________________________
    
    double CompositeSSFs::g1n(double x, double Qsq) const
    {
      auto& fAsf = (fSFs->fAsf);
      auto& fBsf = (fSFs->fAsf);
      double res  = GetWeight(0,x,Qsq)*fAsf.g1n(x,Qsq);
      res        +=  GetWeight(1,x,Qsq)*fBsf.g1n(x,Qsq);
      return( res );
      //InSANEStructureFunctions * fAsf = (InSANEStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
      //return(fAsf.g1n(x,Qsq) );
    }
    //______________________________________________________________________________

    double CompositeSSFs::GetWeight(int iSF, double x, double Q2) const
    {
      // MAID is good for W<1.8 GeV and Q2<5 GeV^2
      // There are 3 overlap regions where the weights change for a smooth transfer
      // The width in W is dW and for Q2 dQ2.

      double W   = insane::kine::W_xQsq(x,Q2);
      double dQ2 = 0.5;
      double dW  = 0.4;
      double W0  = 1.4;
      double Q20 = 3.5;
      return 1.0;

      if( (W < W0 - dW) && (Q2 < Q20 - dQ2) ) {
        if( iSF == 0 ) { // DIS
          return 0.0;
        } else if( iSF == 1 ) { // res
          return 1.0;
        }
      }

      if( (W >= W0 ) || (Q2 >= Q20 ) ) {
        // not g1g209 
        if( iSF == 0 ) { // DIS
          return 1.0;
        } else if( iSF == 1 ) { // g1g209
          return 0.0;
        }
      }

      // Over lap in W
      if( ( TMath::Abs(W - W0 + dW/2.0) <= dW/2.0 ) && (Q2 < Q20 - dQ2) ) {
        double W1 = (W - W0 + dW)/dW;
        if( iSF == 0 ) {
          return(W1);
        } else if( iSF == 1 ) {
          return(1.0 - W1);
        }

      }
      // Overlap in Q2 
      if( ( TMath::Abs(Q2 - Q20 + dQ2/2.0) <= dQ2/2.0 ) && (W < W0 - dW) ) {
        double W1 = (Q2 - Q20 + dQ2)/dQ2;
        if( iSF == 0 ) {
          return(W1);
        } else if( iSF == 1 ) {
          return(1.0 - W1);
        }

      }
      // Overlap in Q2 and W  
      if( ( TMath::Abs(W - W0 + dW/2.0) <= dW/2.0 ) && ( TMath::Abs(Q2 - Q20 + dQ2/2.0) <= dQ2/2.0 ) ) {
        double x1 = (Q2 - Q20 + dQ2)/dQ2;
        double y1 = (W - W0 + dW)/dW;
        double W1 = x1 + y1 - x1*y1;
        if( iSF == 0 ) {
          return(W1);
        } else if( iSF == 1 ) {
          return(1.0 - W1);
        }

      }

      std::cout << "W  " << W <<std::endl;
      std::cout << "Q2 " << Q2 <<std::endl;
      Error("GetWeight","Should not have made it here");
      return 0.0;
    }
    //______________________________________________________________________________

    //double CompositeSSFs::R(double x, double Q2, Nuclei target, Twist t) const
    //{
    //  double res  = GetWeight(0,x,Q2)*fAsf.R(x,Q2,target,t);
    //  res        += GetWeight(1,x,Q2)*fBsf.R(x,Q2,target,t);
    //  return( res );
    //}

  }
}

