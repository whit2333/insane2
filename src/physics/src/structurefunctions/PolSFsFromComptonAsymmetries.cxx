#include "insane/asymmetries/PolSFsFromComptonAsymmetries.h"
#include "insane/base/Physics.h"

namespace insane {
  namespace physics {
    PolSFsFromComptonAsymmetries::PolSFsFromComptonAsymmetries()
    {
      fSFs = new DefaultStructureFunctions();
    }
    //__________________________________________________________________________
    //
    PolSFsFromComptonAsymmetries::~PolSFsFromComptonAsymmetries()
    {}
  }
}
