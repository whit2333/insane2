#include "insane/structurefunctions/NMC95_SFs.h"
#include "insane/pdfs/FortranWrappers.h"
#include "insane/kinematics/KinematicFunctions.h"

namespace insane {
  namespace physics {

    NMC95_SFs::NMC95_SFs()
    { 
      SetLabel("NMC95");
    }
    //______________________________________________________________________________

    NMC95_SFs::~NMC95_SFs()
    { }
    //______________________________________________________________________________

    //void NMC95_SFs::GetSFs(Int_t part,double x, double Qsq) {
    //  //double N          = fA;         
    //  double XBj  = (double)x; 
    //  double Q2   = (double)Qsq;

    //  switch(part){
    //    case 0: // proton 
    //      fF1 = f1psfun_(&XBj,&Q2); 
    //      fF2 = f2psfun_(&XBj,&Q2); 
    //      break; 
    //    case 1: // neutron
    //      fF1 = f1nsfun_(&XBj,&Q2); 
    //      fF2 = f2nsfun_(&XBj,&Q2); 
    //      break; 
    //    case 2: // deuteron (per nucelon) 
    //      fF1 = f1dsfun_(&XBj,&Q2); 
    //      fF2 = f2dsfun_(&XBj,&Q2); 
    //      break;             
    //    case 3: // 3He (per nucleon)  
    //      fF1 = f1hesfun_(&XBj,&Q2); 
    //      fF2 = f2hesfun_(&XBj,&Q2); 
    //      break;  
    //    default:
    //      std::cout << "[NMC95StructureFunctions::GetSFs]: "; 
    //      std::cout << "Invalid target!  Exiting..." << std::endl;
    //      break;
    //  }

    //}
    ////______________________________________________________________________________

    double NMC95_SFs::F1p(double x,double Qsq) const
    {
      auto XBj  = (double)x; 
      auto Q2   = (double)Qsq;
      double res  = f1psfun_(&XBj,&Q2); 
      return res;
    }
    //______________________________________________________________________________

    double NMC95_SFs::F2p(double x,double Qsq) const
    {
      auto XBj  = (double)x; 
      auto Q2   = (double)Qsq;
      double res  = f2psfun_(&XBj,&Q2); 
      return res;
    }
    //______________________________________________________________________________

    double NMC95_SFs::F1n(double x,double Qsq) const
    {
      auto XBj  = (double)x; 
      auto Q2   = (double)Qsq;
      double res  = f1nsfun_(&XBj,&Q2); 
      return res;
    }
    //______________________________________________________________________________

    double NMC95_SFs::F2n(double x,double Qsq) const
    {
      auto XBj  = (double)x; 
      auto Q2   = (double)Qsq;
      double res  = f2nsfun_(&XBj,&Q2); 
      return res;
    }
    //______________________________________________________________________________

    double NMC95_SFs::R(double x, double Q2, Nuclei target, Twist t) const
    {
      return insane::physics::R1998(x,Q2); 
    }

  }
}
