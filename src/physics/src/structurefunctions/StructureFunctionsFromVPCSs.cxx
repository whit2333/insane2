#include "insane/structurefunctions/StructureFunctionsFromVPCSs.h"
#include "TMath.h"
#include "insane/base/PhysicalConstants.h"
//#include "MAIDPhotoAbsorptionCrossSections.h"
#include "insane/kinematics/KinematicFunctions.h"
namespace insane {
namespace physics {

//______________________________________________________________________________
StructureFunctionsFromVPCSs::StructureFunctionsFromVPCSs()
{
   SetLabel("MAID07");
   SetNameTitle("SFs from MAID07","SFs from MAID07");
   //fVPCSs = new MAIDPhotoAbsorptionCrossSections();   // Virtual Photoabsorption Cross Sections
   f4piAlphaOverM = 4.0*TMath::Pi()*TMath::Pi()*fine_structure_const/(M_p/GeV);
}
//______________________________________________________________________________
StructureFunctionsFromVPCSs::~StructureFunctionsFromVPCSs()
{ }
//______________________________________________________________________________
double StructureFunctionsFromVPCSs::F1p(   double x, double Q2){
   if(!fVPCSs) {
      return 0.0;
   }
   fVPCSs->CalculateProton(x,Q2);
   double M     = M_p/GeV;
   double K     = insane::kine::K_Hand(x,Q2);
   double sig_T = fVPCSs->Sig_T();
   double res   = sig_T*K/f4piAlphaOverM;
   return res;
}
//______________________________________________________________________________
double StructureFunctionsFromVPCSs::F2p(   double x, double Q2){
   if(!fVPCSs) {
      return 0.0;
   }
   fVPCSs->CalculateProton(x,Q2);
   double M     = M_p/GeV;
   double nu    = Q2/(2.0*M*x);
   double K     = insane::kine::K_Hand(x,Q2);
   double B1    = (f4piAlphaOverM/(K*nu))*(1.0+nu*nu/Q2) ;
   double sig_T = fVPCSs->Sig_T();
   double sig_L = fVPCSs->Sig_L();
   double res   = (sig_L + sig_T)/B1;
   return res;
}
//______________________________________________________________________________
double StructureFunctionsFromVPCSs::F1n(   double x, double Q2){
   return 0.0;
}
//______________________________________________________________________________
double StructureFunctionsFromVPCSs::F2n(   double x, double Q2){
   return 0.0;
}
//______________________________________________________________________________
double StructureFunctionsFromVPCSs::F1d(   double x, double Q2){
   return 0.0;
}
//______________________________________________________________________________
double StructureFunctionsFromVPCSs::F2d(   double x, double Q2){
   return 0.0;
}
//______________________________________________________________________________
double StructureFunctionsFromVPCSs::F1He3( double x, double Q2){
   return 0.0;
}
//______________________________________________________________________________
double StructureFunctionsFromVPCSs::F2He3( double x, double Q2){
   return 0.0;
}
//______________________________________________________________________________
}}
