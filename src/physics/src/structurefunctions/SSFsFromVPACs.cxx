#include "insane/structurefunctions/SSFsFromVPACs.h"

//namespace insane {
//  namespace physics {
//    //______________________________________________________________________________
//    SSFsFromVPACs::SSFsFromVPACs()
//    {
//      SetLabel("MAID07");
//      SetNameTitle("Spin SFs from MAID07", "Spin SFs from MAID07");
//      fSFs   = nullptr;
//      fVCSAs = 0;
//    }
//    //______________________________________________________________________________
//    SSFsFromVPACs::~SSFsFromVPACs()
//    {
//    }
//    //______________________________________________________________________________
//
//    double SSFsFromVPACs::g1p(   double x, double Q2)
//    {
//      if(!fVCSAs) return 0.0;
//      double M = (M_p/GeV);
//      double gamma2 = (4.0*M*M*x*x)/Q2;
//      double F1 =  fSFs->F1p(x,Q2);
//
//      double g1 = (F1/(1.0+gamma2))*(fVCSAs->A1p(x,Q2) + TMath::Sqrt(gamma2)*fVCSAs->A2p(x,Q2));
//      return g1;
//    }
//    //______________________________________________________________________________
//    double SSFsFromVPACs::g1n(   double x, double Q2)
//    {
//      if(!fVCSAs) return 0.0;
//      double M = (M_p/GeV);
//      double gamma2 = (4.0*M*M*x*x)/Q2;
//      double F1 =  fSFs->F1n(x,Q2);
//
//      double g1 = (F1/(1.0+gamma2))*(fVCSAs->A1n(x,Q2) + TMath::Sqrt(gamma2)*fVCSAs->A2n(x,Q2));
//      return g1;
//    }
//    //______________________________________________________________________________
//    double SSFsFromVPACs::g1d(   double x, double Q2)
//    {
//      return 0.0;
//    }
//    //______________________________________________________________________________
//    double SSFsFromVPACs::g1He3( double x, double Q2)
//    {
//      return 0.0;
//    }
//    //______________________________________________________________________________
//    double SSFsFromVPACs::g2p(   double x, double Q2)
//    {
//      if(!fVCSAs) return 0.0;
//      double M = (M_p/GeV);
//      double gamma2 = (4.0*M*M*x*x)/Q2;
//      double F1 =  fSFs->F1p(x,Q2);
//      double g2 = (F1/(1.0+gamma2))*(fVCSAs->A2p(x,Q2)/TMath::Sqrt(gamma2) - fVCSAs->A1p(x,Q2));
//      return g2;
//    }
//    //______________________________________________________________________________
//    double SSFsFromVPACs::g2n(   double x, double Q2)
//    {
//      return 0.0;
//    }
//    //______________________________________________________________________________
//    double SSFsFromVPACs::g2d(   double x, double Q2)
//    {
//      return 0.0;
//    }
//    double SSFsFromVPACs::g2He3( double x, double Q2)
//    {
//      return 0.0;
//    }
//
//  }
//}
