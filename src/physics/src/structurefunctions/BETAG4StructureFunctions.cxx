#include "insane/structurefunctions/BETAG4StructureFunctions.h"


namespace insane {
namespace physics {
BETAG4StructureFunctions::BETAG4StructureFunctions()
{


}

BETAG4StructureFunctions::~BETAG4StructureFunctions()
{



}

double BETAG4StructureFunctions::F1p(double x, double Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}

double BETAG4StructureFunctions::F2p(double x, double Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}

double BETAG4StructureFunctions::F1n(double x, double Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}

double BETAG4StructureFunctions::F2n(double x, double Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}

double BETAG4StructureFunctions::F1d(double x, double Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}

double BETAG4StructureFunctions::F2d(double x, double Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}

double BETAG4StructureFunctions::g1p(double x, double Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}

double BETAG4StructureFunctions::g2p(double x, double Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}
}}
