#include "insane/structurefunctions/CompositePolarizedStructureFunctions.h"
#include "insane/structurefunctions/PolarizedStructureFunctionsFromVCSAs.h"
#include "insane/asymmetries/MAIDVirtualComptonAsymmetries.h"

//____________________________________________________________________________
namespace insane {
namespace physics {
CompositePolarizedStructureFunctions::CompositePolarizedStructureFunctions(){
   fNSFs = 0;
   fSFList.Clear();
}
CompositePolarizedStructureFunctions::~CompositePolarizedStructureFunctions(){}

void   CompositePolarizedStructureFunctions::Add(PolarizedStructureFunctions * sf){ fSFList.Add(sf); fNSFs = fSFList.GetEntries();}

//______________________________________________________________________________
double CompositePolarizedStructureFunctions::g2p(double x, double Qsq){
   auto * asf = (PolarizedStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (PolarizedStructureFunctions *)fSFList.At( 1 );
   double res = 0.0;

   double w = GetWeight(0,x,Qsq);
   if( w > 0.0 ) {
      res        +=  w*asf->g2p(x,Qsq);
   }
   w =  GetWeight(1,x,Qsq);
   if( w > 0.0 ) {
      res        +=  w*bsf->g2p(x,Qsq);
   }
   return( res );
}
//______________________________________________________________________________
double CompositePolarizedStructureFunctions::g1p(double x, double Qsq){
   auto * asf = (PolarizedStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (PolarizedStructureFunctions *)fSFList.At( 1 );
   double res = 0.0;

   double w = GetWeight(0,x,Qsq);
   if( w > 0.0 ) {
      res        +=  w*asf->g1p(x,Qsq);
   }
   w =  GetWeight(1,x,Qsq);
   if( w > 0.0 ) {
      res        +=  w*bsf->g1p(x,Qsq);
   }
   return( res );
   //PolarizedStructureFunctions * asf = (PolarizedStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
   //return(asf->g1p(x,Qsq) );
}
//______________________________________________________________________________
// Proton
double CompositePolarizedStructureFunctions::g2n(double x, double Qsq){
   auto * asf = (PolarizedStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (PolarizedStructureFunctions *)fSFList.At( 1 );
   double res = GetWeight(0,x,Qsq)*asf->g2n(x,Qsq);
   res        +=  GetWeight(1,x,Qsq)*bsf->g2n(x,Qsq);
   return( res );
   //PolarizedStructureFunctions * asf = (PolarizedStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
   //return(asf->g2n(x,Qsq) );
}
//______________________________________________________________________________
double CompositePolarizedStructureFunctions::g1n(double x, double Qsq){
   auto * asf = (PolarizedStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (PolarizedStructureFunctions *)fSFList.At( 1 );
   double res = GetWeight(0,x,Qsq)*asf->g1n(x,Qsq);
   res        +=  GetWeight(1,x,Qsq)*bsf->g1n(x,Qsq);
   return( res );
   //PolarizedStructureFunctions * asf = (PolarizedStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
   //return(asf->g1n(x,Qsq) );
}
//______________________________________________________________________________
// Deuteron
double CompositePolarizedStructureFunctions::g2d(double x, double Qsq){
   auto * asf = (PolarizedStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (PolarizedStructureFunctions *)fSFList.At( 1 );
   double res = GetWeight(0,x,Qsq)*asf->g2d(x,Qsq);
   res        +=  GetWeight(1,x,Qsq)*bsf->g2d(x,Qsq);
   return( res );
   //PolarizedStructureFunctions * asf = (PolarizedStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
   //return(asf->g2d(x,Qsq) );
}
//______________________________________________________________________________
double CompositePolarizedStructureFunctions::g1d(double x, double Qsq){
   auto * asf = (PolarizedStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (PolarizedStructureFunctions *)fSFList.At( 1 );
   double res = GetWeight(0,x,Qsq)*asf->g1d(x,Qsq);
   res        +=  GetWeight(1,x,Qsq)*bsf->g1d(x,Qsq);
   return( res );
   //PolarizedStructureFunctions * asf = (PolarizedStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
   //return(asf->g1d(x,Qsq) );
}
//______________________________________________________________________________
// He3 
double CompositePolarizedStructureFunctions::g2He3(double x, double Qsq){
   auto * asf = (PolarizedStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (PolarizedStructureFunctions *)fSFList.At( 1 );
   double res = GetWeight(0,x,Qsq)*asf->g2He3(x,Qsq);
   res        +=  GetWeight(1,x,Qsq)*bsf->g2He3(x,Qsq);
   return( res );
   //PolarizedStructureFunctions * asf = (PolarizedStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
   //return(asf->g2He3(x,Qsq) );
}
//______________________________________________________________________________
double CompositePolarizedStructureFunctions::g1He3(double x, double Qsq){
   auto * asf = (PolarizedStructureFunctions *)fSFList.At( 0 );
   auto * bsf = (PolarizedStructureFunctions *)fSFList.At( 1 );
   double res = GetWeight(0,x,Qsq)*asf->g1He3(x,Qsq);
   res        +=  GetWeight(1,x,Qsq)*bsf->g1He3(x,Qsq);
   return( res );
   //PolarizedStructureFunctions * asf = (PolarizedStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
   //return(asf->g1He3(x,Qsq) );
}
//______________________________________________________________________________



//______________________________________________________________________________
LowQ2PolarizedStructureFunctions::LowQ2PolarizedStructureFunctions(){

   SetLabel("MAID07 + DSSV");
   SetNameTitle("LowQ2StructureFunctions","Low Q2 Spin SFs MAID07+DSSV");

   xrange[0] = 0.0;
   xrange[1] = 0.368;
   xrange[2] = 1.0;
   xrange[3] = 1.0;
   xrange[4] = 1.0;

   // initialize pdfs and sfs if needed
   auto * maid_sfs = new PolarizedStructureFunctionsFromVCSAs();
   maid_sfs->SetVCSAs( new MAIDVirtualComptonAsymmetries() );

   // \todo rename this method and polarized partner to just SetPDFs
   fAACSFs.SetPolarizedPDFs(&fAACPolarizedPDFs);   
   fDSSVSFs.SetPolarizedPDFs(&fDSSVPolarizedPDFs);   

   // First one is DIS
   //Add(&fAACSFs);
   Add(&fDSSVSFs);

   // Second one is resonance
   Add(maid_sfs);
}
//______________________________________________________________________________
LowQ2PolarizedStructureFunctions::~LowQ2PolarizedStructureFunctions(){}

//______________________________________________________________________________
double LowQ2PolarizedStructureFunctions::GetWeight(Int_t iSF, double x, double Q2){

   // MAID is good for W<1800 GeV and Q2<5 GeV^2
   // There are 3 overlap regions where the weights change for a smooth transfer
   // The width in W is dW and for Q2 dQ2.
   
   double W     = insane::Kine::W_xQsq(x,Q2);
   double dQ2   = 1.0;
   double dW    = 0.2;
   double W0    = 1.6;
   double Q20   = 4.0;

   // TODO
   // Go back to DIS in Dip region W<1079.1 MeV
   //double W0_2  = 1.0792;
   //double dW_2  = 0.05;

   if( (W < W0 - dW) && (Q2 < Q20 - dQ2) ) {
      // resonance 
      if( iSF == 0 ) { // DIS
         return 0.0;
      } else if( iSF == 1 ) { // resonance
         return 1.0;
      }
   }

   if( (W >= W0 ) || (Q2 >= Q20 ) ) {
      // not resonanace 
      if( iSF == 0 ) { // DIS
         return 1.0;
      } else if( iSF == 1 ) { // resonance
         return 0.0;
      }
   }
   
   // Over lap in W
   if( ( TMath::Abs(W - W0 + dW/2.0) <= dW/2.0 ) && (Q2 < Q20 - dQ2) ) {
      double W1 = (W - W0 + dW)/dW;
      if( iSF == 0 ) {
         return(W1);
      } else if( iSF == 1 ) {
         return(1.0 - W1);
      }

   }
   // Overlap in Q2 
   if( ( TMath::Abs(Q2 - Q20 + dQ2/2.0) <= dQ2/2.0 ) && (W < W0 - dW) ) {
      double W1 = (Q2 - Q20 + dQ2)/dQ2;
      if( iSF == 0 ) {
         return(W1);
      } else if( iSF == 1 ) {
         return(1.0 - W1);
      }

   }
   // Overlap in Q2 and W  
   if( ( TMath::Abs(W - W0 + dW/2.0) <= dW/2.0 ) && ( TMath::Abs(Q2 - Q20 + dQ2/2.0) <= dQ2/2.0 ) ) {
      double x1 = (Q2 - Q20 + dQ2)/dQ2;
      double y1 = (W - W0 + dW)/dW;
      double W1 = x1 + y1 - x1*y1;
      if( iSF == 0 ) {
         return(W1);
      } else if( iSF == 1 ) {
         return(1.0 - W1);
      }

   }

   // TODO ADD second transiation
   // Region below the Delta 
   // Over lap in W
   //if( ( TMath::Abs(W - W0_2 + dW_2/2.0) <= dW/2.0 ) && (Q2 < Q20 - dQ2) ) {
   //   double W1 = (W - W0 + dW)/dW;
   //   if( iSF == 0 ) {
   //      return(W1);
   //   } else if( iSF == 1 ) {
   //      return(1.0 - W1);
   //   }

   //}

   std::cout << "W  " << W <<std::endl;
   std::cout << "Q2 " << Q2 <<std::endl;
   Error("GetWeight","Should not have made it here");
   return 0.0;

      
   //if(Q2 > 8.0 ) return 1.0;
   //   if( x > xrange[1] && x < xrange[2] ) {
   //      double weight = 1.0 - (x - xrange[1])/(xrange[2] - xrange[1]);
   //      return weight;
   //   } else if ( x <= xrange[1] ){
   //      return(1.0);
   //   } else {
   //      return(0.0);
   //   }
   //} else if( iSF == 1 ) {
   //   if(Q2 > 8.0 ) return 0.0;
   //   // F1F209
   //   if( x > xrange[1] && x < xrange[2] ) {
   //      double weight = (x - xrange[1])/(xrange[2] - xrange[1]);
   //      return weight;
   //   } else if ( x <= xrange[1] ){
   //      return(0.0);
   //   } else {
   //      return(1.0);
   //   }
   //}
}
//______________________________________________________________________________
}}
