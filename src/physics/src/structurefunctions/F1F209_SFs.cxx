#include "insane/structurefunctions/F1F209_SFs.h"
#include "insane/pdfs/FortranWrappers.h"

namespace insane {
  namespace physics {

    F1F209_SFs::F1F209_SFs()
    { }
    //______________________________________________________________________________

    F1F209_SFs::~F1F209_SFs()
    { }
    //______________________________________________________________________________

    double F1F209_SFs::F1p(double x,double Qsq) const
    {
      double A    = 1;
      double Z    = 1;
      //double E    = (double)fEs; 
      auto XBj  = (double)x; 
      auto Q2   = (double)Qsq;
      double M    = (double)M_p/GeV; 
      double W2   = M*M + (Q2/XBj) - Q2;
      double R    = 0;
      double F1in=0,F2in=0;
      f1f2in09_(&Z,&A,&Q2,&W2,&F1in,&F2in,&R);
      return(F1in);
    }
    //______________________________________________________________________________
   
    double F1F209_SFs::F2p(double x, double Qsq) const 
    {
      double A    = 1;
      double Z    = 1;
      //double E    = (double)fEs; 
      auto XBj  = (double)x; 
      auto Q2   = (double)Qsq;
      double M    = (double)M_p/GeV; 
      double W2   = M*M + (Q2/XBj) - Q2;
      double R    = 0;
      double F1in=0,F2in=0;
      f1f2in09_(&Z,&A,&Q2,&W2,&F1in,&F2in,&R);
      return(F2in);
    }
    //______________________________________________________________________________
   
    double F1F209_SFs::F1n(double x, double Qsq) const
    {
      double A    = 1;
      double Z    = 0;
      //double E    = (double)fEs; 
      auto XBj  = (double)x; 
      auto Q2   = (double)Qsq;
      double M    = (double)M_p/GeV; 
      double W2   = M*M + (Q2/XBj) - Q2;
      double R    = 0;
      double F1in=0,F2in=0;
      f1f2in09_(&Z,&A,&Q2,&W2,&F1in,&F2in,&R);
      return(F1in);
    }
    //______________________________________________________________________________
    
    double F1F209_SFs::F2n(double x, double Qsq) const
    {
      double A    = 1;
      double Z    = 0;
      //double E    = (double)fEs; 
      auto XBj  = (double)x; 
      auto Q2   = (double)Qsq;
      double M    = (double)M_p/GeV; 
      double W2   = M*M + (Q2/XBj) - Q2;
      double R    = 0;
      double F1in=0,F2in=0;
      f1f2in09_(&Z,&A,&Q2,&W2,&F1in,&F2in,&R);
      return(F2in);
    }
    //______________________________________________________________________________

    double F1F209_SFs::R(double x, double Q2, Nuclei target, Twist t) const
    {
      double A    = 1;
      double Z    = 1;
      //double E    = (double)fEs; 
      auto XBj  = (double)x; 
      double M    = (double)M_p/GeV; 
      double W2   = M*M + (Q2/XBj) - Q2;
      double R    = 0;
      double F1in=0,F2in=0;
      f1f2in09_(&Z,&A,&Q2,&W2,&F1in,&F2in,&R);
      return(R);
    }

  }
}
