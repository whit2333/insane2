#include "insane/structurefunctions/SFBase.h"

namespace insane {
  using namespace units;
  namespace physics {

    SFBase::SFBase()
    { }
    //______________________________________________________________________________

    SFBase::~SFBase()
    { }
    //______________________________________________________________________________

    double SFBase::GetXBjorken(std::tuple<SF,Nuclei> sf) const
    {
      auto f  = static_cast<int>(std::get<SF>(sf));
      auto n  = static_cast<int>(std::get<Nuclei>(sf));
      return(fx_values.at(n).at(f));
    }
    //______________________________________________________________________________

    double SFBase::GetQSquared(std::tuple<SF,Nuclei> sf) const
    {
      auto f  = static_cast<int>(std::get<SF>(sf));
      auto n  = static_cast<int>(std::get<Nuclei>(sf));
      return(fQ2_values.at(n).at(f));
    }
    //______________________________________________________________________________

    bool   SFBase::IsComputed( std::tuple<SF,Nuclei> sf, double x, double Q2) const
    {
      auto f  = static_cast<int>(std::get<SF>(sf));
      auto n  = static_cast<int>(std::get<Nuclei>(sf));
      if(  fx_values.at(n).at(f) != x ) return false;
      if( fQ2_values.at(n).at(f) != Q2 ) return false;
      return true;
    }
    //______________________________________________________________________________

    void SFBase::Reset()
    {
      for(int n = 0; n<NNuclei;  n++) {
        for(int i = 0; i<NStructureFunctions ; i++) {
          fx_values[n][i]        = 0.0;
          fQ2_values[n][i]       = 0.0;
          fValues[n][i]          = 0.0;
          fUncertainties[n][i]   = 0.0;
        }
      }
    }
    //______________________________________________________________________________
    
    double SFBase::Get(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t, OPELimit l) const 
    {
      auto sftype = std::get<SF>(sf);
      auto target = std::get<Nuclei>(sf);
      return 0.0;
    }
    //______________________________________________________________________________


    double  SFBase::Get(std::tuple<SF,Nuclei> sf) const 
    {
      return 0.0;
    }
    //______________________________________________________________________________

  }
}
