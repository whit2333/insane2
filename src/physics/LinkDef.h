#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;

#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;
#pragma link C++ namespace insane::xsec;
#pragma link C++ namespace insane::kinematics;
#pragma link C++ namespace insane::physics::TMCs;

//#pragma link C++ enum  insane::physics::SF;
//#pragma link C++ enum  insane::physics::FormFactor;
//#pragma link C++ enum  insane::physics::Nuclei;
//#pragma link C++ enum  insane::physics::Parton;
//#pragma link C++ enum  insane::physics::ComptonAsymmetry;


// OLD PDFs
//#pragma link C++ class insane::physics::BBSQuarkHelicityDistributions+; 
//#pragma link C++ class insane::physics::BBSUnpolarizedPDFs+; 
//#pragma link C++ class insane::physics::BBSPolarizedPDFs+;
//#pragma link C++ class insane::physics::AvakianQuarkHelicityDistributions+; 
//#pragma link C++ class insane::physics::AvakianUnpolarizedPDFs+; 
//#pragma link C++ class insane::physics::AvakianPolarizedPDFs+; 
//#pragma link C++ class insane::physics::LSS98QuarkHelicityDistributions+; 
//#pragma link C++ class insane::physics::LSS98UnpolarizedPDFs+; 
//#pragma link C++ class insane::physics::LSS98PolarizedPDFs+; 
//#pragma link C++ class insane::physics::StatisticalQuarkFits+;
//#pragma link C++ class insane::physics::StatisticalUnpolarizedPDFs+;
//#pragma link C++ class insane::physics::StatisticalPolarizedPDFs+;
//#pragma link C++ class insane::physics::Stat2015UnpolarizedPDFs+;
//#pragma link C++ class insane::physics::Stat2015PolarizedPDFs+;
////#pragma link C++ clasinsane::physics::s LHAPDFUnpolarizedPDFs+;
////#pragma link C++ clasinsane::physics::s LHAPDFPolarizedPDFs+;
//#pragma link C++ class insane::physics::AAC08PolarizedPDFs+;
//#pragma link C++ class insane::physics::BBPolarizedPDFs+;
//#pragma link C++ class insane::physics::JAMPolarizedPDFs+;
//#pragma link C++ class insane::physics::JAM15PolarizedPDFs+;
//#pragma link C++ class insane::physics::MHKPolarizedPDFs+;
//#pragma link C++ class insane::physics::DNS2005PolarizedPDFs+;
//#pragma link C++ class insane::physics::LSS2006PolarizedPDFs+;
//#pragma link C++ class insane::physics::LSS2010PolarizedPDFs+;
//#pragma link C++ class insane::physics::DSSVPolarizedPDFs+;

//#pragma link C++ global insane::physics::fgPartonDistributionFunctions+;
//#pragma link C++ global insane::physics::fgPolarizedPartonDistributionFunctions+;

#pragma link C++ class std::tuple<insane::physics::FormFactor,insane::physics::Nuclei>+;
#pragma link C++ class std::tuple<insane::physics::SF,insane::physics::Nuclei>+;
#pragma link C++ class std::tuple<insane::physics::ComptonAsymmetry,insane::physics::Nuclei>+;

#pragma link C++ class insane::physics::PDFValues+;
#pragma link C++ class insane::physics::PDFBase2+;

#pragma link C++ class insane::physics::UnpolarizedPDFs+;
#pragma link C++ class insane::physics::PolarizedPDFs+;
#pragma link C++ class insane::physics::Twist3DistributionFunctions+;
#pragma link C++ class insane::physics::Twist4DistributionFunctions+;
#pragma link C++ class insane::physics::T3DFsFromTwist3SSFs+;

#pragma link C++ class insane::physics::CTEQ10_UPDFs+;
#pragma link C++ class std::tuple<insane::physics::CTEQ10_UPDFs>+;

#pragma link C++ class insane::physics::JAM_PPDFs+;
#pragma link C++ class insane::physics::JAM_T3DFs+;
#pragma link C++ class insane::physics::JAM_T4DFs+;

#pragma link C++ class std::tuple<insane::physics::JAM_PPDFs>+;
#pragma link C++ class std::tuple<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs>+;
#pragma link C++ class std::tuple<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs,insane::physics::JAM_T4DFs>+;

#pragma link C++ class insane::physics::Stat2015_UPDFs+;
#pragma link C++ class insane::physics::Stat2015_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::Stat2015_UPDFs>+;
#pragma link C++ class std::tuple<insane::physics::Stat2015_PPDFs>+;

#pragma link C++ class insane::physics::LCWF_UPDFs+;
#pragma link C++ class insane::physics::LCWF_PPDFs+;
#pragma link C++ class insane::physics::LCWF_T3DFs+;

#pragma link C++ class insane::physics::AAC08_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::AAC08_PPDFs>+;

#pragma link C++ class insane::physics::BB_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::BB_PPDFs>+;

#pragma link C++ class insane::physics::BBS_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::BBS_PPDFs>+;

#pragma link C++ class insane::physics::DSSV_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::DSSV_PPDFs>+;

#pragma link C++ class insane::physics::LSS2006_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::LSS2006_PPDFs>+;

#pragma link C++ class insane::physics::LSS2010_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::LSS2010_PPDFs>+;

#pragma link C++ class insane::physics::DNS2005_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::DNS2005_PPDFs>+;

#pragma link C++ class insane::physics::GS_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::GS_PPDFs>+;

#pragma link C++ class insane::physics::ABDY_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::ABDY_PPDFs>+;



#pragma link C++ class insane::physics::F1F209_SFs+;
#pragma link C++ class insane::physics::NMC95_SFs+;

#pragma link C++ class insane::physics::VCSABase+;
#pragma link C++ class insane::physics::VirtualComptonAsymmetries+;
#pragma link C++ class insane::physics::VirtualComptonAsymmetriesModel1+;

#pragma link C++ class insane::physics::AsymmetryBase+;
#pragma link C++ class insane::physics::AsymmetriesFromStructureFunctions+;
#pragma link C++ class insane::physics::VirtualComptonScatteringAsymmetries+;
//#pragma link C++ class insane::physics::VCSAsFromSFs<insane::physics::F1F209_SFs,insane::physics::SpinStructureFunctions>+;


//#pragma link C++ global insane::physics::fgFormFactors+;
#pragma link C++ class insane::physics::FormFactors+;
#pragma link C++ class insane::physics::BonnFormFactors+;
#pragma link C++ class insane::physics::FormFactors2+;
#pragma link C++ class insane::physics::DipoleFormFactors+;
#pragma link C++ class insane::physics::AMTFormFactors+;
#pragma link C++ class insane::physics::AHLYFormFactors+;
#pragma link C++ class insane::physics::MSWFormFactors+;
#pragma link C++ class insane::physics::GalsterFormFactors+;
#pragma link C++ class insane::physics::KellyFormFactors+;
#pragma link C++ class insane::physics::RiordanFormFactors+;
#pragma link C++ class insane::physics::AmrounFormFactors+;
#pragma link C++ class insane::physics::BilenkayaFormFactors+;
#pragma link C++ class insane::physics::BostedFormFactors+; 

#pragma link C++ typedef insane::physics::DefaultFormFactors+;

#pragma link C++ class insane::physics::F1F209QuasiElasticFormFactors+; 

//#pragma link C++ class insane::physics::GeneralizedPartonDistributions+;
//#pragma link C++ class insane::physics::DoubleDistributionGPDs+;
//#pragma link C++ class insane::physics::GK_GPDs+;

#pragma link C++ class insane::physics::GPDBase+;
#pragma link C++ class insane::physics::DD_GPDs+;

#pragma link C++ class insane::physics::GeneralizedPartonDistributions+;
#pragma link C++ class insane::physics::DoubleDistributionGPDs+;
#pragma link C++ class insane::physics::GK_GPDs+;

#pragma link C++ class insane::physics::CFFsFromGPDs<insane::physics::GK_GPDs>+;
#pragma link C++ class insane::physics::CFFsFromGPDs<insane::physics::DD_GPDs>+;


#pragma link C++ class insane::physics::ComptonFormFactors+;

#pragma link C++ class insane::physics::WaveFunction+;
#pragma link C++ class insane::physics::BonnDeuteronWaveFunction+;

#pragma link C++ class insane::physics::NucelonMomentumDistributions+;
#pragma link C++ class insane::physics::FermiMomentumDist+;

#pragma link C++ function fermi3_(double* , int *, double *)+;



//#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::AAC08_PPDFs>+;
//#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs>+;
//#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs>+;
//#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs,insane::physics::JAM_T4DFs>+;
//
//#pragma link C++ class insane::physics::SFsFromPDFs< insane::physics::CTEQ10_PDFs>+;
//
//#pragma link C++ class insane::physics::SFsFromPDFs< insane::physics::Stat2015_UPDFs>+;
//#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::Stat2015_PPDFs>+;

//#pragma link C++ class insane::physics::SFsFromPDFs< std::tuple<insane::physics::Stat2015_UPDFs>>+;
//#pragma link C++ class insane::physics::SSFsFromPDFs<std::tuple<insane::physics::Stat2015_PPDFs>>+;

//#pragma link C++ class insane::physics::WaveFunction+;
//#pragma link C++ class BonnDeuteronWaveFunction+;


#pragma link C++ class insane::physics::StrongCouplingConstant+;

//#pragma link C++ class insane::physics::PDFBase+;
//#pragma link C++ enum  insane::physics::PDFBase::PartonFlavor;
//#pragma link C++ class insane::physics::PartonDistributionFunctions+;
//#pragma link C++ class insane::physics::PolarizedPartonDistributionFunctions+;

//#pragma link C++ class insane::physics::LCWFPartonDistributionFunctions+;
//#pragma link C++ class insane::physics::LCWFPolarizedPartonDistributionFunctions+;

#pragma link C++ class insane::physics::PartonHelicityDistributions+;
#pragma link C++ class insane::physics::BBS_PHDs+;
#pragma link C++ class insane::physics::ABDY_PHDs+;
//#pragma link C++ class insane::physics::UnpolarizedPDFsFromPHDs+;
//#pragma link C++ class insane::physics::PolarizedPDFsFromPHDs+;

//#pragma link C++ class LHAPDFStructureFunctions+;


#pragma link C++ function formc_(double *)+; 
#pragma link C++ function formm_(double *)+; 

#pragma link C++ function inif1f209_()+;
#pragma link C++ function emc_09_(float * ,float * ,int *,float *)+;
#pragma link C++ function cross_tot_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_qe_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function f1f2in09_(double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_tot_mod_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_qe_mod_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function f1f2in09_mod_(double * ,double * ,double *,double * ,double * ,double * ,double * ,double * )+;

#pragma link C++ function EMC_Effect(double * ,double *)+;

#pragma link C++ function qfs_(double*,double*,double*,double*,double*, double*,double*, double*)+;
#pragma link C++ function qfs_born_(double*,double*,double*,double*,double*,double*,double*,double*, double*,double*, double*,int*)+;
#pragma link C++ function qfs_radiated_(double*,double*,double*,double*,double*,double*,double*,double*)+;
#pragma link C++ function wiser_sub_(double * ,int * ,double * ,double * , double * )+;
#pragma link C++ function wiser_all_sig_(double * ,double * ,double * ,double * , int *  ,double * )+;
#pragma link C++ function wiser_all_sig0_(double * ,double * ,double * ,double * , int *  ,double * )+;
#pragma link C++ function wiser_fit_(int * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function vtp_(double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function aac08pdf_(double*,double*,int*,double*,double**)+;
#pragma link C++ function dssvini_(int *)+;
#pragma link C++ function dssvfit_(double *,double *,double *,double *,double *,double *,double *,double *)+;
#pragma link C++ function partondf_(double *,double *,int *)+;
#pragma link C++ function partonevol_(double *x, double *Q2, int *ipol, double * pdf, int * isingle, int *num)+;
#pragma link C++ function mrst2002_(int *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *);
#pragma link C++ function mrst2001e_(int *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *);
#pragma link C++ function mrstpdfs_(int *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *);
#pragma link C++ function setctq6_(int*)+;
#pragma link C++ function getctq6_(int* ,double*, double*, double*)+;
#pragma link C++ function setct10_(int*)+; 
#pragma link C++ function getct10_(int*,double*,double*,double*)+; 
#pragma link C++ function getabkm09_(int*,double*,double*,int*,int*,int*,double*)+; 
#pragma link C++ function getmstw08_(int*,int*,double*,double*,double*)+; 
#pragma link C++ function ppdf_( int*, double*,double*,double*, double*,double*,double*, double*,double*,double*,double*,double*,double*,double*,double*)+;
#pragma link C++ function ini_()+;
#pragma link C++ function jam_xF_(double* res,double* x,double* Q2, char* flav)+;
#pragma link C++ function grid_init_(char*, char*, char*, int*)+;
#pragma link C++ function polfit_(int*,double*,double*,double*, double*, double*,double*,double*, double*,double*,double*)+;
#pragma link C++ function lss2006init_()+;
#pragma link C++ function lss2006_(int*,double*,double*, double*,double*,double*, double*,double*,double*,double*, double*, double*,double*,double*, double*,double*)+;
#pragma link C++ function lss2010init_();
#pragma link C++ function lss2010_(int*,double*,double*,double*,double*,double*,double*,double*,double*,double*,double*);
#pragma link C++ function nloini_()+;
#pragma link C++ function polnlo_(int* ,double*, double*, double*, double*, double*, double*, double*, double*)+;
#pragma link C++ function epcv_single_(char* , int *, double *, double *,double*, double*, double*, double*)+;
#pragma link C++ function epcv_single_v3_(char* , int *, double *, double *,double*, double*, double*, double*)+;
#pragma link C++ function gpc_single_v3_(char* , int *, double *, double *,double*, double*, double*, double*)+;

//#pragma link C++ function sane_pol_(double * ,double * ,double * ,int * ,int * ,double * )+;

//#pragma link C++ class insane::physics::GSPolarizedPDFs+;
//#pragma link C++ class insane::physics::CTEQ6UnpolarizedPDFs+;
//#pragma link C++ class insane::physics::CTEQ10UnpolarizedPDFs+;
//#pragma link C++ class insane::physics::CJ12UnpolarizedPDFs+; 
//#pragma link C++ class insane::physics::ABKM09UnpolarizedPDFs+; 
//#pragma link C++ class insane::physics::MSTW08UnpolarizedPDFs+; 
//#pragma link C++ class insane::physics::MRST2001UnpolarizedPDFs+; 
//#pragma link C++ class insane::physics::MRST2002UnpolarizedPDFs+; 
//#pragma link C++ class insane::physics::MRST2006UnpolarizedPDFs+; 

//// quasi elastic 
//#pragma link C++ class QuasiElasticInclusiveDiffXSec+; 
//#pragma link C++ class QEIntegral+; 
//#pragma link C++ class QEFuncWrap+; 


//#pragma link C++ global insane::physics::fgStructureFunctions+;
//#pragma link C++ global insane::physics::fgSpinStructureFunctions+;
//#pragma link C++ global insane::physics::fgPolarizedStructureFunctions+; // deprecated

#pragma link C++ class insane::physics::MeasuredSpinStructureFunctions+;

#pragma link C++ class insane::physics::StructureFunctionBase+;
#pragma link C++ class insane::physics::StructureFunctions+;
#pragma link C++ class insane::physics::StructureFunctions2+;

#pragma link C++ class insane::physics::SpinStructureFunctions+;
#pragma link C++ class insane::physics::PolarizedStructureFunctions+;

#pragma link C++ class insane::physics::CompositeStructureFunctions+;
#pragma link C++ class insane::physics::CompositePolarizedStructureFunctions+;
#pragma link C++ class insane::physics::LowQ2StructureFunctions+;
#pragma link C++ class insane::physics::LowQ2PolarizedStructureFunctions+;

//#pragma link C++ class insane::physics::StructureFunctionsFromVPCSs+;

#pragma link C++ class insane::physics::F1F209StructureFunctions+;
#pragma link C++ class insane::physics::F1F209QuasiElasticStructureFunctions+;
#pragma link C++ class insane::physics::NMC95StructureFunctions+;

#pragma link C++ class std::vector<insane::physics::StructureFunctions*>+;
#pragma link C++ class std::vector<insane::physics::PolarizedStructureFunctions*>+;
#pragma link C++ class std::vector<insane::physics::StructureFunctionsFromPDFs*>+;
#pragma link C++ class std::vector<insane::physics::PolarizedStructureFunctionsFromPDFs*>+;

#pragma link C++ class insane::physics::PolarizedStructureFunctionsFromVCSAs+;
#pragma link C++ class insane::physics::PolSFsFromComptonAsymmetries+;
//#pragma link C++ class LHAPDFStructureFunctions+;
#pragma link C++ class insane::physics::StructureFunctionsFromPDFs+;
#pragma link C++ class insane::physics::BETAG4StructureFunctions+;

#pragma link C++ class insane::physics::PolarizedStructureFunctionsFromPDFs+;

#pragma link C++ class insane::physics::CompositeSFs+;
#pragma link C++ class insane::physics::CompositeSSFs+;

#pragma link C++ class insane::physics::F1F209_SFs+;
#pragma link C++ class insane::physics::NMC95_SFs+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs,insane::physics::JAM_T4DFs>+;

#pragma link C++ class insane::physics::SFsFromPDFs< insane::physics::CTEQ10_UPDFs>+;

#pragma link C++ class insane::physics::SFsFromPDFs< insane::physics::Stat2015_UPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::Stat2015_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::Stat2015_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::AAC08_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::AAC08_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::BB_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::BB_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::BBS_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::BBS_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::DSSV_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::DSSV_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::LSS2010_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::LSS2010_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::LSS2006_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::LSS2006_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::DNS2005_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::DNS2005_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::GS_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::GS_PPDFs,insane::physics::JAM_T3DFs>+;

#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::ABDY_PPDFs>+;
#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::ABDY_PPDFs,insane::physics::JAM_T3DFs>+;


#pragma link C++ class insane::physics::FragmentationFunctions+;
#pragma link C++ class insane::physics::DSSFragmentationFunctions+;
#pragma link C++ class insane::physics::DSS_FrFs+;

#pragma link C++ class insane::physics::TransverseMomentumDistributions+;
#pragma link C++ class insane::physics::Gaussian_TMDs<insane::physics::Stat2015_UPDFs>+;

#pragma link C++ class insane::physics::SIDISlowPt_SFs<insane::physics::Gaussian_TMDs<insane::physics::Stat2015_UPDFs>,insane::physics::DSS_FrFs>+;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;
#pragma link C++ namespace insane::physics::xs;
#pragma link C++ namespace insane::physics::eic;

#pragma link C++ class insane::physics::CrossSectionInfo+;

#pragma link C++ class insane::physics::CompositeDiffXSec+;
#pragma link C++ class insane::physics::CompositeDiffXSec2+;
#pragma link C++ class insane::physics::DVCSDiffXSec+;
#pragma link C++ class insane::physics::IncoherentDVCSXSec+;
#pragma link C++ class insane::physics::IncoherentDISXSec+;
#pragma link C++ class insane::physics::CoherentDVCSXSec+;
#pragma link C++ class insane::physics::CoherentDVMPXSec+;

#pragma link C++ class insane::physics::CompositeDiffXSec+;
//#pragma link C++ class insane::physics::DiffXSecKinematicKey+;
#pragma link C++ class insane::physics::GridDiffXSec+;
#pragma link C++ class insane::physics::GridXSecValue+;

#pragma link C++ class insane::physics::InclusiveDISXSec+;
#pragma link C++ class insane::physics::InclusiveBornDISXSec+;

#pragma link C++ class insane::physics::eic::EICIncoherentDISXSec+;
#pragma link C++ class insane::physics::eic::EICInclusiveBornDISXSec+;

//#pragma link C++ class insane::physics::CTEQ6eInclusiveDiffXSec+;
//#pragma link C++ class insane::physics::F1F209eInclusiveDiffXSec+;
//#pragma link C++ class insane::physics::F1F209QuasiElasticDiffXSec+;
//#pragma link C++ class insane::physics::PolIncDiffXSec+;
//// quasi elastic 
//#pragma link C++ class QuasiElasticInclusiveDiffXSec+; 
//#pragma link C++ class QEIntegral+; 
//#pragma link C++ class QEFuncWrap+; 

#pragma link C++ class insane::physics::QFSInclusiveDiffXSec+;
#pragma link C++ class insane::physics::QFSXSecConfiguration+;

#pragma link C++ class insane::physics::CrossSectionDifference+;

#pragma link C++ class insane::physics::PolarizedDiffXSec+;
#pragma link C++ class insane::physics::PolarizedDISXSec+;
#pragma link C++ class insane::physics::PolarizedDISXSecParallelHelicity+;
#pragma link C++ class insane::physics::PolarizedDISXSecAntiParallelHelicity+;


#pragma link C++ class insane::physics::QuasiElasticInclusiveDiffXSec+; 
#pragma link C++ class insane::physics::QEIntegral+; 
#pragma link C++ class insane::physics::QEFuncWrap+; 

#pragma link C++ class insane::physics::PhotonDiffXSec+;
#pragma link C++ class insane::physics::InclusiveElectroProductionXSec+;
//#pragma link C++ class InclusiveElectroProductionXSec+;
#pragma link C++ class insane::physics::InclusivePhotoProductionXSec+;
//#pragma link C++ class InclusivePhotoProductionXSec+;
#pragma link C++ class insane::physics::InclusivePionElectroProductionXSec+;
#pragma link C++ class insane::physics::InclusivePionPhotoProductionXSec+;
#pragma link C++ class InclusiveHadronProductionXSec<insane::physics::InclusivePionElectroProductionXSec>+;
#pragma link C++ class InclusiveHadronProductionXSec<insane::physics::InclusivePionPhotoProductionXSec>+;

#pragma link C++ class insane::physics::InclusiveWiserXSec+;

#pragma link C++ class insane::physics::WiserInclusivePhotoXSec+;
#pragma link C++ class insane::physics::WiserInclusivePhotoXSec2+;
#pragma link C++ class insane::physics::PhotoWiserDiffXSec+;
#pragma link C++ class insane::physics::PhotoWiserDiffXSec2+;

#pragma link C++ class insane::physics::WiserInclusiveElectroXSec+;
#pragma link C++ class insane::physics::ElectroWiserDiffXSec+;

#pragma link C++ class insane::physics::PhotonDiffXSec+;

#pragma link C++ class insane::physics::InclusivePhotoProductionXSec+;
//#pragma link C++ class insane::physics::InclusivePhotoProductionXSec+;

#pragma link C++ class insane::physics::InclusiveElectroProductionXSec+;
//#pragma link C++ class insane::physics::InclusiveElectroProductionXSec+;

#pragma link C++ class insane::physics::InclusiveEPCVXSec+;
#pragma link C++ class insane::physics::InclusiveEPCVXSec2+;

#pragma link C++ class insane::physics::VirtualPhotoAbsorptionCrossSections+;
#pragma link C++ class insane::physics::PhotoAbsorptionCrossSections+;
#pragma link C++ class insane::physics::MAIDPhotoAbsorptionCrossSections+;
#pragma link C++ class insane::physics::MAID_VPACs+;

#pragma link C++ class insane::physics::SFsFromVPACs<insane::physics::MAID_VPACs>+;
#pragma link C++ class insane::physics::SSFsFromVPACs<insane::physics::MAID_VPACs>+;


//#pragma link C++ class insane::physics::Helium4ElasticDiffXSec+;
//#pragma link C++ class insane::physics::Helium4ElasticRadiativeTailDiffXSec+;
//#pragma link C++ class insane::physics::ProtonElasticRadiativeTailDiffXSec+;
//#pragma link C++ class insane::physics::DeuteronElasticDiffXSec+;

//#pragma link C++ class insane::physics::ExternalRadiator<insane::physics::Helium4ElasticDiffXSec>+;
//#pragma link C++ class insane::physics::ExternalRadiator<insane::physics::epElasticDiffXSec>+;

//#pragma link C++ class insane::physics::ExternalRadiator<insane::physics::QuasiElasticInclusiveDiffXSec>+;
//#pragma link C++ class insane::physics::QuasiElasticRadiativeTailDiffXSec+;
//#pragma link C++ class insane::physics::ElectronQuasiElasticRadiativeTailDiffXSec+;
//#pragma link C++ class insane::physics::ElectronProtonElasticDiffXSec+;
//#pragma link C++ class insane::physics::ProtonElasticDiffXSec+;


//#pragma link C++ class insane::physics::MAIDInclusiveDiffXSec+; 
//#pragma link C++ class insane::physics::MAIDNucleusInclusiveDiffXSec+; 
//#pragma link C++ class insane::physics::MAIDKinematicKey+; 
//#pragma link C++ class insane::physics::MAIDPolarizedTargetDiffXSec+; 
//#pragma link C++ class insane::physics::MAIDPolarizedKinematicKey+; 

#pragma link C++ class insane::physics::VCSABase+; 
#pragma link C++ class insane::physics::VirtualComptonAsymmetries+; 
#pragma link C++ class insane::physics::VirtualComptonAsymmetriesModel1+; 
#pragma link C++ class insane::physics::MAIDVirtualComptonAsymmetries+; 

#pragma link C++ class insane::physics::MAIDExclusivePionDiffXSec+; 
#pragma link C++ class insane::physics::MAIDInclusiveElectronDiffXSec+; 
#pragma link C++ class insane::physics::MAIDInclusiveElectronDiffXSec::MAIDXSec_5Fold_2D_integrand+; 
#pragma link C++ class insane::physics::MAIDInclusiveElectronDiffXSec::MAIDXSec_5Fold_theta+; 
#pragma link C++ class insane::physics::MAIDInclusiveElectronDiffXSec::MAIDXSec_5Fold_phi+; 

#pragma link C++ class insane::physics::MAIDExclusivePionDiffXSec2+; 
#pragma link C++ class insane::physics::MAIDExclusivePionDiffXSec3+;

#pragma link C++ class insane::physics::MAIDInclusivePionDiffXSec+; 
#pragma link C++ class insane::physics::MAIDInclusivePionDiffXSec::MAIDXSec_5Fold_2D_integrand+; 
#pragma link C++ class insane::physics::MAIDInclusivePi0DiffXSec+; 

#pragma link C++ class insane::physics::VCSAsFromSFs<insane::physics::SFsFromPDFs<insane::physics::Stat2015_UPDFs>, insane::physics::SSFsFromPDFs<insane::physics::Stat2015_PPDFs>>+;
#pragma link C++ class insane::physics::VCSAsFromSFs<insane::physics::F1F209_SFs, insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs,insane::physics::JAM_T4DFs>>+;
#pragma link C++ class insane::physics::VCSAsFromSFs< insane::physics::SFsFromPDFs<insane::physics::CTEQ10_UPDFs>, insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs, insane::physics::JAM_T3DFs, insane::physics::JAM_T4DFs>>+;

#pragma link C++ class insane::physics::VCSAsFromSFs<insane::physics::NMC95_SFs, insane::physics::SSFsFromPDFs<Stat2015_PPDFs>>+;
//#pragma link C++ class insane::physics::VCSAsFromPDFs<insane::physics::Stat2015_UPDFs, insane::physics::Stat2015_PPDFs>+;
#pragma link C++ class insane::physics::MAID_VCSAs+;
#pragma link C++ class insane::physics::MAIDVirtualComptonAsymmetries+;

//#pragma link C++ class std::map<int, double >+;
//#pragma link C++ class std::pair<int, double >+;
//#pragma link C++ class std::vector<int, std::pair<int, double > >+;
#pragma link C++ class insane::physics::A1A2Model1+;
#pragma link C++ class insane::physics::RSSAsymmetryFits+;
#pragma link C++ class insane::physics::RSSAsymmetryFits2+;

//#pragma link C++ class insane::physics::PolarizedCrossSectionDifference+; 

//#pragma link C++ class insane::physics::RADCOR+;
//#pragma link C++ class insane::physics::RADCORVariables+;
//#pragma link C++ class insane::physics::RADCORKinematics+;
//
//#pragma link C++ class insane::physics::RADCOR::StripApproxIntegrand1Wrap+;
//#pragma link C++ class insane::physics::RADCOR::StripApproxIntegrand1_withDWrap+;
//#pragma link C++ class insane::physics::RADCOR::StripApproxIntegrand2Wrap+;
//#pragma link C++ class insane::physics::RADCOR::ExternalOnly_ExactInelasticIntegrandWrap+;
//#pragma link C++ class insane::physics::RADCOR::External2DEnergyIntegral_IntegrandWrap+;

#pragma link C++ class insane::physics::EsFuncWrap+;
#pragma link C++ class insane::physics::EpFuncWrap+;
// exact forms 
// internal 
#pragma link C++ class insane::physics::IntRadFuncWrap+;
#pragma link C++ class insane::physics::IntRadOmegaFuncWrap+;
#pragma link C++ class insane::physics::IntRadCosThkFuncWrap+;
// external 
#pragma link C++ class insane::physics::TExactFuncWrap+;
#pragma link C++ class insane::physics::EsExactFuncWrap+;
#pragma link C++ class insane::physics::EpExactFuncWrap+;
#pragma link C++ class insane::physics::EsExactFunc2Wrap+;
#pragma link C++ class insane::physics::EpExactFunc2Wrap+;

// Monte Carlo integration methods 
//#pragma link C++ function insane::physics::RADCOR::MCAcceptReject(const int &,double (*)(double *),double *,double *,int,int,double &,double &); 
//#pragma link C++ function insane::physics::RADCOR::MCSampleMean(const int &,double (*)(double *),double *,double *,int,double &,double &); 
//#pragma link C++ function insane::physics::RADCOR::MCSampleMean(const int &,double (*)(double),double *,double *,int,double &,double &); 
//#pragma link C++ function insane::physics::RADCOR::MCImportanceSampling(const int,double (*)(double *),double (*)(double *,double *,double *),double (*)(int,double *,double *),double *,double *,int,double &,double &); 
////// Adaptive Simpson integration method 
//#pragma link C++ function insane::physics::RADCOR::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
//#pragma link C++ function insane::physics::RADCOR::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 

// #pragma link C++ class insane::physics::RADCOR::Full_Elastic_integrand+;

//#pragma link C++ class insane::physics::RADCORInternalUnpolarizedDiffXSec+;
//
//#pragma link C++ class insane::physics::RADCORRadiatedDiffXSec+;
//#pragma link C++ class insane::physics::RADCORRadiatedUnpolarizedDiffXSec+;
//
//#pragma link C++ class insane::physics::RADCOR2+;
//#pragma link C++ class insane::physics::RADCOR2::StripApproxIntegrand1Wrap+;
//#pragma link C++ class insane::physics::RADCOR2::StripApproxIntegrand1_withDWrap+;
//#pragma link C++ class insane::physics::RADCOR2::StripApproxIntegrand2Wrap+;
//#pragma link C++ class insane::physics::RADCOR2::External2DEnergyIntegral_IntegrandWrap+;
//#pragma link C++ class insane::physics::RADCOR2::External3DIntegral_IntegrandWrap+;
//
//
//#pragma link C++ class insane::physics::POLRADVariables+;
//#pragma link C++ class insane::physics::POLRADKinematics+;
//
//#pragma link C++ class insane::physics::POLRAD+;
////#pragma link C++ class insane::physics::POLRAD::IRT_R_3+;
//#pragma link C++ class insane::physics::POLRAD::IRT_2D_integrand+;
//#pragma link C++ class insane::physics::POLRAD::ERTFuncWrap+;
//#pragma link C++ class insane::physics::POLRAD::QRTTauFuncWrap+;
//#pragma link C++ class insane::physics::POLRAD::QRTRFuncWrap+;
//#pragma link C++ class insane::physics::POLRAD::IRT_TauFuncWrap_delta+;
//#pragma link C++ class insane::physics::POLRAD::IRT_TauFuncWrap_2+;
//#pragma link C++ class insane::physics::POLRAD::IRT_RFuncWrap_3+;
//#pragma link C++ class insane::physics::POLRAD::QRT_TauFuncWrap_QE_Full+;
//#pragma link C++ class insane::physics::POLRAD::QRT_RFuncWrap_QE_Full+;
////#pragma link C++ function insane::physics::POLRAD::SimpleIntegration(double (*)(double &), double,double,double,int); 
////#pragma link C++ function insane::physics::POLRAD::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
////#pragma link C++ function insane::physics::POLRAD::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 
//
//#pragma link C++ class insane::physics::POLRADUltraRelativistic+;
//#pragma link C++ class insane::physics::POLRADUltraRelativistic::SIG_r_2D_integrand+;
////#pragma link off function insane::physics::POLRADUltraRelativistic::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
////#pragma link off function insane::physics::POLRADUltraRelativistic::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 
//
//#pragma link C++ class insane::physics::POLRADInternalPolarizedDiffXSec+;
//#pragma link C++ typedef insane::physics::PolradDiffXSec;
//#pragma link C++ class insane::physics::POLRADElasticDiffXSec+;
//#pragma link C++ class insane::physics::POLRADBornDiffXSec+;
//#pragma link C++ class insane::physics::POLRADRadiatedDiffXSec+;
//#pragma link C++ class insane::physics::POLRADElasticTailDiffXSec+;
//#pragma link C++ class insane::physics::POLRADQuasiElasticTailDiffXSec+;
//#pragma link C++ class insane::physics::POLRADInelasticTailDiffXSec+;
//#pragma link C++ class insane::physics::POLRADUltraRelInelasticTailDiffXSec+;
////#pragma link off function insane::physics::POLRADInternalPolarizedDiffXSec::AdaptiveSimpson(double (*)(double &), double,double,double,int); 
////#pragma link off function insane::physics::POLRADInternalPolarizedDiffXSec::AdaptiveSimpsonAux(double (*)(double &), double,double,double,double,double,double,double,int); 
//
//#pragma link C++ class insane::physics::RadiativeCorrections1D+;
//#pragma link C++ class insane::physics::RadiativeTail+;
////#pragma link C++ class insane::physics::RadiativeTail2+;
//#pragma link C++ class insane::physics::ElasticRadiativeTail+;
////#pragma link C++ class insane::physics::QuasielasticRadiativeTail+;
//#pragma link C++ class insane::physics::InelasticRadiativeTail+;
//#pragma link C++ class insane::physics::InelasticRadiativeTail2+;
//#pragma link C++ class insane::physics::FullInelasticRadiativeTail+;
//
//#pragma link C++ class insane::physics::ElectroProductionXSec+;
//
//#pragma link C++ class insane::physics::InclusiveMottXSec+;
////#pragma link C++ class insane::physics::ExclusiveMottXSec+;
//#pragma link C++ class insane::physics::epElasticDiffXSec+;
//#pragma link C++ class insane::physics::MollerDiffXSec+;
//#pragma link C++ class insane::physics::InclusiveMollerDiffXSec+;
//#pragma link C++ class insane::physics::eInclusiveElasticDiffXSec+;
//#pragma link C++ class insane::physics::pInclusiveElasticDiffXSec+;
//#pragma link C++ class insane::physics::RCeInclusiveElasticDiffXSec+;
//
//#pragma link C++ class insane::physics::BeamSpinAsymmetry+;
//#pragma link C++ class insane::physics::BeamTargetAsymmetry+;
//#pragma link C++ class insane::physics::PolarizedDISAsymmetry+;

//#pragma link C++ class insane::physics:OARPionDiffXSec+;
//#pragma link C++ class insane::physics:OARPionElectroDiffXSec+;
//#pragma link C++ class insane::physics:OARPionPhotoDiffXSec+;
//#pragma link C++ class insane::physics:ElectroOARPionDiffXSec+;
//#pragma link C++ class insane::physics:PhotoOARPionDiffXSec+;

#pragma link C++ class insane::physics::InclusiveWiserXSec+;
#pragma link C++ class insane::physics::PhaseSpaceSampler+;

#pragma link C++ function formc_(double *)+; 
#pragma link C++ function formm_(double *)+; 

#pragma link C++ function inif1f209_()+;
#pragma link C++ function emc_09_(float * ,float * ,int *,float *)+;
#pragma link C++ function cross_tot_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_qe_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function f1f2in09_(double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_tot_mod_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_qe_mod_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function f1f2in09_mod_(double * ,double * ,double *,double * ,double * ,double * ,double * ,double * )+;

#pragma link C++ function EMC_Effect(double * ,double *)+;

#pragma link C++ function qfs_(double*,double*,double*,double*,double*, double*,double*, double*)+;
#pragma link C++ function qfs_born_(double*,double*,double*,double*,double*,double*,double*,double*, double*,double*, double*,int*)+;
#pragma link C++ function qfs_radiated_(double*,double*,double*,double*,double*,double*,double*,double*)+;
#pragma link C++ function wiser_sub_(double * ,int * ,double * ,double * , double * )+;
#pragma link C++ function wiser_all_sig_(double * ,double * ,double * ,double * , int *  ,double * )+;
#pragma link C++ function wiser_all_sig0_(double * ,double * ,double * ,double * , int *  ,double * )+;
#pragma link C++ function wiser_fit_(int * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function vtp_(double * ,double * ,double * ,double * ,double * ,double * ,double * )+;

// Add all cross sections that can be radiated
//#pragma link C++ class insane::physics::Radiator<insane::physics::InclusiveDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<insane::physics::InclusiveBornDISXSec>+;
//#pragma link C++ class insane::physics::Radiator<insane::physics::CompositeDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<F1F209eInclusiveDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<F1F209QuasiElasticDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<CTEQ6eInclusiveDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<insane::physics::POLRADBornDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<insane::physics::POLRADInelasticTailDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<F1F209QuasiElasticDiffXSec>+;
//#pragma link C++ class insane::physics::Radiator<QFSInclusiveDiffXSec>+;
//#pragma link C++ class insane::physics::ExternalRadiator<insane::physics::InclusiveMollerDiffXSec>+;

#pragma link C++ class insane::xsec::FinalStateParticle+;
#pragma link C++ class std::vector<insane::xsec::FinalStateParticle*>+;
#pragma link C++ class insane::xsec::SamplerBase+;
#pragma link C++ class insane::xsec::GeneratorBase+;

#pragma link C++ class insane::TargetMaterial+;
#pragma link C++ class insane::Target+;
#pragma link C++ class insane::SimpleTarget+;
#pragma link C++ class insane::SimpleTargetWithWindows+;

#pragma link C++ function insane::materials::ComputeLuminosityPerN(std::array<int,2>, double,  double, double );
#pragma link C++ class insane::physics::Luminosity+;

#endif

