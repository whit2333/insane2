#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <typeinfo>

#include "TMath.h"

#include "insane/rad/RadiativeEffects.h"

#define CATCH_CONFIG_MAIN
//#include "catch2/catch.hpp"
#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

SCENARIO( "Energy Loss", "[eloss]" ) {

  using namespace insane::rad;
  using namespace insane::units;
  using namespace insane::masses;

  GIVEN("An incident alpha particle of energy 1 MeV") {

    double M    = M_4He;
    double T    = 1.0 * MeV;
    double E    = T + M;
    double P    = std::sqrt(E * E - M * M);
    double beta = P / E;
    WHEN("stopping in Hydrogen") {
      const double rho        = 8.37480E-05 * g / (cm * cm * cm);
      const double eloss      = dEdx_Bethe(2, beta, 1, 1, rho);
      const double real_value = 7.167e3 * (MeV * cm * cm / g);
      REQUIRE(std::abs((eloss/real_value)-1) < 0.2);
    }
    WHEN("stopping in Beryllium") {
      const double rho        = 1.848 * g / (cm * cm * cm);
      const double eloss      = dEdx_Bethe(2, beta, 4, 9, rho);
      const double real_value = 1.618e3 * (MeV * cm * cm / g);
      REQUIRE(std::abs((eloss/real_value)-1) < 0.5);
    }
  }
  GIVEN("An incident alpha particle of energy 10 MeV") {

    double M    = M_4He;
    double T    = 10.0 * MeV;
    double E    = T + M;
    double P    = std::sqrt(E * E - M * M);
    double beta = P / E;
    WHEN("stopping in Hydrogen") {
      const double rho        = 8.37480E-05 * g / (cm * cm * cm);
      const double real_value = 1283.0 * (MeV * cm * cm / g);
      const double eloss      = dEdx_Bethe(2, beta, 1, 1, rho);
      REQUIRE(std::abs((eloss / real_value) - 1) < 0.1);
    }
    WHEN("stopping in Beryllium") {
      const double rho        = 1.848 * g / (cm * cm * cm);
      const double real_value = 4.461e2 * (MeV * cm * cm / g);
      const double eloss      = dEdx_Bethe(2, beta, 4, 9, rho);
      REQUIRE(std::abs((eloss/real_value)-1) < 0.2);
    }
  }
  GIVEN("An incident alpha particle of energy 100 MeV") {
    double M    = M_4He;
    double T    = 100.0 * MeV;
    double E    = T + M;
    double P    = std::sqrt(E * E - M * M);
    double beta = P / E;
    WHEN("stopping in Hydrogen") {
      const double rho        = 8.37480E-05 * g / (cm * cm * cm);
      const double eloss      = dEdx_Bethe(2, beta, 1, 1, rho);
      const double real_value = 187.0 * (MeV * cm * cm / g);
      REQUIRE(std::abs((eloss/real_value)-1) < 0.1);
    }
    WHEN("stopping in Beryllium") {
      const double rho        = 1.848 * g / (cm * cm * cm);
      const double real_value = 7.095e1 * (MeV * cm * cm / g);
      const double eloss      = dEdx_Bethe(2, beta, 4, 9, rho);
      REQUIRE(std::abs((eloss/real_value)-1) < 0.2);
    }
  }

  GIVEN("An incident alpha particle of energy 1 GeV") {
    double M    = M_4He;
    double T    = 1000.0 * MeV;
    double E    = T + M;
    double P    = std::sqrt(E * E - M * M);
    double beta = P / E;
    WHEN("stopping in Hydrogen") {
      const double rho        = 8.37480E-05 * g / (cm * cm * cm);
      const double eloss      = dEdx_Bethe(2, beta, 1, 1, rho);
      const double real_value = 32.25 * (MeV * cm * cm / g);
      REQUIRE(std::abs((eloss/real_value)-1) < 0.1);
    }
    WHEN("stopping in Beryllium") {
      const double rho        = 1.848 * g / (cm * cm * cm);
      const double real_value = 1.266e1 * (MeV * cm * cm / g);
      const double eloss      = dEdx_Bethe(2, beta, 4, 9, rho);
      REQUIRE(std::abs((eloss/real_value)-1) < 0.2);
    }
  }
}


