set(exename "insane-config")
add_executable(${exename} ${exename}.cxx)

target_include_directories(${exename} PUBLIC
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<INSTALL_INTERFACE:include>
  )
# target_link_libraries(${exename} fieldtest)

# IMPORTANT: Add the bar executable to the "export-set"
install(TARGETS ${exename}
  EXPORT ${PROJECT_NAME}Targets
  RUNTIME DESTINATION bin COMPONENT bin)
