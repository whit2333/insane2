#include "insane/structurefunctions/SIDISlowPt_SFs.h"
#include "fmt/core.h"

using namespace insane::physics;

using SIDIS_SFs = typename insane::physics::SIDISlowPt_SFs<Gaussian_TMDs<Stat2015_UPDFs>,DSS_FrFs>;

void plot_F_UU_cosphi_h(){

  double Q20 = 5.0;
  TMultiGraph * mg = new TMultiGraph("F_UU_cosphi_h"," ; P_{h#perp} [GeV]");
  TMultiGraph * mg2 = new TMultiGraph("F_UU_cosphi_h"," ; P_{h#perp} [GeV]");

  auto* legend = new TLegend(0.6, 0.6, .8, .8);

  SIDIS_SFs sfs;
  int i = 1;
  for (double z : { 0.4, 0.6}) {
  for (double xB : { 0.3, 0.4}) {
    TF1*      FUU_T_vs_PT = new TF1(
        fmt::format("F_{{UU}}^{{#cos#phi_{{h}}}} #pi+ z={}; P_{{h#perp}}",z).c_str(),
        [&](double* x, double* p) {
          auto res = sfs.FUU_cosphi_h(Hadron::pi_plus, Q20, xB, z, x[0],0.0);
          return res;
        },
        0, 1.0, 0);
    FUU_T_vs_PT->SetLineColor(i);

    legend->AddEntry(FUU_T_vs_PT, fmt::format("F_{{UU}}^{{#cos#phi_{{h}}}} #pi+ z={}", z).c_str(),
                     "l");
    auto gr1 = new TGraph(FUU_T_vs_PT);
    gr1->SetLineColor(i);
    mg->Add(gr1, "l");

    TF1*      FUU_T_vs_PT2 = new TF1(
        fmt::format("F_{{UU}}^{{#cos#phi_{{h}}}} #pi- z={}; P_{{h#perp}}",z).c_str(),
        [&](double* x, double* p) {
          auto res = sfs.FUU_cosphi_h(Hadron::pi_minus, Q20, xB, z,x[0] ,0.2);
          return res;
        },
        0, 1.0, 0);
    FUU_T_vs_PT2->SetLineColor(i);
    FUU_T_vs_PT2->SetLineStyle(9);
    legend->AddEntry(FUU_T_vs_PT2, fmt::format("F_{{UU}}^{{#cos#phi_{{h}}}} #pi- z={}", z).c_str(),
                     "l");
    auto gr2 = new TGraph(FUU_T_vs_PT2);
    gr2->SetLineColor(i);
    mg2->Add(gr2, "l");
  }
    i++;
  }

  mg->Add(mg2);

  TCanvas* c = new TCanvas();
  //c->Divide(2,2);
  //c->cd(1);
  mg->Draw("a");
  //mg->GetXaxis()->SetLimits(0,1);
  //auto leg = c->BuildLegend();
  //leg->SetLineColor(0);

  legend->Draw();
}
