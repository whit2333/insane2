#include "insane/tmds/DSS_FrFs.h"

#include "TF1.h"

using namespace insane::physics;

void fragmentation_functions_2D(){

  double Q20 = 10.0;

  TF2* Du_plus = new TF2("Du_plus; z ; P_{h#perp}",[=](double* x, double* p){
    static  DSS_FrFs ffs;
    return (ffs.D1_u_pi_plus(x[0],x[1],Q20)+ ffs.D1_ubar_pi_plus(x[0],x[1],Q20));
  }, 0,1,0,1,0);

  TF2* Dubar_plus = new TF2("Dubar_plus; z ; P_{h#perp}",[=](double* x, double* p){
    static  DSS_FrFs ffs;
    return (ffs.D1_d_pi_plus(x[0],x[1],Q20)+ ffs.D1_dbar_pi_plus(x[0],x[1],Q20));
  }, 0,1,0,1,0);

  TF1* Du_plus_pt = new TF1("Du_plus",[=](double* x, double* p){
    static  DSS_FrFs ffs;
    return (ffs.D1_u_pi_plus(0.2,x[0],Q20));
  }, 0,1,0);

  TF1* Dubar_plus_pt = new TF1("Dubar_plus",[=](double* x, double* p){
    static  DSS_FrFs ffs;
    return ( ffs.D1_ubar_pi_plus(0.2,x[0],Q20));
  }, 0,1,0);


  auto gr_u_plus = new TGraph(Du_plus_pt);
  gr_u_plus->SetLineColor(1);

  auto gr_ubar_plus = new TGraph(Dubar_plus_pt);
  gr_ubar_plus->SetLineColor(2);

  TMultiGraph * mg = new TMultiGraph("FF"," ; pt [GeV]");
  mg->Add(gr_u_plus,"l");
  mg->Add(gr_ubar_plus,"l");

  TCanvas* c = new TCanvas();
  c->Divide(2,2);
  c->cd(1);
  Du_plus->Draw("colz");
  c->cd(2);
  Dubar_plus->Draw("colz");
  c->cd(3);
  mg->Draw("a");

  c->cd(4);
  Dubar_plus_pt->Draw();

}
