#include "insane/tmds/DSS_FrFs.h"

#include "TF1.h"

using namespace insane::physics;

void dss_fragmentation_funcs(){

  double Q20 = 10.0;

  TF1* Du_plus = new TF1("Du_plus",[=](double* x, double* p){
    static  DSS_FrFs ffs;
    return (ffs.D_u_piplus(x[0],Q20)+ ffs.D_ubar_piplus(x[0],Q20));
  }, 0,1,0);

  TF1* Dubar_plus = new TF1("Dubar_plus",[=](double* x, double* p){
    static  DSS_FrFs ffs;
    return ( ffs.D_ubar_piplus(x[0],Q20));
  }, 0,1,0);

  Du_plus->Draw();
  Du_plus->SetLineColor(4);
  Dubar_plus->Draw("same");

}
