void plot_profile_function()
{
   using namespace insane::physics;

   DoubleDistributionGPDs*  gpds = new DoubleDistributionGPDs();
   //auto  gpds = new GK_GPDs();

   int f = 2;
   int i = 1;
   double xi = 0.00001;
   double t  = 0.0;
   double xB = 0.25;
   double alpha = 0.0;
   double beta = 0.0;

   auto func = new TF1("func",[&](double* x, double* p) {
         return( gpds->ProfileFunction(1.0,x[0],alpha ));
         }, -1.0,1.0,0);
   auto func2 = new TF1("func2",[&](double* x, double* p) {
         return( gpds->ProfileFunction(1.0,beta,x[0] ));
         }, -1.0,1.0,0);
   auto func3 = new TF2("func3",[&](double* x, double* p) {
         return( gpds->ProfileFunction(1.0,x[0],x[1] ));
         }, -1.0,1.0,-1.0,1.0,0);
   
   //auto fgluon = new TF1("fgluon",[&](double* x, double* p) {
   //      return( gpds->H_g(x[0],xi, t, 4.0));
   //      }, 0.0001,1.0,0);
   //auto fgluon2 = new TF1("fgluon",[&](double* x, double* p) {
   //      return( gpds->H_i_integrate_DD(0,0,x[0],xi, t, 4.0));
   //      }, 0.0001,1.0,0);

   //auto fsea = new TF1("fsea",[&](double* x, double* p) {
   //      return( gpds->H_i(2,1, x[0],xi, t, 4.0));
   //      }, 0.0001,1.0,0);


   //auto usea = new TF1("usea",[&](double* x, double* p) {
   //      //std::cout << " x = " << x[0] <<  std::endl;
   //      return( x[0]*gpds->H_i(2,1,x[0],xi, t,4.0));
   //      }, -0.5,0.5,0);
   //auto usea2 = new TF1("usea2",[&](double* x, double* p) {
   //      //std::cout << " x = " << x[0] <<  std::endl;
   //      return( x[0]*gpds->H_i_integrate_DD(2,1,x[0],xi, t,4.0));
   //      }, -0.5,0.5,0);

   //auto uval = new TF1("uval",[&](double* x, double* p) {
   //      //std::cout << " x = " << x[0] <<  std::endl;
   //      return( gpds->H_i(2,2,x[0],xi, t,4.0));
   //      }, -0.5,0.5,0);
   //auto uval2 = new TF1("uval2",[&](double* x, double* p) {
   //      //std::cout << " x = " << x[0] << ", xi = " << xi <<   std::endl;
   //      return( gpds->H_i_integrate_DD(2,2,x[0],xi, t,4.0));
   //      }, -0.5,0.5,0);


   func->SetNpx(1500);

   TLegend * leg = 0;
   TCanvas * c = new TCanvas();
   c->Divide(2,2);
   TGraph * gr = 0;
   TMultiGraph * mg =  0;

   //------------------------------------------
   c->cd(1);
   leg = new TLegend (0.3,0.85,0.7,0.98);
   mg = new TMultiGraph();

   gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(1);
   mg->Add(gr,"l");
   leg->AddEntry(gr,Form("#pi_{1}(#beta,#alpha=%.1f)",alpha),"l");

   gr = new TGraph(func2->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(4);
   leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   //xi=0.1;
   //gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(2);
   //mg->Add(gr,"l");

   //xi=0.2;
   //gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(3);
   ////mg->Add(gr,"l");

   mg->Draw("a");
   gPad->SetLogy(true);
   mg->Draw("a");
   leg->Draw();

   //------------------------------------------
   c->cd(2);
   leg = new TLegend (0.3,0.85,0.7,0.98);
   mg = new TMultiGraph();

   alpha = 0.1;
   beta  = 0.1;
   gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(1);
   mg->Add(gr,"l");
   leg->AddEntry(gr,Form("#pi_{1}(#beta,#alpha=%.1f)",alpha),"l");

   gr = new TGraph(func2->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(4);
   leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   mg->Draw("a");
   gPad->SetLogy(true);
   mg->Draw("a");
   leg->Draw();

   //------------------------------------------
   c->cd(3);
   
   leg = new TLegend (0.3,0.85,0.7,0.98);
   mg = new TMultiGraph();

   alpha = 0.5;
   beta  = 0.5;
   gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(1);
   mg->Add(gr,"l");
   leg->AddEntry(gr,Form("#pi_{1}(#beta,#alpha=%.1f)",alpha),"l");

   gr = new TGraph(func2->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(4);
   leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   mg->Draw("a");
   gPad->SetLogy(true);
   mg->Draw("a");
   leg->Draw();

   //------------------------------------------
   c->cd(4);

   gPad->SetLogz(true);
   func3->Draw("col");

   //------------------------------------------

   beta = 0.1;
   std::cout << func2->Integral(-1.0,1.0) << std::endl;
   beta = 0.4;
   std::cout << func2->Integral(-1.0,1.0) << std::endl;
   beta = 0.8;
   std::cout << func2->Integral(-1.0,1.0) << std::endl;

}

