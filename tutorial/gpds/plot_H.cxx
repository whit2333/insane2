void plot_H()
{
   using namespace insane::physics;

   GK_GPDs*  gpds = new GK_GPDs();
   //auto  gpds = new GK_GPDs();

   int f = 0;
   int i = 0;
   double xi = 0.01;

   auto func = new TF1("func",[&](double* x, double* p) {
         return( gpds->H_i(f,i, x[0],xi,0.0,4.0));
         }, 0.0001,1.0,0);

   auto fsea = new TF1("fsea",[&](double* x, double* p) {
         return( x[0]*gpds->H_i(f,1, x[0],xi,0.0,4.0));
         }, -1.0,1.0,0);

   auto fval = new TF1("fval",[&](double* x, double* p) {
         return( gpds->H_i(f,2, x[0],xi,0.0,4.0));
         }, -0.5,0.5,0);

   auto uval = new TF1("fval",[&](double* x, double* p) {
         return( gpds->H_u(x[0],xi,-0.2,4.0));
         }, -0.5,0.5,0);
   TCanvas * c = new TCanvas();
   c->Divide(2,2);
   TGraph * gr = 0;
   TMultiGraph * mg =  0;

   //------------------------------------------
   c->cd(1);
   mg = new TMultiGraph();
   gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(1);
   mg->Add(gr,"l");

   xi=0.1;
   gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(4);
   mg->Add(gr,"l");

   xi=0.2;
   gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(2);
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle("gluon");;
   gPad->SetLogx(true);
   mg->Draw("a");

   //------------------------------------------
   c->cd(2);
   mg = new TMultiGraph();

   f=2;
   xi=0.01;
   gr = new TGraph(fsea->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(1);
   mg->Add(gr,"l");

   xi=0.1;
   gr = new TGraph(fsea->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(4);
   mg->Add(gr,"l");

   xi=0.2;
   gr = new TGraph(fsea->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(2);
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle("sea");
   //gPad->SetLogx(true);
   mg->Draw("a");

   //------------------------------------------
   c->cd(3);
   mg = new TMultiGraph();

   f=2;
   xi=0.01;
   gr = new TGraph(fval->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(1);
   mg->Add(gr,"l");

   xi=0.1;
   gr = new TGraph(fval->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(4);
   mg->Add(gr,"l");

   xi=0.2;
   gr = new TGraph(fval->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(2);
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle("val");
   mg->Draw("a");

   //------------------------------------------
   c->cd(4);
   mg = new TMultiGraph();

   xi=0.1;
   gr = new TGraph(uval->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(1);
   mg->Add(gr,"l");

   xi=0.2;
   gr = new TGraph(uval->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(4);
   mg->Add(gr,"l");

   xi=0.3;
   gr = new TGraph(uval->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(2);
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle("val");
   mg->Draw("a");


}

