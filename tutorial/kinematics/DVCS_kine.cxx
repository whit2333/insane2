R__LOAD_LIBRARY(libinsaneCore.so)
#include "insane/kinematics/DVCS.h"

void DVCS_kine() {
  using namespace insane::kine;
  using namespace insane::kine::dvcs;


  auto input = std::make_tuple(E_beam_v{10.6}, 
                               E_prime_v{4.7}, theta_v{17.0*M_PI/180.0}, phi_v{0.0},
                               E_had_v{3.5}, theta_had_v{14.0*M_PI/180.0}, phi_had_v{M_PI}, M_had_v{0.139});


  DVCS_vars dis_vars = construct_DVCS_variables();
  {
  auto dis_values = dis_vars.ComputeValues(input);

  std::cout << " \n";

  // could probably make this better, like: dis_values.get<x_v>()
  std::cout << " E0  = " << std::get<E_beam_v>(dis_values) << "\n";
  std::cout << " E'  = " << std::get<E_prime_v>(dis_values) << "\n";
  std::cout << " th  = " << std::get<theta_v>(dis_values)*180.0/M_PI << "\n";
  std::cout << " Q2  = " << std::get<Q2_v>(dis_values) << "\n";
  std::cout << " x   = " << std::get<x_v>(dis_values) << "\n";
  std::cout << " W   = " << std::get<W_v>(dis_values) << "\n";
  std::cout << " y   = " << std::get<y_v>(dis_values) << "\n";
  std::cout << " nu  = " << std::get<nu_v>(dis_values) << "\n";
  std::cout << " k   = " << std::get<k_vec>(dis_values) << "\n";
  //std::cout << " k'  = " << std::get<k_prime_vec>(dis_values) << "\n";
  std::cout << " q   = " << std::get<q_vec>(dis_values) << "\n";
  std::cout << " ph  = " << std::get<p_had_vec>(dis_values) << "\n";
  //std::cout << " W'  = " << std::get<W_prime_pi_v>(dis_values) << "\n";
  }

}

