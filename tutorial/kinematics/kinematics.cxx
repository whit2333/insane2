R__LOAD_LIBRARY(libinsaneCore.so)
#include "insane/kinematics/Core.h"
#include "insane/kinematics/Inelastic.h"

void kinematics() {
  using namespace insane::kine;
  using namespace insane::kine::var;

  // Note in the future meta classes might help with the type/tag declarations
  //using E_beam_v  = Var<struct E_beam_tag>;
  //using E_prime_v = Var<struct E_prime_tag>;
  //using theta_v   = Var<struct theta_tag>;

  //using Q2_v = Var<struct Q2_tag>;
  //using nu_v = Var<struct nu_tag>;
  //using x_v  = Var<struct x_tag>;
  //using y_v  = Var<struct y_tag>;
  //using W_v  = Var<struct W_tag>;

  double M_p = 0.938;

  auto measured_vars = make_independent_vars<E_beam_v, E_prime_v, theta_v, phi_v>();

  auto dis_kine_vars = measured_vars
                           .add<Q2_v, nu_v>(
                               [](const auto& v) constexpr {
                                 const auto&  E0    = std::get<E_beam_v>(v);
                                 const auto&  Ep    = std::get<E_prime_v>(v);
                                 const auto&  th    = std::get<theta_v>(v);
                                 const double M_p   = 0.938;
                                 const double sinth = std::sin(th / 2.0);
                                 return 4.0 * E0 * Ep * sinth * sinth;
                               },
                               [](const auto& v) constexpr {
                                 const auto& E0 = std::get<E_beam_v>(v);
                                 const auto& Ep = std::get<E_prime_v>(v);
                                 return E0 - Ep;
                               }) // note that we are chaining here.
                           .add<x_v, y_v>([](const auto& v) constexpr {
                             const auto& Q2  = std::get<Q2_v>(v);
                             const auto& nu  = std::get<nu_v>(v);
                             const auto& E0  = std::get<E_beam_v>(v);
                             double      M_p = 0.938;
                             return std::tuple<x_v, y_v>(Q2 / (2.0 * M_p * nu), nu / E0);
                           })
                           .add<W_v>([](const auto& v) constexpr {
                             // calculate W
                             const double M_p = 0.938;
                             const auto&  Q2  = std::get<Q2_v>(v);
                             const auto&  nu  = std::get<nu_v>(v);
                             return std::sqrt(M_p * M_p - Q2 + 2.0 * M_p * nu);
                           });

  auto input = std::make_tuple(E_beam_v{10.6}, E_prime_v{5.3}, theta_v{17.0*M_PI/180.0}, phi_v{0.0});

  {
    auto dis_values = dis_kine_vars.ComputeValues(input);

    // could probably make this better, like: dis_values.get<x_v>()
    std::cout << " E0  = " << std::get<E_beam_v>(dis_values) << "\n";
    std::cout << " E'  = " << std::get<E_prime_v>(dis_values) << "\n";
    std::cout << " th  = " << std::get<theta_v>(dis_values) * 180.0 / M_PI << "\n";
    std::cout << " Q2  = " << std::get<Q2_v>(dis_values) << "\n";
    std::cout << " x   = " << std::get<x_v>(dis_values) << "\n";
    std::cout << " W   = " << std::get<W_v>(dis_values) << "\n";
    std::cout << " y   = " << std::get<y_v>(dis_values) << "\n";
    std::cout << " nu  = " << std::get<nu_v>(dis_values) << "\n";
  }

  Inelastic_vars dis_vars = construct_Inelastic_variables();
  {
    auto dis_values = dis_vars.ComputeValues(input);
    std::cout << " \n";
    // could probably make this better, like: dis_values.get<x_v>()
    std::cout << " E0  = " << std::get<E_beam_v>(dis_values) << "\n";
    std::cout << " E'  = " << std::get<E_prime_v>(dis_values) << "\n";
    std::cout << " th  = " << std::get<theta_v>(dis_values)*180.0/M_PI << "\n";
    std::cout << " Q2  = " << std::get<Q2_v>(dis_values) << "\n";
    std::cout << " x   = " << std::get<x_v>(dis_values) << "\n";
    std::cout << " W   = " << std::get<W_v>(dis_values) << "\n";
    std::cout << " y   = " << std::get<y_v>(dis_values) << "\n";
    std::cout << " nu  = " << std::get<nu_v>(dis_values) << "\n";
  }

}

