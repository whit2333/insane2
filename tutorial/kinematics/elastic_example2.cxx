R__LOAD_LIBRARY(libinsanePhysics.so)
#include "insane/kinematics/Elastic.h"

void elastic_example() {
  using namespace insane::kine;
  using elastic::theta_v;//   = Var<struct theta_tag>;
  using elastic::phi_v;//     = Var<struct phi_tag>;
  using elastic::E_beam_v;//  = Var<struct E_beam_tag>;
  using elastic::E_prime_v;// = Var<struct E_prime_tag>;
  using elastic::Q2_v; //     = Var<struct Q2_tag>;
  using elastic::nu_v; //     = Var<struct nu_tag>;
  using elastic::W_v;  //     = Var<struct W_tag>;
  using insane::kine::elastic::M_target_v;
  using insane::kine::elastic::M_recoil_v;


  auto vars0 = make_independent_vars<theta_v, phi_v>();
  auto vars1 =  vars0.add<E_beam_v>([](const auto& v) constexpr { return 6; });
  // Here we make use of the library's generic lambda
  // auto elastic_vars = vars1.append(insane::kine::elastic::add_ElasticPrimaryVars) ;
  // but we want to compute everything here as an example:
  auto vars2 = vars1
                   .add<M_target_v, M_recoil_v>(
                       // define all variables with one return tuple
                       [](const auto& v) constexpr {
                         const double M = insane::masses::M_4He / insane::units::GeV;
                         return std::make_tuple(M_target_v(M), M_recoil_v(M));
                       })
                   .add<E_prime_v>(
                       // compute E'
                       [](const auto& v) constexpr {
                         const auto&  E0    = std::get<E_beam_v>(v);
                         const double M   = std::get<M_target_v>(v);
                         const auto&  th    = std::get<theta_v>(v);
                         const double sinth = std::sin(th / 2.0);
                         const double Ep    = E0 / (1.0 + (2.0 * E0 / M) * (sinth * sinth));
                         return Ep;
                       });
  auto vars3 = vars2
                   .add<Q2_v, nu_v>(
                       // define all variables with lambda
                       [](const auto& v) constexpr {
                         const auto&  E0    = std::get<E_beam_v>(v);
                         const auto&  Ep    = std::get<E_prime_v>(v);
                         const auto&  th    = std::get<theta_v>(v);
                         const double M_p   = std::get<M_target_v>(v);
                         const double sinth = std::sin(th / 2.0);
                         const double Q2    = 4.0 * E0 * Ep * sinth * sinth;
                         return Q2;
                       },
                       [](const auto& v) constexpr {
                         const auto& E0 = std::get<E_beam_v>(v);
                         const auto& Ep = std::get<E_prime_v>(v);
                         return E0 - Ep;
                       })
                   .add<W_v>([](const auto& v) constexpr {
                     const double M_p = std::get<M_target_v>(v);
                     const auto&  Q2  = std::get<Q2_v>(v);
                     const auto&  nu  = std::get<nu_v>(v);
                     return std::sqrt(M_p * M_p - Q2 + 2.0 * M_p * nu);
                   })
                   .append(insane::kine::elastic::add_ElasticVectorVars)
                   ;

  auto input = std::make_tuple(theta_v{12.0*M_PI/180.0}, phi_v{0.0});

  {
    auto var_values = vars3.ComputeValues(input);

    // could probably make this better, like: var_values.get<x_v>()
    std::cout << " E0  = " << std::get<E_beam_v>(var_values) << "\n";
    std::cout << " E'  = " << std::get<E_prime_v>(var_values) << "\n";
    std::cout << " th  = " << std::get<theta_v>(var_values) * 180.0 / M_PI << "\n";
    std::cout << " Q2  = " << std::get<Q2_v>(var_values) << "\n";
    std::cout << " W   = " << std::get<W_v>(var_values) << "\n";
    std::cout << " nu  = " << std::get<nu_v>(var_values) << "\n";
    std::cout << " th2 = " << std::get<elastic::p_recoil_vec>(var_values).get().Theta()*180/M_PI << "\n";
  }
}

