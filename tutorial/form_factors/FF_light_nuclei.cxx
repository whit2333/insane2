void FF_light_nuclei(double Q2_min = 0.00005, double Q2_max = 6.0){

  insane::physics::DipoleFormFactors ffs;

  auto Fc_4He = [&](double* x, double* p){ return std::abs(ffs.FCHe4(x[0]*x[0])); };

  auto GEp = [&](double* x, double* p){ return ffs.GEp(x[0]*x[0]); };

  auto GMp = [&](double* x, double* p){ return ffs.GMp(x[0]*x[0]); };

  TF1* f1 = new TF1("Fc_4He",Fc_4He,Q2_min,Q2_max,0);
  f1->SetLineColor(1);
  f1->SetNpx(500);
  auto gr1 = new TGraph(f1);

  TF1* f2 = new TF1("GEp",GEp,Q2_min,Q2_max,0);
  f2->SetLineColor(2);
  auto gr2 = new TGraph(f2);

  TF1* f3 = new TF1("GMp",GMp,Q2_min,Q2_max,0);
  f3->SetLineColor(4);
  auto gr3 = new TGraph(f3);

  auto Ad = [&](double* x, double* p){ return ffs.F_C0(x[0]*x[0]); };
  auto Bd = [&](double* x, double* p){ return ffs.F_C2(x[0]*x[0]); };
  auto T20d = [&](double* x, double* p){ return ffs.F_M1(x[0]*x[0]); };
  TF1* fAd   = new TF1("F_C0",Ad,Q2_min,Q2_max,0);
  TF1* fBd   = new TF1("F_C2",Bd,Q2_min,Q2_max,0);
  TF1* fT20d = new TF1("F_M1",T20d,Q2_min,Q2_max,0);
  fAd->SetLineColor(7);
  fBd->SetLineColor(8);
  fT20d->SetLineColor(6);

  auto gr4 = new TGraph(fAd);
  auto gr5 = new TGraph(fBd);
  auto gr6 = new TGraph(fT20d);

  auto c = new TCanvas();

  auto mg = new TMultiGraph();

  //gPad->SetLogy(true);
  mg->Add(gr1,"l");
  mg->Add(gr2,"l");
  mg->Add(gr3,"l");
  mg->Add(gr4,"l");
  mg->Add(gr5,"l");
  mg->Add(gr6,"l");

  mg->Draw("a");
  mg->GetXaxis()->SetTitle("Q [GeV]");
  mg->GetXaxis()->CenterTitle(true);
  mg->GetYaxis()->SetRangeUser(0.0001,10);
  gPad->SetLogy(1);
  auto leg = gPad->BuildLegend();
  leg->Draw();


}
