//R__LOAD_LIBRARY(libfmt.so)
#include "fmt/core.h"
//R__LOAD_LIBRARY(librwc.so)
#include "rwc/DisplayServer.h"
#include "rwc/MonitoringDisplay.h"
#include <chrono>
#include <thread>
#include <random>
#include <iostream>
//R__LOAD_LIBRARY(libinsaneCore.so)
#include "insane/kinematics/Core.h"
#include "insane/kinematics/Inelastic.h"
#include "insane/kinematics/HallCSettings.h"
#include "insane/new_xsec/PhaseSpaceVariables.h"
#include "insane/new_xsec/NFoldDifferential.h"
#include "insane/new_xsec/InitialState.h"
#include "insane/new_xsec/Jacobians.h"
#include "insane/new_xsec/DiffCrossSection.h"
#include "insane/new_xsec/PSSampler.h"

#include "insane/base/PhysicalConstants.h"

#include "insane/structurefunctions/fwd_SFs.h"
#include "insane/formfactors/AMTFormFactors.h"
#include "insane/xsections/Lee4HeJpsiPhotoproduction.h"

#include "insane/xsec/XSPhaseSpace.h"
#include "insane/xsec/ElasticScattering.h"

#include <tuple>
//#include "ROOT/RDataFrame.hxx"

#include "Math/GenVector/VectorUtil.h"
#include "Math/Vector3D.h"

template<typename T>
using Lim = insane::xsec::VarLimits<T>;
template<typename T>
using ULim = insane::xsec::VarLimits<T,true>;

using HallC = insane::hallc::HallCSetting;
using Lee4HeJpsiPhotoproduction = insane::physics::Lee4HeJpsiPhotoproduction;

using insane::units::GeV;
using insane::units::degree;
//using ROOT::Math::XYZTVector;

int main (int argc, char *argv[]) {

  using namespace insane::kine;
  using namespace insane::kine::var;
  using namespace insane::xsec::elastic;

  // --------------------------------------------------------------------------
  // 
  HallC hc;
  hc.SHMS_p0 = 2.5;
  //
  // --------------------------------------------------------------------------
  // 
  std::random_device               rd;
  std::mt19937                     gen(rd());
  std::uniform_real_distribution<> dist(hc.SHMS_P_min() * GeV, hc.SHMS_P_max() * GeV);
  // 
  // --------------------------------------------------------------------------
  //
  auto ind_vars = make_independent_vars<V_theta, V_phi>();

  auto var_stack =
      ind_vars
          .add<V_M_target, V_M_recoil, V_ebeam>([](const auto& v) constexpr {
            const double M_p = insane::masses::M_p;
            return std::tuple<V_M_target, V_M_recoil, V_ebeam>(
                              M_p,      M_p,      10.6 *GeV);
          })
          .add<V_eprime>([](const auto& v) constexpr {
            const auto&  E0         = std::get<V_ebeam>(v);
            const auto&  M          = std::get<V_M_target>(v);
            const auto&  th         = std::get<V_theta>(v);
            const double sinthOver2 = std::sin(th / 2.0);
            return E0 / (1.0 + (2.0 * E0 / M) * sinthOver2 * sinthOver2);
          })
          .add<V_k_beam_vec, V_k_prime_vec, V_q_vec>( // Four vectors k and k'
            [](const auto& v) {
              const auto&            E0    = std::get<V_ebeam>(v);
              const auto&            th    = std::get<V_theta>(v);
              const auto&            phi   = std::get<V_phi>(v);
              const auto&            M_p   = std::get<V_M_target>(v);
              const auto&            Ep    = std::get<V_eprime>(v);
              const double           sinth = std::sin(th / 2.0);
              const ROOT::Math::XYZTVector kbeam(0.0, 0.0, E0, E0);
              const ROOT::Math::XYZTVector kprime(Ep * std::cos(phi) * std::sin(th),
                                            Ep * std::sin(phi) * std::sin(th), Ep * std::cos(th), Ep);
              return std::tuple<V_k_beam_vec, V_k_prime_vec, V_q_vec>(kbeam, kprime, kbeam - kprime);
            })
          .add<V_Q2>([](const auto& v) constexpr {
            const auto& k0 = std::get<V_k_beam_vec>(v);
            const auto& kp = std::get<V_k_prime_vec>(v);
            return -1.0*(k0.get()-kp.get()).mag();
          })
          .add<V_nu, V_y, V_tau>([](const auto& v) constexpr {
            const auto& E0 = std::get<V_ebeam>(v);
            const auto& Ep = std::get<V_eprime>(v);
            const auto& M  = std::get<V_M_target>(v);
            const auto& Q2 = std::get<V_Q2>(v);
            return std::tuple<V_nu, V_y, V_tau>(E0 - Ep, (E0 - Ep) / E0, Q2 / (4.0 * M * M));
          })
          .add<V_GEp,V_GMp>([](const auto& v) {
            using namespace insane::physics;
            static AMTFormFactors ffs;
            const auto&           Q2 = std::get<V_Q2>(v);
            const double          GE = ffs.GEp(Q2 / GeV / GeV);
            const double          GM = ffs.GMp(Q2 / GeV / GeV);
            return std::tuple<V_GEp,V_GMp> (GE, GM);
          });

  using IndVars_t  = std::tuple<V_Q2, V_phi>;
  //using VarStack_t = decltype(var_stack);
  //using RetVar_t   = typename VarStack_t::Vars_t;
  using IndVars2_t = std::tuple<V_theta, V_phi>;
  //using XS_t = decltype(xs0);
  auto input = IndVars2_t(8.0*degree, 0.0 );
  auto v0    = var_stack(input);
  std::cout << " ===========================================\n";
  std::cout << " cross section : " << insane::xsec::elastic::ep_Elastic_dsigma_dQ2_dphi(v0) << "\n";
  std::cout << " Q2            : " << std::get<V_Q2>(v0)/GeV/GeV << " GeV^{2}\n";
  std::cout << " cross section : " << insane::xsec::elastic::ep_Elastic_dsigma_dQ2_dphi(var_stack,input) << "\n";

  {
    insane::xsec::PhaseSpace ndiff(//Lim<Q2_v>({0.1*GeV*GeV,10.0*GeV*GeV}), 
                                          Lim<V_theta>({hc.SHMS_theta_min(), hc.SHMS_theta_max()}),
                                          Lim<V_phi>({hc.SHMS_phi_min(), hc.SHMS_phi_max()}));
    insane::xsec::TotalCrossSection<decltype(insane::xsec::elastic::ep_Elastic_dsigma_dQ2_dphi),decltype(ndiff)> tot_xs(insane::xsec::elastic::ep_Elastic_dsigma_dQ2_dphi, ndiff);
    std::cout << tot_xs.CalculateTotalXS(var_stack) << " total xs \n\n";
    insane::xsec::PhaseSpaceSampler<decltype(tot_xs)>  ps_sampler(tot_xs);
    std::cout << ps_sampler.Init(var_stack) << " total xs (FOAM)\n";
    auto ev = ps_sampler.Generate();
    auto ev2 = ps_sampler.Generate(var_stack);
  }

  //insane::xsec::jacobian::TransformedDifferentialCrossSection<decltype(xs0),dsigma_dOmega,IndVars2_t> xs2(xs0);
  ////auto input2 = IndVars_t(0.1*GeV*GeV, 0.0 );
  //std::cout << " cross section : " << xs2(input) << "\n";
  //auto res =  xs2.ComputeValues(input);
  //std::cout << " Q2 = " << std::get<Q2_v>(res)/GeV/GeV << "\n";

  //{
  //  auto jm = insane::xsec::jacobian::Jacobian<std::tuple<Q2_v, phi_v>, std::tuple<theta_v, phi_v>,
  //                                             VarStack_t>(var_stack);
  //  std::cout << " det = " << jm.Det(input) << "\n";

  //  auto jm2 = insane::xsec::jacobian::Jacobian<std::tuple<theta_v, phi_v>, std::tuple<Q2_v, phi_v>,
  //                                              VarStack_t>(var_stack);
  //  std::cout << " det = " << jm2.Det(input) << "\n";

  //}
  ////{
  ////  insane::xsec::PhaseSpace ndiff(//Lim<Q2_v>({0.1*GeV*GeV,10.0*GeV*GeV}), 
  ////                                        Lim<theta_v>({hc.SHMS_theta_min(), hc.SHMS_theta_max()}),
  ////                                        ULim<phi_v>({hc.SHMS_phi_min(), hc.SHMS_phi_max()}));
  ////  insane::xsec::IntegratedCrossSection int_xs(xs2, ndiff);
  ////  insane::xsec::PSSampler  ps_sampler(int_xs);
  ////  ps_sampler.Init();

  ////  std::cout << int_xs.CalculateTotalXS() << " total xs \n\n";
  ////}

  //{
  //  insane::xsec::PhaseSpace ndiff(//Lim<Q2_v>({0.1*GeV*GeV,10.0*GeV*GeV}), 
  //                                        Lim<theta_v>({hc.SHMS_theta_min(), hc.SHMS_theta_max()}),
  //                                        ULim<phi_v>({hc.SHMS_phi_min(), hc.SHMS_phi_max()}));
  //  insane::xsec::IntegratedCrossSection int_xs(xs2, ndiff);
  //  insane::xsec::PSSampler              ps_sampler(int_xs);
  //  insane::xsec::PhaseSpaceGenerator    ps_gen(ps_sampler);
  //  ps_gen.Init();
  //  std::cout << int_xs.CalculateTotalXS() << " total xs \n\n";
  //  auto res = ps_gen.Generate();
  //}

  ////insane::xsec::PhaseSpace ndiff(//Lim<Q2_v>({0.1*GeV*GeV,10.0*GeV*GeV}),
  ////                                      Lim<theta_v>({hc.SHMS_theta_min(), hc.SHMS_theta_max()}),
  ////                                      Lim<phi_v>({hc.SHMS_phi_min(), hc.SHMS_phi_max()})
  ////                                      );
  ////insane::xsec::jacobian::TransformedDifferentialCrossSection<XS_t, dsigma_dOmega, IndVars2_t> xs2(xs0);
}
