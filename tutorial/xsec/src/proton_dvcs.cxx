#include "TObject.h"
//R__LOAD_LIBRARY(libfmt.so)
#include "fmt/core.h"
#include <chrono>
#include <thread>
#include <random>
#include <iostream>

#include "insane/kinematics/DVCS.h"
#include "insane/kinematics/HallCSettings.h"

#include "insane/base/Helpers.h"
#include "insane/base/KineFuncs.h"
#include "insane/base/PhysicalConstants.h"

#include "insane/structurefunctions/fwd_SFs.h"
#include "insane/formfactors/AMTFormFactors.h"
#include "insane/xsections/Lee4HeJpsiPhotoproduction.h"

#include "insane/xsec/XSPhaseSpace.h"
#include "insane/xsec/DVCS.h"

#include <tuple>

#include "Math/GenVector/VectorUtil.h"
#include "Math/Vector3D.h"
#include "Math/AxisAngle.h"

template<typename T>
using Lim = insane::xsec::VarLimits<T>;
template<typename T>
using ULim = insane::xsec::VarLimits<T,true>;

using HallC = insane::hallc::HallCSetting;
using Lee4HeJpsiPhotoproduction = insane::physics::Lee4HeJpsiPhotoproduction;

using insane::units::GeV;
using insane::units::degree;
//using ROOT::Math::XYZTVector;

int main (int argc, char *argv[]) {

  using namespace insane::kine;
  using namespace insane::xsec::dvcs;

  // --------------------------------------------------------------------------
  // 
  HallC hc;
  hc.SHMS_p0 = 2.5;
  //
  // --------------------------------------------------------------------------
  // 
  std::random_device               rd;
  std::mt19937                     gen(rd());
  std::uniform_real_distribution<> dist(hc.SHMS_P_min() , hc.SHMS_P_max() );
  // 
  // --------------------------------------------------------------------------
  //

  //auto ind_vars = make_independent_vars<xB, Q2, t, phi_dvcs, phi>();

  auto var_stack = insane::xsec::dvcs::construct_DVCS_Kinematics().add<F1_v, F2_v>([](const auto& v) {
    static insane::physics::AMTFormFactors ffs;
    const auto&                            t = std::get<t_v>(v);
    return std::tuple<F1_v, F2_v>(ffs.F1p(-t), ffs.F2p(-t));
  });
  // ind_vars
  //    .add<M_target, M_recoil, ebeam>([](const auto& v) constexpr {
  //      const double M_p = insane::masses::M_p;
  //      return std::tuple<M_target, M_recoil, ebeam>(
  //                        M_p,      M_p,      10.6 *GeV);
  //    })
  //    .add<Delta2, xi, x>([](const auto& v) constexpr {
  //      const auto&  t  = std::get<t>(v);
  //      const auto&  xB = std::get<xB>(v);
  //      const auto&  Q2 = std::get<Q2>(v);
  //      const double x  = xB / (2.0 - xB);
  //      const double xi = xB * (1.0 + t / (2.0 * Q2)) / (2.0 - xB + xB * t / Q2);
  //      return std::tuple<Delta2,xi,x>(t,xi,x);
  //    })
  //    .add<nu>([](const auto& v) constexpr {
  //      const auto& M  = std::get<M_target>(v);
  //      const auto& Q2 = std::get<Q2>(v);
  //      const auto& xB = std::get<xB>(v);
  //      return Q2/(2.0*M*xB);
  //    })
  //    .add<eprime, theta, y>([](const auto& v) constexpr {
  //      const auto& E0 = std::get<ebeam>(v);
  //      const auto& M  = std::get<M_target>(v);
  //      const auto& nu = std::get<nu>(v);
  //      const auto& Q2 = std::get<Q2>(v);
  //      const auto& xB = std::get<xB>(v);
  //      const double y = nu / E0;
  //      return std::tuple<eprime, theta, y>(
  //          E0 - nu, 2.0 * TMath::ASin(M * xB * y / TMath::Sqrt(Q2 * (1 - y))), y);
  //    })
  //    .add<k_beam_vec, k_prime_vec, q1_vec>( // Four vectors k and k'
  //        [](const auto& v) {
  //          const auto&                  E0    = std::get<ebeam>(v);
  //          const auto&                  th    = std::get<theta>(v);
  //          const auto&                  phi   = std::get<phi>(v);
  //          const auto&                  M_p   = std::get<M_target>(v);
  //          const auto&                  Ep    = std::get<eprime>(v);
  //          const double                 sinth = std::sin(th / 2.0);
  //          const ROOT::Math::XYZTVector kbeam(0.0, 0.0, E0, E0);
  //          const ROOT::Math::XYZTVector kprime(Ep * std::cos(phi) * std::sin(th),
  //                                              Ep * std::sin(phi) * std::sin(th),
  //                                              Ep * std::cos(th), Ep);
  //          return std::tuple<k_beam_vec, k_prime_vec, q1_vec>(kbeam, kprime,
  //                                                                  kbeam - kprime);
  //        })
  //    .add<nu_cm, s, q_cm>([](const auto& v) constexpr {
  //      const auto&  E0    = std::get<ebeam>(v);
  //      const auto&  M     = std::get<M_target>(v);
  //      const auto&  nu    = std::get<nu>(v);
  //      const auto&  Q2    = std::get<Q2>(v);
  //      const auto&  xB    = std::get<xB>(v);
  //      const auto&  y     = std::get<y>(v);
  //      const double s     = M * M - Q2 + 2 * M * nu; // W^2
  //      const double nu_cm = TMath::Sqrt(TMath::Power(M * nu - Q2, 2.0) / (M * M + 2.0 * M * nu -
  //      Q2)); const double q_cm  = TMath::Sqrt(nu_cm * nu_cm + Q2); return std::tuple<nu_cm, s,
  //      q_cm>(nu_cm, s, q_cm);
  //    })
  //    .add<t_min, t_max, y_max>([](const auto& v) constexpr {
  //      using namespace insane::physics;
  //      const auto& M     = std::get<M_target>(v);
  //      const auto& s     = std::get<s>(v);
  //      const auto& xB    = std::get<xB>(v);
  //      const auto& Q2    = std::get<Q2>(v);
  //      const auto& nu_cm = std::get<nu_cm>(v);
  //      const auto& q_cm  = std::get<q_cm>(v);
  //      return std::tuple<t_min, t_max,y_max>(-Q2 - ((s - M * M) / (TMath::Sqrt(s))) * (nu_cm -
  //      q_cm),
  //                                  -Q2 - ((s - M * M) / (TMath::Sqrt(s))) * (nu_cm + q_cm),
  //                                  y_max(epsilon(Q2, xB, M)));
  //    })
  //    .add<eps>([](const auto& v) constexpr {
  //      using namespace insane::physics;
  //      const auto& M     = std::get<M_target>(v);
  //      const auto& s     = std::get<s>(v);
  //      const auto& xB    = std::get<xB>(v);
  //      const auto& Q2    = std::get<Q2>(v);
  //      const auto& nu_cm = std::get<nu_cm>(v);
  //      const auto& q_cm  = std::get<q_cm>(v);
  //      return epsilon(Q2, xB, M);
  //    })
  //    .add<q2_vec, p2_vec>( // Four vectors k and k'
  //        [](const auto& v) {
  //          const auto& M     = std::get<M_target>(v);
  //          const auto& t     = std::get<t>(v);
  //          const auto& Q2    = std::get<Q2>(v);
  //          const auto& nu    = std::get<nu>(v);
  //          const auto& phi    = std::get<phi>(v);
  //          const ROOT::Math::XYZTVector& k1 = std::get<k_beam_vec>(v);
  //          const ROOT::Math::XYZTVector& k2 = std::get<k_prime_vec>(v);
  //          const ROOT::Math::XYZTVector& q1 = std::get<q1_vec>(v);
  //          const ROOT::Math::XYZTVector  p1(0.0, 0.0, 0.0, M);
  //          const ROOT::Math::XYZVector n_gamma = k1.Vect().Cross(k2.Vect());
  //          const ROOT::Math::AxisAngle rot_to_gamma_z(n_gamma, q1.Theta());
  //          const ROOT::Math::AxisAngle rot_from_gamma_z(n_gamma,-q1.Theta());
  //          const ROOT::Math::XYZTVector  q1_B = rot_to_gamma_z(q1);
  //          const double E2_A  = (2.0*M*M-t)/(2.0*M);
  //          const double P2_A  = TMath::Sqrt(E2_A*E2_A - M*M);
  //          const double nu2_A = M + nu - E2_A;
  //          const double cosTheta_qq2_B = (t+Q2+2.0*nu*nu2_A)/(2.0*nu2_A*q1.Vect().r());
  //          const double theta_qq2_B    = TMath::ACos(cosTheta_qq2_B);
  //          const double sintheta_qp2_B = nu2_A*TMath::Sin(theta_qq2_B)/TMath::Sqrt(E2_A*E2_A -
  //          M*M); const double theta_qp2_B    = TMath::ASin(sintheta_qp2_B); const double phi_p2 =
  //          q1_B.Phi() + phi; const ROOT::Math::XYZTVector p2_B(P2_A * std::cos(phi_p2) *
  //          std::sin(theta_qp2_B),
  //                                            P2_A * std::sin(phi_p2) * std::sin(theta_qp2_B),
  //                                            P2_A * std::cos(theta_qp2_B), E2_A);
  //          const ROOT::Math::XYZTVector q2_B(nu2_A * std::cos(phi_p2+M_PI) *
  //          std::sin(theta_qq2_B),
  //                                            nu2_A * std::sin(phi_p2+M_PI) *
  //                                            std::sin(theta_qq2_B), nu2_A *
  //                                            std::cos(theta_qq2_B), nu2_A);
  //          return std::tuple<q2_vec, p2_vec>(rot_from_gamma_z(q2_B),rot_from_gamma_z(p2_B));
  //        })
  //    .add<F1, F2>([](const auto& v) {
  //      static insane::physics::AMTFormFactors ffs;
  //      const auto& Q2    = std::get<Q2>(v);
  //      return std::tuple<F1,F2>(ffs.F1p(Q2/GeV/GeV),ffs.F2p(Q2/GeV/GeV));
  //    });

  // double nu      = ;
  // double y       = nu / E0;
  // double eprime  = E0 - nu;
  // double theta_e = 2.0 * ASin(M * xBjorken * y / Sqrt(Q2 * (1 - y)));
  // double s       = M * M - Q2 + 2 * M * nu; // W^2
  // double nu_cm   = Sqrt(Power(M * nu - Q2, 2.0) / (M * M + 2.0 * M * nu - Q2));
  // double q_cm    = Sqrt(nu_cm * nu_cm + Q2);
  // double ymax    = y_max(epsilon(Q2, xBjorken, M));

  // std::cout << t_min << " > t=" << t << " > " <<  t_max << std::endl;
  // std::cout << "Delta2_min " << Delta2_min(Q2,xBjorken,M) <<std::endl;
  // std::cout << ymax << " > y=" << y << std::endl;
  // if (t < t_max) {
  // return nullptr;
  //}
  // if(t>t_min) {
  //   return nullptr;
  //}
  // if( (y>ymax) || (y>1.0)){
  //   return nullptr;
  //}
  // if(std::isnan(theta_e)) return nullptr;

  // TVector3 k1(0, 0, fBeamEnergy); // incident electron
  // TVector3 k2(0, 0, eprime);          // scattered electron
  // k2.SetMagThetaPhi(eprime, theta_e, phi_e);
  // TVector3 q1 = k1 - k2;
  // TVector3 n_gamma = k1.Cross(k2);
  // n_gamma.SetMag(1.0);
  // TVector3 q1_B = q1;
  // q1_B.Rotate(q1.Theta(), n_gamma);
  ////q1_B.Print();

  // double E2_A  = (2.0*MT*MT-t)/(2.0*MT);
  // double P2_A  = Sqrt(E2_A*E2_A - MT*MT);
  // double nu2_A = MT + nu - E2_A;
  // double cosTheta_qq2_B = (t+Q2+2.0*nu*nu2_A)/(2.0*nu2_A*q1.Mag());
  // double theta_qq2_B    = ACos(cosTheta_qq2_B);
  // double sintheta_qp2_B = nu2_A*Sin(theta_qq2_B)/Sqrt(E2_A*E2_A - MT*MT);
  // double theta_qp2_B    = ASin(sintheta_qp2_B);
  // double phi_p2 = q1_B.Phi() + phi;
  ////std::cout << "P2_A = " << P2_A << std::endl;
  ////std::cout << "E2_A = " << E2_A << std::endl;
  ////std::cout << "t     = " << t << std::endl;
  ////std::cout << "t_min = " << Delta2_min(Q2,xBjorken) << std::endl;
  // TVector3 p2 = {0,0,1} ;
  // TVector3 q2 = {0,0,1} ;
  // p2.SetMagThetaPhi(P2_A, theta_qp2_B,phi_p2);
  // q2.SetMagThetaPhi(nu2_A,theta_qq2_B,phi_p2+180.0*degree);

  // q2.Rotate(-q1.Theta(), n_gamma);
  // p2.Rotate(-q1.Theta(), n_gamma);
  // q1.Rotate(-q1.Theta(), n_gamma);

  // auto net_p = (k1 - k2 - p2 - q2);
  // if( net_p.Mag() > 0.001 ) {
  //   //net_p.Print();
  //   return nullptr;
  //}
  //using IndVars_t  = typename insane::xsec::dvcs::DVCSExpVars;//std::tuple<xB, Q2, phi_dvcs, phi>;
  using IndVars_t  = typename insane::xsec::dvcs::DVCSPrimaryVars;//std::tuple<xB, Q2, phi_dvcs, phi>;
  //using VarStack_t = decltype(var_stack);
  //using RetVar_t   = typename VarStack_t::Vars_t;
  //using IndVars2_t = std::tuple<theta, phi>;

  auto& xs0 = insane::xsec::dvcs::ep_unpolarized_DVCS_dsigma;
  using XS_t = decltype(xs0);


  auto ind_vars = IndVars_t(11.0, 0.42, 2.03, -0.351, 5 * degree, 0 * degree,0 * degree);
  auto v0         = var_stack(ind_vars);

  insane::helpers::print_tuple(v0);

  auto fixed_vars = std::make_tuple(E_beam_v{11.0});
  auto diff_vars  = std::make_tuple(std::get<xB_v>(ind_vars), std::get<Q2_v>(ind_vars), std::get<t_v>(ind_vars), std::get<phi_dvcs_v>(ind_vars), phi_S_v{0 * degree}, phi_v{0 * degree});

  std::cout << " ===========================================\n";
  std::cout << " Q2               : " << std::get<Q2_v>(v0) << " GeV^{2}\n";
  std::cout << " ===========================================\n";
  std::cout << " p2_lab^2         : " << std::get<p2_lab_vec>(v0).get().M() << " GeV\n";
  std::cout << " p1_lab^2         : " << std::get<p1_lab_vec>(v0).get().M() << " GeV\n";
  std::cout << " q2_lab^2         : " << std::get<q2_lab_vec>(v0).get().M() << " GeV\n";
  std::cout << " k2_lab^2         : " << std::get<k2_lab_vec>(v0).get().M() << " GeV\n";
  std::cout << " k1_lab^2         : " << std::get<k1_lab_vec>(v0).get().M() << " GeV\n";
  std::cout << " k1_lab           : " << std::get<k1_lab_vec>(v0).get() << " GeV\n";
  std::cout << " k2_lab           : " << std::get<k2_lab_vec>(v0).get() << " GeV\n";
  std::cout << " q1_lab           : " << std::get<q1_lab_vec>(v0).get() << " GeV\n";
  std::cout << " q2_lab           : " << std::get<q2_lab_vec>(v0).get() << " GeV\n";
  std::cout << " p1_lab           : " << std::get<p1_lab_vec>(v0).get() << " GeV\n";
  std::cout << " p2_lab           : " << std::get<p2_lab_vec>(v0).get() << " GeV\n";
  std::cout << " ===========================================\n";
  std::cout << " k1               : " << std::get<k1_vec>(v0).get() << " \n";
  std::cout << " k2               : " << std::get<k2_vec>(v0).get() << " \n";
  std::cout << " q1               : " << std::get<q1_vec>(v0).get() << " \n";
  std::cout << " k1 - k2          : " << std::get<k1_vec>(v0).get() - std::get<k2_vec>(v0).get() << " \n";
  std::cout << " (k1 - k2)          : " << (std::get<k1_vec>(v0).get() - std::get<k2_vec>(v0).get()).M2() << " \n";
  std::cout << " q2               : " << std::get<q2_vec>(v0).get() << " \n";
  std::cout << " p1               : " << std::get<p1_vec>(v0).get() << " \n";
  std::cout << " p2               : " << std::get<p2_vec>(v0).get() << " \n";
  std::cout << " k2^2             : " << std::get<k2_vec>(v0).get().M() << " GeV\n";
  std::cout << " k1^2             : " << std::get<k1_vec>(v0).get().M() << " GeV\n";
  std::cout << " p2^2             : " << std::get<p2_vec>(v0).get().M() << " GeV\n";
  std::cout << " p1^2             : " << std::get<p1_vec>(v0).get().M() << " GeV\n";
  std::cout << " q2^2             : " << std::get<q2_vec>(v0).get().M() << " GeV\n";
  std::cout << " q1^2             : " << std::get<q1_vec>(v0).get().M2() << " GeV\n";
  std::cout << " ===========================================\n";
  std::cout << " t (p1 - p2)^2 lab: " << (std::get<p1_lab_vec>(v0).get() - std::get<p2_lab_vec>(v0).get()).M2() << "\n";
  std::cout << " t (q1 - q2)^2 lab: " << (std::get<q1_lab_vec>(v0).get() - std::get<q2_lab_vec>(v0).get()).M2() << "\n";
  std::cout << " t (p1 - p2)^2    : " << (std::get<p1_vec>(v0).get()     - std::get<p2_vec>(v0).get()).M2() << "\n";
  std::cout << " t (q1 - q2)^2    : " << (std::get<q1_vec>(v0).get()     - std::get<q2_vec>(v0).get()).M2() << "\n";
  std::cout << " ===========================================\n";
  std::cout << " s (p1 + k1)^2 lab: " << (std::get<p1_lab_vec>(v0).get() + std::get<k1_lab_vec>(v0).get()).M2() << "\n";
  std::cout << " s (p2 + k2 + q2)^2 lab: " << (std::get<p2_lab_vec>(v0).get() + std::get<q2_lab_vec>(v0).get()+ std::get<k2_lab_vec>(v0).get()).M2() << "\n";
  std::cout << " s (p1 + k1)^2    : " << (std::get<p1_vec>(v0).get()     + std::get<k1_vec>(v0).get()).M2() << "\n";
  std::cout << " s (p2 + k2 + q2)^2    : " << (std::get<p2_vec>(v0).get()     + std::get<k2_vec>(v0).get()+ std::get<q2_vec>(v0).get()).M2() << "\n";
  std::cout << " ===========================================\n";
  std::cout << " s (p1 + q1)^2 lab: " << (std::get<p1_lab_vec>(v0).get() + std::get<q1_lab_vec>(v0).get()).M2() << "\n";
  std::cout << " s (p2 + q2)^2 lab: " << (std::get<p2_lab_vec>(v0).get() + std::get<q2_lab_vec>(v0).get()).M2() << "\n";
  std::cout << " s (p1 + q1)^2    : " << (std::get<p1_vec>(v0).get()     + std::get<q1_vec>(v0).get()).M2() << "\n";
  std::cout << " s (p2 + q2)^2    : " << (std::get<p2_vec>(v0).get()     + std::get<q2_vec>(v0).get()).M2() << "\n";
  std::cout << " ===========================================\n";
  std::cout << " cross section (1): " << xs0(v0) << "\n";
  std::cout << " cross section (2): " << xs0(var_stack, ind_vars) << "\n";
  std::cout << " cross section (3): " << xs0(var_stack, fixed_vars, diff_vars) << "\n";

  fmt::print(" {:>20} =  {:10.5f}\n", "F1",     std::get<F1_v>(v0));
  fmt::print(" {:>20} =  {:10.5f}\n", "F2",     std::get<F2_v>(v0));
  fmt::print(" {:>20} =  {:10.5f}\n", "Q/(y*gamma)",    std::sqrt(std::get<Q2_v>(v0))/(std::get<y_v>(v0)*std::get<gamma_v>(v0)));
  fmt::print(" {:>20} =  {:10.5f}\n", "Q2",    std::get<Q2_v>(v0));
  fmt::print(" {:>20} =  {:10.5f}\n", "y",     std::get<y_v>(v0));
  fmt::print(" {:>20} =  {:10.5f}\n", "s",     std::get<s_v>(v0));
  fmt::print(" {:>20} =  {:10.5f}\n", "W",     std::get<W_v>(v0));
  fmt::print(" {:>20} =  {:10.5f}\n", "nu",    std::get<nu_v>(v0));
  fmt::print(" {:>20} =  {:10.5f}\n", "t_min", std::get<t_min_v>(v0));
  fmt::print(" {:>20} =  {:10.5f}\n", "t_max", std::get<t_max_v>(v0));
  fmt::print(" {:>20} =  {:10.5f}\n", "y_max", std::get<y_max_v>(v0));
  fmt::print(" {:>20} =  {:10.5f}\n", "gamma", std::get<gamma_v>(v0));
  fmt::print(" {:>20} =  {:10.5f}\n", "theta_q2", std::get<theta_q2_v>(v0)*180/M_PI);

  {
    double gamma = std::get<gamma_v>(v0);
    double t     = std::get<t_v>(v0);
    double Q2    = std::get<Q2_v>(v0);
    double xB    = std::get<xB_v>(v0);
    fmt::print(" {:>20} =  {:10.5f}\n", "cos(theta_q2)",
               (-1.0 / std::sqrt(1.0 + gamma * gamma)) *
                   (1.0 + (gamma * gamma / 2.0) * (1.0 + t / Q2) / (1.0 + xB * t / Q2)));
    fmt::print(" {:>20} =  {:10.5f}\n", "|q2|", (std::sqrt(Q2)/gamma)*(1.0+ xB*t/Q2));
  }
  //PrintDVCSKinematics(v0);
  //std::cout << " cross section : "
  //          << insane::xsec::dvcs::ep_unpolarized_DVCS_dsigma(var_stack, input) << "\n";

  //{
  //  insane::xsec::PhaseSpace ndiff(Lim<xB_v>({0.0001, 0.999}), Lim<t_v>({-0.5, -0.1}),
  //                                 Lim<Q2_v>({1.0, 8.0}), Lim<phi_dvcs_v>({0, 2.0 * M_PI}),
  //                                 Lim<phi_v>({hc.SHMS_phi_min(), hc.SHMS_phi_max()}));
  //  insane::xsec::TotalCrossSection<decltype(insane::xsec::dvcs::ep_unpolarized_DVCS_dsigma),
  //                                  decltype(ndiff)>
  //      tot_xs(insane::xsec::dvcs::ep_unpolarized_DVCS_dsigma, ndiff);
  //  std::cout << tot_xs.CalculateTotalXS(var_stack) << " total xs \n\n";
  //  insane::xsec::PhaseSpaceSampler<decltype(tot_xs)> ps_sampler(tot_xs);
  //  std::cout << ps_sampler.Init(var_stack) << " total xs (FOAM)\n";
  //  // auto ev = ps_sampler.Generate();
  //  // auto ev2 = ps_sampler.Generate(var_stack);
  //}
}
