//R__LOAD_LIBRARY(libfmt.so)
#include "fmt/core.h"
//R__LOAD_LIBRARY(librwc.so)
#include "rwc/DisplayServer.h"
#include "rwc/MonitoringDisplay.h"
#include <chrono>
#include <thread>
#include <random>
#include <iostream>
//R__LOAD_LIBRARY(libinsaneCore.so)
#include "insane/kinematics/Core.h"
#include "insane/kinematics/Inelastic.h"
#include "insane/kinematics/HallCSettings.h"
//#include "insane/new_xsec/PhaseSpaceVariables.h"
//#include "insane/new_xsec/NFoldDifferential.h"
//#include "insane/new_xsec/InitialState.h"
//#include "insane/new_xsec/Jacobians.h"
//#include "insane/new_xsec/DiffCrossSection.h"
//#include "insane/new_xsec/PSSampler.h"

#include "insane/base/PhysicalConstants.h"

#include "insane/structurefunctions/fwd_SFs.h"
#include "insane/formfactors/DipoleFormFactors.h"
#include "insane/xsections/Lee4HeJpsiPhotoproduction.h"

#include "insane/xsec/XSPhaseSpace.h"
#include "insane/xsec/ElasticScatteringOffNuclei.h"

#include <tuple>
//#include "ROOT/RDataFrame.hxx"

#include "Math/GenVector/VectorUtil.h"
#include "Math/Vector3D.h"
#include "TGraph.h"
#include "TMultiGraph.h"

template<typename T>
using Lim = insane::xsec::VarLimits<T>;
template<typename T>
using ULim = insane::xsec::VarLimits<T,true>;

using HallC = insane::hallc::HallCSetting;
using Lee4HeJpsiPhotoproduction = insane::physics::Lee4HeJpsiPhotoproduction;

using insane::units::GeV;
using insane::units::degree;
//using ROOT::Math::XYZTVector;

int main (int argc, char *argv[]) {

  using namespace insane::kine;
  using namespace insane::kine::var;
  using namespace insane::xsec::elastic_4He;

  // --------------------------------------------------------------------------
  // 
  HallC hc;
  hc.SHMS_p0 = 2.5;
  //
  // --------------------------------------------------------------------------
  // 
  std::random_device               rd;
  std::mt19937                     gen(rd());
  std::uniform_real_distribution<> dist(hc.SHMS_P_min() * GeV, hc.SHMS_P_max() * GeV);
  // 
  // --------------------------------------------------------------------------
  //
  //
  auto ind_vars =     make_independent_vars<V_p_recoil, V_theta_recoil, V_phi_recoil>();
  auto var_stack =
      ind_vars
          .add<V_M_target, V_M_recoil, V_ebeam>([](const auto& v) constexpr {
            const double M = insane::masses::M_4He;
            return std::tuple<V_M_target, V_M_recoil, V_ebeam>(M, M, 10.6 * GeV);
          })
          .add<V_E_recoil>([](const auto& v) constexpr {
            const auto&  M       = std::get<V_M_target>(v);
            const auto&  P_alpha = std::get<V_p_recoil>(v);
            return std::sqrt(P_alpha * P_alpha + M * M);
          })
          .add<V_Es, V_eprime, V_theta, V_phi>([](const auto& v) constexpr {
            using namespace insane::xsec::elastic_4He;
            // Es is the beam energy after some energy loss
            // it is determined from the elastic recoil momentum vector
            const auto&  E0          = std::get<V_ebeam>(v);
            const auto&  M           = std::get<V_M_target>(v);
            const auto&  P_alpha     = std::get<V_p_recoil>(v);
            const auto&  E_alpha     = std::get<V_E_recoil>(v);
            const auto&  theta_alpha = std::get<V_theta_recoil>(v);
            const auto&  phi_alpha   = std::get<V_phi_recoil>(v);
            const double Es          = M * P_alpha / ((M + E_alpha) * std::cos(theta_alpha) - P_alpha);
            const double theta       = GetTheta_e(Es, theta_alpha, M);
            const double Eprime = Es / (1.0 + 2.0 * Es / M * std::pow(std::sin(theta / 2.0), 2));
            const double phi_e  = phi_alpha + M_PI;
            return std::tuple<V_Es, V_eprime, V_theta, V_phi>(Es, Eprime, theta, phi_e);
          })
          .add<V_k_beam_vec, V_k_prime_vec, V_q_vec, V_p_recoil_vec>( // Four vectors k and k'
              [](const auto& v) {
                const auto&                  E0     = std::get<V_ebeam>(v);
                const auto&                  M      = std::get<V_M_target>(v);
                const auto&                  Ep     = std::get<V_eprime>(v);
                const auto&                  th     = std::get<V_theta>(v);
                const auto&                  phi    = std::get<V_phi>(v);
                const auto&                  Prec   = std::get<V_p_recoil>(v);
                const auto&                  Erec   = std::get<V_E_recoil>(v);
                const auto&                  threc  = std::get<V_theta_recoil>(v);
                const auto&                  phirec = std::get<V_phi_recoil>(v);
                const double                 sinth  = std::sin(th / 2.0);
                const ROOT::Math::XYZTVector kbeam(0.0, 0.0, E0, E0);
                const ROOT::Math::XYZTVector kprime(Ep * std::cos(phi) * std::sin(th),
                                                    Ep * std::sin(phi) * std::sin(th),
                                                    Ep * std::cos(th), Ep);
                const ROOT::Math::XYZTVector p_rec(Prec * std::cos(phirec) * std::sin(threc),
                                                   Prec * std::sin(phirec) * std::sin(threc),
                                                   Prec * std::cos(threc), Erec);
                return std::tuple<V_k_beam_vec, V_k_prime_vec, V_q_vec, V_p_recoil_vec>(
                    kbeam, kprime, kbeam - kprime, p_rec);
              })
          .add<V_Q2>([](const auto& v) constexpr {
            const ROOT::Math::XYZTVector& k0 = std::get<V_k_beam_vec>(v);
            const ROOT::Math::XYZTVector& kp = std::get<V_k_prime_vec>(v);
            return -1.0 * (k0 - kp).mag2();
          })
          .add<V_nu, V_y, V_tau>([](const auto& v) constexpr {
            const auto& E0 = std::get<V_ebeam>(v);
            const auto& Ep = std::get<V_eprime>(v);
            const auto& M  = std::get<V_M_target>(v);
            const auto& Q2 = std::get<V_Q2>(v);
            return std::tuple<V_nu, V_y, V_tau>(E0 - Ep, (E0 - Ep) / E0, Q2 / (4.0 * M * M));
          })
          .add<V_FCHe4>([](const auto& v) {
            using namespace insane::physics;
            const auto& Q2 = std::get<V_Q2>(v) / GeV / GeV;
            const auto& E0 = std::get<V_ebeam>(v) / GeV;
            return std::pow(E0,-11.949831224715918*Q2 + 1.3*std::pow(Q2,2) - 0.005*std::pow(Q2,4))*(1 - 0.070344300625*std::pow(Q2,4))*(1 - 112.77389088529493*std::pow(Q2,5));
            //static DipoleFormFactors ffs;
            //return ffs.FCHe4(Q2 );
          });

  using IndVars_t = std::tuple<V_p_recoil, V_theta_recoil, V_phi_recoil>;
  auto input      = IndVars_t(1.0*GeV, 50.0 * degree, 0.0);
  auto v0         = var_stack(input);
  std::cout << " ===========================================\n";
  std::cout << " cross section : " << insane::xsec::elastic_4He::e_4He_ElasticTail_dsigma(v0) << "\n";
  std::cout << " Q2            : " << std::get<V_Q2>(v0)/GeV/GeV << " GeV^{2}\n";
  //std::cout << " cross section : " << insane::xsec::elastic::e_4He_Elastic_dsigma(var_stack,input) << "\n";
  {
    insane::xsec::PhaseSpace ndiff(Lim<V_p_recoil>({0.01*GeV, 5.0*GeV}),
                                   Lim<V_theta_recoil>({20 * degree, 50.0 * degree}),
                                   Lim<V_phi_recoil>({0.0, 2.0 * M_PI}));
    insane::xsec::TotalCrossSection<decltype(insane::xsec::elastic_4He::e_4He_ElasticTail_dsigma),
                                    decltype(ndiff)>
        tot_xs(insane::xsec::elastic_4He::e_4He_ElasticTail_dsigma, ndiff);
    std::cout << tot_xs.CalculateTotalXS(var_stack) << " total xs \n\n";
    insane::xsec::PhaseSpaceSampler<decltype(tot_xs)>  ps_sampler(tot_xs);
    std::cout << ps_sampler.Init(var_stack) << " total xs (FOAM)\n";
    auto ev = ps_sampler.Generate();
  }

  //insane::xsec::jacobian::TransformedDifferentialCrossSection<decltype(xs0),dsigma_dOmega,IndVars2_t> xs2(xs0);
  ////auto input2 = IndVars_t(0.1*GeV*GeV, 0.0 );
  //std::cout << " cross section : " << xs2(input) << "\n";
  //auto res =  xs2.ComputeValues(input);
  //std::cout << " Q2 = " << std::get<Q2_v>(res)/GeV/GeV << "\n";
  TMultiGraph* mg = new TMultiGraph();
  TMultiGraph* mg_Fc = new TMultiGraph();
  TMultiGraph* mg_xs_vs_Q2 = new TMultiGraph();

  auto   kine0   = IndVars_t(1.0 * GeV, 40.0 * degree, 0.0);
  int    Npoints = 100;
  double delta_p = (0.8 - 0.1) / Npoints;
  double delta_th = (50.0 - 15.0) / 20;
  for(int it = 0; it < 20 ; it++) {
    V_theta_recoil th2{(15.0*degree + it * delta_th*degree)};
    std::get<V_theta_recoil>(kine0) = th2;

    TGraph* gr          = new TGraph();
    TGraph* gr_Fc       = new TGraph();
    TGraph* gr_xs_vs_Q2 = new TGraph();

    for (int ip = 0; ip < 100; ip++) {
      V_p_recoil p2{(0.1 + ip * delta_p) * GeV};
      std::get<V_p_recoil>(kine0) = p2;

      // ----------------
      auto         v0    = var_stack(kine0);
      const double Q2    = std::get<V_Q2>(v0)/GeV/GeV;
      const double FCHe4 = std::get<V_FCHe4>(v0);
      double      xs    = insane::xsec::elastic_4He::e_4He_ElasticTail_dsigma(v0);
      gr->SetPoint(ip, p2/GeV, xs);
      gr_Fc->SetPoint(ip, Q2/(0.197*0.197), std::abs(FCHe4));
      gr_xs_vs_Q2->SetPoint(ip, Q2/(0.197*0.197), xs);
    }
    gr->SetTitle(fmt::format("#theta = {:.2f} deg", double(th2/degree)).c_str());
    gr_Fc->SetTitle(fmt::format("#theta = {:.2f} deg", double(th2/degree)).c_str());
    gr_xs_vs_Q2->SetTitle(fmt::format("#theta = {:.2f} deg", double(th2/degree)).c_str());
    mg->Add(gr,"lp");
    mg_Fc->Add(gr_Fc,"lp");
    mg_xs_vs_Q2->Add(gr_xs_vs_Q2,"lp");
  }

  auto web_display = new rwc::MonitoringDisplay();
  web_display->SetRootFolder(std::string("/elastic_4He_tail"));

  web_display->CreateDisplayPlot("/cross_section", "xsec_vs_Precoil",
    [&](rwc::DisplayPlot& plt) {
      auto c = plt.GetCanvas();
      mg->Draw("a PLC");
      gPad->BuildLegend(0.44,0.8,0.74,0.999);
      return 0;
    });
  web_display->CreateDisplayPlot("/cross_section", "xsec_vs_Q2",
    [&](rwc::DisplayPlot& plt) {
      auto c = plt.GetCanvas();
      mg_xs_vs_Q2->Draw("a PLC");
      gPad->BuildLegend(0.44,0.8,0.74,0.999);
      return 0;
    });
  web_display->CreateDisplayPlot("/formfactor", "Fc4He",
    [&](rwc::DisplayPlot& plt) {
      auto c = plt.GetCanvas();
      mg_Fc->Draw("a PLC");
      gPad->BuildLegend(0.44,0.8,0.74,0.999);
      return 0;
    });

  web_display->InitAll();
  web_display->UpdateAll();
  std::quick_exit(0);
}
