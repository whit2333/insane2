R__LOAD_LIBRARY(libinsaneCore.so)
R__LOAD_LIBRARY(libinsanePhysics.so)

#include "fmt/core.h"
#include <chrono>
#include <thread>
#include <random>
#include <iostream>

#include "insane/kinematics/Elastic.h"

#include "insane/base/PhysicalConstants.h"

#include "insane/structurefunctions/fwd_SFs.h"
#include "insane/formfactors/AMTFormFactors.h"

#include "insane/xsec/XSPhaseSpace.h"
#include "insane/xsec/ElasticScattering.h"

#include "Math/GenVector/VectorUtil.h"
#include "Math/Vector3D.h"

#include "insane/base/Helpers.h"
#include "insane/base/Graphs.h"

template<typename T>
using Lim = insane::xsec::VarLimits<T>;

template<typename T>
using ULim = insane::xsec::VarLimits<T,true>;

using insane::units::GeV;
using insane::units::degree;

void plot_elastic_ep() {

  double beam_energy = 10.0;

  using namespace insane::kine;
  using namespace insane::kine::elastic;

  auto var_stack = insane::kine::elastic::construct_Elastic_variables()
          .add<insane::xsec::elastic::dsigma_dOmega_v>([](const auto& v) {
            using namespace insane::physics;
            static AMTFormFactors ffs;
            const auto&           E0    = std::get<E_beam_v>(v);
            const auto&           Ep    = std::get<E_prime_v>(v);
            const auto&           M     = std::get<M_target_v>(v);
            const auto&           Q2    = std::get<Q2_v>(v);
            const auto&           tau   = std::get<tau_v>(v);
            const auto&           theta = std::get<theta_v>(v);
            if (Ep > E0)
              return 0.0;
            double mottXSec      = insane::Kine::Sig_Mott(E0/GeV, theta);
            double recoil_factor = Ep / E0;
            double GE2           = std::pow(ffs.GEp(Q2/GeV/GeV), 2.0);
            double GM2           = std::pow(ffs.GMp(Q2/GeV/GeV), 2.0);
            double tanthetaOver2 = std::tan(theta / 2.0);
            // Rosenbluth formula
            double res =
                mottXSec * recoil_factor *
                ((GE2 + tau * GM2) / (1.0 + tau) + 2.0 * tau * GM2 * tanthetaOver2 * tanthetaOver2);
            res = res * hbarc2_gev_nb;
            return (res);
          });

  auto func = [&](const double& x) {
    using VarStack_t = decltype(var_stack);
    using IndVars_t  = typename VarStack_t::IndependentVariables_t;
    auto vals        = var_stack.ComputeValues(IndVars_t(E_prime_v{beam_energy}, theta_v{x}, phi_v{0.0}));
    return std::make_tuple(std::get<insane::xsec::elastic::dsigma_dOmega_v>(vals), 0.0);
  };
  std::cout << std::get<0>(func(0.1)) << "\n";

  //auto gr = insane::build_graph_error_func(func,0.1,1.0);

  //int N_points = 100;
  //double              dx = (x_max - x_min) / double(N_points - 1);
  //auto                gr = new TGraphErrors(N_points);
  //std::vector<double> v(N_points, 0.0);
  //std::generate(v.begin(), v.end(), [&x_min, &dx, n = 0]() mutable {
  //  double val = x_min + double(n) * dx;
  //  n++;
  //  return val;
  //});
  //std::vector<int> ip(N_points);
  //std::for_each(v.begin(), v.end(), [&, n = 0](double x) mutable {
  //  auto vals = func(x);
  //  gr->SetPoint(n, x, std::get<0>(vals));
  //  gr->SetPointError(n, 0.0, std::get<1>(vals));
  //  n++;
  //});

  //gr->Draw("alp");

  //using VarStack_t = decltype(var_stack);
  //using IndVars_t  = typename VarStack_t::IndependentVariables_t;
  //using RetVar_t   = typename VarStack_t::Vars_t;

  //auto xs0 = insane::xsec::make_dsigma<insane::xsec::elastic::dsigma_dOmega_v>(
  //    [](const auto& v) { return std::get<insane::xsec::elastic::dsigma_dOmega_v>(v); });

  ////insane::xsec::DifferentialCrossSection<VarStack_t, insane::xsec::elastic::dsigma_dOmega_v, IndVars_t> xs0(var_stack);
  //using XS_t = decltype(xs0);

  //double E0    = 10.0;
  //double theta = 12.0 * degree;

  //auto ind_vars   = IndVars_t(E0, theta, 0.0);
  //auto v0         = var_stack.ComputeValues(ind_vars);
  //auto fixed_vars = std::make_tuple(E_beam_v{E0});
  //auto diff_vars  = std::make_tuple(theta_v{theta}, phi_v{0.0});

  //std::cout << " ===========================================\n";
  //std::cout << " Q2               : " << std::get<Q2_v>(v0) << " GeV^{2}\n";
  //std::cout << " cross section (1): " << xs0(v0) << "\n";
  //std::cout << " cross section (2): " << xs0(var_stack, ind_vars) << "\n";
  //std::cout << " cross section (3): " << xs0(var_stack, fixed_vars, diff_vars) << "\n";

  //{
  //  insane::xsec::PhaseSpace ndiff(//Lim<Q2_v>({0.1*GeV*GeV,10.0*GeV*GeV}), 
  //                                        Lim<theta_v>({hc.SHMS_theta_min(), hc.SHMS_theta_max()}),
  //                                        Lim<phi_v>({hc.SHMS_phi_min(), hc.SHMS_phi_max()}));
  //  //insane::xsec::IntegratedCrossSection int_xs(xs0, ndiff);
  //  auto tot_xs = insane::xsec::make_total_xs(xs0, ndiff, fixed_vars);
  //  std::cout << tot_xs.CalculateTotalXS(var_stack) << " total xs \n\n";
  //  auto ps_sampler = insane::xsec::make_ps_sampler(tot_xs);
  //  std::cout << " pss init xs = " <<  ps_sampler.Init(var_stack) << "\n";
  //  //std::cout << int_xs.CalculateTotalXS() << " total xs \n\n";
  //}

  ////insane::xsec::jacobian::TransformedDifferentialCrossSection<decltype(xs0),dsigma_dOmega,IndVars2_t> xs2(xs0);
  //////auto input2 = IndVars_t(0.1*GeV*GeV, 0.0 );
  ////std::cout << " cross section : " << xs2(input) << "\n";
  ////auto res =  xs2.ComputeValues(input);
  ////std::cout << " Q2 = " << std::get<Q2_v>(res)/GeV/GeV << "\n";

  ////{
  ////  auto jm = insane::xsec::jacobian::Jacobian<std::tuple<Q2_v, phi_v>, std::tuple<theta_v, phi_v>,
  ////                                             VarStack_t>(var_stack);
  ////  std::cout << " det = " << jm.Det(input) << "\n";

  ////  auto jm2 = insane::xsec::jacobian::Jacobian<std::tuple<theta_v, phi_v>, std::tuple<Q2_v, phi_v>,
  ////                                              VarStack_t>(var_stack);
  ////  std::cout << " det = " << jm2.Det(input) << "\n";

  ////}
  //////{
  //////  insane::xsec::PhaseSpace ndiff(//Lim<Q2_v>({0.1*GeV*GeV,10.0*GeV*GeV}), 
  //////                                        Lim<theta_v>({hc.SHMS_theta_min(), hc.SHMS_theta_max()}),
  //////                                        ULim<phi_v>({hc.SHMS_phi_min(), hc.SHMS_phi_max()}));
  //////  insane::xsec::IntegratedCrossSection int_xs(xs2, ndiff);
  //////  insane::xsec::PSSampler  ps_sampler(int_xs);
  //////  ps_sampler.Init();

  //////  std::cout << int_xs.CalculateTotalXS() << " total xs \n\n";
  //////}

  ////{
  ////  insane::xsec::PhaseSpace ndiff(//Lim<Q2_v>({0.1*GeV*GeV,10.0*GeV*GeV}), 
  ////                                        Lim<theta_v>({hc.SHMS_theta_min(), hc.SHMS_theta_max()}),
  ////                                        ULim<phi_v>({hc.SHMS_phi_min(), hc.SHMS_phi_max()}));
  ////  insane::xsec::IntegratedCrossSection int_xs(xs2, ndiff);
  ////  insane::xsec::PSSampler              ps_sampler(int_xs);
  ////  insane::xsec::PhaseSpaceGenerator    ps_gen(ps_sampler);
  ////  ps_gen.Init();
  ////  std::cout << int_xs.CalculateTotalXS() << " total xs \n\n";
  ////  auto res = ps_gen.Generate();
  ////}



  //////insane::xsec::PhaseSpace ndiff(//Lim<Q2_v>({0.1*GeV*GeV,10.0*GeV*GeV}), 
  //////                                      Lim<theta_v>({hc.SHMS_theta_min(), hc.SHMS_theta_max()}),
  //////                                      Lim<phi_v>({hc.SHMS_phi_min(), hc.SHMS_phi_max()})
  //////                                      );
  //////insane::xsec::jacobian::TransformedDifferentialCrossSection<XS_t, dsigma_dOmega, IndVars2_t> xs2(xs0);
    root_app.Run(); 
}
