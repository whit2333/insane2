Int_t fermi_dist(Int_t aNumber = 0)
{
   using namespace insane::physics;

   FermiMomentumDist fermi_dist;

   // --------------------------------

   TCanvas     * c   = 0;//new TCanvas();
   TGraph      * gr  = 0;
   TLegend     * leg = 0;
   TMultiGraph * mg  = 0;

   // ---------------------------------
   // n(k)
   auto nHe4 = [=](double* xs, double *ps) {
      double p1 = xs[0];
      return fermi_dist.n(p1,4);
   };
   TF1 * n_He4 = new TF1("nHe4", nHe4, 0.0,1.0,0);
   leg = new TLegend(0.7,0.7,0.89,0.89);
   mg  = new TMultiGraph();
   c   = new TCanvas();
   c->cd(0);
   gPad->SetLogy(true);
   n_He4->SetLineColor(1);
   n_He4->SetLineWidth(2);
   gr = new TGraph( n_He4->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"^{4}He","l");

   mg->Draw("a");
   mg->GetYaxis()->SetTitle("n(k) [GeV^{-3}]");
   mg->GetXaxis()->SetTitle("k [GeV]");
   leg->Draw();
   c->SaveAs(Form("results/cross_sections/fermi_dist_n_%d.pdf",aNumber));

   auto knHe4 = [=](double* xs, double *ps) {
      double p1 = xs[0];
      return p1*p1*fermi_dist.n(p1,4);
   };
   TF1 * kn_He4 = new TF1("knHe4", knHe4, 0.0,1.0,0);
   std::cout << "integral k*n(k,4) : " <<  4.0*CLHEP::pi*kn_He4->Integral(0.0,10.0) << std::endl;

   // ---------------------------------
   // P(k)
   auto PHe4 = [=](double* xs, double *ps) {
      double p1 = xs[0];
      return fermi_dist.Evaluate(p1,4);
   };
   TF1 * P_He4 = new TF1("PHe4", PHe4, 0.0,1.0,0);
   leg = new TLegend(0.7,0.7,0.89,0.89);
   mg  = new TMultiGraph();
   c   = new TCanvas();
   c->cd(0);
   gPad->SetLogy(true);

   P_He4->SetLineColor(1);
   P_He4->SetLineWidth(2);
   gr = new TGraph( P_He4->DrawCopy("goff")->GetHistogram());
   mg->Add(gr,"l");
   leg->AddEntry(gr,"^{4}He","l");

   mg->Draw("a");
   mg->GetYaxis()->SetTitle("P(k)");
   mg->GetXaxis()->SetTitle("k [GeV]");
   leg->Draw();

   std::cout << "integral (4) : " <<  P_He4->Integral(0.0,10.0) << std::endl;

   c->SaveAs(Form("results/cross_sections/fermi_dist_P_%d.pdf",aNumber));

   return(0);
}
