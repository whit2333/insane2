#include "TGraph.h"
#include "TMultiGraph.h"
#include "TF1.h"
#include "TF2.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TLatex.h"

#include "NucDB/Manager.h"
R__LOAD_LIBRARY(libNucDB.so);

using namespace nucdb;


#include "insane/structurefunctions/AllSSFs.h"
#include "insane/structurefunctions/SSFsFromVPACs.h"
#include "insane/xsections/MAID_VPACs.h"
#include "insane/asymmetries/fwd_VCSAs.h"

R__LOAD_LIBRARY(libinsanePhysics.so);
R__LOAD_LIBRARY(libinsaneCore.so);

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

//#include "WEvolve/A2Fit_T3DFs.h"
//#include "WEvolve/A2Fit_F1F209_T3DFs.h"
//R__LOAD_LIBRARY(libWEvolve.so);

int A1n(double Qsq = 5.0 ) {

  using namespace insane::physics;

  TLatex tl;
  tl.SetNDC(true);
  TLegend * leg = new TLegend(0.7,0.7,0.84,0.84);


  auto A1A2_stat2015 = new Stat2015_VCSAs();
  TF1 * f_stat_ssf = new TF1("A1n_stat",[&](double* xs, double* ps){ return A1A2_stat2015->A1n(xs[0],ps[0]);}, 0, 1, 1);
  f_stat_ssf->SetParameter(0,Qsq);
  f_stat_ssf->SetLineColor(kBlack);
  leg->AddEntry(f_stat_ssf,"stat(2015)","l");

  auto A1A2_jam = new CTEQ10_JAM_VCSAs();
  TF1 * f_jam_ssf = new TF1("A1n_stat",[&](double* xs, double* ps){ return A1A2_jam->A1n(xs[0],ps[0]);}, 0.01, 0.9, 1);
  f_jam_ssf->SetParameter(0,Qsq);
  f_jam_ssf->SetLineColor(kRed);
  leg->AddEntry(f_jam_ssf,"CTEQ10/JAM","l");

  ////TF1 * f_stat_ssf3 = new TF1("xg1_stat",[&](double* xs, double* ps){ return xs[0]*stat_ssf->g1He3(xs[0],ps[0]);}, 0, 1, 1);
  ////f_stat_ssf3->SetParameter(0,Qsq);
  ////f_stat_ssf3->SetLineColor(kBlack);

  //TF1 * f_ssf_ratio = new TF1("xg1_LSS/xg1_BB",[&](double* xs, double* ps){ return lss_ssf->A1n(xs[0],ps[0])/(lss_ssf->A1n(xs[0]+0.01,ps[0])+0.0001);}, 0.1, 0.9, 1);
  //f_ssf_ratio->SetParameter(0,Qsq);
  //f_ssf_ratio->SetLineColor(8);
  ////leg->AddEntry(f_ssf_ratio,"ratio LSS2010/BB","l");


  // ----------------------------------
  TCanvas*     c  = new TCanvas("A1n", "A1n",1000,1000);
  TMultiGraph* mg = new TMultiGraph(); 
  c->Divide(1,2);
  c->cd(1);

  leg->SetFillColor(kWhite);

  //auto allsf = GetAllSSFs();

  Manager* manager = Manager::GetManager();

  BinnedVariable Q2_bin("Q2","Q^2",{{1.0, 10}});
  BinnedVariable W_bin("W","W",{{2.0, 10.0}});
  /// Get the g1p measurments from all experiments.
  auto A1n_measurements = manager->GetAllMeasurements("A1n");
  auto g1He3_measurements = manager->GetAllMeasurements("A1He3");
  auto A1n_exps = GetExperiments(A1n_measurements);

  std::vector<std::string> d_exps;
  std::vector<std::string> other_exps;

  std::cout << std::size(A1n_exps) << "\n";
  std::copy_if(std::begin(A1n_exps), std::end(A1n_exps), std::back_inserter(d_exps),
               [&](std::string& e) { return !HasMeasurement(e.c_str(), "g1d"); });
  std::copy_if(std::begin(A1n_exps), std::end(A1n_exps), std::back_inserter(other_exps),
               [&](std::string& e) { return HasMeasurement(e.c_str(), "g1d"); });
  //A1n_exps.erase(non_d_exps, std::end(A1n_exps));
  std::cout << std::size(other_exps) << "\n";
  std::cout << std::size(d_exps) << "\n";

  auto A1n_from_d_meas = GetMeasurements("A1n",d_exps);
  ApplyFilterOnList(&Q2_bin, A1n_from_d_meas);
  //ApplyFilterOnList(&W_bin, A1n_from_d_meas);

  auto A1n_from_other_meas = GetMeasurements("A1n",other_exps);
  ApplyFilterOnList(&Q2_bin, A1n_from_other_meas);
  //ApplyFilterOnList(&W_bin, A1n_from_other_meas);

  ApplyFilterOnList(&Q2_bin, g1He3_measurements);
  //ApplyFilterOnList(&W_bin, g1He3_measurements);

  //TLegend* leg = new TLegend(0.7, 0.7, 0.9, 0.9);
  TF2* xfunc = new TF2("xg1","x*y", 0, 1);

  TGraphErrors* aGraph = 0;
  Measurement*  aMeas  = 0;
  // TMultiGraph *      mg      = 0;
  // c->cd(1);


  TLegend * leg2 = new TLegend(0.50,0.15,0.65,0.35);
  TLegend * leg3 = new TLegend(0.65,0.15,0.8,0.35);
  TLegend * leg4 = new TLegend(0.70,0.15,0.8,0.3);
  leg2->SetHeader("D target");
  leg3->SetHeader("^{3}He target");
  leg4->SetHeader("A_{1}^{3}He");
  auto colors = GoodColors();
  TString variable = "x";
  // mg = new TMultiGraph;
  mg->SetTitle("x g_{1} ; x; ");
  for (int i = 0;auto aMeas:  A1n_from_d_meas) {
    if (aMeas->GetNDataPoints() < 1)
      continue;
    aGraph = aMeas->BuildGraph(variable.Data());
    aGraph->SetMarkerColor(colors[i]);
    aGraph->SetLineColor(colors[i]);
    //aGraph->Apply(xfunc);
    leg3->AddEntry(aGraph, aMeas->GetExperimentName(), "ep");
    mg->Add(aGraph, "ep");
    i++;
   }

  for (int i = 0;auto aMeas:  A1n_from_other_meas) {
    if (aMeas->GetNDataPoints() < 1)
      continue;
    aGraph = aMeas->BuildGraph(variable.Data());
    aGraph->SetMarkerColor(colors[i]);
    aGraph->SetLineColor(colors[i]);
    aGraph->SetMarkerStyle(24+i);
    //aGraph->Apply(xfunc);
    leg2->AddEntry(aGraph, aMeas->GetExperimentName(), "ep");
    mg->Add(aGraph, "ep");
    i++;
   }

  TMultiGraph * mg3  = new TMultiGraph();
  for (int i = 0;auto aMeas:  g1He3_measurements) {
    if (aMeas->GetNDataPoints() < 1)
      continue;
    aGraph = aMeas->BuildGraph(variable.Data());
    aGraph->SetMarkerColor(colors[i]);
    aGraph->SetLineColor(colors[i]);
    aGraph->SetMarkerStyle(22);
    //aGraph->Apply(xfunc);
    leg4->AddEntry(aGraph, aMeas->GetExperimentName(), "ep");
    mg3->Add(aGraph, "ep");
    i++;
   }



   //mg->Add(new TGraph(f_bb_ssf), "l");
   mg->Add(new TGraph(f_jam_ssf), "l");
   mg->Add(new TGraph(f_stat_ssf), "l");

   mg->Draw("a");
   mg->GetXaxis()->SetTitle("x");
   mg->GetXaxis()->CenterTitle();
   mg->GetYaxis()->SetTitle("A_{1}^{n}");
   mg->GetYaxis()->CenterTitle();
   mg->GetYaxis()->SetRangeUser(-1, 1);
   mg->Draw("a");

   ////xF2pCTEQ->Draw("same");
   ////xF2pA->Draw("same");
   ////xF2pB->Draw("same");

   leg->Draw();
   leg2->Draw();
   leg3->Draw();

   c->cd(2);

   //mg3->Add(new TGraph(f_stat_ssf3), "l");

   mg3->Draw("a");
   mg3->GetXaxis()->SetTitle("x");
   mg3->GetXaxis()->CenterTitle();
   mg3->GetYaxis()->SetTitle("A_{1}^{^{3}He}");
   mg3->GetYaxis()->CenterTitle();
   mg3->Draw("a");
   //mg3->GetYaxis()->SetRangeUser(-0.04, 0.02);
   leg4->Draw();


   //auto gr_ratio = new TGraph(f_ssf_ratio);
   //gr_ratio->Draw("al");
   //gr_ratio->GetYaxis()->SetRangeUser(-1, 2);

   c->SaveAs("results/A1n.png");
   c->SaveAs("results/A1n.pdf");

   return (0);
}
