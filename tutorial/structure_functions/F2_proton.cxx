#include "insane/structurefunctions/fwd_SFs.h"
#include "insane/structurefunctions/SSFsFromVPACs.h"
#include "insane/xsections/MAID_VPACs.h"

#include "TH1F.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TF1.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "insane/structurefunctions/F1F209_SFs.h"

R__LOAD_LIBRARY(libinsaneCore.so);
R__LOAD_LIBRARY(libinsanePhysics.so);

int F2_proton(double Qsq = 4.0 ) {

  using namespace insane::physics;

  TLatex tl;
  tl.SetNDC(true);
  TLegend * leg = new TLegend(0.7,0.7,0.84,0.84);

  // F1F209 
  F1F209_SFs* f1f2SFs = new F1F209_SFs();
  TF1 * xF2pA = new TF1("xF2pA",[&](double* xs, double* ps){ return f1f2SFs->xF2p(xs[0],ps[0]);}, 0, 1, 1);
  xF2pA->SetParameter(0,Qsq);
  xF2pA->SetLineColor(kBlue);
  leg->AddEntry(xF2pA,"F1F209","l");

  TCanvas * c = new TCanvas("F2p","F2p");
  TMultiGraph* mg = new TMultiGraph(); 

  leg->SetFillColor(kWhite);



   mg->Add(new TGraph( xF2pA), "l");
   //mg->Add(new TGraph( xF2pB), "l");
   //mg->Add(new TGraph( xF2pC), "l");

   mg->Draw("a");
   mg->GetXaxis()->SetTitle("x");
   mg->GetXaxis()->CenterTitle();
   mg->GetYaxis()->SetTitle("xF_{2}^{p}");
   mg->GetYaxis()->CenterTitle();
   mg->Draw("a");

   ////xF2pCTEQ->Draw("same");
   ////xF2pA->Draw("same");
   ////xF2pB->Draw("same");

   leg->Draw();


   //// c->SaveAs(fn1);
   //// c->SaveAs(fn2);

   return(0);
}
