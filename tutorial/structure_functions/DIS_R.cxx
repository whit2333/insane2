#include "TGraph.h"
#include "TMultiGraph.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TLegend.h"


#include "insane/structurefunctions/fwd_SFs.h"
#include "insane/structurefunctions/SSFsFromVPACs.h"
#include "insane/xsections/MAID_VPACs.h"

R__LOAD_LIBRARY(libinsaneCore.so);
R__LOAD_LIBRARY(libinsanePhysics.so);


//#include "WEvolve/A2Fit_T3DFs.h"
//#include "WEvolve/A2Fit_F1F209_T3DFs.h"
//R__LOAD_LIBRARY(libWEvolve.so);

int DIS_R(double Qsq = 4.0 ) {

  using namespace insane::physics;

  TLatex tl;
  tl.SetNDC(true);
  TLegend * leg = new TLegend(0.7,0.7,0.84,0.84);

  // F1F209 
  F1F209_SFs* f1f2SFs = new F1F209_SFs();
  TF1 * xF2pA = new TF1("xF2pA",[&](double* xs, double* ps){ return f1f2SFs->R(xs[0],ps[0],Nuclei::p);}, 0, 1, 1);
  xF2pA->SetParameter(0,Qsq);
  xF2pA->SetLineColor(kBlue);
  leg->AddEntry(xF2pA,"F1F209","l");

  // NMC95
  NMC95_SFs* NMCSFs = new NMC95_SFs();
  TF1 * xF2pB = new TF1("xF2pB",[&](double* xs, double* ps){ return NMCSFs->R(xs[0],ps[0],Nuclei::p);}, 0, 1, 1);
  xF2pB->SetParameter(0,Qsq);
  xF2pB->SetLineColor(kGreen);
  leg->AddEntry(xF2pB,"NMC95","l");
  
  // CTEQ  
  auto cteqSFs = new SFsFromPDFs<CTEQ10_UPDFs>();
  //cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());
  TF1 * xF2pC = new TF1("xF2pC",[&](double* xs, double* ps){ return cteqSFs->R(xs[0],ps[0]);}, 0, 1, 1);
  xF2pC->SetParameter(0,Qsq);
  leg->AddEntry(xF2pC,"CTEQ6","l");
  xF2pC->SetLineColor(2);


  // ----------------------------------
  TCanvas * c = new TCanvas("F2p","R");
  TMultiGraph* mg = new TMultiGraph(); 

  leg->SetFillColor(kWhite);



   mg->Add(new TGraph( xF2pA), "l");
   mg->Add(new TGraph( xF2pB), "l");
   mg->Add(new TGraph( xF2pC), "l");

   mg->Draw("a");
   mg->GetXaxis()->SetTitle("x");
   mg->GetXaxis()->CenterTitle();
   mg->GetYaxis()->SetTitle("R");
   mg->GetYaxis()->CenterTitle();
   mg->Draw("a");

   ////xF2pCTEQ->Draw("same");
   ////xF2pA->Draw("same");
   ////xF2pB->Draw("same");

   leg->Draw();


   c->SaveAs("results/DIS_R.png");
   c->SaveAs("results/DIS_R.pdf");

   return(0);
}
