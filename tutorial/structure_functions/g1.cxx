#include "TGraph.h"
#include "TMultiGraph.h"
#include "TF1.h"
#include "TF2.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TLatex.h"

#include "NucDB/Manager.h"
R__LOAD_LIBRARY(libNucDB.so);

using namespace nucdb;


#include "insane/structurefunctions/AllSSFs.h"
#include "insane/structurefunctions/SSFsFromVPACs.h"
#include "insane/xsections/MAID_VPACs.h"

R__LOAD_LIBRARY(libinsaneCore.so);
R__LOAD_LIBRARY(libinsanePhysics.so);

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

//#include "WEvolve/A2Fit_T3DFs.h"
//#include "WEvolve/A2Fit_F1F209_T3DFs.h"
//R__LOAD_LIBRARY(libWEvolve.so);

int g1(double Qsq = 5.0 ) {

  using namespace insane::physics;

  TLatex tl;
  tl.SetNDC(true);
  TLegend * leg = new TLegend(0.7,0.7,0.84,0.84);
  TLegend * leg5 = new TLegend(0.7,0.7,0.84,0.84);

  // BB 
  auto jam_ssf = new JAM_SSFs_t2();
  auto bb_ssf = new BB_SSFs();

  leg->SetHeader("Q^{2} = 5 GeV/c^{2}");
  TF1* f_jam_ssf = new TF1(
      "xg1_JAM", [&](double* xs, double* ps) { return xs[0] * jam_ssf->g1n(xs[0], ps[0]); }, 0, 1,
      1);
  f_jam_ssf->SetParameter(0, Qsq);
  f_jam_ssf->SetLineColor(kBlack);
  leg->AddEntry(f_jam_ssf, "JAM", "l");

  TF1* f_bb_ssf = new TF1(
      "xg1_BB", [&](double* xs, double* ps) { return xs[0] * bb_ssf->g1n(xs[0], ps[0]); }, 0, 1, 1);
  f_bb_ssf->SetParameter(0, Qsq);
  f_bb_ssf->SetLineColor(8);
  leg->AddEntry(f_bb_ssf, "BB", "l");

  // g1 3He 
  TF1* f_xg1_3He_jam_ssf = new TF1(
      "xg1_JAM", [&](double* xs, double* ps) { 
        return xs[0] * (0.879 * jam_ssf->g1n(xs[0], ps[0]) - 0.021 * jam_ssf->g1p(xs[0], ps[0]));
      }, 0, 1,
      1);
  f_xg1_3He_jam_ssf->SetParameter(0, Qsq);
  f_xg1_3He_jam_ssf->SetLineColor(kBlack);

  TF1* f_xg1_3He_bb = new TF1(
      "xg1_3He_BB",
      [&](double* xs, double* ps) {
        return xs[0] * (0.879 * bb_ssf->g1n(xs[0], ps[0]) - 0.021 * bb_ssf->g1p(xs[0], ps[0]));
      },
      0, 1, 1);
  f_xg1_3He_bb->SetParameter(0, Qsq);
  f_xg1_3He_bb->SetLineColor(8);

  //  LSS2010
  auto lss_ssf   = new LSS2010_SSFs();
  TF1* f_lss_ssf = new TF1(
      "xg1_LSS", [&](double* xs, double* ps) { return xs[0] * lss_ssf->g1n(xs[0], ps[0]); }, 0, 1,
      1);
  f_lss_ssf->SetParameter(0, Qsq);
  f_lss_ssf->SetLineColor(kBlue);
  leg->AddEntry(f_lss_ssf, "LSS2010", "l");

  auto stat_ssf   = new Stat2015_SSFs();
  TF1* f_stat_ssf = new TF1(
      "xg1_stat", [&](double* xs, double* ps) { return xs[0] * stat_ssf->g1n(xs[0], ps[0]); }, 0, 1,
      1);
  f_stat_ssf->SetParameter(0, Qsq);
  f_stat_ssf->SetLineColor(kRed);
  leg->AddEntry(f_stat_ssf, "stat(2015)", "l");

  // TF1 * f_stat_ssf3 = new TF1("xg1_stat",[&](double* xs, double* ps){ return
  // xs[0]*stat_ssf->g1He3(xs[0],ps[0]);}, 0, 1, 1); f_stat_ssf3->SetParameter(0,Qsq);
  // f_stat_ssf3->SetLineColor(kBlack);

  TF1* f_ssf_ratio = new TF1(
      "g1_LSS/g1_JAM",
      [&](double* xs, double* ps) {
        return lss_ssf->g1n(xs[0], ps[0]) / (jam_ssf->g1n(xs[0], ps[0]) + 0.0001);
      },
      0.1, 0.9, 1);
  f_ssf_ratio->SetParameter(0, Qsq);
  f_ssf_ratio->SetLineColor(kBlack);
  f_ssf_ratio->SetLineWidth(3);
  leg5->AddEntry(f_ssf_ratio, "ratio LSS2010/JAM", "l");

  TF1* f_ssf_ratio2 = new TF1(
      "g1_LSS/g1_JAM",
      [&](double* xs, double* ps) {
        return jam_ssf->g1n(xs[0] - 0.05, ps[0]) / (jam_ssf->g1n(xs[0], ps[0]) + 0.0001);
      },
      0.1, 0.9, 1);
  f_ssf_ratio2->SetParameter(0, Qsq);
  f_ssf_ratio2->SetLineColor(kBlue);
  f_ssf_ratio2->SetLineWidth(3);
  leg5->AddEntry(f_ssf_ratio2, "ratio JAM(x-0.05)/JAM(x)", "l");

  TF1* f_ssf_ratio3 = new TF1(
      "g1_LSS/g1_JAM",
      [&](double* xs, double* ps) {
        return stat_ssf->g1n(xs[0] - 0.05, ps[0]) / (stat_ssf->g1n(xs[0], ps[0]) + 0.0001);
      },
      0.1, 0.9, 1);
  f_ssf_ratio3->SetParameter(0, Qsq);
  f_ssf_ratio3->SetLineColor(kRed);
  f_ssf_ratio3->SetLineWidth(3);
  leg5->AddEntry(f_ssf_ratio3, "ratio Stat(x-0.05)/Stat(x)", "l");

  TF1* f_ssf_squared = new TF1(
      "g1_stat^2",
      [&](double* xs, double* ps) {
        return stat_ssf->g1n(xs[0], ps[0]) * stat_ssf->g1n(xs[0], ps[0]);
      },
      0.1, 0.9, 1);
  f_ssf_squared->SetParameter(0, Qsq);
  f_ssf_squared->SetLineColor(kRed);
  f_ssf_squared->SetLineWidth(4);
  leg5->AddEntry(f_ssf_squared, "Stat (g_1)^{2}", "l");

  TF1* f_ssf_squared2 = new TF1(
      "g1_lss^2",
      [&](double* xs, double* ps) {
        return lss_ssf->g1n(xs[0], ps[0]) * lss_ssf->g1n(xs[0], ps[0]);
      },
      0.1, 0.9, 1);
  f_ssf_squared2->SetParameter(0, Qsq);
  f_ssf_squared2->SetLineColor(kBlue);
  f_ssf_squared2->SetLineWidth(4);
  leg5->AddEntry(f_ssf_squared2, "LSS2010 (g_1)^{2}", "l");

  TF1* f_ssf_squared3 = new TF1(
      "g1_JAM^2",
      [&](double* xs, double* ps) {
        return jam_ssf->g1n(xs[0], ps[0]) * jam_ssf->g1n(xs[0], ps[0]);
      },
      0.1, 0.9, 1);
  f_ssf_squared3->SetParameter(0, Qsq);
  f_ssf_squared3->SetLineColor(kBlack);
  f_ssf_squared3->SetLineWidth(4);
  leg5->AddEntry(f_ssf_squared3, "JAM (g_1)^{2}", "l");

  TF1* f_ssf_squared4 = new TF1(
      "g1_BB^2",
      [&](double* xs, double* ps) { return bb_ssf->g1n(xs[0], ps[0]) * bb_ssf->g1n(xs[0], ps[0]); },
      0.1, 0.9, 1);
  f_ssf_squared4->SetParameter(0, Qsq);
  f_ssf_squared4->SetLineColor(8);
  f_ssf_squared4->SetLineWidth(4);
  leg5->AddEntry(f_ssf_squared4, "JAM (g_1)^{2}", "l");

  TF2* f_ssf_R_As = new TF2(
      "g1He3_BB",
      [&](double* xs, double* ps) {
        // double num = 0.879 * jam_ssf->g1n(xs[0], ps[0]) - 0.021 * jam_ssf->g1p(xs[0], ps[0]);
        double den = 0.879 * bb_ssf->g1n(xs[0], ps[0]) - 0.021 * bb_ssf->g1p(xs[0], ps[0]);
        return xs[1] / den;
      },
      0.1, 0.9, -0.1, 1, 2);
  f_ssf_R_As->SetParameter(0,Qsq);
  f_ssf_R_As->SetLineColor(kBlue);
  f_ssf_R_As->SetLineWidth(3);
  //leg5->AddEntry(f_ssf_R_As,"R_{As}^{3He}","l");


  // ----------------------------------
  TCanvas*     c  = new TCanvas("g1n", "g1n",900,1200);
  TMultiGraph* mg = new TMultiGraph(); 
  TMultiGraph* mg_squared = new TMultiGraph(); 
  auto mg4 = new TMultiGraph();
  c->Divide(1,2);
  c->cd(1);

  leg->SetFillColor(kWhite);

  //auto allsf = GetAllSSFs();

  Manager* manager = Manager::GetManager();

  BinnedVariable Q2_bin("Q2","Q^2",{{1.0, 20}});
  BinnedVariable W_bin("W","W",{{2.4, 10.0}});
  /// Get the g1p measurments from all experiments.
  auto g1n_measurements = manager->GetAllMeasurements("g1n");
  auto g1He3_measurements = manager->GetAllMeasurements("g1He3");
  auto g1n_exps = GetExperiments(g1n_measurements);

  std::vector<std::string> d_exps;
  std::vector<std::string> other_exps;

  std::cout << std::size(g1n_exps) << "\n";
  std::copy_if(std::begin(g1n_exps), std::end(g1n_exps), std::back_inserter(d_exps),
               [&](std::string& e) { return !HasMeasurement(e.c_str(), "g1d"); });
  std::copy_if(std::begin(g1n_exps), std::end(g1n_exps), std::back_inserter(other_exps),
               [&](std::string& e) { return HasMeasurement(e.c_str(), "g1d"); });
  //g1n_exps.erase(non_d_exps, std::end(g1n_exps));
  std::cout << std::size(other_exps) << "\n";
  std::cout << std::size(d_exps) << "\n";

  auto g1n_from_d_meas0 = GetMeasurements("g1n",d_exps);
  auto g1n_from_d_meas = FilterMeasurements(g1n_from_d_meas0,&Q2_bin);
  ApplyFilterOnList(&W_bin, g1n_from_d_meas);

  auto g1n_from_other_meas0 = GetMeasurements("g1n",other_exps);
  auto g1n_from_other_meas = FilterMeasurements(g1n_from_other_meas0,&Q2_bin);
  ApplyFilterOnList(&W_bin, g1n_from_other_meas);

  ApplyFilterOnList(&Q2_bin, g1He3_measurements);
  ApplyFilterOnList(&W_bin, g1He3_measurements);

  //TLegend* leg = new TLegend(0.7, 0.7, 0.9, 0.9);
  TF2* xfunc = new TF2("xg1","x*y", 0, 1);
  TF1* oneFunc = new TF1("one","1", 0, 1);
  oneFunc->SetLineStyle(kDashed);
  oneFunc->SetLineColor(1);
  TF1* zeroFunc = new TF1("zero","0", 0, 1);
  zeroFunc->SetLineStyle(kDashed);
  zeroFunc->SetLineColor(1);
  TF2* ySquaredFunc = new TF2("g1^2","y*y", 0, 1);

  TGraphErrors* aGraph = 0;
  Measurement*  aMeas  = 0;
  // TMultiGraph *      mg      = 0;
  // c->cd(1);


  TLegend * leg2 = new TLegend(0.50,0.15,0.65,0.35);
  TLegend * leg3 = new TLegend(0.65,0.15,0.8,0.35);
  TLegend * leg4 = new TLegend(0.70,0.15,0.8,0.3);
  leg2->SetHeader("D target");
  leg3->SetHeader("^{3}He target");
  leg4->SetHeader("g_{1} ^{3}He");
  auto colors = GoodColors();
  TString variable = "x";
  // mg = new TMultiGraph;
  mg->SetTitle("x g_{1} ; x; ");
  for (int i = 0;auto aMeas:  g1n_from_d_meas) {
    if (aMeas->GetNDataPoints() < 1)
      continue;
    aGraph = aMeas->BuildGraph(variable.Data());
    aGraph->SetMarkerColor(colors[i]);
    aGraph->SetLineColor(colors[i]);
    auto bGraph = new TGraphErrors(*aGraph);
    bGraph->Apply(ySquaredFunc);
    mg_squared->Add(bGraph, "ep");
    aGraph->Apply(xfunc);
    leg3->AddEntry(aGraph, aMeas->GetExperimentName(), "ep");
    mg->Add(aGraph, "ep");
    i++;
  }

  for (int i = 0; auto aMeas : g1n_from_other_meas) {
    if (aMeas->GetNDataPoints() < 1)
      continue;
    aGraph = aMeas->BuildGraph(variable.Data());
    aGraph->SetMarkerColor(colors[i]);
    aGraph->SetLineColor(colors[i]);
    aGraph->SetMarkerStyle(24 + i);
    auto bGraph = new TGraphErrors(*aGraph);
    bGraph->Apply(ySquaredFunc);
    mg_squared->Add(bGraph, "ep");
    aGraph->Apply(xfunc);
    leg2->AddEntry(aGraph, aMeas->GetExperimentName(), "ep");
    mg->Add(aGraph, "ep");
    i++;
  }

  TMultiGraph* mg3 = new TMultiGraph();
  for (int i = 0; auto aMeas : g1He3_measurements) {
    if (aMeas->GetNDataPoints() < 1)
      continue;
    aGraph = aMeas->BuildGraph(variable.Data());
    aGraph->SetMarkerColor(colors[i]);
    aGraph->SetLineColor(colors[i]);
    aGraph->SetMarkerStyle(22);
     auto bGraph = new TGraphErrors(*aGraph);
     bGraph->Apply(f_ssf_R_As);
     mg4->Add(bGraph, "ep");
    aGraph->Apply(xfunc);
    leg4->AddEntry(aGraph, aMeas->GetExperimentName(), "ep");
    mg3->Add(aGraph, "ep");
    i++;
  }

  mg->Add(new TGraph(zeroFunc), "l");
  mg->Add(new TGraph(f_bb_ssf), "l");
  mg->Add(new TGraph(f_jam_ssf), "l");
  mg->Add(new TGraph(f_lss_ssf), "l");
  mg->Add(new TGraph(f_stat_ssf), "l");

  mg->Draw("a");
  mg->GetXaxis()->SetTitle("x");
  mg->GetXaxis()->CenterTitle();
  mg->GetYaxis()->SetTitle("xg_{1}^{n}");
  mg->GetYaxis()->CenterTitle();
  mg->Draw("a");
  mg->GetXaxis()->SetLimits(0, 1.0);
  mg->GetYaxis()->SetRangeUser(-0.04, 0.02);

  ////xF2pCTEQ->Draw("same");
  ////xF2pA->Draw("same");
  ////xF2pB->Draw("same");

  leg->Draw();
  leg2->Draw();
  leg3->Draw();

  c->cd(2);

  // mg3->Add(new TGraph(f_stat_ssf3), "l");

  mg3->Add(new TGraph(zeroFunc), "l");
  mg3->Draw("a");
  mg3->GetXaxis()->SetTitle("x");
  mg3->GetXaxis()->CenterTitle();
  mg3->GetYaxis()->SetTitle("xg_{1}^{^{3}He}");
  mg3->GetYaxis()->CenterTitle();
  mg3->Draw("a");
  mg3->GetXaxis()->SetLimits(0, 1.0);
  mg3->GetYaxis()->SetRangeUser(-0.04, 0.02);
  leg4->Draw();

  c->Update();
  // auto gr_ratio = new TGraph(f_ssf_ratio);
  // gr_ratio->Draw("al");
  // gr_ratio->GetYaxis()->SetRangeUser(-1, 2);

  c->SaveAs("results/g1n.png");
  c->SaveAs("results/g1n.pdf");

  TCanvas*     c2  = new TCanvas("ratio", "g1n",900,1200);
  c2->Divide(1,2);
  c2->cd(1);

  //mg4->Add(new TGraph(f_ssf_ratio), "l");
  //mg4->Add(new TGraph(f_ssf_ratio2), "l");
  //mg4->Add(new TGraph(f_ssf_ratio3), "l");
  //mg4->Add(new TGraph(f_ssf_R_As), "l");

  mg4->Add(new TGraph(oneFunc),"l");
  mg4->Draw("a");
  mg4->GetXaxis()->SetTitle("x");
  mg4->GetXaxis()->CenterTitle();
  mg4->GetYaxis()->SetTitle("g_{1}^{^{3}He}/(0.879 g_{1}^{n} - 0.021 g_{1}^{p})");
  mg4->GetYaxis()->CenterTitle();
  mg4->GetXaxis()->SetLimits(0, 1.0);
  mg4->GetYaxis()->SetRangeUser(0, 2);
  mg4->Draw("a");
  leg4->Draw();

  c2->cd(2);
  gPad->SetLogy(true);
  auto mg5 = new TMultiGraph();
  mg5->Add(new TGraph(f_ssf_squared), "l");
  mg5->Add(new TGraph(f_ssf_squared2), "l");
  mg5->Add(new TGraph(f_ssf_squared3), "l");
  mg5->Add(new TGraph(f_ssf_squared4), "l");

  mg5->Add(mg_squared);

  mg5->Draw("a");
  mg5->GetXaxis()->SetTitle("x");
  mg5->GetXaxis()->CenterTitle();
  mg5->GetYaxis()->SetTitle("(g_{1}^{n})^{2}");
  mg5->GetYaxis()->CenterTitle();
  mg5->GetYaxis()->SetRangeUser(1.0e-7, 0.1);
  mg5->Draw("a");

  c2->SaveAs("results/g1n_ratio.pdf");
  c2->SaveAs("results/g1n_ratio.png");

  auto c3 = new TCanvas("xg1_3He");


  TMultiGraph* mg_3He = new TMultiGraph(); 
  mg_3He->Add(new TGraph(f_jam_ssf),"l");
  mg_3He->Add(new TGraph(f_bb_ssf),"l");
  mg_3He->Add(new TGraph(f_xg1_3He_jam_ssf),"l");
  mg_3He->Add(new TGraph(f_xg1_3He_bb),"l");

  mg_3He->Draw("a");
  mg_3He->GetXaxis()->SetTitle("x");
  mg_3He->GetXaxis()->CenterTitle();
  mg_3He->GetYaxis()->SetTitle("g_{1}");
  mg_3He->GetYaxis()->CenterTitle();
  //mg_3He->GetYaxis()->SetRangeUser(1.0e-7, 0.1);
  mg_3He->Draw("a");
  c3->SaveAs("results/g1_3He.pdf");
  c3->SaveAs("results/g1_3He.png");


  return (0);
}
