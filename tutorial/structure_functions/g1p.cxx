#include "TGraph.h"
#include "TMultiGraph.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TLatex.h"

#include "NucDB/Manager.h"
R__LOAD_LIBRARY(libNucDB.so);

#include "insane/structurefunctions/AllSSFs.h"
#include "insane/structurefunctions/SSFsFromVPACs.h"
#include "insane/xsections/MAID_VPACs.h"

R__LOAD_LIBRARY(libinsaneCore.so);
R__LOAD_LIBRARY(libinsanePhysics.so);

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

//#include "WEvolve/A2Fit_T3DFs.h"
//#include "WEvolve/A2Fit_F1F209_T3DFs.h"
//R__LOAD_LIBRARY(libWEvolve.so);

int g1p(double Qsq = 4.0 ) {

  using namespace insane::physics;

  TLatex tl;
  tl.SetNDC(true);
  TLegend * leg = new TLegend(0.7,0.7,0.84,0.84);

  // BB
  auto bb_ssf = new BB_SSFs();
  TF1 * f_bb_ssf = new TF1("xg1_BB",[&](double* xs, double* ps){ return xs[0]*bb_ssf->g1p(xs[0],ps[0]);}, 0, 1, 1);
  f_bb_ssf->SetParameter(0,Qsq);
  f_bb_ssf->SetLineColor(kBlue);
  leg->AddEntry(f_bb_ssf,"BB","l");

  //  LSS2010
  auto lss_ssf = new LSS2010_SSFs();
  TF1 * f_lss_ssf = new TF1("xg1_LSS",[&](double* xs, double* ps){ return xs[0]*lss_ssf->g1p(xs[0],ps[0]);}, 0, 1, 1);
  f_lss_ssf->SetParameter(0,Qsq);
  f_lss_ssf->SetLineColor(kRed);
  leg->AddEntry(f_lss_ssf,"LSS2010","l");

  auto stat_ssf = new Stat2015_SSFs();
  TF1 * f_stat_ssf = new TF1("xg1_stat",[&](double* xs, double* ps){ return xs[0]*stat_ssf->g1p(xs[0],ps[0]);}, 0, 1, 1);
  f_stat_ssf->SetParameter(0,Qsq);
  f_stat_ssf->SetLineColor(kBlack);
  leg->AddEntry(f_stat_ssf,"stat(2015)","l");

  //// NMC95
  //NMC95_SFs* NMCSFs = new NMC95_SFs();
  //TF1 * xF2pB = new TF1("xF2pB",[&](double* xs, double* ps){ return NMCSFs->xF2p(xs[0],ps[0]);}, 0, 1, 1);
  //xF2pB->SetParameter(0,Qsq);
  //xF2pB->SetLineColor(kGreen);
  //leg->AddEntry(xF2pB,"NMC95","l");
  //
  //// CTEQ  
  //auto cteqSFs = new SFsFromPDFs<CTEQ10_UPDFs>();
  ////cteqSFs->SetUnpolarizedPDFs(new CTEQ6UnpolarizedPDFs());
  //TF1 * xF2pC = new TF1("xF2pC",[&](double* xs, double* ps){ return cteqSFs->xF2p(xs[0],ps[0]);}, 0, 1, 1);
  //xF2pC->SetParameter(0,Qsq);
  //leg->AddEntry(xF2pC,"CTEQ6","l");
  //xF2pC->SetLineColor(2);


  TLegend * leg5 = new TLegend(0.7,0.7,0.84,0.84);
  TF1 * f_ssf_ratio = new TF1("xg1_LSS/xg1_BB",[&](double* xs, double* ps){ return lss_ssf->A1n(xs[0],ps[0])/(lss_ssf->A1n(xs[0]+0.01,ps[0])+0.0001);}, 0.1, 0.9, 1);
  f_ssf_ratio->SetParameter(0,Qsq);
  f_ssf_ratio->SetLineColor(8);
  leg5->AddEntry(f_ssf_ratio,"ratio LSS2010/BB","l");


  // ----------------------------------
  TCanvas*     c  = new TCanvas("g1p", "g1p");
  TMultiGraph* mg = new TMultiGraph(); 

  leg->SetFillColor(kWhite);

  auto allsf = GetAllSSFs();

  //int icol = 1;
  //for(auto& ssf: allsf){
  //  std::visit(
  //      [&](auto f) {
  //        using namespace insane::physics;
  //        TF1* func = new TF1(
  //            "xg1", [&](double* xs, double* ps) { return xs[0] * f->g1p(xs[0], ps[0]); }, 0, 1, 1);
  //        func->SetParameter(0, Qsq);
  //        func->SetLineColor(icol++);
  //        leg->AddEntry(func, f->GetLabel(), "l");
  //        mg->Add(new TGraph(func), "l");
  //      },
  //      ssf);
  //}


  mg->Add(new TGraph(f_bb_ssf), "l");
  mg->Add(new TGraph(f_lss_ssf), "l");
  mg->Add(new TGraph(f_stat_ssf), "l");

  mg->Draw("a");
  mg->GetXaxis()->SetTitle("x");
  mg->GetXaxis()->CenterTitle();
  mg->GetYaxis()->SetTitle("xg_{1}^{p}");
  mg->GetYaxis()->CenterTitle();
  mg->Draw("a");

  ////xF2pCTEQ->Draw("same");
  ////xF2pA->Draw("same");
  ////xF2pB->Draw("same");

  leg->Draw();

  //// c->SaveAs(fn1);
  //// c->SaveAs(fn2);

  return (0);
}
