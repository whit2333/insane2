#include "insane/structurefunctions/fwd_SFs.h"
#include "insane/structurefunctions/SSFsFromVPACs.h"
#include "insane/xsections/MAID_VPACs.h"
#include "insane/xsections/InclusiveBornDISXSec.h"
#include "insane/base/Nucleus.h"


int DIS_vs_W(){
  using namespace insane::physics;
  using namespace insane;

  double degree = TMath::Pi()/180.0;

  double beamEnergy = 9; //GeV
  double phi        = 0.0*degree;  
  double Q2         = 1.0; 

  Nucleus targetNucleus = Nucleus::Proton();

  // For plotting cross sections 
  int    npar     = 2;
  double Wmin     = 0.5;//3.5;
  double Wmax     = 4.0;//5.10;

  // ------------------
  // 
  auto xsec0 = new insane::physics::InclusiveBornDISXSec();
  xsec0->SetBeamEnergy(beamEnergy);
  xsec0->SetTargetNucleus(targetNucleus);
  xsec0->InitializePhaseSpaceVariables();
  xsec0->InitializeFinalStateParticles();
  xsec0->UsePhaseSpace(false);
  xsec0->GetPhaseSpace()->Print();

  TF1 * sigma0 = new TF1("sigma0", xsec0, &insane::physics::InclusiveDiffXSec::WDependentXSec, 
                         Wmin, Wmax, npar,"InclusiveDiffXSec","WDependentXSec");
  sigma0->SetParameter(0,Q2);
  sigma0->SetParameter(1,phi);
  sigma0->SetLineColor(1);

  // --------------------

  TCanvas * c = new TCanvas();

  sigma0->Draw();
  //sigma0->SetTitle(title);
  sigma0->GetXaxis()->SetTitle("W (GeV)");
  sigma0->GetXaxis()->CenterTitle(true);
  sigma0->GetYaxis()->SetTitle(xsec0->GetLabel());
  sigma0->GetYaxis()->CenterTitle(true);
  sigma0->DrawCopy();

  //c->SaveAs("results/cross_sections/inclusive/DIS_vs_W.png");

  return 0;
}
