Int_t xsec_test_DVCS(){

  using namespace insane;
  using namespace insane::physics;
  double degree = TMath::Pi()/180.0;

  Double_t beamEnergy = 8.0; //GeV
  Double_t Eprime     = 1.41; 
  Double_t theta      = 25.*degree;
  Double_t phi        = 0.0*degree;  
  Int_t TargetType    = 0; // 0 = proton, 1 = neutron, 2 = deuteron, 3 = He-3  

  // For plotting cross sections 
  Int_t    npar     = 4;
  Double_t Emin     = 0.5;
  Double_t Emax     = 3.0;
  Double_t minTheta = 15.0*degree;
  Double_t maxTheta = 55.0*degree;

  double t = -0.001;

  insane::physics::DVCSDiffXSec * fDiffXSec = new insane::physics::DVCSDiffXSec();
  fDiffXSec->SetBeamEnergy(beamEnergy);
  fDiffXSec->SetTargetNucleus(Nucleus::Proton());
  fDiffXSec->InitializePhaseSpaceVariables();
  fDiffXSec->InitializeFinalStateParticles();
  fDiffXSec->UsePhaseSpace(false);
  fDiffXSec->Print();;

  TString title =  fDiffXSec->GetPlotTitle();
  TString yAxisTitle = fDiffXSec->GetLabel();
  TString xAxisTitle = "E' (GeV)"; 
  //TString xAxisTitle = "#theta"; 

  //TF1 * sigmaE = new TF1("sigma", fDiffXSec, &InSANEExclusiveDiffXSec::EnergyDependentXSec, 
  //      Emin, Emax, npar,"InSANEExclusiveDiffXSec","EnergyDependentXSec");
  // x t Q2 phi phie

  //auto l = [=](double* xs, double *ps) {
  //   std::array<double,5> args = {30.0*degree, 0.2, -0.1, 1.5, 0};
  //   double ep = xs[0];
  //   double Q2 = 4.0*beamEnergy*ep*TMath::Sin(theta/2.0)*TMath::Sin(theta/2.0);
  //   args[3] = Q2;
  //   return fDiffXSec->EvaluateXSec(fDiffXSec->GetDependentVariables(args.data()));
  //};
  //TF1 * sigmaE = new TF1("sigma", l, 0,1.0,0);

  auto lt = [=](double* xs, double *ps) {
    std::array<double,5> args = {30.0*degree, 0.2, xs[0], 2.5, 0};
    return fDiffXSec->EvaluateXSec(fDiffXSec->GetDependentVariables(args.data()));
  };
  TF1 * sigmaE = new TF1("sigmat", lt, -2.0,-0.001,0);
  // ------------------------------------
  //
  TCanvas * c = new TCanvas();
  sigmaE->SetLineColor(kRed);
  sigmaE->Draw();
  sigmaE->SetTitle(title);
  sigmaE->GetXaxis()->SetTitle(xAxisTitle);
  sigmaE->GetXaxis()->CenterTitle(true);
  sigmaE->GetYaxis()->SetTitle(yAxisTitle);
  sigmaE->GetYaxis()->CenterTitle(true);
  sigmaE->Draw();

  auto lphi = [=](double* xs, double *ps) {
    std::array<double,5> args = {0.0*degree, 0.1, -0.2, 1.0, xs[0]*degree};
    return fDiffXSec->EvaluateXSec(fDiffXSec->GetDependentVariables(args.data()));
  };
  TF1 * sigmaPhi = new TF1("sigmaPhi", lphi, -180,180,0);

  // ------------------------------------
  c = new TCanvas();
  sigmaPhi->SetLineColor(kRed);
  sigmaPhi->Draw();
  sigmaPhi->SetTitle(title);
  sigmaPhi->GetXaxis()->SetTitle("#phi [deg]");
  sigmaPhi->GetXaxis()->CenterTitle(true);
  sigmaPhi->GetYaxis()->SetTitle(yAxisTitle);
  sigmaPhi->GetYaxis()->CenterTitle(true);
  sigmaPhi->Draw();
  return 0;
}
