#include "insane/xsections/CoherentDVCSXSec.h"

Int_t xsec_test_coherent_DVCS() {

  using namespace insane;
  using namespace insane::physics;

  Double_t beamEnergy = 11.0; // GeV

  insane::physics::CoherentDVCSXSec* fDiffXSec = new insane::physics::CoherentDVCSXSec();
  fDiffXSec->SetBeamEnergy(beamEnergy);
  fDiffXSec->SetTargetNucleus(Nucleus(2, 4));
  fDiffXSec->InitializePhaseSpaceVariables();
  fDiffXSec->InitializeFinalStateParticles();
  fDiffXSec->UsePhaseSpace(false);
  fDiffXSec->Print();
  ;

  TString title      = fDiffXSec->GetPlotTitle();
  TString yAxisTitle = fDiffXSec->GetLabel();
  TString xAxisTitle = "t [GeV^{2}]";

  double t_0   = -0.5;
  double x_0   = 0.1;
  double Q2_0  = 1.5;
  double phi_0 = -180.0 * degree;
  double phi_e = 0.0 * degree;

  // ------------------------------------
  auto lt = [=](double* xs, double* ps) {
    std::array<double, 5> args = {phi_e, x_0, xs[0], Q2_0, phi_0};
    return fDiffXSec->EvaluateXSec(fDiffXSec->GetDependentVariables(args.data()));
  };
  TF1* sigmat = new TF1("sigmat", lt, -2.0, -0.001, 0);

  // ------------------------------------
  TCanvas* c = new TCanvas();
  sigmat->SetLineColor(kRed);
  sigmat->Draw();
  sigmat->SetTitle(title);
  sigmat->GetXaxis()->SetTitle(xAxisTitle);
  sigmat->GetXaxis()->CenterTitle(true);
  sigmat->GetYaxis()->SetTitle(yAxisTitle);
  sigmat->GetYaxis()->CenterTitle(true);
  sigmat->Draw();
  //// ------------------------------------

  auto lphi = [=](double* xs, double* ps) {
    std::array<double, 5> args = {phi_e, x_0, t_0, Q2_0, xs[0] * degree};
    return fDiffXSec->EvaluateXSec(fDiffXSec->GetDependentVariables(args.data()));
  };
  TF1* sigmaPhi = new TF1("sigmaPhi", lphi, -180, 180, 0);

  // ------------------------------------
  c = new TCanvas();
  sigmaPhi->SetLineColor(kRed);
  sigmaPhi->Draw();
  sigmaPhi->SetTitle(title);
  sigmaPhi->GetXaxis()->SetTitle("#phi [deg]");
  sigmaPhi->GetXaxis()->CenterTitle(true);
  sigmaPhi->GetYaxis()->SetTitle(yAxisTitle);
  sigmaPhi->GetYaxis()->CenterTitle(true);
  sigmaPhi->Draw();

  auto lQ2 = [=](double* xs, double* ps) {
    std::array<double, 5> args = {phi_e, x_0, t_0, xs[0], phi_0};
    return fDiffXSec->EvaluateXSec(fDiffXSec->GetDependentVariables(args.data()));
  };
  TF1* sigmaQ2 = new TF1("sigmaQ2", lQ2, 1, 5, 0);

  // ------------------------------------
  c = new TCanvas();
  sigmaQ2->SetLineColor(kRed);
  sigmaQ2->Draw();
  sigmaQ2->SetTitle(title);
  sigmaQ2->GetXaxis()->SetTitle("Q^{2} [GeV^{2}]");
  sigmaQ2->GetXaxis()->CenterTitle(true);
  sigmaQ2->GetYaxis()->SetTitle(yAxisTitle);
  sigmaQ2->GetYaxis()->CenterTitle(true);
  sigmaQ2->Draw();

  return 0;
}
