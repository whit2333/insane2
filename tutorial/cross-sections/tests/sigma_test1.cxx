//#include "insane/new_xsec/DiffCrossSection.h"
#include "insane/new_xsec/Sigma.h"
#include "insane/kinematics/Core.h"
#include "insane/kinematics/Inelastic.h"

using namespace insane::physics;
using namespace insane::kine::var;
using namespace insane::kine;

void sigma_test1() {

  using Diff1 = Differential<E_prime_v, theta_v, phi_v> ;
  Inelastic_vars dis_vars = construct_Inelastic_variables();

  auto           xs_vars =
      dis_vars.add<insane::physics::tags::sigma_xs>([](const auto& vars) constexpr { return 1.0; });

  auto xs = xsec::make_Sigma<Diff1>(xs_vars);

  auto input = std::make_tuple(E_beam_v{10.6}, E_prime_v{5.3}, theta_v{17.0*M_PI/180.0}, phi_v{0.0});

  {
    auto dis_values = xs_vars.ComputeValues(input);

    // could probably make this better, like: dis_values.get<x_v>()
    std::cout << " E0  = " << std::get<E_beam_v>(dis_values) << "\n";
    std::cout << " E'  = " << std::get<E_prime_v>(dis_values) << "\n";
    std::cout << " th  = " << std::get<theta_v>(dis_values) * 180.0 / M_PI << "\n";
    std::cout << " Q2  = " << std::get<Q2_v>(dis_values) << "\n";
    std::cout << " x   = " << std::get<x_v>(dis_values) << "\n";
    std::cout << " W   = " << std::get<W_v>(dis_values) << "\n";
    std::cout << " y   = " << std::get<y_v>(dis_values) << "\n";
    std::cout << " nu  = " << std::get<nu_v>(dis_values) << "\n";
    std::cout << " XS  = " << std::get<insane::physics::tags::sigma_xs>(dis_values) << "\n";
    std::cout << " XS  = " << xs(input) << "\n";

  }

}
