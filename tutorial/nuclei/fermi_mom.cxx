#include "insane/nuclear/FermiMomentumDist.h"

using namespace insane::physics;

void fermi_mom() {

  FermiMomentumDist f;
  std::cout << f.n(0.1, 2) << "\n";
  std::cout << f.GetNorm( 2) << "\n";

  TGraph * gr = new TGraph();
  for(int i = 0; i < 100; i++){
    double px = i*0.004;
    gr->SetPoint(i,px,f.GetProb(px,2));
  }

  gr->Draw("al");

}
