cmake_minimum_required(VERSION 3.3 FATAL_ERROR)

project(insane_tutorial_xsec)

include(GNUInstallDirs)

find_package (Eigen3 3.3 NO_MODULE)
find_package(ROOT REQUIRED COMPONENTS Geom GenVector MathCore Foam Net)
find_package(fmt REQUIRED)
find_package(spdlog REQUIRED)
find_package(insane REQUIRED )
find_package(RootWebCanvas REQUIRED )
include(${ROOT_USE_FILE})

set(exe_name elastic_ep)
add_executable(${exe_name} src/${exe_name}.cxx)
target_link_libraries(${exe_name}
  )
target_include_directories(${exe_name} PUBLIC
  PRIVATE include )
target_compile_features(${exe_name}
  PUBLIC cxx_auto_type
  PUBLIC cxx_trailing_return_types
  PRIVATE cxx_variadic_templates )
target_link_libraries(${exe_name} 
  PUBLIC insane::insaneCore insane::insanePhysics
  PUBLIC ROOT::GenVector ROOT::Gpad ROOT::Foam
  PUBLIC RootWebCanvas::rwc  
  )
target_compile_options(${exe_name} PUBLIC -O3)

