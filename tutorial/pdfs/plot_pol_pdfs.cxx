Int_t plot_pol_pdfs(Double_t Q2 = 5.0) {

  using namespace insane::physics;
   TList pdfs;

   PolarizedPartonDistributionFunctions * dns =  new DNS2005PolarizedPDFs();
   pdfs.Add(dns);

   // Crashes...
   //PolarizedPartonDistributionFunctions * aac =  new AAC08PolarizedPDFs();
   //pdfs.Add(aac);

   LCWFPolarizedPartonDistributionFunctions * lcwfPDFs = new LCWFPolarizedPartonDistributionFunctions();
   pdfs.Add(lcwfPDFs);

   BBSPolarizedPDFs * bbsPDFs = new BBSPolarizedPDFs();
   pdfs.Add(bbsPDFs);

   //StatisticalPolarizedPDFs * statPDFs = new StatisticalPolarizedPDFs();
   Stat2015PolarizedPDFs * statPDFs = new Stat2015PolarizedPDFs();
   pdfs.Add(statPDFs);

   PolarizedPartonDistributionFunctions * bb =  new BBPolarizedPDFs();
   pdfs.Add(bb);

   PolarizedPartonDistributionFunctions *dssv = new DSSVPolarizedPDFs(); 
   pdfs.Add(dssv); 

   PolarizedPartonDistributionFunctions * gs =  new GSPolarizedPDFs();
   pdfs.Add(gs);

   PolarizedPartonDistributionFunctions *lss2006 = new LSS2006PolarizedPDFs(); 
   pdfs.Add(lss2006); 

   PolarizedPartonDistributionFunctions *lss2010 = new LSS2010PolarizedPDFs(); 
   pdfs.Add(lss2010); 

   TCanvas * fCanvas = new TCanvas("polPDFs","Polarized PDFs",1000,800);
   fCanvas->Divide(3,2);

   TLatex * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(62);
   t->SetTextColor(36);
   t->SetTextSize(0.06);
   t->SetTextAlign(12);

   TLegend * leg = new TLegend(0.7,0.7,0.95,0.95);
   leg->SetHeader(Form("x#Deltaq(x) at Q^{2}=%2.2f",Q2));

//    leg->AddEntry(u1,dns->fLabel.Data(),"l");
//    leg->AddEntry(u2,bb->fLabel.Data(),"l");
//    leg->AddEntry(u3,aac->fLabel.Data(),"l");
//    leg->AddEntry(u4,gs->fLabel.Data(),"l");
//   leg->Draw();

   fCanvas->cd(1);
   THStack * s1 = new THStack();
   for(int i = 0;i<pdfs.GetEntries();i++){
      PolarizedPartonDistributionFunctions * aPDF = (PolarizedPartonDistributionFunctions *)pdfs.At(i);
      TF1 * f = aPDF->GetFunction(PDFBase::kUP);
      f->SetParameter(0,Q2);
      s1->Add(f->GetHistogram());
      leg->AddEntry(f,aPDF->GetLabel(),"l");
   }
   s1->Draw("nostack L");
   leg->Draw();

   t->DrawLatex(0.5,0.95,"#Deltau(x)");

   fCanvas->cd(2);
   THStack * s2 = new THStack();
   for(int i = 0;i<pdfs.GetEntries();i++){
      PolarizedPartonDistributionFunctions * aPDF = (PolarizedPartonDistributionFunctions *)pdfs.At(i);
      TF1 * f = aPDF->GetFunction(PDFBase::kDOWN);
      f->SetParameter(0,Q2);
      s2->Add(f->GetHistogram());
   }
   s2->Draw("nostack L");
   t->DrawLatex(0.5,0.95,"#Deltad(x)");


   fCanvas->cd(3);
   THStack * s3 = new THStack();
   for(int i = 0;i<pdfs.GetEntries();i++){
      PolarizedPartonDistributionFunctions * aPDF = (PolarizedPartonDistributionFunctions *)pdfs.At(i);
      TF1 * f = aPDF->GetFunction(PDFBase::kSTRANGE);
      f->SetParameter(0,Q2);
      s3->Add(f->GetHistogram());

   }
   s3->Draw("nostack L");
   t->DrawLatex(0.5,0.95,"#Deltas(x)");

   fCanvas->cd(4);
   THStack * s4 = new THStack();
   for(int i = 0;i<pdfs.GetEntries();i++){
      PolarizedPartonDistributionFunctions * aPDF = (PolarizedPartonDistributionFunctions *)pdfs.At(i);
      TF1 * f = aPDF->GetFunction(PDFBase::kANTIUP);
      f->SetParameter(0,Q2);
      s4->Add(f->GetHistogram());

   }
   s4->Draw("nostack L");
   t->DrawLatex(0.5,0.95,"#Delta#bar{u}(x)");


   fCanvas->cd(5);
   THStack * s5 = new THStack();
   for(int i = 0;i<pdfs.GetEntries();i++){
      PolarizedPartonDistributionFunctions * aPDF = (PolarizedPartonDistributionFunctions *)pdfs.At(i);
      TF1 * f = aPDF->GetFunction(PDFBase::kANTIDOWN);
      f->SetParameter(0,Q2);
      s5->Add(f->GetHistogram());

   }
   s5->Draw("nostack L");
   t->DrawLatex(0.5,0.95,"#Delta#bar{d}(x)");

   fCanvas->cd(6);
   THStack * s6 = new THStack();
   for(int i = 0;i<pdfs.GetEntries();i++){
      PolarizedPartonDistributionFunctions * aPDF = (PolarizedPartonDistributionFunctions *)pdfs.At(i);
      TF1 * f = aPDF->GetFunction(PDFBase::kGLUON);
      f->SetParameter(0,Q2);
      s6->Add(f->GetHistogram());

   }
   s6->Draw("nostack L");
   t->DrawLatex(0.5,0.95,"#Deltag(x)");

   fCanvas->SaveAs("results/pdfs/plot_pol_pdfs.png");
   fCanvas->SaveAs("results/pdfs/plot_pol_pdfs.pdf");
   return(0);
}
