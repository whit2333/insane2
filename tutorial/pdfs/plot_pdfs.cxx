#include "insane/structurefunctions/fwd_SFs.h"
#include "insane/structurefunctions/SSFsFromVPACs.h"
//#include "insane/pdfs/MAID_VPACs.h"
#include "insane/pdfs/CTEQ10_UPDFs.h"
#include "insane/pdfs/Stat2015_UPDFs.h"

Int_t plot_pdfs(Double_t Q2 = 4.0) {

  using namespace insane::physics;

   TList pdfs;

   Stat2015_UPDFs * statPDFs = new Stat2015_UPDFs();
   pdfs.Add(statPDFs);

   CTEQ10_UPDFs * cteqPDFs = new CTEQ10_UPDFs();
   pdfs.Add(cteqPDFs);

   //LCWFPartonDistributionFunctions * lcwfPDFs = new LCWFPartonDistributionFunctions();
   //pdfs.Add(lcwfPDFs);

   //CTEQ6UnpolarizedPDFs * cteqPDFs = new CTEQ6UnpolarizedPDFs();
   //pdfs.Add(cteqPDFs);

   //BBSUnpolarizedPDFs * bbsPDFs = new BBSUnpolarizedPDFs();
   //pdfs.Add(bbsPDFs);

   ////jStatisticalUnpolarizedPDFs * statPDFs = new StatisticalUnpolarizedPDFs();
   //Stat2015UnpolarizedPDFs * statPDFs = new Stat2015UnpolarizedPDFs();
   //pdfs.Add(statPDFs);

   //CJ12UnpolarizedPDFs *cj12PDFs = new CJ12UnpolarizedPDFs(); 
   //pdfs.Add(cj12PDFs); 

   //BBSQuarkHelicityDistributions * bbsPHDs = new BBSQuarkHelicityDistributions();
   //UnpolarizedPDFsFromPHDs * bbsPDFs2 = new UnpolarizedPDFsFromPHDs(); 
   //bbsPDFs2->SetPHDs(bbsPHDs);
   //pdfs.Add(bbsPDFs2); 

   //LHAPDFUnpolarizedPDFs * lhaPDFs =  new LHAPDFUnpolarizedPDFs();
   //lhaPDFs->SetPDFType("cteq6m",1);
   //pdfs.Add(lhaPDFs);

   //int cols[] = {2,3,4,6,7,8,9};
   //color is set by PartonDistributionFunction datamember fDefaultLineColor

   TCanvas * fCanvas = new TCanvas("PDFs","Unpolarized PDFs",1000,800);
   fCanvas->Divide(3,2);

   TF2 * fx = new TF2("fx","x*y",0,1,0,1);
   TMultiGraph * mg = 0;
   TGraph      * gr = 0;
   TLatex * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(62);
   t->SetTextColor(36);
   t->SetTextSize(0.06);
   t->SetTextAlign(12);

   TLegend * leg = new TLegend(0.7,0.7,0.95,0.95);
   leg->SetHeader(Form("xq(x) at Q^{2}=%2.2f",Q2));

   // Up
   fCanvas->cd(1);
   mg = new TMultiGraph();
   for(int i = 0;i<pdfs.GetEntries();i++){ 
     
     UnpolarizedPDFs * aPDF = (UnpolarizedPDFs *)pdfs.At(i);

     TF1 * f = new TF1("u(x)",[&](Double_t* x, Double_t* p) { return aPDF->Get(PartonFlavor::kUP,x[0],4.0); },0,1,0);
     f->SetParameter(0,Q2);
     gr = new TGraph(f->DrawCopy("goff")->GetHistogram());
     gr->Apply(fx);
     //gr->SetLineColor(i);
     //gr->SetMarkerColor(i);
     mg->Add(gr,"l");
     //s1->Add(f->GetHistogram());
     leg->AddEntry(gr,aPDF->GetLabel(),"l");
   }
   mg->Draw("a");
   leg->Draw();

   t->DrawLatex(0.5,0.95,"u(x)");

   // Down
   fCanvas->cd(2);
   mg = new TMultiGraph();
   for(int i = 0;i<pdfs.GetEntries();i++){
     UnpolarizedPDFs * aPDF = (UnpolarizedPDFs *)pdfs.At(i);

     TF1 * f = new TF1("u(x)",[&](Double_t* x, Double_t* p) { return aPDF->Get(PartonFlavor::kDOWN,x[0],4.0); },0,1,0);
     f->SetParameter(0,Q2);
     gr = new TGraph(f->DrawCopy("goff")->GetHistogram());
     gr->Apply(fx);
     //gr->SetLineColor(i);
     //gr->SetMarkerColor(i);
     mg->Add(gr,"l");
     //s1->Add(f->GetHistogram());
     //leg->AddEntry(gr,aPDF->GetLabel(),"l");
   }
   mg->Draw("a");
   //s1->Draw("nostack L");
   leg->Draw();
   t->DrawLatex(0.5,0.95," d(x)");

   //// Strange
   fCanvas->cd(3);
   mg = new TMultiGraph();
   for(int i = 0;i<pdfs.GetEntries();i++){
     UnpolarizedPDFs * aPDF = (UnpolarizedPDFs *)pdfs.At(i);

     TF1 * f = new TF1("u(x)",[&](Double_t* x, Double_t* p) { return aPDF->Get(PartonFlavor::kSTRANGE,x[0],4.0); },0,1,0);
     f->SetParameter(0,Q2);
     gr = new TGraph(f->DrawCopy("goff")->GetHistogram());
     gr->Apply(fx);
     //gr->SetLineColor(i);
     //gr->SetMarkerColor(i);
     mg->Add(gr,"l");
     //s1->Add(f->GetHistogram());
     //leg->AddEntry(gr,aPDF->GetLabel(),"l");
   }
   mg->Draw("a");
   //s1->Draw("nostack L");
   leg->Draw();
   t->DrawLatex(0.5,0.95," s(x)");

   // U-bar
   fCanvas->cd(4);
   mg = new TMultiGraph();
   for(int i = 0;i<pdfs.GetEntries();i++){ 
     
     UnpolarizedPDFs * aPDF = (UnpolarizedPDFs *)pdfs.At(i);

     TF1 * f = new TF1("u(x)",[&](Double_t* x, Double_t* p) { return aPDF->Get(PartonFlavor::kANTIUP,x[0],4.0); },0,1,0);
     f->SetParameter(0,Q2);
     gr = new TGraph(f->DrawCopy("goff")->GetHistogram());
     gr->Apply(fx);
     //gr->SetLineColor(i);
     //gr->SetMarkerColor(i);
     mg->Add(gr,"l");
     //s1->Add(f->GetHistogram());
     //leg->AddEntry(gr,aPDF->GetLabel(),"l");
   }
   mg->Draw("a");
   leg->Draw();
   t->DrawLatex(0.5,0.95,"#bar{u}(x)");

   // D-bar
   fCanvas->cd(5);
   mg = new TMultiGraph();
   for(int i = 0;i<pdfs.GetEntries();i++){ 
     
     UnpolarizedPDFs * aPDF = (UnpolarizedPDFs *)pdfs.At(i);

     TF1 * f = new TF1("u(x)",[&](Double_t* x, Double_t* p) { return aPDF->Get(PartonFlavor::kANTIDOWN,x[0],4.0); },0,1,0);
     f->SetParameter(0,Q2);
     gr = new TGraph(f->DrawCopy("goff")->GetHistogram());
     gr->Apply(fx);
     //gr->SetLineColor(i);
     //gr->SetMarkerColor(i);
     mg->Add(gr,"l");
     //s1->Add(f->GetHistogram());
     //leg->AddEntry(gr,aPDF->GetLabel(),"l");
   }
   mg->Draw("a");
   leg->Draw();
   t->DrawLatex(0.5,0.95,"#bar{u}(x)");

   // D-bar
   fCanvas->cd(6);
   mg = new TMultiGraph();
   for(int i = 0;i<pdfs.GetEntries();i++){ 
     
     UnpolarizedPDFs * aPDF = (UnpolarizedPDFs *)pdfs.At(i);

     TF1 * f = new TF1("u(x)",[&](Double_t* x, Double_t* p) { return aPDF->Get(PartonFlavor::kANTISTRANGE,x[0],4.0); },0,1,0);
     f->SetParameter(0,Q2);
     gr = new TGraph(f->DrawCopy("goff")->GetHistogram());
     gr->Apply(fx);
     //gr->SetLineColor(i);
     //gr->SetMarkerColor(i);
     mg->Add(gr,"l");
     //s1->Add(f->GetHistogram());
     //leg->AddEntry(gr,aPDF->GetLabel(),"l");
   }
   mg->Draw("a");
   leg->Draw();
   t->DrawLatex(0.5,0.95,"#bar{u}(x)");
   //}
   //s4->Draw("nostack L");
   //t->DrawLatex(0.5,0.95,"#bar{u}(x)");

   //// D-bar
   //fCanvas->cd(5);
   //THStack * s5 = new THStack();
   //for(int i = 0;i<pdfs.GetEntries();i++){
   //   PartonDistributionFunctions * aPDF = (PartonDistributionFunctions *)pdfs.At(i);
   //   TF1 * f = aPDF->GetFunction(PDFBase::kANTIDOWN);
   //   f->SetParameter(0,Q2);
   //   s5->Add(f->GetHistogram());

   //}
   //s5->Draw("nostack L");
   //t->DrawLatex(0.5,0.95,"#bar{d}(x)");

   //// S-bar
   //fCanvas->cd(6);
   //THStack * s6 = new THStack();
   //for(int i = 0;i<pdfs.GetEntries();i++){
   //   PartonDistributionFunctions * aPDF = (PartonDistributionFunctions *)pdfs.At(i);
   //   TF1 * f = aPDF->GetFunction(PDFBase::kGLUON);
   //   f->SetParameter(0,Q2);
   //   s6->Add(f->GetHistogram());
   //}
   //s6->Draw("nostack L");
   //t->DrawLatex(0.5,0.95,"g(x)");

/*
   fCanvas->cd(1);
   TLegend * leg = new TLegend(0.7,0.7,0.95,0.95);
   leg->SetHeader("q(x)");
   leg->AddEntry(u1,lha->fLabel.Data(),"l");
//    leg->AddEntry(u2,bb->fLabel.Data(),"l");
//    leg->AddEntry(u3,aac->fLabel.Data(),"l");
   leg->Draw();*/

   fCanvas->SaveAs("results/plot_pdfs.png");


return(0);
}
