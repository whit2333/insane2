#include "insane/pdfs/Stat2015_UPDFs.h"

#include "TF1.h"
#include "TGraph.h"
#include "TCanvas.h"

Int_t pdf_test1(Double_t Q2 = 4.0) {

  using namespace insane::physics;

   Stat2015_UPDFs * pdfs = new Stat2015_UPDFs();
   std::cout << " F1 = " << pdfs->F1p_Twist2(0.3,4.0) << "\n";

   TF1 * f = new TF1("F1p", [=](Double_t* x, Double_t* p) { return x[0]*pdfs->F2p_Twist2(x[0],4.0); },0.01,0.99,0);
   TGraph* gr = new TGraph(f);

   TCanvas * fCanvas = new TCanvas("PDFs","Unpolarized PDFs",1000,800);
   gr->Draw("alp");

//   fCanvas->Divide(3,2);
//
//   TF2 * fx = new TF2("fx","x*y",0,1,0,1);
//   TMultiGraph * mg = 0;
//   TGraph      * gr = 0;
//   TLatex * t = new TLatex();
//   t->SetNDC();
//   t->SetTextFont(62);
//   t->SetTextColor(36);
//   t->SetTextSize(0.06);
//   t->SetTextAlign(12);
//
//   TLegend * leg = new TLegend(0.7,0.7,0.95,0.95);
//   leg->SetHeader(Form("xq(x) at Q^{2}=%2.2f",Q2));
//
//   // Up
//   fCanvas->cd(1);
//   mg = new TMultiGraph();
//   for(int i = 0;i<pdfs.GetEntries();i++){
//      PartonDistributionFunctions * aPDF = (PartonDistributionFunctions *)pdfs.At(i);
//
//      TF1 * f = new TF1("u(x)",[&](Double_t* x, Double_t* p) { return aPDF->u(x[0]); },0,1);
//      f->SetParameter(0,Q2);
//      gr = new TGraph(f->DrawCopy("goff")->GetHistogram());
//      //gr->Apply(fx);
//      //gr->SetLineColor(i);
//      //gr->SetMarkerColor(i);
//      mg->Add(gr,"l");
//      //s1->Add(f->GetHistogram());
//      leg->AddEntry(gr,aPDF->GetLabel(),"l");
//   }
//   mg->Draw("a");
//   leg->Draw();
//
//   t->DrawLatex(0.5,0.95,"u(x)");
//
//   // Down
//   fCanvas->cd(2);
//   mg = new TMultiGraph();
//   for(int i = 0;i<pdfs.GetEntries();i++){
//      PartonDistributionFunctions * aPDF = (PartonDistributionFunctions *)pdfs.At(i);
//      TF1 * f = aPDF->GetFunction(PDFBase::kDOWN);
//      f->SetParameter(0,Q2);
//      gr = new TGraph(f->DrawCopy("goff")->GetHistogram());
//      //gr->Apply(fx);
//      //gr->SetLineColor(i);
//      //gr->SetMarkerColor(i);
//      mg->Add(gr,"l");
//      //s1->Add(f->GetHistogram());
//      leg->AddEntry(gr,aPDF->GetLabel(),"l");
//   }
//   mg->Draw("a");
//   //s1->Draw("nostack L");
//   leg->Draw();
//   t->DrawLatex(0.5,0.95," d(x)");
//
//   // Strange
//   fCanvas->cd(3);
//   THStack * s3 = new THStack();
//   for(int i = 0;i<pdfs.GetEntries();i++){
//      PartonDistributionFunctions * aPDF = (PartonDistributionFunctions *)pdfs.At(i);
//      TF1 * f = aPDF->GetFunction(PDFBase::kSTRANGE);
//      f->SetParameter(0,Q2);
//      s3->Add(f->GetHistogram());
//   }
//   s3->Draw("nostack L");
//   t->DrawLatex(0.5,0.95,"s(x)");
//
//   // U-bar
//   fCanvas->cd(4);
//   THStack * s4 = new THStack();
//   for(int i = 0;i<pdfs.GetEntries();i++){
//      PartonDistributionFunctions * aPDF = (PartonDistributionFunctions *)pdfs.At(i);
//      TF1 * f = aPDF->GetFunction(PDFBase::kANTIUP);
//      f->SetParameter(0,Q2);
//      s4->Add(f->GetHistogram());
//
//   }
//   s4->Draw("nostack L");
//   t->DrawLatex(0.5,0.95,"#bar{u}(x)");
//
//   // D-bar
//   fCanvas->cd(5);
//   THStack * s5 = new THStack();
//   for(int i = 0;i<pdfs.GetEntries();i++){
//      PartonDistributionFunctions * aPDF = (PartonDistributionFunctions *)pdfs.At(i);
//      TF1 * f = aPDF->GetFunction(PDFBase::kANTIDOWN);
//      f->SetParameter(0,Q2);
//      s5->Add(f->GetHistogram());
//
//   }
//   s5->Draw("nostack L");
//   t->DrawLatex(0.5,0.95,"#bar{d}(x)");
//
//   // S-bar
//   fCanvas->cd(6);
//   THStack * s6 = new THStack();
//   for(int i = 0;i<pdfs.GetEntries();i++){
//      PartonDistributionFunctions * aPDF = (PartonDistributionFunctions *)pdfs.At(i);
//      TF1 * f = aPDF->GetFunction(PDFBase::kGLUON);
//      f->SetParameter(0,Q2);
//      s6->Add(f->GetHistogram());
//   }
//   s6->Draw("nostack L");
//   t->DrawLatex(0.5,0.95,"g(x)");
//
///*
//   fCanvas->cd(1);
//   TLegend * leg = new TLegend(0.7,0.7,0.95,0.95);
//   leg->SetHeader("q(x)");
//   leg->AddEntry(u1,lha->fLabel.Data(),"l");
////    leg->AddEntry(u2,bb->fLabel.Data(),"l");
////    leg->AddEntry(u3,aac->fLabel.Data(),"l");
//   leg->Draw();*/
//
//   fCanvas->SaveAs("results/pdfs/plot_pdfs.png");
//
//
   return(0);
}

