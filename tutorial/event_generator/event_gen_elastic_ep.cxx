R__LOAD_LIBRARY(libinsanePhysics.so)

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

#include "Math/Vector3Dfwd.h"
#include "insane/new_xsec/PhaseSpaceVariables.h"
#include "insane/new_xsec/NFoldDifferential.h"
#include "insane/new_xsec/Jacobians.h"
#include "insane/new_xsec/DiffCrossSection.h"
#include "insane/new_xsec/FinalState.h"
#include <typeinfo>

#include "insane/new_xsec/PSSampler.h"

#include "insane/formfactors/AMTFormFactors.h"
#include "TH1F.h"

void event_gen_elastic_ep(){

  using namespace insane::units;
  using namespace insane::physics;
  using namespace insane::helpers;
  double test_prec = 1.0e-7;

  // Q2 as a function of x and y
  auto Q2_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double yv = vars[1];
    return (s-m2)*xv*yv; };

  // x as a function of x (trivial)
  auto x_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double xv = vars[0];
    return xv; };

  // x as a function of x (trivial)
  auto simple_func = [=](const InitialState& is, const std::array<double,1>& vars){
    double xv = vars[0];
    return xv; };

  // y as a function  of x and Q2
  auto y_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double Q2v = vars[1];
    return Q2v/((s-m2)*xv); };

  //  as a function of x and nu
  auto nu_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double yv = vars[1];
    double Q2 = (s-m2)*xv*yv;
    return Q2/(2.0*TMath::Sqrt(m2)*xv);
  };

  // -------------------------------------------------------
  // Phase space variables

  PSV<Invariant> Q2_psv({0.5, 2.0}, Invariant::Q2, "Q2");
  IPSV phi_psv({0.0, pi},  "phi");

  // Prepare the initial state
  double P1 = 2.0;
  double m1 = 0.000511;
  double E1 = std::sqrt(P1*P1+m1*m1);

  double P2 = 0.0;
  double m2 = 0.938;
  double E2 = std::sqrt(P2*P2+m2*m2);

  InitialState init_state(P1, P2, m2);

  // Final state kinematics
  double Q2_0 = 0.2;

  auto diff0 = make_diff(Q2_psv, phi_psv);
  auto ps0   = make_phase_space( diff0 );

  auto E_prime = [](const InitialState& is, const std::array<double,2>& x){
    double Q2   = x.at(0);
    double phi  = x.at(1);
    double s    = is.s();
    double M    = is.p2().M();
    double E0   = is.p1().E();
    double Ep   = E0 - Q2/(2.0*M);
    //double th   = 2.0*std::asin(std::sqrt(Q2/(4.0*Ep*E0)));
    double th   = pi-2.0*std::asin(std::sqrt(Q2/(4.0*Ep*E0)));
    ROOT::Math::Polar3D<double> pvec(Ep,th,phi);
    return ROOT::Math::XYZTVector(pvec.x(), pvec.y(), pvec.z(), Ep);
  };
  auto P_prime = [](const InitialState& is, const std::array<double,2>& x){
    double Q2   = x.at(0);
    double phi  = x.at(1);
    double s    = is.s();
    double M    = is.p2().M();
    double E0   = is.p1().E();
    double Ep   = E0 - Q2/(2.0*M);
    double th   = pi-2.0*std::asin(std::sqrt(Q2/(4.0*Ep*E0)));
    ROOT::Math::Polar3D<double> pvec(Ep,th,phi);
    auto E_prime = ROOT::Math::XYZTVector(pvec.x(), pvec.y(), pvec.z(), Ep);
    return is.p1()+is.p2()-E_prime;
  };

  auto elastic_DiffXS_func = [](const InitialState& is, const std::array<double,2>& x){
    static AMTFormFactors ffs;
    double M   = is.p2().M();
    double Q2  = x.at(0);
    double tau = Q2/(4.0*M*M);
    double E0  = is.p1().E();
    double Ep  = E0 - Q2/(2.0*M);
    if( Ep > E0 ) return 0.0;
    double theta = 2.0*std::asin(std::sqrt(Q2/(4.0*Ep*E0)));
    double mottXSec      = insane::Kine::Sig_Mott(E0,theta);
    double recoil_factor = Ep/E0;
    double GE2           = std::pow(ffs.GEp(Q2), 2.0);
    double GM2           = std::pow(ffs.GMp(Q2), 2.0);
    double tanthetaOver2 = std::tan(theta/2.0);
    // Rosenbluth formula
    double res = mottXSec*recoil_factor*( (GE2+tau*GM2)/(1.0+tau) 
                                         + 2.0*tau*GM2*tanthetaOver2*tanthetaOver2 );
    res = res * hbarc2_gev_nb;
    return(res);
  };

  auto elastic_xs = make_diff_cross_section( ps0, elastic_DiffXS_func  );

  auto fsp0 = make_final_state_particle(E_prime,11 );
  auto fsp1 = make_final_state_particle(P_prime,2212,0.938 );

  std::cout << fsp0(init_state, {Q2_0,0.1}) << std::endl;
  std::cout << fsp1(init_state, {Q2_0,0.1}) << std::endl;

  auto fs0   = make_final_state(ps0, std::make_tuple(fsp0,fsp1));
  auto parts = fs0.CalculateFinalState(init_state, {Q2_0,0.1});

  //for(auto p : parts) {
  //  std::cout << p << std::endl;
  //  std::cout << p.M() << std::endl;
  //}

  // ----------------------------
  // PS Sampler
  auto integrated_elastic_xs = make_integrated_cross_section(init_state, elastic_xs);
  auto elastic_sampler = make_ps_sampler(integrated_elastic_xs);

  // --------------------------
  // Event Generator
  auto elastic_evgen = make_event_generator(elastic_sampler, fs0);
  auto total_XS      = elastic_evgen.Init();

  // --------------------------
  //
  double Ebeam    = init_state.p1().E();
  auto   p_target = init_state.p2();

  TH1F* h1 = new TH1F("hQ2"," ;Q^{2}", 100,0,2.5);

  // --------------------------
  
  for(int i=0; i<10000;i++) {

    //std::cout << "Event " << i << "\n";
    auto parts = elastic_evgen.GenerateEvent();

    auto   q      = init_state.p1()-parts.at(0);
    double Q2_ev  = -1.0*(q.Dot(q));

    //std::cout << " p1 = " << init_state.p1() << "\n";
    //std::cout << " Q2 = " << Q2_ev << "\n";
    h1->Fill(Q2_ev);

    //for(auto p : parts) {
    //  //std::cout << " x  = " << x_ev << "\n";
    //  std::cout << p << std::endl;
    //}
    //for(auto v : elastic_evgen.fVars) {
    //  std::cout << v << std::endl;
    //}
  }

  h1->Draw();
}
