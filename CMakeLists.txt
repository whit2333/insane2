cmake_minimum_required(VERSION 3.6 FATAL_ERROR)
project(insane 
  LANGUAGES CXX Fortran 
  VERSION 2.1.0 
  DESCRIPTION "insane2")

# --------------------------------------------------------------------------
# Unit tests
#enable_testing()
#add_subdirectory(src/3rd_party/catch)
Include(FetchContent)

FetchContent_Declare(
  Catch2
  GIT_REPOSITORY https://github.com/catchorg/Catch2.git
  GIT_TAG        v3.4.0 # or a later release
)

FetchContent_MakeAvailable(Catch2)

#add_executable(tests test.cpp)
#target_link_libraries(tests PRIVATE Catch2::Catch2WithMain)

set(CMAKE_MODULE_PATH
  ${CMAKE_MODULE_PATH}
  ${PROJECT_SOURCE_DIR}/cmake
  ${PROJECT_SOURCE_DIR}/cmake/modules)

# This assumes you are installing into a director like
# /usr/local/opt/project_version and 
# /usr/local/etc/modulesfiles is in MODULEPATH
set(LMOD_INSTALL_PREFIX ../../etc/modulefiles)
set(INSTALL_LMOD_FILES OFF)

include(cmake/os.cmake)
include(cmake/root.cmake)

# These are the main shared libraries from insane:
#  - "core"       insaneCore
#  - "physics"    insanePhysics
#  - "experiment" insaneExperiment 
set(INSANE_LIB_NAMES
  core
  physics
  #experiment
  )

set(INSANE_DATA_DIR        ${CMAKE_INSTALL_PREFIX}/share/insane  CACHE PATH "insane data directrory" )
set(InSANE_PDF_GRID_DIR    ${INSANE_DATA_DIR}/grid)

#-------------------------------------------------------
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/cmake)

# ---------------------------------------------------------------------------
# undocumented cmake:
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)

#----------------------------------------------------------------------------
# Find Libraries
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
find_package(ROOT REQUIRED 
  COMPONENTS MathCore RIO Hist MathMore Graf Graf3d Gpad GX11 GeomPainter X3d Gviz3d RGL
  Eve EG Foam GenVector Geom GLEW Gui HistPainter MathCore Matrix MLP Net Physics Thread Tree TreeViewer)
include(${ROOT_USE_FILE})
include_directories(${ROOT_INCLUDE_DIRS})

#include_directories("/opt/software/local/etc/cling/lib/clang/5.0.0/include/include"
#"/opt/software/local/etc/cling/lib/clang/5.0.0/include")
#add_definitions(${ROOT_CXX_FLAGS})

#find_package(CLHEP REQUIRED)
#find_package(LHAPDF QUIET)
#find_package(MySQL QUIET)

list(APPEND CMAKE_PREFIX_PATH $ENV{HOME})
#list(APPEND CMAKE_PREFIX_PATH /usr/local/share/eigen3)
#list(APPEND CMAKE_PREFIX_PATH /usr/local/share/eigen3/cmake)
#list(APPEND CMAKE_PREFIX_PATH /usr/local/share/eigen3)
#set(Eigen3_DIR ${CMAKE_SOURCE_DIR}/src/eigen3)
find_package (Eigen3 3.3 REQUIRED NO_MODULE)
#find_package(PkgConfig REQUIRED)
#pkg_search_module(Eigen3 REQUIRED IMPORTED_TARGET Eigen)
#target_include_directories(${PROJECT_NAME} INTERFACE ${EIGEN3_INCLUDE_DIR})
message("Eigen3_INCLUDE_DIRS : ${Eigen3_INCLUDE_DIRS}")
message("EIGEN3_INCLUDE_DIRS : ${EIGEN3_INCLUDE_DIRS}")
message("EIGEN3_INCLUDE_DIR : ${EIGEN3_INCLUDE_DIR}")
include_directories(${EIGEN3_INCLUDE_DIRS})
#add_library(eigen INTERFACE IMPORTED)

#----------------------------------------------------------------------------
# not sure this is the best way to do this....
#set(InSANE_Fortran_FLAGS "${InSANE_Fortran_FLAGS} -finit-local-zero -Wno-conversion -Wno-unused-variable -Wno-tabs -Wno-unused-dummy-argument")
#set(InSANE_Fortran_FLAGS "${InSANE_Fortran_FLAGS} -D\"InSANE_PDF_GRID_DIR='${InSANE_PDF_GRID_DIR}'\" -g -O2 -Wall -ffixed-line-length-none -x f77-cpp-input ")
#set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} ${InSANE_Fortran_FLAGS}")
#set(InSANE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")
#set(CMAKE_Fortran_FLAGS " -D\"InSANE_PDF_GRID_DIR='${InSANE_PDF_GRID_DIR}'\" -g -O2 -Wall -fPIC -Iinclude -ffixed-line-length-none -x f77-cpp-input ")
#set(CMAKE_Fortran_FLAGS_RELEASE "${CMAKE_Fortran_FLAGS}")
#message(" CMAKE_Fortran_FLAGS  : ${CMAKE_Fortran_FLAGS}")
#message("  CMAKE_CXX_FLAGS     : ${CMAKE_CXX_FLAGS}")

# ---------------------------------------
#foreach(aSharedLib ${INSANE_LIB_NAMES})
#   set(InSANE_CXX_LIBS      "${InSANE_CXX_LIBS} -linsane${aSharedLib} ")
#endforeach(aSharedLib)

# configure a header file to pass some of the CMake settings to the source code
configure_file (
   "${PROJECT_SOURCE_DIR}/src/insaneConfig.h.in"
   "${PROJECT_BINARY_DIR}/insaneConfig.h"
   )

#----------------------------------------------------------------------------
# Add the executable
#add_executable(insane-config insane-config.cxx )
add_subdirectory(config)


#set(INSANE_SHARED_LIBS)
#set(INSANE_LIBRARIES)
#set(INSANE_ROOT_DICTS)
#set(INSANE_PCM_FILES)

# ---------------------------------------
#foreach(aSharedLib ${INSANE_LIB_NAMES})
#   set(INSANE_LIBRARIES ${INSANE_LIBRARIES} insane${aSharedLib})
#   #set(INSANE_SHARED_LIBS ${INSANE_SHARED_LIBS} "${CMAKE_BINARY_DIR}/lib/InSANE${aSharedLib}.so")
#   set(INSANE_ROOT_DICTS ${INSANE_ROOT_DICTS} "insane${aSharedLib}Dict.h")
#   set(INSANE_ROOT_DICT_SRCS ${INSANE_ROOT_DICT_SRCS} "insane${aSharedLib}Dict.cxx")
#   set(INSANE_PCM_FILES ${INSANE_PCM_FILES} "insane${aSharedLib}Dict_rdict.pcm")
#   include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src/${aSharedLib})
#endforeach(aSharedLib)
#ADD_CUSTOM_TARGET(ROOTDICTS DEPENDS ${INSANE_ROOT_DICT_SRCS} ${INSANE_ROOT_DICT_HEADERS})


foreach(aSharedLib ${INSANE_LIB_NAMES})
   add_subdirectory("src/${aSharedLib}")
endforeach(aSharedLib)


#FILE(GLOB GENERATED_HEADERS 
#   "${CMAKE_CURRENT_SOURCE_DIR}/*.h" 
#   "${PROJECT_BINARY_DIR}/insaneConfig.h")
#FILE(GLOB PCM_FILES "${PROJECT_BINARY_DIR}/*.pcm")

# -----------------------------------------------------------
# Set the library version in the main CMakeLists.txt
SET(InSANE_LIBRARY_PROPERTIES ${InSANE_LIBRARY_PROPERTIES}
    VERSION "${insane_VERSION}"
    SOVERSION "${insane_MAJOR_VERSION}"
    SUFFIX ".so"
)


# ------------------------------------------------------------------------------
# Build the binary 
#install(TARGETS InSANE DESTINATION lib)
#INSTALL(FILES ${files} DESTINATION include/insane)
#install(FILES ${PCM_FILES} DESTINATION lib)
#install(TARGETS insane-config DESTINATION bin)


add_subdirectory(doc)

# ----------------------------------------------------------------------------
# The cmake stuff goes here

# Add all targets to the build-tree export set
#export(TARGETS InSANE FILE "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Targets.cmake")
# TODO: replace "InSANE" with smaller libraries

# Export the package for use from the build-tree
# (this registers the build-tree with a global CMake-registry)
export(PACKAGE ${PROJECT_NAME})

#set(CONF_LIBRARIES 
#   ${PROJECT_NAME}  # TODO: Break big library into smaller ones
#   )
#
#set(CONF_EXECUTABLES 
#   insane-config
#   )
#
#set(CONF_INCLUDE_DIRS 
#   ${CMAKE_INSTALL_PREFIX}/include
#   ${CMAKE_INSTALL_PREFIX}/include/${PROJECT_NAME}
#   )
#
#set(CONF_LIBRARY_DIRS 
#   ${CMAKE_INSTALL_PREFIX}/lib
#   )

 #configure_file(config/${PROJECT_NAME}Config.cmake.in "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${PROJECT_NAME}Config.cmake" @ONLY)
 #configure_file(config/${PROJECT_NAME}ConfigVersion.cmake.in "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${PROJECT_NAME}ConfigVersion.cmake" @ONLY)

# Install the FooBarConfig.cmake and FooBarConfigVersion.cmake
#install(FILES
#  "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${PROJECT_NAME}Config.cmake"
#  "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${PROJECT_NAME}ConfigVersion.cmake"
#  DESTINATION lib/${PROJECT_NAME} COMPONENT dev)
#
## Install the export set for use with the install-tree
#install(EXPORT ${PROJECT_NAME}Targets
#  DESTINATION lib/${PROJECT_NAME} 
#  COMPONENT dev)

# ---------------------------------
# 
if(INSTALL_LMOD_FILES)
  configure_file (
    etc/lmod.in
    ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_VERSION}
    )

  install(
    FILES ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_VERSION}
    DESTINATION ${LMOD_INSTALL_PREFIX}/${PROJECT_NAME}
    )
endif(INSTALL_LMOD_FILES)

# -------------------------
# install library config
include(CMakePackageConfigHelpers)

# Install and export targets
install(EXPORT ${PROJECT_NAME}Targets
  FILE ${PROJECT_NAME}Targets.cmake
  NAMESPACE insane::
  DESTINATION lib/insane
  )

set(TARGETS_INSTALL_PATH lib/insane/insaneTargets.cmake)

configure_package_config_file(
  cmake/insaneConfig.cmake.in  
  insaneConfig.cmake
  INSTALL_DESTINATION lib/insane
  PATH_VARS TARGETS_INSTALL_PATH
  )

write_basic_package_version_file("insaneConfigVersion.cmake"
  VERSION ${VERSION}
  COMPATIBILITY SameMajorVersion
  )

install(FILES
  ${CMAKE_CURRENT_BINARY_DIR}/insaneConfig.cmake
  ${CMAKE_CURRENT_BINARY_DIR}/insaneConfigVersion.cmake
  DESTINATION lib/insane
  )

