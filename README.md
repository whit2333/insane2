InSANE Library   {#mainpage}
==============

[![Documentation Status](https://readthedocs.org/projects/insane/badge/?version=latest)](https://insane.readthedocs.io/en/latest/?badge=latest)
[![pipeline status](https://eicweb.phy.anl.gov/whit/insane2/badges/master/pipeline.svg)](https://eicweb.phy.anl.gov/whit/insane2/commits/master)

https://eicweb.phy.anl.gov/whit/insane2/badges/master/pipeline.svg


\tableofcontents

\section introduction Introduction

  `InSANE` is a collection C++ libraries for nuclear physics. Starting as
  a tool for analysis of the SANE experiment, `InSANE` has evolved since its
  first version and is currently being refactored for modern C++, hence \b InSANE2.
  (\e 2SANE or \e 2InSANE were considered for new names but starting with 
  a number is probably not a good idea.)
  
InSANE is a multipurpose physics library which is mostly geared towards 
experimentalists and experiment minded theorists .

It provides some of the following :

* PDFs
* Structure Functions
* Form Factors
* GPDs
* Cross Sections
* Phase space samplers ( with cross section )
* Event Generators (a collection of phase space samplers)
* Composite target yield  and rate calculators
* Much more...

\section overview Overview

InSANE consists of three modules/shared libraries:
- [core](\ref core1)
- [physics](src/physics/physics)
- [experiments](src/experiments/experiments)

\section quickstart Quick Start

### Using `eic-shell`

Using the [`eic-shell`](https://github.com/eic/eic-shell) to download singularity images is a convenient way to get setup.
However, building the documentation requires `doxygen` which is not in the container.

```
git clone https://eicweb.phy.anl.gov/whit/insane2.git
mkdir build && cd build
curl -L https://github.com/eic/eic-shell/raw/main/install.sh | bash -s -- -c jug_dev -v testing
sudo singularity build --sandbox development/ local/lib/jug_dev-testing.sif
sudo singularity shell --writable development/
## inside container
apt update && apt install --no-install-recommends graphviz doxygen texlive
exit
## 
sudo singularity build insane2_dev.sif development/
singularity shell insane2_dev.sif
cmake ../insane2/. -DCMAKE_INSTALL_PREFIX=$HOME
make doc_doxygen 
make -j4 install
```
If you do not need the docs, you can just use `eic-shell` as usual.


### Building source

  - `InSANE2` requires a \b C++17 compiler! But you might be OK with C++14 ;) 
  - [ROOT](https://root.cern.ch/) (compiled with `-Dcxx17:BOOL=ON -Droot7:BOOL=ON` among others)
  - [EIGEN](http://eigen.tuxfamily.org)
  - [CLHEP](https://gitlab.cern.ch/CLHEP/CLHEP)

```
git clone https://eicweb.phy.anl.gov/whit/insane2.git
mkdir build && cd build
cmake ../insane2/. -DCMAKE_INSTALL_PREFIX=$HOME
make -j4 install

# then make sure that paths are configured
export PATH=$HOME/bin:$PATH
export LD_LIBRARY_PATH=$HOME/lib:$LD_LIBRARY_PATH
```




